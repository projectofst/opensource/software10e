/*
 * hw is a in wheel data aquisition software used in Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128 // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4 // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <xc.h>

// NOTE: Always include timing.h
#include "lib_pic33e/adc.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/flash.h"
#include "lib_pic33e/pps.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/timing.h"
#include "lib_pic33e/trap.h"

#include "can-ids-v2/can_cfg.h"
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"

#include "shared/mov_avg/mov_avg.h"
#include "shared/scheduler/scheduler.h"

#include "hw.h"
#include "hw_app.h"
#include "hw_version.h"

can_t can_global;
uint32_t timer;
uint16_t adc_data[N_SENSORS];
uint32_t flash[CFG_HW_SIZE];
uint32_t param[CFG_HW_SIZE] = { [0 ... CFG_HW_SIZE - 1] = 0xFFFFFFFF };
hw_t hw;

void dev_send_msg(CANdata msg)
{
    send_can1(msg);

    return;
}

uint16_t dev_get_id()
{

    return DEVICE_ID_HW;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{
    multiple_return_t mt = { 0 };

    if (id == CMD_MASTER_SAVE_FLASH) {
        write_flash(flash, 0, CFG_HW_SIZE);
    }

    return mt;
}

void cfg_set_callback(uint32_t* table)
{
    uint16_t i;

    for (i = 0; i < CFG_HW_SIZE; i++)
        flash[i] = table[i];

    return;
}

void timer1_callback(void)
{
    timer++;

    return;
}
#ifndef FAKE
void debug_led(void)
{
    LATEbits.LATE0 ^= 1;

    return;
}
#endif

#ifdef FAKE
void debug_led(void)
{
    return;
}
#endif
void adc_average(void)
{
    uint8_t i;

    get_pin_cycling_values(adc_data, 0, N_SENSORS);

    for (i = 0; i < N_SENSORS; i++) {
        printf("adc_value %d %d \n", i, adc_data[i]);
        moving_average(adc_data[i], i);
    }
    return;
}
uint16_t ntc_value(uint16_t adc, uint16_t b_param, float ntc_r, float r)
{
    printf("adc %d\n", adc);
    if (adc < 400 || adc > 4083)
        return 5000;

    return (1 / ((log(r * (4096.0 - adc) / (ntc_r * adc)) / b_param) + 1 / 298.15) - 273.15) * 10;
}

uint16_t thermopile_value(uint16_t adc)
{
    // float s = *(hw.bd.tp_s);
    float s = 5 * pow(10, -11);
    uint16_t t_ref = ntc_value(get_ma_value(BD_NTC), *(hw.bd.b_param), *(hw.bd.ntc_r), *(hw.bd.r));
    uint16_t gain = *(hw.bd.tp_gain);

    printf("t_ref:%d", t_ref);
    printf("s: %.15f", s);

    printf("adc_tp: %d\n", adc);
    printf("pow1 %lf\n", pow((double)((adc * 3.3) / (gain * 4096 * s)), 0.25));
    printf("pow %lf\n", pow((double)(adc * 3.3 / (gain * 4096 * s) + pow(t_ref + 273.15, 4)), 0.25));

    return pow(adc * 3.3 / (gain * 4096 * s) + pow(t_ref + 273.15, 4), 0.25) - 273.15;
}

void temp_value(void)
{
    printf("adc_data: %d\n", adc_data[2]);
    printf("ma_value %d\n", get_ma_value(BD_THERMOPILE));

    HW_UR_NTC(can_global, HW_POS) = ntc_value(get_ma_value(UR_NTC), *(hw.ur.b_param), *(hw.ur.ntc_r), *(hw.ur.r));
    HW_BD_NTC(can_global, HW_POS) = ntc_value(get_ma_value(BD_NTC), *(hw.bd.b_param), *(hw.bd.ntc_r), *(hw.bd.r));
    HW_BD_TP(can_global, HW_POS) = thermopile_value(get_ma_value(BD_THERMOPILE));

    printf("ma_value %d\n", get_ma_value(BD_THERMOPILE));

    printf("bd_ntc: %d\n", HW_BD_NTC(can_global, HW_POS));
    printf("bd_tp: %d\n", HW_BD_TP(can_global, HW_POS));

    send_can1(HW_ENCODE(HW_POS)(HW_STATUS(can_global, HW_POS)));

    reset_moving_average();

    return;
}

void status_number(void)
{
    HW_ALIVE(can_global, HW_POS) = WHEEL_NUMBER;

    return;
}

void hw_init(void)
{
    uint8_t i = 0;

    param[CFG_HW_VERSION] = VERSION;

    hw.bd.b_param = (uint16_t*)&param[CFG_HW_BD_B_PARAM];
    hw.default_values[CFG_HW_BD_B_PARAM] = BD_B_PARAM;

    hw.bd.ntc_r = (float*)&param[CFG_HW_BD_NTC_R];
    hw.default_values[CFG_HW_BD_NTC_R] = BD_NTC_R;

    hw.bd.r = (float*)&param[CFG_HW_BD_R];
    hw.default_values[CFG_HW_BD_R] = BD_R;

    hw.bd.tp_s = (float*)&param[CFG_HW_BD_TP_S];
    /*
     * TODO: There was a merge conflict here. Below is what was supposed to be more
     * up to date. But commented remains what looks like the more correct
     * solution.
     *
     * Ass: joajfreitas
    float bd_tp_s = 5 * pow(10, -11);
    hw.default_values[CFG_HW_BD_TP_S] = *((uint32_t*)&bd_tp_s);
    */
    hw.default_values[CFG_HW_BD_TP_S] = 5 * pow(10, -11);

    hw.bd.tp_gain = (uint16_t*)&param[CFG_HW_BD_TP_G];
    hw.default_values[CFG_HW_BD_TP_G] = BD_TP_GAIN;

    hw.ur.b_param = (uint16_t*)&param[CFG_HW_UR_B_PARAM];
    hw.default_values[CFG_HW_UR_B_PARAM] = UR_B_PARAM;

    hw.ur.ntc_r = (float*)&param[CFG_HW_UR_NTC_R];
    hw.default_values[CFG_HW_UR_NTC_R] = UR_NTC_R;

    hw.ur.r = (float*)&param[CFG_HW_UR_R];
    hw.default_values[CFG_HW_UR_R] = UR_R;

    read_flash(flash, 0, 2 * CFG_HW_SIZE);

    printf("backing up from flash\n");
    for (i = 1; i < CFG_MASTER_SIZE; i++) {
        printf("flash[%d] = %d\n", i, flash[i]);
        if (flash[i] == 0xFFFFFFFF || flash[i] == 0x0) {
            param[i] = hw.default_values[i];
            flash[i] = hw.default_values[i];
        } else
            param[i] = flash[i];
    }

    write_flash(flash, 0, 2 * CFG_HW_SIZE);

    cfg_config(param, CFG_HW_SIZE);

    return;
}

int main()
{
    ADC_parameters adc_config;
    tasks_t hw_tasks[N_TASKS] = {
        { .period = 500, .func = debug_led },
        { .period = 50, .func = adc_average },
        { .period = 1500, .func = temp_value },
    };

    scheduler_init(hw_tasks, N_TASKS);

    PPSUnLock;
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118); // CAN1TX in RPI119
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119); // CAN1RX in RP118
    PPSLock;

    config_can1(NULL);

    config_pin_cycling(AN3 | AN4 | AN5, 10UL, 64, 4, &adc_config);
    config_adc1(adc_config);

    send_can1(logD(LOG_RESET_MESSAGE, 0, 0, 0));

    config_timer1(1, 4);

    TRISEbits.TRISE0 = 0;

    hw_init();

    status_number();

    while (1) {
        scheduler(timer);

        ClrWdt();
    } // end while

    return 0;
} // end main

void trap_handler(TrapType type)
{

    switch (type) {
    case HARD_TRAP_OSCILATOR_FAIL:
        break;
    case HARD_TRAP_ADDRESS_ERROR:
        break;
    case HARD_TRAP_STACK_ERROR:
        break;
    case HARD_TRAP_MATH_ERROR:
        break;
    case CUSTOM_TRAP_PARSE_ERROR:
        break;
    default:
        break;
    }

    return;
}
