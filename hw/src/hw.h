/* 
 * hw is a in wheel data aquisition software used in Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __HW_H__
#define __HW_H__

#include "hw_app.h"
#include <stdint.h>

#define N_TASKS 3
#define N_SENSORS 3

#define UR_NTC 0
#define BD_NTC 1
#define BD_THERMOPILE 2

#define UR_B_PARAM 3971
#define UR_NTC_R 10000
#define UR_R 2200

#define BD_B_PARAM 3960
#define BD_NTC_R 100000
#define BD_R 100000
#define BD_TP_GAIN 100

// clang-format off
#define HW_ALIVE(can, HW) HW_ALIVE_(can, HW)
#define HW_ALIVE_(can, HW) can.hw.hw_status_ ## HW.hw_alive_ ## HW

#define HW_UR_NTC(can, HW) HW_UR_NTC_(can, HW)
#define HW_UR_NTC_(can, HW) can.hw.hw_status_ ## HW.hw_ur_temp_ ## HW

#define HW_BD_NTC(can, HW) HW_BD_NTC_(can, HW)
#define HW_BD_NTC_(can, HW) can.hw.hw_status_ ## HW.hw_bd_ntc_ ## HW

#define HW_BD_TP(can, HW) HW_BD_TP_(can, HW)
#define HW_BD_TP_(can, HW) can.hw.hw_status_ ## HW.hw_bd_tp_ ## HW

#define HW_ENCODE(HW) HW_ENCODE_(HW)
#define HW_ENCODE_(HW) encode_hw_status_ ## HW

#define HW_STATUS(can, HW) HW_STATUS_(can, HW)
#define HW_STATUS_(can, HW) can.hw.hw_status_ ## HW
// clang-format on

typedef struct ur_struct {
    uint16_t* b_param;
    float* ntc_r;
    float* r;
} ur_t;

typedef struct bd_struct {
    uint16_t* b_param;
    float* ntc_r;
    float* r;

    float* tp_s;
    uint16_t* tp_gain;
} bd_t;

typedef struct hw_struct {
    ur_t ur;
    bd_t bd;
    uint32_t default_values[CFG_HW_SIZE];

} hw_t;

#endif
