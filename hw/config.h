/* 
 * hw is a in wheel data aquisition software used in Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

#define VIO_PORT 8888
#define VIO_HOST "127.0.0.1"
#define SIM_TIME 100

#define VIO_ADC1_CONFIG {{"ntc_ur", 0}, {"ntc_bd", 0}, {"tp_bd", 0}, {"adc4", 0}, {"adc5", 0}, {"adc6", 0}, {"adc7", 0}, {"adc8", 0}, {NULL, 0}}

#define CAN_ESSENTIAL	57005
#define CAN_SENSOR		48879
#define CAN_AMK			43962

#define PORT1			CAN_ESSENTIAL
#define PORT2			CAN_SENSOR

#define SIM_DELTA_T_US	100
#endif
