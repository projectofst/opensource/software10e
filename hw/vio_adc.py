##
# hw is a in wheel data aquisition software used in Formula
# Student cars developed by FST Lisboa.
# Copyright © 2021 Projecto FST.

# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.

# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
##

import sys, time, click, socket

from threading import Thread
import queue


def adc_pack(id, value):
    bs = []
    bs.append((value >> 24) & 0xFF)
    bs.append((value >> 16) & 0xFF)
    bs.append((value >> 8) & 0xFF)
    bs.append((value >> 0) & 0xFF)
    bs.append(len(id))
    for i, c in enumerate(id):
        bs.append(ord(c))
    return bytearray(bs)


def read_input_thread(name, q):
    while True:
        r = input("> ")
        print(f"Cmd: {r}")
        q.put(r)


def send_adcs_thread(period, sock, host, port, q):
    values = [1758, 2337, 1000]

    def update_value(var, index, value):
        var[index] += value

    key_actions = {
        "q": lambda x: update_value(values, 0, x),
        "a": lambda x: update_value(values, 0, -x),
        "w": lambda x: update_value(values, 1, x),
        "s": lambda x: update_value(values, 1, -x),
        "e": lambda x: update_value(values, 2, x),
        "d": lambda x: update_value(values, 2, -x),
    }

    last_cmd = "q"
    last_jump = 10
    while True:
        if not q.empty():
            user = q.get()
            user = user.split(" ")

            if len(user) == 0 or (len(user) == 1 and user[0] == ""):
                cmd = last_cmd
                jump = last_jump
            else:
                cmd = user[0]

            if len(user) > 1:
                try:
                    jump = int(user[1])
                except Exception as e:
                    jump = 10
            else:
                jump = last_jump

            v = key_actions.get(cmd)

            if v is not None:
                v(jump)
                last_cmd = cmd
                last_jump = jump

        id = "ntc_ur"
        value = values[0]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "ntc_bd"
        value = values[1]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "tp_bd"
        value = values[2]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        time.sleep(period / 1000)


@click.group(invoke_without_command=True)
@click.argument("host")
@click.argument("port", type=int)
@click.argument("period", type=float)
def main(host, port, period):
    q = queue.Queue()

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    send_thread = Thread(target=send_adcs_thread, args=(period, sock, host, port, q))
    read_input = Thread(target=read_input_thread, args=("read_input", q))

    print(
        """
    Command syntax: <cmd> <increment>
    A single return will repeat the last command.

    q - increment ntc_ur
    a - decrement ntc_ur
    w - increment ntc_bd
    s - decrement ntc_bd
    e - increment tp_bd
    d - decrement tp_bd
    """
    )

    send_thread.start()
    read_input.start()

    send_thread.join()
    read_input.join()


if __name__ == "__main__":
    main()
