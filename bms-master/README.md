# Readme

## Info

This code is part of *FST BMS*.

## Code structure

This project no longer makes use of submodules!
Don't remember that when cloning.

Code specific to the master module is within src/

## Building

`make` to target a dsPIC33EP256MU806
`make fake` to target your current computer architecture
`make test` run unit tests

## Contributing

Follow the guidelines in [README.md](../README.md)

## Authors

[AUTHORS](AUTHORS)
