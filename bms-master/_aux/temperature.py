from math import *
from matplotlib import pyplot as plt
import numpy as np


def beta_formula(T, beta):
    T0 = 25 + 273.15
    T = T + 273.15
    return 10e3 * e ** (beta * (1 / T - 1 / T0))


def vt_formula(NTC, R, v_ref):
    return NTC / (R + NTC) * v_ref


def polinomial_7(coef, x):
    sum = 0
    p1, p2, p3, p4, p5, p6, p7, p8 = tuple(coef)

    return (
        p1 * x**7
        + p2 * x**6
        + p3 * x**5
        + p4 * x**4
        + p5 * x**3
        + p6 * x**2
        + p7 * x**1
        + p8 * x**0
    )


beta1 = 3936  # 25 - 50
beta2 = 3971  # 25 - 80
vref = 3

curve = [
    (t, ceil(vt_formula(beta_formula(t, beta1), 10e3, vref) * 10e3))
    for t in range(-20, 120, 1)
]
curve = [list(l) for l in zip(*curve)]

coef = np.polyfit(curve[1], curve[0], 7)

fitted_curve = [polinomial_7(coef, i) for i in range(0, vref * 10000)]

minimum, maximum = (0, 0)
for i, x in enumerate(fitted_curve):
    if x < -20 and minimum == 0:
        minimum = i
    if x < 120 and maximum == 0:
        maximum = i

windowed_curve = [ceil(x * 10) for x in fitted_curve if (x > -20 and x < 120)]

print(minimum, maximum)
for w in windowed_curve:
    print(w)

# plt.plot(range(0,len(windowed_curve)), windowed_curve)
# plt.show()
