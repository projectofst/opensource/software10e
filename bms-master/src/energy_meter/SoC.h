/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _SOC_H_
#define _SOC_H_

#include <stdint.h>
#include <stdlib.h>

#include "../battery.h"

#define  MIN_CELL_VOLTAGE 33700
#define  MAX_CELL_VOLTAGE 41600
#define  SOC_TABLE_SIZE 790
#define  AUTO_TRIGGER_SLOPE 1


uint16_t estimate_cell_SoC(uint16_t voltage/*, int16_t current*/);

uint32_t estimate_soc(Battery* battery);

void update_battery_SoC(Battery* battery);

uint16_t estimate_soc_backup(uint16_t voltage);

void compute_battery_cell_soc(Battery* battery);

void calibrate_SoC(Battery* battery);

void soc_init(Battery* battery);

void automatic_calibration(Battery* battery);


#endif
