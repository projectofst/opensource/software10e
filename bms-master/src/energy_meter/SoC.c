/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>

#include "SoC.h"
#include "battery.h"

uint32_t timer;

uint16_t soc_table[SOC_TABLE_SIZE] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5,
    5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 9,
    9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12,
    13, 13, 13, 13, 14, 14, 14, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 17, 17,
    17, 17, 18, 18, 18, 18, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 22, 22,
    22, 23, 23, 23, 24, 24, 24, 24, 25, 25, 25, 26, 26, 26, 27, 27, 27, 28, 28,
    28, 28, 29, 29, 29, 30, 30, 30, 31, 31, 31, 32, 32, 32, 33, 33, 33, 34, 34,
    34, 35, 35, 35, 36, 36, 36, 37, 37, 37, 38, 38, 38, 39, 39, 39, 40, 40, 40,
    41, 41, 41, 42, 42, 43, 43, 43, 44, 44, 44, 45, 45, 45, 46, 46, 46, 47, 47,
    47, 48, 48, 48, 49, 49, 49, 50, 50, 50, 51, 51, 51, 52, 52, 52, 53, 53, 53,
    54, 54, 54, 55, 55, 55, 56, 56, 56, 57, 57, 57, 58, 58, 58, 59, 59, 59, 59,
    60, 60, 60, 61, 61, 61, 62, 62, 62, 62, 63, 63, 63, 64, 64, 64, 65, 65, 65,
    65, 66, 66, 66, 66, 67, 67, 67, 68, 68, 68, 68, 69, 69, 69, 69, 70, 70, 70,
    70, 71, 71, 71, 71, 72, 72, 72, 72, 73, 73, 73, 73, 73, 74, 74, 74, 74, 75,
    75, 75, 75, 75, 76, 76, 76, 76, 76, 77, 77, 77, 77, 77, 78, 78, 78, 78, 78,
    79, 79, 79, 79, 79, 79, 80, 80, 80, 80, 80, 80, 81, 81, 81, 81, 81, 81, 81,
    82, 82, 82, 82, 82, 82, 82, 83, 83, 83, 83, 83, 83, 83, 84, 84, 84, 84, 84,
    84, 84, 84, 85, 85, 85, 85, 85, 85, 85, 85, 85, 86, 86, 86, 86, 86, 86, 86,
    86, 86, 86, 87, 87, 87, 87, 87, 87, 87, 87, 87, 87, 87, 88, 88, 88, 88, 88,
    88, 88, 88, 88, 88, 88, 89, 89, 89, 89, 89, 89, 89, 89, 89, 89, 89, 90, 90,
    90, 90, 90, 90, 90, 90, 90, 90, 90, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91,
    91, 92, 92, 92, 92, 92, 92, 92, 92, 92, 92, 93, 93, 93, 93, 93, 93, 93, 93,
    93, 94, 94, 94, 94, 94, 94, 94, 94, 95, 95, 95, 95, 95, 95, 95, 95, 96, 96,
    96, 96, 96, 96, 96, 96, 97, 97, 97, 97, 97, 97, 97, 98, 98, 98, 98, 98, 98,
    98, 99, 99, 99, 99, 99, 99, 99, 100, 100, 100, 100, 100, 100, 100, 100,
    100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
    100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
    100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
    100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
    100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
    100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 };

/**********************************************************************
 * Name:	estimate_cell_SOC
 * Args:	unsigned int voltage
 * Return:	estimated SOC
 * Desc:	Provide an estimation of SOC in %*10 for a single cell.
 *
 *          Without Coulomb counting, there's no other way than
 *          moddeling the discharge curve!
 **********************************************************************/

void soc_init(Battery* battery)
{
    battery->energy_meter.soc_calibrated = 0;
    battery->energy_meter.volt_updated = 0;
    battery->energy_meter.offset_charge = 0;
    battery->energy_meter.soc_calibrated_forced = 0;
    battery->energy_meter.auto_time = timer;
    battery->energy_meter.prev_volt = 0;
    battery->energy_meter.prev_time = 0;
    battery->energy_meter.auto_state = 0;
}

uint16_t estimate_cell_SoC(uint16_t voltage)
{
    uint16_t soc;

    if (voltage <= 32000) {
        soc = 0;
    } else if (voltage >= 41500) {
        soc = 1000;

        // [V_CRITICAL_L, 3.4](V) --- [0, 20](%)
    } else if (voltage <= 36600) {
        soc = (uint16_t)(0.0933 * (float)voltage / 10.0 - 280);
        // ]3.4, 3.7](V) --- ]20, 90](%)
    } else if (voltage <= 40000) {
        soc = (uint16_t)(2.332 * (float)voltage / 10.0 - 8475);

        // ]3.7, V_CRITICAL_H](V) --- ]90, 100](%)
    } else {
        soc = (uint16_t)(0.9693 * (float)voltage / 10.0 - 3023);
    }

    return soc;
}

uint32_t estimate_soc(Battery* battery)
{
    uint32_t soc;
    uint32_t updated_charge;
    uint32_t initial_capacity;
    uint32_t isa_charge;
    uint32_t voltage = battery->min_voltage;

    // If min voltage is within the required interval, we can calculate
    // updated charge and soc based on isa readings.

    if (((voltage - MIN_CELL_VOLTAGE) / 10) < 0)
        soc = 0;
    else if (((voltage - MIN_CELL_VOLTAGE) / 10) > (SOC_TABLE_SIZE - 1))
        soc = 1000;
    else {
        initial_capacity = battery->energy_meter.effective_cell_charge; // [mAh]
        isa_charge = (battery->energy_meter.charge * 10) / 36; //[As] -> [mAh]
        if ((isa_charge - battery->energy_meter.offset_charge) < initial_capacity) {
            updated_charge = (initial_capacity - (isa_charge - battery->energy_meter.offset_charge));
            soc = ((updated_charge * 1000) / battery->energy_meter.aparent_cell_nominal_capacity);
        } else
            soc = 0; // To prevent soc underflow
        // print soc.
        // printf("soc: %d\n\n", soc);
    }
    return soc;
}

uint16_t estimate_soc_backup(uint16_t voltage)
{
    uint16_t soc;
    if (voltage < 33700) {
        soc = 0;
    } else if (voltage > 41600) {
        soc = 1000;
    } else {
        soc = soc_table[(voltage - 33700) / 10];
    }

    return 10 * soc;
}

void update_battery_SoC(Battery* battery)
{
#if ISA_USED == true
    battery->energy_meter.state_of_charge = estimate_soc(battery);

    if (battery->energy_meter.isa_wtd > ISA_WTD_TIMEOUT)
#else
    battery->energy_meter.state_of_charge = estimate_soc_backup(battery->min_voltage);
#endif
        return;
}

void compute_battery_cell_soc(Battery* battery)
{

    uint16_t i = 0;
    uint16_t j = 0;

    for (i = 0; i < BATTERY_SERIES_STACKS; i++) {

        for (j = 0; j < BATTERY_SERIES_CELLS_PER_STACK; i++) {

            battery->stacks[i].cell_pack[j].state_of_charge = estimate_cell_SoC(battery->stacks[i].cell_pack[j].voltage);
        }
    }

    return;
}

void calibrate_SoC(Battery* battery)
{
    uint32_t voltage = battery->min_voltage;
    uint32_t soc;
    uint32_t isa_charge;

    // If min voltage is within the required interval, we go to the soc table
    // to get our initial soc value.
    // If soc_table is altered, these conditions may not be adequate.
    if (((voltage - MIN_CELL_VOLTAGE) / 10) < 0) {
        soc = 0;
    } else if (((voltage - MIN_CELL_VOLTAGE) / 10) > (SOC_TABLE_SIZE - 1)) {
        soc = 100;
    } else if (battery->energy_meter.soc_calibrated_forced) {
        soc = battery->energy_meter.forced_soc;
        battery->energy_meter.soc_calibrated_forced = 0;
    } else {
        // Fetch soc from table based on voltage
        soc = soc_table[(voltage - MIN_CELL_VOLTAGE) / 10];
    }

    battery->energy_meter.aparent_cell_nominal_capacity = *(battery->settable.bms_aparent_cell_nominal_capacity);
    // Calculate effective initial capacity [mAh].
    battery->energy_meter.effective_cell_charge = (soc * battery->energy_meter.aparent_cell_nominal_capacity) / 100;
    // Store state of charge (i.e. 99% = 990).
    battery->energy_meter.state_of_charge = soc * 10;
    // Mark calibration as done.
    battery->energy_meter.soc_calibrated = 1;
    // Calculate state of health based on aparent capacity vs spec capacity.
    battery->energy_meter.state_of_health = (battery->energy_meter.aparent_cell_nominal_capacity * 1000) / battery->energy_meter.spec_cell_nominal_capacity;
    // Store isa charge value on calibration for offseting the soc calculation
    isa_charge = (battery->energy_meter.charge * 10) / 36;
    battery->energy_meter.offset_charge = isa_charge;
    // Print calibration info.
    // printf("Offset charge: %d\n\n",battery->energy_meter.offset_charge);
    printf("calibration done: %d (charge) vs %d (capacity) [mAh]\n", battery->energy_meter.effective_cell_charge, battery->energy_meter.aparent_cell_nominal_capacity);
    printf("soc: %d\n", soc);
    printf("State of Health: %d\n\n", battery->energy_meter.state_of_health);
    return;
}

void auto_calibration_check(Battery* battery)
{
    // Calculates slope for min voltage values.
    int slope = (int)((int32_t)battery->min_voltage - battery->energy_meter.prev_volt) / (float)(timer - battery->energy_meter.prev_time);
    // A time threshold is set to determine for how long the slope can remain below the set value.
    uint32_t time_threshold = 1000 * (*(battery->settable.bms_auto_calibration_time_threshold)); //[s]->[ms]

    battery->energy_meter.prev_volt = battery->min_voltage;
    battery->energy_meter.prev_time = timer;

    // Checks if slope is within set value.
    if (abs(slope) <= AUTO_TRIGGER_SLOPE) {
        // If enough time as passed, set flag to 0 to perform calibration.
        if (battery->energy_meter.auto_state == 1 && timer - battery->energy_meter.auto_time >= time_threshold) {
            battery->energy_meter.soc_calibrated = 0;
            battery->energy_meter.auto_state = 0;
            printf("calibrated automatically\n");
        }
        // If slope has just gone below set value, we save a timestamp as the starting point for counting time.
        else if (battery->energy_meter.auto_state == 0) {
            battery->energy_meter.auto_time = timer;
            battery->energy_meter.auto_state = 1;
        }
    }
    // If slope is not within set value, state flag is set to 0 to reset auto calibration check.
    else if (battery->energy_meter.auto_state)
        battery->energy_meter.auto_state = 0;
}

void calculate_battery_SoC(Battery* battery)
{
    if (battery->energy_meter.volt_updated) {
        if (!battery->energy_meter.soc_calibrated) {
            calibrate_SoC(battery);
        } else {
            battery->energy_meter.state_of_charge = estimate_soc(battery);
        }
    }
    auto_calibration_check(battery);
}
