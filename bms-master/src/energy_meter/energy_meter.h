/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*! \file energy_meter.h
 *	\brief Contains data from IVT-S sensor, general powertrain data
 * 		   and other parameters calculated from voltages/currents, etc
 * 		   such as SoC and SoH.
 *
 *	**Author/s:** Rui Cunha, Manuel Passadoro, Miguel Lourenço
 */

#ifndef _ENERGY_METER_H_
#define _ENERGY_METER_H_

#include <stdint.h>
#include <stdbool.h> 	

/*!
 * Contains EM info, voltage, current, Soc, SoH 
 * and other auxiliary variables.
 */
typedef struct energy_meter{

	/// General EM data
	int32_t current;		    /**< Baterry output current, [mA]. */
	int32_t battery_voltage;	/**< Battery voltage before fuse, always live, [mV]. */
	int32_t power;			    /**< Battery power, calculated from battttery_voltage [W]. */

	/// (IVT-S/IVT) Sensor specific values
	int32_t fuse_voltage;	    /**< Voltage from car side fuse terminal, live when fuse is OK, [mV]. */
	int32_t output_voltage;	  	/**< Car side voltage, live when TS is ON, [mV]. */
	int32_t charge;			    /**< Sum of output and input battery Charge, resets if bms turns off, [As] */
	int32_t energy;			    /**< Sum of output and input battery energy, resets if bms turns off,  [Wh] */
	int32_t ivt_temp;			/**< Module internal temperature, [0.1ºC] */

	/// Setable values
	uint32_t spec_cell_nominal_capacity;
	uint32_t aparent_cell_nominal_capacity;	/**< Can change over time, capacity of the groups of cells in parallel, [mAh]. */
	uint32_t forced_soc;
	
	/// Aux
	uint32_t auto_time;				/**< Timestamp, start of auto calibration check (Não gosto deste comentário). */
	int32_t prev_volt;				/**< Voltage in previous iteration, used to calculate voltage slope. */
	uint32_t prev_time;				/**< Timestamp of prev_volt (Não gosto deste comentário). */
	uint32_t effective_cell_charge; /**< Battery charge after calibration. */
	int32_t offset_charge;			/**< Delta between current and previous iteration isa charge output. */
	
	/// Variables computed by BMS
	uint32_t state_of_charge;	/**< Actual capacity/Maximum capacity, value from cell with lowest SoH, [0.1%] */ 
	uint32_t state_of_health;	/**< SoH = Max_cap/Nominal_Max_Cap, [0-1], value from cell with lowest SoH */

	/// Flags
	bool auto_state;
	bool soc_calibrated_forced;
	bool soc_calibrated;
	bool volt_updated;
	int32_t isa_wtd;	/**< [ms] */

}energy_meter_t;


#endif
