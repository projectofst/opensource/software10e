/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * LTC68xx.c
 *
 * Implementation of the interface with LTC6812-1
 */

#include "LTC68xx.h"

#ifndef FAKE
#include <stdbool.h>
#include <stdint.h>
#include <xc.h>

#include "PEC.h"

#include "lib_pic33e/SPI.h"
#include "lib_pic33e/timing.h"

#endif

/**
 * @brief Initializes a LTC6812 struct
 */
void LTC_init(LTC* ltc)
{
    memset(ltc, 0, sizeof(LTC));

    return;
}

#ifndef FAKE

/**
 * @brief Wakes up an entire daisy chain from core SLEEP state
 *
 * @warning
 * The function is blocking and takes daisy_chain_n * MAX_T_WAKE.
 * This is about 2.4ms for a 6 IC daisy chain and 4.8ms for a 12 IC daisy chain
 */
void wake_up_LTC_from_SLEEP(LTC_daisy_chain* daisy_chain)
{
    unsigned int i;
    for (i = 0; i < daisy_chain->n; i++) {
        daisy_chain->enable_comm();
        daisy_chain->disable_comm();
        __delay_us(MAX_T_WAKE);
    }
    daisy_chain->reset_interface_watchdog();
    daisy_chain->core_state = CORE_STANDBY_STATE;

    return;
}

/**
 * @brief Wakes up an entire daisy chain from isoSPI interface IDLE state
 *
 * @warning
 * The function is blocking and takes daisy_chain_n * MAX_T_READY.
 * This is about 60us for a 6 IC daisy chain and 120us for a 12 IC daisy chain
 */
void wake_up_LTC_from_IDLE(LTC_daisy_chain* daisy_chain)
{
    unsigned int i;
    for (i = 0; i < daisy_chain->n; i++) {
        daisy_chain->enable_comm();
        daisy_chain->disable_comm();
        __delay_us(MAX_T_READY);
    }
    daisy_chain->reset_interface_watchdog();
    daisy_chain->interface_state = ISOSPI_READY_STATE;

    return;
}

/**
 * @brief Ensures the daisy_chain is waken and enables SS
 */
void start_comm_with_LTC(LTC_daisy_chain* daisy_chain)
{
    if (daisy_chain->core_state == CORE_SLEEP_STATE) {
        wake_up_LTC_from_SLEEP(daisy_chain);

    } else if (daisy_chain->interface_state == ISOSPI_IDLE_STATE) {
        wake_up_LTC_from_IDLE(daisy_chain);
    }
    daisy_chain->enable_comm();

    return;
}

/**
 * @brief Disables SS
 */
void end_comm_with_LTC(LTC_daisy_chain* daisy_chain)
{
    daisy_chain->disable_comm();
    daisy_chain->reset_interface_watchdog();
    // fake the interface goes idle so the daisy_chain is woken every time
    daisy_chain->interface_state = ISOSPI_IDLE_STATE;

    return;
}

/**
 * @brief Broadcasts a poll command
 *
 * @param [in]  daisy_chain LTC68xx daisy chain to broadcast the command
 * @param [in]  command     Command to broadcast
 */
int broadcast_poll(LTC_daisy_chain* daisy_chain, unsigned int command)
{

    unsigned char message[4];
    unsigned int PEC;
    unsigned int transmitted_bytes;
    message[0] = command >> 8;
    message[1] = command;
    PEC = PEC_calculate(message, 2);
    message[2] = PEC >> 8;
    message[3] = PEC;

    start_comm_with_LTC(daisy_chain);
    transmitted_bytes = SPI2_ExchangeBuffer(message, 4, NULL);
    end_comm_with_LTC(daisy_chain);
    if (transmitted_bytes != 4)
        return -1;

    return 0;
}

/**
 * @brief Writes size*daisy_chain.n bytes from the location pointed by data and
 * sends them to the daisy_chain using command.
 *
 * @param   [in]    daisy_chain LTC6812 daisy chain to write to.
 * @param   [in]    command     Command to use to write
 * @param   [in]    size        Number of bytes to write to each LTC6812
 * @param   [in]    data        Location where the data to write is
 */
void broadcast_write(
    LTC_daisy_chain* daisy_chain,
    unsigned int command,
    unsigned int size,
    unsigned char* data)
{

    unsigned int i, slave;
    unsigned int PEC, n;
    unsigned char message[4 + daisy_chain->n * (size + 2)];

    message[0] = command >> 8;
    message[1] = command;
    PEC = PEC_calculate(message, 2);
    message[2] = PEC >> 8;
    message[3] = PEC;

    n = daisy_chain->n;

    for (slave = 0; slave < n; ++slave) {

        // (n-1-slave) reverses the order of the data so the right slaves receive
        // the right data
        for (i = 0; i < size; i++) {
            message[4 + (n - 1 - slave) * (size + 2) + i] = data[slave * size + i];
        }
        PEC = PEC_calculate(&data[slave * size], size);
        message[4 + (n - 1 - slave) * (size + 2) + size + 0] = PEC >> 8;
        message[4 + (n - 1 - slave) * (size + 2) + size + 1] = PEC;
    }

    start_comm_with_LTC(daisy_chain);
    SPI2_ExchangeBuffer(message, 4 + n * (size + 2), NULL);
    end_comm_with_LTC(daisy_chain);

    return;
}

/**
 * @brief Reads size*daisy_chain.n bytes from the daisy_chain using command and
 * stores them in the location pointed by data
 *
 * @param   [in]    daisy_chain LTC6812 daisy chain from where to read.
 * @param   [in]    command     Command to use to read
 * @param   [in]    size        Number of bytes to read from each LTC6812
 * @param   [out]   data        Location to store the read data
 */
int broadcast_read(LTC_daisy_chain* daisy_chain, unsigned int command, unsigned int size, unsigned char* data)
{

    unsigned char command_message[4];
    unsigned char data_PEC[2];
    unsigned int PEC;
    unsigned int command_PEC;
    unsigned int slave;
    unsigned int everything_is_valid = true;

    command_message[0] = command >> 8;
    command_message[1] = command;
    command_PEC = PEC_calculate(command_message, 2);
    command_message[2] = command_PEC >> 8;
    command_message[3] = command_PEC;

    start_comm_with_LTC(daisy_chain);
    SPI2_ExchangeBuffer(command_message, 4, NULL);

    for (slave = 0; slave < daisy_chain->n; slave++) {
        SPI2_ExchangeBuffer(NULL, size, &data[slave * size]);
        SPI2_ExchangeBuffer(NULL, 2, data_PEC);
        PEC = data_PEC[0] << 8 | data_PEC[1];
        if (PEC_verify(&data[slave * size], size, PEC) < 0)
            everything_is_valid = false;
    }

    end_comm_with_LTC(daisy_chain);

    if (everything_is_valid == false) {
        return -1;
    }

    return 0;
}

#endif
