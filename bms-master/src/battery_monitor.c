/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "battery_monitor.h"

#include <string.h>
#ifdef FAKE
#include <unistd.h>
#endif

#include "bat_cfg.h"
#include "battery_monitor.h"
#include "energy_meter/energy_meter.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/timing.h"
#include "lib_pic33e/utils.h"
#include "shared/scheduler/scheduler.h"
#include "signals.h"
#include "slave/slave.h"
#include "temperature.h"

uint16_t error_states[ERROR_CHECKS] = { 0 };
uint16_t error_counter_end[ERROR_CHECKS] = { 0 };

#ifdef B4V2

uint16_t error_period_table[ERROR_CHECKS] = {
    HALF_SEC, // overvoltage
    HALF_SEC, // undervoltage
    ONE_SEC, // overtemperature
    ONE_SEC, // undertemperature
    ONE_SEC, // Stack 0 ntc faults
    ONE_SEC, // Stack 1 ntc faults
    ONE_SEC, // Stack 2 ntc faults
    ONE_SEC, // Stack 3 ntc faults
    ONE_SEC, // Stack 4 ntc faults
    ONE_SEC, // Stack 5 ntc faults
    ONE_SEC, // Stack 6 ntc faults
    ONE_SEC, // Stack 7 ntc faults
    ONE_SEC, // Stack 8 ntc faults
    ONE_SEC, // Stack 9 ntc faults
    ONE_SEC, // Stack 10 ntc faults
    ONE_SEC, // Stack 11 ntc faults
    HALF_SEC, // Current positive
    HALF_SEC, // Current negative
};

uint16_t error_limits[ERROR_CHECKS] = {
    BAT_LIMIT_MAX_CELL_V,
    BAT_LIMIT_MIN_CELL_V,
    BAT_LIMIT_MAX_T_DISCH,
    BAT_LIMIT_MIN_T_DISCH,
    // MAX TEMP CHARGING
    // MIN TEMP CHARGING
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    BAT_LIMIT_MAX_I_DISCH,
    BAT_LIMIT_MAX_I_CHRG,

    // 10x OWC for each stack, with function diferent
    // 10x OWC for ntc's within a stack
    // Watchdog timer for current measurement
};

#endif

#ifdef B5V2

uint16_t error_period_table[ERROR_CHECKS] = {
    HALF_SEC, // overvoltage
    HALF_SEC, // undervoltage
    ONE_SEC, // overtemperature
    ONE_SEC, // undertemperature
    ONE_SEC, // Stack 0 ntc faults
    ONE_SEC, // Stack 1 ntc faults
    ONE_SEC, // Stack 2 ntc faults
    ONE_SEC, // Stack 3 ntc faults
    ONE_SEC, // Stack 4 ntc faults
    ONE_SEC, // Stack 5 ntc faults
    ONE_SEC, // Stack 6 ntc faults
    ONE_SEC, // Stack 7 ntc faults
    ONE_SEC, // Stack 8 ntc faults
    ONE_SEC, // Stack 9 ntc faults
    ONE_SEC, // Stack 10 ntc faults
    ONE_SEC, // Stack 11 ntc faults
    HALF_SEC, // Current positive
    HALF_SEC, // Current negative
    0, // Isa watchdog
};

uint16_t error_limits[ERROR_CHECKS] = {
    BAT_LIMIT_MAX_CELL_V,
    BAT_LIMIT_MIN_CELL_V,
    BAT_LIMIT_MAX_T_DISCH,
    BAT_LIMIT_MIN_T_DISCH,
    // MAX TEMP CHARGING
    // MIN TEMP CHARGING
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    BAT_LIMIT_MAX_I_DISCH,
    BAT_LIMIT_MAX_I_CHRG,
    ISA_WTD_TIMEOUT,

    // 10x OWC for each stack, with function diferent
    // 10x OWC for ntc's within a stack
    // Watchdog timer for current measurement
};

#endif

#ifdef B6V1

uint16_t error_period_table[ERROR_CHECKS] = {
    HALF_SEC, // overvoltage
    HALF_SEC, // undervoltage
    ONE_SEC, // overtemperature
    ONE_SEC, // undertemperature
    ONE_SEC, // Stack 0 ntc faults
    ONE_SEC, // Stack 2 ntc faults
    ONE_SEC, // Stack 3 ntc faults
    ONE_SEC, // Stack 4 ntc faults
    ONE_SEC, // Stack 5 ntc faults
    ONE_SEC, // Stack 6 ntc faults
    ONE_SEC, // Stack 7 ntc faults
    ONE_SEC, // Stack 8 ntc faults
    ONE_SEC, // Stack 9 ntc faults
    ONE_SEC, // Stack 9 ntc faults
    HALF_SEC, // Current positive
    HALF_SEC, // Current negative
    0, // Isa watchdog
};

uint16_t error_limits[ERROR_CHECKS] = {
    BAT_LIMIT_MAX_CELL_V,
    BAT_LIMIT_MIN_CELL_V,
    BAT_LIMIT_MAX_T_DISCH,
    BAT_LIMIT_MIN_T_DISCH,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    STACK_LIMIT_MIN_CELL_NTC,
    BAT_LIMIT_MAX_I_DISCH,
    BAT_LIMIT_MAX_I_CHRG,
    ISA_WTD_TIMEOUT
    // 10x OWC for each stack, with function diferent
    // 10x OWC for ntc's within a stack
    // Watchdog timer for current measurement
};

#endif

uint16_t error_counter[ERROR_CHECKS] = { 0 };
uint16_t monitor_routine_flag = 0;

bool below_i32(int32_t value, int32_t limit)
{
    return value < limit;
}

bool above_i32(int32_t value, int32_t limit)
{
    return value > limit;
}

bool below_u32(int32_t value, int32_t limit)
{

    unsigned value_uns = *((uint32_t*)&value);
    unsigned limit_uns = *((uint32_t*)&limit);

    return value_uns < limit_uns;
}

bool above_u32(int32_t value, int32_t limit)
{

    unsigned value_uns = *((uint32_t*)&value);
    unsigned limit_uns = *((uint32_t*)&limit);

    return value_uns > limit_uns;
}

bool below_i16(int32_t value, int32_t limit)
{

    int16_t value_uns = *((int16_t*)&value);
    int16_t limit_uns = *((int16_t*)&limit);

    return value_uns < limit_uns;
}

bool above_i16(int32_t value, int32_t limit)
{

    int16_t value_uns = *((int16_t*)&value);
    int16_t limit_uns = *((int16_t*)&limit);

    return value_uns > limit_uns;
}

bool below_u16(int32_t value, int32_t limit)
{

    unsigned value_uns = *((uint16_t*)&value);
    unsigned limit_uns = *((uint16_t*)&limit);

    return value_uns < limit_uns;
}

bool above_u16(int32_t value, int32_t limit)
{

    unsigned value_uns = *((uint16_t*)&value);
    unsigned limit_uns = *((uint16_t*)&limit);

    return value_uns > limit_uns;
}

bool different(int16_t value, int16_t limit)
{

    return value != limit;
}

#ifdef B4V2

bool (*cmp_functions[ERROR_CHECKS])(int32_t value, int32_t limit) = {
    above_u16,
    below_u16,
    above_u16,
    below_i16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,

    above_i16,
    below_i16,
};

const uint16_t error_reason[ERROR_CHECKS] = {
    TS_OFF_REASON_OVERVOLTAGE,
    TS_OFF_REASON_UNDERVOLTAGE,
    TS_OFF_REASON_OVERTEMPERATURE,

    TS_OFF_REASON_UNDERTEMPERATURE,

    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,

    TS_OFF_REASON_OVERCURRENT_OUT,
    TS_OFF_REASON_OVERCURRENT_IN,
};

#endif

#ifdef B5V2

bool (*cmp_functions[ERROR_CHECKS])(int32_t value, int32_t limit) = {
    above_u16,
    below_u16,
    above_u16,
    below_i16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,

    above_i16,
    below_i16,
    above_u32,
};

const uint16_t error_reason[ERROR_CHECKS] = {
    TS_OFF_REASON_OVERVOLTAGE,
    TS_OFF_REASON_UNDERVOLTAGE,
    TS_OFF_REASON_OVERTEMPERATURE,

    TS_OFF_REASON_UNDERTEMPERATURE,

    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,

    TS_OFF_REASON_OVERCURRENT_OUT,
    TS_OFF_REASON_OVERCURRENT_IN,
    TS_OFF_REASON_NO_ISA,
};

#endif

#ifdef B6V1

bool (*cmp_functions[ERROR_CHECKS])(int32_t value, int32_t limit) = {
    above_u16,
    below_u16,
    above_u16,
    below_i16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    below_u16,
    above_i16,
    below_i16,
    above_u32,
};

const uint16_t error_reason[ERROR_CHECKS] = {
    TS_OFF_REASON_OVERVOLTAGE,
    TS_OFF_REASON_UNDERVOLTAGE,
    TS_OFF_REASON_OVERTEMPERATURE,
    TS_OFF_REASON_UNDERTEMPERATURE,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_OVERCURRENT_OUT,
    TS_OFF_REASON_OVERCURRENT_IN,
    TS_OFF_REASON_NO_ISA,
};

#endif

void calculate_summary_values(Battery* battery)
{

    /* Voltage Values */
    uint16_t lowest_V = UINT16_MAX;
    uint16_t lowest_V_cell = 0;
    uint16_t highest_V = 0;
    uint16_t highest_V_cell = 0;
    uint32_t total_V = 0;

    uint16_t bat_lowest_V = UINT16_MAX;
    uint16_t bat_lowest_V_cell[2] = { 0 };
    uint16_t bat_highest_V = 0;
    uint16_t bat_highest_V_cell[2] = { 0 };
    uint32_t bat_total_V = 0;

    /* Temperature Values */
    int16_t lowest_T = INT16_MAX;
    int16_t lowest_T_cell = 0;
    int16_t highest_T = INT16_MIN;
    int16_t highest_T_cell = 0;
    int32_t total_T = 0;
    uint16_t stack_valid_NTC = 0;

    int16_t bat_lowest_T = INT16_MAX;
    int16_t bat_lowest_T_cell[2] = { 0 };
    int16_t bat_highest_T = INT16_MIN;
    int16_t bat_highest_T_cell[2] = { 0 };
    int32_t bat_total_T = 0;
    uint16_t bat_valid_NTC = 0;

    uint16_t stack_i, cell_i; // iterators

    Stack* stack;
    Cell* cell;

    for (stack_i = 0; stack_i < BATTERY_SERIES_STACKS; ++stack_i) {

        lowest_V = UINT16_MAX;
        total_V = 0;
        highest_V = 0;

        lowest_T = INT16_MAX;
        total_T = 0;
        highest_T = INT16_MIN;

        stack = &battery->stacks[stack_i];

        /* calculate stack values */
        for (cell_i = 0; cell_i < BATTERY_SERIES_CELLS_PER_STACK; ++cell_i) {
            cell = &stack->cell_pack[cell_i];

            /* voltage */
            total_V += cell->voltage;
            if (cell->voltage < lowest_V) {
                lowest_V = cell->voltage;
                lowest_V_cell = cell_i;
            }
            if (cell->voltage > highest_V) {
                highest_V = cell->voltage;
                highest_V_cell = cell_i;
            }
            /* temperature */
            if (NTC_is_valid(stack->slave.temp_fault_mask, cell_i)) { // only use valid NTC
                total_T += cell->temperature;
                if (cell->temperature < lowest_T) {
                    lowest_T = cell->temperature;
                    lowest_T_cell = cell_i;
                }
                if (cell->temperature > highest_T) {
                    highest_T = cell->temperature;
                    highest_T_cell = cell_i;
                }
            }
        }

        /* Update stack summary voltage values */
        stack->total_voltage = total_V / 10; /* PODEMOS VIR A TIRAR */
        stack->mean_voltage = total_V / BATTERY_SERIES_CELLS_PER_STACK; /* PODEMOS VIR A TIRAR */
        stack->max_voltage = highest_V;
        stack->max_voltage_cell = highest_V_cell; /* Não vai para CAN */
        stack->min_voltage = lowest_V;
        stack->min_voltage_cell = lowest_V_cell; /* Não vai para CAN */

        /* Update stack summary temperature values */

        stack_valid_NTC = count_valid_cell_NTC_in_stack(stack->slave.temp_fault_mask); /* Compute number of valid ntc's */

        stack->n_valid_cell_NTC = stack_valid_NTC;

        if (stack_valid_NTC != 0) {

            stack->mean_temperature = total_T / stack_valid_NTC; /* Só para debug */

        } else {
            stack->mean_temperature = 0;
        }

        stack->max_temperature = highest_T; /* Meter a ir para CAN maybe */
        stack->max_temperature_cell = highest_T_cell;
        stack->min_temperature = lowest_T; /* Meter a ir para CAN maybe */
        stack->min_temperature_cell = lowest_T_cell;

        /* calculate battery values */
        /* voltage */
        bat_total_V += total_V;
        if (lowest_V < bat_lowest_V) {
            bat_lowest_V = lowest_V;
            bat_lowest_V_cell[0] = stack_i;
            bat_lowest_V_cell[1] = lowest_V_cell;
        }
        if (highest_V > bat_highest_V) {
            bat_highest_V = highest_V;
            bat_highest_V_cell[0] = stack_i;
            bat_highest_V_cell[1] = highest_V_cell;
        }
        /* temperature */
        bat_total_T += total_T;
        bat_valid_NTC += stack_valid_NTC;
        if (lowest_T < bat_lowest_T) {
            bat_lowest_T = lowest_T;
            bat_lowest_T_cell[0] = stack_i;
            bat_lowest_T_cell[1] = lowest_T_cell;
        }
        if (highest_T > bat_highest_T) {
            bat_highest_T = highest_T;
            bat_highest_T_cell[0] = stack_i;
            bat_highest_T_cell[1] = highest_T_cell;
        }
    }

    /* Update battery summary values */

    /* Voltage */

    /*  Mean/total */
    battery->total_voltage = bat_total_V / 100;
    battery->mean_voltage = bat_total_V / BATTERY_TOTAL_SERIES_CELLS;

    /* Minimum */
    battery->min_voltage = bat_lowest_V;
    battery->min_voltage_cell[0] = bat_lowest_V_cell[0];
    battery->min_voltage_cell[1] = bat_lowest_V_cell[1];
    /* Maximum */
    battery->max_voltage = bat_highest_V;
    battery->max_voltage_cell[0] = bat_highest_V_cell[0];
    battery->max_voltage_cell[1] = bat_highest_V_cell[1];

    /* Temperature */

    /* Mean */
    if (bat_valid_NTC != 0) {
        battery->mean_temperature = bat_total_T / bat_valid_NTC;
    } else {
        battery->mean_temperature = 0;
    }

    /* Minimum */
    battery->min_temperature = bat_lowest_T;
    battery->min_temperature_cell[0] = bat_lowest_T_cell[0];
    battery->min_temperature_cell[1] = bat_lowest_T_cell[1];
    /* Maximum */
    battery->max_temperature = bat_highest_T;
    battery->max_temperature_cell[0] = bat_highest_T_cell[0];
    battery->max_temperature_cell[1] = bat_highest_T_cell[1];

    return;
}

int error_state_machine(Battery* battery, uint16_t* state,
    bool (*cmp)(int32_t value, int32_t limit),
    int32_t value, int16_t limit, bool counter, uint16_t reason)
{
    switch (*state) {
    case ERROR_START:
        if (cmp(value, limit)) {
            *state = ERROR_WAITING;
        }
        return 0;
    case ERROR_WAITING:
        if (!cmp(value, limit)) {
            *state = ERROR_START;
        } else if (counter) {
            *state = ERROR_CONFIRM;
        }
        return 1;
    case ERROR_CONFIRM:
        set_ams_ok(battery, 0, reason);
        *state = ERROR_DONE;
        return 1;
    case ERROR_DONE:
        if (!cmp(value, limit)) {
            *state = ERROR_START;
        }
        return 1;
    }

    return 1;
}

void check_battery_limits(Battery* battery)
{

    int32_t* error_values[ERROR_CHECKS] = {
        (int32_t*)&(battery->max_voltage),
        (int32_t*)&(battery->min_voltage),
        (int32_t*)&(battery->max_temperature),
        (int32_t*)&(battery->min_temperature),
        (int32_t*)&(battery->stacks[0].n_valid_cell_NTC),
        (int32_t*)&(battery->stacks[1].n_valid_cell_NTC),
        (int32_t*)&(battery->stacks[2].n_valid_cell_NTC),
        (int32_t*)&(battery->stacks[3].n_valid_cell_NTC),
        (int32_t*)&(battery->stacks[4].n_valid_cell_NTC),
        (int32_t*)&(battery->stacks[5].n_valid_cell_NTC),
        (int32_t*)&(battery->stacks[6].n_valid_cell_NTC),
        (int32_t*)&(battery->stacks[7].n_valid_cell_NTC),
        (int32_t*)&(battery->stacks[8].n_valid_cell_NTC),
        (int32_t*)&(battery->stacks[9].n_valid_cell_NTC),

#if defined(B4V2) || defined(B5V2)
        (int32_t*)&(battery->stacks[10].n_valid_cell_NTC),
        (int32_t*)&(battery->stacks[11].n_valid_cell_NTC),
#endif

        (int32_t*)&(battery->energy_meter.current),
        (int32_t*)&(battery->energy_meter.current),

#if defined(B6V1) || defined(B5V2)
        (int32_t*)&(battery->energy_meter.isa_wtd),
#endif

    };

    // error_limits[0] = battery->settable.bms_cell_limit_max_v;
    // error_limits[1] = battery->settable.bms_cell_limit_min_v;
    // error_limits[2] = battery->settable.bms_cell_limit_max_t;
    // error_limits[3] = battery->settable.bms_cell_limit_min_t;

    int i = 0;
    int check = 0;
    // int aux = 0;

    for (i = 0; i < ERROR_CHECKS; i++) { // -2 ignores current checks
        // aux = check;
        check += error_state_machine(battery, error_states + i, cmp_functions[i], *(error_values[i]), error_limits[i], error_counter_end[i], error_reason[i]);
        /*if(aux != check){
            printf("value: %d, limit: %d \n",*(error_values[i]),error_limits[i]);
        }*/
    }
    if (!check) {

        set_ams_ok(battery, 1, TS_OFF_REASON_NO_ERROR);

    } else if (check != 0) { // If there is an error reset while there is an ams error, thi forces ams_ok to 0 again

        battery->ams_ok = 0; // force AMS to 0
    }

    return;
}

#if defined(B4V2) || defined(B5V2)

void battery_monitor_routine(Battery* battery)
{

    /* Measure all cell voltages */
    start_cell_voltages_conversion(battery);
    __delay_us(T_CONV_CELL_ALL_7K);
    get_cell_voltages(battery);

    /* Measure all cell temperatures */
    unsigned int address = 0;
    for (address = 0; address < 16; ++address) {
        start_temp_voltage_conversion(battery, address);
        __delay_us(T_CONV_GPIO_ONE_7K);
        get_temperatures(battery, address);
    }

    /* Calculate battery summary values from the newly collected values*/
    calculate_summary_values(battery);

    /* Check if the values are within the limits, set AMS_OK to 0 if needed */
    // check_battery_limits(battery);

    set_discharge_channels(battery);

    return;
}

#endif

#ifdef B6V1

void battery_monitor_routine(Battery* battery)
{

    if (monitor_routine_flag == VOLT_CONV_STATE) {
        start_cell_voltages_conversion(battery);
        scheduler_change_period("monitor_routine", T_CONV_GPIO_ALL_PLUS_REFERENCE_7K);
        monitor_routine_flag = TEMP_CONV_STATE;
    } else if (monitor_routine_flag == TEMP_CONV_STATE) {
        get_cell_voltages(battery);
        start_temp_voltage_conversion(battery);
        scheduler_change_period("monitor_routine", T_CONV_GPIO_ALL_7K);
        monitor_routine_flag = GET_TEMP_STATE;
        // flag fo soc calculation
        battery->energy_meter.volt_updated = 1;
    } else if (monitor_routine_flag == GET_TEMP_STATE) {
        get_temperatures(battery);
        monitor_routine_flag = OWC_CONV_STATE1;
    }

    if (monitor_routine_flag == OWC_CONV_STATE1 || monitor_routine_flag == OWC_CONV_STATE2) {
        if (battery->OWC_sequence == 0)
            start_cell_open_wire_check_conversion(battery, PUP_PULL_UP); // firstly convert with pull up
        if (battery->OWC_sequence == 1)
            start_cell_open_wire_check_conversion(battery, PUP_PULL_DOWN); // firstly convert with pull down
        monitor_routine_flag++;
        scheduler_change_period("monitor_routine", T_CONV_GPIO_ALL_PLUS_REFERENCE_7K);
    } else if (monitor_routine_flag == 5) {
        if (battery->OWC_sequence == 0) {
            get_cell_open_wire_check_reading(battery);
            battery->OWC_sequence = 1;
        } else if (battery->OWC_sequence == 1) {
            get_cell_open_wire_check_reading(battery);
            open_wire_check_calculation(battery);
            // CHECKS FOR AMS, clear OWC_sequence
            battery->OWC_sequence = 0; // NEED TO REMOVE
        }
        /* Calculate battery summary values from the newly collected values*/
        calculate_summary_values(battery);
        /* Check if the values are within the limits, set AMS_OK to 0 if needed */
        // removed: went to separate scheduler task -> check_battery_limits(battery);
        set_discharge_channels(battery);
        scheduler_change_period("monitor_routine", MONITOR_ROUTINE_FREQ);
        monitor_routine_flag = VOLT_CONV_STATE;
    }

    return;
}

#endif

void enable_daisy_chain_B_comm(void)
{
    set_CS_B(0);
    return;
}

void disable_daisy_chain_B_comm(void)
{
    set_CS_B(1);
    return;
}

void daisy_chain_B_watchdog_init(void)
{
    config_timer2_us(MIN_T_IDLE, 4); // timer configured to 4.3ms with priority 4
    return;
}
void daisy_chain_B_watchdog_enable(void)
{
    enable_timer2();
    return;
}
void daisy_chain_B_watchdog_reset(void)
{
    reset_timer2();
    return;
}
void daisy_chain_B_watchdog_disable(void)
{
    disable_timer2();
    return;
}

void daisy_chain_B_init(LTC_daisy_chain* daisy_chain_B)
{
    daisy_chain_B->n = BATTERY_SERIES_STACKS;
    daisy_chain_B->core_state = CORE_SLEEP_STATE;
    daisy_chain_B->interface_state = ISOSPI_IDLE_STATE;
    daisy_chain_B->enable_comm = &enable_daisy_chain_B_comm;
    daisy_chain_B->disable_comm = &disable_daisy_chain_B_comm;
    daisy_chain_B->reset_interface_watchdog = &daisy_chain_B_watchdog_reset;
    return;
}
