/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#define CMD_ID_COMMON_TS 18

#include "bms_version.h"
#include "battery.h"
#include "lib_pic33e/can.h"
#include "can-ids-v2/can_ids.h"
#include "shared/fifo/fifo.h"
#include "ts_turn_on.h"
#include <stdbool.h>


can_t* get_can();
CANdata compose_master_ts_message(bool state, MASTER_MSG_TS_OFF_Reason off_reason);
void update_master_energy_meter_message(const Battery* battery);
void update_cell_voltage_info_message(const Battery* battery);
void update_cell_temperature_info_message(const Battery* battery);
void update_master_imd_error_message();
CANdata compose_master_ams_error_message();
void update_master_sc_open_message();
void update_cell_info_message(const Cell* cell);
void update_slave_info_1_message(const Stack* stack);
void update_slave_info_2_message(const Stack* stack);
CANdata compose_slave_info_3_message(const Stack* stack);
void send_verbose_messages(const Battery* battery);
void battery_info_messages(const Battery* battery);
void update_status_message(const Battery* battery);
void send_ts_state(const Battery* battery);
void battery_charging_info(const Battery* battery);
void update_charging_message();
void can_dispatch(Fifo* fifo1);
void can_receive_handler(Battery* battery);
void save_energy_meter_data(Battery *battery, can_t *can, uint16_t msg_id);

#endif
