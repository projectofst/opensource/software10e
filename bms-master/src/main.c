/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = XT // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS512 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128 // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR128 // Power-on Reset Timer Value Select bits (128ms)
#pragma config BOREN = ON // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>

#include "bms_version.h"

#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"

#include "lib_pic33e/SPI.h"
#include "lib_pic33e/adc.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/external.h"
#include "lib_pic33e/flash.h"
#include "lib_pic33e/pps.h"
#include "lib_pic33e/pwm.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/timing.h"

#include "shared/fifo/fifo.h"
#include "shared/scheduler/scheduler.h"

#include "LTC/LTC68xx.h"

#include "energy_meter/SoC.h"
#include "energy_meter/energy_meter.h"

#include "bat_cfg.h"
#include "battery.h"
#include "battery_monitor.h"
#include "communication.h"
#include "cooling.h"
#include "signals.h"
#include "slave/slave.h"
#include "ts_turn_on.h"

#define FIFO_SIZE 1000
#define TIME1 100

/** Flags and timer counters */
uint32_t time_counter = 0; // dont know time step?
uint32_t seconds = 0; // s
uint32_t milliseconds = 0; // ms
bool status_flag = 0; // for what

Battery battery;

Fifo _can_fifo;
Fifo* can_fifo = &_can_fifo;

void timer1_callback()
{
    time_counter++;
    if (time_counter == 1000 / TIME1) {
        status_flag = true;
        seconds++;
        time_counter = 0;
    }

    int i;
    for (i = 0; i < ERROR_CHECKS; i++) {
        if (error_states[i] == ERROR_WAITING) {
            error_counter[i] += TIME1;
            if (error_counter[i] >= error_period_table[i]) {
                error_counter_end[i] = 1;
                error_counter[i] = 0;
            }
        } else {
            error_counter[i] = 0;
            error_counter_end[i] = 0;
        }
    }
}
void timer3_callback()
{
    milliseconds++;
    battery.energy_meter.isa_wtd++;

    return;
}

typedef struct _external_signals {
    union {
        struct {
            bool ams_ok : 1;
            bool imd_ok : 1;
            bool imd_latch : 1;
            bool ams_latch : 1;
            bool air_positive : 1;
            bool air_negative : 1;
            bool precharge : 1;
            bool discharge : 1;
            bool sc_dcu_imd : 1;
            bool sc_imd_ams : 1;
            bool sc_ams_dcu : 1;
            bool sc_tsms_relays : 1;
            bool shutdown_above_minimum : 1;
            bool shutdown_open : 1;
        };
        uint16_t status;
    };
} ExternalSignals;

ExternalSignals external_sigs;

#ifdef FAKE
void poll_signals()
{
    battery.imd_ok = 1;
    battery.imd_latch = 1;
    battery.ams_latch = 1;
    battery.air_positive = get_Det_AIR_Plus() ^ 1;
    battery.air_negative = get_Det_AIR_Plus() ^ 1;
    battery.precharge = get_Det_PreChK() ^ 1;
    battery.discharge = get_Det_DisChK() ^ 1;
    battery.sc_dcu_imd = 1;
    battery.sc_imd_ams = 1;
    battery.sc_ams_dcu = 1;
    battery.sc_tsms_relays = 1;
    battery.shutdown_above_minimum = 1;
    battery.shutdown_open = 0;

    return;
}
#else
void poll_signals()
{
    battery.imd_ok = Det_IMD_OK;
    battery.imd_latch = Det_IMD_latch;
    battery.ams_latch = Det_AMS_latch;
    battery.air_positive = get_Det_AIR_Plus() ^ 1;
    battery.air_negative = get_Det_AIR_Minus() ^ 1;
    battery.precharge = get_Det_PreChK() ^ 1;
    battery.discharge = get_Det_DisChK() ^ 1;
    battery.sc_dcu_imd = Det_Shutdown_DCU_IMD;
    battery.sc_imd_ams = Det_Shutdown_IMD_AMS;
    battery.sc_ams_dcu = Det_Shutdown_AMS_DCU;
    battery.sc_tsms_relays = Det_Shutdown_TSMS_Relays;
    battery.shutdown_above_minimum = Shutdown_Above_Min;
    battery.shutdown_open = Shutdown_Opening_Sig;

    return;
}
#endif

TS_TURN_ON ts_turn_on_state = TS_WAIT_FOR_COMMAND;

#ifdef FAKE
bool check_ts_on()
{
    return ts_turn_on_state == TS_END_STATE;
}
#else
bool check_ts_on()
{
    return !HV_Present_Sig || !get_Det_AIR_Plus() || !get_Det_AIR_Minus() || !get_Det_PreChK();
}
#endif

void ts_turn_off_sequence()
{
    // send_can1(logW(LOG_TS_STATE, 0, battery.ams_off_reason, 0));
    set_AIR_Plus_Control(0);
    __delay_ms(RELAY_RELEASE_TIME);
    set_PreChK_Control(0);
    __delay_ms(RELAY_RELEASE_TIME);
    set_DisChK_Control(0);
    __delay_ms(RELAY_OPERATION_TIME + RELAY_BOUNCE_TIME);
    set_AIR_Minus_Control(0);

    return;
}

bool need_to_turn_off()
{
    return battery.sc_tsms_relays == 0 || battery.ams_ok == 0; // || battery.imd_ok == 0
}

int32_t last_counter = 0;

void ts_turn_on_sequence()
{
    // printf("ts_turn_on_state %d\n", ts_turn_on_state);
    switch (ts_turn_on_state) {
    case TS_WAIT_FOR_COMMAND:
        if (need_to_turn_off()) {
            battery.master_state.ts_turn_on = 0;
            ts_turn_on_state = TS_ERROR_STATE;
        } else if (battery.master_state.ts_turn_on) {
            ts_turn_on_state = TS_INITIAL_STATE;
        }
        break;
    case TS_INITIAL_STATE:
        if (need_to_turn_off()) {
            battery.master_state.ts_turn_on = 0;
            ts_turn_on_state = TS_ERROR_STATE;
        } else if (battery.master_state.ts_turn_on && battery.ams_ok) {
            set_DisChK_Control(1);
            set_AIR_Minus_Control(1);
            __delay_ms(RELAY_OPERATION_TIME + RELAY_BOUNCE_TIME);
            set_PreChK_Control(1);
            __delay_ms(RELAY_OPERATION_TIME + RELAY_BOUNCE_TIME);

            last_counter = milliseconds;
            ts_turn_on_state = TS_WAIT_FOR_PRECHARGE;
        }
        break;
    case TS_WAIT_FOR_PRECHARGE: {
        if (need_to_turn_off()) {
            ts_turn_on_state = TS_ERROR_STATE;
            return;
        }

        if (ISA_USED) {
            // printf("ISA: %d, BAT: %d\n", battery.energy_meter.output_voltage, battery.energy_meter.battery_voltage);
            if ((battery.energy_meter.output_voltage > (battery.energy_meter.battery_voltage * 0.95)) && ((milliseconds - last_counter) > (RC_TIME_CONSTANT * 5.0) * 1000)) { // multiply by 1000 to convert to ms
                set_AIR_Plus_Control(1);
                __delay_ms(RELAY_OPERATION_TIME + RELAY_BOUNCE_TIME);
                set_PreChK_Control(0);
                ts_turn_on_state = TS_END_STATE;
                // send_can1(logW(LOG_TS_STATE, 1, battery.ams_off_reason, 0));
            }
        }

        else {

            if (get_PreCh_End_Sig() == 1 && ((milliseconds - last_counter) > 2500)) {

                set_AIR_Plus_Control(1);
                __delay_ms(RELAY_OPERATION_TIME + RELAY_BOUNCE_TIME);
                set_PreChK_Control(0);
                ts_turn_on_state = TS_END_STATE;
                // send_can1(logW(LOG_TS_STATE, 1, battery.ams_off_reason, 0));
            }
        }

        break;
    }
    case TS_END_STATE:
        if (need_to_turn_off() || battery.master_state.ts_turn_on == 0) {
            battery.master_state.ts_turn_on = 0;
            ts_turn_on_state = TS_ERROR_STATE;
            return;
        }
        break;
    case TS_ERROR_STATE:
        ts_turn_on_state = TS_WAIT_FOR_COMMAND;
        ts_turn_off_sequence();
        break;
    }
}

void config_adc1_bms()
{
    ANSELEbits.ANSE5 = 1;

    AD1CON1bits.AD12B = 1;
    AD1CON1bits.SSRCG = 0;
    AD1CON1bits.SSRC = AUTO_CONV;

    AD1CON1bits.ADSIDL = 0;
    AD1CON1bits.ASAM = AUTO;
    AD1CON1bits.FORM = 0;

    AD1CON2 = 0;
    AD1CON2bits.SMPI = 1;
    AD1CON2bits.ALTS = 0;
    AD1CON2bits.VCFG = 0;
    AD1CON2bits.CSCNA = 0;
    AD1CON2bits.BUFM = 0; // Always starts filling the buffer from the Start address

    AD1CON3bits.ADRC = 0;
    // Auto-Sample Time (31 is MAX)
    // ADC Conversion Clock 256.TAD (255 is MAX)
    AD1CON3bits.SAMC = 31;
    AD1CON3bits.ADCS = 255;

    AD1CON4 = 0;
    AD1CON4bits.ADDMAEN = 0;

    AD1CHS0bits.CH0SA = 29;
    AD1CHS0bits.CH0NA = 0;

    // ANSELEbits.ANSE5 = 1;
    // ANSELEbits.ANSE6 = 1;

    // AD1CSSHbits.CSS30 = 1;
    // AD1CSSHbits.CSS29 = 1;
    // AD1CSSHbits.CSS28 = 1;

    AD1CON1bits.ADON = 1;

    // IFS0bits.AD1IF = 0;
    // IEC0bits.AD1IE = 1;
    // IPC3bits.AD1IP = 5;

    __delay_ms(1);
}

int16_t convert_htfs_200p(uint16_t v)
{
    double v_out = v / 4096.0 * 5;

    return 1600 * (v_out - 2.5) - 35; // 35 is the offset
}

void bms_modules_cfg()
{

    /*
     * Configuration of the modules and the pin assignment
     */

    /* Configuration of the SPI module for slave communication */
    SPI_Peripheral_Configuration spi_config = {
        .enabled = 1,
        .stop_in_idle = 0,
        .disable_SCK_pin = 0,
        .disable_SDO_pin = 0,
        .transfer_mode = SPI_TRANSFER_MODE_8BIT,
        .data_sample_phase = SPI_DATA_SAMPLE_PHASE_MIDDLE,
        .SPI_mode = SPI_MODE_3,
        .enable_SS_pin = 0,
        .enable_master_mode = 1,
        .baudrate = 1000000
    };

    config_SPI2(spi_config);

    // TODO: config SPI AS
    //  Oi

    do_the_pps();
    CANconfig can_config;
    default_can_config(&can_config);
    // config_master_filters(&can_config);
    can_config.number_masks = 1;
    can_config.filter_masks[0] = 0b00000011111;
    can_config.number_filters = 2;
    can_config.filter_sids[0] = 31;
    can_config.filter_sids[1] = 10;
    can_config.filter_mask_source[0] = 0;
    can_config.filter_mask_source[1] = 0;
    config_can1(&can_config);

    /* Shutdown open external interrupt */
    // config_external0(RISING, 4);

    config_timer1(TIME1, 6);
    config_timer3(1, 4);

    if (ISA_USED == false) {
        config_adc1_bms(); // ADC used for current measurement
    }
}

// Use only when the battery has no ISA
void update_EM_data(Battery* battery)
{

    uint16_t bat_voltage = battery->total_voltage;
    int32_t bat_current = convert_htfs_200p(ADC1BUF0) / 10; // Ver unidades
    int32_t bat_power = (bat_current / 100.0) * (bat_voltage / 100.0) / 10.0;

    battery->energy_meter.current = bat_current;
    battery->energy_meter.battery_voltage = bat_voltage;
    battery->energy_meter.power = bat_power;
}

int main(void)
{
    printf("ola\n");
    config_outputs();

    bms_modules_cfg();

    // Reset message
    send_can1(logI(LOG_RESET_MESSAGE, RCON, 0, 0));

    battery_init(&battery);
    // set_ams_ok(&battery, 1, TS_OFF_REASON_NO_ERROR);

    tasks_t BMS_tasks[] = {
        { .name = "monitor_routine", .period = MONITOR_ROUTINE_FREQ, .func = battery_monitor_routine, .args_on = 1, .args = (void*)&battery },
        { .name = "check_battery_limits", .period = 1, .func = check_battery_limits, .args_on = 1, .args = (void*)&battery },
        { .name = "update_battery_SoC", .period = 100, .func = update_battery_SoC, .args_on = 1, .args = (void*)&battery },
        { .name = "poll_signals", .period = 10, .func = poll_signals, .args_on = 0 },
        { .name = "ts_turn_on", .period = 1, .func = ts_turn_on_sequence, .args_on = 0 },
        { .name = "control_disch", .period = 100, .func = control_discharge_channels, .args_on = 1, .args = (void*)&battery },
        { .name = "battery_info_messages", .period = 1000, .func = battery_info_messages, .args_on = 1, .args = (void*)&battery },
        { .name = "battery_charging", .period = 1000, .func = battery_charging_info, .args_on = 1, .args = (void*)&battery },
        { .name = "verbose_messages", .period = 1000, .func = send_verbose_messages, .args_on = 1, .args = (void*)&battery },
        //{ .name = "ts_state", .period = 1000, .func = send_ts_state, .args_on = 1, .args = (void*)&battery },
        { .name = "can_dispatch", .period = 0, .func = can_dispatch, .args_on = 1, .args = (void*)can_fifo },
        { .name = "can_receive", .period = 0, .func = can_receive_handler, .args_on = 1, .args = (void*)&battery },
        { .name = "fan_actuation", .period = 100, .func = fan_actuation, .args_on = 0 }
    };

    PPSUnLock;
    PPSOutput(OUT_FN_PPS_OC1, OUT_PIN_PPS_RP84);
    PPSLock;

    config_timer2(1, 5);
    pwm1_config(0, PWM_FREQUENCY, CLK_TIMER2);

    can_t* can = get_can();

    scheduler_init_args(BMS_tasks, sizeof(BMS_tasks) / sizeof(tasks_t));

    CANdata msg[FIFO_SIZE];

    fifo_init(can_fifo, msg, FIFO_SIZE);

    bms_modules_cfg();

    fan_setup();

    // Reset message
    send_can1(logI(LOG_RESET_MESSAGE, RCON, 0, 0));

    /*
     * Initial code run
     */
    start_communication_with_slaves(&battery);

    /*
     * Main Loop
     */

    battery.ams_ok = 1; /**< Assume every thing is fine first */

    while (1) {
        master_send_msgs(can->master, milliseconds);
        scheduler(milliseconds);

#ifdef ISA_USED
        update_EM_data(&battery);
#endif

#ifndef FAKE
        AMS_OK = battery.ams_ok;
#endif

        /*
         * When there is a request to turn ON the TS and AMS is ok we start the
         * TS turn ON state machine
         */

        if (battery.master_state.car_ts == 1) {
            // printf("car ts\n");
            battery.master_state.car_ts = 0;
            battery.master_state.ts_turn_on ^= 1;
            battery.ams_off_reason = TS_OFF_REASON_DASH_BUTTON;
            pwm1_update_duty_cycle(1);
        }

#if 0
		if (master_state.car_ts) {
			master_state.car_ts = 0;
			if (battery.ams_ok && !master_state.ts_turn_on) {
				master_state.ts_turn_on = 1;
			}
			else {
				battery.ams_off_reason = TS_OFF_REASON_DASH_BUTTON;
				master_state.ts_turn_on = 0;
			}
		}

#endif

        ClrWdt();
    }

    return 0;
}
