/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*! \file cooling.h
    \brief A Documented file.
    
    Temperature maping and cooling performace algorithms.
    Other cooling and heat generation related functions,
    as well as fan actuation 
*/


#ifndef _COOLING_
#define _COOLING_

#define PWM_FREQUENCY 25000

void perform_cooling_algo(void); 

void compute_cell_losses(void);
void compute_battery_losses(void);

void fan_setup(void);
void fan_actuation(void);

#endif 
