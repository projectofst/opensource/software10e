/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 


/*! \file slave.h
 *  \brief A Documented file.
 *
 *  File to support different accumulators, different slaves use different NTC's or have different
 *  hardware characteristics
 */

#ifndef _SLAVE_
#define _SLAVE_

#include "battery.h"

#ifdef FAKE
#define BROADCAST_US 32
#endif

#ifdef FAKE
//FAKE_VOLTAGE_* must be floats
#define FAKE_VOLTAGE_BEGIN 4060.0
#define FAKE_VOLTAGE_END 4200.0
#define FAKE_VOLTAGE_NTC_MIN 1350.0
#define FAKE_VOLTAGE_NTC_MAX 1500.0
#define VOLTAGE(stack,cell) 10*(FAKE_VOLTAGE_BEGIN + (((FAKE_VOLTAGE_END-FAKE_VOLTAGE_BEGIN)/((BATTERY_SERIES_CELLS_PER_STACK) * (BATTERY_SERIES_STACKS))*((stack)*(BATTERY_SERIES_CELLS_PER_STACK) + cell))))
#define VOLTAGE_NTC(stack,cell) 10*(FAKE_VOLTAGE_NTC_MAX - (((FAKE_VOLTAGE_NTC_MAX-FAKE_VOLTAGE_NTC_MIN)/((BATTERY_SERIES_CELLS_PER_STACK) * (BATTERY_SERIES_STACKS))*((stack)*(BATTERY_SERIES_CELLS_PER_STACK) + cell))))
#endif

#if defined(B4V2) || defined(B5V2)

void control_temp_mux_address(Battery* battery, uint16_t address);
void control_discharge_channels(Battery* battery);
void start_cell_voltages_conversion(Battery* battery);
void get_cell_voltages(Battery* battery);
void start_temp_voltage_conversion(Battery* battery, uint16_t address);
int16_t* get_temp_address(Battery* battery, uint16_t slave, uint16_t index);
void write_to_temp_fault_mask(uint16_t* temp_fault_mask, bool value, uint16_t index);
void get_temperatures(Battery* battery, uint16_t address);
void set_stack_discharge_channels(Stack *stack, uint16_t min_voltage, bool charging);
void set_discharge_channels(Battery *battery);
void start_communication_with_slaves(Battery* battery);

#endif

/**************************************************************************************************/

#ifdef B6V1

void control_discharge_channels(Battery* battery);
void start_cell_open_wire_check_conversion(Battery* battery, uint16_t PUP);
void start_cell_voltages_conversion(Battery* battery);
void write_to_temp_fault_mask(uint16_t* temp_fault_mask, bool value, uint16_t index);
void get_cell_open_wire_check_reading(Battery* battery);
void get_cell_voltages(Battery* battery);
void get_temperatures(Battery* battery);
void open_wire_check_calculation(Battery* battery);
void start_temp_voltage_conversion(Battery* battery);
void set_stack_discharge_channels(Stack* stack, uint16_t min_voltage, bool charging);
void set_discharge_channels(Battery* battery);
void start_communication_with_slaves(Battery* battery);


#endif

#endif
