/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "slave.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/timing.h"
#include "lib_pic33e/utils.h"

#include "LTC/LTC68xx.h"

#include "energy_meter/energy_meter.h"

#include "shared/scheduler/scheduler.h"

#include "battery.h"
#include "signals.h"
#include "temperature.h"

#if defined(B4V2) || defined(B5V2)

/*!
 * @brief Maps the mux address to the temp index
 *
 * The array is indexed by the mux address, contents are the index of the
 * temperature (as used in the temp_fault_mask bitfiled)
 */
const unsigned int NTC_mux_mapping[16] = {
    // mux 1
    3, 4, 5, 14, 2, 13, 1, 0,
    // mux 2
    10, 11, 15, 9, 8, 12, 7, 6
};

#ifndef FAKE
/**
 * @brief Change temperature mux address of all slaves to the passed address.
 */
void control_temp_mux_address(Battery* battery, uint16_t address)
{
    if (address >= 16)
        return;
    uint8_t to_send[battery->daisy_chain_B.n * LTC_REG_SIZE];
    unsigned int i = 0;

    for (i = 0; i < battery->daisy_chain_B.n; i++) {
        battery->stacks[i].slave.BM.GPIO &= 0b10000;
        battery->stacks[i].slave.BM.GPIO |= (address & 0b1111);
        memcpy(to_send + (i * LTC_REG_SIZE), battery->stacks[i].slave.BM.CFGR,
            LTC_REG_SIZE);
    }
    broadcast_write(&battery->daisy_chain_B, WRCFGA, LTC_REG_SIZE, to_send);
    return;
}

#endif

#ifdef FAKE

void control_temp_mux_address(Battery* battery, uint16_t address)
{
    return;
}

#endif

#ifndef FAKE
/**
 * @brief Changes discharge channels state to the contained in the battery
 * struct.
 */
void control_discharge_channels(Battery* battery)
{
    uint8_t to_send[BATTERY_SERIES_STACKS * LTC_REG_SIZE];
    unsigned int i = 0;

    for (i = 0; i < BATTERY_SERIES_STACKS; i++) {
        battery->stacks[i].slave.BM.DCC = battery->stacks[i].slave.discharge_channel_state;
        memcpy(to_send + (i * LTC_REG_SIZE), battery->stacks[i].slave.BM.CFGR,
            LTC_REG_SIZE);
    }
    broadcast_write(&battery->daisy_chain_B, WRCFGA, LTC_REG_SIZE, to_send);
    return;
}

#endif

#ifdef FAKE

void control_discharge_channels(Battery* battery)
{
    return;
}

#endif

#ifndef FAKE
/**
 * @brief Commands the LTC6811 to start converting the cells voltages
 */
void start_cell_voltages_conversion(Battery* battery)
{
    broadcast_poll(&battery->daisy_chain_B,
        ADCV(MD_NORMAL, DCP_DISCHARGE_NOT_PERMITTED, CH_ALL_CELLS));
    return;
}

#endif

#ifdef FAKE

void start_cell_voltages_conversion(Battery* battery)
{

    return;
}

#endif

#ifndef FAKE

/**
 * @brief Reads all cell voltages from the slaves and writes it on the battery
 * struct
 */
void get_cell_voltages(Battery* battery)
{
    uint16_t voltages[3 * BATTERY_SERIES_STACKS];
    uint16_t stack, cell;

    /*
     * Collect cells 0-2 (group A) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVA, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            battery->stacks[stack].cell_pack[0 + cell].voltage = voltages[stack * 3 + cell];
        }
    }

    /*
     * Collect cells 3-5 (group B) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVB, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            battery->stacks[stack].cell_pack[3 + cell].voltage = voltages[stack * 3 + cell];
        }
    }

    /*
     * Collect cells 6-8 (group C) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVC, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            battery->stacks[stack].cell_pack[6 + cell].voltage = voltages[stack * 3 + cell];
        }
    }
    /*
     * Collect cells 9-11 (group D) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVB, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            battery->stacks[stack].cell_pack[9 + cell].voltage = voltages[stack * 3 + cell];
        }
    }

    return;
}

#endif

#ifdef FAKE

void get_cell_voltages(Battery* battery)
{
    uint16_t stack, cell;

    usleep(BROADCAST_US * 5);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack)
        for (cell = 0; cell < BATTERY_SERIES_CELLS_PER_STACK; ++cell) {
            battery->stacks[stack].cell_pack[cell].voltage = VOLTAGE(stack, cell);
            printf("stack: %d cell: %d value: %d \n", stack, cell, battery->stacks[stack].cell_pack[cell].voltage);
        }

    return;
}

#endif

#ifndef FAKE
/**
 * @brief Commands each LTC6811 to start converting a temperature (voltage on
 * GPIO5)
 *
 * @param temp The index of the temperature to measure.
 */
void start_temp_voltage_conversion(Battery* battery, uint16_t address)
{
    control_temp_mux_address(battery, address);
    broadcast_poll(&battery->daisy_chain_B, ADAX(MD_NORMAL, CHG_GPIO5));
    return;
}

#endif

#ifdef FAKE

void start_temp_voltage_conversion(Battery* battery, uint16_t address)
{
    return;
}

#endif
/**
 * @brief Returns the correct pointer to write a temperature value.
 *
 * index is the temperature value id (0-11 for the cell temperatures, 12-13 for
 * the slave temperatures, 14-15 for the heatsink temperatures)
 *
 * slave is the slave number
 */
int16_t* get_temp_address(Battery* battery, uint16_t slave, uint16_t index)
{
    if (index < 12) {
        return &battery->stacks[slave].cell_pack[index].temperature;
    } else if (index < 14) {
        return &battery->stacks[slave].slave.slave_temperature[index - 12];
    } else if (index < 16) {
        return &battery->stacks[slave].slave.disch_temp[index - 14];
    }

    return NULL;
}

/**
 * @brief Writes value to the bit indicated by index of the mask pointed to by
 * temp_fault_mask
 */
void write_to_temp_fault_mask(uint16_t* temp_fault_mask, bool value, uint16_t index)
{
    *temp_fault_mask &= (~(1 << index));
    *temp_fault_mask |= (value << index);

    return;
}

#ifndef FAKE
/**
 * @brief Reads the temperature voltages from the slaves, checks faults,
 * converts and stores the temperatures
 */
void get_temperatures(Battery* battery, uint16_t address)
{
    uint16_t register_contents[3 * BATTERY_SERIES_STACKS];
    uint16_t slave; // iterator
    uint16_t temp_voltage;
    int16_t temperature;
    int16_t* temp_address;
    bool NTC_fault;
    uint16_t temp_index = NTC_mux_mapping[address];

    /* read the data from the slaves */
    broadcast_read(&battery->daisy_chain_B, RDAUXB, LTC_REG_SIZE,
        (uint8_t*)register_contents);

    for (slave = 0; slave < BATTERY_SERIES_STACKS; ++slave) {
        /*
         * Get the contents of the G5V field in the LTC AVBR register (AVBR2 and
         * AVBR3 bytes);
         */
        temp_voltage = register_contents[slave * 3 + 1];

        /* check if the measurement is valid */
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[slave].slave.temp_fault_mask,
            NTC_fault, temp_index);

        if (!NTC_fault) {
            /* convert voltage to temperature */
            temperature = convert_ntc_temp(temp_voltage); // ºC/10

            /* store the temperature value */
            temp_address = get_temp_address(battery, slave, temp_index);
            *temp_address = temperature;
        }
    }

    return;
}

#endif

#ifdef FAKE

void get_temperatures(Battery* battery, uint16_t address)
{
    uint16_t register_contents[3 * BATTERY_SERIES_STACKS];
    uint16_t slave; // iterator
    uint16_t temp_voltage;
    int16_t temperature;
    int16_t* temp_address;
    bool NTC_fault;
    uint16_t temp_index = NTC_mux_mapping[address];

    /** Read temperature from GPIO 1, 2, 3 */
    usleep(BROADCAST_US * 4);

    for (slave = 0; slave < BATTERY_SERIES_STACKS; ++slave) {
        /*
         * Get the contents of the G5V field in the LTC AVBR register (AVBR2 and
         * AVBR3 bytes);
         */
        temp_voltage = VOLTAGE_NTC(slave, temp_index);

        /* check if the measurement is valid */
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[slave].slave.temp_fault_mask,
            NTC_fault, temp_index);

        if (!NTC_fault) {
            /* convert voltage to temperature */
            temperature = convert_ntc_temp(temp_voltage); // ºC/10

            /* store the temperature value */
            temp_address = get_temp_address(battery, slave, temp_index);
            *temp_address = temperature;
        }
    }

    return;
}

#endif

void set_stack_discharge_channels(Stack* stack, uint16_t min_voltage, bool charging)
{
    Slave* slave = &stack->slave;
    int i;
    for (i = 0; i < BATTERY_SERIES_CELLS_PER_STACK; i++) {
        if (stack->cell_pack[i].voltage - min_voltage > BAT_CHARGING_DELTA && slave->disch_temp[0] < MAX_BAL_TEMP && slave->disch_temp[1] < MAX_BAL_TEMP && charging) {
            slave->discharge_channel_state |= 1 << i;
        } else {
            slave->discharge_channel_state &= ~(1 << i);
        }
    }
}

void set_discharge_channels(Battery* battery)
{
    Stack* stack;
    int i;
    for (i = 0; i < BATTERY_SERIES_STACKS; i++) {
        stack = &(battery->stacks[i]);
        set_stack_discharge_channels(stack, battery->min_voltage, battery->master_state.charging);
    }
}

#ifndef FAKE
/*
 * @brief Ensures a valid communication with the slaves
 *
 * Wakes up the daisy chain, writes to to the configuration registers and
 * reads from them to check is the write was successful, thus checking
 * valid communication with the slaves.
 */
void start_communication_with_slaves(Battery* battery)
{
    uint16_t slave, i; // iterators
    bool slave_comm_OK = 0;
    uint16_t slave_comm_error = 0;
#if 0
    struct CFGR{
        uint8_t ADCOPT : 1;
        uint8_t DTEN   : 1;
        uint8_t REFON  : 1;
        uint8_t GPIO    : 5;
        uint32_t : 24;
        uint16_t DCC    : 12;
        uint8_t DCTO    : 4;
    }default_CFGR = {
        .ADCOPT = 0,
        .DTEN = 0,
        .REFON = 1,
        .GPIO = 0x1F,
        .DCC = 0,
        .DCTO = 0
    };
#endif
    uint8_t default_CFGR[6] = { 0b11111100, 123, 123, 123, 0, 0 };
    uint8_t CFGR_contents[6 * BATTERY_SERIES_STACKS];
    uint8_t receive[6 * BATTERY_SERIES_STACKS];

    TRIS_CS_B = 0; // set slave select pin as output

    /* Wake up the slaves  from sleep */
    wake_up_LTC_from_SLEEP(&battery->daisy_chain_B);

    /* Fill the array to send */
    for (slave = 0; slave < BATTERY_SERIES_STACKS; ++slave) {
        memcpy(&CFGR_contents[6 * slave], &default_CFGR, LTC_REG_SIZE);
    }

    while (!slave_comm_OK) {
        slave_comm_error = 0;
        broadcast_write(&battery->daisy_chain_B, WRCFGA, LTC_REG_SIZE, CFGR_contents);

        if (
            broadcast_read(&battery->daisy_chain_B, RDCFGA, LTC_REG_SIZE,
                receive))
            slave_comm_error++;

        /* check the received contents are equal to the sent */
        for (slave = 0; slave < BATTERY_SERIES_STACKS; ++slave) {
            // ignore first byte because of GPIO read being different from
            // write
            for (i = 1; i < 6; ++i) {
                if (receive[6 * slave + i] != CFGR_contents[6 * slave + i])
                    slave_comm_error++;
            }
        }

        if (slave_comm_error == 0)
            slave_comm_OK = 1;
        slave_comm_OK = 1;
        /* else maybe send the count for debug */
    }

    // update the battery structure with the written CFGR contents
    for (slave = 0; slave < BATTERY_SERIES_STACKS; ++slave) {
        memcpy(&battery->stacks[slave].slave.BM.CFGR, &default_CFGR,
            LTC_REG_SIZE);
    }
    return;
}

#endif

#ifdef FAKE

void start_communication_with_slaves(Battery* battery)
{
    return;
}

#endif

#endif
/**************************************************************************************************/

#ifdef B6V1

void control_discharge_channels(Battery* battery)
{
    uint8_t to_send[BATTERY_SERIES_STACKS * LTC_REG_SIZE];
    uint8_t to_send1[BATTERY_SERIES_STACKS * LTC_REG_SIZE];
    unsigned int i = 0;

    for (i = 0; i < BATTERY_SERIES_STACKS; i++) {

        if (battery->master_state.charging) {

            battery->stacks[i].slave.BM.DCC = battery->stacks[i].slave.discharge_channel_state & 0b00111111111111;
            battery->stacks[i].slave.BM.DCC_2 = (battery->stacks[i].slave.discharge_channel_state & 0b11000000000000) >> 12;
        } else {

            battery->stacks[i].slave.BM.DCC = 0;
            battery->stacks[i].slave.BM.DCC_2 = 0;
        }

        memcpy(to_send + (i * LTC_REG_SIZE), battery->stacks[i].slave.BM.CFGR, LTC_REG_SIZE);
        memcpy(to_send1 + (i * LTC_REG_SIZE), battery->stacks[i].slave.BM.CFGRB, LTC_REG_SIZE);
    }

#ifndef FAKE

    broadcast_write(&battery->daisy_chain_B, WRCFGA, LTC_REG_SIZE, to_send);
    broadcast_write(&battery->daisy_chain_B, WRCFGB, LTC_REG_SIZE, to_send1);

#endif

    return;
}

/**
 * @brief Commands the LTC6811 to start converting the cells voltages
 */
#ifndef FAKE

void start_cell_open_wire_check_conversion(Battery* battery, uint16_t PUP)
{
    broadcast_poll(&battery->daisy_chain_B,
        ADOW(MD_FILTERED, PUP, DCP_DISCHARGE_NOT_PERMITTED, CH_ALL_CELLS));
    return;
}

#endif

#ifdef FAKE

void start_cell_open_wire_check_conversion(Battery* battery, uint16_t PUP)
{
    return;
}

#endif

/**
 * @brief Commands the LTC6811 to start converting the cells voltages
 */
#ifndef FAKE

void start_cell_voltages_conversion(Battery* battery)
{
    broadcast_poll(&battery->daisy_chain_B,
        ADCV(MD_NORMAL, DCP_DISCHARGE_NOT_PERMITTED, CH_ALL_CELLS));
    return;
}

#endif

#ifdef FAKE

void start_cell_voltages_conversion(Battery* battery)
{
    return;
}

#endif
/**
 * @brief Writes value to the bit indicated by index of the mask pointed to by
 * temp_fault_mask
 */
void write_to_temp_fault_mask(uint16_t* temp_fault_mask, bool value, uint16_t index)
{
    *temp_fault_mask &= (~(1 << index));
    *temp_fault_mask |= (value << index);

    return;
}

#ifndef FAKE

/*
 * Executes OWC check (real thing)
 *
 */
void get_cell_open_wire_check_reading(Battery* battery)
{

    uint16_t voltages[3 * BATTERY_SERIES_STACKS];
    uint16_t stack, cell;

    /*
     * Collect cells 0-2 (group A) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVA, LTC_REG_SIZE, (uint8_t*)voltages);

    // verify crc where, broadcast = -1, activate wtd

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            if (battery->OWC_sequence == 0) {
                battery->stacks[stack].slave.cell_pu[0 + cell] = voltages[stack * 3 + cell];
            } else if (battery->OWC_sequence == 1) {
                battery->stacks[stack].slave.cell_pd[0 + cell] = voltages[stack * 3 + cell];
            }
        }
    }

    /*
     * Collect cells 3-5 (group B) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVB, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            if (battery->OWC_sequence == 0) {
                battery->stacks[stack].slave.cell_pu[3 + cell] = voltages[stack * 3 + cell];
            } else if (battery->OWC_sequence == 1) {
                battery->stacks[stack].slave.cell_pd[3 + cell] = voltages[stack * 3 + cell];
            }
        }
    }

    /*
     * Collect cells 6-8 (group C) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVC, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            if (battery->OWC_sequence == 0) {
                battery->stacks[stack].slave.cell_pu[6 + cell] = voltages[stack * 3 + cell];
            } else if (battery->OWC_sequence == 1) {
                battery->stacks[stack].slave.cell_pd[6 + cell] = voltages[stack * 3 + cell];
            }
        }
    }
    /*
     * Collect cells 9-11 (group D) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVD, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            if (battery->OWC_sequence == 0) {
                battery->stacks[stack].slave.cell_pu[9 + cell] = voltages[stack * 3 + cell];
            } else if (battery->OWC_sequence == 1) {
                battery->stacks[stack].slave.cell_pd[9 + cell] = voltages[stack * 3 + cell];
            }
        }
    }
    /*
     * Collect cells 12-14 (group E) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVE, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 2; ++cell) {
            if (battery->OWC_sequence == 0) {
                battery->stacks[stack].slave.cell_pu[12 + cell] = voltages[stack * 3 + cell];

            } else if (battery->OWC_sequence == 1) {
                battery->stacks[stack].slave.cell_pd[12 + cell] = voltages[stack * 3 + cell];
            }
        }
    }

    return;
}
#endif

#ifdef FAKE
/*
 * Simulates OWC check (fake)
 *
 */
void get_cell_open_wire_check_reading(Battery* battery)
{
    uint16_t stack, cell;

    usleep(BROADCAST_US * 5);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < BATTERY_SERIES_CELLS_PER_STACK; ++cell) {
            if (battery->OWC_sequence == 0) {
                battery->stacks[stack].slave.cell_pu[cell] = VOLTAGE(stack, cell);
            } else if (battery->OWC_sequence == 1) {
                battery->stacks[stack].slave.cell_pd[cell] = VOLTAGE(stack, cell);
            }
        }
    }

    return;
}

#endif

#ifndef FAKE
/**
 * @brief Reads all cell voltages from the slaves and writes it on the battery
 * struct
 */
void get_cell_voltages(Battery* battery)
{
    uint16_t voltages[3 * BATTERY_SERIES_STACKS];
    uint16_t stack, cell;

    /*
     * Collect cells 0-2 (group A) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVA, LTC_REG_SIZE, (uint8_t*)voltages);

    // verify crc where, broadcast = -1, activate wtd

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            battery->stacks[stack].cell_pack[0 + cell].voltage = voltages[stack * 3 + cell];
        }
    }

    /*
     * Collect cells 3-5 (group B) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVB, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            battery->stacks[stack].cell_pack[3 + cell].voltage = voltages[stack * 3 + cell];
        }
    }

    /*
     * Collect cells 6-8 (group C) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVC, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            battery->stacks[stack].cell_pack[6 + cell].voltage = voltages[stack * 3 + cell];
        }
    }
    /*
     * Collect cells 9-11 (group D) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVD, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            battery->stacks[stack].cell_pack[9 + cell].voltage = voltages[stack * 3 + cell];
        }
    }
    /*
     * Collect cells 12-14 (group E) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVE, LTC_REG_SIZE, (uint8_t*)voltages);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {
        for (cell = 0; cell < 3; ++cell) {
            battery->stacks[stack].cell_pack[12 + cell].voltage = voltages[stack * 3 + cell];
        }
    }

    return;
}
#endif

#ifdef FAKE

/**
 * @brief Reads all cell voltages from the slaves and writes it on the battery
 * struct
 */
void get_cell_voltages(Battery* battery)
{
    uint16_t stack, cell;

    usleep(BROADCAST_US * 5);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack)
        for (cell = 0; cell < BATTERY_SERIES_CELLS_PER_STACK; ++cell) {
            battery->stacks[stack].cell_pack[cell].voltage = VOLTAGE(stack, cell);
            printf("stack: %d cell: %d value: %d \n", stack, cell, battery->stacks[stack].cell_pack[cell].voltage);
        }
    return;
}

#endif

#ifndef FAKE
/**
 * @brief Reads the temperature voltages from the slaves, checks faults,
 * converts and stores the temperatures
 */
void get_temperatures(Battery* battery)
{

    uint16_t register_contents[3 * BATTERY_SERIES_STACKS];
    uint16_t stack = 0; // iterator
    uint16_t temp_voltage = 0;
    bool NTC_fault;

    /** Read temperature from GPIO 1, 2, 3 */
    broadcast_read(&battery->daisy_chain_B, RDAUXA, LTC_REG_SIZE,
        (uint8_t*)register_contents);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {

        temp_voltage = register_contents[stack * 3 + 0]; // GPIO1
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 7);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[7].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = register_contents[stack * 3 + 1]; // GPIO2
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 9);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[9].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = register_contents[stack * 3 + 2]; // GPIO3
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 11);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[11].temperature = convert_ntc_temp(temp_voltage);
        }
    }

    /** Read temperature from GPIO 4, 5, and REF voltage */
    broadcast_read(&battery->daisy_chain_B, RDAUXB, LTC_REG_SIZE,
        (uint8_t*)register_contents);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {

        temp_voltage = register_contents[stack * 3 + 0]; // GPIO4
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 13);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[13].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = register_contents[stack * 3 + 1]; // GPIO5
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 0);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[0].temperature = convert_ntc_temp(temp_voltage);
        }
    }

    /** Read temperature from GPIO 6, 7, 8 */
    broadcast_read(&battery->daisy_chain_B, RDAUXC, LTC_REG_SIZE,
        (uint8_t*)register_contents);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {

        temp_voltage = register_contents[stack * 3 + 0]; // GPIO6
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 6);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[6].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = register_contents[stack * 3 + 1]; // GPIO7
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 4);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[4].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = register_contents[stack * 3 + 2]; // GPIO8
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 2);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[2].temperature = convert_ntc_temp(temp_voltage);
        }
    }

    /** Read temperature from GPIO 9, RSVD, RSVD1 */
    broadcast_read(&battery->daisy_chain_B, RDAUXD, LTC_REG_SIZE,
        (uint8_t*)register_contents);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {

        temp_voltage = register_contents[stack * 3 + 0]; // GPIO9

        battery->stacks[stack].slave.disch_temp[0] = convert_ntc_temp(temp_voltage);
    }

    return;
}
#endif

#ifdef FAKE
void get_temperatures(Battery* battery)
{

    uint16_t register_contents[3 * BATTERY_SERIES_STACKS];
    uint16_t stack = 0; // iterator
    uint16_t temp_voltage = 0;
    bool NTC_fault;

    /** Read temperature from GPIO 1, 2, 3 */
    usleep(BROADCAST_US * 4);

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {

        temp_voltage = VOLTAGE_NTC(stack, 7); // GPIO1
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 7);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[7].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = VOLTAGE_NTC(stack, 9); // GPIO2
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 9);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[9].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = VOLTAGE_NTC(stack, 11); // GPIO3
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 11);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[11].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = VOLTAGE_NTC(stack, 13); // GPIO4
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 13);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[13].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = VOLTAGE_NTC(stack, 0); // GPIO5
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 0);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[0].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = VOLTAGE_NTC(stack, 6); // GPIO6
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 6);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[6].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = VOLTAGE_NTC(stack, 4); // GPIO7
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 4);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[4].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = VOLTAGE_NTC(stack, 2); // GPIO8
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[stack].slave.temp_fault_mask, NTC_fault, 2);

        if (!NTC_fault) {
            battery->stacks[stack].cell_pack[2].temperature = convert_ntc_temp(temp_voltage);
        }

        temp_voltage = VOLTAGE_NTC(stack, 0); // GPIO9

        battery->stacks[stack].slave.disch_temp[0] = convert_ntc_temp(temp_voltage);
    }

    return;
}
#endif

void open_wire_check_calculation(Battery* battery)
{
    uint16_t stack = 0, cell = 0;

    for (stack = 0; stack < BATTERY_SERIES_STACKS; ++stack) {

        for (cell = 0; cell < BATTERY_SERIES_CELLS_PER_STACK; ++cell) {

            battery->stacks[stack].slave.OWC_diff[cell] = ((battery->stacks[stack].slave.cell_pu[cell] / 10)
                - (battery->stacks[stack].slave.cell_pd[cell] / 10));

            if (battery->stacks[stack].slave.OWC_diff[cell] < (-1 * OWC_ERROR_DIFF)) {

                battery->stacks[stack].slave.OWC_cell_status |= 1 << (cell + 1);
            } else {

                battery->stacks[stack].slave.OWC_cell_status &= ~(1 << (cell + 1));
            }
        }

        /* Check cell pole 0 C(0) */

        if (battery->stacks[stack].slave.cell_pu[0] == 0) {

            battery->stacks[stack].slave.OWC_cell_status |= 1;

        } else {
            battery->stacks[stack].slave.OWC_cell_status &= 0xFFFE;
        }

        /* Check cell pole 14 C(14) */

        if (battery->stacks[stack].slave.cell_pd[13] == 0) {

            battery->stacks[stack].slave.OWC_cell_status |= 1 << 14;

        } else {
            battery->stacks[stack].slave.OWC_cell_status &= 0x3FFF;
        }
    }

    return;
}

/**
 * @brief Commands each LTC6811 to start converting a temperature (voltage on
 * GPIO5)
 *
 * @param temp The index of the temperature to measure.
 */
#ifndef FAKE
void start_temp_voltage_conversion(Battery* battery)
{
    // control_temp_mux_address(battery, address);

    broadcast_poll(&battery->daisy_chain_B, ADAX(MD_NORMAL, CHG_ALL_GPIO));
    return;
}

#endif

#ifdef FAKE

void start_temp_voltage_conversion(Battery* battery)
{
    return;
}

#endif

void set_stack_discharge_channels(Stack* stack, uint16_t min_voltage, bool charging)
{

    Slave* slave = &stack->slave;
    int i;
    for (i = 0; i < BATTERY_SERIES_CELLS_PER_STACK; i++) { // change 0 to stack id
        if (stack->cell_pack[i].voltage - min_voltage > BAT_CHARGING_DELTA && slave->disch_temp[0] < MAX_BAL_TEMP && charging) {
            slave->discharge_channel_state |= 1 << i;
        } else {
            slave->discharge_channel_state &= ~(1 << i);
        }
    }
}

void set_discharge_channels(Battery* battery)
{

    Stack* stack;
    int i;
    for (i = 0; i < BATTERY_SERIES_STACKS; i++) {
        stack = &(battery->stacks[i]);
        set_stack_discharge_channels(stack, battery->min_voltage, battery->master_state.charging);
    }
}
#ifndef FAKE
/*
 * @brief Ensures a valid communication with the slaves
 *
 * Wakes up the daisy chain, writes to to the configuration registers and
 * reads from them to check is the write was successful, thus checking
 * valid communication with the slaves.
 */
void start_communication_with_slaves(Battery* battery)
{
    uint16_t slave, i; // iterators
    bool slave_comm_OK = 0;
    uint16_t slave_comm_error = 0;
#if 0
    struct CFGR{
        uint8_t ADCOPT : 1;
        uint8_t DTEN   : 1;
        uint8_t REFON  : 1;
        uint8_t GPIO    : 5;
        uint32_t : 24;
        uint16_t DCC    : 12;
        uint8_t DCTO    : 4;
    }default_CFGR = {
        .ADCOPT = 0,
        .DTEN = 0,
        .REFON = 1,
        .GPIO = 0x1F,
        .DCC = 0,
        .DCTO = 0
    };
#endif
    uint8_t default_CFGR[6] = { 0b11111111, 123, 123, 123, 0, 0 };
    uint8_t default_CFGRB[6] = { 0b00001111, 0, 0, 0, 0, 0 };
    uint8_t CFGR_contents[6 * BATTERY_SERIES_STACKS];
    uint8_t CFGRB_contents[6 * BATTERY_SERIES_STACKS];
    uint8_t receive[6 * BATTERY_SERIES_STACKS];
    uint8_t receive1[6 * BATTERY_SERIES_STACKS];

    TRIS_CS_B = 0; // set slave select pin as output

    /* Wake up the slaves  from sleep */
    wake_up_LTC_from_SLEEP(&battery->daisy_chain_B);

    /* Fill the array to send */
    for (slave = 0; slave < BATTERY_SERIES_STACKS; ++slave) {
        memcpy(&CFGR_contents[6 * slave], &default_CFGR, LTC_REG_SIZE);
    }

    /* Fill the array to send */
    for (slave = 0; slave < BATTERY_SERIES_STACKS; ++slave) {
        memcpy(&CFGRB_contents[6 * slave], &default_CFGRB, LTC_REG_SIZE);
    }

    while (!slave_comm_OK) {
        slave_comm_error = 0;
        broadcast_write(&battery->daisy_chain_B, WRCFGA, LTC_REG_SIZE, CFGR_contents);
        broadcast_write(&battery->daisy_chain_B, WRCFGB, LTC_REG_SIZE, CFGRB_contents);

        if (broadcast_read(&battery->daisy_chain_B, RDCFGA, LTC_REG_SIZE, receive)
            && broadcast_read(&battery->daisy_chain_B, RDCFGB, LTC_REG_SIZE, receive1))
            slave_comm_error++;

        /* check the received contents are equal to the sent */
        for (slave = 0; slave < BATTERY_SERIES_STACKS; ++slave) {
            // ignore first byte because of GPIO read being different from
            // write
            for (i = 1; i < 6; ++i) {
                if (receive[6 * slave + i] != CFGR_contents[6 * slave + i]
                    && receive1[6 * slave + i] != CFGRB_contents[6 * slave + i])
                    slave_comm_error++;
            }
        }

        if (slave_comm_error == 0)
            slave_comm_OK = 1;
        slave_comm_OK = 1;
        /* else maybe send the count for debug */
    }

    // update the battery structure with the written CFGR contents
    for (slave = 0; slave < BATTERY_SERIES_STACKS; ++slave) {
        memcpy(&battery->stacks[slave].slave.BM.CFGR, &default_CFGR,
            LTC_REG_SIZE);
        memcpy(&battery->stacks[slave].slave.BM.CFGRB, &default_CFGRB,
            LTC_REG_SIZE);
    }
    return;
}

#endif

#ifdef FAKE

void start_communication_with_slaves(Battery* battery)
{
    return;
}

#endif

#endif

/**************************************************************************************************/
