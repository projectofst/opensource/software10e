/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdint.h>

#include "LTC/LTC68xx.h"
#include "battery.h"
#include "battery_monitor.h"
#include "can-ids-v2/can_cfg.h"
#include "can-ids-v2/master_can.h"
#include "cell.h"
#include "lib_pic33e/flash.h"
#include "lib_pic33e/version.h"

#include "bat_cfg.h"

static uint32_t param[CFG_MASTER_SIZE] = { [0 ... CFG_MASTER_SIZE - 1] = 0xFFFFFFFF };
uint16_t flash[CFG_MASTER_SIZE];

/**
 * @brief Initializes a battery structure
 */
void battery_init(Battery* battery)
{

    uint16_t stack_i, cell_i; // iterators
    uint16_t i;
    Stack* stack;
    Cell* cell;

    /* init stack values */
    for (stack_i = 0; stack_i < BATTERY_SERIES_STACKS; ++stack_i) {

        stack = &battery->stacks[stack_i];
        stack->id = stack_i;

        /* init cell values */
        for (cell_i = 0; cell_i < BATTERY_SERIES_CELLS_PER_STACK; ++cell_i) {

            cell = &stack->cell_pack[cell_i];

            cell->id = stack_i * BATTERY_SERIES_CELLS_PER_STACK + cell_i;
            cell->voltage = UINT16_MAX; // 6.5535 V
            cell->temperature = INT16_MAX; // 3276.7 ºC
            cell->charge = UINT16_MAX;
            cell->state_of_charge = UINT16_MAX;
            cell->soh = 100;
            cell->pwr_lss = 0; ///< Cell power losses ir*|I_bat^2|
            cell->ir = 0; ///< Cell internal resistance
            cell->bal_curr = 0; ///< Balancing current when channel is on
            cell->bal_time = 0;
            cell->nominal_cap = BAT_TOTAL_CAPACITY;
        }

        /* init slave values */
        stack->slave.discharge_channel_state = 0x0000; // All off
        stack->slave.discharge_channel_fault_mask = 0x0000; // Assume OK
        stack->slave.cell_connection_fault_mask = 0x0000; // Assume OK
        stack->slave.temp_fault_mask = 0xFFFF; // Assume NOK

        /* Ver */
        stack->slave.disch_temp[0] = INT16_MAX;
        stack->slave.disch_temp[1] = INT16_MAX;
        stack->slave.BM_temperature = INT16_MAX;
        stack->slave.OWC_cell_status = 0x0000;

        /* init OWC verification values */
        for (cell_i = 0; cell_i < BATTERY_SERIES_CELLS_PER_STACK; ++cell_i) {

            stack->slave.cell_pu[cell_i] = 0x0000;
            stack->slave.cell_pd[cell_i] = 0x0000;
            stack->slave.OWC_diff[cell_i] = 0x0000;
        }

        stack->slave.cell_pd[0] = 1500;

        /* init battery monitor values */
        LTC_init(&stack->slave.BM);

        /* init stack summary values */
        stack->total_voltage = UINT16_MAX;
        stack->max_voltage = UINT16_MAX;
        stack->min_voltage = UINT16_MAX;
        stack->mean_voltage = UINT16_MAX;
        stack->max_voltage_cell = UINT8_MAX;
        stack->min_voltage_cell = UINT8_MAX;
        stack->state_of_charge = UINT16_MAX;
        stack->max_temperature = INT16_MAX;
        stack->min_temperature = INT16_MIN;
        stack->mean_temperature = 0;
        stack->max_temperature_cell = UINT8_MAX;
        stack->max_temperature_cell = UINT8_MAX;
        stack->min_temperature_cell = UINT8_MAX;
        stack->min_temperature_cell = UINT8_MAX;
    }

    /* slave communication */
    daisy_chain_B_init(&battery->daisy_chain_B);

    /* battery voltage values */
    battery->total_voltage = UINT16_MAX;
    battery->max_voltage = UINT16_MAX;
    battery->min_voltage = UINT16_MAX;
    battery->mean_voltage = UINT16_MAX;
    battery->max_voltage_cell[0] = UINT8_MAX;
    battery->max_voltage_cell[1] = UINT8_MAX;
    battery->min_voltage_cell[0] = UINT8_MAX;
    battery->min_voltage_cell[1] = UINT8_MAX;
    battery->OWC_sequence = 0;

    /* battery temperature values */
    battery->max_temperature = INT16_MAX;
    battery->min_temperature = INT16_MIN;
    battery->mean_temperature = 0;
    battery->max_temperature_cell[0] = UINT8_MAX;
    battery->max_temperature_cell[1] = UINT8_MAX;
    battery->min_temperature_cell[0] = UINT8_MAX;
    battery->min_temperature_cell[1] = UINT8_MAX;

    /* Energy meter values */

    energy_meter_t* EM = &battery->energy_meter;

    memset(EM, 0, sizeof(energy_meter_t));

    /* relay states */
    battery->relay_state.all = 1; // assume all relays start closed
    battery->relay_state.all = 0; // but the intended state is opened

    /* master status */
    battery->ams_ok = 0; // assume AMS is not OK
    battery->imd_ok = 0; // assume IMD is not OK
    battery->imd_latch = 0;
    battery->ams_latch = 0;
    battery->precharge = 1;
    battery->discharge = 1;
    battery->sc_dcu_imd = 0;
    battery->sc_imd_ams = 0;
    battery->sc_ams_dcu = 0;
    battery->sc_tsms_relays = 0;
    battery->shutdown_above_minimum = 0;
    battery->shutdown_open = 1;

    battery->dcdc_temp[0] = 0;
    battery->dcdc_temp[1] = 0;

    // get_version(VERSION, (unsigned*)&battery->settable); //! ver isto

    param[CFG_MASTER_VERSION] = VERSION;

    battery->settable.bms_cell_limit_max_t = (uint16_t*)&param[CFG_MASTER_CELL_MAX_T];
    battery->settable.defaults[1] = BAT_LIMIT_MAX_T_DISCH;

    battery->settable.bms_cell_limit_min_t = (uint16_t*)&param[CFG_MASTER_CELL_MIN_T];
    battery->settable.defaults[2] = BAT_LIMIT_MIN_T_DISCH;

    battery->settable.bms_cell_limit_max_v = (uint16_t*)&param[CFG_MASTER_CELL_MAX_V];
    battery->settable.defaults[3] = BAT_LIMIT_MAX_CELL_V;

    battery->settable.bms_cell_limit_min_v = (uint16_t*)&param[CFG_MASTER_CELL_MIN_V];
    battery->settable.defaults[4] = BAT_LIMIT_MIN_CELL_V;

    battery->settable.bms_bat_limit_max_v = (uint16_t*)&param[CFG_MASTER_BAT_MAX_V];
    battery->settable.defaults[5] = BAT_LIMIT_MAX_V;

    battery->settable.bms_bat_charging_delta = (uint16_t*)&param[CFG_MASTER_BAT_CHARGE_DELTA];
    battery->settable.defaults[6] = BAT_CHARGING_DELTA;

    battery->settable.bms_cell_limit_max_t = (uint16_t*)&param[CFG_MASTER_MAX_DISCH_CHANNEL_T];
    battery->settable.defaults[7] = MAX_BAL_TEMP;

    read_flash(flash, 0, CFG_MASTER_SIZE);

    for (i = 1; i < CFG_MASTER_SIZE; i++) {
        if (flash[i] == 0xFFFF || flash[i] == 0x0) {
            param[i] = battery->settable.defaults[i];
            flash[i] = battery->settable.defaults[i];
        } else
            param[i] = flash[i];
    }

    write_flash(flash, 0, CFG_MASTER_SIZE);

    cfg_config(param, CFG_MASTER_SIZE);

    MasterState _master_state = {
        .fans = 1, // on
        .verbose = 0, // off
        .charging = 0, // off
        .car_ts = 0, // off
    };

    battery->master_state = _master_state;

    return;
}

void set_ams_ok(Battery* battery, bool intended_ams_ok, MASTER_MSG_TS_OFF_Reason reason)
{
    // TODO: should check ams_ok status
    bool ams_ok = 1;

    if (intended_ams_ok && !ams_ok) {
        return;
    }

    battery->ams_ok = intended_ams_ok;
    battery->ams_off_reason = reason;
    return;
}
