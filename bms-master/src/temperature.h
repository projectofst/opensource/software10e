/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __TEMP_H_
#define __TEMP_H_

#include <stdbool.h>
#include <stdint.h>

#include "bms_version.h"

#define MAX_VALID_TEMP_VOLT 27325 /**< [mv/10] */
#define MIN_VALID_TEMP_VOLT 1133 /**< [mv/10] */

/**
 * @brief Converts ntc voltage to temperature
 */
int16_t convert_ntc_temp(uint16_t v);

bool NTC_is_faulty(uint16_t voltage);
bool NTC_is_valid(uint16_t temp_fault_mask, uint16_t index);
uint16_t count_valid_cell_NTC_in_stack(uint16_t temp_fault_mask);
#endif
