/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*! \file bat_cfg.h
    \brief File with specs for each accumulator and other
    important static parameters.

    **Author/s:** André Agostinho, Miguel Lourenço.
*/

#ifndef _BAT_CFG_
#define _BAT_CFG_

#include "cell.h"
#include <stdbool.h>

#define TRUE  1
#define FALSE 0

/// Time based defines
#define HALF_SEC 500    /**< [ms] */
#define ONE_SEC 1000    /**< [ms] */

/****************************************************************
 * Some bateries use ISA module, others dont.
 */
#ifdef B4V2
#define ISA_USED false
#endif

#if defined(B6V1) || defined(B5V2)
#define ISA_USED true
#define ISA_WTD_TIMEOUT 1000    /**< ISA wathdog time limit [ms] */
#endif
/****************************************************************/

/**
 * \brief Reasons for the TS to be turned OFF
 *
 * Possible values of "reason" field in MASTER_MSG_TS
 */
typedef enum {
    TS_OFF_REASON_DASH_BUTTON,
    TS_OFF_REASON_OVERVOLTAGE,
    TS_OFF_REASON_UNDERVOLTAGE,
    TS_OFF_REASON_OVERCURRENT_IN,
    TS_OFF_REASON_OVERCURRENT_OUT,
    TS_OFF_REASON_OVERTEMPERATURE,
    TS_OFF_REASON_UNDERTEMPERATURE,
    TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_IMD,
    TS_OFF_REASON_FAKE_ERROR,
    TS_OFF_REASON_OTHER,
    TS_OFF_REASON_NO_ERROR,
    TS_OFF_REASON_NO_ISA,
    TS_OFF_REASON_SOC_LIMTS
} MASTER_MSG_TS_OFF_Reason;

/**
 * \brief Reasons for existing error
 *
 * Possible values of "error" field...(TODO)
 */
typedef enum {
    AMS_ERR_OVERVOLTAGE,
    AMS_ERR_UNDERRVOLTAGE,
    AMS_ERR_OVERCURRENT_CHR,
    AMS_ERR_OVERCURRENT_DSCHR,
    IMD_ERR_GENERAL,
    AMS_ERR_UNDERTEMP_CHR,
    AMS_ERR_OVERTEMP_CHR,
    AMS_ERR_UNDERTEMP_DSCHR,
    AMS_ERR_OVERTEMP_DSCHR,
    AMS_ERR_MIN_CELL_LTC,
    AMS_ERR_FAKE_ERROR,
} MASTER_MSG_ERR_Reason;


/***************************************************************************************************
 *  \brief General configuration for Green 09e used accumulator
 *  (green casing) and red casing battery.
 */
#if defined(B4V2) || defined(B5V2)

/****************************************************************
 * @name Battery Operation Limits error offsets
 * @{
 *
 *  Temp and voltage error offset, FSG ESF, TRUE - apply, FALSE - dont apply.
 *  There is also an optional current(I) error offset
 */

#define APPLY_MEASUREMENT_ERROR

#ifdef APPLY_MEASUREMENT_ERROR
#define TEMPERATURE_ERROR_OFFSET 10 /**< [ºC*0.1] */
#define VOLTAGE_ERROR_OFFSET 500 	/**< [mV] */
#define I_ERROR_OFFSET	10			/**< [mA] */
#endif

#ifndef APPLY_MEASUREMENT_ERROR
#define TEMPERATURE_ERROR_OFFSET 0  /**< [ºC*0.1] */
#define VOLTAGE_ERROR_OFFSET 0 	    /**< [mV] */
#define I_ERROR_OFFSET 0			/**< [mA] */
#endif

/****************************************************************
 * @name Battery Electrical Configuration
 * @{
 */

#define BATTERY_SERIES_CELLS_PER_STACK 12
#define BATTERY_PARALLEL_CELLS 2
#define BATTERY_SERIES_STACKS 12

#define BATTERY_TOTAL_SERIES_CELLS (BATTERY_SERIES_CELLS_PER_STACK * BATTERY_SERIES_STACKS)
#define BATTERY_TOTAL_CELLS (BATTERY_TOTAL_SERIES_CELLS * BATTERY_PARALLEL_CELLS)
#define BAT_TOTAL_CAPACITY CELL_CAPACITY * BATTERY_PARALLEL_CELLS /** [mAh] **/


/****************************************************************
 * @name Battery Operation Limits
 * @{
 */

/// Temperature limits

/// Discharge
#define BAT_LIMIT_MAX_T_DISCH CELL_DISCH_MAX_T - TEMPERATURE_ERROR_OFFSET /**< [ºC*0.1] */
#define BAT_LIMIT_MIN_T_DISCH CELL_DISCH_MIN_T + TEMPERATURE_ERROR_OFFSET /**< [ºC*0.1] */

/// Charge
#define BAT_LIMIT_MAX_T_CHRG CELL_CHRG_MAX_T - TEMPERATURE_ERROR_OFFSET /**< [ºC*0.1] */
#define BAT_LIMIT_MIN_T_CHRG CELL_CHRG_MIN_T + TEMPERATURE_ERROR_OFFSET /**< [ºC*0.1] */

/// Temperature sensors
#define STACK_NUMBER_OF_NTC 10      /**< Number of NTC's per stack used for cell temp measurement */
#define STACK_LIMIT_MIN_CELL_NTC 5  /**< Below this number AMS error must trigger */


/// Voltage limits

/// Cell
#define BAT_LIMIT_MAX_CELL_V CELL_MAX_VOLTAGE - VOLTAGE_ERROR_OFFSET 	        /**< [mV*0.1] */
#define BAT_LIMIT_MIN_CELL_V CELL_MIN_VOLTAGE + VOLTAGE_ERROR_OFFSET 	        /**< [mV*0.1] */
/// Battery
#define BAT_LIMIT_MAX_V (CELL_MAX_VOLTAGE / 100) * BATTERY_TOTAL_SERIES_CELLS	/**< [mV*10] */ //Dá para adicionar um offset tmb tipo 599.9 V
#define BAT_LIMIT_MIN_V (CELL_MIN_VOLTAGE / 100) * BATTERY_TOTAL_SERIES_CELLS	/**< [mV*10] */ //Dá para adicionar offset


/// Current limits

/// Charge
#define BAT_LIMIT_MAX_I_CHRG_CONT CELL_MAX_I_CHRG_CONT * BATTERY_PARALLEL_CELLS /**< [mA*100] */
#define BAT_LIMIT_MAX_I_CHRG CELL_MAX_I_CHRG_PEAK * BATTERY_PARALLEL_CELLS      /**< [mA*100] */
#define BAT_LIMIT_MAX_I_CHRG_TIME CELL_MAX_I_CHRG_PEAK_TIME                     /**< [ms] */

/// Discharge
#define BAT_LIMIT_MAX_I_DISCH_CONT CELL_MAX_I_DISCH_CONT * BATTERY_PARALLEL_CELLS   /**< [mA*100] */
#define BAT_LIMIT_MAX_I_DISCH CELL_MAX_I_DISCH_PEAK * BATTERY_PARALLEL_CELLS        /**< [mA*100] */
#define BAT_LIMIT_MAX_I_DISCH_TIME CELL_MAX_I_DISCH_PEAK_TIME                       /**< [ms] */


/****************************************************************
 * @name Cell balancing
 * @{
 */

#define MAX_BAL_TEMP 750 /**< Maximum temperature of ntc placed near balancing channels heatsink, [ºC*0.1] */
#define BAT_CHARGING_DELTA 50 /**< Voltage difference allowed, end of balancing [0.1*mV] */
#define BALANCING_MIN_DELTA_SOC_PERC 0.5 /**< SoC difference allowed, end of balancing [0.1*%] */
#define BMS_BAL_RESISTANCE_OHM 15.0 /**< Resistance of an individual channel, [ohm] */


/****************************************************************
 * @name Relay timings
 */

#define RELAY_BOUNCE_TIME 6 /**< Relay closes, contacts bounce before closing, [ms]*/
#define RELAY_OPERATION_TIME 25 /**< Time for relay to close, [ms]*/
#define RELAY_RELEASE_TIME 10 /**< Time before contact opens, [ms] */


/****************************************************************
 * @name Misc parameters
 */

#define OWC_ERROR_DIFF 400
// #define CELL_TEMP_SCHMITT_LOW 350
// #define CELL_TEMP_SCHMITT_HIGH 400
// #define CELL_TEMP_LIMIT_RISING
// #define CELL_TEMP_LIMIT_FALLING


/**
 * Maximum voltage drop over fuse. If the measured voltage difference
 * across the fuse is larger than this value, fuse has tripped.
 *
 * Fuse used: Siba - 20 189 29-80A,
 * Imax =  160A, Pwr_loss = 21,5W: -> voltage drop roughly 140 mV.
 * -> select 500mV because of measurement inaccuracies
 */
#if ISA_USED == TRUE /**< Use only if there is a dedicated voltage measurement for this*/
#define MAX_VOLTAGE_DROP_OVER_FUSE_mV 500
#endif

/// Precharge
#define DC_LINK_CAPACITANCE 300 /**< Inverter DC link capacitors capacitance, 4*75uF [uF] */
#define PRECHARGE_R 1000
#define RC_TIME_CONSTANT 0.3    /**< tau = R*C, [s] */

#endif


/***************************************************************************************************
 *  \brief General configuration for Kevlar accumulator
 *  (yellow casing), used in 10e.
 */

#ifdef B6V1

/****************************************************************
 * @name Battery Operation Limits error offsets
 * @{
 *
 *  Temp and voltage error offset, FSG ESF, TRUE - apply, FALSE - dont apply.
 *  There is also an optional current(I) error offset
 */

#define APPLY_MEASUREMENT_ERROR TRUE

#if APPLY_MEASUREMENT_ERROR == TRUE
#define TEMPERATURE_ERROR_OFFSET 10 /**< [ºC*0.1] */
#define VOLTAGE_ERROR_OFFSET 500    /**< [mV] */
#define I_ERROR_OFFSET  10          /**< [mA] */
#endif

#if APPLY_MEASUREMENT_ERROR == FALSE
#define TEMPERATURE_ERROR_OFFSET 0  /**< [ºC*0.1] */
#define VOLTAGE_ERROR_OFFSET 0      /**< [mV] */
#define I_ERROR_OFFSET 0            /**< [mA] */
#endif

/****************************************************************
 * @name Battery Electrical Configuration
 * @{
 */

#define BATTERY_SERIES_CELLS_PER_STACK 14
#define BATTERY_PARALLEL_CELLS 2
#define BATTERY_SERIES_STACKS 10

#define BATTERY_TOTAL_SERIES_CELLS (BATTERY_SERIES_CELLS_PER_STACK * BATTERY_SERIES_STACKS)
#define BATTERY_TOTAL_CELLS (BATTERY_TOTAL_SERIES_CELLS * BATTERY_PARALLEL_CELLS)
#define BAT_TOTAL_CAPACITY CELL_CAPACITY * BATTERY_PARALLEL_CELLS /** [mAh] **/

/****************************************************************
 * @name Battery Operation Limits
 * @{
 */

/// Temperature limits

/// Discharge
#define BAT_LIMIT_MAX_T_DISCH CELL_DISCH_MAX_T - TEMPERATURE_ERROR_OFFSET /**< [ºC*0.1] */
#define BAT_LIMIT_MIN_T_DISCH CELL_DISCH_MIN_T + TEMPERATURE_ERROR_OFFSET /**< [ºC*0.1] */

/// Charge
#define BAT_LIMIT_MAX_T_CHRG CELL_CHRG_MAX_T - TEMPERATURE_ERROR_OFFSET /**< [ºC*0.1] */
#define BAT_LIMIT_MIN_T_CHRG CELL_CHRG_MIN_T + TEMPERATURE_ERROR_OFFSET /**< [ºC*0.1] */

/// Temperature sensors
#define STACK_NUMBER_OF_NTC 8      /**< Number of NTC's per stack used for cell temp measurement */
#define STACK_LIMIT_MIN_CELL_NTC 5  /**< Below this number AMS error must trigger */


/// Voltage limits

/// Cell
#define BAT_LIMIT_MAX_CELL_V CELL_MAX_VOLTAGE - VOLTAGE_ERROR_OFFSET            /**< [mV*0.1] */
#define BAT_LIMIT_MIN_CELL_V CELL_MIN_VOLTAGE + VOLTAGE_ERROR_OFFSET            /**< [mV*0.1] */
/// Battery
#define BAT_LIMIT_MAX_V (CELL_MAX_VOLTAGE / 100) * BATTERY_TOTAL_SERIES_CELLS   /**< [mV*10] */ //Dá para adicionar um offset tmb tipo 599.9 V
#define BAT_LIMIT_MIN_V (CELL_MIN_VOLTAGE / 100) * BATTERY_TOTAL_SERIES_CELLS   /**< [mV*10] */ //Dá para adicionar offset


/// Current limits

/// Charge
#define BAT_LIMIT_MAX_I_CHRG_CONT CELL_MAX_I_CHRG_CONT * BATTERY_PARALLEL_CELLS /**< [mA*100] */
#define BAT_LIMIT_MAX_I_CHRG CELL_MAX_I_CHRG_PEAK * BATTERY_PARALLEL_CELLS      /**< [mA*100] */
#define BAT_LIMIT_MAX_I_CHRG_TIME CELL_MAX_I_CHRG_PEAK_TIME                     /**< [ms] */

/// Discharge
#define BAT_LIMIT_MAX_I_DISCH_CONT CELL_MAX_I_DISCH_CONT * BATTERY_PARALLEL_CELLS   /**< [mA*100] */
#define BAT_LIMIT_MAX_I_DISCH CELL_MAX_I_DISCH_PEAK * BATTERY_PARALLEL_CELLS        /**< [mA*100] */
#define BAT_LIMIT_MAX_I_DISCH_TIME CELL_MAX_I_DISCH_PEAK_TIME                       /**< [ms] */


/****************************************************************
 * @name Cell balancing
 * @{
 */

#define MAX_BAL_TEMP 550 /**< Maximum temperature of ntc placed near balancing channels heatsink, [ºC*0.1] */
#define BAT_CHARGING_DELTA 50 /**< Voltage difference allowed, end of balancing [0.1*mV] */
#define BALANCING_MIN_DELTA_SOC_PERC 0.5 /**< SoC difference allowed, end of balancing [0.1*%] */
#define BMS_BAL_RESISTANCE_OHM 20.0 /**< Resistance of an individual channel, [ohm] */


/****************************************************************
 * @name Relay timings
 */

#define RELAY_BOUNCE_TIME 6 /**< Relay closes, contacts bounce before closing, [ms]*/
#define RELAY_OPERATION_TIME 25 /**< Time for relay to close, [ms]*/
#define RELAY_RELEASE_TIME 10 /**< Time before contact opens, [ms] */


/****************************************************************
 * @name Misc parameters
 */

#define OWC_ERROR_DIFF 400
// #define CELL_TEMP_SCHMITT_LOW 350
// #define CELL_TEMP_SCHMITT_HIGH 400
// #define CELL_TEMP_LIMIT_RISING
// #define CELL_TEMP_LIMIT_FALLING


/**
 * Maximum voltage drop over fuse. If the measured voltage difference
 * across the fuse is larger than this value, fuse has tripped.
 *
 * Fuse used: Littelfuse L70QS070,
 * Imax =  70A, Pwr_loss = 20W: -> voltage drop roughly 286 mV.
 * -> select 500mV because of measurement inaccuracies
 */
#if ISA_USED == TRUE /**< Use only if there is a dedicated voltage measurement for this*/
#define MAX_VOLTAGE_DROP_OVER_FUSE_mV 500
#endif

/// Precharge
#define DC_LINK_CAPACITANCE 300 /**< Inverter DC link capacitors capacitance, 4*75uF [uF] */
#define PRECHARGE_R 1000
#define RC_TIME_CONSTANT 0.3    /**< tau = R*C, [s] */

#endif

#endif
