/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file bat_cfg.h
    \brief File with specs for each accumulator and other 
    important static parameters.
    
    **Author/s:** André Agostinho, João Freitas, Miguel Lourenço.
*/

#ifndef _BATTERY_H_
#define _BATTERY_H_

#include <stdbool.h>
#include <stdint.h>

#include "bms_version.h"

#include "can-ids-v2/master_can.h"

#include "energy_meter/energy_meter.h"

#include "LTC/LTC68xx.h"

#include "bat_cfg.h"

/**
 * \brief When TS is ON, tells wich load state the battery is in. 
 */
typedef enum {
    BAT_CHRG,   /**< Charging */
    BAT_DISCH,  /**< Discharging */
    BAT_IDLE,   /**< Idle, no currrent or change in voltage */
} BAT_LIVE_STATE;

/**
 * \brief Miscellaneous defines
 */

#define N_TASKS 13                  /**< Number of tasks for the bms scheduler. */
#define MONITOR_ROUTINE_FREQ 100    /**< Frequency that wich battery monitor function runs */
#define ERROR_STATES_FREQ 0         /**< What is this? */

#define  AUTO_CALIBRATION_TIME 120 //[s]

/*!
 * Data structures representing changeable parameters and their values.
 */ 
typedef struct set_struct {
    uint16_t* bms_cell_limit_max_t;
    uint16_t* bms_cell_limit_min_t;
    uint16_t* bms_cell_limit_max_v;
    uint16_t* bms_cell_limit_min_v;
    uint16_t* bms_bat_limit_max_v;
    uint16_t* bms_bat_charging_delta;
    uint16_t* max_disch_temp;
    uint16_t defaults[CFG_MASTER_SIZE];
    uint16_t* bms_aparent_cell_nominal_capacity;
    uint16_t* bms_auto_calibration_time_threshold;


    uint16_t* bms_soc_upper_limit;  /**< Maximum value 100%, never lower than bms_soc_lower_limit, [0.1%]*/
    uint16_t* bms_soc_lower_limit;  /**< Minimum value 0%, never higher than bms_soc_upper_limit, [0.1%]*/
} Settables;


/**
 * @brief Structure representing a cell air_negatived its parameters
 */

/****************************************************************
 * @name Cell information data structure
 * @{
 */
typedef struct cell_struct {
    uint8_t id; /**< cell ID within the whole battery, starts in 0 */
    uint8_t voltage_state : 4; /**< voltage status (OV/UV) */
    uint8_t temperature_state : 4; /**< temperature status (OV/UV) */
    uint16_t voltage; /**< [mV*0.1] current cell voltage */
    int16_t temperature; /**< [ºC*0.1] current cell temperature */
    uint16_t charge; /**< [mAh] estimated cell remaining charge */
    uint16_t state_of_charge; /**< [%] estimated cell state of charge */
    uint16_t soh;
    uint16_t pwr_lss;   ///< Cell power losses ir*|I_bat^2|
    uint16_t ir;        ///< Cell internal resistance
    uint16_t bal_curr;  ///< Balancing current when channel is on
    uint16_t bal_time;  ///< Time to balance to minimum voltage 
    uint16_t nominal_cap; /**< Nominal cell capacity, value from datasheet or test data */

} Cell;

/**
 * @brief Structure representing a BMS slave
 */
typedef struct slave_struct {
    /**
     * Bitfield to keep what discharge channels are ON or OFF
     */
    uint16_t discharge_channel_state; /**< [ON/OFF] */

    /**
     * Bitfield to keep faulty discharge channels
     */
    uint16_t discharge_channel_fault_mask; /**< [Fault - '1' / No fault - '0'] */

    /**
     * Bitfiled to keep faulty cell connections
     * Basically if this is not 0 then the TS must be turned off
     */
    uint16_t cell_connection_fault_mask; /**< [Fault - '1' / No fault - '0'] */

    /**
     * Bitfield to keep faulty cell NTC
     */
    uint16_t temp_fault_mask; /**< [Fault - '1' / No fault - '0'] */

    /**
     * Array of cell voltage when performing OWC in pull up
     */
    uint16_t cell_pu[BATTERY_SERIES_CELLS_PER_STACK];

    /**
     * Array of cell voltage when performing OWC in pull down
     */
    uint16_t cell_pd[BATTERY_SERIES_CELLS_PER_STACK];

    /**
     * Difference from pull up and pull down measurements in OWC verification
     */
    int16_t OWC_diff[BATTERY_SERIES_CELLS_PER_STACK];

    /**
     * Bitfiled to keep status about cell connections
     * 0 - not open, 1 - connection opened
     */
    uint16_t OWC_cell_status; /**< [OPEN/CLOSED] */

    /**
     * @name Temperatures in the Slave PCB
     * @{
     */

    int16_t BM_temperature;
    int16_t slave_temperature[2];
    int16_t disch_temp[2];
    /**@}*/

    LTC BM; /**< battery monitor information */
} Slave;

typedef struct _master_state {
    bool fans;
    bool verbose;
    bool charging;
    bool car_ts;
    bool ts_turn_on;
} MasterState;

/**
 * @brief Structure representing a battery stack
 */
typedef struct stack_struct {

    uint8_t id;

    Cell cell_pack[BATTERY_SERIES_CELLS_PER_STACK]; /**< stack's cell group */
    Slave slave; /**< BMS Slave */

    uint16_t total_voltage; /**< [mV] stack's voltage */
    uint16_t max_voltage; /**< [mV*0.1] voltage of the cell with the highest voltage */
    uint16_t min_voltage; /**< [mV*0.1] voltage of the cell with the lowest voltage */
    uint16_t mean_voltage; /**< [mv*0.1] mean of the stack's cell voltages */
    uint8_t max_voltage_cell; /**< cell with the highest voltage */
    uint8_t min_voltage_cell; /**< cell with the lowest voltage */

    /**
     * @brief State of charge of the entire cell pack
     *
     * The state of charge of the stack is defined as the state of charge of the
     * cell with the lowest state of charge within this stack.
     */
    uint16_t state_of_charge;

    int16_t max_temperature; /**< [ºC*0.1] temperature of the hottest cell */
    int16_t min_temperature; /**< [ºC*0.1] temperature of the coldest cell */
    int16_t mean_temperature; /**< [ºC*0.1] mean of the stack's cell temperatures */
    uint8_t max_temperature_cell; /**< hottest cell */
    uint8_t min_temperature_cell; /**< coldest cell */
    uint16_t n_valid_cell_NTC; /**< number of valid cell NTC in this stack */
} Stack;

/**
 * @brief Structure representing the entire battery
 */
typedef struct battery_struct {

    Stack stacks[BATTERY_SERIES_STACKS];

    LTC_daisy_chain daisy_chain_B;

    uint16_t total_voltage; /**< [mV*10] battery's total voltage */
    uint16_t max_voltage; /**< [mV*0.1] voltage of the cell with the highest voltage */
    uint16_t min_voltage; /**< [mV*0.1] voltage of the cell with the lowest voltage */
    uint16_t mean_voltage; /**< [mv*0.1] mean of the battery's cell voltages */
    uint8_t max_voltage_cell[2]; /**< cell with the highest voltage [stack, cell]*/
    uint8_t min_voltage_cell[2]; /**< cell with the lowest voltage [stack, cell]*/

    uint16_t OWC_sequence;

    /**
     * @brief State of charge of the battery
     *
     * The state of charge of the battery is defined as the state of charge of the
     * cell with the lowest state of charge within the whole battery.
     */

    energy_meter_t energy_meter;

    int16_t max_temperature; /**< [ºC*0.1] temperature of the hottest cell */
    int16_t min_temperature; /**< [ºC*0.1] temperature of the coldest cell */
    int16_t mean_temperature; /**< [ºC*0.1] mean of the battery's cell temperatures */
    uint8_t max_temperature_cell[2]; /**< hottest cell [stack, cell]*/
    uint8_t min_temperature_cell[2]; /**< coldest cell [stack, cell]*/

    union {
        struct {
            bool air_plus : 1;
            bool air_minus : 1;
            bool precharge_relay : 1;
            bool discharge_relay : 1;
        };
        uint16_t all;
    } relay_state;

    union {
        struct {
            bool air_plus : 1;
            bool air_minus : 1;
            bool precharge_relay : 1;
            bool discharge_relay : 1;
        };
        uint16_t all;
    } relay_intended_state;

    union {
        struct {
            bool ams_ok : 1;
            bool imd_ok : 1;
            bool imd_latch : 1;
            bool ams_latch : 1;
            bool air_positive : 1;
            bool air_negative : 1;
            bool precharge : 1;
            bool discharge : 1;
            bool sc_dcu_imd : 1;
            bool sc_imd_ams : 1;
            bool sc_ams_dcu : 1;
            bool sc_tsms_relays : 1;
            bool shutdown_above_minimum : 1;
            bool shutdown_open : 1;
        };
        uint16_t status;
    };

    MASTER_MSG_TS_OFF_Reason ams_off_reason;
    //bool ams_reasons_bitfield :

    Settables settable;

    MasterState master_state;
    /*
     * Missing structures for master PCB and HV monitor PCB
     */

    uint16_t dcdc_temp[2];

} Battery;

/**
 * @brief Initializes a battery structure
 */

void battery_init(Battery* battery);
void set_ams_ok(Battery* battery, bool intended_ams_ok, MASTER_MSG_TS_OFF_Reason reason);
bool check_ts_on();

#endif /* End include guard */
