/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*! \file cell.h
 *	\brief File with specifications from different cells used by FST Lisboa
 *  
 *	**Author/s:** Miguel Lourenço.
 */

#ifndef _CELL_
#define _CELL_

#include "bms_version.h"

/** 
 * \brief List of cells used by FST Lisboa in HV batteries
 *  
 * SLPB8763124 (used in 07e, 08e, 09e).
 * SLPB9956117 (used in 10e).
 */



/****************************************************************
 * @name Melasta SLPB8763124
 * @{ 
 */

#if defined(B4V2) || defined(B5V2)

#define CELL_CAPACITY 7500 /**< Nominal capacity, [mAh] **/

/// Voltages 
#define CELL_NOM_VOLTAGE 37000 /**< [mV*0.1] */
#define CELL_MIN_VOLTAGE 30000 /**< [mV*0.1] */
#define CELL_MAX_VOLTAGE 42000 /**< [mV*0.1] */

/// Charge current limits
#define CELL_MAX_I_CHRG_CONT (-150) 	/**< [mA*100] */
#define CELL_MAX_I_CHRG_PEAK (-300) 	/**< [mA*100] */
#define CELL_MAX_I_CHRG_PEAK_TIME 1000 	/* < [ms] */ 		

/// Charge temperature limits
#define CELL_CHRG_MAX_T 450  /** [ºC*0.1] **/
#define CELL_CHRG_MIN_T 0	 /** [ºC*0.1] **/


/// Discharge current limits
#define CELL_MAX_I_DISCH_CONT (1125) 	/**< [mA*100] */
#define CELL_MAX_I_DISCH_PEAK (1500) 	/**< [mA*100] */
#define CELL_MAX_I_DISCH_PEAK_TIME 3000 /**< [ms] */

/// Discharge temperature limits
#define CELL_DISCH_MAX_T 600  /** [ºC*0.1] **/
#define CELL_DISCH_MIN_T (-200) /** [ºC*0.1] **/

#endif


/****************************************************************
 * @name Melasta SLPB9956117
 * @{ 
 */

#ifdef B6V1

#define CELL_CAPACITY 7400 /**< Nominal capacity, [mAh] **/

/// Voltages 
#define CELL_NOM_VOLTAGE 37000 /**< [mV*0.1] **/
#define CELL_MIN_VOLTAGE 30000 /**< [mV*0.1] **/
#define CELL_MAX_VOLTAGE 42000 /**< [mV*0.1] **/

/// Charge current limits
#define CELL_MAX_I_CHRG_CONT (-148) 	/**< [mA*100] */
#define CELL_MAX_I_CHRG_PEAK (-296) 	/**< [mA*100] */
#define CELL_MAX_I_CHRG_PEAK_TIME 1000 	/**< [ms] */

/// Charge temperature limits
#define CELL_CHRG_MAX_T 450  /**< [ºC*0.1] **/
#define CELL_CHRG_MIN_T 0	 /**< [ºC*0.1] **/


/// Discharge current limits
#define CELL_MAX_I_DISCH_CONT (1111) 	/**< [mA*100] */
#define CELL_MAX_I_DISCH_PEAK (1480) 	/**< [mA*100] */
#define CELL_MAX_I_DISCH_PEAK_TIME 3000 /**< [ms] */

/// Discharge temperature limits
#define CELL_DISCH_MAX_T 600  	/**< [ºC*0.1] **/
#define CELL_DISCH_MIN_T (-200) /**< [ºC*0.1] **/

#endif


#endif
