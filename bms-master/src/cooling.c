/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cooling.h"
#include "signals.h"

void perform_cooling_algo(void)
{

    return;
}

void compute_cell_losses(void)
{

    return;
}

void compute_battery_losses(void)
{

    return;
}

void fan_setup(void)
{

    return;
}

#ifndef FAKE
void fan_actuation(void)
{

    FansR_Control = 1;
    FansF_Control = 1;

    return;
}
#endif

#ifdef FAKE
void fan_actuation(void)
{
    return;
}

#endif
