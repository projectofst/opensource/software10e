/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "temperature.h"
#include "bat_cfg.h"
#include "lib_pic33e/utils.h"
#include <stdbool.h>

/**
 * @brief Checks the validity of an NTC_connection
 *
 * @retval true  NTC connection is faulty
 * @retval false NTC connection is OK
 */
bool NTC_is_faulty(uint16_t voltage)
{
    // connection is open or shorted to V > MAX_VALID_TEMP_VOLT
    if (voltage > MAX_VALID_TEMP_VOLT) {
        return true;

        // connection is shorted to ground or to V < MIN_VALID_TEMP_VOLT
    } else if (voltage < MIN_VALID_TEMP_VOLT) {
        return true;
    } else {
        return false;
    }
}

/**
 * @brief Returns true if a NTC in a slave's fault mask is valid
 */
bool NTC_is_valid(uint16_t temp_fault_mask, uint16_t index)
{
    return (((temp_fault_mask >> index) & 1) == 0);
}

/**
 * @brief Returns the number of valid NTC in a stack based on its
 * temp_fault_mask
 */
uint16_t count_valid_cell_NTC_in_stack(uint16_t temp_fault_mask)
{

    // uint16_t mask = BATTERY_SERIES_CELLS_PER_STACK;

    uint16_t mask = 0x0000;
    uint16_t i = 0;

    for (i = 0; i < BATTERY_SERIES_CELLS_PER_STACK; i++) {

        mask = mask | (1 << i);
    }

    return popcount((~temp_fault_mask) & mask);
}
