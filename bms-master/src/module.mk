MODULE_C_SOURCES:=battery.c
MODULE_C_SOURCES+=battery_monitor.c
MODULE_C_SOURCES+=communication.c
MODULE_C_SOURCES+=cooling.c
MODULE_C_SOURCES+=fcp.c
MODULE_C_SOURCES+=main.c
MODULE_C_SOURCES+=ntc_curve.c
MODULE_C_SOURCES+=signals.c
MODULE_C_SOURCES+=temperature.c
MODULE_C_SOURCES+=LTC/LTC68xx_com.c
MODULE_C_SOURCES+=LTC/PEC.c
MODULE_C_SOURCES+=energy_meter/SoC.c
MODULE_C_SOURCES+=energy_meter/energy_meter.c
MODULE_C_SOURCES+=slave/slave.c


PWD:=src/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
