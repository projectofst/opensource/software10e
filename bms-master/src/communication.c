/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "communication.h"
#include "battery.h"
#include "can-ids-v2/can_cfg.h"
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/flash.h"
#include "shared/fifo/fifo.h"
#include "signals.h"
#include "ts_turn_on.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

can_t global_can;
extern Fifo* can_fifo;
extern TS_TURN_ON ts_turn_on_state;

can_t* get_can()
{
    return &global_can;
}

bool filter_can1(unsigned sid) //! ???? tf is this
{
    uint16_t dev_id = (sid)&0b11111;
    uint16_t msg_id = ((sid) >> 5) & 0b111111;

    if ((msg_id == 27) && (msg_id == 28))
        if (dev_id == DEVICE_ID_DASH)
            return false;

    return true;
}

/*
 * Non-verbose messages
 */
#if 0
CANdata compose_master_ts_message(bool state,
    MASTER_MSG_TS_OFF_Reason off_reason)
{
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_MASTER;
    msg.msg_id = MSG_ID_MASTER_TS_STATE;
    msg.dlc = 2;
    msg.data[0] = ((off_reason << 1) | (state & 0b1));

    return msg;
}
#endif

void update_master_energy_meter_message(const Battery* battery)
{
    can_t* can = get_can();

    can->master.master_em.master_em_voltage = battery->energy_meter.battery_voltage;
    can->master.master_em.master_em_current = (int16_t)battery->energy_meter.current;
    can->master.master_em.master_em_power = (int16_t)battery->energy_meter.power * 100;
    can->master.master_em.master_em_soc = battery->energy_meter.state_of_charge;

    return;
}

void update_cell_voltage_info_message(const Battery* battery)
{
    can_t* can = get_can();

    can->master.cell_v_info.cell_min_v = battery->min_voltage;
    can->master.cell_v_info.cell_mean_v = battery->mean_voltage;
    can->master.cell_v_info.cell_max_v = battery->max_voltage;
    can->master.cell_v_info.bal_trg_stack = battery->min_voltage_cell[0];
    can->master.cell_v_info.bal_trg_cell = battery->min_voltage_cell[1];

    return;
}

void update_cell_temperature_info_message(const Battery* battery)
{
    can_t* can = get_can();

    can->master.cell_tmp_info.cell_min_tmp = battery->min_temperature;
    can->master.cell_tmp_info.cell_mean_tmp = battery->mean_temperature;
    can->master.cell_tmp_info.cell_max_tmp = battery->max_temperature;
    can->master.cell_tmp_info.hottest_cell_stack = battery->max_temperature_cell[0];
    can->master.cell_tmp_info.hottest_cell = battery->max_temperature_cell[1];

    return;
}

/* Master status message is composed elsewhere put it here */

void update_master_imd_error_message()
{

    return;
}

void update_master_ams_error_message()
{

    return;
}

void update_master_sc_open_message()
{

    return;
}

/*
 * Verbose messages
 */
void update_cell_info_message(const Cell* cell)
{
    can_t* can = get_can();

    can->master.cell_verb_info.verb_cell_v[cell->id] = cell->voltage;
    can->master.cell_verb_info.verb_cell_tmp[cell->id] = (uint16_t)cell->temperature;
    can->master.cell_verb_info.verb_cell_soc[cell->id] = cell->state_of_charge; // calc every SOC

    return;
}

void update_slave_info_1_message(const Stack* stack)
{
    can_t* can = get_can();

    can->master.slave_verb_info1.slave_verb_dcfm[stack->id] = stack->slave.discharge_channel_fault_mask;
    can->master.slave_verb_info1.slave_verb_owc[stack->id] = stack->slave.OWC_cell_status;
    can->master.slave_verb_info1.slave_verb_tfm[stack->id] = stack->slave.temp_fault_mask;

    return;
}

void update_slave_info_2_message(const Stack* stack)
{
    can_t* can = get_can();

    can->master.slave_verb_info2.slave_verb_dcs[stack->id] = stack->slave.discharge_channel_state;
    can->master.slave_verb_info2.slave_verb_dct[stack->id] = stack->slave.disch_temp[0];
    can->master.slave_verb_info2.slave_verb_bmt[stack->id] = stack->slave.BM_temperature;

    return;
}

#if 0
CANdata compose_slave_info_3_message(const Stack* stack)
{
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_VERBOSE;
    msg.msg_id = MSG_ID_MASTER_SLAVE_INFO_3;
    msg.dlc = 8;
    msg.data[0] = stack->id;
    msg.data[1] = stack->slave.slave_temperature[0];
    msg.data[2] = stack->slave.slave_temperature[1];
    msg.data[3] = stack->slave.BM_temperature;

    return msg;
}

/* Debug message */
CANdata compose_slave_info_4_message(const Stack* stack){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_VERBOSE;
    msg.msg_id = MSG_ID_MASTER_SLAVE_INFO_4;
    msg.dlc = 8;
    msg.data[0] = stack->id;
    msg.data[1] = stack->n_valid_cell_NTC;
    msg.data[2] = stack->total_voltage;
    msg.data[3] = stack->mean_temperature;

    return msg;
}
#endif

void send_verbose_messages(const Battery* battery)
{
    uint16_t slave, cell; // iterators
    can_t* can = get_can();

    if (battery->master_state.verbose) {
        for (slave = 0; slave < BATTERY_SERIES_STACKS; ++slave) {
            update_slave_info_1_message(&battery->stacks[slave]);
            // append_fifo(can_fifo, encode_slave_verb_info1(can->master.slave_verb_info1, slave));
            send_can1(encode_slave_verb_info1(can->master.slave_verb_info1, slave));
            update_slave_info_2_message(&battery->stacks[slave]);
            // append_fifo(can_fifo, encode_slave_verb_info2(can->master.slave_verb_info2, slave));
            send_can1(encode_slave_verb_info2(can->master.slave_verb_info2, slave));
#if 0
            msg = compose_slave_info_3_message(&battery->stacks[slave]);
            append_fifo(can_fifo, msg);
            msg = compose_slave_info_4_message(&battery->stacks[slave]);
            append_fifo(can_fifo, msg);
#endif

            for (cell = 0; cell < BATTERY_SERIES_CELLS_PER_STACK; ++cell) {
                update_cell_info_message(&(battery->stacks[slave].cell_pack[cell]));
                // fifo_append(can_fifo, encode_cell_verb_info(can->master.cell_verb_info, slave * BATTERY_SERIES_CELLS_PER_STACK + cell));
                send_can1(encode_cell_verb_info(can->master.cell_verb_info, slave * BATTERY_SERIES_CELLS_PER_STACK + cell));
            }
        }
    }

    return;
}

void battery_info_messages(const Battery* battery)
{

    update_master_energy_meter_message(battery);
    update_cell_voltage_info_message(battery);
    update_cell_temperature_info_message(battery);
    update_status_message(battery);

    return;
}

/* Build master status message */
void update_status_message(const Battery* battery)
{
    can_t* can = get_can();

    can->master.master_status.master_status_ams_ok = battery->ams_ok;
    can->master.master_status.master_status_imd_ok = battery->imd_ok;
    can->master.master_status.master_status_imd_latch = battery->imd_latch;
    can->master.master_status.master_status_ams_latch = battery->ams_latch;
    can->master.master_status.master_status_air_p = battery->air_positive;
    can->master.master_status.master_status_air_n = battery->air_negative;
    can->master.master_status.master_status_precharge = battery->precharge;
    can->master.master_status.master_status_discharge = battery->discharge;
    can->master.master_status.master_status_sdc1 = battery->sc_dcu_imd;
    can->master.master_status.master_status_sdc2 = battery->sc_imd_ams;
    can->master.master_status.master_status_sdc3 = battery->sc_ams_dcu;
    can->master.master_status.master_status_tsms_relays = battery->sc_tsms_relays;
    can->master.master_status.master_status_sdc_above = battery->shutdown_above_minimum;
    can->master.master_status.master_status_sdc_open = battery->shutdown_open;
    can->master.master_status.master_status_ts_on = check_ts_on();
    can->master.master_status.master_status_verbose = battery->master_state.verbose;
    can->master.master_status.master_pes = get_PreCh_End_Sig();
    can->master.master_status.bat_id = 103;

    return;
}

void send_ts_state(const Battery* battery)
{

    if (ts_turn_on_state == TS_ERROR_STATE)
        send_can1(logW(LOG_TS_STATE, 0, battery->ams_off_reason, 0));

    if (ts_turn_on_state == TS_INITIAL_STATE)
        send_can1(logW(LOG_TS_STATE, 1, battery->ams_off_reason, 0));

    return;
}

void battery_charging_info(const Battery* battery)
{

    if (battery->master_state.charging) {
        update_charging_message();
        // append_fifo(can_fifo, charging_msg);
    }

    return;
}

void update_charging_message()
{

    return;
}

/* Handles two fifos. The verbose one and the normal communication channel
 * Should not write to the CAN TX buffer more than it can handle
 */
void can_dispatch(Fifo* fifo1)
{
    CANdata* msg;

    LED5 ^= 1;
    uint16_t available = can1_tx_available();
    for (; available > 0 && !fifo_empty(fifo1); available--) {
        msg = fifo_pop(fifo1);
        if (msg == NULL) {
            /* Should never happen */
            break;
            ;
        }
        send_can1(*msg);
    }

    return;
}

void save_energy_meter_data(Battery* battery, can_t* can, uint16_t msg_id)
{

    if (msg_id == MSG_ID_ISABEL_ISA_CURRENT) {
        battery->energy_meter.isa_wtd = 0;
        battery->energy_meter.current = (int32_t)can->isabel.isa_current.isa_current / 100;
    }

    battery->energy_meter.battery_voltage = can->isabel.isa_voltage_1.isa_voltage_1 / 10;
    battery->energy_meter.fuse_voltage = can->isabel.isa_voltage_2.isa_voltage_2 / 10;
    battery->energy_meter.output_voltage = can->isabel.isa_voltage_3.isa_voltage_3 / 10;
    battery->energy_meter.power = can->isabel.isa_power.isa_power / 1000;
    battery->energy_meter.charge = can->isabel.isa_charge.isa_charge;
    battery->energy_meter.energy = can->isabel.isa_energy.isa_energy;

    return;
}

void can_receive_handler(Battery* battery)
{
    CANdata msg;
    can_t* can = get_can();

    if (can1_rx_empty()) {
        return;
    }

    msg = pop_can1();

    uint16_t msg_id = msg.sid >> 5;
    uint16_t dev_id = msg.sid & 0x1F;
    // printf("dev_id %d\n", dev_id);
    // printf("msg_id %d\n", msg_id);
    if (msg_id == 18) {
        printf("msg.msg_id: %d\n", msg.sid >> 5);
    }

    decode_can(msg, can);
    cfg_dispatch(msg);
    cmd_dispatch(msg);

    if (msg.dev_id == DEVICE_ID_ISABEL) {
        save_energy_meter_data(battery, can, msg.msg_id);
    }

    return;
}
