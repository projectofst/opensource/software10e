/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* Include Guard */
#ifndef _BATTERY_MONITOR_H_
#define _BATTERY_MONITOR_H_

#include "bms_version.h"

#include "battery.h"

typedef enum {
    ERROR_START,
    ERROR_WAITING,
    ERROR_CONFIRM,
    ERROR_DONE
} ErrorState;

#ifdef B4V2
#define ERROR_CHECKS 18
#endif

#ifdef B5V2
#define ERROR_CHECKS 19
#endif

#ifdef B6V1
#define ERROR_CHECKS 17
#endif

extern uint16_t error_states[ERROR_CHECKS];
extern uint16_t error_counter_end[ERROR_CHECKS];
extern uint16_t error_period_table[ERROR_CHECKS];
extern uint16_t error_counter[ERROR_CHECKS];

#define VOLT_CONV_STATE 0
#define TEMP_CONV_STATE 1
#define GET_TEMP_STATE 2
#define OWC_CONV_STATE1 3
#define OWC_CONV_STATE2 4
#define MON_FINAL_STATE 5



void check_isa_watchdog(Battery* battery);
/**
 * @brief Change temperature mux address of all slaves to the passed address.
 */
void control_temp_mux_address(Battery* battery, uint16_t address);

/**
 * @brief Commands the LTC6811 to start converting the cells voltages
 */
void start_cell_voltages_conversion(Battery* battery);
/**
 * @brief Reads all cell voltages from the slaves and writes it on the battery
 * struct
 */

void start_cell_open_wire_check_conversion(Battery* battery, uint16_t PUP);

/**
 * @brief Check battery limits. Set amk_ok depending on error state.
 * @param battery - battery struct
 */
void check_battery_limits(Battery* battery);


void battery_monitor_routine(Battery* battery);

/**
 * @brief Initializes the battery's daisy chain B struct
 */
void daisy_chain_B_init(LTC_daisy_chain* daisy_chain_B);

#endif /* End include guard */
