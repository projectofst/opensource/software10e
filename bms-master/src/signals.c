/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>

#include "lib_pic33e/pps.h"

#include "signals.h"

void do_the_pps(void)
{
    PPSUnLock;

/* CAN 1 */
#ifdef TESTING
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
#endif

#ifndef TESTING
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP65);
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI62);
#endif

    /* SPI 1 for Energy Meter */
    PPSInput(IN_FN_PPS_SDI1, IN_PIN_PPS_RP66);
    PPSOutput(OUT_FN_PPS_SCK1, OUT_PIN_PPS_RP67);
    PPSInput(IN_FN_PPS_SCK1, IN_PIN_PPS_RP67);

    /* Output Compare for Fan Control */
    /* Unmappable
     * PPSOutput(OUT_FN_PPS_OC1, OUT_PIN_PPS_RPI81);
     * PPSOutput(OUT_FN_PPS_OC2, OUT_PIN_PPS_RPI83);
     */

    /* Secundary SPI module */
    PPSInput(IN_FN_PPS_SDI3, IN_PIN_PPS_RP101);
    PPSOutput(OUT_FN_PPS_SDO3, OUT_PIN_PPS_RP99);
    PPSOutput(OUT_FN_PPS_SCK3, OUT_PIN_PPS_RP100);
    PPSInput(IN_FN_PPS_SCK3, IN_PIN_PPS_RP100);

    /* Shutdown openning sig internal interrupt */
    PPSInput(IN_FN_PPS_INT1, IN_PIN_PPS_RP64);

    /* IMD ok signal interrupt */
    PPSInput(IN_FN_PPS_INT2, IN_PIN_PPS_RPI121);

    PPSLock;

    return;
}

#ifndef FAKE
void config_outputs(void)
{

    /* Define BMS Master pins as in/outputs */

    /* Outputs */

    TRIS_AMS_OK = BMS_OUTPUT;
    TRIS_GPIO3 = BMS_OUTPUT;
    TRIS_GPIO4 = BMS_OUTPUT;
    TRIS_CHIP_SELECT_EM = BMS_OUTPUT;
    TRIS_CONVST_EM = BMS_OUTPUT;
    TRIS_RESET_EM = BMS_OUTPUT;
    TRIS_AIR_Plus_Control = BMS_OUTPUT;
    TRIS_AIR_Minus_Control = BMS_OUTPUT;
    TRIS_PreChK_Control = BMS_OUTPUT;
    TRIS_DisChK_Control = BMS_OUTPUT;
    TRIS_FansR_Control = BMS_OUTPUT;
    TRIS_FansF_Control = BMS_OUTPUT;
    TRIS_BMS10e_DEBUG_LED = BMS_OUTPUT;

    /* Inputs */

    TRIS_GPIO0 = BMS_INPUT;
    TRIS_GPIO1 = BMS_INPUT;
    TRIS_GPIO2 = BMS_INPUT;
    TRIS_LED3 = BMS_INPUT;
    TRIS_LED4 = BMS_INPUT;
    TRIS_LED5 = BMS_INPUT;
    TRIS_Det_IMD_latch = BMS_INPUT;
    TRIS_Det_AMS_latch = BMS_INPUT;
    TRIS_HV_Present_Sig = BMS_INPUT;
    TRIS_Det_AIR_Plus = BMS_INPUT;
    TRIS_Det_AIR_Minus = BMS_INPUT;
    TRIS_Det_PreChK = BMS_INPUT;
    TRIS_Det_DisChK = BMS_INPUT;
    TRIS_PreCh_End_Sig = BMS_INPUT;
    TRIS_Shutdown_Opening_Sig = BMS_INPUT;
    TRIS_BUSY_EM = BMS_INPUT;
    TRIS_Shutdown_Above_Min = BMS_INPUT;
    TRIS_Det_Shutdown_IMD_AMS = BMS_INPUT;
    TRIS_Det_Shutdown_DCU_IMD = BMS_INPUT;
    TRIS_Det_Shutdown_TSMS_Relays = BMS_INPUT;
    TRIS_Det_Shutdown_AMS_DCU = BMS_INPUT;
    TRIS_Det_IMD_OK = BMS_INPUT;

    /* Set output pin's status */

    AMS_OK = 0;
    GPIO3 = 0;
    GPIO4 = 0;
    AIR_Plus_Control = 0;
    AIR_Minus_Control = 0;
    PreChK_Control = 0;
    DisChK_Control = 0;
    FansR_Control = 0;
    FansF_Control = 0;
    BMS10e_DEBUG_LED = 1;
    /*
        GPIO0							= 0;
        GPIO1							= 0;
        GPIO2							= 0;
        LED3							= 0;
        LED4							= 0;
        LED5							= 0;
        */

    return;
}
#endif

#ifdef FAKE

void config_outputs(void)
{
    return;
}
#endif

#ifndef FAKE

bool get_Det_PreChK()
{
    return Det_PreChK;
}

bool get_PreCh_End_Sig()
{
    return PreCh_End_Sig;
}
void set_PreChK_Control(bool value)
{
    PreChK_Control = value;
}
bool get_Det_DisChK()
{
    return Det_DisChK;
}
void set_DisChK_Control(bool value)
{
    DisChK_Control = value;
}
bool get_Det_AIR_Plus()
{
    return Det_AIR_Plus;
}
void set_AIR_Plus_Control(bool value)
{
    AIR_Plus_Control = value;
}
bool get_Det_AIR_Minus()
{
    return Det_AIR_Minus;
}
void set_AIR_Minus_Control(bool value)
{
    AIR_Minus_Control = value;
}
bool get_HV_Present_Sig()
{
    return 1;
}

void set_AMS_OK(bool value)
{
    AMS_OK = value;
}

void set_CS_B(bool value)
{
    CS_B = value;
}

#else
bool _PreChK = 0;
bool _DisChK = 1;
bool _PreCh_End_Sig = 1;
bool _AIR_Plus = 0;
bool _AIR_Minus = 0;
bool _AMS_OK = 0;
bool _CS_B = 0;

bool get_Det_PreChK()
{
    return _PreChK;
}

bool get_PreCh_End_Sig()
{
    return _PreCh_End_Sig;
}
void set_PreChK_Control(bool value)
{
    _PreChK = !value;
}
bool get_Det_DisChK()
{
    return _DisChK;
}
void set_DisChK_Control(bool value)
{
    _DisChK = value;
}
bool get_Det_AIR_Plus()
{
    return _AIR_Plus;
}
void set_AIR_Plus_Control(bool value)
{
    _AIR_Plus = !value;
}
bool get_Det_AIR_Minus()
{
    return _AIR_Minus;
}
void set_AIR_Minus_Control(bool value)
{
    _AIR_Minus = !value;
}

void set_AMS_OK(bool value)
{
    _AMS_OK = value;
}

void set_CS_B(bool value)
{
    _CS_B = value;
}
#endif
