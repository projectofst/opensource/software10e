/* 
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __TS_TURN_ON
#define __TS_TURN_ON

typedef enum { 
	TS_WAIT_FOR_COMMAND,
    TS_INITIAL_STATE,
    TS_WAIT_FOR_PRECHARGE,
    TS_END_STATE,
    TS_ERROR_STATE 
} TS_TURN_ON;

#endif
