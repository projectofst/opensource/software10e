/*
 * bms-master is a battery management software used in the batteries of Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>

#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"

#include "lib_pic33e/flash.h"

#include "shared/fifo/fifo.h"

#include "battery.h"
#include "signals.h"

extern Fifo* can_fifo;
extern Battery battery;

extern uint16_t flash[CFG_MASTER_SIZE];

uint16_t dev_get_id()
{
    return DEVICE_ID_MASTER;
}

float arg1_float;

void dev_send_msg(CANdata msg)
{
    // append_fifo(can_fifo, msg);
    send_can1(msg);

    return;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{

    multiple_return_t arg = { 0 };

    if (id == CMD_MASTER_SAVE_FLASH) {
        write_flash(flash, 0, CFG_MASTER_SIZE);
    } else if (id == CMD_MASTER_TOGGLE_VERBOSE) {
        // LED3 ^= 1;
        battery.master_state.verbose ^= 1;
    } else if (id == CMD_MASTER_TOGGLE_FANS) {
        battery.master_state.fans ^= 1;
    } else if (id == CMD_MASTER_TOGGLE_CHARGE) {
        battery.master_state.charging ^= 1;
    } else if (id == CMD_MASTER_FAKE_ERROR) {
        battery.ams_ok = 0;
        set_AMS_OK(0);
    } else if (id == CMD_MASTER_RESET_ERROR) {
        set_AMS_OK(1);
        // TODO: should check the status of amk_ok
        if (1) {
            battery.ams_ok = 1;
        }
    } else if (id == CMD_MASTER_RECALIBRATE_SOC) {
        if ((arg1 == 1) && (arg2 >= 0) && (arg2 <= 100)) {
            battery.energy_meter.forced_soc = arg2;
            battery.energy_meter.soc_calibrated_forced = 1;
            battery.energy_meter.soc_calibrated = 0;
        } else if (arg1 == 0) {
            battery.energy_meter.soc_calibrated = 0;
        }
    } else if (id == CMD_MASTER_SET_PWM_FAN_SIDE) {
        if (arg1 > 0 && arg1 < 100) {
            arg1_float = arg1 / 100;
            pwm1_update_duty_cycle(arg1_float);
        }
    } else if (id == CMD_MASTER_COMMON_TS) {

        if (arg1 == 0) {
            battery.master_state.car_ts ^= 1;
            arg.arg1 = 1; // For debug
        }
    } else if (id == CMD_MASTER_GET_CELL_CONFIG) {
        arg.arg1 = BATTERY_SERIES_STACKS;
        arg.arg2 = BATTERY_SERIES_CELLS_PER_STACK;
    }

    return arg;
}

void cfg_set_callback(uint32_t* table)
{
    uint16_t i;

    for (i = 0; i < CFG_MASTER_SIZE; i++)
        flash[i] = table[i];

    return;
}
