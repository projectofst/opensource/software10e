ARCH:=zybo/
OBJDIR := $(G_OBJDIR)$(ARCH)$(APP)/
-include build/objects.mk

CC:=arm-linux-gnueabihf-gcc

CFLAGS += $(OPT)
INCLUDES := -Ilib/$(HAL)_$(ARCH) -Isrc -I. $(CPPFLAGS)

$(BINDIR)$(PROGNAME)-$(APP).out: $(LINUX_OBJECTS)
	@mkdir -p $(BINDIR)
	@printf "\033[1;32m >> fake.out \033[0m \n"
	@$(CC) $(CFLAGS) $(CUSTOM_C_FLAGS) $(INCLUDES) -DFAKE -pthread -o $@ $^ -lm

$(OBJDIR)%.o: %.c
	@ mkdir -p `dirname $@`
	@ printf "\033[1;32m >> \033[1;34m $@ \033[0m \n"
	@$(CC) -c $(CFLAGS) $(CUSTOM_C_FLAGS) $(INCLUDES) -DFAKE -pthread -o $@ $<
