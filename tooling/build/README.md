# Build System

Date : 2020-06-27

## Intro

The build system for microcontroller projects of FST Lisboa is documented
here.

At the moment the build system is based on GNU Make files. 

Diagram of makefile organization for a project.
```
.
├── apps
│   ├── v1-08e-old
│   │   └── module.mk
│   └── v2-010e-raw
│       └── module.mk
├── bin
├── build -> ../tooling/build
├── lib
│   ├── can-ids-v1 -> ../../lib/can-ids
│   │   └── module.mk
│   ├── can-ids-v2 -> ../../lib/can-ids-spec/out
│   │   └── module.mk
│   ├── lib_pic30f_linux -> ../../lib/hal/lib_pic30f_linux
│   │   ├── module.mk
│   ├── lib_pic30f_xc16 -> ../../lib/hal/lib_pic30f_xc16
│   │   └── module.mk
│   ├── lib_pic33e_linux -> ../../lib/hal/lib_pic33e_linux
│   │   ├── module.mk
│   ├── lib_pic33e_xc16 -> ../../lib/hal/lib_pic33e_xc16
│   │   ├── module.mk
│   └── shared -> ../../lib/shared
│       ├── amk
│       │   └── module.mk
│       ├── fifo
│       │   ├── module.mk
│       ├── rtd
│       │   └── module.mk
│       ├── scheduler
│       │   ├── module.mk
│       ├── tlc5947
│       │   └── module.mk
│       └── watchdog
│           └── module.mk
├── Makefile
└── src
    └── module.mk
```
* A project makefile allows the setting of project specific build parameters
* A build folder symlinked to the global tooling/build provides common
	definitions for the build system
* Every directory that can be used as source code provides a module.mk listing
all source files.
* Not included in the diagram but of some usefulness is the
	[Makefile](../../Makefile) at the
	root of the repo that can run builds for selected projects.


If you want to know how to use the build system read [Invocation](#Invocation)
, [Configuration](#Configuration), [Apps](#Apps) and [Includes](#Includes).

To learn how to change the build system read the rest of the document as well
as the source Make documents. Always remember that if something is not well
explained you should try to improve it so that the next person can have an
easier time.


## Invocation

The project can be compiled with default settings with `make`.
This will produce a binary in the bin directory, see [Configuration](#Configuration).

`make verbose` compiles exactly like `make` but displays a memory layout
report.

`make test` compiles and runs unit tests for the project. See
[TESTING](../../TESTING.md).

`make docs` is used to generate documentation using
[Doxygen](https://www.doxygen.nl/index.html) and
[m.css](https://blog.magnum.graphics/meta/improved-doxygen-documentation-and-search/).

`make clang-format` formats the source code using
[clang-format](https://clang.llvm.org/docs/ClangFormat.html). This ensures a
consistent style in the code base.

`make clang-format-check` checks that files are formatted.

`make clean` cleans the repository. Use this before `git add` to guarantee that
no unwanted files are pushed to upstream.


## Configuration

The project can be configured in the corresponding Makefile. The following
setting are available:

### BINDIR 

Directory for binaries that are the output of the compilation process

### OBJDIR 

Directory for object files.

### LIBDIR 

Directory for libraries.

### PROGNAME
Modifies the generated binary name:

* `$(PROGNAME).out` for linux
* `$(PROGNAME.hex` for dspic33ep/dspic30f

### APP
Selects the APP to be compiled. See [Apps](#Apps).

### MCU
Select the MCU: 30F6012A/33EP256MU806. [XC16 Compiler
Manual](http://ww1.microchip.com/downloads/en/DeviceDoc/MPLAB%20XC16%20C%20Compiler%20Users%20Guide%20DS50002071H.pdf#page=96).

### OPT
Select optimization level. [GCC Optmize
Options](https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html).

You can also use this field to set debug flags.

Ex: `-O0 -g`.


Variables can also be set from the command line: `make APP=example_app
TARGET=linux`.

## Modules

The build system uses a concept we call modules to define the files to be
compiled.

Example module.mk:
```
-include build/module-lib.mk

MODULE_C_SOURCES := check_functions.c
MODULE_C_SOURCES += config_functions.c
MODULE_C_SOURCES += general_purpose_functions.c
MODULE_C_SOURCES += main.c
MODULE_C_SOURCES += send_functions.c
MODULE_C_SOURCES += update_functions.c

PWD:=apps/v1-08e-old/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))
```

Each module must contain a module.mk and some source files. 

* Modules are automatically added to the include dir. 
* In the module.mk source files are manually listed
* Makefile [settings](#Configuration) can be set in the module.mk.
* Other modules can be included inside a module.mk, example:

```
include lib/shared/fifo/module.mk
MODULES += lib/shared/fifo
```

### Uses of modules

#### Apps

With modules we can select the high level algorithms that are to be compiled.
to this concept we give the name Apps.

Usually each App is in a directory named with the App name, this directory is
itself inside the apps directory. To setup an App using modules follow the next
example in the project Makefile:

```
APP := v1-08e-old
MODULES:=apps/$(APP)
```

Now when compiling the `v1-08-old` app will be used. It is also possible to set
the app from the command line with `make APP=some_other_app`.

#### BSP

With modules we can select the board support package that are to be compiled.
to this concept we give the name BSP.

Usually each BSP is in a directory named with the BSP name, this directory is
itself inside the bsp directory. To setup an BSP using modules follow the next
example in the project Makefile:

```
BSP := s2c
MODULES:=bsp/$(BSP)
```

Now when compiling the `s2c` bsp will be used. It is also possible to set
the bsp from the command line with `make BSP=some_other_bsp`.


## `software10e/tooling/build` directory overview

	arch_linux.mk   - linux specific rules
	arch_xc16.mk    - pic specific rules
	docs.mk         - documentation generation
	format.mk       - auto formatting with clang-format
	ftest.mk        - rules to run ftest tests.
	git.mk          - git rules, commit hash versioning
	modules.mk      - helper for module system
	objects.mk      - rule to create objects from SOURCE list
	os.mk           - Operating System specific rules
	warning.mk      - warning flags for clang/gcc
	global.mk       - global rules


Link to the [build](.) directory.
