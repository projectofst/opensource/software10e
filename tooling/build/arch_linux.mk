PROJECT_OBJDIR:=$(ARCH_OBJDIR)/$(PROJECT_PATH)

ARCH_OBJECTS:=$(addprefix $(PROJECT_OBJDIR)/, $(MODULE_C_SOURCES:.c=.o))
ARCH_OBJECTS+=$(addprefix $(PROJECT_OBJDIR)/, $(MODULE_S_SOURCES:.s=.o))

$(PROJECT_OBJDIR)/%.o: %.c
	@mkdir -p `dirname $@`
	@printf "\033[1;32m >> \033[1;34m $@ \033[0m \n"
	@$(CC) -c $(CFLAGS) $(CUSTOM_C_FLAGS) $(INCLUDES) -DLINUX -pthread -o $@ $<

all: $(ARCH_OBJECTS)

.DEFAULT_GOAL := all
