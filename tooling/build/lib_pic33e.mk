hal:
	@echo "Building lib_pic33e"
	@rm -rf  lib/lib_pic*
	@cd lib; ln -s ../../lib/hal/lib_pic33e lib_pic33e
	@cd lib; ln -s ../../lib/hal/lib_pic33e_linux lib_pic33e_linux
	@cd lib; ln -s ../../lib/hal/lib_pic33e_xc16 lib_pic33e_xc16
