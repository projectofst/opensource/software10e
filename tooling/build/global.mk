# Directories
BINDIR := $(if $(BINDIR),$(BINDIR),bin/)
LIBDIR := $(if $(LIBDIR),$(LIBDIR),lib/)
OBJDIR := $(if $(OBJDIR),$(OBJDIR),~/.cache/fst/obj/$(PROGNAME))

# Include the LIBDIR and the APP directories.
CPPFLAGS += -I$(LIBDIR) -Ilib/shared
CPPFLAGS += -MMD -MP

.PHONY: init verbose all flash clean test format fake hal arch target $(MODULES)
.DEFAULT_GOAL := all

MONOREPO_ROOT:=$(abspath $(MONOREPO_ROOT))
PROJECT_ROOT:=$(abspath .)

C_SOURCES:=
S_SOURCES:=
#MODULES+=lib/$(HAL)_$(TARGET)
#include build/modules.mk

# os specific definitions
-include build/os.mk

# git hash
-include build/git.mk

# documentation generation
-include build/docs.mk

# source code formatter
-include build/format.mk

# selects the corresponding target architecture compilation rules
ifeq ($(TARGET),linux)
	-include build/module_linux.mk
else ifeq ($(TARGET), xc16)
	-include build/module_xc16.mk
else
	-include build/arch_error.mk	
endif

ifeq ($(CAN-IDS),v2)
	-include build/can-ids-v2.mk
else
	-include build/can-ids-default.mk	
endif

-include build/ftest.mk

test:
	@make -C test

list-targets:
	@echo $(MODULES)

list-apps:
	@echo $(shell ls apps)

verbose:
	VERBOSE=-Wl,--report-mem make

export

$(MODULES):
	$(MAKE) -C $@ -f module.mk PROJECT_PATH=$@

all: can-ids $(MODULES)
	make arch

clean: clean-docs clean-can-ids
	@printf "Cleaning \033[1;31m $$(pwd) \033[0m \n"
	@rm -rf $(OBJDIR) $(BINDIR) || true
	touch .dummy

-include $(OBJECTS:.o=.d)

# This rebuilds everything if the Makefile was modified
# http://stackoverflow.com/questions/3871444/making-all-rules-depend-on-the-makefile-itself/3892826#3892826
-include .dummy
.dummy: Makefile
	@ touch $@
	@ $(MAKE) -s clean
