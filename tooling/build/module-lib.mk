define abs_path
   	$(foreach source, $(1), $(2)$(source))
endef

define load_module
    include $(1)module.mk
endef
