ARCH:=xc16/

ARCH_OBJDIR:=$(OBJDIR)/$(APP)/$(TARGET)

CC := xc16-gcc

CPPFLAGS += -I$(PROJECT_ROOT)/lib -I$(PROJECT_ROOT)/lib/shared
CPPFLAGS += $(addprefix -I$(PROJECT_ROOT)/,$(MODULES))

CFLAGS := -mcpu=$(MCU) -msfr-warn=on -fast-math -pipe -merrata=all -mlarge-code -mlarge-arrays -mno-eds-warn $(OPT)
LDFLAGS := -Wl,--script=p$(MCU).gld,--warn-common,-Map="$(BINDIR)$(PROGNAME)-$(APP).map"

flash:
	@/opt/microchip/mplabx/v4.05/sys/java/*/bin/java -jar /opt/microchip/mplabx/v4.05/mplab_ipe/ipecmd.jar -TPPK3 -P$(MCU) -M -F$(BINDIR)$(PROGNAME).hex
	@ rm -f log.[0-9] MPLABXLog.xml*

remote-flash:
	flash_pic local-program	$(BINDIR)$(PROGNAME).hex lv_room

$(BINDIR)$(PROGNAME)-$(TARGET)-$(APP).hex: $(BINDIR)$(PROGNAME)-$(TARGET)-$(APP).elf
	@printf "\033[1;32m >> \033[1;34m $@ \033[0m \n"
	@xc16-bin2hex $(BINDIR)$(PROGNAME)-$(TARGET)-$(APP).elf 2>&1
	@printf "\033[1;32m ✓ $(PROGNAME)-$(TARGET)-$(APP) finished \033[0m \n"

$(BINDIR)$(PROGNAME)-$(TARGET)-$(APP).elf:
	@printf "\033[1;32m >> \033[1;34m $@ \033[0m \n"
	@$(CC) $(VERBOSE) $(LDFLAGS) $(CUSTOM_C_FLAGS) -o $@ $(shell find $(ARCH_OBJDIR) -name "*.o") 2>&1 


arch:
	@printf "\033[1;34m Start \033[1;32m$(PROGNAME)\033[1;34m build\033[0m \n"
	@mkdir -p $(BINDIR) $(OBJDIR)
	@make -j$(NPROC) $(BINDIR)$(PROGNAME)-$(TARGET)-$(APP).hex
