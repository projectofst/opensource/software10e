PROJECT_OBJDIR:=$(ARCH_OBJDIR)/$(PROJECT_PATH)

ARCH_OBJECTS:=$(addprefix $(PROJECT_OBJDIR)/, $(MODULE_C_SOURCES:.c=.o))
ARCH_OBJECTS+=$(addprefix $(PROJECT_OBJDIR)/, $(MODULE_S_SOURCES:.s=.o))

#$(MODULE_OBJDIR):=$(ARCH_OBJDIR)/



$(PROJECT_OBJDIR)/%.o: %.s
	@ mkdir -p `dirname $@`
	@ printf "\033[1;32m >> \033[1;34m $@ \033[0m \n"
	@$(CC) -Wa,-a=$(@:.o=.s) -c $(CFLAGS) $(CPPFLAGS) $(CUSTOM_C_FLAGS) -o $@ $< 2>&1 | perl -pe 's/warning/\x1b[33;01mwarning\x1b[m/g'| perl -pe 's/error/\x1b[31;01merror\x1b[m/g'

$(PROJECT_OBJDIR)/%.o: %.c
	@mkdir -p `dirname $@`
	@printf "\033[1;32m >> \033[1;34m $@ \033[0m \n"
	@$(CC) -Wa,-a=$(@:.o=.s) -c $(CFLAGS) $(CPPFLAGS) $(CUSTOM_C_FLAGS) -o $@ $< 2>&1 | perl -pe 's/warning/\x1b[33;01mwarning\x1b[m/g'| perl -pe 's/error/\x1b[31;01merror\x1b[m/g'


all: $(ARCH_OBJECTS)


.DEFAULT_GOAL := all
