hal:
	@echo "Building lib_pic30f"
	@rm -rf  lib/lib_pic*
	@cd lib; ln -s ../../lib/hal/lib_pic30f lib_pic30f
	@cd lib; ln -s ../../lib/hal/lib_pic30f_linux lib_pic30f_linux
	@cd lib; ln -s ../../lib/hal/lib_pic30f_xc16 lib_pic30f_xc16
	@cd lib; ln -s ../../lib/hal/lib_pic30f_zybo lib_pic30f_zybo


