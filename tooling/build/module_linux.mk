ARCH:=linux/

ARCH_OBJDIR:=$(OBJDIR)/$(APP)/$(TARGET)

CC := clang

CFLAGS := $(OPT)

CPPFLAGS += -I$(PROJECT_ROOT)/lib -I$(PROJECT_ROOT)/lib/shared -I$(PROJECT_ROOT)/lib/$(HAL)_linux
CPPFLAGS += $(addprefix -I$(PROJECT_ROOT)/,$(MODULES))
INCLUDES := -Ilib/$(HAL)_$(ARCH) -I$(PROJECT_ROOT)/src -I$(PROJECT_ROOT) $(CPPFLAGS)


$(BINDIR)$(PROGNAME)-$(TARGET)-$(APP).out:
	@mkdir -p $(BINDIR)
	@printf "\033[1;32m >> $(PROGNAME)-$(TARGET)-$(APP).out \033[0m \n"
	@$(CC) $(CFLAGS) $(CUSTOM_C_FLAGS) $(CPPFLAGS) $(INCLUDES) -DLINUX -lm -pthread -o $@ $(shell find $(ARCH_OBJDIR) -name "*.o")

arch: 
	@printf "\033[1;34m Start \033[1;32m$(PROGNAME)\033[1;34m build\033[0m \n"
	@mkdir -p $(BINDIR) $(ARCH_OBJDIR)	
	@make -j$(NPROC) $(BINDIR)$(PROGNAME)-$(TARGET)-$(APP).out
