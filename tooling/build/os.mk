# Get number of logical cores, on Linux and on macOS.
OS := $(shell uname -s)
ifeq ($(OS),Darwin)
	NPROC := $(shell sysctl -n hw.ncpu)
else
	NPROC := $(shell nproc)
endif
