MODULE_FILES:=$(foreach module, $(MODULES), $(module)/module.mk)
include $(MODULE_FILES)

CPPFLAGS+=$(foreach module, $(MODULES), -I$(module))
