clang-format:
	@clang-format -i -style=Webkit $(C_SOURCES) || true

clang-format-check:
	@clang-format --dry-run -Werror -i -style=Webkit $(C_SOURCES)
