# git hash as C include
# same as defining the git version like this in the code #define VERSION 0xdeadbeef
GIT_VERSION := $(shell git rev-parse HEAD | head -c 8)
CPPFLAGS+= -DVERSION=0x$(GIT_VERSION)
