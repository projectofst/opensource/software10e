import click
import json
import socket
import select
import can


def parseConfigFile(config):
    with open(config) as f:
        return json.loads(f.read())


def bridge(udp_sock, vcan_sock, adress):
    while True:
        rlist, _, _ = select.select([udp_sock, vcan_sock], [], [])
        for sock in rlist:
            if sock == udp_sock:
                data, _ = udp_sock.recvfrom(1024)  # buffer size is 1024 bytes
                data = json.loads(data)
                new_data = []
                for piece in data["data"]:
                    new_data.append(int(piece) & 0b0000000011111111)
                    new_data.append(int(piece) >> 8)
                vcan_sock.send(
                    can.Message(
                        arbitration_id=data["sid"],
                        timestamp=data["timestamp"],
                        data=new_data,
                        is_extended_id=False,
                    )
                )
            # else:
            # data = vcan_sock.recv()
            # udp_sock.sendto(bytes(json.dumps({
            #    "sid": data.arbitration_id,
            #    "dlc": data.dlc,
            #    "data": data.data,
            #    "timestamp": data.timestamp
            # }), "ascii"), adress)


@click.command()
@click.option("--config", help="Bridge config file")
def main(config):
    config = parseConfigFile(config)
    udp_ip = config["socket_udp"]["host"]
    udp_port = config["socket_udp"]["port"]
    # Setup Vcan socket
    can_interface = config["socket_vcan"]["interface"]
    vcan_sock = can.interface.Bus(can_interface, bustype="socketcan")
    # Setup UDP Socket
    udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
    udp_sock.sendto(
        bytes(
            json.dumps({"sid": 1, "dlc": 1, "data": [0, 1, 2, 3], "timestamp": 0}),
            "ascii",
        ),
        (udp_ip, int(udp_port)),
    )

    bridge(udp_sock, vcan_sock, (udp_ip, int(udp_port)))


if __name__ == "__main__":
    main()
