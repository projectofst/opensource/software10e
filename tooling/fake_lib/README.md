# Fake Lib

## Launch
Run `./launch` to launch vsniffer and middleman

## vsniffer
Documentation for [vsniffer](vsniffer/README.md)

## middleman

Documentation for [middleman](middleman/README.md)

# Fake Lib - Quick Start Guide


## Run vsniffer
In order to run the fake lib we need to create a virtual CAN bus
For that we use vsniffer. You can find more info about it [here](https://gitlab.com/projectofst/software10e/-/tree/dev/tooling/fake_lib/vsniffer)

**Steps to run:**
- Open a new terminal on the repository path and go to `software10e/tooling/fake_lib/vsniffer`

- Run the following command
`python3 vsniffer.py config.toml`
 *(Note: You can learn more about the configuration file at the link above)*

After this you should see the script running without any output. 
This means that it's up and running.
  
## Build and run fake iib
One example of a device where we can use fake lib is the iib.
You can find more info about it [here](https://gitlab.com/projectofst/software10e/-/tree/dev/iib)
In order to run the fake iib we need to compile it first.

**Steps to build:**
- Open a new terminal on the repository path and go to `software10e/iib`

- Run the following command to compile fake iib code: 
   `make fake` 

**Steps to run:**
- In the same directory go to `/bin`

- Run the `*.out` output file (`./outputfile.out`)

## Build and run fake bms
Another useful device where you can use fake lib is the bms.
You can find more info about it [here](https://gitlab.com/projectofst/software10e/-/tree/dev/bms-master)

**Steps to build:**
- Open a new terminal on the repository path and go to `software10e/bms-master`

- Run the following command to compile fake bms code: 
   `make fake_b621` 
   
   After the `_` is the bms version that we want to compile, which in this case is `b621`
   If you want, you can also build using the default version by simply running: `make fake`

**Steps to run:**
- In the same directory go to `/bin`

- Run the `*.out` output file (`./outputfile.out`)

  
## Connecting fst can interface to virtual can
If we want to see the interface behavior and interact with the devices connected to virtual can we can connect it through an UDP socket.

**Steps to connect:**

- Ensure that vsniffer is up and running

- If you just opened the interface you should see the communications tab, otherwise go to `coms` button on the left upper side

- Select `Network` on the first dropdown and `Localhost` on the second

- Finnaly hit `connect` and it should connect to vsniffer

- To verify if you are connected just open the Console tab and see if it's displaying data
