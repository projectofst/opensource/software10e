import socket
import struct
import time
import click
import toml
import time
import os
import sys
from datetime import datetime

from queue import Queue
from threading import Thread as Process

# from multiprocessing import Process, Queue
from datetime import datetime


from can import Message


class Connection:
    def __init__(self):
        self.fail_count = 0
        self.warned = False


def send_bus_node(sock, queue, addrs):
    while True:
        msg = queue.get()
        msg = msg.encode_json()

        for addr in addrs.keys():
            print("msg from interface queue", msg, addr)
            sock.sendto(msg, addr)


def recv_bus_node(sock, queue, addrs, log_q):
    time_ms = 0

    while True:
        msg, addr = sock.recvfrom(1024)

        if addr not in addrs.keys():
            print("New connection", addr, flush=True)
            addrs[addr] = Connection()

        msg = Message.decode_json(msg)
        msg.timestamp = time_ms

        for address in addrs.keys():
            if addr == address:
                continue

            sock.sendto(msg.encode_json(), address)

        queue.put(msg)
        log_q.put(msg)

        time_ms += 1


def bus_thread(cfg, recv_queues, send_queues, log_q):
    recv_threads = []
    send_threads = []

    for cfg, recv_queue, send_queue in zip(
        cfg["can"].values(), recv_queues, send_queues
    ):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            sock.bind((cfg["host"], cfg["port"]))
        except Exception as e:
            print("Error:", e)
            print(f"Tried to bind to ({cfg['host']} , {cfg['port']})")
            continue

        addrs = {}

        recv = Process(target=recv_bus_node, args=(sock, recv_queue, addrs, log_q))
        send = Process(target=send_bus_node, args=(sock, send_queue, addrs))

        recv_threads.append(recv)
        send_threads.append(send)

    for thread in recv_threads:
        thread.start()

    for thread in send_threads:
        thread.start()

    for thread in recv_threads:
        thread.join()

    for thread in send_threads:
        thread.join()

    recv = Process(target=recv_bus, args=(cfg, recv_queues))
    send = Process(target=send_bus, args=(cfg, send_queues))

    recv.start()
    send.start()

    recv.join()
    send.join()


def recv_interface(sock, addr, send_queues, cfg, finished, log_q):
    while True:
        try:
            msg, addr = sock.recvfrom(1024)
            sock.sendto(msg, addr)
        except Exception as e:
            logger.error(e)
            finished.put("finished")
            return

        msg = Message.decode_struct(msg)

        count = 0
        for queue, active in zip(
            send_queues, [c["interface"] for c in cfg["can"].values()]
        ):
            if active:
                print(count, msg)
                queue.put(msg)
                log_q.put(msg)

            count += 1


def send_interface_node(sock, addr, queue, cfg, finished):
    while True:
        msg = queue.get()

        if cfg["interface"]:
            try:
                sock.sendto(msg.encode_struct(), addr)
            except Exception as e:
                logger.error(e)
                finished.put("finished")
                return


def send_interface(sock, addr, recv_queues, cfg, _finished):
    threads = []

    finished = Queue()
    for queue, cfg in zip(recv_queues, cfg["can"].values()):
        thread = Process(
            target=send_interface_node, args=(sock, addr, queue, cfg, finished)
        )
        threads.append(thread)

    for thread in threads:
        thread.start()

    finished.get()
    _finished.put("finished")


def interface_thread(cfg, recv_queues, send_queues, log_q):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((cfg["interface"]["host"], cfg["interface"]["port"]))
    print(
        "Waiting for connections at "
        + str(cfg["interface"]["host"])
        + ":"
        + str(cfg["interface"]["port"])
    )
    msg, addr = sock.recvfrom(1024)

    finished = Queue()

    while True:
        recv = Process(
            target=recv_interface, args=(sock, addr, send_queues, cfg, finished, log_q)
        )
        send = Process(
            target=send_interface, args=(sock, addr, recv_queues, cfg, finished)
        )

        recv.start()
        send.start()

        finished.get()

    return


def logger_thread(log_file, log_q):
    f = open(log_file, "w")
    f.flush()
    count = 0

    start = time.time()

    message_list = []
    while True:
        msg = log_q.get()
        stop = time.time()
        timestamp = stop - start
        data = msg.get_data8()
        message_list.append(
            (
                timestamp,
                1,
                msg.sid,
                2,
                8,
                data[0],
                data[1],
                data[2],
                data[3],
                data[4],
                data[5],
                data[6],
                data[7],
                count,
            )
        )

        count += 1

        if count % 100 == 0:
            for msg in message_list:
                f.write(str(msg)[1:-1].replace(" ", "") + "\n")
            message_list = []
            f.flush()


@click.group(invoke_without_command=True)
@click.argument("config")
@click.option("--verbose/--noverbose", default=False)
def main(config, verbose):
    if verbose:
        from loguru import logger

        logger.info("starting")

    with open(config) as f:
        r = f.read()

    cfg = toml.loads(r)

    recv_queues = [Queue() for bus in cfg["can"].values()]
    send_queues = [Queue() for bus in cfg["can"].values()]

    log_q = Queue()

    bus = Process(target=bus_thread, args=(cfg, recv_queues, send_queues, log_q))
    interface = Process(
        target=interface_thread, args=(cfg, recv_queues, send_queues, log_q)
    )

    log_file = cfg["logger"]["file"]
    if log_file == "auto":
        log_file = str(datetime.now()).replace(" ", "_")

    log_file = log_file + ".log"

    log = Process(target=logger_thread, args=(log_file, log_q))

    bus.start()
    interface.start()
    log.start()

    bus.join()
    interface.join()
    log.join()

    return


def check_runtime():
    while True:
        with open("simulator_.runtime", "r") as f:
            if "stop" in f.read():
                print("Bye Bye")
                os._exit(0)
        time.sleep(1)


def check_simulator_runtime_file():
    if os.path.exists("simulator_.runtime"):
        ck_runtime = Process(target=check_runtime)
        ck_runtime.start()


if __name__ == "__main__":
    check_simulator_runtime_file()
    main()
