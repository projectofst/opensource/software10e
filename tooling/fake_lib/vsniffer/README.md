# vsniffer

## Description
Creates a virtual CAN bus to be used in fake lib.
Allows connection to fst-can-interface through the Telemetry UDP socket.

## Usage

``` 
python3 vsniffer.py <config> 
```

## Configuration

vsniffer uses a toml file as configuration.

Example:
```
[can]
	[can.essential]
	host = "127.0.0.1"
	port = 57005
	interface = true

	[can.sensor]
	host = "127.0.0.1"
	port = 48879
	interface = true

	[can.amk]
	host = "127.0.0.1"
	port = 43962
	interface = false

	[can.steering]
	host = "127.0.0.1"
	port = 57007
	interface = false

[interface]
host = "127.0.0.1"
port = 8887
```
