from enum import Enum


class Header(Enum):
    SYN = 1
    ADC = 2


def decode_adc(bstring):
    header = bstring[0]
    id = bstring[1] * 256 + bstring[2]
    value = bstring[3] * 256 + bstring[4]

    return id, value


def encode_adc(id, value):
    bstring = [0 for i in range(5)]
    print(Header.ADC.value)
    bstring[0] = int(Header.ADC.value)
    bstring[1] = id >> 8
    bstring[2] = id & 0xFF
    bstring[3] = value >> 8
    bstring[4] = value & 0xFF

    return bytes(bstring)
