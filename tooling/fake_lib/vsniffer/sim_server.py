import click
import socket


from sim_protocol import *


class Connection:
    def __init__(self):
        self.fail_count = 0
        self.warned = False


addrs = {}


@click.group(invoke_without_command=True)
@click.argument("port", type=int)
def main(port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("127.0.0.1", port))

    while True:
        msg, addr = sock.recvfrom(1024)

        print(msg)
        if msg[0] == Header.SYN:
            addrs[addr] = Connection()
        else:
            for addr in addrs.keys():
                sock.sendto(msg, addr)


if __name__ == "__main__":
    main()
