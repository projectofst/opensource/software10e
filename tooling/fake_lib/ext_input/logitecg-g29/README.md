# Logitech G29 for External Input

## Requirements

[Node](https://nodejs.org/en/) version 8 or greater.

Make sure the [Logitech G29](https://www.logitechg.com/pt-br/products/driving/driving-force-racing-wheel.html) mode switch is set to PS3. This code is developed with [logitech-g29](https://github.com/nightmode/logitech-g29) libs, refer to it when changing the code.

## Install

This library uses [node-hid](https://github.com/node-hid/node-hid) behind the scenes. Depending on your OS and Node version, you may have an effortless install. If not, you may want to consult node-hid's [compiling from source](https://github.com/node-hid/node-hid#compiling-from-source) guide for assistance.

```
npm install logitech-g29
```

[Ubuntu](http://www.ubuntu.com/desktop) users will most likely want to remove the `sudo` requirement of interfacing with the wheel. This can be easily accomplished by creating a file at `/etc/udev/rules.d/99-hidraw-permissions.rules` with the following code. After saving the file, reboot and then you can move on to more fun tasks.

```
KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0664", GROUP="plugdev"
```

You will need to replace the `data-map.js` in logitech-g29 libraries with the one present on this folder due to wrong pins in the lib. You will also need [toml](https://www.npmjs.com/package/toml).

## Using

```
node g29.js config.toml
```

If the `listen -> listening` doesn't appear, press any button so the code recognizes a g29 command. Wait for the `Ready` output so you can start pressing pedals and moving the wheel