import click
from can import Message as CANmessage
import socket
from fcp_mock import Fcp
from fcp.spec import *
import logging
import json
from fcp.validator import validate
from time import sleep

IP = "127.0.0.1"
PORT = 57005


def get_spec(json_file: str, logger: logging.Logger) -> Spec:

    spec = Spec()

    spec = Spec()
    with open(json_file) as f:
        j = json.loads(f.read())

    r, msg = validate(logger, j)
    if not r:
        print(msg)
        exit()

    spec.decompile(j)

    return spec


def setup_logging() -> logging.Logger:

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    handler = logging.FileHandler("fcp.log")
    handler.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


def get_data(file_name):

    data = []

    with open(file_name, "r") as f:
        for line in f.read().splitlines()[1:]:
            line = line.split(",")
            line[0] = float(line[0])
            line[1] = float(line[1])
            data.append(line)

    return data


@click.group(invoke_without_command=True)
@click.argument("fcp_json")
def main(fcp_json):

    logger = setup_logging()
    spec = get_spec(fcp_json, logger)
    fcp = Fcp(spec)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    current_data = get_data("current_data.csv")
    voltage_data = get_data("voltage_data.csv")
    power_data = get_data("power_data.csv")
    charge_data = get_data("charge_data.csv")
    energy_data = get_data("energy_data.csv")

    isa_id = 29

    while True:
        end_of_file = time = c_i = u_i = p_i = e_i = ch_i = 0

        while not end_of_file:

            while current_data[c_i][0] < time:
                msg = fcp.encode_msg(
                    isa_id, "isa_current", {"isa_current": int(current_data[c_i][1])}
                )
                msg.timestamp = round(current_data[c_i][0] * 100000)
                # print("current:",int(current_data[c_i][1]/100))
                sock.sendto(msg.encode_json(), (IP, PORT))
                if c_i + 1 != len(current_data):
                    c_i += 1
                else:
                    end_of_file = 1
                    break

            while voltage_data[u_i][0] < time:
                msg = fcp.encode_msg(
                    isa_id,
                    "isa_voltage_1",
                    {"isa_voltage_1": int(voltage_data[u_i][1])},
                )
                msg.timestamp = round(voltage_data[u_i][0] * 100000)
                sock.sendto(msg.encode_json(), (IP, PORT))
                msg = fcp.encode_msg(
                    isa_id,
                    "isa_voltage_3",
                    {"isa_voltage_3": int(voltage_data[u_i][1])},
                )
                msg.timestamp = round(voltage_data[u_i][0] * 100000)
                sock.sendto(msg.encode_json(), (IP, PORT))
                if u_i + 1 != len(voltage_data):
                    u_i += 1
                else:
                    end_of_file = 1
                    break

            while power_data[p_i][0] < time:
                msg = fcp.encode_msg(
                    isa_id, "isa_power", {"isa_power": int(power_data[p_i][1])}
                )
                msg.timestamp = round(power_data[p_i][0] * 100000)
                sock.sendto(msg.encode_json(), (IP, PORT))
                if p_i + 1 != len(power_data):
                    p_i += 1
                else:
                    end_of_file = 1
                    break

            while (energy_data[e_i][0]) < time:
                msg = fcp.encode_msg(
                    isa_id, "isa_energy", {"isa_energy": int(energy_data[e_i][1])}
                )
                msg.timestamp = round(energy_data[e_i][0] * 100000)
                sock.sendto(msg.encode_json(), (IP, PORT))
                if e_i + 1 != len(energy_data):
                    e_i += 1
                else:
                    end_of_file = 1
                    break

            while (charge_data[ch_i][0]) < time:
                msg = fcp.encode_msg(
                    isa_id, "isa_charge", {"isa_charge": int(charge_data[ch_i][1])}
                )
                msg.timestamp = round(charge_data[ch_i][0] * 100000)
                sock.sendto(msg.encode_json(), (IP, PORT))
                if ch_i + 1 != len(charge_data):
                    ch_i += 1
                else:
                    end_of_file = 1
                    break

            time += 0.01
            sleep(0.01)


if __name__ == "__main__":
    main()
