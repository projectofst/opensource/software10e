from can import Connection


class Driver:
    def __init__(self, cfg, fcp):
        self.fcp = fcp
        self.cfg = cfg

        self.essential_bus = Connection(
            (cfg["can"]["essential"]["host"], cfg["can"]["essential"]["port"])
        )

        return

    def recv_msgs(self):
        while True:
            print(self.essential_bus.sock.recvfrom(1024))

    def loop(self):
        return
