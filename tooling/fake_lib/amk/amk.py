import json
import logging
import socket
from math import *
from threading import Thread
from time import sleep


import fcp as fcp_tool
import click
import toml
from fcp.spec import *
from fcp.validator import validate

from can import Message
from fcp_mock import Fcp
from sim_protocol import *

from units import *
from car import Car

from loguru import logger


import pygame, sys
from pygame.locals import *
import random, time

node_ids = [0, 1, 4, 5]
motor_amb_temp = 25
inv_amb_temp = 35
water_temp = 30


def setup_logging() -> logging.Logger:

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    handler = logging.FileHandler("fcp.log")
    handler.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


def get_spec(json_file: str, logger: logging.Logger) -> Spec:

    spec = Spec()

    spec = Spec()
    with open(json_file) as f:
        j = json.loads(f.read())

    r, msg = validate(logger, j)
    if not r:
        # print(msg)
        exit()

    spec.decompile(j)

    return spec


def saturation(value, sat):
    return sat if value > sat else value


def upper_limit(value, limit):
    return limit if value > limit else value


def lower_limit(value, limit):
    return limit if value < limit else value


def amk_torque_curve(rpm, torque_lim, power_lim):
    if rpm > 23000:
        return 0

    w = 2 * pi * rpm / 60

    if w > 0:
        torque_power_lim = saturation(power_lim / w, torque_lim)
    else:
        return torque_lim

    return upper_limit(torque_power_lim, torque_lim)


def amk_inner_control(v, inv, car):
    k = 21 / 2000
    rpm = 60 * v / car.wheel_radius * car.gear_ratio / (2 * pi)
    torque_curve_limit = amk_torque_curve(rpm, car.max_torque, car.power_limit)
    # print("inv.req_speed - rpm: ", inv.req_speed - rpm)
    torque = k * (inv.req_speed - rpm)

    torque = upper_limit(torque, inv.req_t_p)
    torque = lower_limit(torque, inv.req_t_n)

    return rpm, upper_limit(torque, torque_curve_limit)


class SimulationParams:
    def __init__(self):
        self.invs = [Inverter(node_ids[i]) for i in range(4)]

        self.bat = []  # init bat struct
        self.R = 1000
        self.C = 300e-6
        self.v1 = 588
        self.v2 = 588
        self.v3 = 0
        self.disch_i = 0.040

        self.current = 0
        self.power = 0
        self.energy = 0
        self.charge = 0
        self.steer = 0


class Inverter:
    def __init__(self, node_id):
        self.av1_id = 0x282 + node_id
        self.av2_id = 0x284 + node_id

        self.status = 0
        self.set_sys_rdy()

        self.magnetizing_current = 0
        self.actual_current = 0

        self.temp_motor = motor_amb_temp
        self.temp_inv = inv_amb_temp
        self.error_info = 0
        self.temp_igbt = inv_amb_temp

        self.req_t_p = 0
        self.req_t_n = 0
        self.req_speed = 0
        self.wdg = 0

        # state variables
        self.velocity = 0
        self.torque = 0

    def encode_av2(self):
        msg = Message()
        msg.sid = self.av2_id
        msg.dlc = 8
        msg.data = list(range(4))
        msg.data[0] = encode_temp(self.temp_motor)
        msg.data[1] = encode_temp(self.temp_inv)
        msg.data[2] = self.error_info
        msg.data[3] = encode_temp(self.temp_igbt)
        msg.timestamp = 0

        return msg

    def encode_av1(self):
        msg = Message()
        msg.sid = self.av1_id
        msg.dlc = 8
        msg.data = list(range(4))
        msg.data[0] = self.status
        msg.data[1] = int(self.velocity)
        msg.data[2] = encode_current(self.torque / 0.26)
        msg.data[3] = encode_current(self.magnetizing_current)
        msg.timestamp = 0

        return msg

    def get_sys_rdy(self):
        return (self.status >> 8) & 0b1

    def set_sys_rdy(self):
        self.status = self.status | (1 << 8)

    def clear_sys_rdy(self):
        self.status = self.status & (0xFFFF ^ (1 << 8))


class AMK:
    def __init__(self, conn, sp: SimulationParams):
        self.conn = conn
        self.params = sp
        return

    def inv_on(self, i):

        self.params.invs[i].status = self.params.invs[i].status | (1 << 14)

    def inv_off(self, i):

        self.params.invs[i].status = self.params.invs[i].status & (0xFFFF ^ (1 << 14))
        self.params.invs[i].velocity = 0
        self.params.invs[i].torque = 0
        self.params.invs[i].magnetizing_current = 0

    def calc_temp(self, t_amb, temp, current, k_1, k_2):

        k_3 = 0
        water_v = 25  # m/s

        return (
            temp
            + k_1 * (max(current - 41, 0) ** 2)
            - k_2 * max(temp - t_amb, 0)
            + k_3 * water_v**2 * max(temp - water_temp, 0)
        )

    def send_av1(self, sock, addr):
        for inv in self.params.invs:
            msg = inv.encode_av1()
            sock.sendto(msg.encode_json(), addr)

    def send_av2(self, sock, addr):
        for inv in self.params.invs:
            msg = inv.encode_av2()
            sock.sendto(msg.encode_json(), addr)

    def send_msg(self):
        while True:
            self.send_av1(self.conn.sock, self.conn.addr)
            self.send_av2(self.conn.sock, self.conn.addr)
            sleep(0.1)

    def receive_msg(self):
        while True:
            try:
                msg, addr = self.conn.sock.recvfrom(1024)
                msg = Message.decode_json(msg)
                dev_id, msg_id = decompose_id(msg.sid)

                if dev_id == 10 and msg_id == 34:
                    if msg.data[0] & (1 << 15) != 0:  # negative
                        self.params.steer = (msg.data[0] - (1 << 16)) / 10
                    else:  # positive
                        self.params.steer = msg.data[0] / 10

                # inv rtd-on
                for i, inv in enumerate(self.params.invs):
                    # inv rtd-on
                    if (
                        msg.sid == (0x183 + node_ids[i])
                        and msg.data[0] == (1 << 10 | 1 << 9 | 1 << 8)
                        and (inv.status & (1 << 14)) != 0x4000
                    ):
                        self.inv_on(i)
                        logger.info(f"inv {i} on")
                    # inv rtd-off
                    elif (
                        msg.sid == (0x183 + node_ids[i])
                        and msg.data[0] == (1 << 9)
                        and (inv.status & (1 << 14)) == 0x4000
                    ):
                        self.inv_off(i)
                        logger.info(f"inv {i} off")
                    if (
                        msg.sid == (0x183 + node_ids[i])
                        and (inv.status & (1 << 14)) == 0x4000
                    ):
                        inv.wdg = 0

                        # print("request: ", msg.data)
                        inv.req_speed = float(msg.data[1])
                        inv.req_t_p = decode_torque(msg.data[2])
                        inv.req_t_n = decode_torque(msg.data[3])
                        # print(inv.req_t_p)

            except Exception as e:
                pass
                # print("Error: ", e)


class Isa:
    def __init__(self, conn, fcp: Fcp, sp: SimulationParams):
        self.conn = conn
        self.params = sp
        self.fcp = fcp
        self.can = {}
        self.can["master"] = {}
        self.can["master"]["master_status_precharge"] = 0
        self.can["master"]["master_status_air_p"] = 0
        self.can["master"]["master_status_air_n"] = 0

        logger = setup_logging()

    def get_current(self):
        return 1000 * self.params.current

    def get_v1(self):
        return 1000 * self.params.v1

    def get_v2(self):
        return 1000 * self.params.v2

    def get_v3(self):
        return 1000 * self.params.v3

    def get_power(self):
        return self.params.power

    def get_energy(self):
        return self.params.energy

    def get_charge(self):
        return self.params.charge

    def receive_msg(self):

        while True:
            msg, addr = self.conn.sock.recvfrom(1024)
            # msg = Message.decode_json(msg)
            msg = fcp_tool.can.CANMessage.decode_json(msg)
            # name, signal = self.fcp.decode_msg(msg)
            name, signal = self.fcp.decode_msg(msg)
            if name == "master_status":
                # print("master_status_precharge: ", signal["master_status_precharge"], "master_status_air_p: ", signal["master_status_air_p"], "master_status_air_n: ", signal["master_status_air_n"])
                self.can["master"]["master_status_precharge"] = signal[
                    "master_status_precharge"
                ]
                self.can["master"]["master_status_air_p"] = signal[
                    "master_status_air_p"
                ]
                self.can["master"]["master_status_air_n"] = signal[
                    "master_status_air_n"
                ]

    def send_msg(self):
        isa_id = 29

        isa_msgs = {
            "isa_current": self.get_current,
            "isa_voltage_1": self.get_v1,
            "isa_voltage_2": self.get_v2,
            "isa_voltage_3": self.get_v3,
            "isa_power": self.get_power,
            "isa_energy": self.get_energy,
            "isa_charge": self.get_charge,
        }

        while True:
            for name, getter in isa_msgs.items():
                signals = {}
                signals[name] = int(getter())
                msg = self.fcp.encode_msg(isa_id, name, signals)
                self.conn.sock.sendto(msg.encode_json(), self.conn.addr)

            sleep(0.01)

    def loop(self):
        ts_on = 0
        v3_init = 0
        t = 0

        while True:
            self.params.power = sum(
                [300 * inv.actual_current for inv in self.params.invs]
            )
            self.params.current = self.params.power / self.params.v1
            self.params.v2 = self.params.v1 - 0.004 * self.params.current

            print(
                "pre:",
                self.can["master"]["master_status_precharge"],
                "air_p:",
                self.can["master"]["master_status_air_p"],
                "air_n:",
                self.can["master"]["master_status_air_n"],
            )
            if self.can["master"]["master_status_precharge"] == 1:
                print("pre-charge")
                if ts_on == 0:
                    ts_on = 1
                    t = 0
                else:
                    self.params.v3 = self.params.v2 * (
                        1 - e ** (-t / (self.params.R * self.params.C))
                    )
                    print("v2: ", self.params.v2, "v3: ", self.params.v3)
            elif self.can["master"]["master_status_air_p"] == 1:
                print("on")
                self.params.v3 = self.params.v2
            elif (
                self.can["master"]["master_status_air_p"] == 0
                and self.can["master"]["master_status_air_n"] == 0
                and self.can["master"]["master_status_precharge"] == 0
            ):
                print("discharge")
                if ts_on == 1:
                    t = 0
                    ts_on = 0
                    v3_init = self.params.v3
                self.params.v3 = v3_init - (self.params.disch_i * t / self.params.C)

            self.params.energy = self.params.energy + self.params.power * 0.01
            # self.params.charge

            t += 0.01
            sleep(0.01)


class Simulator:
    def __init__(self, amk, isa, sp):
        self.amk = amk
        self.isa = isa
        self.params = sp

    def gui(self):

        # Initializing
        pygame.init()

        # Setting up FPS
        FPS = 60
        FramePerSec = pygame.time.Clock()

        # Creating colors
        BLUE = (0, 0, 255)
        RED = (255, 0, 0)
        GREEN = (0, 255, 0)
        BLACK = (0, 0, 0)
        WHITE = (255, 255, 255)

        # Other Variables for use in the program
        SCREEN_WIDTH = 800
        SCREEN_HEIGHT = 600
        SPEED = 5

        # Create a white screen
        DISPLAYSURF = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
        DISPLAYSURF.fill(WHITE)
        pygame.display.set_caption("Game")

        class Player(pygame.sprite.Sprite):
            def __init__(self):
                self.previous_x = 0
                self.previous_y = 0
                super().__init__()
                self.image = pygame.Surface((10, 10))
                self.surf = pygame.Surface((10, 10))
                self.rect = self.surf.get_rect(center=(150, 500))

            def move(self, x, y):
                pressed_keys = pygame.key.get_pressed()

                dy = int(self.previous_y - y)
                dx = int(x - self.previous_x)

                if dx >= 1 or dx <= -1:
                    self.previous_x = x
                else:
                    dx = 0

                if dy >= 1 or dy <= -1:
                    self.previous_y = y
                else:
                    dy = 0

                print(x, y)
                print(dx, dy)

                self.rect.move_ip(dx, dy)

        # Setting up Sprites
        P1 = Player()

        # Creating Sprites Groups
        all_sprites = pygame.sprite.Group()
        all_sprites.add(P1)

        # Adding a new User event
        INC_SPEED = pygame.USEREVENT + 1
        pygame.time.set_timer(INC_SPEED, 5000)

        # Game Loop
        while True:

            # Cycles through all events occuring
            for event in pygame.event.get():
                if event.type == INC_SPEED:
                    SPEED += 2

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

            DISPLAYSURF.fill(WHITE)

            # Moves and Re-draws all Sprites
            for entity in all_sprites:
                DISPLAYSURF.blit(entity.image, entity.rect)
                entity.move(self.x, self.y)

            pygame.display.update()
            FramePerSec.tick(FPS)

    def loop(self):
        self.car = Car()
        self.v = 0
        self.v_x = 0
        self.v_y = 0
        self.x = 0
        self.y = 0
        self.yaw = 0
        self.yaw_rate = 0
        self.beta = 0

        self.delta_t = 10e-3

        while True:
            torque = sum([inv.torque for inv in self.amk.params.invs])
            self.a = (
                torque * self.car.gear_ratio / (self.car.wheel_radius * self.car.mass)
            )
            self.beta = atan2(
                self.car.l_r * tan(self.params.steer / 360 * 2 * pi),
                self.car.l_r + self.car.l_f,
            )
            # newton's second law
            self.v = self.v + self.a * self.delta_t
            self.v = saturation(self.v, self.car.max_speed)

            self.v_x = self.v * cos(self.yaw + self.beta)
            self.v_y = self.v * sin(self.yaw + self.beta)
            self.yaw_rate = self.v / self.car.l_r * sin(self.beta)
            self.yaw = self.yaw + self.yaw_rate * self.delta_t

            self.x = self.x + self.v_x * self.delta_t
            self.y = self.y + self.v_y * self.delta_t
            # print("torque: ", torque)
            # print("a: ", a)
            # print(f"x: {x}, y: {y}")
            # print(f"{self.x},{self.y}")
            # print(f"v: {self.v}, v_x: {self.v_x}, v_y: {self.v_y}")
            # print(f"yaw: {yaw}, yaw_rate: {yaw_rate}")

            for i, inv in enumerate(self.amk.params.invs):
                # print(f"req_speed: {inv.req_speed}")
                # print(f"req_t_p: {inv.req_t_p}")
                # print(f"req_t_n: {inv.req_t_n}")
                inv.velocity, inv.torque = amk_inner_control(self.v, inv, self.car)
                # print(inv.torque)
                # print("rpm: ", inv.velocity)

                inv.wdg += 1

                if inv.wdg > 100:
                    inv.req_speed = 0
                    inv.req_t_n = 0
                    inv.req_t_p = 0

                # torque = inv.req_t_p
                # inv.velocity = inv.req_speed

                # print("inv.velocity: ", inv.velocity)
                if inv.velocity > 10000:
                    inv.magnetizing_current = 0.007 * inv.velocity - 70
                    # print("inv.magnetizing_current: ", inv.magnetizing_current)
                else:
                    inv.magnetizing_current = 0

                inv.actual_current = sqrt(
                    abs(inv.magnetizing_current**2 + (inv.torque / 0.26) ** 2)
                )
                # print("torque: ", inv.torque, "actual_current: ", inv.actual_current, "magnetizing_current: ", inv.magnetizing_current, "req_speed: ", inv.req_speed, "velocity: ", inv.velocity, "id: ", inv.av1_id)

                inv.temp_motor = self.amk.calc_temp(
                    motor_amb_temp,
                    inv.temp_motor,
                    inv.actual_current,
                    0.26**2 * 0.25 * 10 ** (-6),
                    0.5 * 10 ** (-2),
                )
                inv.temp_inv = self.amk.calc_temp(
                    inv_amb_temp,
                    inv.temp_inv,
                    inv.actual_current,
                    0.26**2 * 0.25 * 10 ** (-6),
                    0.5 * 10 ** (-2),
                )
                inv.temp_igbt = self.amk.calc_temp(
                    inv_amb_temp,
                    inv.temp_igbt,
                    inv.actual_current,
                    0.26**2 * 0.25 * 10 ** (-6),
                    0.5 * 10 ** (-2),
                )
                inv.error_info = 0

            sleep(self.delta_t)
