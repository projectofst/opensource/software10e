import pygame
from pygame import *
from math import *

import json
import sys
import click
import zmq
from socket import socket

from lib import read_config, AttrDict, Connection, Pedals, Steering


def adc_pack(id, value):
    bs = []
    bs.append((value >> 24) & 0xFF)
    bs.append((value >> 16) & 0xFF)
    bs.append((value >> 8) & 0xFF)
    bs.append((value >> 0) & 0xFF)
    bs.append(len(id))
    for i, c in enumerate(id):
        bs.append(ord(c))
    return bytearray(bs)


def pub(sock, pedals, steering):
    sock.sendto(adc_pack("apps0", int(pedals.apps0)))
    sock.sendto(adc_pack("apps1", int(pedals.apps1)))
    sock.sendto(adc_pack("pressure0", int(pedals.bps0)))
    sock.sendto(adc_pack("pressure1", int(pedals.bps1)))
    sock.sendto(adc_pack("eletric", int(pedals.be)))
    sock.sendto(adc_pack("steering", steering.steering))


def run_gui(cfg, sock):

    pedals = Pedals()
    steer = Steering()

    pygame.init()
    window_width = 1000
    window_height = 700

    screen = pygame.display.set_mode(
        (window_width, window_height)
    )  # , pygame.FULLSCREEN)
    pygame.display.set_caption("Simulation")

    track = pygame.image.load(cfg.track)
    car = pygame.image.load(cfg.car)

    x = 50
    y = 150

    v = 0
    inc_y = 0

    clock = pygame.time.Clock()

    aperture = pi / 4

    torques = [0, 0, 0, 0]
    rpm = 0
    wheel_radius = 0.2
    gear_ratio = 16
    mass = 300
    l_r = 1
    l_f = 1

    max_rpm = 23000
    max_speed = 2 * pi * (max_rpm / 60) / gear_ratio * wheel_radius
    yaw = 0
    yaw_rate = 0

    def saturation(value, sat):
        return sat if value > sat else value

    def walk(x, y, angle):
        walk_x = walk_y = 0

        walk = []

        for i in range(5, 150):
            walk_x = x + i * cos(angle)
            walk_y = y + i * sin(angle)
            color = 0
            try:
                r, g, b, a = screen.get_at((int(walk_x), int(walk_y)))
                color = sqrt(r**2 + g**2 + b**2)
            except Exception as e:
                pass
            walk.append(color > 50)

        return walk

    norm = 150

    def sensors(x, y, yaw):
        s1 = s2 = norm
        s3 = s4 = s5 = norm
        # print("yaw: ", yaw)

        for i, v in enumerate(walk(x, y, yaw + aperture)):
            if v >= 1:
                s1 = i + 5
                break

        for i, v in enumerate(walk(x, y, yaw - aperture)):
            if v >= 1:
                s2 = i + 5
                break

        for i, v in enumerate(walk(x, y, yaw + pi / 2)):
            if v >= 1:
                s3 = i + 5
                break

        for i, v in enumerate(walk(x, y, yaw - pi / 2)):
            if v >= 1:
                s4 = i + 5
                break

        for i, v in enumerate(walk(x, y, yaw)):
            if v >= 1:
                s5 = i + 5
                break

        return s1 / norm, s2 / norm, s3 / norm, s4 / norm, s5 / norm

    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()

            if event.type == KEYDOWN:
                if event.key == K_q:
                    pygame.quit()

                # if event.key == K_d:
                #    w += 1

                # if event.key == K_a:
                #    w -= 1

                # if event.key == K_w:
                #    v += 1

                # if event.key == K_s:
                #    v -= 1

        car_rotated = pygame.transform.rotate(car, -yaw)

        r, g, b, a = screen.get_at((int(x), int(y)))
        color_normal = sqrt(r**2 + g**2 + b**2)
        # if color_normal > 100:
        # v = 0

        center_x, center_y = (
            x + car_rotated.get_width() // 2,
            y + car_rotated.get_height() // 2,
        )
        s1, s2, s3, s4, s5 = sensors(center_x, center_y, yaw)
        print("sensors:", s1, s2, s3, s4)

        # screen.fill([0,0,0])

        start_x = center_x + 5 * cos(yaw + aperture)
        start_y = center_y + 5 * sin(yaw + aperture)
        end_x = center_x + s1 * norm * cos(yaw + aperture)
        end_y = center_y + s1 * norm * sin(yaw + aperture)
        pygame.draw.line(
            screen, Color(255, 0, 0), (start_x, start_y), (end_x, end_y), 2
        )
        pygame.display.flip()

        start_x = center_x + 5 * cos(yaw - aperture)
        start_y = center_y + 5 * sin(yaw - aperture)
        end_x = center_x + s2 * norm * cos(yaw - aperture)
        end_y = center_y + s2 * norm * sin(yaw - aperture)
        pygame.draw.line(
            screen, Color(0, 255, 0), (start_x, start_y), (end_x, end_y), 2
        )
        pygame.display.flip()

        start_x = center_x + 5 * cos(yaw + pi / 2)
        start_y = center_y + 5 * sin(yaw + pi / 2)
        end_x = center_x + s3 * norm * cos(yaw + pi / 2)
        end_y = center_y + s3 * norm * sin(yaw + pi / 2)
        pygame.draw.line(
            screen, Color(255, 0, 0), (start_x, start_y), (end_x, end_y), 2
        )
        pygame.display.flip()

        start_x = center_x + 5 * cos(yaw - pi / 2)
        start_y = center_y + 5 * sin(yaw - pi / 2)
        end_x = center_x + s4 * norm * cos(yaw - pi / 2)
        end_y = center_y + s4 * norm * sin(yaw - pi / 2)
        pygame.draw.line(
            screen, Color(0, 255, 0), (start_x, start_y), (end_x, end_y), 2
        )
        pygame.display.flip()

        start_x = center_x + 5 * cos(yaw)
        start_y = center_y + 5 * sin(yaw)
        end_x = center_x + s5 * norm * cos(yaw)
        end_y = center_y + s5 * norm * sin(yaw)
        pygame.draw.line(
            screen, Color(0, 0, 255), (start_x, start_y), (end_x, end_y), 2
        )
        pygame.display.flip()

        # w = 15*(s1-s2) + 15*(s3-s4)
        # v = 20*(1.1-e**(-100*s5))

        # yaw += w
        # x += v * cos(2*pi*yaw/360)
        # y += v * sin(2*pi*yaw/360)

        car_rotated = pygame.transform.rotate(car, -yaw / (2 * pi) * 360)
        screen.blit(track, (0, 0))
        screen.blit(car_rotated, (x, y))

        apps = s5 + 0.1 * sqrt(s1**2 + s2**2)
        brake = 0
        if v > 5:
            brake = 1 - s5

        if apps > 0.25:
            brake = 0

        sign = 1 if s1 > s2 else -1
        steering = 45 * (s1 - s2)
        # steering = 10

        for i in range(4):
            if apps > 0.5 * brake:
                torques[i] = 20 * apps
            else:
                torques[i] = -20 * brake

        torque = sum(torques)
        a = torque * gear_ratio / (wheel_radius * mass)
        # print("a:", a)
        beta = atan2(l_r * tan(steering / 360 * 2 * pi), l_r + l_f)
        # print("beta:", beta)
        # print("steering:", steering)
        v = v + a * (1 / cfg.fps)
        v = saturation(v, max_speed)
        v = v if v > 0 else 0

        # print("yaw:", yaw)
        # print("yaw_rate:", yaw_rate)
        v_x = 2 * v * cos(yaw + beta)
        v_y = -2 * v * sin(yaw + beta)

        # print("v_x:", v_x)
        # print("v_y:", v_y)
        yaw_rate = v / l_r * sin(beta)
        yaw_rate = yaw_rate if yaw_rate < 1 else 1
        yaw = yaw + yaw_rate * (1 / cfg.fps)
        x = x + v_x * (1 / cfg.fps)
        y = y - v_y * (1 / cfg.fps)

        print("v:", v)

        pedals.set_apps(apps)
        pedals.set_brake(brake)

        steer.set_steering(steering)
        pub(sock, pedals, steer)
        pygame.display.update()
        clock.tick(cfg.fps)


@click.group(invoke_without_command=True)
@click.argument("config")
def main(config):
    cfg = read_config(config)

    sock = Connection((cfg["middleman"]["host"], cfg["middleman"]["port"]))

    cfg = AttrDict(cfg["gui"])
    run_gui(cfg, sock)


if __name__ == "__main__":
    main()
