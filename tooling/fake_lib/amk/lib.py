import toml
import socket


class Pedals:
    def __init__(self):
        self.apps0 = 0
        self.apps1 = 0
        self.bps0 = 0
        self.bps1 = 0
        self.be = 0

    def set_apps(self, apps):
        if apps > 1:
            apps = 1

        self.apps0 = 2921 * apps + 877
        self.apps1 = 2922 * apps + 298

    def set_brake(self, brake):
        self.bps0 = 3278 * brake + 409
        self.bps1 = 3278 * brake + 409
        # self.be = 1699*brake + 2048


class Steering:
    def __init__(self):
        self.steering = 0

    def set_steering(self, wheel_steer):
        steering = 4 * wheel_steer
        print("steering:", steering, flush=True)
        steering = steering / 360

        self.steering = int(1699 + 1699 * steering)


class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


def read_config(pathname):
    with open(pathname) as f:
        r = f.read()

    cfg = toml.loads(r)

    return cfg


class Connection:
    def __init__(self, addr=None):
        if addr == None:
            self.sock = None
        else:
            self.connect(addr)

    def connect(self, addr):
        self.addr = addr
        self.sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

        # msg = Message(sid=0, dlc=8, data=[0,0,0,0])
        # self.sock.sendto(msg.encode_json(), self.addr)

    def sendto(self, buf):
        self.sock.sendto(buf, self.addr)
