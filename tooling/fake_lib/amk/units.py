def bitmask(n):
    return 2**n - 1


def sign_conv(value, length):
    if (value >> (length - 1)) == 1:
        return -((value ^ bitmask(length)) + 1)
    else:
        return value


def decode_current(current):
    return sign_conv(current, 16) * 107.2 / 16384


def encode_current(current):
    return int(current / 107.2 * 16384)


def decode_torque(torque):
    return sign_conv(torque, 16) * 9.81 / 1000


def encode_torque(torque):
    return int(torque / 9.81 * 1000)


def decode_temp(temp):
    return temp / 10


def encode_temp(temp):
    return int(temp * 10)
