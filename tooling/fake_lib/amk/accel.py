from math import pi
from matplotlib import pyplot as plt

mass = 250 + 60
wheel_radius = 0.228
gear_ratio = 16


def calc_torque(v):
    w = v / wheel_radius
    w = w * 16
    rpm = 60 * w / (2 * pi)

    if w > 23000:
        return 0

    torque_lim = 20000 / w
    if torque_lim < 21:
        return torque_lim

    return 21


x0 = 0
v0 = 0

torque_ref = 21

delta_t = 1e-3

x = x0
v = v0

torque = torque_ref

sim_time = 10

vs = []
torques = []
for i in range(int(sim_time / delta_t)):
    if x > 80:
        t = i
        break

    a = 4 * torque * gear_ratio / (wheel_radius * mass)
    if v > 34:
        v = 34
    else:
        v = v + a * delta_t

    vs.append(v)
    x = x + v * delta_t + a * (delta_t) ** 2
    torque = calc_torque(v)
    torques.append(torque)
    # print(torque)


print(x)
print(v * 3.6)
print(t * delta_t)

ts = [t / delta_t for t in range(int(t))]
plt.plot(ts, vs)
plt.plot(ts, torques)
plt.show()
