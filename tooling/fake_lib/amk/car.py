from math import pi


class Car:
    def __init__(self):
        self.wheel_radius = 0.228
        self.mass = 250 + 60
        self.gear_ratio = 16
        self.max_rpm = 23000
        self.max_speed = (
            2 * pi * (self.max_rpm / 60) / self.gear_ratio * self.wheel_radius
        )
        self.max_torque = 21
        self.power_limit = 20000
        self.l_r = 1
        self.l_f = 1
