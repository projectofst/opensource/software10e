import toml
import click
import json

from threading import Thread

from can import Connection, Message
from amk import AMK, Simulator, Isa, SimulationParams

from fcp import Spec
from fcp import Fcp
import pygame
from pygame import *
from math import *

import sys
from lib import read_config


@click.group(invoke_without_command=True)
@click.argument("config")
@click.argument("fcp_json")
def main(config, fcp_json):

    cfg = read_config(config)

    amk_cfg = cfg["can"]["amk"]
    amk_bus = Connection((amk_cfg["host"], amk_cfg["port"]))
    essential_cfg = cfg["can"]["essential"]
    essential_bus = Connection((essential_cfg["host"], essential_cfg["port"]))

    with open(fcp_json) as f:
        r = f.read()
        j = json.loads(r)

    spec = Spec()
    spec.decompile(j)
    fcp = Fcp(spec)

    sp = SimulationParams()
    amk = AMK(amk_bus, sp)
    isa = Isa(essential_bus, fcp, sp)
    sim = Simulator(amk, isa, sp)

    amk_receive_thread = Thread(target=amk.receive_msg, args=())
    amk_receive_thread.start()

    amk_send_thread = Thread(target=amk.send_msg, args=())
    amk_send_thread.start()

    isa_loop_thread = Thread(target=isa.loop, args=())
    isa_loop_thread.start()

    isa_receive_thread = Thread(target=isa.receive_msg, args=())
    isa_receive_thread.start()

    isa_send_thread = Thread(target=isa.send_msg, args=())
    isa_send_thread.start()

    sim_thread = Thread(target=sim.loop, args=())
    sim_thread.start()

    amk_receive_thread.join()
    amk_send_thread.join()
    isa_send_thread.join()
    sim_thread.join()
    # gui_thread.join()


if __name__ == "__main__":
    main()
