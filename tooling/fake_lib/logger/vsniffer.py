import socket
import struct
import time
import click
import toml

from queue import Queue
from threading import Thread as Process

# from multiprocessing import Process, Queue
from datetime import datetime


from can import Message

from loguru import logger


@click.group(invoke_without_command=True)
@click.argument("config")
@click.option("--verbose/--noverbose", default=False)
def main(config, verbose):

    logger.info("starting")

    with open(config) as f:
        r = f.read()

    cfg = toml.loads(r)

    return


if __name__ == "__main__":
    main()
