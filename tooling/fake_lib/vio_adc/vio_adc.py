import sys, time, click, socket
from protobuf_python import fake_lib_pb2

from threading import Thread
import queue


def read_input_thread(name, q):
    while True:
        r = input("> ")
        print(f"Cmd: {r}")
        q.put(r)


def send_adcs_thread(period, sock, host, port, q):
    adc = fake_lib_pb2.ADC()

    values = [1758, 2337, 410, 410, 410]

    def update_value(var, index, value):
        var[index] += value

    key_actions = {
        "q": lambda x: update_value(values, 0, x),
        "a": lambda x: update_value(values, 0, -x),
        "w": lambda x: update_value(values, 1, x),
        "s": lambda x: update_value(values, 1, -x),
        "e": lambda x: update_value(values, 2, x),
        "d": lambda x: update_value(values, 2, -x),
        "r": lambda x: update_value(values, 3, x),
        "f": lambda x: update_value(values, 3, -x),
        "t": lambda x: update_value(values, 4, x),
        "g": lambda x: update_value(values, 4, -x),
    }

    last_cmd = "q"
    last_jump = 10
    while True:
        if not q.empty():
            user = q.get()
            user = user.split(" ")

            if len(user) == 0 or (len(user) == 1 and user[0] == ""):
                cmd = last_cmd
                jump = last_jump
            else:
                cmd = user[0]

            if len(user) > 1:
                try:
                    jump = int(user[1])
                except Exception as e:
                    jump = 10
            else:
                jump = last_jump

            v = key_actions.get(cmd)

            if v is not None:
                v(jump)
                last_cmd = cmd
                last_jump = jump

        adc.id = "apps1"
        adc.value = values[0]

        msg = adc.SerializeToString()
        sock.sendto(msg, (host, port))

        adc.id = "apps0"
        adc.value = values[1]

        msg = adc.SerializeToString()
        sock.sendto(msg, (host, port))

        adc.id = "pressure0"
        adc.value = values[2]

        msg = adc.SerializeToString()
        sock.sendto(msg, (host, port))

        adc.id = "pressure1"
        adc.value = values[3]

        msg = adc.SerializeToString()
        sock.sendto(msg, (host, port))

        adc.id = "eletric"
        adc.value = values[4]

        msg = adc.SerializeToString()
        sock.sendto(msg, (host, port))

        time.sleep(period / 1000)


@click.group(invoke_without_command=True)
@click.argument("host")
@click.argument("port", type=int)
@click.argument("period", type=float)
def main(host, port, period):
    q = queue.Queue()

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    send_thread = Thread(target=send_adcs_thread, args=(period, sock, host, port, q))
    read_input = Thread(target=read_input_thread, args=("read_input", q))

    print(
        """
    Command syntax: <cmd> <increment>
    A single return will repeat the last command.

    q - increment apps0
    a - decrement apps0
    w - increment apps1
    s - decrement apps1
    e - increment bps0
    d - decrement bps0
    r - increment bps1
    f - decrement bps1
    t - increment be 
    g - decrement be 
    """
    )

    send_thread.start()
    read_input.start()

    send_thread.join()
    read_input.join()


if __name__ == "__main__":
    main()
