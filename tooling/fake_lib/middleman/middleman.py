import socket
import struct
import time
import click
import toml

from queue import Queue
from threading import Thread

# from multiprocessing import Process as Thread, Queue
from datetime import datetime


from loguru import logger


class Connection:
    def __init__(self):
        self.fail_count = 0
        self.warned = False


def send_thread(sock, addrs, queue, verbose):
    while True:
        msg = queue.get()

        for addr in addrs:
            sock.sendto(msg, addr)
            if verbose:
                logger.warning(str(msg) + str(addr))


def recv_thread(sock, addrs, queue, verbose):
    while True:
        msg, addr = sock.recvfrom(1024)
        if verbose:
            logger.info(str(msg) + str(addr))

        if addr not in addrs:
            print("New connection", addr, flush=True)
            addrs[addr] = Connection()

        for address, conn in addrs.items():
            if address == addr:
                continue
            if verbose:
                logger.error("msg: " + str(msg))
            sock.sendto(msg, address)

        queue.put(msg)


def bus_thread(sock, cfg, q1, q2, verbose):
    sock.bind((cfg["host"], cfg["port"]))
    addrs = {}

    recv = Thread(target=recv_thread, args=(sock, addrs, q1, verbose))
    send = Thread(target=send_thread, args=(sock, addrs, q2, verbose))

    recv.start()
    send.start()

    recv.join()
    send.join()


@click.group(invoke_without_command=True)
@click.argument("config")
@click.option("--verbose/--noverbose", default=False)
def main(config, verbose):
    if verbose:
        logger.info("starting")

    with open(config) as f:
        r = f.read()

    cfg = toml.loads(r)

    bus_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    interface_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    q1 = Queue()
    q2 = Queue()

    bus = Thread(target=bus_thread, args=(bus_socket, cfg["bus"], q1, q2, verbose))
    interface = Thread(
        target=bus_thread, args=(interface_socket, cfg["interface"], q2, q1, verbose)
    )

    bus.start()
    interface.start()

    bus.join()
    interface.join()

    return


if __name__ == "__main__":
    main()
