# middleman

## Description
Redirects messages from one UDP socket to another UDP socket.
All clients receive all messages.

## Usage

``` 
python3 middleman.py <config> 
```

## Configuration

middleman uses a toml file as configuration.

Example:

```
[bus]
host = "127.0.0.1"
port = 9999

[interface]
host = "127.0.0.1"
port = 8888
```

