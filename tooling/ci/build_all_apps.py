#!/usr/bin/env python3

import os
import subprocess
import sys
import yaml
import click

targets = [
    "pic30f_template",
    "pic33e_template",
    "sniffer",
    "steering_wheel",
    "te",
    "bms-master",
    "dash",
    "dcu",
    "iib",
    "hw",
]


def allow_fail(allowed, test):
    target, app = test
    return target + "/" + app in allowed


def list_apps(target):
    result = subprocess.check_output(f"make -s -C {target} clean", shell=True)
    result = subprocess.check_output(f"make -s -C {target} list-apps", shell=True)
    apps = result.decode("utf-8").strip().split(" ")
    return apps


@click.command()
@click.option("--xc16/--noxc16", default=False)
@click.option("--linux/--nolinux", default=False)
@click.option("--config")
def main(xc16, linux, config):
    with open(config) as f:
        config = yaml.load(f.read())

    hardware_targets = []
    if xc16 == True:
        hardware_targets.append("xc16")

    if linux == True:
        hardware_targets.append("linux")

    results = []

    for platform in hardware_targets:
        for target in targets:
            apps = list_apps(target)
            if len(apps) == 0:
                apps.append("main")
            for app in apps:
                r = subprocess.run(
                    ["make", "-C", target, f"APP={app}", f"TARGET={platform}"]
                )

                status = "fail"
                if r.returncode == 0:
                    status = "success"

                if allow_fail(config["allow_fail"], (target, app)):
                    status = "warning"

                results.append((status, target, platform, app, r.returncode))

    for status, target, platform, app, rcode in [
        result for result in results if result[0] != "fail"
    ]:
        print(f"{status}: APP={app}, TARGET={platform}, {target} exited with {rcode}")

    for status, target, app, platform, rcode in [
        result for result in results if result[0] == "fail"
    ]:
        print(f"{status}: APP={app}, TARGET={platform}, {target} exited with {rcode}")

    if len([result for result in results if result[0] == "fail"]) != 0:
        sys.exit(1)


if __name__ == "__main__":
    main()
