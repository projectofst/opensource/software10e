from setuptools import setup, find_packages

setup(
    name="fst-cantools",
    description="CAN library for FST Lisboa",
    version="0.9",
    author="Joao Freitas",
    author_email="joaj.freitas@gmail.com",
    license="GPLv3",
    url="https://github.com/joajfreitas/fcp",
    ##download_url="https://github.com/joajfreitas/fcp/archive/v0.1.tar.gz",
    packages=find_packages(),
    install_requires=[],
)
