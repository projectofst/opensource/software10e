#!/usr/bin/env python3

import click
import os
import sys

from pathlib import Path


@click.command()
@click.argument("tool")
@click.argument("pic")
@click.argument("hex")
def remote_program(tool, pic, hex):
    ipe_cmd = "/opt/microchip/mplabx/v5.40/sys/java/*/*/bin/java -jar /opt/microchip/mplabx/v5.40/mplab_platform/mplab_ipe/ipecmd.jar"
    print(f"{ipe_cmd} -T{tool} -P{pic} -M -OL -F{hex}")
    os.system(f"{ipe_cmd} -T{tool} -P{pic} -M -OL -F{hex}")

    os.system("rm -rf MPLAB* log.*")


@click.command()
@click.argument("hex", type=Path)
@click.argument("dst_ip")
@click.argument("tool")  # PKK3 or PKK4
@click.argument("pic")  # dsPIC33EP256MU806 or dsPIC30F6012A
def local_program(hex, dst_ip, tool, pic):
    os.system(f"scp {hex} {dst_ip}:/home/lv_room/bin/{hex.stem}.hex")
    os.system(
        f"sudo python /home/lv_room/software10e/tooling/remote_testbench/flash/flash_pic.py remote-program {tool} {pic} /home/lv_room/bin/{hex.stem}.hex"
    )


@click.group()
def main():
    pass


main.add_command(remote_program)
main.add_command(local_program)

if __name__ == "__main__":
    main()
