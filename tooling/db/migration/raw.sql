create table raw (
	timestamp timestamp with time zone,
	data numeric,
	sid smallint,
	dlc smallint, 
	car text);
