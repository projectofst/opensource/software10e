import psycopg2
import sys
import os


if len(sys.argv) != 6:
    print("Usage: dump_raw.py <user> <password> <host> <port> <database>")
    sys.exit()

connection = psycopg2.connect(
    user=sys.argv[1],
    password=sys.argv[2],
    host=sys.argv[3],
    port=sys.argv[4],
    database=sys.argv[5],
)
