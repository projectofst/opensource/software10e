import psycopg2
import sys

if len(sys.argv) != 6:
    print("Usage: dump_raw.py <user> <password> <host> <port> <database>")
    sys.exit()

ids = [
    (0,),
    (1,),
    (6,),
    (7,),
    (16,),
    (21,),
    (48,),
    (53,),
    (61,),
    (66,),
    (67,),
    (70,),
    (71,),
    (80,),
    (93,),
    (94,),
    (101,),
    (110,),
    (125,),
    (130,),
    (131,),
    (134,),
    (135,),
    (144,),
    (157,),
    (189,),
    (221,),
    (224,),
    (253,),
    (285,),
    (301,),
    (329,),
    (368,),
    (398,),
    (494,),
    (520,),
    (521,),
    (522,),
    (528,),
    (554,),
    (560,),
    (586,),
    (590,),
    (642,),
    (643,),
    (644,),
    (645,),
    (646,),
    (647,),
    (648,),
    (649,),
    (710,),
    (874,),
    (877,),
    (880,),
    (897,),
    (906,),
    (909,),
    (912,),
    (936,),
    (937,),
    (938,),
    (940,),
    (944,),
    (948,),
    (949,),
    (950,),
    (951,),
    (1025,),
    (1032,),
    (1033,),
    (1034,),
    (1036,),
    (1042,),
    (1044,),
    (1045,),
    (1046,),
    (1053,),
    (1064,),
    (1065,),
    (1072,),
    (1076,),
    (1077,),
    (1078,),
    (1081,),
    (1085,),
    (1096,),
    (1097,),
    (1098,),
    (1099,),
    (1104,),
    (1108,),
    (1109,),
    (1110,),
    (1130,),
    (1133,),
    (1136,),
    (1140,),
    (1141,),
    (1142,),
    (1161,),
    (1165,),
    (1168,),
    (1172,),
    (1173,),
    (1174,),
    (1181,),
    (1193,),
    (1197,),
    (1200,),
    (1204,),
    (1205,),
    (1206,),
    (1207,),
    (1213,),
    (1232,),
    (1236,),
    (1237,),
    (1238,),
    (1239,),
    (1245,),
    (1264,),
    (1268,),
    (1269,),
    (1270,),
    (1271,),
    (1277,),
    (1292,),
    (1296,),
    (1297,),
    (1300,),
    (1301,),
    (1302,),
    (1324,),
    (1328,),
    (1332,),
    (1333,),
    (1334,),
    (1335,),
    (1356,),
    (1364,),
    (1365,),
    (1389,),
    (1396,),
    (1397,),
    (1428,),
    (1429,),
    (1453,),
    (1456,),
    (1460,),
    (1461,),
    (1485,),
    (1492,),
    (1493,),
    (1503,),
    (1517,),
    (1524,),
    (1525,),
    (1549,),
    (1581,),
    (1600,),
    (1601,),
    (1602,),
    (1603,),
    (1612,),
    (1613,),
    (1616,),
    (1632,),
    (1633,),
    (1634,),
    (1635,),
    (1644,),
    (1645,),
    (1648,),
    (1676,),
    (1680,),
    (1712,),
    (1741,),
    (1804,),
    (1806,),
    (1815,),
    (1818,),
    (1836,),
    (1838,),
    (1847,),
    (1850,),
    (1868,),
    (1904,),
    (1936,),
    (1966,),
    (1990,),
    (1997,),
    (2000,),
    (2032,),
    (2036,),
    (2037,),
]
try:
    connection = psycopg2.connect(
        user=sys.argv[1],
        password=sys.argv[2],
        host=sys.argv[3],
        port=sys.argv[4],
        database=sys.argv[5],
    )

    cursor = connection.cursor()
    # Print PostgreSQL Connection properties
    print(connection.get_dsn_parameters(), "\n")

    # Print PostgreSQL version
    cursor.execute("SELECT version();")
    record = cursor.fetchone()
    print("You are connected to - ", record, "\n")

    # (datetime.datetime(2019, 8, 11, 12, 11, 33, 175630), Decimal('0'), 2037, 0, 'fst09e'

    for id in ids[10:]:
        print("start", id)
        id = int(id[0])
        cursor.execute(f"select * from raw where sid={id}")
        rows = cursor.fetchmany(1000)
        f = open(f"{id}.raw", "w")

        for row in rows:
            date = row[0]
            data = row[1]
            sid = row[2]
            dlc = row[3]
            car = row[4]
            f.write(f"{str(date)},{str(data)},{sid},{dlc},{car}\n")

        f.close()
        print("done")


except (Exception, psycopg2.Error) as error:
    print("Error while connecting to PostgreSQL", error)
finally:
    # closing database connection.
    if connection:
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
