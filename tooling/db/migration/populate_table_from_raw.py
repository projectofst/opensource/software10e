import psycopg2
from psycopg2.extras import execute_values


import sys
import json

from ww import f

from pprint import pprint

from fcp import Fcp, Spec
from fst_cantools import *

import datetime


user = "g03285_grafana"
host = "db.tecnico.ulisboa.pt"
port = 5432
database = "g03285_grafana"


def main():
    connection = psycopg2.connect(
        user=user, password=sys.argv[1], host=host, port=port, database=database
    )

    cur = connection.cursor()

    json_file = sys.argv[2]
    with open(json_file) as f:
        r = f.read()

    j = json.loads(r)
    spec = Spec()
    spec.decompile(j)

    fcp = Fcp(spec)

    # cur.execute("select distinct sid from raw;")
    # sids = cur.fetchall()

    # for sid in sids:
    #    sid = sid[0]
    #    msg = Message(sid, 0, 0, 0)
    #    m = fcp.find_msg(msg)
    #    if m != None:
    #        for sig in m.signals:
    #            cur.execute(f"select to_regclass('{sig}');")
    #            r = cur.fetchone()[0]
    #            if r != None:
    #                print("remove", r)
    #                cur.execute(f"drop table {sig}")
    #            print("create", sig)
    #            cur.execute(f"create table {sig} (time timestamp with time zone, value numeric)")

    # connection.commit()

    # return

    # (datetime.datetime(2019, 7, 29, 13, 16, 19, 322940), Decimal('893386753'), 301, 8, 'fst09e')

    cur.execute("select name, start, stop from log")
    logs = cur.fetchall()

    for log in logs:
        name, start, stop = log
        print(name)

        print(
            f"select timestamp, sid, dlc, data from raw where timestamp >= '{start}' and timestamp <= '{stop}';"
        )
        cur.execute(
            f"select timestamp, sid, dlc, data from raw where timestamp >= '{start}' and timestamp <= '{stop}';"
        )
        raws = cur.fetchall()

        timeseries = {}
        for raw in raws:
            timestamp, sid, dlc, data = raw
            d4 = []

            data = int(data)
            d4.append((data >> 48) & 0xFFFF)
            d4.append((data >> 32) & 0xFFFF)
            d4.append((data >> 16) & 0xFFFF)
            d4.append((data >> 0) & 0xFFFF)
            msg = Message(int(sid), int(dlc), d4, timestamp)
            # print(msg.sid, msg.dlc)
            name, signals = fcp.decode_msg(msg)

            if name == "":
                continue

            for sig_name, signal in signals.items():
                if sig_name not in timeseries.keys():
                    timeseries[sig_name] = []

                timeseries[sig_name].append((timestamp, signal))

        for sig_name, timeserie in timeseries.items():
            print(sig_name)
            execute_values(
                cur, f"insert into {sig_name} (time, value) VALUES %s", timeserie
            )
            # for time, value in timeserie:
            #    cur.execute(f"insert into {sig_name} (time, value) VALUES ('{time}', {value});")

        connection.commit()


if __name__ == "__main__":
    main()
