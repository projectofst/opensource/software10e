import psycopg2
import sys
import os

from schema import create_raw_schema


if len(sys.argv) != 7:
    print("Usage: dump_raw.py <user> <password> <host> <port> <database> <raws>")
    sys.exit()

connection = psycopg2.connect(
    user=sys.argv[1],
    password=sys.argv[2],
    host=sys.argv[3],
    port=sys.argv[4],
    database=sys.argv[5],
)

raw_path = sys.argv[6]

cursor = connection.cursor()

create_raw_schema(cursor)
connection.commit()

# 2019-07-29 12:46:06.699840,0,1044,2,fst09e

# data = [
#    {'hero': 'Spider-Man', 'alter_ego': 'Peter Parker'},
#    {'hero': 'Captain Marvel', 'alter_ego': 'Carol Danvers'},
#    {'hero': 'Superman', 'alter_ego': 'Val Zod'},  # Look up Earth 2 Superman
#    {'hero': 'Batman', 'alter_ego': 'Bruce Wayne'}
# ]
#
# query = "INSERT INTO superheroes (hero, alter_ego) VALUES (%(hero)s, %(alter_ego)s);"
#
# with db.cursor() as c:
#    for d in data:
#        c.execute(query, d)

query = "INSERT INTO raw (time, sid, data, dlc, car) VALUES (%s, %s, %s, %s, %s), );"

dirs = os.listdir(sys.argv[6])
dirs_size = len(dirs)
for i, d in enumerate(dirs):

    print(f"file: {d}, progress: {i/dirs_size*100}")
    datas = []
    with open(raw_path + "/" + d) as f:
        lines = f.readlines()
        size = len(lines)
        for j, line in enumerate(lines):
            if j % 1000 == 0:
                print(f"progress: {j/size*100}")
            date, data, sid, dlc, car = line.split(",")
            datas.append((date, sid, data, dlc, car))

        cursor.executemany(query, datas)


connection.commit()
connection.close()
