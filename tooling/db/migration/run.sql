create table log (
	start timestamp with time zone,
	stop timestamp with time zone,
	name text,
	description text
);

create table run (
	start timestamp with time zone,
	stop timestamp with time zone,
	number int
);
