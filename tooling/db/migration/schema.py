def create_raw_schema(cursor):
    cursor.execute(
        """create table raw (
        time timestamp with timezone,
        dlc smallint,
        sid integer,
        data numeric,
        car text
    )
    """
    )
