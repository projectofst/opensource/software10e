#!/bin/bash

source var.sh

sigint()
{
	echo "bye"
	kill -9 $vsniffer_id
	kill -9 $middleman_id
	#kill -9 $vio_adc_id
	kill -9 $te_id
}

trap 'sigint' INT

#cd "$software10e/bms-master"; make fake
#cd "$software10e/iib"; make fake

cd $here

python "$fake_lib/vsniffer/vsniffer.py" --verbose "$fake_lib/vsniffer/config.toml" > vsniffer.log &
vsniffer_id=$!

python "$fake_lib/middleman/middleman.py" --verbose "$fake_lib/middleman/config.toml" > middleman.log &
middleman_id=$!

sleep 2


#python "$fake_lib/vio_adc/vio_adc.py" "127.0.0.1" 8888 10 > vio_adc.log &
#vio_adc_id=$!

$software10e/te/bin/te.out > te.log &
te_id=$!

echo "setup done"

while :
do
	sleep 10 || break
done
