#!/bin/bash

source var.sh

sigint()
{
	echo "bye"
	kill -9 $vsniffer_id
	kill -9 $amk_id
	kill -9 $master_id
	kill -9 $iib_id
	kill -9 $middleman_id
	kill -9 $te_id
}

trap 'sigint' INT

#cd "$software10e/bms-master"; make fake
#cd "$software10e/iib"; make fake

cd "$here"

python3 "$fake_lib/vsniffer/vsniffer.py" --verbose "$fake_lib/vsniffer/config.toml" > vsniffer.log &
vsniffer_id=$!

python "$fake_lib/middleman/middleman.py" "$fake_lib/middleman/config.toml" > middleman.log &
middleman_id=$!

sleep 2

python3 "$fake_lib/amk/simulator.py" "$fake_lib/amk/config.toml" "../../can-ids-spec/fst10e.json" > amk.log &
amk_id=$!

"$software10e/bms-master/fake" > bms-master.log &
master_id=$!

"$software10e/iib/fake" > iib.log &
iib_id=$!

$software10e/te/fake > te.log &
te_id=$!

echo "setup done"

while :
do
	sleep 10 || break
done
