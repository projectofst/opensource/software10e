FTest Function
====================
\acos
-----------------
Return the arc cosine (measured in radians) of x.

The result is between 0 and pi.

\acosh
-----------------
Return the inverse hyperbolic cosine of x.

\asin
-----------------
Return the arc sine (measured in radians) of x.

The result is between -pi/2 and pi/2.

\asinh
-----------------
Return the inverse hyperbolic sine of x.

\atan
-----------------
Return the arc tangent (measured in radians) of x.

The result is between -pi/2 and pi/2.

\atan2
-----------------
Return the arc tangent (measured in radians) of y/x.

Unlike atan(y/x), the signs of both x and y are considered.

\atanh
-----------------
Return the inverse hyperbolic tangent of x.

\ceil
-----------------
Return the ceiling of x as an Integral.

This is the smallest integer >= x.

\copysign
-----------------
Return a float with the magnitude (absolute value) of x but the sign of y.

On platforms that support signed zeros, copysign(1.0, -0.0)
returns -1.0.

\cos
-----------------
Return the cosine of x (measured in radians).

\cosh
-----------------
Return the hyperbolic cosine of x.

\degrees
-----------------
Convert angle x from radians to degrees.

\dist
-----------------
Return the Euclidean distance between two points p and q.

The points should be specified as sequences (or iterables) of
coordinates.  Both inputs must have the same dimension.

Roughly equivalent to:
    sqrt(sum((px - qx) ** 2.0 for px, qx in zip(p, q)))

\erf
-----------------
Error function at x.

\erfc
-----------------
Complementary error function at x.

\exp
-----------------
Return e raised to the power of x.

\expm1
-----------------
Return exp(x)-1.

This function avoids the loss of precision involved in the direct evaluation of exp(x)-1 for small x.

\fabs
-----------------
Return the absolute value of the float x.

\factorial
-----------------
Find x!.

Raise a ValueError if x is negative or non-integral.

\floor
-----------------
Return the floor of x as an Integral.

This is the largest integer <= x.

\fmod
-----------------
Return fmod(x, y), according to platform C.

x % y may differ.

\frexp
-----------------
Return the mantissa and exponent of x, as pair (m, e).

m is a float and e is an int, such that x = m * 2.**e.
If x is 0, m and e are both 0.  Else 0.5 <= abs(m) < 1.0.

\fsum
-----------------
Return an accurate floating point sum of values in the iterable seq.

Assumes IEEE-754 floating point arithmetic.

\gamma
-----------------
Gamma function at x.

\gcd
-----------------
Greatest Common Divisor.

\hypot
-----------------
hypot(*coordinates) -> value

Multidimensional Euclidean distance from the origin to a point.

Roughly equivalent to:
    sqrt(sum(x**2 for x in coordinates))

For a two dimensional point (x, y), gives the hypotenuse
using the Pythagorean theorem:  sqrt(x*x + y*y).

For example, the hypotenuse of a 3/4/5 right triangle is:

    >>> hypot(3.0, 4.0)
    5.0

\isclose
-----------------
Determine whether two floating point numbers are close in value.

  rel_tol
    maximum difference for being considered "close", relative to the
    magnitude of the input values
  abs_tol
    maximum difference for being considered "close", regardless of the
    magnitude of the input values

Return True if a is close in value to b, and False otherwise.

For the values to be considered close, the difference between them
must be smaller than at least one of the tolerances.

-inf, inf and NaN behave similarly to the IEEE 754 Standard.  That
is, NaN is not close to anything, even itself.  inf and -inf are
only close to themselves.

\isfinite
-----------------
Return True if x is neither an infinity nor a NaN, and False otherwise.

\isinf
-----------------
Return True if x is a positive or negative infinity, and False otherwise.

\isnan
-----------------
Return True if x is a NaN (not a number), and False otherwise.

\isqrt
-----------------
Return the integer part of the square root of the input.

\lcm
-----------------
Least Common Multiple.

\ldexp
-----------------
Return x * (2**i).

This is essentially the inverse of frexp().

\lgamma
-----------------
Natural logarithm of absolute value of Gamma function at x.

\log
-----------------
log(x, [base=math.e])
Return the logarithm of x to the given base.

If the base not specified, returns the natural logarithm (base e) of x.

\log1p
-----------------
Return the natural logarithm of 1+x (base e).

The result is computed in a way which is accurate for x near zero.

\log10
-----------------
Return the base 10 logarithm of x.

\log2
-----------------
Return the base 2 logarithm of x.

\modf
-----------------
Return the fractional and integer parts of x.

Both results carry the sign of x and are floats.

\pow
-----------------
Return x**y (x to the power of y).

\radians
-----------------
Convert angle x from degrees to radians.

\remainder
-----------------
Difference between x and the closest integer multiple of y.

Return x - n*y where n*y is the closest integer multiple of y.
In the case where x is exactly halfway between two multiples of
y, the nearest even value of n is used. The result is always exact.

\sin
-----------------
Return the sine of x (measured in radians).

\sinh
-----------------
Return the hyperbolic sine of x.

\sqrt
-----------------
Return the square root of x.

\tan
-----------------
Return the tangent of x (measured in radians).

\tanh
-----------------
Return the hyperbolic tangent of x.

\trunc
-----------------
Truncates the Real x to the nearest Integral toward 0.

Uses the __trunc__ magic method.

\prod
-----------------
Calculate the product of all the elements in the input iterable.

The default start value for the product is 1.

When the iterable is empty, return the start value.  This function is
intended specifically for use with numeric values and may reject
non-numeric types.

\perm
-----------------
Number of ways to choose k items from n items without repetition and with order.

Evaluates to n! / (n - k)! when k <= n and evaluates
to zero when k > n.

If k is not specified or is None, then k defaults to n
and the function returns n!.

Raises TypeError if either of the arguments are not integers.
Raises ValueError if either of the arguments are negative.

\comb
-----------------
Number of ways to choose k items from n items without repetition and without order.

Evaluates to n! / (k! * (n - k)!) when k <= n and evaluates
to zero when k > n.

Also called the binomial coefficient because it is equivalent
to the coefficient of k-th term in polynomial expansion of the
expression (1 + x)**n.

Raises TypeError if either of the arguments are not integers.
Raises ValueError if either of the arguments are negative.

\nextafter
-----------------
Return the next floating-point value after x towards y.

\ulp
-----------------
Return the value of the least significant bit of the float x.






\get_cache_token
-----------------
Returns the current ABC cache token.

The token is an opaque object (supporting equality testing) identifying the
current version of the ABC cache for virtual subclasses. The token changes
with every call to register() on any ABC.

\namedtuple
-----------------
Returns a new subclass of tuple with named fields.

>>> Point = namedtuple('Point', ['x', 'y'])
>>> Point.__doc__                   # docstring for the new class
'Point(x, y)'
>>> p = Point(11, y=22)             # instantiate with positional args or keywords
>>> p[0] + p[1]                     # indexable like a plain tuple
33
>>> x, y = p                        # unpack like a regular tuple
>>> x, y
(11, 22)
>>> p.x + p.y                       # fields also accessible by name
33
>>> d = p._asdict()                 # convert to a dictionary
>>> d['x']
11
>>> Point(**d)                      # convert from a dictionary
Point(x=11, y=22)
>>> p._replace(x=100)               # _replace() is like str.replace() but targets named fields
Point(x=100, y=22)

\recursive_repr
-----------------
Decorator to make a repr function return fillvalue for a recursive call

\RLock
-----------------
None

\GenericAlias
-----------------
Represent a PEP 585 generic type

E.g. for t = list[int], t.__origin__ is list and t.__args__ is (int,).



\update_wrapper
-----------------
Update a wrapper function to look like the wrapped function

wrapper is the function to be updated
wrapped is the original function
assigned is a tuple naming the attributes assigned directly
from the wrapped function to the wrapper function (defaults to
functools.WRAPPER_ASSIGNMENTS)
updated is a tuple naming the attributes of the wrapper that
are updated with the corresponding attribute from the wrapped
function (defaults to functools.WRAPPER_UPDATES)

\wraps
-----------------
Decorator factory to apply update_wrapper() to a wrapper function

Returns a decorator that invokes update_wrapper() with the decorated
function as the wrapper argument and the arguments to wraps() as the
remaining arguments. Default arguments are as for update_wrapper().
This is a convenience function to simplify applying partial() to
update_wrapper().

\total_ordering
-----------------
Class decorator that fills in missing ordering methods

\cmp_to_key
-----------------
Convert a cmp= function into a key= function.

\reduce
-----------------
reduce(function, sequence[, initial]) -> value

Apply a function of two arguments cumulatively to the items of a sequence,
from left to right, so as to reduce the sequence to a single value.
For example, reduce(lambda x, y: x+y, [1, 2, 3, 4, 5]) calculates
((((1+2)+3)+4)+5).  If initial is present, it is placed before the items
of the sequence in the calculation, and serves as a default when the
sequence is empty.

\partial
-----------------
partial(func, *args, **keywords) - new function with partial application
of the given arguments and keywords.

\partialmethod
-----------------
Method descriptor with partial application of the given arguments
and keywords.

Supports wrapping existing descriptors and handles non-descriptor
callables as instance methods.

\lru_cache
-----------------
Least-recently-used cache decorator.

If *maxsize* is set to None, the LRU features are disabled and the cache
can grow without bound.

If *typed* is True, arguments of different types will be cached separately.
For example, f(3.0) and f(3) will be treated as distinct calls with
distinct results.

Arguments to the cached function must be hashable.

View the cache statistics named tuple (hits, misses, maxsize, currsize)
with f.cache_info().  Clear the cache and statistics with f.cache_clear().
Access the underlying function with f.__wrapped__.

See:  http://en.wikipedia.org/wiki/Cache_replacement_policies#Least_recently_used_(LRU)

\cache
-----------------
Simple lightweight unbounded cache.  Sometimes called "memoize".

\singledispatch
-----------------
Single-dispatch generic function decorator.

Transforms a function into a generic function, which can have different
behaviours depending upon the type of its first argument. The decorated
function acts as the default implementation, and additional
implementations can be registered using the register() attribute of the
generic function.

\singledispatchmethod
-----------------
Single-dispatch generic method descriptor.

Supports wrapping existing descriptors and handles non-descriptor
callables as instance methods.

\cached_property
-----------------
None

\+
-----------------
Same as a+b.

args: (x y ) 

(builtin_plus x y ) 

\-
-----------------
Same as a-b.

args: (x y ) 

(builtin_sub x y ) 

\*
-----------------
Same as a * b.

\/
-----------------
Same as a/b.

args: (x y ) 

(builtin_div x y ) 

\**
-----------------
Same as a**b.

args: (x y ) 

(builtin_pow x y ) 

\>
-----------------
Same as a > b.

args: (x y ) 

(builtin_gt x y ) 

\<
-----------------
Same as a < b.

args: (x y ) 

(builtin_lt x y ) 

\>=
-----------------
Same as a >= b.

args: (x y ) 

(builtin_ge x y ) 

\<=
-----------------
Same as a <= b.

args: (x y ) 

(builtin_le x y ) 

\=
-----------------
Same as a == b.

args: (x y ) 

(builtin_eq x y ) 

\abs
-----------------
Return the absolute value of the argument.

\append
-----------------
Appends an element y to the x list.

args: (x y ) 

(builtin_append x y ) 

\last
-----------------
Return the last element of a list.

\nth
-----------------


args: (x y ) 

(builtin_nth x y ) 

\car
-----------------
Return the first element of a list.

\cdr
-----------------
Return the elements of a list except for the first one.

\cons
-----------------
Return the list x and the element y at its tail.

\eq?
-----------------
Same as a is b.

\equal?
-----------------
Same as a == b.

\length
-----------------
Return the number of items in a container.

\list
-----------------
Return a list of arguments.

\list?
-----------------
Check if argument is a list.

\map
-----------------
map(func, *iterables) --> map object

Make an iterator that computes the function using arguments from
each of the iterables.  Stops when the shortest iterable is exhausted.

\max
-----------------
max(iterable, *[, default=obj, key=func]) -> value
max(arg1, arg2, *args, *[, key=func]) -> value

With a single iterable argument, return its biggest item. The
default keyword-only argument specifies an object to return if
the provided iterable is empty.
With two or more arguments, return the largest argument.

\min
-----------------
min(iterable, *[, default=obj, key=func]) -> value
min(arg1, arg2, *args, *[, key=func]) -> value

With a single iterable argument, return its smallest item. The
default keyword-only argument specifies an object to return if
the provided iterable is empty.
With two or more arguments, return the smallest argument.

\not
-----------------
Same as not a.

\null?
-----------------
Check if argument is null.

\number?
-----------------
Check if argument is a number.

\procedure?
-----------------
Return whether the object is callable (i.e., some kind of function).

Note that classes are callable, as are instances of classes with a
__call__() method.

\round
-----------------
Round a number to a given precision in decimal digits.

The return value is an integer if ndigits is omitted or None.  Otherwise
the return value has the same type as the number.  ndigits may be negative.

\symbol?
-----------------
Check if argument is a symbol.

\print
-----------------
Python print function.

\cmd
-----------------


args: (dev cmd arg1 arg2 arg3 ) 

(builtin_cmd dev cmd arg1 arg2 arg3 ) 

\set
-----------------


args: (dev cmd value ) 

(builtin_set dev cmd value ) 

\get
-----------------


args: (dev cmd ) 

(builtin_get dev cmd ) 

\unwrap
-----------------
None

\filter
-----------------
filter(function or None, iterable) --> filter object

Return an iterator yielding those items of iterable for which function(item)
is true. If function is None, return the items that are true.


\launch
-----------------
None

\send_adc
-----------------
None

\help
-----------------
Get the help string for a function.

\get_signal
-----------------
None

\~=
-----------------
Check for equality with a error margin.

