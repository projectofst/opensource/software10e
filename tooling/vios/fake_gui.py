from tkinter import *
import json
import tkinter as tk
from tkinter import ttk

# def show_values():
#    print (w1.get(), w2.get())

# master = Tk()
# w1 = Scale(master, from_=0, to=4096)
# w1.set(0)
# w1.pack()
# w2 = Scale(master, from_=0, to=4096)
# w2.set(0)
# w2.pack()
# Button(master, text='Show', command=show_values).pack()

# with open('vio_config.json') as f:
#  data = json.load(f)

# software10e/lib/can-ids-spec/fst10e.json
# Opening JSON file
with open("vio_config.json") as f:
    json = json.loads(f.read())


# print(json["devices"].values)

# root = tk.Tk()
# root.title("Virtual io's")
# tabControl = ttk.Notebook(root)


dev_num = 0

for device in json["devices"]:
    dev_num += 1

print(dev_num)

# self.add_tab()

# tab1 = ttk.Frame(tabControl)
# tab2 = ttk.Frame(tabControl)

# tabControl.add(tab1, text =json["devices"]["te"]["name"])
# tabControl.add(tab2, text ='device 2')
# tabControl.pack(expand = 1, fill ="both")

# ttk.Label(tab1,text ="Welcome to \ GeeksForGeeks").grid(column = 0,
#                                row = 0,
#                                padx = 30,
#                                pady = 30)


# ttk.Scale(tab1, from_=0, to=42)
# #w2 = Scale(master, from_=0, to=200, orient=HORIZONTAL)
# #w2.set(23)
# #w2.pack()
# #Button(master, text='Show', command=show_values).pack()


class App(Tk):
    def __init__(self, tittle):
        Tk.__init__(self)
        self.notebook = ttk.Notebook()
        self.notebook.grid(row=0)
        self.title(tittle)

    def add_tab(self, device, description):
        tab = Area(description)
        self.notebook.add(tab, text=device)


class Area(Frame):
    def __init__(self, description):
        Frame.__init__(self)
        self.label = Label(self, text=description)
        self.label.grid(row=1, column=0, padx=200, pady=10)
        self.name = description


my_app = App("Virtual io's")

for device in json["devices"]:

    description = json["devices"][device]["description"]

    my_app.add_tab(device, description)
    print(device, description)

    for adc in json["devices"][device]["adcs"]:
        print(adc)

my_app.mainloop()
