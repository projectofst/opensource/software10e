OBJDIR:=$(OBJDIR)/$(TARGET)/$(APP)/

ARCH_OBJECTS := $(addprefix $(OBJDIR), $(C_SOURCES:.c=.o))
ARCH_OBJECTS += $(addprefix $(OBJDIR), $(S_SOURCES:.s=.o))
