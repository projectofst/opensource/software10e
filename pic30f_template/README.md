# PIC30F Project Template

## Submodules
This template includes the `can-ids` and `lib_pic30f` submodules inside the
`lib` directory. 

Run `make submodules` at any time to update your submodules.

## Makefile
The makefile includes by default only the `lib` and `src` directories. Please use 
paths relative to these directories in your `#include` directives.

[//]: # "(e.g., to include `lib/lib_pic30f/CAN.h`, do `#include "lib_pic30f/CAN.h"`)."

Run `make` to compile your code. Run `make flash` to compile and program the 
PIC.

Run `make init URL=git@gitlab.com:projectofst/your-repo-name-here` to create a
new ready to go repo, based on the template.

Run `make verbose` to show the memory layout after linking.

The default pic is dsPIC30F6012A. Please change the `MCU` variable if you're
using a different pic in the 30F family.

Remember to change the `PROGNAME` variable.

## `src/main.c`
A minimal `main.c` is provided, with generic configuration bits.

## `AUTHORS`
This file includes the project's authors, just for cute historical purposes
for those unwilling to `git log`.
