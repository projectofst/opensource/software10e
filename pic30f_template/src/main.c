// DSPIC30F6012A Configuration Bit Settings

// 'C' source line config statements

// FOSC
#pragma config FOSFPR = HS2_PLL8 // Oscillator (HS2 w/PLL 8x)
#pragma config FCKSMEN = CSW_FSCM_OFF // Clock Switching and Monitor (Sw Disabled, Mon Disabled)

// FWDT
#pragma config FWPSB = WDTPSB_16 // WDT Prescaler B (1:16)
#pragma config FWPSA = WDTPSA_512 // WDT Prescaler A (1:512)
#pragma config WDT = WDT_OFF // Watchdog Timer (Disabled)

// FBORPOR
#pragma config FPWRT = PWRT_OFF // POR Timer Value (Timer Disabled)
#pragma config BODENV = NONE // Brown Out Voltage (Reserved)
#pragma config BOREN = PBOR_OFF // PBOR Enable (Disabled)
#pragma config MCLRE = MCLR_EN // Master Clear Enable (Enabled)

// FBS
#pragma config BWRP = WR_PROTECT_BOOT_OFF // Boot Segment Program Memory Write Protect (Boot Segment Program Memory may be written)
#pragma config BSS = NO_BOOT_CODE // Boot Segment Program Flash Memory Code Protection (No Boot Segment)
#pragma config EBS = NO_BOOT_EEPROM // Boot Segment Data EEPROM Protection (No Boot EEPROM)
#pragma config RBS = NO_BOOT_RAM // Boot Segment Data RAM Protection (No Boot RAM)

// FSS
#pragma config SWRP = WR_PROT_SEC_OFF // Secure Segment Program Write Protect (Disabled)
#pragma config SSS = NO_SEC_CODE // Secure Segment Program Flash Memory Code Protection (No Secure Segment)
#pragma config ESS = NO_SEC_EEPROM // Secure Segment Data EEPROM Protection (No Segment Data EEPROM)
#pragma config RSS = NO_SEC_RAM // Secure Segment Data RAM Protection (No Secure RAM)

// FGS
#pragma config GWRP = GWRP_OFF // General Code Segment Write Protect (Disabled)
#pragma config GCP = GSS_OFF // General Segment Code Protection (Disabled)

// FICD
#pragma config ICS = ICS_PGD // Comm Channel Select (Use PGC/EMUC and PGD/EMUD)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
// NOTE: Always include timing.h
#include "lib_pic30f/can.h"
#include "lib_pic30f/lib_pic30f.h"
#include "lib_pic30f/timing.h"
#include <stdio.h>

#include <stdint.h>

int main(void)
{
    uint64_t version = VERSION;

    config_can2();
    CANdata message;
    message.sid = 20;
    message.dlc = 8;
    message.data[3] = (version & 0xF) + ((version)&0xF0);
    message.data[2] = ((version >> 8) & 0xF) + ((version >> 8 & 0xF0));
    message.data[1] = ((version >> 16) & 0xF) + ((version >> 16) & 0xF0);
    message.data[0] = ((version >> 24) & 0xF) + ((version >> 24) & 0xF0);

    while (1) {
        write_to_can2_buffer(message, 1);
        // TODO: this doesn't work with TARGET=linux
        // The best solution is probably to create the __delay_ms and __delay_us function to lib_pic30f_linux
        //__delay_ms(1000);
        send_can2_buffer();
    }

    return 0;
}
