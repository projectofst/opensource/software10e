#ifndef __CONFIG_H__
#define __CONFIG_H__

#define VIO_PORT 8888
#define VIO_HOST "127.0.0.1"
#define SIM_TIME 100

#define VIO_ADC1_CONFIG {{"LV_current", 0}, {"HV_current", 0}, {"LV_voltage", 0}, {"adc4", 0}, {"adc5", 0}, {"adc6", 0}, {"adc7", 0}, {"adc8", 0}, {NULL, 0}}

#define CAN_ESSENTIAL	57005
#define CAN_SENSOR		48879
#define CAN_AMK			43962

#define PORT1			CAN_ESSENTIAL
#define PORT2			CAN_SENSOR

#define	SIM_DELTA_T_US	100
#endif
