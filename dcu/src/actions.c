#include "communication.h"
#include "dcu.h"
#include "dcu_config.h"
#include "dcu_pins.h"

#include "lib_pic30f/io.h"

#include <libpic30.h>
//#include "lib_pic30f_linux/xc.h"
#include <stdio.h>

// do the rtd sequence
void set_RTD_sequence(COMMON_MSG_RTD_ON rtd_message, Status_car* car)
{
    // star rtd sequence by starting the buzzer
    if ((rtd_message.step == RTD_STEP_INV_OK) && (car->RTD_mode == 0)) {
        car->RTD_buzz = 1;
    }

    return;
}

/*
//toggle debug mode as required by theinterface
//not doing anything with this mode for now
void set_bebug_mode(INTERFACE_MSG_DEBUG_TOGGLE debug_message){
    if(debug_message.device == DEBUG_TOGGLE_DCU){
        board.debug_mode = !(board.debug_mode);
    }
    return;
}
*/

#ifdef LINUX

void get_current_voltage(Status_car* car)
{

    car->LV_current[car->current_step] = get_adcbuf0();
    car->HV_current[car->current_step] = get_adcbuf1();
    car->LV_voltage[car->current_step] = get_adcbuf2();
    car->current_step = ((car->current_step + 1) % 16);

    return;
}

#endif

#ifndef LINUX

// gets the current values mesured by the sensor and converted in the adc and then converted by the respective fuctions
// missing fuction for the HV sensor
void get_current_voltage(Status_car* car)
{

    // get LV_current

    select_input_pin(15); /*AN15 pin -> LV_currentP*/
    start_samp();
    wait_for_done();
    car->LV_current[car->current_step] = get_adc_value(0);

    // get HV_current
    select_input_pin(14); /*AN14 pin -> HV_current*/
    start_samp();
    wait_for_done();
    car->HV_current[car->current_step] = get_adc_value(0);

    // get LV voltage
    select_input_pin(2); /* AN2 pin -> LV_voltage*/
    start_samp();
    wait_for_done();
    car->LV_voltage[car->current_step] = get_adc_value(0);

    car->current_step = ((car->current_step + 1) % 16);
    return;
}

#endif

// returns the average of the 16 current values mesured every 100 ms
uint16_t get_average(uint16_t value[16])
{
    int i;
    uint32_t sum = 0;

    for (i = 0; i < 16; i++) {
        sum += value[i];
    }
    return sum / 16;
}

// returns detections of the fuses and sc signals to be sent in the dcu status
void check_detections(DETECTIONS* det)
{
    det->DETECTION_SC_FRONT_BSPD = get_port(DET_SC_FRONT_BSPD); // OK
    det->DETECTION_SC_MH_FRONT = get_port(DET_SC_MH_FRONT); // OK
    det->DETECTION_SC_BSPD_AMS = get_port(DET_SC_BSPD_AMS); // OK
    det->DETECTION_SC_ORIGIN_MH = get_port(DET_SC_ORIGIN); //
    det->DETECTION_VCC_AMS = get_port(DET_VCC_AMS); // OK
    det->DETECTION_VCC_FANS_AMS = get_port(DET_VCC_FANS_AMS); // OK
    det->DETECTION_VCC_TSAL = get_port(DET_VCC_TSAL); // OK
    det->DETECTION_VCC_CUA = get_port(DET_VCC_CUA); // OK
    det->DETECTION_VCC_EM = get_port(DET_VCC_EM); // OK
    det->DETECTION_VCC_PUMPS = get_port(DET_VCC_PUMPS); // OK
    det->DETECTION_VCC_FANS = get_port(DET_VCC_FANS); // OK
    det->DETECTION_VCC_CUB = get_port(DET_VCC_CUB); // OK
    det->DETECTION_VCC_CAN_E = get_port(DET_VCC_CAN_E); // OK
    det->DETECTION_VCC_BL = get_port(DET_VCC_BL); // OK
}

// turn on/off brake light acording to bps_pressure
void set_brake_light(Status_car* car)
{
    // pressure is more then  2.5% of max value
    if (car->bps_pressure > BPS_PRESSURE_MAX / 100) {
        BL_SIG = 1;
    } else {
        BL_SIG = 0;
    }
    return;
}

void counters_init(Status_car* car)
{
    car->RTD_buzz_count = 0;
    car->current_step = 0;
    car->AS_emergency_buzzer_count = 0;
    car->AS_emergency = false;
    return;
}

void check_receiving_can(Status_car* car)
{

    if (received_can_flag) {
        received_can_flag = 0;
        if (car->LV_voltage_value > 3000) { // bigger than 3V ant it is receiving CAN
            GPIO1 ^= 1;
        }
    } else {
        if (car->LV_voltage_value > 3000) { // bigger than 3V and it is not receiving CAN
            GPIO1 = 1;
        }
    }

    if (car->LV_voltage_value < 3000) {
        received_can_flag = 0;
        GPIO1 = 0;
    }
    return;
}

void handle_RTD(Status_car* car)
{
    /* RTD */

    if (car->RTD_buzz == 1) {
        if (car->RTD_buzz_count < 2500) {
            BUZZ_SIG = 1;
        } else { // Ends buzzing
            BUZZ_SIG = 0;
            car->RTD_buzz_count = 0;
            car->RTD_buzz = 0;
            send_rtd_message();
        }
    } else {
        BUZZ_SIG = 0;
        car->RTD_buzz_count = 0;
    }

    return;
}

void handle_AS_emergency(Status_car* car)
{
    if (car->AS_emergency) {
        if (car->AS_emergency_buzzer_count < 9000) {
            // printf("Time: %d ,Sounding the buzzer, count: %d\n", time, car->AS_emergency_buzzer_count);
            BUZZ_SIG = 1;
        } else {
            BUZZ_SIG = 0;
            car->AS_emergency_buzzer_count = 0;
            car->AS_emergency = false;
        }
    }
}
