#ifndef __DCU_PINS_H__
#define __DCU_PINS_H__


#define DET_SC_BSPD_AMS             D,4   // UPDATED
#define DET_SC_MH_FRONT             D,6   // UPDATED
#define DET_SC_FRONT_BSPD           D,7   // UPDATED
#define DET_VCC_BL                  G,2   // UPDATED
#define DET_VCC_CAN_E               D,0   // UPDATED
#define DET_VCC_CUB                 D,10  // UPDATED
#define DET_VCC_FANS                D,8   // UPDATED
#define DET_VCC_CUA                 D,11  // UPDATED
#define DET_VCC_TSAL                C,13  // UPDATED
#define DET_VCC_FANS_AMS            F,2   // UPDATED
#define DET_VCC_AMS                 G,3   // UPDATED
#define DET_SC_ORIGIN               D,5   // UPDATED
#define DET_VCC_PUMPS               F,3   // UPDATED
#define DET_VCC_EM                  F,6   // UPDATED



#endif
