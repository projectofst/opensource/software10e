#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__

#include "can-ids-v2/can_ids.h"
#include "dcu.h"

can_t* get_can();
void receive_can(Status_car *car);
void send_status();
void send_rtd_message();
void send_current_voltage_status(Status_car *car);
void send_to_sensors(uint16_t dev_id, uint16_t msg_id);
void send_to_essential(uint16_t dev_id, uint16_t msg_id);
#endif
