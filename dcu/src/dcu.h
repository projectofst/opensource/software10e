#ifndef __DCU_H__
#define __DCU_H__

#include <stdbool.h>

/* IO definitions */
#define TRIS_BLUE_SIG               TRISBbits.TRISB0   // UPDATED
#define TRIS_LV_VOLTAGE             TRISBbits.TRISB2   // UPDATED
#define TRIS_AN4                    TRISBbits.TRISB4   // UPDATED  
#define TRIS_AN5                    TRISBbits.TRISB5   // UPDATED
#define TRIS_HV_CS                  TRISBbits.TRISB14  // UPDATED 
#define TRIS_LV_CS                  TRISBbits.TRISB15  // UPDATED
#define TRIS_LED_1                  TRISDbits.TRISD1   // UPDATED
#define TRIS_LED_2                  TRISDbits.TRISD2   // UPDATED
#define TRIS_GPIO1                  TRISGbits.TRISG15  // UPDATED    
#define TRIS_GPIO2                  TRISCbits.TRISC1   // UPDATED

#define TRIS_DET_VCC_CAN_E          TRISDbits.TRISD0    // UPDATED
#define TRIS_DET_SC_BSPD_AMS        TRISDbits.TRISD4    // UPDATED
#define TRIS_DET_SC_ORIGIN          TRISDbits.TRISD5  // UPDATED
#define TRIS_DET_SC_MH_FRONT        TRISDbits.TRISD6    // UPDATED
#define TRIS_DET_SC_FRONT_BSPD      TRISDbits.TRISD7    // UPDATED
#define TRIS_DET_VCC_FANS           TRISDbits.TRISD8    // UPDATED
#define TRIS_DET_VCC_CUB            TRISDbits.TRISD10   // UPDATED
#define TRIS_DET_VCC_CUA            TRISDbits.TRISD11   // UPDATED

#define TRIS_DET_VCC_BL             TRISGbits.TRISG2    // UPDATED 

#define TRIS_DCDC_SWITCH            TRISCbits.TRISC14 // UPDATED
#define TRIS_BL_SIG                 TRISBbits.TRISB12 // UPDATED
#define TRIS_BUZZ_SIG               TRISBbits.TRISB8  // UPDATED
#define TRIS_DET_VCC_TSAL           TRISCbits.TRISC13 // UPDATED
#define TRIS_DET_VCC_FANS_AMS       TRISFbits.TRISF2  // UPDATED
#define TRIS_DET_VCC_AMS            TRISGbits.TRISG3  // UPDATED
#define TRIS_DET_VCC_PUMPS          TRISFbits.TRISF3  // UPDATED
#define TRIS_DET_VCC_EM             TRISFbits.TRISF6  // UPDATED


/* Input definitions */
/*#define DET_SC_BSPD_AMS             PORTDbits.RD4   // UPDATED
#define DET_SC_MH_FRONT             PORTDbits.RD6   // UPDATED
#define DET_SC_FRONT_BSPD           PORTDbits.RD7   // UPDATED
#define DET_VCC_BL                  PORTGbits.RG2   // UPDATED
#define DET_VCC_CAN_E               PORTDbits.RD0   // UPDATED
#define DET_VCC_CUB                 PORTDbits.RD10  // UPDATED
#define DET_VCC_FANS                PORTDbits.RD8   // UPDATED
#define DET_VCC_CUA                 PORTDbits.RD11  // UPDATED
#define DET_VCC_TSAL                PORTCbits.RC13  // UPDATED
#define DET_VCC_FANS_AMS            PORTFbits.RF2   // UPDATED
#define DET_VCC_AMS                 PORTGbits.RG3   // UPDATED
#define DET_SC_ORIGIN               PORTDbits.RD5   // UPDATED
#define DET_VCC_PUMPS               PORTFbits.RF3   // UPDATED
#define DET_VCC_EM                  PORTFbits.RF6   // UPDATED
*/
#define GPIO2                       LATCbits.LATC1  // UPDATED
#define GPIO1                       LATGbits.LATG15 // UPDATED
#define BLUE_SIG                    LATBbits.LATB0  // UPDATED
#define LED_1                       LATDbits.LATD1
#define LED_2                       LATDbits.LATD2

#define DCDC_SWITCH                 LATCbits.LATC14  // UPDATED
#define BL_SIG                      LATBbits.LATB12  // UPDATED
#define BUZZ_SIG                    LATBbits.LATB8   // UPDATED



/*BPS related values*/
#define BPS_PRESSURE_MAX            25000


extern volatile int received_can_flag;


typedef struct {
    union {
        struct 
        {
            bool DETECTION_SC_FRONT_BSPD: 1; //lsb
            bool DETECTION_SC_MH_FRONT  : 1;
            bool DETECTION_SC_BSPD_AMS  : 1;
            bool DETECTION_SC_ORIGIN_MH : 1;
            bool DETECTION_VCC_AMS      : 1;
            bool DETECTION_VCC_FANS_AMS : 1;
            bool DETECTION_VCC_TSAL     : 1;
            bool DETECTION_VCC_CUA      : 1;
            bool DETECTION_VCC_EM       : 1;
            bool DETECTION_VCC_PUMPS    : 1;
            bool DETECTION_VCC_FANS     : 1;
            bool DETECTION_VCC_CUB      : 1;
            bool DETECTION_VCC_CAN_E    : 1;
            bool DETECTION_VCC_BL       : 1; //msb
        }; 
        uint16_t FUSE_DETECTIONS;
    };
} DETECTIONS;


typedef struct{
    bool RTD_buzz;
    bool RTD_mode;
    bool TS_ON;
    uint16_t LV_voltage[16];
    uint16_t LV_current[16];
    uint16_t HV_current[16];
    uint32_t LV_voltage_value;
    unsigned int bps_pressure;
    unsigned int apps;
    unsigned int current_step;
    unsigned int RTD_buzz_count;
    bool AS_emergency;
    unsigned int AS_emergency_buzzer_count;
}Status_car;

extern Status_car car;

#endif 
