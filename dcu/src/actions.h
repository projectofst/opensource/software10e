#ifndef __ACTIONS_H__
#define __ACTIONS_H__

#include "shared/rtd/rtd.h"
#include "dcu.h"

void set_RTD_sequence(COMMON_MSG_RTD_ON rtd_message, Status_car *car);
//void deactivate_RTD_mode(COMMON_MSG_RTD_OFF rtd_off_message, Status_car *car);
//void set_bebug_mode(INTERFACE_MSG_DEBUG_TOGGLE debug_message);
void get_current_voltage(Status_car *car);
unsigned int get_average(uint16_t value[16]);
unsigned int check_detections();
void set_brake_light(Status_car *car);
void counters_init (Status_car *car);
void check_receiving_can (Status_car *car);
//void get_apps(TE_MESSAGE te_message, Status_car *car);
void handle_RTD(Status_car *car);
uint16_t convert_HV_current( uint32_t adc_value);

void handle_AS_emergency(Status_car *car);

#endif
