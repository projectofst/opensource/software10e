#include "actions.h"
#include "dcu.h"
#include "dcu_config.h"
#include "dcu_pins.h"
#include "lib_pic30f/eeprom.h"
#include "lib_pic30f/version.h"

#include "can-ids-v2/can_cmd.h"

can_t global_can;

can_t* get_can()
{
    return &global_can;
}

void send_to_sensors(CANdata message)
{
    send_can2(message);
}

void send_to_essential(CANdata message)
{
    send_can1(message);
}

bool check_sensors(uint16_t dev_id, uint16_t msg_id)
{

    return dev_id == DEVICE_ID_DASH
        || msg_id == MSG_ID_MASTER_MASTER_STATUS
        || (dev_id == DEVICE_ID_MASTER && msg_id == MSG_ID_MASTER_CELL_V_INFO)
        || (dev_id == DEVICE_ID_IIB && (msg_id == MSG_ID_IIB_IIB_CAR || msg_id == MSG_ID_IIB_IIB_MOTOR || msg_id == MSG_ID_IIB_IIB_INV));
}

bool check_essential(uint16_t dev_id, uint16_t msg_id)
{
    return (dev_id == DEVICE_ID_ISABEL && (msg_id == MSG_ID_ISABEL_ISA_ENERGY || MSG_ID_ISABEL_ISA_CURRENT || MSG_ID_ISABEL_ISA_POWER)) || (dev_id == DEVICE_ID_IIB && msg_id == MSG_ID_IIB_IIB_MOTOR);
}

// called in the main while and selects what actions to take depending on the received message
void receive_can(Status_car* car)
{
    static char line = 0;
    CANdata* message;
    COMMON_MSG_RTD_ON rtd_message;

    if (!line) {
        message = pop_can1();
    }
    if (line) {
        message = pop_can2();
    }

    line ^= 1;

    if (message == NULL) {
        return;
    }

    ///////////////////////////////////
    can_t* can = get_can();

    decode_can(*message, can);
    cmd_dispatch(*message);
    ////////////////////////////////////

    /*set rtd sequence*/
    if (message->msg_id == CMD_ID_COMMON_RTD_ON) {
        parse_can_common_RTD(*message, &rtd_message);
        set_RTD_sequence(rtd_message, car);
    }

    /* sent to sensor can line */
    if (check_sensors(message->dev_id, message->msg_id)) {
        send_to_sensors(*message);
    }

    if (check_essential(message->dev_id, message->msg_id)) {
        send_to_essential(*message);
    }

    // TODO: remove this

    /* interface interaction */
    // USED TO GET CODE VERSION
    // TODO: IMPLEMENT FCP GETS
    // if(msg.dev_id == DEVICE_ID_INTERFACE){
    //     if (msg.msg_id == CMD_ID_COMMON_GET && msg.data[0] == DEVICE_ID_DCU){

    //         CANdata msg;
    //         uint16_t data[4];

    //         get_version(VERSION, data);

    //         msg.dev_id = DEVICE_ID_DCU;
    //         msg.msg_id = CMD_ID_COMMON_GET;
    //         msg.dlc = 8;
    //         msg.data[0] = DEVICE_ID_DCU;
    //         msg.data[3] = 0;

    //     }
    // }

    /*TE message to get the BPS pressure and Implausibilities*/
    if ((message->msg_id == MSG_ID_TE_TE_MAIN) && (message->dev_id == DEVICE_ID_TE)) {
        car->bps_pressure = can->te.te_main.te_main_BPSp;
        if (can->te.te_main.te_status_imp_apps_timer_exceeded || can->te.te_main.te_status_imp_apps_bps_timer_exceeded) {
            BLUE_SIG = 1;
        } else {
            BLUE_SIG = 0;
        }
    }

    if (message->dev_id == DEVICE_ID_IIB) {
        if (message->msg_id == MSG_ID_IIB_IIB_INFO) {
            car->RTD_mode = can->iib.iib_info.inv_rtd;
            if (car->RTD_buzz == 1 && can->iib.iib_info.inv_rtd == 0) {
                car->RTD_buzz = 0;
            }
        }
    }
    // The code below was to be possible for the Dynamic Unit to have acess to some specific messages from Essential CAN
    if (message->dev_id == DEVICE_ID_IIB) {
        if (message->msg_id == MSG_ID_IIB_IIB_CAR) {
            send_to_sensors(*message);
        }
    }

    if (message->dev_id == DEVICE_ID_MASTER) {
        if (message->msg_id == MSG_ID_MASTER_MASTER_STATUS) {
            send_to_sensors(*message);
        }
    }
    // Code for the DU ends here
}

// message containig the dcu status
void send_status()
{

    can_t* can = get_can();

    // TODO: remove detections middle step
    // TODO: bump
    DETECTIONS det;

    check_detections(&det);

    can->dcu.message_status.Detection_SC_BSPD_AMS = det.DETECTION_SC_BSPD_AMS;
    can->dcu.message_status.Detection_SC_Front_BSPD = det.DETECTION_SC_FRONT_BSPD;
    can->dcu.message_status.Detection_SC_MH_Front = det.DETECTION_SC_MH_FRONT;
    can->dcu.message_status.Detection_SC_Origin_MH = det.DETECTION_SC_ORIGIN_MH;
    can->dcu.message_status.Detection_VCC_AMS = det.DETECTION_VCC_AMS;
    can->dcu.message_status.Detection_VCC_CAN_E = det.DETECTION_VCC_CAN_E;
    can->dcu.message_status.Detection_VCC_CUA = det.DETECTION_VCC_CUA;
    can->dcu.message_status.Detection_VCC_CUB = det.DETECTION_VCC_CUB;
    can->dcu.message_status.Detection_VCC_DATA = 0; // what is this?
    can->dcu.message_status.Detection_VCC_EM = det.DETECTION_VCC_EM;
    can->dcu.message_status.Detection_VCC_FANS = det.DETECTION_VCC_FANS;
    can->dcu.message_status.Detection_VCC_FANS_AMS = det.DETECTION_VCC_FANS_AMS;
    can->dcu.message_status.Detection_VCC_PUMPS = det.DETECTION_VCC_PUMPS;
    can->dcu.message_status.Detection_VCC_TSAL = det.DETECTION_VCC_TSAL;

    can->dcu.message_status.BL_sig = BL_SIG;
    can->dcu.message_status.Blue_sig = BLUE_SIG;
    can->dcu.message_status.Buzz_sig = BUZZ_SIG;
    can->dcu.message_status.Dcdc_switch = DCDC_SWITCH;

    return;
}

// message containing the HV/LV current values and LV voltage values
void send_current_voltage_status(Status_car* car)
{

    can_t* can = get_can();

    car->LV_voltage_value = convert_LV_voltage(get_average(car->LV_voltage));

    can->dcu.dcu_message_current_voltage.LV_current = convert_LV_current(get_average(car->LV_current));
    can->dcu.dcu_message_current_voltage.HV_current = convert_HV_current(get_average(car->HV_current));
    can->dcu.dcu_message_current_voltage.LV_voltage = car->LV_voltage_value;

    return;
}

// message containing the next step of the rtd sequence
void send_rtd_message()
{
    CANdata message;
    message.dev_id = DEVICE_ID_DCU;
    message.msg_id = CMD_ID_COMMON_RTD_ON;
    message.dlc = 2;
    message.data[0] = RTD_STEP_DCU;
    write_to_can1_buffer(message, 1);
    return;
}
