#include "dcu_config.h"
#include "can-ids-v2/can_log.h"
#include "communication.h"
#include "dcu.h"
#include <libpic30.h>

extern volatile int received_can_flag;

// define self device id
unsigned int device_id(void)
{
    return DEVICE_ID_DCU;
}

// trap handler
void trap_handler(TrapType type)
{

    write_to_can1_buffer(logE(LOG_TRAP_HANDLER_TYPE, type, 0, 0), 0);
    return;
}

// define pins as outputs/inputs
#ifdef LINUX
void config_pins()
{
    return;
}
#endif

#ifndef LINUX

void config_pins()
{

    TRIS_HV_CS = 1;
    TRIS_LV_CS = 1;
    TRIS_LV_VOLTAGE = 1;
    TRIS_AN4 = 1;
    TRIS_AN5 = 1;
    TRIS_DET_SC_BSPD_AMS = 1;
    TRIS_DET_SC_MH_FRONT = 1;
    TRIS_DET_SC_FRONT_BSPD = 1;
    TRIS_DET_VCC_BL = 1;
    TRIS_DET_VCC_CAN_E = 1;
    TRIS_DET_VCC_CUB = 1;
    TRIS_DET_VCC_FANS = 1;

    TRIS_DET_VCC_CUA = 1;
    TRIS_DET_VCC_TSAL = 1;
    TRIS_DET_VCC_FANS_AMS = 1;
    TRIS_DET_VCC_AMS = 1;
    TRIS_DET_SC_ORIGIN = 1;
    TRIS_DET_VCC_PUMPS = 1;
    TRIS_DET_VCC_EM = 1;

    TRIS_GPIO1 = 0;
    TRIS_GPIO2 = 0;
    TRIS_BLUE_SIG = 0;
    TRIS_DCDC_SWITCH = 0;
    TRIS_BL_SIG = 0;
    TRIS_BUZZ_SIG = 0;
    return;
}

#endif

#ifdef LINUX

void set_default_signals()
{
    return;
}

#endif

#ifndef LINUX

// set initial value for the signals
// check this for new dcu
void set_default_signals()
{
    /* brake light is normally OFF */
    BL_SIG = 1;
    /* buzzers are normally OFF   */
    BUZZ_SIG = 0;
    /* blue led is normally OFF   */
    BLUE_SIG = 0;
}

#endif

#ifdef LINUX

// config adc parameters struct
void config_adc_parameters(ADC_parameters *parameters)
{

    return;
}

#endif

#ifndef LINUX

// config adc parameters struct
void config_adc_parameters(ADC_parameters *parameters)
{
    /* works in idle mode
     * form set to unsigned integer
     * auto conversion
     * manual sampling
     * use internal voltage reference
     * manualy change between differrent inputs
     * no interruption
     * scan pins AN2, AN14 AN15
     * do not interrupt
     * turn the module on
     */
    parameters->idle = 1;
    parameters->form = 0;
    parameters->conversion_trigger = AUTO_CONV;
    parameters->sampling_type = MANUAL;
    parameters->voltage_ref = INTERNAL;
    parameters->sample_pin_select_type = MANUAL;
    parameters->smpi = 0;
    parameters->pin_select = AN2 | AN14 | AN15; // LV_VOLTAGE| LV_CURRENT | HV_CURRENT
    parameters->interrupt_priority = 4;
    parameters->turn_on = 1;
    return;
}
#endif

/* Select which messages to accept */
bool filter_can1(uint16_t sid)
{

    received_can_flag = 1;
    uint16_t dev_id = (sid)&0b11111;
    uint16_t msg_id = ((sid) >> 5) & 0b111111;

    if (msg_id == CMD_ID_COMMON_RTD_ON || msg_id == CMD_ID_COMMON_RTD_OFF)
        return true;

    if (dev_id == DEVICE_ID_TE && msg_id == MSG_ID_TE_TE_MAIN)
        return true;

    // if (dev_id == DEVICE_ID_INTERFACE) {
    // 	if (msg_id == CMD_ID_INTERFACE_DCU_TOGGLE ||
    // 			msg_id == CMD_ID_INTERFACE_DEBUG_TOGGLE){
    //         return true;
    //     }
    // }

    // if (msg_id == CMD_ID_COMMON_GET || msg_id == CMD_ID_COMMON_SET){
    //     return true;
    // }

    if (dev_id == DEVICE_ID_DASH)
    {
        return true;
    }

    return true;
}
