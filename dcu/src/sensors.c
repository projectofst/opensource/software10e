#include "sensors.h"

// converts adc value in to a current value in mA
// used for the LV current sensor
int16_t convert_LV_current(uint16_t adc_value)
{
    uint32_t Vout;
    uint16_t Ip = 0;

    /*convert adc_value to voltage*/
    Vout = (500UL * adc_value) / 4095;
    /*convert output voltage to measured current*/
    Ip = ((((int)Vout - VOE) * IPN_LV) * 16) / 10;

    return Ip;
}

int16_t convert_HV_current(uint32_t adc_value)
{

    int32_t Vout;
    uint16_t Ip = 0;

    /*convert adc_value to voltage */
    Vout = (500UL * adc_value) / 4096;
    /*convert output voltage to measured current*/
    Ip = (((Vout - VOE) * IPN_HV) * 8) / 100;

    return Ip;
}

uint16_t convert_LV_voltage(uint32_t adc_value)
{
    return (adc_value * 24000UL) / 4096;
}
