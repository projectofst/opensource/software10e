#ifndef __SENSORS_H__
#define __SENSORS_H__

#include <stdint.h>

#define VOE                         250
#define IPN_LV                      10
#define IPN_HV                      200
#define ADC_MAX_VALUE               4095 

int16_t convert_LV_current(uint16_t adc_value);
uint16_t convert_LV_voltage(uint32_t adc_value);
int16_t convert_HV_current(uint32_t adc_value);

#endif
