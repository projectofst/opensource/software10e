#ifndef __DCU_CONFIG_H__
#define __DCU_CONFIG_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <xc.h>
#include "lib_pic30f/timing.h"
#include "lib_pic30f/can.h"
#include "lib_pic30f/timer.h"
#include "lib_pic30f/adc.h"
#include "lib_pic30f/trap.h"

#include "can-ids-v2/can_ids.h"
#include "shared/rtd/rtd.h"

#include "dcu.h"

//config.c
void config_pins();
void set_default_signals();
void config_adc_parameters(ADC_parameters *parameters);
void config_timer3_us(unsigned int time, unsigned int priority);

#endif
