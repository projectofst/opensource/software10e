#include <stdint.h>

#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"

#include "dcu.h"
#include "lib_pic30f/can.h"
#include <stdio.h>

extern Status_car car;

uint16_t dev_get_id()
{
    return DEVICE_ID_DCU;
}

void dev_send_msg(CANdata msg)
{
    write_to_can1_buffer(msg, 0);

    return;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{

    multiple_return_t arg = { 0 };

    if (id == CMD_DCU_AS_EMERGENCY_BUZZER) {
        printf("Sounding the buzzer\n");
        car.AS_emergency = true;
    }

    return arg;
}
