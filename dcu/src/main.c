// DSPIC30F6012A Configuration Bit Settings

// 'C' source line config statements

// FOSC
#pragma config FOSFPR = HS2_PLL8 // Oscillator (HS2 w/PLL 8x)
#pragma config FCKSMEN = CSW_FSCM_OFF // Clock Switching and Monitor (Sw Disabled, Mon Disabled)

// FWDT
#pragma config FWPSB = WDTPSB_16 // WDT Prescaler B (1:16)
#pragma config FWPSA = WDTPSA_512 // WDT Prescaler A (1:512)
#pragma config WDT = WDT_ON // Watchdog Timer (Disabled)

// FBORPOR
#pragma config FPWRT = PWRT_OFF // POR Timer Value (Timer Disabled)
#pragma config BODENV = NONE // Brown Out Voltage (Reserved)
#pragma config BOREN = PBOR_OFF // PBOR Enable (Disabled)
#pragma config MCLRE = MCLR_EN // Master Clear Enable (Enabled)

// FBS
#pragma config BWRP = WR_PROTECT_BOOT_OFF // Boot Segment Program Memory Write Protect (Boot Segment Program Memory may be written)
#pragma config BSS = NO_BOOT_CODE // Boot Segment Program Flash Memory Code Protection (No Boot Segment)
#pragma config EBS = NO_BOOT_EEPROM // Boot Segment Data EEPROM Protection (No Boot EEPROM)
#pragma config RBS = NO_BOOT_RAM // Boot Segment Data RAM Protection (No Boot RAM)

// FSS
#pragma config SWRP = WR_PROT_SEC_OFF // Secure Segment Program Write Protect (Disabled)
#pragma config SSS = NO_SEC_CODE // Secure Segment Program Flash Memory Code Protection (No Secure Segment)
#pragma config ESS = NO_SEC_EEPROM // Secure Segment Data EEPROM Protection (No Segment Data EEPROM)
#pragma config RSS = NO_SEC_RAM // Secure Segment Data RAM Protection (No Secure RAM)

// FGS
#pragma config GWRP = GWRP_OFF // General Code Segment Write Protect (Disabled)
#pragma config GCP = GSS_OFF // General Segment Code Protection (Disabled)

// FICD
#pragma config ICS = ICS_PGD // Comm Channel Select (Use PGC/EMUC and PGD/EMUD)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <libpic30.h>
#include <xc.h>

#include "shared/scheduler/scheduler.h"

#include "lib_pic30f/eeprom.h"
#include "lib_pic30f/timing.h" // NOTE: Always include timing.h

#include "can-ids-v2/can_log.h"

#include "actions.h"
#include "communication.h"
#include "dcu.h"
#include "dcu_config.h"

Status_car car;
uint32_t time;

// Global declarations
volatile int received_can_flag = 0;

void timer1_callback()
{
    time++;
    if (car.AS_emergency)
        car.AS_emergency_buzzer_count++;

    if (car.RTD_buzz)
        car.RTD_buzz_count++;

    return;
}

int main(void)
{

    can_t* can = get_can();

    tasks_t tasks[] = {
        { .name = "send_I_V_status",
            .period = 100,
            .func = send_current_voltage_status,
            .args_on = 1,
            .args = (void*)&car },
        { .name = "brake_light",
            .period = 50,
            .func = set_brake_light,
            .args_on = 1,
            .args = (void*)&car },
        { .name = "check_receiving_can",
            .period = 500,
            .func = check_receiving_can,
            .args_on = 1,
            .args = (void*)&car },
        { .name = "receive_can",
            .period = 0,
            .func = receive_can,
            .args_on = 1,
            .args = (void*)&car },
        { .name = "handle_rtd",
            .period = 20,
            .func = handle_RTD,
            .args_on = 1,
            .args = (void*)&car },
        { .name = "get_I_V",
            .period = 50,
            .func = get_current_voltage,
            .args_on = 1,
            .args = (void*)&car },
        { .name = "send_status",
            .period = 100,
            .func = send_status,
            .args_on = 0 },
        { .name = "send_can",
            .period = 0,
            .func = send_can1_buffer,
            .args_on = 0 },
        { .name = "handle_AS_emergency",
            .period = 20,
            .func = handle_AS_emergency,
            .args_on = 1,
            .args = (void*)&car },
    };

    scheduler_init_args(tasks, sizeof(tasks) / sizeof(tasks_t));

    ADC_parameters parameters;

    config_can1();
    config_can2();
    config_pins();

    config_timer1(1, 5);

    config_adc_parameters(&parameters);
    config_adc(parameters);

    set_default_signals();

    send_can1(logI(LOG_RESET_MESSAGE, RCON, 0, 0));

    // initialization counters
    counters_init(&car);

    while (1) {
        scheduler(time);
        dcu_send_msgs(can->dcu, time);
        ClrWdt();
    }
    return 0;
}
