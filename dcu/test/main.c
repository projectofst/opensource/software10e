#include <stdio.h>
#include <stdint.h>
#include "munit/munit.h"
#include "math.h"

#include "sensors.h"


static MunitResult lv_current_conversion_test(const MunitParameter params[], void* user_data) {
	munit_assert_int(convert_LV_current(2048), ==, 0);
	munit_assert_int(convert_LV_current(4095), ==, 4000);
    munit_assert_int(convert_LV_current(0), ==, -4000);

	return MUNIT_OK;
}

static MunitResult lv_voltage_conversion_test(const MunitParameter params[], void* user_data) {
	munit_assert_int(convert_LV_voltage(2048), ==, 12000);
	munit_assert_int(convert_LV_voltage(4096), ==, 24000);
    munit_assert_int(convert_LV_voltage(0), ==, 0);

	return MUNIT_OK;
}

static MunitResult hv_current_conversion_test(const MunitParameter params[], void* user_data) {
	munit_assert_int(convert_HV_current(2048), ==, 0);
	munit_assert_int(convert_HV_current(4096), ==, 4000);
    munit_assert_int(convert_HV_current(0), ==, -4000);

	return MUNIT_OK;
}

static MunitTest test_suite_tests[] = {
  	{ (char*) "/sensors/lv_current", lv_current_conversion_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/sensors/lv_voltage", lv_voltage_conversion_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/sensors/hv_current", hv_current_conversion_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
  	(char*) "", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

#include <stdlib.h>

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {


  	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
