import sys, time, click, socket

from threading import Thread
import queue


def adc_pack(id, value):
    bs = []
    bs.append(0)
    bs.append((value >> 24) & 0xFF)
    bs.append((value >> 16) & 0xFF)
    bs.append((value >> 8) & 0xFF)
    bs.append((value >> 0) & 0xFF)
    bs.append(len(id))
    for i, c in enumerate(id):
        bs.append(ord(c))
    return bytearray(bs)


def io_pack(value, pin, group):
    bs = []
    bs.append(1)
    bs.append(value)
    bs.append(pin)
    bs.append(ord(group[0]))
    return bytearray(bs)


def read_input_thread(name, q):
    while True:
        r = input("> ")
        print(f"Cmd: {r}")
        q.put(r)


def send_adcs_thread(period, sock, host, port, q):
    values = [1758, 2337, 410, 410, 410]
    values_io = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    def update_value(var, index, value):
        var[index] += value

    def update_value_io(var, index):
        var[index] ^= 1

    key_actions = {
        "q": lambda x: update_value(values, 0, x),
        "a": lambda x: update_value(values, 0, -x),
        "w": lambda x: update_value(values, 1, x),
        "s": lambda x: update_value(values, 1, -x),
        "e": lambda x: update_value(values, 2, x),
        "d": lambda x: update_value(values, 2, -x),
        "m": lambda x: update_value_io(values_io, 0),
        "n": lambda x: update_value_io(values_io, 1),
        "b": lambda x: update_value_io(values_io, 2),
        "v": lambda x: update_value_io(values_io, 3),
        "c": lambda x: update_value_io(values_io, 4),
        "x": lambda x: update_value_io(values_io, 5),
        "z": lambda x: update_value_io(values_io, 6),
        "l": lambda x: update_value_io(values_io, 7),
        "k": lambda x: update_value_io(values_io, 8),
        "j": lambda x: update_value_io(values_io, 9),
        "h": lambda x: update_value_io(values_io, 10),
        "g": lambda x: update_value_io(values_io, 11),
        "f": lambda x: update_value_io(values_io, 12),
        "p": lambda x: update_value_io(values_io, 13),
    }

    last_cmd = "q"
    last_jump = 10
    while True:
        if not q.empty():
            user = q.get()
            user = user.split(" ")

            if len(user) == 0 or (len(user) == 1 and user[0] == ""):
                cmd = last_cmd
                jump = last_jump
            else:
                cmd = user[0]

            if len(user) > 1:
                try:
                    jump = int(user[1])
                except Exception as e:
                    jump = 10
            else:
                jump = last_jump

            v = key_actions.get(cmd)

            if v is not None:
                v(jump)
                last_cmd = cmd
                last_jump = jump

        id = "LV_current"
        value = values[0]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "HV_current"
        value = values[1]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "LV_voltage"
        value = values[2]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        value = values_io[0]
        msg = io_pack(value, 4, "D")
        sock.sendto(msg, (host, port))
        # DET_SC_BSPD_AMS

        value = values_io[1]
        msg = io_pack(value, 6, "D")
        sock.sendto(msg, (host, port))
        # DET_SC_MH_FRONT

        value = values_io[2]
        msg = io_pack(value, 7, "D")
        sock.sendto(msg, (host, port))
        # DET_SC_FRONT_BSPD

        value = values_io[3]
        msg = io_pack(value, 2, "G")
        sock.sendto(msg, (host, port))
        # DET_VCC_BL

        value = values_io[4]
        msg = io_pack(value, 0, "D")
        sock.sendto(msg, (host, port))
        # DET_VCC_CAN_E

        value = values_io[5]
        msg = io_pack(value, 10, "D")
        sock.sendto(msg, (host, port))
        # DET_VCC_CUB

        value = values_io[6]
        msg = io_pack(value, 8, "D")
        sock.sendto(msg, (host, port))
        # DET_VCC_FANS

        value = values_io[7]
        msg = io_pack(value, 11, "D")
        sock.sendto(msg, (host, port))
        # DET_VCC_CUA

        value = values_io[8]
        msg = io_pack(value, 13, "C")
        sock.sendto(msg, (host, port))
        # DET_VCC_TSAL

        value = values_io[9]
        msg = io_pack(value, 2, "F")
        sock.sendto(msg, (host, port))
        # DET_VCC_FANS_AMS

        value = values_io[10]
        msg = io_pack(value, 3, "G")
        sock.sendto(msg, (host, port))
        # DET_VCC_AMS

        value = values_io[11]
        msg = io_pack(value, 5, "D")
        sock.sendto(msg, (host, port))
        # DET_SC_ORIGIN

        value = values_io[12]
        msg = io_pack(value, 3, "F")
        sock.sendto(msg, (host, port))
        # DET_VCC_PUMPS

        value = values_io[13]
        msg = io_pack(value, 6, "F")
        sock.sendto(msg, (host, port))
        # DET_VCC_EM

        time.sleep(period / 1000)


@click.group(invoke_without_command=True)
@click.argument("host")
@click.argument("port", type=int)
@click.argument("period", type=float)
def main(host, port, period):
    q = queue.Queue()

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    send_thread = Thread(target=send_adcs_thread, args=(period, sock, host, port, q))
    read_input = Thread(target=read_input_thread, args=("read_input", q))

    print(
        """
    Command syntax: <cmd> <increment>
    A single return will repeat the last command.

    q - increment LV_current
    a - decrement LV_current
    w - increment HV_current
    s - decrement HV_current
    e - increment LV_voltage
    d - decrement LV_voltage
    m - change SC_BSPD_AMS
    n - change SC_MH_FRONT
    b - change SC_FRONT_BSPD
    v - change VCC_BL
    c - change VCC_CAN_E
    x - change VCC_CUB
    z - change VCC_FANS 
    l - change VCC_CUA
    k - change VCC_TSAL 
    j - change VCC_FANS_AMS
    h - change VCC_AMS 
    g - change SC_ORIGIN
    f - change VCC_PUMPS
    p - change VCC_EM 
    """
    )

    send_thread.start()
    read_input.start()

    send_thread.join()
    read_input.join()


if __name__ == "__main__":
    main()
