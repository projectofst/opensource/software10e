MODULE_C_SOURCES:=communication.c
MODULE_C_SOURCES+=sw_config.c
MODULE_C_SOURCES+=driverInput.c
MODULE_C_SOURCES+=fcp.c
MODULE_C_SOURCES+=led.c
MODULE_C_SOURCES+=main.c
MODULE_C_SOURCES+=tasks.c

PWD:=src/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
