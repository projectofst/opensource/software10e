#ifndef NOCOMMUNICATION

#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/sw_can.h"

#include "lib_pic33e/can.h"
#include "lib_pic33e/flash.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/version.h"

#include "communication.h"
#include "sw_config.h"

#if 0
void initParams(void)
{
    int i;
	
    get_version(VERSION, version);
    for (i = 0; i < 4; i++)
        params[i] = version[i];

    // Read Maximum LEDs Intensity
    read_flash(params + 4, 0, 1);

    return;
}
#endif

#if 0
void getParams(uint16_t data)
{
    CANdata message;

    message.dev_id = DEVICE_ID_SW;
    message.msg_id = CMD_ID_COMMON_GET;
    message.dlc = 6;
    message.data[0] = DEVICE_ID_STEERING_WHEEL;
    message.data[1] = data;
    // Since data is of type unsigned it is always greater or equal to 0
    if (data < P_STEERING_WHEEL_LED_0) {
        message.data[2] = params[data];
        send_can1(message);
        // If the get is for LEDs
    } else if (data <= P_STEERING_WHEEL_LED_23) {
        message.data[2] = LEDs[data - P_STEERING_WHEEL_LED_0];
        send_can1(message);
    } else
        sendError();

    return;
}
#endif

#if 0
void setParams(uint16_t id, uint16_t data)
{

    disable_timer1();

#if 0
    if (id == P_STEERING_WHEEL_LED_INTENSITY) {
        if (data > 4095)
            params[P_STEERING_WHEEL_LED_INTENSITY] = 4095;
        else
            params[P_STEERING_WHEEL_LED_INTENSITY] = data;

        // If the maximum allowed intensity was changed, reduce LED intensity if
        // previous intensity was greater than the one now being set
        resetLEDIntensity(params[P_STEERING_WHEEL_LED_INTENSITY]); // check this
        write_flash(params + P_STEERING_WHEEL_LED_INTENSITY, 0, 1);
    } else if (id >= P_STEERING_WHEEL_LED_0 && id <= P_STEERING_WHEEL_LED_23) { // Verify if the set is for the LEDs
        if (data > params[P_STEERING_WHEEL_LED_INTENSITY])
            data = params[P_STEERING_WHEEL_LED_INTENSITY];
        LEDs[id - P_STEERING_WHEEL_LED_0] = data;
    } else
        sendError();

    enable_timer1();
#endif
    return;
}
#endif

#if 0
void initMessage(void)
{
    send_can1(make_reset_msg(DEVICE_ID_SW, RCON));
}
#endif

void selector_send(uint8_t selector, uint8_t position)
{
    msg_sw_sw_selector_t msg;
    msg.sw_selector_mux = selector;
    msg.sw_selector_value[selector] = position;

    send_can1(encode_sw_selector(msg, selector));
    return;
}

void button_send(uint8_t button)
{
    msg_sw_sw_button_t msg = {
        .sw_button_mux = button,
    };

    send_can1(encode_sw_button(msg));
}

#if 0
void sendError(void)
{
    CANdata message;

    message.dev_id = DEVICE_ID_STEERING_WHEEL;
    message.msg_id = MSG_ID_COMMON_PARSE_ERROR;
    message.dlc = 0;

    send_can2(message);

    return;
}
#endif

#endif
