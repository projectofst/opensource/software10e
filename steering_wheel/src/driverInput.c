#ifndef NODRIVERINPUT

#include <driverInput.h>
#include <lib_pic33e/timing.h>

#include "communication.h"
#include "sw_config.h"

button_t buttons[totalButtons];
selector_t selectors[totalSelectors];

#if !defined(LINUX)
// Interrupt service routine for the CNI interrupt.
void __attribute__((interrupt, no_auto_psv)) _CNInterrupt(void)
{
    uint8_t auxSelector;

    // Start by verifying if it was a button which caused the int. and which one

    if (buttons[0].state != PORT_BUT_1)
        buttons[0].changes++;
    else if (buttons[1].state != PORT_BUT_2)
        buttons[1].changes++;
    else if (buttons[2].state != PORT_BUT_3)
        buttons[2].changes++;
    else if (buttons[3].state != PORT_BUT_4)
        buttons[3].changes++;
    else if (buttons[4].state != PORT_BUT_5)
        buttons[4].changes++;
    else if (buttons[5].state != PORT_BUT_6)
        buttons[5].changes++;
    else if (buttons[6].state != PORT_BUT_7)
        buttons[6].changes++;
    else if (buttons[7].state != PORT_BUT_8)
        buttons[7].changes++;
    else if (buttons[8].state != PORT_BUT_9)
        buttons[8].changes++;
    else if (buttons[9].state != PORT_BUT_10)
        buttons[9].changes++;

    // If any of the previous conditions is true, its because it was one of the
    // selectors which caused the interrupt

    else {
        auxSelector = selector1Position();
        setSelector1();
        if (auxSelector != selector1Position())
            selector1Changed();
        auxSelector = selector2Position();
        setSelector2();
        if (auxSelector != selector2Position())
            selector2Changed();
        auxSelector = selector3Position();
        setSelector3();
        if (auxSelector != selector3Position())
            selector3Changed();
    }

    // Clear the flag
    IFS1bits.CNIF = 0;

    return;
}
#endif

void selector1Changed(void)
{
    // If ME 1 position changed, set as changed
    selectors[0].changed = 1;
    return;
}

void selector2Changed(void)
{
    // If ME 2 position changed, set as changed
    selectors[1].changed = 1;
    return;
}

void selector3Changed(void)
{
    // If ME 3 position changed, set as changed
    selectors[2].changed = 1;
    return;
}

void selector1Unchanged(void)
{
    // If ME 1 position was send, set as unchanged
    selectors[0].changed = 1;
    return;
}

void selector2Unchanged(void)
{
    // If ME 2 position was send, set as unchanged
    selectors[1].changed = 1;
    return;
}

void selector3Unchanged(void)
{
    // If ME 3 position was send, set as unchanged
    selectors[2].changed = 1;
    return;
}

uint8_t selector1Position(void)
{
    // Return Mechanical Encoder 1 position
    return selectors[0].position;
}

uint8_t selector2Position(void)
{
    // Return Mechanical Encoder 2 position
    return selectors[1].position;
}

uint8_t selector3Position(void)
{
    // Return Mechanical Encoder 3 position
    return selectors[2].position;
}

void setSelector1(void)
{
    // Initialize Mechanical Encoder 1 variable with current position
    selectors[0].ME_1 = PORT_ME1_1;
    selectors[0].ME_2 = PORT_ME1_2;
    selectors[0].ME_4 = PORT_ME1_4;
    // ME with 8 positions is being used, no need to update pin with value 8
    // selectors[0].ME_8 = PORT_ME1_8;

    return;
}

void setSelector2(void)
{
    // Initialize Mechanical Encoder 2 variable with current position
    selectors[1].ME_1 = PORT_ME2_1;
    selectors[1].ME_2 = PORT_ME2_2;
    selectors[1].ME_4 = PORT_ME2_4;
    // ME with 8 positions is being used, no need to update pin with value 8
    // selectors[1].ME_8 = PORT_ME2_8;

    return;
}

void setSelector3(void)
{
    // Initialize Mechanical Encoder 3 variable with current position
    selectors[2].ME_1 = PORT_ME3_1;
    selectors[2].ME_2 = PORT_ME3_2;
    selectors[2].ME_4 = PORT_ME3_4;
    // ME with 8 positions is being used, no need to update pin with value 8
    // selectors[2].ME_8 = PORT_ME3_8;

    return;
}

void driverInputInterrupt(void)
{
    // CNI: Change Notification Interrupt
    // Priority: 5
    IPC4bits.CNIP = 5;

    // Interrupt on change : any

    // Mechanical Encoder 1
    CNENEbits.CNIEE3 = 1; // Pin : RE3
    CNENEbits.CNIEE2 = 1; // Pin : RE2
    CNENEbits.CNIEE1 = 1; // Pin : RE1
    CNENEbits.CNIEE0 = 1; // Pin : RE0

    // Mechanical Encoder 2
    CNENDbits.CNIED7 = 1; // Pin : RD7
    CNENDbits.CNIED6 = 1; // Pin : RD6
    CNENDbits.CNIED5 = 1; // Pin : RD5
    CNENDbits.CNIED4 = 1; // Pin : RD4

    // Mechanical Encoder 3
    CNENBbits.CNIEB15 = 1; // Pin : RB15
    CNENBbits.CNIEB14 = 1; // Pin : RB14
    CNENBbits.CNIEB13 = 1; // Pin : RB13
    CNENBbits.CNIEB12 = 1; // Pin : RB12

    // Buttons
    CNENBbits.CNIEB6 = 1; // Pin : RB6
    CNENBbits.CNIEB7 = 1; // Pin : RB7
    CNENBbits.CNIEB8 = 1; // Pin : RB8
    CNENBbits.CNIEB9 = 1; // Pin : RB9
    CNENBbits.CNIEB10 = 1; // Pin : RB10
    CNENEbits.CNIEE4 = 1; // Pin : RE4
    CNENEbits.CNIEE5 = 1; // Pin : RE5
    CNENEbits.CNIEE6 = 1; // Pin : RE6
    CNENEbits.CNIEE7 = 1; // Pin : RE7
    CNENGbits.CNIEG7 = 1; // Pin : RG6

    IFS1bits.CNIF = 0; // Clear CNI interrupt flag
    IEC1bits.CNIE = 1; // Enable CNI interrupt

    return;
}

void setDriverInput(void)
{
    int i;

    // Set buttons as input
    TRIS_BUT_1 = 1;
    TRIS_BUT_2 = 1;
    TRIS_BUT_3 = 1;
    TRIS_BUT_4 = 1;
    TRIS_BUT_5 = 1;
    TRIS_BUT_6 = 1;
    TRIS_BUT_7 = 1;
    TRIS_BUT_8 = 1;
    TRIS_BUT_9 = 1;
    TRIS_BUT_10 = 1;

    // Set mechanical encoder 1 pins as input
    TRIS_ME1_1 = 1;
    TRIS_ME1_2 = 1;
    TRIS_ME1_4 = 1;
    TRIS_ME1_8 = 1;

    // Set mechanical encoder 2 pins as input
    TRIS_ME2_1 = 1;
    TRIS_ME2_2 = 1;
    TRIS_ME2_4 = 1;
    TRIS_ME2_8 = 1;

    // Set mechanical encoder 3 pins as input
    TRIS_ME3_1 = 1;
    TRIS_ME3_2 = 1;
    TRIS_ME3_4 = 1;
    TRIS_ME3_8 = 1;

    // Set initial state of Mechanical Encoders
    setSelector1();
    setSelector2();
    setSelector3();
    // Initially they are changed so a message with current position of each one
    // of the selectors is sent
    selector1Changed();
    selector2Changed();
    selector3Changed();

    // Initialize buttons as they were never pressed. Assumes buttons are released.
    // If such assumption wasn't made, initialize buttons.state with the value of
    // the correspondent Port - for cycle couldn't be used in this case
    for (i = 0; i < totalButtons; i++)
        buttons[i].changes = 0;

    // Initialize buttons state with the corresponding sensed in the IO pin in the PIC
    buttons[0].state = PORT_BUT_1;
    buttons[1].state = PORT_BUT_2;
    buttons[2].state = PORT_BUT_3;
    buttons[3].state = PORT_BUT_4;
    buttons[4].state = PORT_BUT_5;
    buttons[5].state = PORT_BUT_6;
    buttons[6].state = PORT_BUT_7;
    buttons[7].state = PORT_BUT_8;
    buttons[8].state = PORT_BUT_9;
    buttons[9].state = PORT_BUT_10;

    // Set interrupt for pin change notification
    driverInputInterrupt();

    return;
}

#endif
