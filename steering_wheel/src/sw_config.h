#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <xc.h>
#include <stdlib.h>
#include "driverInput.h"

#define totalButtons   10
#define totalSelectors  3  //ME 1, ME 2, ME 3
#define totalLEDDrivers 1
#define totalLEDs totalLEDDrivers*24 //each LED Driver has 24 channels
#define totalLEDBytes totalLEDs*15/10 // bytes equal 1.5x totalLeds (12 bits per LED)

/*
PROBLEM with bitpacking - struct occupies 2 bytes instead of 1 byte:
https://stackoverflow.com/questions/35830171/formatting-unions-inside-bitfields-in-c
*/

//Struct reorganized so it only occupies 1 byte
typedef union _selector{
  struct{
    uint8_t position : 4;
    uint8_t unused : 1;
  };
  struct{
    uint8_t ME_1 : 1;
    uint8_t ME_2 : 1;
    uint8_t ME_4 : 1;
    uint8_t ME_8 : 1;
    uint8_t changed : 1;
  };
} selector_t;

extern selector_t selectors[totalSelectors]; //ME 1, ME 2, M
//extern uint16_t params[P_STEERING_WHEEL_LED_0]; //All params not related to LEDs

//LED driver control pinout
#define LAT_SCLK    LATDbits.LATD1
#define LAT_SIN     LATDbits.LATD2
#define LAT_XLAT    LATDbits.LATD3
#define LAT_BLANK   LATCbits.LATC14

#define TRIS_SCLK   TRISDbits.TRISD1
#define TRIS_SIN    TRISDbits.TRISD2
#define TRIS_XLAT   TRISDbits.TRISD3
#define TRIS_BLANK  TRISCbits.TRISC14

#define PORT_SCLK   PORTDbits.RD1
#define PORT_SIN    PORTDbits.RD2
#define PORT_XLAT   PORTDbits.RD3
#define PORT_BLANK  PORTCbits.RC14

//Rotary Switch 1 aka Mechanical Encoder 1
//Pins 1-2 and 4-8 changed in PCB nomenclature
#define LAT_ME1_1   LATEbits.LATE2
#define LAT_ME1_2   LATEbits.LATE1
#define LAT_ME1_4   LATEbits.LATE0
#define LAT_ME1_8   LATEbits.LATE1

#define TRIS_ME1_1  TRISEbits.TRISE2
#define TRIS_ME1_2  TRISEbits.TRISE3
#define TRIS_ME1_4  TRISEbits.TRISE0
#define TRIS_ME1_8  TRISEbits.TRISE1

#define PORT_ME1_1  PORTEbits.RE2
#define PORT_ME1_2  PORTEbits.RE3
#define PORT_ME1_4  PORTEbits.RE0
#define PORT_ME1_8  PORTEbits.RE1

//Rotary Switch 2 aka Mechanical Encoder 2
//Pins 1-2 and 4-8 changed in PCB nomenclature
#define LAT_ME2_1   LATDbits.LATD6
#define LAT_ME2_2   LATDbits.LATD7
#define LAT_ME2_4   LATDbits.LATD4
#define LAT_ME2_8   LATDbits.LATD5

#define TRIS_ME2_1  TRISDbits.TRISD6
#define TRIS_ME2_2  TRISDbits.TRISD7
#define TRIS_ME2_4  TRISDbits.TRISD4
#define TRIS_ME2_8  TRISDbits.TRISD5

#define PORT_ME2_1  PORTDbits.RD6
#define PORT_ME2_2  PORTDbits.RD7
#define PORT_ME2_4  PORTDbits.RD4
#define PORT_ME2_8  PORTDbits.RD5

//Rotary Switch 3 aka Mechanical Encoder 3
//Pins 1-2 and 4-8 changed in PCB nomenclature
#define LAT_ME3_1   LATBbits.LATB14
#define LAT_ME3_2   LATBbits.LATB15
#define LAT_ME3_4   LATBbits.LATB12
#define LAT_ME3_8   LATBbits.LATB13

#define TRIS_ME3_1  TRISBbits.TRISB14
#define TRIS_ME3_2  TRISBbits.TRISB15
#define TRIS_ME3_4  TRISBbits.TRISB12
#define TRIS_ME3_8  TRISBbits.TRISB13

#define PORT_ME3_1  PORTBbits.RB14
#define PORT_ME3_2  PORTBbits.RB15
#define PORT_ME3_4  PORTBbits.RB12
#define PORT_ME3_8  PORTBbits.RB13

//Button 1
#define LAT_BUT_1   LATBbits.LATB6

#define TRIS_BUT_1  TRISBbits.TRISB6

#define PORT_BUT_1  PORTBbits.RB6

//Button 2
#define LAT_BUT_2   LATBbits.LATB7

#define TRIS_BUT_2  TRISBbits.TRISB7

#define PORT_BUT_2  PORTBbits.RB7

//Button 3
#define LAT_BUT_3   LATBbits.LATB8

#define TRIS_BUT_3  TRISBbits.TRISB8

#define PORT_BUT_3  PORTBbits.RB8

//Button 4
#define LAT_BUT_4   LATBbits.LATB9

#define TRIS_BUT_4  TRISBbits.TRISB9

#define PORT_BUT_4  PORTBbits.RB9

//Button 5
#define LAT_BUT_5   LATBbits.LATB10

#define TRIS_BUT_5  TRISBbits.TRISB10

#define PORT_BUT_5  PORTBbits.RB10

//Button 6
#define LAT_BUT_6   LATEbits.LATE4

#define TRIS_BUT_6  TRISEbits.TRISE4

#define PORT_BUT_6  PORTEbits.RE4

//Button 7
#define LAT_BUT_7   LATEbits.LATE5

#define TRIS_BUT_7  TRISEbits.TRISE5

#define PORT_BUT_7  PORTEbits.RE5

//Button 8
#define LAT_BUT_8   LATEbits.LATE6

#define TRIS_BUT_8  TRISEbits.TRISE6

#define PORT_BUT_8  PORTEbits.RE6

//Button 9
#define LAT_BUT_9   LATEbits.LATE7

#define TRIS_BUT_9  TRISEbits.TRISE7

#define PORT_BUT_9  PORTEbits.RE7

//Button 10
#define LAT_BUT_10   LATGbits.LATG6

#define TRIS_BUT_10  TRISGbits.TRISG6

#define PORT_BUT_10  PORTGbits.RG6

//Built-in LED
#define LAT_LED    LATGbits.LATG7

#define TRIS_LED   TRISGbits.TRISG7


#endif
