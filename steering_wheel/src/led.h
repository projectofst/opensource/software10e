#ifndef _LED_H_
#define _LED_H_

#include <stdint.h>

void resetLEDIntensity(uint16_t newIntensity);
void writeLED(void);
void beginLEDS(void);

#endif
