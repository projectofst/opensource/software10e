#include "shared/tlc5947/tlc5947.h"

#include "sw_config.h"

void tlc_xlat_set(bool state)
{
    LAT_XLAT = state;
}

void tlc_sclk_set(bool state)
{
    LAT_SCLK = state;
}

void tlc_sin_set(bool state)
{
    LAT_SIN = state;
}

void tlc_blank_set(bool state)
{
    LAT_BLANK = state;
}

void tlc_tris_xlat_set(tlc_pin_dir_t state)
{
    TRIS_XLAT = state;
}

void tlc_tris_sclk_set(tlc_pin_dir_t state)
{
    TRIS_SCLK = state;
}

void tlc_tris_sin_set(tlc_pin_dir_t state)
{
    TRIS_SIN = state;
}

void tlc_tris_blank_set(tlc_pin_dir_t state)
{
    TRIS_BLANK = state;
}
