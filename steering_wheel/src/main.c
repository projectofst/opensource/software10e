#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128 // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4 // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xc.h>

#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"

// NOTE: Always include timing.h
#include <lib_pic33e/can.h>
#include <lib_pic33e/pps.h>
#include <lib_pic33e/timer.h>
#include <lib_pic33e/timing.h>

#include <shared/scheduler/scheduler.h>
#include <tlc5947.h>

#include "communication.h"
#include "driverInput.h"
#include "led.h"
#include "sw_config.h"
#include "tasks.h"

uint32_t timer1_flag = 0;

void timer1_callback(void)
{
    timer1_flag++;
    return;
}

uint32_t sw_configs[CFG_SW_SIZE] = { VERSION };

tlc5947_t* tlc = NULL;

void visual_check()
{
    tlc5947_set_all(tlc, 2000);
    tlc5947_write(tlc);
    __delay_ms(2000);
    tlc5947_clear_all(tlc);
    tlc5947_write(tlc);
#if 0
    int i = 0;

    tlc5947_clear_all(tlc);
    tlc5947_write(tlc);
    for (i = 0; i < tlc->total / 2; i++) {
        tlc5947_set(tlc, 2000, i);
        tlc5947_set(tlc, 2000, tlc->total - i);
        tlc5947_write(tlc);
        __delay_ms(200);
    }

    for (i = 0; i <= tlc->total / 2; i++) {
        tlc5947_clear(tlc, i);
        tlc5947_clear(tlc, tlc->total - i);
        tlc5947_write(tlc);
        __delay_ms(200);
    }

    tlc5947_clear_all(tlc);
    tlc5947_write(tlc);

#endif
    return;
}

int main()
{

    uint16_t intensities[totalLEDs];
    tlc5947_t _tlc = tlc5947_init(totalLEDs, intensities);
    tlc = &_tlc;

    tasks_t sw_tasks[] = {
        { .period = 100, .func = poll_buttons },
        { .period = 100, .func = poll_selectors },
        { .period = 100, .func = update_leds },
        { .period = 1000, .func = send_status },
        { .period = 1, .func = receive_can }
    };

    // Configure the PPS for the CAN module.
    PPSUnLock;
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP64); // CAN1_TX at pin 46
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI75); // CAN1_RX at pin 45
    PPSLock;

    setDriverInput();
    config_can1(NULL);
    config_timer1(1, 4);

    tlc5947_begin(tlc);

    visual_check();

    cfg_config(sw_configs, CFG_SW_SIZE);
    send_can1(logI(LOG_RESET_MESSAGE, RCON, 0, 0));

    scheduler_init(sw_tasks, sizeof(sw_tasks) / sizeof(tasks_t));

    while (1) {
        scheduler(timer1_flag);
        ClrWdt();
    }
    return 0;
}
