#include "shared/tlc5947/tlc5947.h"

#include "can-ids-v2/sw_can.h"

#include "lib_pic33e/can.h"

#include "sw_config.h"

extern tlc5947_t* tlc;
extern selector_t selectors[]; // ME 1, ME 2, ME
extern button_t buttons[];

void update_leds(void)
{
    tlc5947_write(tlc);
}

void receive_can(void)
{
    if (!can1_rx_empty()) {
        CANdata msg = pop_can1();
        cmd_dispatch(msg);
        cfg_dispatch(msg);
    }

    return;
}

void poll_buttons(void)
{
    int i = 0;
    // Verify if any button was pressed. If it was send message
    for (i = 0; i < totalButtons; i++) {
        if (buttons[i].changes != 0) {
            button_send(i);
            buttons[i].changes = 0;
        }
    }
}

void poll_selectors(void)
{
    int i = 0;
    // Verify if any Mechanical Encoder's position changed. If it did, send message
    for (i = 0; i < totalSelectors; i++) {
        if (selectors[i].changed == 1) {
            selector_send(i, selectors[i].position);
            selectors[i].changed = 0;
        }
    }
}

void send_status(void)
{
    msg_sw_sw_status_t msg = {
        .status_me0_pos = selectors[0].position,
        .status_me1_pos = selectors[1].position,
        .status_me2_pos = selectors[2].position,
    };

    send_can1(encode_sw_status(msg));

    return;
}
