#include <stdbool.h>

#include "can-ids-v2/can_cfg.h"
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"

#include "shared/tlc5947/tlc5947.h"
#include "shared/types/types.h"

#include "sw_config.h"

extern uint16_t LEDs[];
extern uint32_t sw_configs[];
extern tlc5947_t* tlc;

void dev_send_msg(CANdata msg)
{
    send_can1(msg);
}

uint16_t dev_get_id()
{
    return DEVICE_ID_SW;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{
    multiple_return_t mt = { 0 };
    int i = 0;

    uint16_t luminosity = sw_configs[CFG_SW_LUMINOSITY];

    if (id == CMD_SW_LEDS_SET) {
        tlc5947_set(tlc, arg2 ? luminosity : arg2, arg1);
    } else if (id == CMD_SW_LEDS_CLEAR) {
        tlc5947_clear(tlc, arg1);
    } else if (id == CMD_SW_LEDS_SET_ALL) {
        tlc5947_set_all(tlc, arg1 ? luminosity : arg1);
    } else if (id == CMD_SW_LEDS_CLEAR_ALL) {
        tlc5947_clear_all(tlc);
    } else if (id == CMD_SW_LEDS_SET_BITFIELD) {
        uint32_t bitfield = arg2;
        bitfield |= (u32)arg1 << 16;
        tlc5947_set_all_bitfield(tlc, arg3 ? luminosity : arg3, bitfield);
    }
    return mt;
}
