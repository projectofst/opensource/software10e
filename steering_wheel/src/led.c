#ifndef NOLED

#include <led.h>
#include <lib_pic33e/timing.h>
#include <stdint.h>
#include <sw_config.h>

typedef struct {
    uint16_t intensity[totalLEDs];
} leds_t;

leds_t leds;

void resetLEDIntensity(uint16_t newIntensity)
{
    int i = 0;

    for (i = 0; i < totalLEDs; i++)
        if (leds.intensity[i] > newIntensity)
            leds.intensity[i] = newIntensity;

    return;
}

/*
 * H're yond cometh i warneth thee, the dark w'rds spoken bellow art not f'r
 * those of valorous mind.
 * Thee w're did warn and proce'd at thy owneth p'ril f'r yond beneath: "th're
 * be dragons"
 */
void writeLED(void)
{
    int8_t bits;
    int16_t channel;

    LAT_XLAT = 0;

    // 24 channels per TLC5974
    for (channel = 24 - 1; channel >= 0; channel--)
        // 12 bits per channel, send MSB first
        for (bits = 11; bits >= 0; bits--) {
            LAT_SCLK = 0;
            if (leds.intensity[channel] & (1 << bits))
                LAT_SIN = 1;
            else
                LAT_SIN = 0;
            LAT_SCLK = 1;
        }

    LAT_SCLK = 0;

    LAT_XLAT = 1;
    LAT_XLAT = 0;

    return;
}

/*
 * ¯\_(ツ)_/¯
 */
void beginLEDS(void)
{
    int i;

    TRIS_SCLK = 0;
    TRIS_SIN = 0;
    TRIS_XLAT = 0;
    TRIS_BLANK = 0;

    // For now, blank should always be initialized with a LOW
    // PCB could have a pulldown for blank and this pin could be not used
    LAT_BLANK = 0;

    // The data in the grayscale shift register are moved to the grayscale
    // data latch with a low-to-high transition on this pin
    LAT_XLAT = 0;

    // Initialize LEDs with OFF state
    for (i = 0; i < totalLEDs; i++)
        leds.intensity[i] = 0;

    return;
}

#endif
