#ifndef _DRIVERINPUT_H_
#define _DRIVERINPUT_H_

#include <xc.h>
#include <stdlib.h>
#include <stdint.h>
#include "sw_config.h"

typedef struct _button{
  uint8_t changes : 7;
  uint8_t state : 1;
} button_t;

#ifndef LINUX
void __attribute__ (( interrupt, no_auto_psv )) _CNInterrupt(void);
#endif
void selector1Changed(void);
void selector2Changed(void);
void selector3Changed(void);
void selector1Unchanged(void);
void selector2Unchanged(void);
void selector3Unchanged(void);
uint8_t selector1Position(void);
uint8_t selector2Position(void);
uint8_t selector3Position(void);
void setSelector1(void);
void setSelector2(void);
void setSelector3(void);
void driverInputVerify(void);
void driverInputInterrupt(void);
void setDriverInput(void);

#endif
