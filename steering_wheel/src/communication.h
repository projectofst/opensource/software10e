#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include <xc.h>
#include <stdlib.h>
#include <stdint.h>

/*void receive_can(void);*/
void send_status(void);
/*void initParams(void);*/
void getParams(uint16_t data);
void setParams(uint16_t id, uint16_t data);
void initMessage(void);
void sendSelectors(uint8_t selector, uint8_t position);
void sendButton(uint8_t button);
void sendError(void);

#endif
