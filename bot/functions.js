const fs = require('fs');
var exec = require('child_process').exec;
var localip = require('local-ip');
var download = require('./download.js')

//Execute Child Process
function execute(command, callback){
    exec(command, function(error, stdout, stderr){ callback(stdout); });
};

 
//Our amazing functions


var ifconfig = function(discord, message) {
    execute("hostname -i", function(content){
        message.reply(content);
    });
}


var uptime = function(discord, message) {
    execute("uptime", function(content){
        message.reply(content);
    });
}

var who = function(discord, message) {
    execute("who", function(content){
        message.reply(content);
    });
}

var version = function(discord, message) {
    console.log("Checking version...")
    try {
    	let config = JSON.parse(fs.readFileSync('/home/lv_room/bot/package.json'))
    	let version = config["version"]
    	let author = config["author"]
    	message.channel.send("\n\n---------\nFST Bot \nVersion: " + version + "\nMade By: "+author+"🍫🍫\n>hielp for help\n---------")
    	.then((message) => {
        	message.react("🍫")
    	})
    } catch(e) {
    	message.channel.send("Error: " + e)
    }
}

var ssh = function(discord, message) {
    var iface = 'enp2s0';
    localip(iface, function(err, res) {
    if (err) {
        message.channel.send("I have no idea what my local ip is").then((message) => {
            message.react("🇫")
        })
    }
        message.reply("ssh lv_room@"+res)
    });
}


var hackerman = function(discord, message) {
    message.reply("https://tenor.com/view/hackerman-mr-gif-12747764")
}


function pray(message, i) {
    if (i > 0) {
        i--;
        message.channel.send("🇫")
        setTimeout(function() {
            pray(message, i)
        }, 1000)
    } 
}

var andrecorreia = function(discord, message) { 
    message.channel.send("https://imgur.com/3O5sEsW").then((message) => {
            message.react("🇫")
    })
    message.channel.send("Let's pray")
    setTimeout(function() {
            pray(message, 5)
        }, 1000)
}

var cmdHelp = {
	"ip": "Runs Ifconfig",
	"version": "Do I have to explain this?",
	"hackerman": "Mr. Robot GIF",
	"ssh": "Gives you the command for SSH. So you can enter inside me ohhh",
	"andrecorreia": "In honor to our Team Leader",
	"goncalo": "In honor to our software guy",
	"freitas": "In honor to our alumni guy",
	"uptime": "Stats about my uptime",
	"who": "Who's in the house?? Software is in the house! (It shows you the users that are logged in)",
	"upload": "Give me your files 😈"
} 

var hielp = function(discord, message) {
	var helpMsg = "\n It looks like you're lost... \n Let me help you \n"
	Object.keys(cmdHelp).forEach(function(key) {
   		 helpMsg += ">"+key+" - "+cmdHelp[key]+"\n"
	});
	message.reply(helpMsg)
}

var upload = function(discord, message) {
	var attach = (message.attachments)
  	if (attach && attach.array()[0]) {
		message.channel.send("Thanks for the file! I'm receiving it now... ")
    		download(message, attach.array()[0].url)
  	} else {
		message.channel.send("You didn't send any attachment (or maybe there was an error while trying to read it) AND NOW I'M SAD :(").then((message) => {
            		message.react("🇫")
        	})
	}
}


var get_artist = function() {
    var artist = "";
    execute("playerctl metadata artist", function(content){artist = content});
    return artist;
}

var get_title = function() {
    var title = "";
    execute("playerctl metadata title", function(content){title = content});
    return title;
}

var next = function(discord, message) {
    execute("playerctl next", function(content){});
    artist = get_artist();
    title = get_title();
    message.reply("next: " + artist.substring(0,artist.length-1) + " - " + title);
}

var previous = function(discord, message) {
    execute("playerctl previous", function(content){});
    artist = "";
    execute("playerctl metadata artist", function(content){artist = content});
    execute("playerctl metadata title", function(content){message.reply("next: " + artist.substring(0,artist.length-1) + " - " + content)});
}

var title = function(discord, message) {
    execute("playerctl metadata title", function(content){
        message.reply(content);
    });
}

var pause = function(discord, message) {
    execute("playerctl pause", function(content){
        message.reply("paused");
    });
}

var play = function(discord, message) {
    execute("playerctl play", function(content){});
    execute("playerctl metadata title", function(content){message.reply("playing: " + content)});
}

var goncalo = function(discord, message) {
    message.reply("https://imgur.com/gallery/XRrCB").then((msg) => {msg.react("🍫")});
}

var freitas = function(discord, message) {
    message.reply("https://cdn.discordapp.com/attachments/739889889587495023/757342899926204476/freitas_kill_me.png")
}

// Export Functions as commands
// name : function

module.exports = {
    ip : ifconfig,
    version : version,
    hackerman : hackerman,
    ssh : ssh,
    andrecorreia : andrecorreia,
    uptime : uptime,
    who : who,
    hielp : hielp,
    upload : upload,
    next : next,
    title : title,
    pause : pause,
    play : play,
    previous : previous,
    goncalo : goncalo,
    freitas : freitas
}
