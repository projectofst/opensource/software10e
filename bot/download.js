var fs = require('fs');
var https = require('https');


var download = function(message, link) {
	linkArray = link.split("/")
	console.log("/home/lv_room/bin/"+linkArray[linkArray.length - 1])
	var file = fs.createWriteStream("/home/lv_room/bin/"+linkArray[linkArray.length - 1]);
	var contentLength;
	var length;
	var responseData;
	var steps = [0, 25, 50, 75, 100]
	var currentStep = 0
	var request = https.get(link, 
	function (response) {
    		contentLength = parseInt(response.headers['content-length']); // in bytes
    		length = [];
    		var pb = "[";
    		// Grab the data buffer of the request
		response.on('data', (d) => {
			file.write(d);
        		responseData += d;
        		length.push(d.length);
        		let sum = length.reduce((a, b) => a + b, 0);
        		let completedParcentage = (sum / contentLength) * 100;
			if(steps[currentStep] < completedParcentage) {
				message.channel.send(`Uploading ${steps[currentStep]}% complete \n ${pb}> \n`);	
				pb += "========"
				currentStep++
			}
        		// Modify this to alter the width of progress-bar using percentageCompleted
		});

    		response.on('end', () => {
			message.channel.send("Done \n" + pb + "] \n")
			message.channel.send("Writing data... \n")
       			message.channel.send("Done \n").then((msg) => {msg.react("🍫")})
    		})

	});
}

module.exports = download
