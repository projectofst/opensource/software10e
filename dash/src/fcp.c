#include <stdint.h>

#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"

#include "lib_pic33e/flash.h"

#include "shared/fifo/fifo.h"

#include "signals.h"

extern Fifo* can_fifo;

extern uint16_t flash[CFG_DASH_SIZE];

extern uint32_t parameters[CFG_DASH_SIZE];

uint16_t dev_get_id()
{
    return DEVICE_ID_DASH;
}

void dev_send_msg(CANdata msg)
{
    // append_fifo(can_fifo, msg);
    send_can2(msg);
    send_can1(msg);

    return;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{
    multiple_return_t arg = { 0 };
    if (id == CMD_DASH_SE_CALIBRATE) {
        parameters[CFG_DASH_SE_OFFSET] = getADCAverage(16);
    }

    if (id == CMD_DASH_WRITE_FLASH) {
        write_flash((uint16_t*)parameters, 0, 2 * CFG_DASH_SIZE);
    }

    return arg;
}
/*
void setParams(uint16_t id, uint16_t data)
{

    disable_timer1();

    switch (id) {
    case 4:
        parameters[id] = getADCAverage(16);
        write_flash(params + id, 0, 1);
        break;
    case 5:
        write_flash(&data, 1, 1);
        parameters[id] = data;
        break;
    case 6:
        write_flash(&data, 2, 1);
        parameters[id] = data;
        break;
    case 7:
        write_flash(&data, 3, 1);
        parameters[id] = data;
        break;
    }

    enable_timer1();

    return;
}
*/

void cfg_set_callback(uint32_t* table)
{
    uint16_t i;

    for (i = 0; i < CFG_DASH_SIZE; i++)
        // flash[i] = table[i];

        return;
}
