#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include <stdint.h>
#include <stdlib.h>
#include <xc.h>

#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"
#include "shared/fifo/fifo.h"

#include "data.h"

extern int toprint;

can_t* get_can();
void receiveSWCAN(void);
void receiveEssentialCAN(void);
void send_TS_command(void);
void updateMasterLEDs(dev_master_t master);
void updateTSALIIB(unsigned TSALSignal);
void send_RTD_ON_message(void);
void send_RTD_OFF_message(void);
void setRTDMode(int);
void initMessage(void);
void initParams(void);
void getParams(uint16_t data);
void setParams(uint16_t id, uint16_t data);
void sendError(void);
void update_buttons(statusCar* car);
void update_can(statusCar* car);
void receive_can_handler(void);
void can_dispatch(Fifo* fifo1);

#endif
