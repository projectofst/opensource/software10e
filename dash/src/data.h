#ifndef __DATA_H__
#define __DATA_H__

#include <stdlib.h>

#include "button.h"
#include "led.h"

enum State {
    OFF,
    ON
}; //OFF - 0, ON - 1

typedef struct _watchdog {
    uint8_t received : 1;
    uint8_t TSAL : 1;
} WATCHDOG;

typedef struct _data {
    WATCHDOG masterWatchdog;
    WATCHDOG IIBWatchdog;
} DATA;

typedef struct _statusCar {
    enum State RTDState;
    //enum State TSState;
    Button RTD;
    Button TS;
    Led leds[5]; // Backlight, IMD - SC1, AMS - SC2, TSOFF - SC3, RND - SC4
} statusCar;

extern uint16_t params[8];
extern DATA dash;

// for scheduler

#define N_TASKS 6

#define totalLEDS 5

extern statusCar car;

#endif