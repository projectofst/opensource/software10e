#ifndef _LED_H_
#define _LED_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <xc.h>

typedef struct _led {
    bool on : 1;
    //int8_t level;
} Led;

void VerifyWatchdogs(void);
void TSLEDon(void);
void setLEDOutput(void);
void turnAllON(void);
void turnAllOFF(void);
void turnLEDOn(int n);
void turnLEDOff(int n);
void initializeLEDS(void);

#endif
