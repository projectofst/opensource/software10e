#ifndef NOSE

#include "SE.h"
#include "can-ids-v2/can_ids.h"
#include "communication.h"
#include "data.h"
#include "lib_pic33e/adc.h"
#include "signals.h"

extern uint32_t parameters[CFG_DASH_SIZE];

uint16_t getADCAverage(int N)
{
    int i;
    uint16_t SE = 0;

    for (i = 0; i < N; i++) {
        SE += get_adc_value(i);
    }
    SE /= N;

    return SE;
}

void getSE(void)
{
    uint16_t SE = 0;
    int16_t intermediate;

    can_t* can = get_can();

    // Do the average of 16 measures of the SE value
    SE = getADCAverage(16);

    // SE only has values between 8.5% and 91.5% of the reading of the ADC. Garantee
    // that the average is between those values. If it is outside, send a message error
    // With tests, was tested that sensor outputted ADC values up to 3727. This is
    // due to the voltage divider - which is not perfect
    if (SE < 349 || SE > 3747) {
        can->dash.dash_se.dash_se = 3600;
        return;
    }

    // convert the SE value into a angle between -180º and 180º
    intermediate = (int16_t)((int32_t)(SE - parameters[CFG_DASH_SE_OFFSET]) * (int32_t)(10594 / 10000));
    if (SE < parameters[CFG_DASH_SE_OFFSET] && (parameters[CFG_DASH_SE_OFFSET] - SE > 1699)) {
        // 3277 makes range from -1799 to 1799
        // 3278 makes range from -1800 to 1799
        // intermediate += 3277*10986/10000;
        // intermediate += 36001122/10000;
        intermediate += 3600;
    } else if (SE > parameters[CFG_DASH_SE_OFFSET] && (SE - parameters[CFG_DASH_SE_OFFSET] > 1699)) {
        // 3277 makes range from -1799 to 1799
        // 3278 makes range from -1800 to 1799
        // intermediate -= 3277*10986/10000;
        // intermediate -= 36001122/10000;
        intermediate -= 3600;
    }

    can->dash.dash_se.dash_se = intermediate;
}

void configADCParameters(ADC_parameters* parameters)
{

    parameters->idle = 1; /*works in idle mode                       */
    parameters->form = 0; /*form set to unsigned integer             */
    parameters->conversion_trigger = AUTO_CONV; /*auto conversion                          */
    parameters->sampling_type = AUTO; /*manual sampling                          */
    parameters->voltage_ref = INTERNAL; /*use internal voltage reference           */
    parameters->sample_pin_select_type = MANUAL; /*manualy change between differrent inputs */
    parameters->smpi = 15; /*Do 16 samples                            */
    // To use S1 ADC library, specifically config_adc1 needs to be changed because
    // it only uses analogic ports from B (S1 is E analogical port)
    parameters->pin_select = S2; /*scan pin AN0 [SE] and AN2 [board_temp]   */
    parameters->interrupt_priority = 0; /*Interrupt priority 5                     */
    parameters->turn_on = 1; /*turn the module on                       */

    return;
}

void configADC(void)
{
    ADC_parameters adcConfig;
    // Generate configuration:
    configADCParameters(&adcConfig);
    config_adc1(adcConfig);
    select_input_pin(5);

    return;
}

#endif
