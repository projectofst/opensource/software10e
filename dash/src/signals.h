#ifndef __SIGNALS_H__
#define __SIGNALS_H__

#include <stdlib.h>
#include <xc.h>

//IMD - SC1, AMS - SC2, TSOFF - SC3, RND - SC4

#define LAT_IMD LATBbits.LATB8
#define LAT_AMS LATBbits.LATB9
#define LAT_TSOFF LATBbits.LATB10
#define LAT_RND LATBbits.LATB11
#define LAT_BACKLIGHT LATEbits.LATE7

#define TRIS_IMD TRISBbits.TRISB8
#define TRIS_AMS TRISBbits.TRISB9
#define TRIS_TSOFF TRISBbits.TRISB10
#define TRIS_RND TRISBbits.TRISB11
#define TRIS_BACKLIGHT TRISEbits.TRISE7

#define LAT_RTD LATBbits.LATB4
#define LAT_TS LATBbits.LATB6

#define TRIS_RTD TRISBbits.TRISB4
#define TRIS_TS TRISBbits.TRISB6

#define PORT_RTD PORTBbits.RB4
#define PORT_TS PORTBbits.RB6

#define TRIS_SC TRISFbits.TRISF3

#define PORT_SC PORTFbits.RF3

//For steering encoder
#define S1 AN29
#define LAT_S1 LATEbits.LATE5
#define TRIS_S1 TRISEbits.TRISE5

#define S2 AN5
#define LAT_S2 LATBbits.LATB5
#define TRIS_S2 TRISBbits.TRISB5

#endif
