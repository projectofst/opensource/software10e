// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128 // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4 // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xc.h>

// NOTE: Always include timing.h
#include <lib_pic33e/timing.h>

// Comment this line if USB is not needed
//#include <lib_pic33e/usb_lib.h>
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"
#include "can-ids-v2/dash_can.h"
#include "shared/fifo/fifo.h"
#include <lib_pic33e/can.h>
#include <lib_pic33e/pps.h>
#include <lib_pic33e/timer.h>
#include <lib_pic33e/trap.h>
#include <shared/scheduler/scheduler.h>

#include "SE.h"
#include "button.h"
#include "communication.h"
#include "data.h"
#include "led.h"
#include "signals.h"

#define FIFO_SIZE 64

statusCar car;
DATA dash;

Fifo _can_fifo;
Fifo* can_fifo = &_can_fifo;

uint32_t parameters[CFG_DASH_SIZE] = { 0 };

void trap_handler(TrapType type)
{
    send_can2(logE(LOG_TRAP_HANDLER_TYPE, type, 0, 0));
    __delay_ms(7000);
    return;
}

volatile uint32_t timer1_flag = 0;

void timer1_callback(void)
{
    timer1_flag++;
    return;
}

int main()
{

    parameters[CFG_DASH_SE_OFFSET] = 1699;
    timer1_flag = 0;

    tasks_t dash_tasks[] = {
        //{.period=1000,.func=send_status},
        //{.period=40,.func=sendSE},
        { .period = 400, .func = verifyButtons },
        { .period = 400, .func = TSLEDon },
        { .period = 2000, .func = VerifyWatchdogs },
        { .period = 0, .func = receive_can_handler },
        { .period = 20, .func = getSE },
        //{ .name = "can_dispatch", .period = 1, .func = can_dispatch, .args_on = 1, .args = (void*)can_fifo },
        // .period = 0, .func = receiveSWCAN }
    };

    // Configure the PPS for the CAN module.
    PPSUnLock;
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP120); // CAN1_TX 
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI121); // CAN1_RX 
    PPSOutput(OUT_FN_PPS_C2TX, OUT_PIN_PPS_RP64); // CAN2_TX at spin 46
    PPSInput(IN_FN_PPS_C2RX, IN_PIN_PPS_RPI61); // CAN2_RX at pin 47
    PPSLock;

    TRIS_SC = 1; // ShutDown Circuit AIR detection as input
    TRIS_S1 = 1; // Steering Encoder as input

    // set Watchdogs as not received messages
    dash.masterWatchdog.received = 0;
    dash.IIBWatchdog.received = 0;

    // set TSALs signals has to turn off TSAL off
    dash.masterWatchdog.TSAL = 1;
    dash.IIBWatchdog.TSAL = 1;

    can_t* can = get_can();

    CANdata msg[FIFO_SIZE];

    fifo_init(can_fifo, msg, FIFO_SIZE);

    scheduler_init_args(dash_tasks, sizeof(dash_tasks) / sizeof(tasks_t));

    initParams();

    setButtonInput();

    initializeLEDS();

    configADC();

    /* Configure and start the CAN modules with the default config.
     CAN2 corresponds to essential line CAN. CAN1 corresponds to
     Steering Wheel CAN line. */
    config_can1(NULL);
    config_can2(NULL);

    /* Timer to count mili seconds and send messages */
    config_timer1(1, 4);

    initMessage();

    cfg_config(parameters, CFG_DASH_SIZE);

    while (1) {
        scheduler(timer1_flag);
        dash_send_msgs(can->dash, timer1_flag);
        ClrWdt();
        //__delay_ms(2);
    }
    return 0;
}
