#ifndef _SE_H_
#define _SE_H_

#include <lib_pic33e/adc.h>

uint16_t getADCAverage(int N);
void getSE(void);
void configADCParameters(ADC_parameters* parameters);
void configADC(void);

#endif
