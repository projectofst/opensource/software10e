#ifndef NOBUTTON

#include <button.h>
#include <communication.h>
#include <lib_pic33e/timing.h>

#include "data.h"
#include "signals.h"

void verifyButtons(void)
{
    /*If RTD Button was pressed and released
    start the RTD sequence*/
    if (car.RTD.changes >= 2) {
        car.RTD.changes = 0;
        if (car.RTDState == OFF) {
            send_RTD_ON_message();
        }
        if (car.RTDState == ON) {
            send_RTD_OFF_message();
        }
    }
    /*If TS Button was pressed and released
    send TS ON command*/
    if (car.TS.changes >= 2) {
        car.TS.changes = 0;
        send_TS_command();
    }
}

/* Interrupt service routine for the CNI interrupt. */
#ifndef LINUX

void __attribute__((interrupt, no_auto_psv)) _CNInterrupt(void)
{
    // Check if was RTD Button whose value changed
    if (car.RTD.value != PORT_RTD) {
        car.RTD.changes++;
        car.RTD.value = PORT_RTD;
        //__delay_ms(50);
    }
    // Check if was TS Button whose value changed
    if (car.TS.value != PORT_TS) {
        car.TS.changes++;
        car.TS.value = PORT_TS;
        //__delay_ms(50);
    }
    // Clear the flag
    IFS1bits.CNIF = 0;
}

#endif

#ifndef LINUX

void buttonsInterrupt(void)
{
    //  CNI: Change Notification Interrupt
    //  Priority: 5
    IPC4bits.CNIP = 5;
    //  Interrupt on change : any
    CNENBbits.CNIEB4 = 1; // Pin : RB4
    CNENBbits.CNIEB6 = 1; // Pin : RB6

    IFS1bits.CNIF = 0; // Clear CNI interrupt flag
    IEC1bits.CNIE = 1; // Enable CNI interrupt
}
#endif

#ifdef LINUX

void buttonsInterrupt(void)
{
    return;
}

#endif

#ifndef LINUX
void setButtonInput(void)
{
    // Set as input - RTD and TS button
    TRIS_RTD = 1;
    TRIS_TS = 1;
    /* Set current value of pin
     logic level at input */
    car.RTD.value = PORT_RTD;
    car.RTD.changes = 0;
    car.RTDState = OFF;
    car.TS.value = PORT_TS;
    car.TS.changes = 0;
    // car.TSState = ON;

    // Set interrupt for pin change notification
    buttonsInterrupt();
}
#endif

#ifdef LINUX
void setButtonInput(void)
{
    return;
}
#endif
#endif
