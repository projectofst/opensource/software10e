#ifndef _BUTTON_H_
#define _BUTTON_H_

#include <stdint.h>
#include <stdlib.h>
#include <xc.h>

typedef struct _button {
    uint8_t changes : 7;
    uint8_t value : 1;
} Button;

void verifyButtons(void);
void buttonsInterrupt(void);
void setButtonInput(void);

#endif
