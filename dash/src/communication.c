#ifndef NOCOMMUNICATION

/*#include <stdlib.h>
#include <stdint.h>*/

#include "SE.h"
#include "communication.h"
#include "data.h"
#include "led.h"
#include "signals.h"

#include "lib_pic33e/can.h"
#include "lib_pic33e/flash.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/version.h"

#include "can-ids-v2/can_cfg.h"
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"
#include "can-ids-v2/candata.h"

extern uint32_t parameters[CFG_DASH_SIZE];

can_t global_can;

can_t* get_can()
{
    return &global_can;
}

void receiveSWCAN(void)
{
    CANdata message;

    if (!can1_rx_empty()) { // Receive Steering Wheel CAN and send it to the Car
        message = pop_can1();
        send_can2(message);
    }

    return;
}

// Sending messeges: update can struct -> encode with fcp (dash_send_msg) -> send to fifo (dev_send_msg) -> can_dispatch

void receive_can_handler(void)
{
    CANdata msg;
    can_t* can = get_can();

    if (can2_rx_empty()) {
        return;
    }

    msg = pop_can2();

    decode_can(msg, can);

    send_can1(msg); // Send Car CAN to Steering wheel

    switch (msg.dev_id) {
    case DEVICE_ID_IIB: // Receive IBB info for TS LED
        dash.IIBWatchdog.received = 1;
        switch (msg.msg_id) {
        case MSG_ID_IIB_IIB_INFO:
            setRTDMode(can->iib.iib_info.inv_on);
            break;
        case MSG_ID_IIB_IIB_CAR:
            updateTSALIIB(can->iib.iib_car.car_tsal);
            break;
        }
        break;
    case DEVICE_ID_MASTER: // Receive BMS info for IMD, AMS & TS LEDs
        dash.masterWatchdog.received = 1;
        if (msg.msg_id == MSG_ID_MASTER_MASTER_STATUS)
            updateMasterLEDs(can->master);
        break;
    }

    cfg_dispatch(msg);
    cmd_dispatch(msg);

    return;
}

/* Handles two fifos. The verbose one and the normal communication channel
 * Should not write to the CAN TX buffer more than it can handle
 */
void can_dispatch(Fifo* fifo1)
{
    CANdata msg;

    // uint16_t available = can1_tx_available();
    /*  for (; available > 0 && !fifo_empty(fifo1); available--) {
        //LED5 ^= 1;
        msg = fifo_pop(fifo1);
        if (msg == NULL) {
          //should never happen
            break;

        }
        send_can1(*msg);
    }
 */
    msg.dev_id = 10;
    msg.msg_id = 30;
    msg.dlc = 8;
    msg.data[0] = 69;
    msg.data[1] = 420;
    msg.data[2] = 666;
    msg.data[3] = 123;
    send_can1(msg);

    return;
}

void updateMasterLEDs(dev_master_t master)
{

    if (!master.master_status.master_status_imd_latch && car.leds[1].on == false) {
        turnLEDOn(1);
    } else if (master.master_status.master_status_imd_latch && car.leds[1].on == true) {
        turnLEDOff(1);
    }

    if (!master.master_status.master_status_ams_latch && car.leds[2].on == false) {
        turnLEDOn(2);
    } else if (master.master_status.master_status_ams_latch && car.leds[2].on == true)
        turnLEDOff(2);

    if (master.master_status.master_status_ts_on)
        dash.masterWatchdog.TSAL = 1;
    else
        dash.masterWatchdog.TSAL = 0;

    return;
}

void updateTSALIIB(unsigned TSALSignal)
{
    if (TSALSignal)
        dash.IIBWatchdog.TSAL = 1;
    else
        dash.IIBWatchdog.TSAL = 0;

    return;
}

void send_RTD_ON_message(void)
{
    // Send message to essential CAN line
    send_can2(cmd_send(CMD_IIB_RTD_ON, DEVICE_ID_IIB, 0, 0, 0));

    return;
}

void send_RTD_OFF_message(void)
{
    // send_can2(cmd_send(CMD_IIB_RTD_OFF, DEVICE_ID_IIB, 0, 0, 0));

    return;
}

void setRTDMode(int bit)
{

    if (bit == 1) {
        car.RTDState = ON;
        turnLEDOn(4);
    } else {
        car.RTDState = OFF;
        turnLEDOff(4);
    }
}

void send_TS_command(void)
{
    // Send message to essential CAN line
    send_can2(cmd_send(CMD_MASTER_COMMON_TS, DEVICE_ID_MASTER, 0, 0, 0));

    return;
}

void initMessage(void)
{
    CANdata message;

    send_can2(logI(LOG_RESET_MESSAGE, RCON, 0, 0));
    send_can1(logI(LOG_RESET_MESSAGE, RCON, 0, 0));

    return;
}

void initParams(void)
{

    read_flash((uint16_t*)parameters, 0, 2 * CFG_DASH_SIZE);
    parameters[0] = VERSION;

    return;
}

/*
void setParams(uint16_t id, uint16_t data)
{

    disable_timer1();

    switch (id) {
    case 4:
        parameters[id] = getADCAverage(16);
        write_flash(params + id, 0, 1);
        break;
    case 5:
        write_flash(&data, 1, 1);
        parameters[id] = data;
        break;
    case 6:
        write_flash(&data, 2, 1);
        parameters[id] = data;
        break;
    case 7:
        write_flash(&data, 3, 1);
        parameters[id] = data;
        break;
    }

    enable_timer1();

    return;
}
*/

void update_buttons(statusCar* car)
{
    can_t* can = get_can();

    /*If RTD Button was pressed and released
      start the RTD sequence*/
    if (car->RTD.changes >= 2) {
        car->RTD.changes = 0;
        if (car->RTDState == OFF) {
            // send_RTD_ON_message(RTD_STEP_DASH_BUTTON);
            can->dash.dash_status.dash_rtd = ON;
            if (car->RTDState == ON) {
                // send_RTD_OFF_message();
                can->dash.dash_status.dash_rtd = OFF;
            }
            /*If TS Button was pressed and released
        send TS ON command*/
            if (car->TS.changes >= 2) {
                car->TS.changes = 0;
                // send_TS_command();
                can->dash.dash_status.dash_ts = ON; // master.master_em.master_em_voltage = battery->energy_meter.battery_voltage;
            }
        }
    }
    return;
}
void update_can(statusCar* car)
{

    update_buttons(car);

    return;
}

#endif
