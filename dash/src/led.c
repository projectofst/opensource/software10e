#ifndef NOLED

#include "data.h"
#include "signals.h"
#include <led.h>
#include <lib_pic33e/timing.h>

void VerifyWatchdogs(void)
{

    if (dash.masterWatchdog.received == 0) {
        // Turn on IMD LED
        turnLEDOn(1);
        // Turn on AMS LED
        turnLEDOn(2);
        // Turn off TS OFF
        turnLEDOff(3);
        dash.masterWatchdog.TSAL = 1;
    }
    // If a message wasn't received from IIB in 3 sec, there is a problem
    // This acts as a watchdog
    if (dash.IIBWatchdog.received == 0) {
        // Turn off TS OFF
        turnLEDOff(3);
        dash.IIBWatchdog.TSAL = 1;
    }

    // Reset Watchdog
    dash.masterWatchdog.received = 0;
    dash.IIBWatchdog.received = 0;

    return;
}

void TSLEDon(void)
{

    if (dash.IIBWatchdog.TSAL || dash.masterWatchdog.TSAL) {
        if (car.leds[3].on == true)
            turnLEDOff(3);
    } else {
        if (car.leds[3].on == false)
            turnLEDOn(3);
    }

    return;
}

void setLEDOutput(void)
{
    // set pins connected to LEDs as output pins
    TRIS_BACKLIGHT = 0;
    TRIS_IMD = 0;
    TRIS_AMS = 0;
    TRIS_TSOFF = 0;
    TRIS_RND = 0;
    return;
}

void turnAllON(void)
{
    int i;

    for (i = 0; i < totalLEDS; i++) {
        car.leds[i].on = true;
    }
    LAT_BACKLIGHT = 1;
    LAT_IMD = 1;
    LAT_AMS = 1;
    LAT_TSOFF = 1;
    LAT_RND = 1;

    __delay_us(33);

    return;
}

void turnAllOFF(void)
{
    int i;

    for (i = 0; i < totalLEDS; i++) {
        car.leds[i].on = false;
    }
    LAT_BACKLIGHT = 0;
    LAT_IMD = 0;
    LAT_AMS = 0;
    LAT_TSOFF = 0;
    LAT_RND = 0;

    __delay_us(500);

    return;
}

void turnLEDOn(int n)
{
    switch (n) {
    case 0:
        LAT_BACKLIGHT = 1;
        break;
    case 1:
        LAT_IMD = 1;
        break;
    case 2:
        LAT_AMS = 1;
        break;
    case 3:
        LAT_TSOFF = 1;
        break;
    case 4:
        LAT_RND = 1;
        break;
    }
    __delay_us(33);
    car.leds[n].on = true;
    return;
}

void turnLEDOff(int n)
{
    switch (n) {
    case 0:
        LAT_BACKLIGHT = 0;
        break;
    case 1:
        LAT_IMD = 0;
        break;
    case 2:
        LAT_AMS = 0;
        break;
    case 3:
        LAT_TSOFF = 0;
        break;
    case 4:
        LAT_RND = 0;
        break;
    }
    __delay_us(500);
    car.leds[n].on = false;
    return;
}

void initializeLEDS(void)
{
    int i;

    setLEDOutput();
    // turn all LEDs on during 1s to 3s
    turnAllON();

    __delay_ms(1500);

    for (i = 3; i < totalLEDS; i++) {
        // car.leds[i].level = 31;
        turnLEDOff(i);
    }

    return;
}

#endif
