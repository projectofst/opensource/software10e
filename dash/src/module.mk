MODULE_C_SOURCES:=SE.c
MODULE_C_SOURCES+=button.c
MODULE_C_SOURCES+=communication.c
MODULE_C_SOURCES+=fcp.c
MODULE_C_SOURCES+=led.c
MODULE_C_SOURCES+=main.c

PWD:=src/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
