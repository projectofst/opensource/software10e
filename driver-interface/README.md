# FST DI - Version 2.0.2

## Description
The FST Driver Interface *(FST DI)* is an operative system with an interface capable of delivering real-time info to the driver while allowing to change some driving parameters straight from the cockpit.
It features a simple, fast and clear GUI which can be controlled directly from the buttons and knobs located at the steering wheel.
This operative system also feats the telemetry server (wich can be found here) that will allow the rest of the team to see the state of every system inside the car.

## How to setup FST DI development environment

 1. Setup the rep
 
    `git clone https://gitlab.com/projectofst/software10e.git`<br>
    `git checkout feat/dash/new_driver_interface`

 2. Install the required packages

    `pip3 install fcp`<br>
    `pip3 install PyQt5`<br>
    `sudo pacman -S qt5`

 3. Setup vCAN
    
    `sudo modprobe vcan`<br>
    `sudo ip link add dev vcan0 type vcan`<br>
    `sudo ip link set up vcan0`

    Finally just run `python3 dash.py <config>` and replace _config_ with the desired configuration file
