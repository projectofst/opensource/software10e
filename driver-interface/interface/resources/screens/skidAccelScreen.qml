import QtQuick 2.12
//import QtQuick.Controls 2.0
import QtQuick.Window 2.12
//import QtQuick.Layouts 1.0
//import QtQuick.Shapes 1.0
//import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4


Rectangle{
    id: wind
    color: "#fdfdfd"
    width: 800
    height: 480
    visible: true


    //NameTextWidget{}
    /*
    CircularGauge {
        id:circgage

        value: 0
        maximumValue: 140
        anchors {
            margins: wind.height * 0.2
        }

        Component.onCompleted: forceActiveFocus()

        Behavior on value { NumberAnimation { duration: 200 }}
    }

    StatusIndicator {
        id: statusIndicator
        x: -161
        y: -17
        active: true
    }*/



    Rectangle {
        id: rectangle
        color: "#121313"
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 600
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        border.color: batteryTempWidget.fontColor
        border.width: 6

        NameValueWidget {
            id: nameValueWidget2
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 320
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
            name: "Battery Temp"
            value:batteryTempWidget.value
            backgroundColor: batteryTempWidget.backgroundColor
            fontColor:batteryTempWidget.fontColor
        }

        NameValueWidget {
            id: nameValueWidget3
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 160
            anchors.top: parent.top
            anchors.topMargin: 160
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            name: "Lap"
            value:"0"//secondWidget.value
        }

        NameValueWidget {
            id: nameValueWidget4
            anchors.top: parent.top
            anchors.topMargin: 320
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            value:"0"//secondWidget.value
        }
    }

    Rectangle {
        id: rectangle2
        color: "#121313"
        anchors.left: rectangle.right
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 200
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        border.width: 6

        NameValueWidget {
            id: nameValueWidget1
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 161
            anchors.top: parent.top
            anchors.topMargin: 0
            scale: 1
            clip: false
            z: 1
            name: "speed"
            y: 2
            value: velocityWidget.value
            backgroundColor: socWidget.backgroundColor
            fontColor:socWidget.fontColor
            fontSize: 120
        }


        NameValueWidget {
            id: nameValueWidget10
            name:"G Force"
            value:  "10"
            backgroundColor: "#1b1c1c"
            fontColor: lapTimeWidget.fontColor
            x: 0
            y: 319
            width: 400
            height: 161
        }

    }



    Rectangle {
        id: rectangle3
        color: torqueWidget.fontColor
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 600
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        NameValueWidget {
            id: nameValueWidget5
            value:torqueWidget.motor1
            backgroundColor: torqueWidget.backgroundColor
            fontColor:torqueWidget.fontColor
            name:'Front torque'
            width: 200
            height: 120
        }

        NameValueWidget {
            id: nameValueWidget8
            name: "Rear torque"
            x: 0
            y: 120
            width: 200
            height: 120
            backgroundColor: torqueWidget.backgroundColor
            value: torqueWidget.motor3
            fontColor: torqueWidget.fontColor
            border.color: torqueWidget.fontColor
        }

        NameValueWidget {
            id: nameValueWidget6
            name:'dif'
            y: 240
            width: 200
            value: diferWidget.value
            backgroundColor: diferWidget.backgroundColor
            fontColor:diferWidget.fontColor
            height: 120
        }

        NameValueWidget {
            id: nameValueWidget7
            value: "0"//brakeBiasWidget.value
            //backgroundColor: brakeBiasWidget.backgroundColor
            //fontColor:brakeBiasWidget.fontColor
            name:'bias'
            x: 0
            y: 360
            width: 200
            height: 120
        }

        border.width: 6
    }



    /*
    Canvas {
            anchors.fill: parent
            onPaint: {
                var ctx = getContext("2d");
                ctx.reset();

                var centreX = width / 2;
                var centreY = height / 2;

                ctx.beginPath();
                ctx.fillStyle = "black";
                ctx.moveTo(centreX, centreY);
                ctx.arc(centreX, centreY, width / 4, 0, Math.PI * 0.5, false);
                ctx.lineTo(centreX, centreY);
                ctx.fill();

                ctx.beginPath();
                ctx.fillStyle = "red";
                ctx.moveTo(centreX, centreY);
                ctx.arc(centreX, centreY, width / 4, Math.PI * 0.5, Math.PI * 2, false);
                ctx.lineTo(centreX, centreY);
                ctx.fill();
            }
        }*/

}





/*##^##
Designer {
    D{i:11;anchors_height:120}D{i:12;anchors_height:120}
}
##^##*/
