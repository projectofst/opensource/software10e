import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.Extras 1.4
import "../subwidgets"

Rectangle {
    id: dialog_box
    width: 800
    height: 480
    visible: true
    color: "#ff0000"

        Text {
            id: main_text
            anchors.left: dialog_box.left
            anchors.leftMargin: 20
            color: "#ffffff"
            text: "INTERFACE CAN ERROR :("
            font.bold: true
            font.pixelSize: 40
        }

        Text {
            id: desc_text
            anchors.top: dialog_box.top
            anchors.topMargin: 50
            anchors.left: dialog_box.left
            anchors.leftMargin: 20
            color: "#ffffff"
            text: "There was an error while setting up vcan_sock \n"
            font.bold: false
            font.pixelSize: 25
        }

        Text {
            id: error_code
            anchors.top: dialog_box.top
            anchors.topMargin: 250
            anchors.left: dialog_box.left
            anchors.leftMargin: 20
            color: "#ffffff"
            text: "error code: oopsie_woopsie_can_dead ([Errno 19] No such device) \n"
            font.bold: true
            font.pixelSize: 18
        }

        Text {
            id: options_text
            anchors.top: dialog_box.top
            anchors.topMargin: 90
            anchors.left: dialog_box.left
            anchors.leftMargin: 20
            color: "#ffffff"
            text: "What you can try: \n> Check 'can_interface' parameter at dash config file (.json)\n> Check your CAN connection"
            font.bold: false
            font.pixelSize: 20
        }

}





/*##^##
Designer {
    D{i:12;anchors_height:120}D{i:13;anchors_height:120}
}
##^##*/
