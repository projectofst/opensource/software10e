import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.Extras 1.4
import "../subwidgets"

Rectangle {
    id: wind
    width: 800
    height: 480
    visible: true
    color: "#000000"

    Timer {
        interval: 200; running: true; repeat: true
        onTriggered: function() {
            wind.color = dash.getBackgroundColor()
        }
    }

    Rectangle {
        id: rectangle
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 600
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        border.width: 1
        color: "transparent"

        NameValueWidget {
            id: nameValueWidget2
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 320
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
            name: "Battery Temp"
            value: "0"
            backgroundColor: "transparent"

            Timer {
                interval: 500; running: true; repeat: true
                onTriggered: function() {
                    nameValueWidget2.value = dash.getBattery()
                    nameValueWidget2.valueColor = dash.getBatteryColor()
                }
            }
        }

        NameValueWidget {
            id: nameValueWidget3
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 160
            anchors.top: parent.top
            anchors.topMargin: 160
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            name: "Lap"
//            valueColor: fontColor
            backgroundColor: "transparent"
            value:"0"//secondWidget.value
        }

        NameValueWidget {
            id: nameValueWidget4
            anchors.top: parent.top
            anchors.topMargin: 320
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
//            valueColor: fontColor
            backgroundColor: "transparent"
            value:"F"//secondWidget.value
        }
    }

    Rectangle {
        id: rectangle2
        anchors.left: rectangle.right
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 200
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        border.width: 1
        color: "transparent"

        NameValueWidget {
            id: nameValueWidget1
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 280
            anchors.top: parent.top
            anchors.topMargin: 0
            scale: 1
            clip: false
            z: 1
            name: "SoC"
            y: 2
            value: "0"
            backgroundColor: "transparent"
//            valueColor: fontColor
            fontSize: 80

            Text {
                id: elementPercent
                x: 280
                y: 64
                width: 95
                height: 47
                color: "#ffffff"
                text: qsTr("%")
                font.family: "Tahoma"
                font.pixelSize: 51
            }
            
            Timer {
                interval: 500; running: true; repeat: true
                onTriggered: function() {
                    nameValueWidget1.value = dash.getSoC()
                    nameValueWidget1.valueColor = dash.getSoCColor()
                }
            }
        }

        NameValueWidget {
            id: nameValueWidget
            anchors.top: parent.top
            anchors.topMargin: 200
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 100
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            z: 1
            name: "speed"
            
            Timer {
                interval: 500; running: true; repeat: true
                onTriggered: function() {
                    nameValueWidget.value = dash.getSpeed()
                    nameValueWidget.valueColor = dash.getSpeedColor()
                }
            }
            backgroundColor: "transparent"
//            valueColor: velocityWidget.fontColor
        }


        LastTimeWidget {
            id: nameValueWidget10
            name: "last"
            lastTimevalue:  qsTr("00:00:00")
            timeDifference: qsTr("00:00:00")
            backgroundColor: "transparent"
            x: 0
            y: 380
            width: 200
            height: 100
        }


        NameValueWidget {
            id: nameValueWidget9
            name:"best"
            fontSize:35
            value: "0"
            x: 200
            y: 380
            width: 200
            height: 100
            backgroundColor: "transparent"
//            valueColor: velocityWidget.fontColor
        }
    }



    Rectangle {
        id: rectangle3
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 600
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        color: "transparent"

        NameValueWidget {
            id: nameValueWidget5
            value: "0"
            backgroundColor: "transparent"
            name:'Front torque'
            width: 200
            height: 120

            Timer {
                interval: 500; running: true; repeat: true
                onTriggered: function() {
                    nameValueWidget5.value = dash.getTorque0()
                    nameValueWidget5.valueColor = dash.getTorque0Color()
                }
            }
//            valueColor: velocityWidget.fontColor
        }

        NameValueWidget {
            id: nameValueWidget8
            name: "Rear torque"
            x: 0
            y: 120
            width: 200
            height: 120
            backgroundColor: "transparent"
            value: "0"

            Timer {
                interval: 500; running: true; repeat: true
                onTriggered: function() {
                    nameValueWidget8.value = dash.getTorque2()
                    nameValueWidget8.valueColor = dash.getTorque2Color()
                }
            }
//            valueColor: velocityWidget.fontColor
        }

        VariableValueWidget {
            id: nameValueWidget6
            name:'Diff'
            y: 240
            width: 200
            value: "0"
            backgroundColor: "#333333"
            height: 120
            border.color: "#6c6c6c"
            labelColor: "#4a9eff"
            valueColor: "#ffffff"

            Timer {
                interval: 500; running: true; repeat: true
                onTriggered: function() {
                    nameValueWidget6.value = dash.getDiff()
                    nameValueWidget6.valueColor = dash.getDiffColor()
                }
            }
        }

        NameValueWidget {
            id: nameValueWidget7
            value: "0"
            backgroundColor: "transparent"
            name:'Bias'
            x: 0
            y: 360
            width: 200
            height: 120
            Timer {
                interval: 500; running: true; repeat: true
                onTriggered: function() {
                    nameValueWidget7.value = dash.getBias()
                    nameValueWidget7.valueColor = dash.getBiasColor()
                }
            }
        }

    }

}






