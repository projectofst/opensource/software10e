import QtQuick 2.12
//import QtQuick.Controls 2.0
import QtQuick.Window 2.12
//import QtQuick.Layouts 1.0
//import QtQuick.Shapes 1.0
//import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4


Rectangle{
    id: wind
    color: "#fdfdfd"
    width: 800
    height: 480
    visible: true



    Rectangle {
        id: rectangle
        x: 0
        y: 0
        width: 800
        height: 480
        color: "#121313"
        border.width: 6

        DebugWidget {
            id: debugWidget
            x: 5
            y: 5
            name: "Steering"
            value:velocityWidget.value
            backgroundColor: velocityWidget.backgroundColor
            fontColor:velocityWidget.fontColor
        }

        DebugWidget {
            id: debugWidget1
            x: 5
            y: 80
            name: "Battery Temp"
            value:socWidget.value
            backgroundColor: socWidget.backgroundColor
            fontColor:socWidget.fontColor
        }

        DebugWidget {
            id: debugWidget4
            name: "master_sdc_1"
            x: 270
            y: 5
            backgroundColor: masterSdc1.backgroundColor
            value: masterSdc1.value
            fontColor: masterSdc1.fontColor

            StatusIndicator {
                id: statusIndicator5
                x: 220
                y: 22
                width: 41
                height: 48
                active: masterSdc1.value==="1"
            }
        }

        DebugWidget {
            id: debugWidget5
            name: "master_sdc_2"
            x: 270
            y: 80
            backgroundColor: masterSdc2.backgroundColor
            value: masterSdc2.value
            fontColor: masterSdc2.fontColor

            StatusIndicator {
                id: statusIndicator4
                x: 220
                y: 22
                width: 41
                height: 48
                active: masterSdc2.value==="1"
            }
        }

        DebugWidget {
            id: debugWidget6
            name: "master_sdc_3"
            x: 270
            y: 155
            backgroundColor: masterSdc3.backgroundColor
            value: masterSdc3.value
            fontColor: masterSdc3.fontColor

            StatusIndicator {
                id: statusIndicator3
                x: 220
                y: 22
                width: 41
                height: 48
                active: masterSdc3.value==="1"
            }
        }

        DebugWidget {
            id: debugWidget7
            name: "master sdc open"
            x: 270
            y: 230
            backgroundColor: sdcOpen.backgroundColor
            value: sdcOpen.value
            fontColor: sdcOpen.fontColor

            StatusIndicator {
                id: statusIndicator2
                x: 220
                y: 22
                width: 41
                height: 48
                active: sdcOpen.value==="1"
            }
        }

        DebugWidget {
            id: debugWidget9
            name: "dash sdc"
            x: 535
            y: 5
            backgroundColor: dashSdc.backgroundColor
            value: dashSdc.value
            fontColor: dashSdc.fontColor

            StatusIndicator {
                id: statusIndicator6
                x: 216
                y: 22
                width: 41
                height: 48
                active: dashSdc.value==="1"
            }
        }

        DebugWidget {
            id: debugWidget10
            name: "te sdc"
            x: 535
            y: 80
            backgroundColor: teSdc.backgroundColor
            value: teSdc.value
            fontColor: teSdc.fontColor

            StatusIndicator {
                id: statusIndicator7
                x: 219
                y: 22
                width: 41
                height: 48
                active: teSdc.value==="1"
            }
        }

        DebugWidget {
            id: debugWidget11
            name: "tsms relays"
            x: 270
            y: 305
            backgroundColor: masterTsms.backgroundColor
            value: masterTsms.value
            fontColor: masterTsms.fontColor

            StatusIndicator {
                id: statusIndicator
                x: 220
                y: 22
                width: 41
                height: 48
                active: masterTsms.value==="1";
            }
        }


    }
}

