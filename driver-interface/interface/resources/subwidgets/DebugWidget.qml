import QtQuick 2.0

Rectangle {
    id: rectangle
    width: 260
    height: 70
    property string backgroundColor: "#1b1c1c"
    color: backgroundColor
    property string name
    property string value
    property string fontColor:"#fdfdfd"
    property string fontColorText:"#4a9eff"
    property int fontSize:30

    Text {
        id: element
        x: 2
        y: 0
        width: 140
        height: 70
        color: fontColor
        text: name
        font.pixelSize: 22
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        id: element1
        x: 147
        y: 0
        width: 110
        height: 70
        color: fontColor
        text: value
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 40

    }

}
