import QtQuick 2.0
import QtQuick.Extras 1.4

Rectangle {
    id: rectangle
    x: 0
    y: 0
    width: 65
    height: 200
    color: "#2e3436"
    property string name
    property string value

    Gauge {
        id: gauge
        x: 1
        y: 50
        width: 69
        height: 147
        value: value
        transformOrigin: Item.Center
        scale: 1
        clip: false
        tickmarkStepSize: 25
    }

    Text {
        id: element
        x: 0
        y: 6
        width: 65
        height: 38
        color: "#fdfdfd"
        font.pointSize: 12
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 19
    }

}


/*##^##
Designer {
    D{i:1;anchors_height:200;anchors_width:59;anchors_x:16;anchors_y:42}
}
##^##*/
