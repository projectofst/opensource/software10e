import QtQuick 2.0

Rectangle {
    width: 200
    height: 100

    //Typical properties

    property string name
    property string lastTimevalue: "0"
    property string timeDifference: "0"
    property int fontSize: 30


    //Colors

    property string backgroundColor: "#000000"
    property string labelColor: "#4a9eff"
    property string valueColor: "#ffffff"
    property string timeDiffColor: valueColor

    //Link Properties

    color: backgroundColor
    border.color: "#555555"
    border.width: 1

    Text {
        id: nameElement
        x: 5
        y: 65
        width: 190
        height: 30
        text: name
        verticalAlignment: Text.AlignVCenter
        font.bold: true
        color: labelColor
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 20
    }

    Text {
        id: timeDiffElement
        x: 20
        y: 42
        width: 156
        height: 27
        color: valueColor
        text: timeDifference
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.bold: true
        font.pixelSize: 25
    }

    Text {
        id: timeElement
        x: 20
        y: 0
        width: 156
        height: 45
        color: valueColor
        text: lastTimevalue
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.bold: true
        font.pixelSize: 35
    }
}
