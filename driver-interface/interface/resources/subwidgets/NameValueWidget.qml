import QtQuick 2.0

Rectangle {

    //Base init

    id: rectangle
    x: 0
    y: 0
    width: 120
    height: 120

    //Typical properties

    property string name
    property string value
    property int fontSize: 70

    //Colors

    property string backgroundColor: "#000000"
    property string valueColor:"#ffffff"
    property string labelColor:"#4a9eff"

    //Link properties

    color: backgroundColor
    border.color: "#555555"
    border.width: 1

    Text {
        id: element
        color: labelColor
        text: name
        y: 110
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: element1.bottom
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 20
    }

    Text {
        id: element1
        text: value
        anchors.top: parent.top
        anchors.topMargin: 17
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 38
        anchors.right: parent.right
        anchors.rightMargin: 11
        anchors.left: parent.left
        anchors.leftMargin: 11
        verticalAlignment: Text.AlignVCenter
        font.family: "Ubun40"
        horizontalAlignment: Text.AlignHCenter
        font.bold: true
        color: valueColor
        font.pixelSize: fontSize
    }
}
