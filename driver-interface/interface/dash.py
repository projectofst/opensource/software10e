#!/usr/bin/env python3

"""
    FST Driver Interface - Version 2.0.1
    Made by:
        - Gonçalo Abreu Corrêa Brito da Silva 

    using PySide2 and FCP lib.

    (Feel free to add your name if you have contributed to this code)
    
    Important notes:
        - The QML dash files are from the previous version (C++/Qt)
        - Every code that has some kind of physics related calculations goes to VD lib
"""

import sys
import os
from typing import Mapping
from PySide2.QtWidgets import QApplication
from PySide2.QtQuick import QQuickView
from PySide2.QtCore import QUrl
from threading import Thread as Process
from fcp import Fcp, Spec, CANMessage
from fcp import FcpCom
from libs.logger import Logger
from libs.dashValues import DashValues
from libs.bridge_endurance import Bridge
from libs.steeringwheel_handler import ButtonHandler
from result import Ok, Err
import json
import time
import can
import select

global dash_values_obj
app = QApplication([])
app_running = True
view = QQuickView()
global_cfg = {}
current_theme = {}
dash_log = Logger("dash_log.txt")
dash_log.log("Starting Dash...")
global mapping_available
global mapping
mapping_available = False


with open("cfgs/dash_config_debug.json") as f:
    global_cfg = json.loads(f.read())
    current_theme = global_cfg["themes"][global_cfg["themes"]["use_theme"]]


def verify_timeout():
    while app_running:
        dash_values_obj.verifyValues()
        time.sleep(0.1)


def can_error(e, can):
    print("Error! " + str(e))
    print("Socket CAN Aborting!")
    changeScreen("errorScreen.qml")
    return


def socketCAN():
    vcan_socks = []
    for can_interface in global_cfg["coms"]["can_interface"]:
        try:
            vcan_socks.append(can.interface.Bus(can_interface, bustype="socketcan"))
        except OSError as e:
            can_error(e, can_interface)
            return
    spec_obj = Spec()
    with open("cfgs/fst10e.json") as f:
        spec_obj.decompile(json.loads(f.read()))
    fcp_obj = Fcp(spec_obj)
    proxy = Proxy(vcan_socks)
    fcp_com = FcpCom(fcp_obj, proxy, 26)
    # the 26 arg needs to be changed
    fcp_com.start()
    while app_running:
        decoded_msg = fcp_com.q.get()
        dash_values_obj.updateValues(decoded_msg)
        if mapping_available:
            mapping.check(dash_values_obj, decoded_msg, fcp_com)


class Proxy:
    def __init__(self, sockets):
        self.sockets = sockets

    def recv(self):
        reads, _, _ = select.select(self.sockets, [], [])
        data = reads.pop().recv()
        if data.arbitration_id in global_cfg["coms"]["sid_whitelist"]:
            n_data = 0
            dash_log.log(str(data.data))
            for i, dataa in enumerate(data.data):
                n_data += dataa << (8 * i)
            new_can_msg = CANMessage(
                data.arbitration_id, data.dlc, data.timestamp, data64=n_data
            )
            return Ok(new_can_msg)
        return Err("No message found!")

    def send(self, msg):
        message = can.Message(arbitration_id=msg.sid, data=msg.get_data8())
        for socket in self.sockets:
            socket.send(message)


def changeScreen(screen_str):
    global mapping_available
    global mapping
    qml_file = os.path.join(
        os.path.dirname(__file__), "resources/screens/" + screen_str
    )
    view.setSource(QUrl.fromLocalFile(os.path.abspath(qml_file)))
    mapping = ButtonHandler(global_cfg, screen_str)
    mapping_available = mapping.valid()


if __name__ == "__main__":
    dash_values_obj = DashValues(global_cfg)
    can_reader = Process(target=socketCAN)
    timeout_verify = Process(target=verify_timeout)
    can_reader.start()
    timeout_verify.start()
    bridge = Bridge(dash_values_obj, current_theme)
    # Expose the Python object to QML
    context = view.rootContext()
    context.setContextProperty("dash", bridge)
    # Get the path of the current directory, and then add the name
    # of the QML file, to load it.
    changeScreen("enduranceScreen.qml")
    view.show()
    app.exec_()
    app_running = False
