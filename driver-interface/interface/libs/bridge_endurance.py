from PySide2.QtCore import QObject, Slot
import libs.VD as VD


class Bridge(QObject):
    def __init__(self, dash_vls, theme):
        super().__init__()
        self.dash_values = dash_vls
        self.current_theme = theme

    @Slot(result=str)
    def getSpeed(self):
        if self.dash_values.getValue("amk_actual_speed").value is not None:
            return str(
                round(
                    VD.calc_velocity(
                        self.dash_values.getValue("amk_actual_speed").value
                    )
                )
            )
        else:
            return self.current_theme["nan_str"]

    @Slot(result=str)
    def getSpeedColor(self):
        return self.dash_values.getValue("amk_actual_speed").color

    @Slot(result=str)
    def getSoC(self):
        if self.dash_values.getValue("master_em_soc").value is not None:
            val = self.dash_values.getValue("master_em_soc").value / 10

            self.dash_values.getValue("master_em_soc").color = self.current_theme[
                self.dash_values.getStatusColor("master_em_soc", val)
            ]

            return str(round(self.dash_values.getValue("master_em_soc").value / 10))
        else:
            return self.current_theme["nan_str"]

    @Slot(result=str)
    def getSoCColor(self):
        return self.dash_values.getValue("master_em_soc").color

    @Slot(result=str)
    def getBattery(self):
        val1 = self.dash_values.getValue("cell_max_tmp").value
        if val1 is not None:
            self.dash_values.getValue("cell_max_tmp").color = self.current_theme[
                self.dash_values.getStatusColor("cell_max_tmp", val1)
            ]
            return str(round(self.dash_values.getValue("cell_max_tmp").value))
        else:
            return self.current_theme["nan_str"]

    @Slot(result=str)
    def getBatteryColor(self):
        return self.dash_values.getValue("cell_max_tmp").color

    @Slot(result=str)
    def getTorque0(self):
        val0 = self.dash_values.getValue("max_sfty_torque0").value
        if val0 is not None:
            self.dash_values.getValue("max_sfty_torque0").color = self.current_theme[
                self.dash_values.getStatusColor("max_sfty_torque0", val0)
            ]
            return str(
                round(self.dash_values.getValue("max_sfty_torque0").value / 1000)
            )
        else:
            return self.current_theme["nan_str"]

    @Slot(result=str)
    def getTorque0Color(self):
        return self.dash_values.getValue("max_sfty_torque0").color

    @Slot(result=str)
    def getTorque2(self):
        val3 = self.dash_values.getValue("max_sfty_torque2").value
        if val3 is not None:
            self.dash_values.getValue("max_sfty_torque2").color = self.current_theme[
                self.dash_values.getStatusColor("max_sfty_torque2", val3)
            ]
            return str(
                round(self.dash_values.getValue("max_sfty_torque2").value / 1000)
            )
        else:
            return self.current_theme["nan_str"]

    @Slot(result=str)
    def getTorque2Color(self):
        return self.dash_values.getValue("max_sfty_torque2").color

    @Slot(result=str)
    def getDiff(self):
        val2 = self.dash_values.getValue("iib_diff_gain").value
        if val2 is not None:
            self.dash_values.getValue("iib_diff_gain").color = self.current_theme[
                self.dash_values.getStatusColor("iib_diff_gain", val2)
            ]
            return str(round(self.dash_values.getValue("iib_diff_gain").value))
        else:
            return self.current_theme["nan_str"]

    @Slot(result=str)
    def getDiffColor(self):
        return self.dash_values.getValue("iib_diff_gain").color

    @Slot(result=str)
    def getBias(self):
        front = self.dash_values.getValue("te_press_f").value
        rear = self.dash_values.getValue("te_press_r").value
        if (front is not None) and (rear is not None) and (2 * front + rear) > 0:
            bias = round(VD.calc_brake_bias(front, rear), 2)
            self.dash_values.getValue("te_press_f").color = self.current_theme[
                self.dash_values.getStatusColor("te_press_f", bias)
            ]
            return str(bias)
        else:
            return self.current_theme["nan_str"]

    @Slot(result=str)
    def getBiasColor(self):
        return self.dash_values.getValue("te_press_f").color

    @Slot(result=str)
    def getBackgroundColor(self):
        return self.current_theme["default_background_color"]
