global is_active
is_active = False


def decrease_torque(values, fcp_com):
    torque_front = values.getValue("max_sfty_torque0").value - 1000
    torque_rear = values.getValue("max_sfty_torque2").value - 1000
    fcp_com.set("iib", "torque_fr", torque_front)
    fcp_com.set("iib", "torque_fl", torque_front)
    fcp_com.set("iib", "torque_rr", torque_rear)
    fcp_com.set("iib", "torque_rl", torque_rear)


def increase_torque(values, fcp_com):
    torque_front = values.getValue("max_sfty_torque0").value + 1000
    torque_rear = values.getValue("max_sfty_torque2").value + 1000
    fcp_com.set("iib", "torque_fr", torque_front)
    fcp_com.set("iib", "torque_fl", torque_front)
    fcp_com.set("iib", "torque_rr", torque_rear)
    fcp_com.set("iib", "torque_rl", torque_rear)


def select_src(args):
    print("Changing Source")
    print("Source --> " + args["src"])
    print("Name --> " + args["name"])


def toggle_interface(args):
    global is_active
    is_active = args["state"]


class ButtonHandler:
    def __init__(self, cfg, screen_name):
        self.id_mapping = cfg["coms"]["sid_buttons"]
        if screen_name not in cfg["mapping"]:
            self.is_valid = False
        else:
            self.is_valid = True
        self.mapping = cfg["mapping"][screen_name]

    def get_button(self, id):
        return self.id_mapping[id]

    def check(self, values, decoded_value, fcp_com):
        name, signals = decoded_value
        if name in list(self.id_mapping.keys()):
            if "me" in name:
                if name in list(self.mapping.keys()):
                    selector = self.mapping[name]
                    selector_value = str(round(decoded_value[1][name + "_pos"]))
                    if selector_value in list(selector.keys()):
                        if (not is_active and name == "me1") or is_active:
                            globals()[selector[selector_value]["function"]](
                                selector[selector_value]
                            )
                            print("Okay")
            else:
                globals()[self.mapping[name]](values, fcp_com)

    def valid(self):
        return self.is_valid
