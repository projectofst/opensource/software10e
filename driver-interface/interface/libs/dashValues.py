from dataclasses import dataclass


@dataclass
class DashValue:
    value: float
    color: str
    watchdog: int
    timeout: int


class DashValues:
    def __init__(self, g_cfg):
        self.dash_values = {}
        self.global_cfg = g_cfg
        self.current_theme = self.global_cfg["themes"][
            self.global_cfg["themes"]["use_theme"]
        ]

        for key in self.global_cfg["dash_values"]:
            self.dash_values[key] = DashValue(
                value=0,
                timeout=self.global_cfg["dash_values"][key]["timeout"],
                watchdog=0,
                color=self.current_theme[self.global_cfg["dash_values"][key]["color"]],
            )

    def updateValues(self, new_data):
        _, signals = new_data
        for key in signals:
            if key in self.dash_values:
                self.dash_values[key].value = new_data[1][key]
                self.dash_values[key].watchdog = 0
                self.dash_values[key].color = self.current_theme["default_font_color"]

    def verifyValues(self):
        for value in self.dash_values.values():
            value.watchdog += 1
            if value.watchdog > value.timeout:
                value.value = None
                value.color = self.current_theme["default_font_missing_color"]

    def getValue(self, dash_parameter):
        return self.dash_values[dash_parameter]

    def getStatusColor(self, dash_parameter, value):
        widget_ranges = self.global_cfg["dash_values"][dash_parameter]["color_ranges"]
        else_color = "default_font_color"
        for key in widget_ranges:
            if isinstance(widget_ranges[key], list):
                if value > widget_ranges[key][0] and value <= widget_ranges[key][1]:
                    return key
            else:
                else_color = key
        return else_color
