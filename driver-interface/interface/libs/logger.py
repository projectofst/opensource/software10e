from datetime import datetime


class Logger:
    def __init__(self, filename):
        self.filename = filename

    def log(self, string):
        self.file = open(self.filename, "a")
        now = datetime.now()
        line = now.strftime("%m/%d/%Y, %H:%M:%S") + " - " + string + "\n"
        self.file.write(line)
        self.file.close()
