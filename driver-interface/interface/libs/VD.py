import math

GEAR_RATIO = 16
WHEEL_RADIUS = 0.22


def calc_velocity(amk_speed):
    return 2 * math.pi * (abs(amk_speed) / GEAR_RATIO / 60) * WHEEL_RADIUS * 3.6


def calc_brake_bias(front, rear):
    return (2 * front) / (2 * front + rear)
