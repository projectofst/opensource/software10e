cd interface

read -r -p "Clean last build? [Y/n] " input

case $input in
    [yY][eE][sS]|[yY])
    rm -rf build
    rm -rf __pycache__
    rm -rf dist
    rm dash.spec
 ;;
    [nN][oO]|[nN])
 ;;
    *)
 echo "Invalid input..."
 exit 1
 ;;
esac

read -r -p "Proceed to build? [Y/n] " input

case $input in
    [yY][eE][sS]|[yY])
 ;;
    [nN][oO]|[nN])
    exit 1
 ;;
    *)
 echo "Invalid input..."
 exit 1
 ;;
esac

read -r -p "Use default release config file? [Y/n] " input
 
case $input in
    [yY][eE][sS]|[yY])
    default_config=1
 ;;
    [nN][oO]|[nN])
    default_config=0
 ;;
    *)
 echo "Invalid input..."
 exit 1
 ;;
esac

echo "Building FST DI..."

if [ $default_config == 1 ]
then
    sed 's/config_debug/config/g' interface/dash.py > interface/dash_release.py
    pyinstaller dash_release.py
    directory="dash_release"
else
    pyinstaller dash.py
    directory="dash"
fi

echo "Executable exported to interface/dist/dash/${directory}"

echo "Copying libs..."
cp -r libs dist/${directory}

echo "Copying resources..."
cp -r resources dist/${directory}

echo "Copying cfgs..."
cp -r cfgs dist/${directory}

echo "Copying CAN interfaces modules..."
mkdir dist/${directory}/can
cp -r build/${directory}/$(ls build/${directory} | grep "can")/can/interfaces dist/${directory}/can/interfaces
