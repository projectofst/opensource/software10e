/* 
 * progmux is the firmware of a multiplexer for Microchip Pickit3/4.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */



// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128 // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4 // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
// NOTE: Always include timing.h
#include "lib_pic33e/timing.h"
#include <stdint.h>

// Trap handling
#include "can-ids/can_cmd.h"
#include "can-ids/can_ids.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/pps.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/trap.h"
#include "shared/scheduler/scheduler.h"

#define LED_1 LATBbits.LATB6
#define LED_2 LATBbits.LATB7
#define LED_3 LATBbits.LATB8
#define LED_4 LATBbits.LATB9
#define LED_5 LATBbits.LATB10
#define LED_6 LATBbits.LATB11
#define LED_7 LATBbits.LATB12
#define LED_8 LATBbits.LATB13

#define LED_1_enable TRISBbits.TRISB6
#define LED_2_enable TRISBbits.TRISB7
#define LED_3_enable TRISBbits.TRISB8
#define LED_4_enable TRISBbits.TRISB9
#define LED_5_enable TRISBbits.TRISB10
#define LED_6_enable TRISBbits.TRISB11
#define LED_7_enable TRISBbits.TRISB12
#define LED_8_enable TRISBbits.TRISB13

#define PinSel_1 LATBbits.LATB3
#define PinSel_2 LATBbits.LATB4
#define PinSel_3 LATBbits.LATB5

#define PinSel_1_enable TRISBbits.TRISB3
#define PinSel_2_enable TRISBbits.TRISB4
#define PinSel_3_enable TRISBbits.TRISB5

#define TLED LATEbits.LATE0
#define LED_GREEN LATBbits.LATB14
#define LED_RED LATBbits.LATB15

#define TLED_enable TRISEbits.TRISE0
#define LED_GREEN_enable TRISBbits.TRISB14
#define LED_RED_enable TRISBbits.TRISB15

can_t can_global;

uint16_t prog_sequence = 0;

uint32_t timer = 0;
void timer1_callback(void)
{
    timer++;
    return;
}

uint16_t dev_get_id()
{
    return 6;
}

void dev_send_msg(CANdata msg)
{
    send_can1(msg);
}

void debug_leds(void)
{
    TLED ^= 1;
    if (prog_sequence > 0) {
        prog_sequence--;
        LED_GREEN ^= 1;
    }
}

int16_t select_channel(uint16_t chan)
{
    if (chan > 8) {
        return -1;
    }

    printf("chan %d\n", 1UL << (chan + 6));
    LATB &= ~(0xFF << 6);
    LATB |= 1UL << (chan + 6);

    LATB &= ~(0x7 << 3);
    LATB |= chan << 3;
    //PinSel_1 = (chan >> 0) & 1;
    //PinSel_2 = (chan >> 1) & 1;
    //PinSel_3 = (chan >> 2) & 1;

    prog_sequence = 10;

    can_global.prog_mux.prog_mux_status.prog_mux_channel = chan;
    return chan;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{

    multiple_return_t mt;

    if (id == CMD_PROG_MUX_SET_CHANNEL) {
        mt.arg1 = select_channel(arg1);
    }

    return mt;
}

void recv_msg(void)
{

    if (!can1_rx_empty()) {
        CANdata msg = pop_can1();
        cmd_dispatch(msg);
    }

    return;
}

void config()
{
    PinSel_1_enable = 0;
    PinSel_2_enable = 0;
    PinSel_3_enable = 0;

    LED_1_enable = 0;
    LED_2_enable = 0;
    LED_3_enable = 0;
    LED_4_enable = 0;
    LED_5_enable = 0;
    LED_6_enable = 0;
    LED_7_enable = 0;
    LED_8_enable = 0;

    LED_GREEN_enable = 0;
    LED_RED_enable = 0;
    TLED_enable = 0;

    config_timer1(1, 4);

    //RGB LED Configuration
    TRISEbits.TRISE3 = 0; //Red
    TRISEbits.TRISE5 = 0; //Green
    TRISEbits.TRISE7 = 0; //Blue

    //CAN
    PPSUnLock;
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
    PPSLock;

    config_can1(NULL);
}

void send_msgs(void)
{
    prog_mux_send_msgs(can_global.prog_mux, timer);
}

int main()
{
    config();

    tasks_t progmux_tasks[] = {
        { .period = 1000, .func = debug_leds },
        { .period = 0, .func = recv_msg },
        { .period = 0, .func = send_msgs },

    };

    select_channel(1);

    scheduler_init(progmux_tasks, sizeof(progmux_tasks) / sizeof(tasks_t));

    while (1) {
        scheduler(timer);
        ClrWdt();
    } //end while

    return 0;
}

void trap_handler(TrapType type)
{
    switch (type) {
    case HARD_TRAP_OSCILATOR_FAIL:
        break;
    case HARD_TRAP_ADDRESS_ERROR:
        break;
    case HARD_TRAP_STACK_ERROR:
        break;
    case HARD_TRAP_MATH_ERROR:
        break;
    case CUSTOM_TRAP_PARSE_ERROR:
        break;
    default:
        break;
    }

    return;
}
