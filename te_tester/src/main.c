/* 
 * te tester is a Formula Student torque encoder functionalty tester software 
 * used  in Formula Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF				// General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF				// General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF				// General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC				// Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF				// Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS				// Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF			// OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF			// Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD			// Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768		// Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128			// Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON				// PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF				// Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON				// Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4		        // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON				// Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF			// Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE				// ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF				// Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF				// JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF				// Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF				// Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF				// Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
// NOTE: Always include timing.h
#include "lib_pic33e/timing.h"

//Comment this line if USB is not needed
#include "lib_pic33e/usb_lib.h"

// Trap handling
#include "lib_pic33e/trap.h"

int main(){
	uint16_t count = 0; 

    //RGB LED Configuration
    TRISEbits.TRISE3 = 0;//Red
    TRISEbits.TRISE5 = 0;//Green
    TRISEbits.TRISE7 = 0;//Blue

    USBInit();
		
    while(1)
    {
		uprintf("ola %4d\n", count++);
        USBTasks();

        LATEbits.LATE3 ^= 1;
        __delay_ms(500);

       //Clear watchdog timer
       ClrWdt();
    }//end while

    return 0;
}//end main


void trap_handler(TrapType type){

    switch(type){
        case HARD_TRAP_OSCILATOR_FAIL:
            break;
        case HARD_TRAP_ADDRESS_ERROR:
            break;
        case HARD_TRAP_STACK_ERROR:
            break;
        case HARD_TRAP_MATH_ERROR:
            break;
        case CUSTOM_TRAP_PARSE_ERROR:
            break;
        default:
            break;
    }


    return;
}
