#include <xc.h>

#include "ADC.h"

void config_adc1 (ADC_parameters parameter){

    AD1CON1bits.ADSIDL  = ~parameter.idle;
    AD1CON1bits.AD12B   = 1;
    AD1CON1bits.FORM    = parameter.form;
    AD1CON1bits.SSRCG   = 0;
    AD1CON1bits.SSRC    = parameter.conversion_trigger;
    AD1CON1bits.ASAM    = parameter.sampling_type;

    AD1CON2bits.VCFG    = parameter.voltage_ref;
    AD1CON2bits.CSCNA   = parameter.sample_pin_select_type;
    AD1CON2bits.SMPI    = parameter.smpi;
    AD1CON2bits.BUFM    = 0;                                    //Always starts filling the buffer from the Start address
    AD1CON2bits.ALTS    = 0;                                    /* always use MUX A                              */

    AD1CON3bits.SAMC    = parameter.auto_sample_time;           //Auto-Sample Time (31 is MAX)
    AD1CON3bits.ADRC    = 0;                                    //Clock derived from system clock                              
    AD1CON3bits.ADCS    = parameter.conversion_time;            //ADC Conversion Clock 256.TAD (255 is MAX)             
    
    AD1CON4bits.ADDMAEN = 0;

    //ANSEL##parameter.port## = parameter.pin_select;           /*set pins as analog inputs                       */
    ANSELB  = parameter.pin_select;                             /*set pins as analog inputs                       */
    AD1CSSL = parameter.pin_select;                             /*select which pins to scan                       */

    AD1CON1bits.ADON = parameter.turn_on;

    /* interrupts */
    IFS0bits.AD1IF = 0;                                       /*clear interrupt flag                            */
    IEC0bits.AD1IE = (parameter.interrupt_priority > 0);      /*enable interrupt if the priority is more then 0 */
    IPC3bits.AD1IP = parameter.interrupt_priority;

}

void __attribute__((weak)) adc_callback(void) {
	return;
}

/**********************************************************************
 * Assign End of ADC conversion interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _AD1Interrupt (void){
    adc_callback();
    IFS0bits.AD1IF = 0;        /*clear interrupt flag */
    return;
}

/**********************************************************************
 * Name:    get_adc_value
 * Args:    the sample you want to retrive (the firs one is sample_number 0,
 *          the second one is sample_number one...)
 * Return:  the value read by the adc
 * Desc:    gets the adc value from the buffer and returns it
 **********************************************************************/

unsigned int get_adc_value(unsigned int sample_number){
    if(sample_number>15){
        return -1;
    }else{
        return *(&ADC1BUF0 + sample_number);
    }
}

/**********************************************************************
 * Name:    turn_on_adc
 * Args:    -
 * Return:  -
 * Desc:    turns on the adc module
 **********************************************************************/
void turn_on_adc(){
    AD1CON1bits.ADON = 1;
}

/**********************************************************************
 * Name:    turn_off_adc
 * Args:    -
 * Return:  -
 * Desc:    turns off the adc module
 **********************************************************************/
void turn_off_adc(){
    AD1CON1bits.ADON = 0;
}

/**********************************************************************
 * Name:    start_samp
 * Args:    -
 * Return:  -
 * Desc:    starts a new sample 
 * IMPORTANTE: only use this fuction if your sampling_type is set to MANUAL
 **********************************************************************/
void start_samp(){
    AD1CON1bits.SAMP = 1;
}

/**********************************************************************
 * Name:    start_conversion
 * Args:    -
 * Return:  -
 * Desc:    ends sampling and starts conversion
 * IMPORTANTE: only use this fuction if your conversion_trigger is set to SAMP
 **********************************************************************/
void start_conversion(){
    AD1CON1bits.SAMP = 0;
}

/**********************************************************************
 * Name:    wait_for_done
 * Args:    -
 * Return:  -
 * Desc:    waits until conversion is done
 **********************************************************************/
void wait_for_done(){
    while(!AD1CON1bits.DONE);
    return;
}

/**********************************************************************
 * Name:    set_default_parameters
 * Args:    ADC_parameters * (empty struct to be filled)
 * Return:  -
 * Desc:    fills a ADC_parameters struct with default configuration
 **********************************************************************/
void set_default_parameters(ADC_parameters *parameters){

    parameters->idle = 1;                        /*works in idle mode                       */
    parameters->form = 0;                        /*form set to unsigned integer             */
    parameters->conversion_trigger = AUTO_CONV;  /*auto conversion                          */
    parameters->sampling_type = AUTO;            /*auto sampling                            */
    parameters->voltage_ref = INTERNAL;          /*use internal voltage reference           */
    parameters->sample_pin_select_type = AUTO;    /*auto change between differrent inputs    */
    parameters->smpi = 0;                        /*1 sample per interrupt                   */
    parameters->pin_select = 1;                  /*scan pin AN0                             */
    parameters->interrupt_priority = 5;          /*interrupt priority set to 5              */
    parameters->turn_on = 1;                     /*turn the module on                       */
    return;

}

/**********************************************************************
 * Name:    select_input_pin
 * Args:    number of the pin you want to scan
 * Return:  -
 * Desc:    defines what pin to scan next
 **********************************************************************/
void select_input_pin(unsigned int pin){
    AD1CHS0bits.CH0SA = pin;
    return;
}
