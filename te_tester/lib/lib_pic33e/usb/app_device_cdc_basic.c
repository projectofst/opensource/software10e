/*******************************************************************************
Copyright 2016 Microchip Technology Inc. (www.microchip.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

To request to license the code under the MLA license (www.microchip.com/mla_license), 
please contact mla_licensing@microchip.com
*******************************************************************************/

/** INCLUDES *******************************************************/
#include "system.h"

#include <stdint.h>
#include <string.h>
#include <stddef.h>

#include "usb.h"

#include "app_device_cdc_basic.h"
#include "usb_config.h"

#include "../usb_lib.h"

/** VARIABLES ******************************************************/

//static bool buttonPressed;
//static char buttonMessage[] = "Button pressed.\r\n";
static uint8_t readBuffer[CDC_DATA_OUT_EP_SIZE];
static uint8_t writeBuffer[CDC_DATA_IN_EP_SIZE];

//maximum bulk transfer size
uint8_t outBuffer[BULK_SIZE];

unsigned int counter=0;




/*********************************************************************
* Function: void APP_DeviceCDCBasicDemoInitialize(void);
*
* Overview: Initializes the demo code
*
* PreCondition: None
*
* Input: None
*
* Output: None
*
********************************************************************/
void APP_DeviceCDCBasicDemoInitialize()
{   
    line_coding.bCharFormat = 0;
    line_coding.bDataBits = 8;
    line_coding.bParityType = 0;
    line_coding.dwDTERate = 9600;
}

/*********************************************************************
* Function: void APP_DeviceCDCBasicDemoTasks(void);
*
* Overview: Keeps the demo running.
*
* PreCondition:pThe demo should have been initialized and started via
*   the APP_DeviceCDCBasicDemoInitialize() and APP_DeviceCDCBasicDemoStart() demos
*   respectively.
*
* Input: None
*
* Output: None
*
********************************************************************/
void APP_DeviceCDCBasicDemoTasks()
{
    /* If the USB device isn't configured yet, we can't really do anything
     * else since we don't have a host to talk to.  So jump back to the
     * top of the while loop. */
    if( USBGetDeviceState() < CONFIGURED_STATE )
    {
        return;
    }


    /* If we are currently suspended, then we need to see if we need to
     * issue a remote wakeup.  In either case, we shouldn't process any
     * keyboard commands since we aren't currently communicating to the host
     * thus just continue back to the start of the while loop. */
    if( USBIsDeviceSuspended() == true )
    {
        return;
    }
        
    /*Deleted code that sent a message everytime a button was pressed, 
    using functions mUSBUSARTIsTxTrfReady() and putrsUSBUSART(String message)*/

    /* Check to see if there is a transmission in progress, if there isn't, then
     * we can see about performing an echo response to data received.
     */
    if(USBUSARTIsTxTrfReady())
    {
        int i=0;
        uint8_t numBytesRead;

        char readString[CDC_DATA_OUT_EP_SIZE];

        numBytesRead = getsUSBUSART(readBuffer, sizeof(readBuffer));

        /* For every byte that was read... */
        for(i=0; i<numBytesRead; i++){
            //echo everything that is read
            readString[i] = readBuffer[i];        
            writeBuffer[i] = readBuffer[i]; 
        }
        readString[i]='\0';

        if(numBytesRead > 0){
            /* After processing all of the received data, we need to send out
                * the "echo" data now.
            */
            receive_handler_usb(readString);
			return;
        }
            //if nothing is read, send pending messages
        else{

            if (read_index==write_index && around_flag==0){
                //LATEbits.LATE3=1;
                 return;
            }

            i=0;
            while((i<(BULK_SIZE-1)) && (read_index!=write_index)){
                outBuffer[i]=gpacket_buffer[read_index];
                read_index=((read_index+1)%BUFFER_SIZE);

                i++;

                if (read_index == 0)
                    around_flag = 0;
            }
            outBuffer[i]='\0';

            putUSBUSART(outBuffer, strlen(outBuffer));

            counter++;      
        }
    }
    CDCTxService();    
}

