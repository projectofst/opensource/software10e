#include <xc.h>
#include "lib_pic33e/timing.h"

// Configure PLL. See dsPIC33 Family Reference section 7. Oscillator,
// subsection 7.0 Phase-Locked Loop (PLL).
void __attribute__((user_init)) config_clock(void)
{
	/*
	 * FIN is external primary oscillator.
	 * FPPLIN = FIN / N1, must be in [0.8, 8] MHz.
	 * FVCO = FPPLIN * M, must be in [120, 340] MHz.
	 * FOSC = FVCO / N2, must be in [15, 140] MHz (max. temperature 85C).
	 * Overall:
	 * FOSC = FIN * M/(N1*N2) =
	 * = FIN * (PLLFDB + 2)/((PPLPRE + 2) * 2(PLLPOST + 1))
	 */
	#if FCY64 // 64 MHz
		CLKDIVbits.PLLPRE = 2; // N1 = 4
		PLLFBD = 62; // M = 64
		CLKDIVbits.PLLPOST = 0b00; // N2 = 2
		// Overall PLL: X8 = 64/(4*2)
	#elif FCY70 // 70 MHz
		CLKDIVbits.PLLPRE = 2; // N1 = 4
		PLLFBD = 68; // M = 70
		CLKDIVbits.PLLPOST = 0b00; // N2 = 2
		// Overall PLL: X8.75 = 70/(4*2)
	#elif FAST // 106 MHz
		// Gotta go fast
		CLKDIVbits.PLLPRE = 2; // N1 = 4
		PLLFBD = 104; // M = 106
		CLKDIVbits.PLLPOST = 0b00; // N2 = 2
		// Overall PLL: X13.25 = 106/(4*2)
	#else // DEFAULT: 16 MHz
		CLKDIVbits.PLLPRE = 2; // N1 = 4
		PLLFBD = 30; // M = 32
		CLKDIVbits.PLLPOST = 0b01; // N2 = 4
		// Overall PLL: X2 = 32/(4*4)
	#endif

	// Initiate Clock Switch to Primary Oscillator with PLL (NOSC=0b011)
	__builtin_write_OSCCONH(0b011);
	__builtin_write_OSCCONL(OSCCONL | 0b1);

	// Wait for Clock switch to occur
	while (OSCCONbits.COSC != 0b011);

	// Wait for PLL to lock
	while (OSCCONbits.LOCK != 1);

    //USB Clock configuration assuming 16MHz crystal
    
    /*Configure the auxilliary PLL to provide 48MHz needed for USB Operation.*/
    
    // Configuring the auxiliary PLL, since the primary
    // oscillator provides the source clock to the auxiliary
    // PLL, the auxiliary oscillator is disabled. Note that
    // the AUX PLL is enabled. The input 16MHz clock is divided
    // by 2, multiplied by 24 and then divided by 4. Wait till
    // the AUX PLL locks.
    
    ACLKCON3bits.ENAPLL = 0; // APLL is disabled, the USB clock source is the input clock to the APLL
    ACLKCON3bits.SELACLK = 1; // Auxiliary PLL or oscillators provide the source clock for the Auxiliary Clock divider
    ACLKCON3bits.AOSCMD = 0b00; // (AOSC) Auxiliary Oscillator is disabled (default)
    ACLKCON3bits.ASRCSEL = 1; // Primary Oscillator is the clock source for APLL
    ACLKCON3bits.FRCSEL = 0; // Auxiliary Oscillator or Primary Oscillator is the clock source for APLL (determined by ASRCSEL bit)
    ACLKCON3bits.APLLPOST = 0b110; // Divided by 2
    ACLKCON3bits.APLLPRE = 0b011; // Divided by 4
    
    ACLKDIV3bits.APLLDIV = 0b111;
    
    ACLKCON3bits.ENAPLL = 1;
    while(ACLKCON3bits.APLLCK != 1);
}
