#include <xc.h>

/**********************************************************************
 * Name:	set_CPU_priority
 * Args:    unsigned int n
 * Return:	previous priority OR error code
 * Desc:	Sets CPU priority to n, provided it is between 0 and 7.
 *          Provides a way to disable interruptions in critical parts
 *          of the code.
 *          Needs to be set to 6 or lower to allow interruptions,
 *          depending on interruptions' priorities.
 *          Previous priority returned for restoring purposes.
 **********************************************************************/
int set_CPU_priority(unsigned int n){

	unsigned int prevIPL;

	if(n<=7){
		prevIPL = SRbits.IPL;
		SRbits.IPL = n; 			/* Status Register . CPU priority 0-7 */
		return prevIPL;
	}
	return -1;
}
