#ifndef _USB_LIB_H
#define _USB_LIB_H

#include "lib_pic33e/usb/system.h"
#include "lib_pic33e/usb/app_device_cdc_basic.h"
#include "lib_pic33e/usb/usb.h"
#include "lib_pic33e/usb/usb_device.h"
#include "lib_pic33e/usb/usb_device_cdc.h"

enum _uprintf_ERROR {
    STRING_OVER_MAX = 0,
    BUFFER_FULL     = 1,
};


#define PACKETS_PER_BUFFER 10
#define PACKET_SIZE 255

#define BUFFER_SIZE (PACKET_SIZE * PACKETS_PER_BUFFER)


extern uint8_t gpacket_buffer[BUFFER_SIZE];
extern unsigned volatile int write_index;
extern unsigned volatile int read_index;

extern volatile unsigned int around_flag;



//Blue Led lights up when usb is successfully configured
#define USB_BLUE_LED

void USBInit();

void USBTasks();

void receive_handler_usb(char *readString);

void uprintf(char *format, ...);
void append_string_usb(uint8_t * buffer, unsigned int len);

#endif
