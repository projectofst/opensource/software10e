// FST Lisboa
// Clock definitions

#ifndef _LIB_PIC33E_TIMING_H
#define _LIB_PIC33E_TIMING_H

#if FCY64 // 64 MHz
    #define FCY 64000000UL
    #define FOSC FCY*2
#elif FCY70 // 70 MHz
    #define FCY 70000000UL
    #define FOSC FCY*2
#elif FAST // 106 MHz
    // Gotta go fast
    #define FCY 106000000UL
    #define FOSC FCY*2
#else // DEFAULT: 16 MHz
    #define FCY 16000000UL
    #define FOSC FCY*2
#endif

/**
 * The delay functions were copied from the libpic30.h from microchip. 
 * If you need to include the libpic30.h for other reason other than 
 * the delay then you should take care with the multiple definition 
 * of the delay functions
 */

/*
 *
 * __delay32() provides a 32-bit delay routine which will delay for the number
 * of cycles denoted by its argument.  The minimum delay is 12 cycles
 * including call and return statements.  With this function only the time
 * required for argument transmission is not accounted for.  Requests for
 * less than 12 cycles of delay will cause an 12 cycle delay.
 */

extern void __delay32(unsigned long cycles);

/*
 * __delay_ms() and __delay_us() are defined as macros. They depend
 * on a user-supplied definition of FCY. If FCY is defined, the argument
 * is converted and passed to __delay32(). Otherwise, the functions
 * are declared external.
 *
 * For example, to declare FCY for a 10 MHz instruction rate:
 *
 * #define FCY 10000000UL
 */

#if !defined(FCY)
extern void __delay_ms(unsigned long);
extern void __delay_us(unsigned long);
#else
#define __delay_ms(d) \
  { __delay32( (unsigned long) (((unsigned long long) d)*(FCY)/1000ULL)); }
#define __delay_us(d) \
  { __delay32( (unsigned long) (((unsigned long long) d)*(FCY)/1000000ULL)); }
#endif


// Configure PLL. See dsPIC33 Family Reference section 7. Oscillator,
// subsection 7.0 Phase-Locked Loop (PLL).
void __attribute__((user_init)) config_clock(void);

#endif
