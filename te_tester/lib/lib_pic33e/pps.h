// FST Lisboa
// Set default PPS mappings per the PIC33EP SchDoc and devboard.

#ifndef _LIB_PIC33E_PPS_H
#define _LIB_PIC33E_PPS_H

#include <pps.h>
#include <xc.h>

// See section 10. IO/Ports, subsection 4 of the family reference.

// CAN
void config_pps_can(void)
{

	TRISGbits.TRISG6 = 0;
	TRISGbits.TRISG7 = 1;
	TRISGbits.TRISG8 = 0;
	TRISGbits.TRISG9 = 1;
	PPSUnLock;
	PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
	PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
	PPSOutput(OUT_FN_PPS_C2TX, OUT_PIN_PPS_RP120);
	PPSInput(IN_FN_PPS_C2RX, IN_PIN_PPS_RPI121);
	PPSLock;
}

// UART
void config_pps_uart(void)
{

	TRISGbits.TRISG8 = 0;
	TRISGbits.TRISG9 = 1;
	// Unlock PPS Registers: clear bit 6 IOLOCK in OSCCONL
	__builtin_write_OSCCONL(OSCCONL & ~(1<<6));
	
    // Map UART1 and UART2
	RPINR18bits.U1RXR = 97; // input: U1RX => RP97
    RPOR7bits.RP96R = 0b000001; // output: RP96 => U1TX
    RPINR19bits.U2RXR = 100; //input: U2RX => RP100
    RPOR8bits.RP99R = 0b000011; // output: RP99 => U2TX

	// Lock PPS Registers: set bit 6 IOLOCK in OSCCONL
	__builtin_write_OSCCONL(OSCCONL | (1<<6));
}

// SPI
void config_pps_spi(void)
{
    // Map SPI1 and SPI3
}

#endif
