#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "usb_lib.h"

unsigned volatile int write_index=0;
unsigned volatile int read_index=0;


volatile unsigned int around_flag=0;
uint8_t gpacket_buffer[BUFFER_SIZE];

__attribute__((weak)) void receive_handler_usb(char *readString){
    
}

void USBInit(){

    USBDeviceInit();
    USBDeviceAttach();
}

void USBTasks(){

    SYSTEM_Tasks();

    #if defined(USB_BLUE_LED)  
    if (USBDeviceState==CONFIGURED_STATE){
        LATEbits.LATE7=1;
    } 
    else{
        LATEbits.LATE7=0;
    }
    #endif

    //Application specific tasks        
    APP_DeviceCDCBasicDemoTasks();
}

void append_string_usb(uint8_t *buffer, unsigned int len){
    int i;
    if(around_flag){
        if(write_index+len>=read_index)
            return;
        
        for(i = 0 ; i<len ; i ++){
            gpacket_buffer[write_index] = buffer[i];
            write_index = ((write_index + 1) % BUFFER_SIZE);
        }
    }

    /*if the write and read indexes are on the same loop, we must check first if
        we won't overwrite unread bytes*/
    else{
        if( (write_index + len >= BUFFER_SIZE) && 
            (((write_index+len) % BUFFER_SIZE) >= read_index))
            return;

        for(i = 0 ; i<len ; i ++){
            gpacket_buffer[write_index] = buffer[i];

            write_index = ((write_index + 1) % BUFFER_SIZE);

            if(write_index == 0){
                around_flag = 1;
            }
        }  
    }

    return;
}

void uprintf(char *format, ...){
/*TODO: must increase cpu priority, to make sure this doesn't get interrupted*/
    va_list aptr; 

    va_start(aptr, format);

    uint8_t buffer[254];

    vsprintf(buffer,format, aptr);

    va_end(va);

    int buffer_len=strlen(buffer);

    append_string_usb(buffer, buffer_len);
    
    return;
}

