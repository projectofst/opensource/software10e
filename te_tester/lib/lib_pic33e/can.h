#ifndef _LIB_PIC33E_CAN_H
#define _LIB_PIC33E_CAN_H

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

/* A CAN message contains an id, a dlc and a array of 4 16 bit unsigned
 * integers
 *
 * The id is a 11 bit unsigned integer, separated into a 5 bit device id and a
 * 6 bit message id
 *
 * The first bit of the message id signals a reserved message. 0 means a
 * reserved id.
 */
typedef struct {
	union {
		struct {
			uint16_t dev_id:5; // Least significant
			uint16_t msg_id:6; // Most significant
		};
		uint16_t sid;
	};
	uint16_t dlc:4;
	uint16_t data[4];
} CANdata;


typedef struct _CANconfig {
	unsigned long baudrate;
	unsigned sync_jump_width:2;
	unsigned prop_seg:3;
	unsigned phase1_seg:3;
	unsigned phase2_seg:3;
	unsigned triple_sampling:1;
	unsigned fifo_size:3;
	unsigned fifo_start:5;
} CANconfig;

typedef enum {
	SUCCESS      = 0,
	ENULL        = -1,
	ECONFIG      = -2,
	ETIMING      = -3,
	ESENDFAILURE = -4,
} CANerror;


CANerror default_can_config(CANconfig *config);

void config_can1_filter();
CANerror config_can1(CANconfig *config);
CANerror send_can1(CANdata msg);
bool can1_rx_empty();
CANdata pop_can1();
bool filter_can1(unsigned int sid);

void config_can2_filter();
CANerror config_can2(CANconfig *config);
CANerror send_can2(CANdata msg);
bool can2_rx_empty();
CANdata pop_can2();
bool filter_can2(unsigned int sid);
#define RING_NEXT(INDEX, SIZE) (INDEX = (INDEX+1)%SIZE)

#endif /* ifndef CAN_H */
