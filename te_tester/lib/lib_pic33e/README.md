# Module libraries for dsPIC33E

## CAN

### Usage
Here X represents either 1 or 2 depending on the module.

Configure the CAN modules with config_canX.

Send messages to the CAN bus using send_canX.

Receive messages with pop_canX. __Always__ remember to call canX_rx_empty to
check if there are messages to be read from the buffer.

filter_canX can be used to filter by software messages received in the bus.

### Functions
__default_can_config__
```
CANerror default_can_config(CANconfig *config)

Creates a config structure with a baudrate of 1 MHz and 8TQ distributed in this fashion:
* propagation segment : 2 TQ
* phase 1 segment     : 3 TQ
* phase 2 segment     : 2 TQ
The sample point is at 75%, triple sampling is not enabled.

args    : config   - CANconfig struct to be filled with the default values
returns : CANerror - Error value
```


__config_can1__
```
CANerror config_can1(CANconfig *config)

Configure can 1 module. If no configuration is provided defaults to the configuration defined in default_can_config. Checks if bit timings are whithin allowed values.

args    : config     - CANconfig configuration
returns : CANerror   - Error code
```

__send_can1__
```
CANerror send_can1(CANdata msg)

Sends a CAN message.  Searches for available buffer, sends on the first found.
If none is found returns an ESENDFAILURE error

args    : msg      - CANdata CAN message
returns : CANerror - Error code
```

__can1_rx_empty__
```
bool can1_rx_empty()

Checks if there are any messages left to send in the CAN 1 circular ring
buffer 

args    : none
returns : bool - The function proposition
```

__pop_can1__
```
CANdata pop_can1()

Get a message from the circular ring buffer 
Always check if the buffer is empty with can1_rx_empty

args    : none
returns : CANdata - CAN message
```

__filter_can1__
```
bool __attribute__((weak)) filter_can1(unsigned int sid)

CAN1 filter. Redefine this function in your code to create software filters
for CAN messages 

args    : sid  - message standard id
returns : bool - proposition
```

___C1Interrupt__
```
void __attribute__ ((interrupt, auto_psv, shadow) ) _C1Interrupt( void )

CAN 1 interrupt 
Handles the receiving of messages
```

__config_can2__
```
CANerror config_can2(CANconfig *config) {

Configure can 2 module. If no configuration is provided defaults to the
configuration defined in default_can_config. Checks if bit timings are whithin
allowed values.

args    : config   - CANconfig config structure
returns : CANerror - Error code
```

__send_can2__
```
CANerror send_can2(CANdata msg)

Sends a CAN message. Searches for available buffer, sends on the first found.
If none is found returns an ESENDFAILURE error

args    : msg      - CANdata CAN message
returns : CANerror - Error code
```

__can2_rx_empty__
```
bool can2_rx_empty()

Checks if there are any messages left to send in the CAN 2 circular ring
buffer 

args    : none
returns : bool - The function proposition
```

```
CANdata pop_can2()

Get a message from the circular ring buffer 
Always check if the buffer is empty with can1_rx_empty

args    : none
returns : CANdata - CAN message

```

__filter_can2__
```
bool __attribute__((weak)) filter_can2(unsigned int sid)

CAN2 filter. Redefine this function in your code to create software filters
for CAN messages 

args    : sid  - message standard id
returns : bool - proposition
```

___C2Interrupt__
```
void __attribute__ ((interrupt, auto_psv, shadow)) _C2Interrupt( void )

CAN 2 interrupt 
Handles the receiving of messages
```

### CAN bit timings

In the CAN bus the propagation time of a bit is given by the sum of the
duration of 4 phases:

```
|--------------|---------------------|-----------------|-----------------|
| Sync Segment | Propagation Segment | Phase 1 Segment | Phase 2 Segment |
|--------------|---------------------|-----------------|-----------------|
												      / \
                                                       |
                                                       |
                                                       Sample point 
|------------------------------------------------------------------------|
```
The basic unit in which we measure the length of each phase is called the
time quanta (TQ). 

The propagation of a bit as at least 8 TQ and at most 25 TQ.

The sync segment is fixed at 1TQ.

The propagation segment and the phase 1 segment can be configured between
1-8TQ.

The phase 2 segment can be configured between 2-8 TQ.

The sample point is the point in time in each we expect the bit to be
sampled. The ECAN module allows us to choose between sampling once and
thrice for increased accuracy.

It is recommended to keep the sample point between 80% and 90% of the total
bit length.

The syncronization jump width is how much we allow the bit time to be
 shortened or lengthened to keep the bus synchronous.
 It is recommend that it as big as long as it is smaller than phase 2.

#### Bibliography
* http://ww1.microchip.com/downloads/en/AppNotes/00754.pdf
* https://vector.com/portal/medien/cmc/application_notes/AN-AND-1-106_Basic_CAN_Bit_Timing.pdf

## Timing
FCY and delay definitions.

## PPS
PPS config functions.

## USB
To use the USB module all you have to do is include usb_lib.h and use the following functions:
* **USBInit():** Configures everything USB Related for you, except the clock configurations, which is being done in timing.h
* **USBTasks():** Sends and receives USB Packets. Call it in your main's while(1).
* **receiveHandler(char * string):** Callback function for USB receive. 'string' is the received string. 
Since this function is declared as weak in usb_lib.c, if you wish to do something with received packets 
you must redeclare this function in your project.
                            
* **uprintf(char *format, ...):** Prints formatted data to USB. Works just like printf.

#### How to connect to serial port

**Alternatives:**
* [CoolTerm](http://freeware.the-meiers.org), tested on Mac and Linux.

## ADC



