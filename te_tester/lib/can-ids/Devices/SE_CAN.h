#ifndef __SE_CAN_H__
#define __SE_CAN_H__

#include "can-ids/CAN_IDs.h"

#define MSG_ID_DASH_STEER   33

typedef struct{
    unsigned int time_stamp;
    unsigned int steer_encoder_sig;
} STEER_MSG_SIG;

void parse_can_SE(CANdata message, STEER_MSG_SIG *steer);
#endif
