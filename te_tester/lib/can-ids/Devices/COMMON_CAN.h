#ifndef __COMMON_CAN_H__
#define __COMMON_CAN_H__

#include <stdbool.h>
#include <stdint.h>
#include "can-ids/CAN_IDs.h"

typedef enum{
    RTD_STEP_DASH_BUTTON = 0b0000,
    RTD_STEP_TE_OK       = 0b0001,
    RTD_STEP_TE_NOK      = 0b1001,
    RTD_STEP_INV_OK      = 0b0010,
    RTD_STEP_INV_NOK     = 0b1010,
    RTD_STEP_DCU         = 0b0011,
    RTD_STEP_INV_GO      = 0b0100,
    RTD_STEP_DASH_LED    = 0b0101,

}RTD_ON;

typedef enum{
    RTD_DASH_OFF      = 0,
    RTD_INV_CONFIRM   = 1,
	RTD_INV_OFF
}RTD_OFF;

/*enum RTD_ERROR{
};*/

typedef enum{
    LOG_RESET       = 0,
    LOG_TRAP_OSC    = 1,
    LOG_TRAP_MATH   = 2,
    LOG_TRAP_ADDR   = 3,
    LOG_TRAP_STACK  = 4,
    LOG_ERROR_PARSE = 5,
}LOG_TYPE;

typedef enum{
    TS_DASH_BUTTON  = 0,
    TS_MASTER_ON    = 1,
    TS_MASTER_OFF   = 2,
}TS_STEP;

typedef struct{
    RTD_ON step;
//    RTD_ERROR error;
} COMMON_MSG_RTD_ON;

typedef struct{
    RTD_OFF step;

}COMMON_MSG_RTD_OFF;

typedef struct{
    TS_STEP step;

}COMMON_MSG_TS;

typedef struct{
    unsigned int type;
    unsigned int parse_error_id; /*find out this shit*/
} COMMON_LOG_MSG;


void parse_can_common_RTD(CANdata message, COMMON_MSG_RTD_ON *RTD_ON);
void parse_can_common_LOG(CANdata message, COMMON_LOG_MSG *LOG);
void parse_can_common_TS(CANdata message, COMMON_MSG_TS *TS);
void parse_can_common_RTD_OFF(CANdata message, COMMON_MSG_RTD_OFF *RTD_OFF);
#endif
