#!/usr/bin/env python3

import sys, os
from pprint import pprint


def find_index(line, char):
    for i, c in enumerate(line):
        if c == char:
            return i
    return len(line)


types = {
    "MSG": [],
    "DEVICE": [],
    "CMD": [],
    "OTHER": [],
}

root = sys.argv[1]
devices = os.path.join(sys.argv[1], "Devices")
can_ids = os.path.join(sys.argv[1], "CAN_IDs.h")
dirs = os.listdir(devices)

can_devices = [
    os.path.join(devices, dir) for dir in dirs if os.path.splitext(dir)[1] == ".h"
]

id_files = []
for device in can_devices:
    with open(device) as file:
        id_files.append(file.read())
with open(can_ids) as file:
    id_files.append(file.read())

lines = []
for device in id_files:
    for line in device.split("\n"):
        if "#define" in line:
            line = line.split()
            if len(line) < 3:
                continue

            id = line[1].split("_")
            if id[0] in types.keys():
                types[id[0]].append(line[1:3])
            else:
                types["OTHER"].append(line[1:3])

keys = {
    "MSG": lambda x: x[0],
    "DEVICE": lambda x: int(x[1]),
    "CMD": lambda x: x[0],
    "OTHER": lambda x: x[0],
}
for type in types.keys():
    print(f"# {type}")
    print("|CAN_ID|Value|\n|------|-----|")
    for line in sorted(types[type], key=keys[type]):
        try:
            print(
                f"|[{line[0]}](https://gitlab.com/projectofst/can-ids/wikis/{line[0]}.md)|{hex(int(line[1]))}|"
            )
        except Exception as e:
            pass
