/**********************************************************************
 *	 CAN IDs for FST 08e
 *
 *	 log:
 *	 ______________________________________________________________
 *	 2017 - Projecto FST Lisboa
 **********************************************************************/

#ifndef __CAN_IDS_H__
#define __CAN_IDS_H__

#include <stdint.h>

typedef struct {
	union {
        struct {
			uint16_t dev_id:5; // Least significant
			// First bit of msg_id determines if msg is reserved or not.
			// 0 == reserved (higher priority) (0-31 decimal)
			uint16_t msg_id:6; // Most significant
		};
		uint16_t sid;
	};
	uint16_t dlc:4;
	uint16_t data[4];
} CANdata;

#define CAN_GET_MSG_ID(sid) ((sid & 0b0000011111100000) >> 5)
#define CAN_GET_DEV_ID(sid) (sid & 0b0000000000011111)

/* DEVICE IDs (0-31) */
#define DEVICE_ID_DCU				8
#define DEVICE_ID_TE				9
#define DEVICE_ID_DASH				10
#define DEVICE_ID_LOGGER			11
#define DEVICE_ID_STEERING_WHEEL	12
#define DEVICE_ID_ARM				13
#define DEVICE_ID_MASTER			14
#define DEVICE_ID_IIB				15
#define DEVICE_ID_TELEMETRY			16
#define DEVICE_ID_SUPOT_FRONT		17
#define DEVICE_ID_SUPOT_REAR		18
#define DEVICE_ID_AHRS				19
#define DEVICE_ID_IMU				20
#define DEVICE_ID_GPS				21
#define DEVICE_ID_WATER_TEMP		22
#define DEVICE_ID_TIRETEMP          23
#define DEVICE_ID_STEER				24
#define DEVICE_ID_INTERFACE			31


/* RESERVED MESSAGE IDS (0-31)
 * Place your unique high priority message IDs here */
// Digital Control Unit
// Torque Encoder
#define MSG_ID_TE_MAIN				10

//RTD ACTIVATION SEQUENCE
//pls fix nomenclatura 
#define CMD_ID_COMMON_RTD_ON		28
#define CMD_ID_COMMON_RTD_OFF       29  
#define CMD_ID_COMMON_TS 			30	




// LOG (RESET/TRAP/ERROR) (common to all devices)
#define MSG_ID_COMMON_LOG           31


#endif
