#include "io.h"

void set_input_output()
{

    /** Outputs */
    EF_B = 0;
    EF_A = 0;
    BE2_INV1 = 0;
    BE1_INV1 = 0;
    BE2_INV2 = 0;
    BE1_INV2 = 0;
    BE2_INV3 = 0;
    BE1_INV3 = 0;
    BE2_INV4 = 0;
    BE1_INV4 = 0;
    SDC_INV1 = 0;
    SDC_INV2 = 0;
    FAN_CONTROL = 0;
    LED1 = 0;
    LED2 = 0;

    /** Inputs */
    TEMP = 1;
    TEMP1 = 1;
    CAR_sdc2 = 1;
    CAR_sdc1 = 1;
    TSAL_INV = 1;

    TRISBbits.TRISB1 = 0;

    /*GPIO you decide*/
    GPIO1 = 0;
    GPIO2 = 0;
    /// GPIO3 = x;
    /// GPIO4 = x;
    //
    return;
}

void init_pins()
{

    EF_B_WRITE = 0;
    EF_A_WRITE = 0;

    BE2_INV1_WRITE = 0;
    BE1_INV1_WRITE = 0;
    BE2_INV2_WRITE = 0;
    BE1_INV2_WRITE = 0;
    BE2_INV3_WRITE = 0;
    BE1_INV3_WRITE = 0;
    BE2_INV4_WRITE = 0;
    BE1_INV4_WRITE = 0;

    SDC_INV1_WRITE = 0;
    SDC_INV2_WRITE = 0;

    LED1_WRITE = 1; /** Green led [LED2] */
    LED2_WRITE = 1; /** Yellow led [LED3] */

    FAN_CONTROL_WRITE = 0;

    return;
}

void io_config()
{

    set_input_output();
    init_pins();

    return;
}

void switch_relays()
{

    SDC_INV1_WRITE ^= 1;
    SDC_INV2_WRITE ^= 1;

    return;
}
