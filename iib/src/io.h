#ifndef __IO_H__
#define __IO_H__

#include "iib.h"
#include <xc.h>

/** OUTPUTS PINS */
#define EF_B TRISFbits.TRISF1
#define EF_A TRISEbits.TRISE2

#define BE2_INV1 TRISBbits.TRISB2
#define BE1_INV1 TRISBbits.TRISB3
#define BE2_INV2 TRISBbits.TRISB4
#define BE1_INV2 TRISBbits.TRISB5
#define BE2_INV3 TRISDbits.TRISD6
#define BE1_INV3 TRISDbits.TRISD5
#define BE2_INV4 TRISEbits.TRISE3
#define BE1_INV4 TRISFbits.TRISF0

#define SDC_INV1 TRISEbits.TRISE0
#define SDC_INV2 TRISEbits.TRISE1
#define FAN_CONTROL TRISCbits.TRISC14

#define LED1 TRISBbits.TRISB6
#define LED2 TRISBbits.TRISB7

/** INPUT PINS */
#define TEMP TRISDbits.TRISD7
#define TEMP1 TRISBbits.TRISB13
#define CAR_sdc2 TRISEbits.TRISE4
#define CAR_sdc1 TRISEbits.TRISE5

/** GPIO */
#define GPIO1 TRISDbits.TRISD1
#define GPIO2 TRISDbits.TRISD2
#define GPIO3 TRISDbits.TRISD3
#define GPIO4 TRISDbits.TRISD4

/*TSAL*/
#define TSAL_INV GPIO3

/** For read our write */

/// Write
#define EF_B_WRITE LATFbits.LATF1
#define EF_A_WRITE LATEbits.LATE2
#define BE2_INV1_WRITE LATBbits.LATB2
#define BE1_INV1_WRITE LATBbits.LATB3
#define BE2_INV2_WRITE LATBbits.LATB4
#define BE1_INV2_WRITE LATBbits.LATB5
#define BE2_INV3_WRITE LATDbits.LATD6
#define BE1_INV3_WRITE LATDbits.LATD5
#define BE2_INV4_WRITE LATEbits.LATE3
#define BE1_INV4_WRITE LATFbits.LATF0
#define SDC_INV1_WRITE LATEbits.LATE0
#define SDC_INV2_WRITE LATEbits.LATE1
#define FAN_CONTROL_WRITE LATCbits.LATC14
#define LED1_WRITE LATBbits.LATB6
#define LED2_WRITE LATBbits.LATB7

/// Read
#define TEMP_READ PORTDbits.RD7
#define TEMP1_READ PORTBbits.RB13
#define sdc2_READ PORTEbits.RE4
#define sdc1_READ PORTEbits.RE5

#define TSAL_INV_READ PORTDbits.RD3

void set_input_output();
void init_pins();
void switch_relays();
void io_config();

#endif
