MODULE_C_SOURCES:=acceleration.c
MODULE_C_SOURCES+=car.c
MODULE_C_SOURCES+=differential.c
MODULE_C_SOURCES+=iib.c
MODULE_C_SOURCES+=io.c
MODULE_C_SOURCES+=main.c
MODULE_C_SOURCES+=safety.c
MODULE_C_SOURCES+=update_can.c

PWD:=src/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
