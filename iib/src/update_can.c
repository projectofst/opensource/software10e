#include "update_can.h"

can_t global_can;

can_t* get_can(void)
{
    return &global_can;
}

void update_iib_motor(uint8_t mux)
{
    can_t* can = get_can();

    can->iib.iib_motor.temp_motor[mux] = (iib.inv[mux].temp_motor / 10) + 1;
    can->iib.iib_motor.motor_speed[mux] = iib.inv[mux].actual_speed;
    can->iib.iib_motor.motor_current[mux] = iib.inv[mux].current;
    can->iib.iib_motor.motor_torque[mux] = iib.inv[mux].torque;

    // printf("can->iib_motor.motor_torque[mux]: %d\n", can->iib.iib_motor.motor_current[mux]);

    return;
}

void update_iib_inv(uint8_t mux)
{
    can_t* can = get_can();

    can->iib.iib_inv.inv_active[mux] = iib.inv[mux].inverter_active;
    can->iib.iib_inv.BE_1[mux] = iib.inv[mux].BE_1;
    can->iib.iib_inv.BE_2[mux] = iib.inv[mux].BE_2;
    can->iib.iib_inv.inv_derating_on[mux] = iib.iib_safety.temp_status[mux].inv_derating_on;
    can->iib.iib_inv.inv_max_passed[mux] = iib.iib_safety.temp_status[mux].inv_max_passed;
    can->iib.iib_inv.igbt_derating_on[mux] = iib.iib_safety.temp_status[mux].igbt_derating_on;
    can->iib.iib_inv.igbt_max_passed[mux] = iib.iib_safety.temp_status[mux].igbt_max_passed;
    can->iib.iib_inv.i2t_overload_active[mux] = iib.iib_safety.temp_status[mux].i2t_overload_active;
    can->iib.iib_inv.i2t_cooldown_on[mux] = iib.iib_safety.temp_status[mux].i2t_cooldown_on;
    can->iib.iib_inv.inv_temp_igbt[mux] = iib.inv[mux].temp_IGBT;
    can->iib.iib_inv.inv_temp[mux] = iib.inv[mux].temp_inverter;
    can->iib.iib_inv.inv_error[mux] = iib.inv[mux].error_info;

    return;
}

void update_iib_limits(uint8_t mux)
{
    can_t* can = get_can();

    can->iib.iib_limits.max_sfty_power[mux] = (uint8_t)(iib.iib_safety.inv_lim[mux].max_sfty_pwr / 1000.0);
    can->iib.iib_limits.max_op_power[mux] = *(iib.iib_safety.inv_lim[mux].max_op_pwr);
    can->iib.iib_limits.max_sfty_torque[mux] = iib.iib_safety.inv_lim[mux].max_sfty_t;
    can->iib.iib_limits.max_op_torque[mux] = *(iib.iib_safety.inv_lim[mux].max_op_t);

    return;
}

void update_iib_car(void)
{
    can_t* can = get_can();

    can->iib.iib_car.car_te_ok = iib.car.status.te_ok;
    can->iib.iib_car.car_ext_exceed = iib.car.status.ext_wtd_exceeded;
    can->iib.iib_car.car_etas_stupid = iib.car.status.etas_stupid;
    can->iib.iib_car.car_arm_msg = iib.car.status.arm_recv_msg;
    can->iib.iib_car.car_sdc1 = iib.car.status.sdc1;
    can->iib.iib_car.car_sdc2 = iib.car.status.sdc2;
    can->iib.iib_car.car_rtd = iib.car.status.car_rtd;
    can->iib.iib_car.inv_hv = iib.car.status.inv_hv;
    can->iib.iib_car.car_tsal = iib.car.status.tsal_status;
    can->iib.iib_car.te_perc = iib.car.apps;
    can->iib.iib_car.iib_input_mode = *(iib.input_mode);
    can->iib.iib_car.bms_voltage = iib.car.bms_voltage;

    return;
}

void update_iib_info(void)
{
    can_t* can = get_can();

    can->iib.iib_info.iib_air_tmp = iib.air_temp;
    can->iib.iib_info.iib_disch_tmp = iib.discharge_temp;
    can->iib.iib_info.max_sfty_rpm = *(iib.iib_safety.max_rpm);
    can->iib.iib_info.iib_fan_on = iib.fan_on;
    can->iib.iib_info.cua_power = iib.cu_power_a;
    can->iib.iib_info.cub_power = iib.cu_power_b;
    can->iib.iib_info.inv_rtd = iib.inv_rtd;
    can->iib.iib_info.inv_on = iib.inv_on;
    can->iib.iib_info.regen_on = *(iib.regen_on);
    can->iib.iib_info.iib_debug_mode = *(iib.debug_mode);
    can->iib.iib_info.iib_error_latch = iib.iib_safety.error_latch[0] | iib.iib_safety.error_latch[1] << 1 | iib.iib_safety.error_latch[2] << 2 | iib.iib_safety.error_latch[3] << 3;
    can->iib.iib_info.pl_on = *(iib.pl_on);

    return;
}

void update_iib_debug1_info(uint8_t mux)
{
    can_t* can = get_can();

    can->iib.iib_debug1_info.debug1_arm_msg = iib.car.status.arm_recv_msg;
    can->iib.iib_debug1_info.debug1_node_address[mux] = iib.inv[mux].node_address;
    can->iib.iib_debug1_info.debug1_te_realive = iib.car.te_alive_again;
    can->iib.iib_debug1_info.debug1_max_i2t_current[mux] = iib.iib_safety.max_i2t_current[mux];
    can->iib.iib_debug1_info.debug1_i2t_sat_time[mux] = iib.iib_safety.i2t_saturation_time[mux];
    can->iib.iib_debug1_info.debug1_cldwn_on_time[mux] = iib.iib_safety.cool_down_on_time[mux];

    return;
}

void update_iib_debug2_info(uint8_t mux)
{
    can_t* can = get_can();

    can->iib.iib_debug2_info.debug2_inv_sys_rdy[mux] = iib.inv[mux].status.sys_ready;
    can->iib.iib_debug2_info.debug2_inv_err[mux] = iib.inv[mux].status.err;
    can->iib.iib_debug2_info.debug2_inv_warn[mux] = iib.inv[mux].status.warn;
    can->iib.iib_debug2_info.debug2_inv_quit_dc_on[mux] = iib.inv[mux].status.quit_dc_on;
    can->iib.iib_debug2_info.debug2_inv_dc_on[mux] = iib.inv[mux].status.dc_on;
    can->iib.iib_debug2_info.debug2_inv_quit_inv_on[mux] = iib.inv[mux].status.quit_inverter_on;
    can->iib.iib_debug2_info.debug2_inv_on[mux] = iib.inv[mux].status.inverter_on;
    can->iib.iib_debug2_info.debug2_derating[mux] = iib.inv[mux].status.derating;
    can->iib.iib_debug2_info.debug2_msg1_id[mux] = iib.inv[mux].message1_id;
    can->iib.iib_debug2_info.debug2_msg2_id[mux] = iib.inv[mux].message2_id;
    can->iib.iib_debug2_info.debug2_redid[mux] = iib.inv[mux].reference_id;

    return;
}

void update_iib_regen_limits(uint8_t mux)
{
    can_t* can = get_can();

    can->iib.iib_regen_limits.regen_max_sfty_torque[mux] = iib.iib_safety.inv_lim[mux].max_sfty_reg_t;
    can->iib.iib_regen_limits.regen_max_op_torque[mux] = *(iib.iib_safety.inv_lim[mux].max_op_reg_t);
    can->iib.iib_regen_limits.regen_max_sfty_pwr[mux] = *(iib.iib_safety.inv_lim[mux].max_op_reg_pwr);

    return;
}

void update_iib_amk_values1(uint8_t mux)
{
    can_t* can = get_can();

    can->iib.iib_amk_values_1.amk_sys_rdy[mux] = iib.inv[mux].status.sys_ready;
    can->iib.iib_amk_values_1.amk_err[mux] = iib.inv[mux].status.err;
    can->iib.iib_amk_values_1.amk_warn[mux] = iib.inv[mux].status.warn;
    can->iib.iib_amk_values_1.amk_quit_dc_on[mux] = iib.inv[mux].status.quit_dc_on;
    can->iib.iib_amk_values_1.amk_dc_on[mux] = iib.inv[mux].status.dc_on;
    can->iib.iib_amk_values_1.amk_quit_inv_on[mux] = iib.inv[mux].status.quit_inverter_on;
    can->iib.iib_amk_values_1.amk_inv_on[mux] = iib.inv[mux].status.inverter_on;
    can->iib.iib_amk_values_1.amk_derating[mux] = iib.inv[mux].status.derating;
    can->iib.iib_amk_values_1.amk_actual_speed[mux] = iib.inv[mux].actual_speed;
    can->iib.iib_amk_values_1.amk_torque_c[mux] = iib.inv[mux].torque_current;
    can->iib.iib_amk_values_1.amk_magn_c[mux] = iib.inv[mux].magn_curr;

    return;
}

void update_iib_amk_values2(uint8_t mux)
{
    can_t* can = get_can();

    can->iib.iib_amk_values_2.amk_temp_motor[mux] = iib.inv[mux].temp_motor;
    can->iib.iib_amk_values_2.amk_temp_inverter[mux] = iib.inv[mux].temp_inverter;
    can->iib.iib_amk_values_2.amk_error_info[mux] = iib.inv[mux].error_info;
    can->iib.iib_amk_values_2.amk_temp_igbt[mux] = iib.inv[mux].temp_IGBT;

    return;
}

void update_iib_debug_amk(uint8_t mux)
{
    can_t* can = get_can();

    can->iib.iib_debug_amk.debug_b_inv_on[mux] = iib.inv[mux].setp.ctrl.b_inverter_on;
    can->iib.iib_debug_amk.debug_b_dc_on[mux] = iib.inv[mux].setp.ctrl.b_dc_on;
    can->iib.iib_debug_amk.debug_b_enable[mux] = iib.inv[mux].setp.ctrl.b_enable;
    can->iib.iib_debug_amk.debug_b_err_rst[mux] = iib.inv[mux].setp.ctrl.b_error_reset;
    can->iib.iib_debug_amk.debug_dsr_rpm[mux] = iib.inv[mux].setp.rpm;
    can->iib.iib_debug_amk.debug_dsr_t_n[mux] = iib.inv[mux].setp.t_n;
    can->iib.iib_debug_amk.debug_dsr_t_p[mux] = iib.inv[mux].setp.t_p;

    return;
}

void update_iib_status(void)
{
    can_t* can = get_can();

    can->iib.iib_status.iib_status_sys_rdy = iib.inv[2].status.sys_ready;
    can->iib.iib_status.iib_status_err = iib.inv[2].status.err;
    can->iib.iib_status.iib_status_warn = iib.inv[2].status.warn;
    can->iib.iib_status.iib_status_quit_dc_on = iib.inv[2].status.quit_dc_on;
    can->iib.iib_status.iib_status_dc_on = iib.inv[2].status.dc_on;
    can->iib.iib_status.iib_status_quit_inv_on = iib.inv[2].status.quit_inverter_on;
    can->iib.iib_status.iib_status_inv_on = iib.inv[2].status.inverter_on;
    can->iib.iib_status.iib_status_derating_on = iib.inv[2].status.derating;

    return;
}

void update_iib_can(uint8_t max)
{
    uint8_t i;

    update_iib_car();
    update_iib_info();
    update_iib_status();

    for (i = 0; i < max; i++) {
        update_iib_motor(i);
        update_iib_inv(i);
        update_iib_limits(i);
        update_iib_debug1_info(i);
        update_iib_debug2_info(i);
        update_iib_regen_limits(i);
        update_iib_amk_values1(i);
        update_iib_amk_values2(i);
        update_iib_debug_amk(i);
    }

    return;
}
