#ifndef __ACCEL_H__
#define __ACCEL_H__

#include "iib.h"
#include "safety.h"
#include <stdint.h>

#define RAD(RPM) ((RPM) * 2 * PI /60)

uint16_t get_accel_curve(int i);
int16_t get_brake_curve(int i);

#endif
