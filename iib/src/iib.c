/*! \file
 *  \brief  IIB functions
 */

#include "iib.h"
#include "car.h"
#include "update_can.h"

/// Extern variable
extern uint16_t line;

iib_t iib;

/**
 *   Name: convert_torque_unit
 *   Description: converts torque values to 0.1% of nominal torque, unit specified by datasheet to send
 *   Input's: IIb structure containing the info about all 4 inverters torque setp
 *   Output: void
 */
void convert_unit(void)
{

    int i = 0;
    for (i = 0; i < 4; i++) {
        /** rpm */
        iib.inv[i].setp.rpm = iib.dsr[i].rpm;

        /** positive torque */
        iib.inv[i].setp.t_p = iib.dsr[i].t_p / NOMINAL_TORQUE;

        /** negative torque */
        iib.inv[i].setp.t_n = ((int16_t)iib.dsr[i].t_n) / NOMINAL_TORQUE;
    }

    return;
}

/**
 *   Name: send_to_amk !rever toda esta funcao
 *   Description: final stage before sending the setpoint message, prepares the 4 messages one for each motor to be sent
 *   Input's: IIB structure containing 4 inverters, with all the id's and the substructure setp were the message
 *            parameters are found,
 *   Output: void
 */
void send_to_amk(void)
{

    can_t* can = get_can();
    uint16_t i = 0;
    CANdata inverters_ref[4];

    convert_unit(); /** Before sending, the torques are converted to the correct unit */

    /** References to inverters */
    for (i = 0; i < 4; i++) {

        inverters_ref[i].sid = iib.inv[i].reference_id;

        inverters_ref[i].dlc = 8;

        inverters_ref[i].data[0] = iib.inv[i].setp.ctrl.word;
        /** Target speed for the motor */
        inverters_ref[i].data[1] = iib.inv[i].setp.rpm;
        /** Positive Torque for motor */
        inverters_ref[i].data[2] = iib.inv[i].setp.t_p;
        /** Negative Torque for motor */
        inverters_ref[i].data[3] = iib.inv[i].setp.t_n;
        // Update can struct
        update_iib_debug_amk(i);
    }

    /// We if are about to turn on, see wich inverter's are active and enable the BE's
    /// Do this before sending to inverters

    for (i = 0; i < 4; i++) { /** Send only to the inverters that are active */

        /* if (iib.debug_mode)
            send_can2(encode_iib_debug_amk(can->iib.iib_debug_amk, i)); */

        // if (iib.inv[i].inverter_active == 1) {// && *(iib.input_mode) != ETAS_MODE) {
        send_can2(inverters_ref[i]);
        send_can1(inverters_ref[i]); /** Send to inverter can line */
        //}

        /// Turning OFF
        if (iib.turning_off == 1) {

            iib.turning_off = 0; /** Turn off process over */
            iib.inv_on = 0; /** Inverters are now off */
        }

        /// Turning ON
        if (iib.turning_on == 1) {
            iib.inv_on = 1; /** Acknoledge that inverters are on */
            iib.turning_on = 0; /** Stop turning on */
        }

        update_iib_info();
    }
    return;
}

void set_idle(int inverter)
{

    if (inverter != 0 && inverter != 1 && inverter != 2 && inverter != 3) {
        return;
    }

    iib.dsr[inverter].t_p = 0;
    iib.dsr[inverter].t_n = 0;
    iib.dsr[inverter].rpm = 0;

    return;
}

/**
 *   Name: set_idle
 *   Description: sets all setp to send to the motor's at 0
 *   Input's: iib_t structure containing the 4 inverters where we can update the setp for each inverter
 *   Output: void
 */
void set_idle_all()
{

    int i = 0;

    for (i = 0; i < 4; i++) {

        set_idle(i);
    }

    return;
}

void set_turn_off(int inverter)
{

    if (inverter != 0 && inverter != 1 && inverter != 2 && inverter != 3) {
        return;
    }

    set_idle(inverter);

    iib.inv[inverter].setp.ctrl.b_inverter_on = 0;
    iib.inv[inverter].setp.ctrl.b_dc_on = 1;
    iib.inv[inverter].setp.ctrl.b_enable = 0;
    iib.inv[inverter].setp.ctrl.b_error_reset = 0;

    return;
}

/**
 *   Name: set_turn_off
 *   Description: prepares for the turn off sequence
 *   Input's: iib_t structure containing the inverter setp and the respective control word
 *   Output: void
 */
void set_turn_off_all(void)
{

    int i = 0;

    for (i = 0; i < 4; i++) {

        set_turn_off(i);
    }

    return;
}

/**
 *   Name: transform_id_to_inverter
 *   Description: Based upon a msg id, decodes from what inverter it is
 *   Input's: the CAN message containing the id, iib_t structure to compare with all id's
 *   Output: corresponding inverter number (1,2,3,4)
 */
int transform_id_to_inverter(CANdata msg)
{

    int i = 0;

    for (i = 0; i < 4; i++) {

        /** check if id is any of the 2 messages id's from a specific inverter */
        if (msg.sid == iib.inv[i].reference_id || msg.sid == iib.inv[i].message1_id || msg.sid == iib.inv[i].message2_id) {
            return i;
        }
    }

    return -1; /** message doesn't match any knowned id(NOT GOOD) */
}

/**
 *   Name: calculate_torque
 *   Description: Calculates individual motor torque's
 *   Input's: iib_t structure, inverter number
 *   Output: void
 */
void calculate_torque(int inverter)
{
    float iq = 0;

    /// Se amk datasheet for this calculation
    iq = (iib.inv[inverter].torque_current * CONVERTER_PEAK_CURRENT) / 16384.0;

    /// Compute motor torque
    iib.inv[inverter].torque = (int16_t)(iq * MOTOR_KT);
    //("%d: %d / %f \n",inverter,iib.inv[inverter].torque,iq * MOTOR_KT);
    update_iib_motor(inverter);

    return;
}

/**
 *   Name: calculate_current
 *   Description: Calculates individual motor current
 *   Input's: iib_t structure, inverter number
 *   Output: void
 */
void calculate_current(int inverter)
{
    uint16_t _id, _iq;
    int16_t id, iq;

    /// Computes id and iq
    _id = (iib.inv[inverter].magn_curr * CONVERTER_PEAK_CURRENT) / 16384;

    _iq = (iib.inv[inverter].torque_current * CONVERTER_PEAK_CURRENT) / 16384;

    id = *((int16_t*)&_id);
    iq = *((int16_t*)&_iq);

    // printf("id: %d, iq: %d\n", id, iq);

    /// Module of vectorial sum
    iib.inv[inverter].current = sqrt(id * id + iq * iq); /** this is the current module in [A] */

    // printf("inv current %d\n", iib.inv[inverter].current);

    update_iib_motor(inverter);

    return;
}

/**
 *   Name: update_inverter_actual_values_1
 *   Description: Updates information that came from message 1
 *   Input's: the number of the inverter we want to update the info, the information(data), iib_t structure
 *   Output: void
 */
void update_inverter_actual_values_1(int inverter_num, uint16_t message_data[4])
{
    if (inverter_num != 0 && inverter_num != 1 && inverter_num != 2 && inverter_num != 3) {
        return;
    }

    parse_amk_actual_values_1(message_data, &iib.inv[inverter_num]);

    // Update CAN structure

    update_iib_debug2_info(inverter_num);
    update_iib_amk_values1(inverter_num);
    update_iib_motor(inverter_num);
    update_iib_status();

    //

    // see_this
    // iib.inv_on = iib.inv[MOTOR_FL].status.inverter_on || iib.inv[MOTOR_FR].status.inverter_on || iib.inv[MOTOR_RL].status.inverter_on || iib.inv[MOTOR_RR].status.inverter_on;

    /// calculate torque and current for that inverter
    calculate_torque(inverter_num);
    calculate_current(inverter_num);
    //
    return;
}

/**
 *   Name: update_inverter_actual_values_2
 *   Description: Updates information that came from message 2
 *   Input's: the number of the inverter we want to update the info, the information(data), iib_t structure
 *   Output: void
 */
void update_inverter_actual_values_2(int inverter_num, uint16_t message_data[4])
{

    if (inverter_num != 0 && inverter_num != 1 && inverter_num != 2 && inverter_num != 3) {
        line = 37;
        return;
    }

    parse_amk_actual_values_2(message_data, &iib.inv[inverter_num]);

    // Update CAN structure
    update_iib_motor(inverter_num);
    update_iib_inv(inverter_num);
    update_iib_amk_values2(inverter_num);

    line = 13;

    return;
}

/**
 *   Name: process_amk_message   !rever
 *   Description: discovers which message we recieved, from which inverter it was, and updates the information
 *   Input's: The received CAN message(msg), iib_t structure for updating info
 *   Output: void
 */
void process_amk_message(CANdata msg)
{
    can_t* can = get_can();

    int inverter = 0; /** variable to identify the inverter */

    inverter = transform_id_to_inverter(msg);

    /** we discover the inverter number */
    if (inverter == -1) {
        return;
    }
    /* Check if it is message 1 and what inverter sent it
     * AMK Actual values 1 - consult AMK manual
     */

    else if (msg.sid == iib.inv[inverter].message1_id) {

        update_inverter_actual_values_1(inverter, msg.data); /* update message 1 info */

        send_can2(encode_iib_amk_values_1(can->iib.iib_amk_values_1, inverter));

    }

    /// Check if it is message 2 and what inverter send it
    else if (msg.sid == iib.inv[inverter].message2_id) {

        update_inverter_actual_values_2(inverter, msg.data); /** update message 2 info */

        send_can2(encode_iib_amk_values_2(can->iib.iib_amk_values_2, inverter));
    }

    /// Clear wtd timer for the inverter that sent the message
    // switch(inverter){

    //     case 0: wtd_FL = 0 ;break;    /** FL */
    //     case 1: wtd_FR = 0 ;break;    /** FR */
    //     case 2: wtd_RL = 0 ;break;    /** RL */
    //     case 3: wtd_RR = 0 ;break;    /** RR */
    // }

    return;
}

void set_turn_on(int inverter)
{

    if (inverter != 0 && inverter != 1 && inverter != 2 && inverter != 3) {
        return;
    }

    iib.inv[inverter].setp.ctrl.b_inverter_on = 1;
    iib.inv[inverter].setp.ctrl.b_dc_on = 1;
    iib.inv[inverter].setp.ctrl.b_enable = 1;
    iib.inv[inverter].setp.ctrl.b_error_reset = 0;

    return;
}

/**
 *   Name: set_turn_on
 *   Description: prepares for the turn on sequence
 *   Input's: iib_t structure containing the control word
 *   Output: void
 */
void set_turn_on_all()
{

    int i = 0;

    for (i = 0; i < 4; i++) {

        if (iib.inv[i].inverter_active == 1) { /** Verify if inverter is active */

            /** Controll word for turn on */
            set_turn_on(i);
        }
    }

    return;
}

/**
 *   Name: set_reset
 *   Description: resets 1 inverter
 *   Input's: iib_t structure containing the inverter setp and the respective control word
 *   Output: void
 */
void set_reset(int inverter)
{

    /*if (inverter != 0 && inverter != 1 && inverter != 2 && inverter != 3) {
        return;
    }*/

    /// Set idle
    iib.dsr[inverter].t_p = 0;
    iib.dsr[inverter].t_n = 0;
    iib.dsr[inverter].rpm = 0;

    /** Controll word for reset */
    iib.inv[inverter].setp.ctrl.b_inverter_on = 0;
    iib.inv[inverter].setp.ctrl.b_dc_on = 0;
    iib.inv[inverter].setp.ctrl.b_enable = 0;
    iib.inv[inverter].setp.ctrl.b_error_reset = 1;

    return;
}

/**
 *   Name: set_reset_all
 *   Description: resets all inverters
 *   Input's: iib_t structure containing the inverter setp and the respective control word
 *   Output: void
 */
void set_reset_all()
{

    int i = 0;

    for (i = 0; i < 4; i++) {

        set_reset(i);
    }

    return;
}
