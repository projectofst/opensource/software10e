#ifndef __IIB_H__
#define __IIB_H__

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "can-ids-v2/can_cfg.h"
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "lib_pic33e/can.h"
#include "shared/amk/amk.h"

#include "io.h"

#define N_MOTOR 4

/// Base adresses for the 3 amk messages (see AMK Datasheet!)
#define BASE_ADRESS_ACTUAL_V1 0x282
#define BASE_ADRESS_ACTUAL_V2 0x284
#define BASE_ADRESS_SETPOINTS 0x183

/// Motor number
#define MOTOR_FL 0
#define MOTOR_FR 1
#define MOTOR_RL 2
#define MOTOR_RR 3

/// Node adress for each inverter
#define NODE_FL 0x0
#define NODE_FR 0x1
#define NODE_RL 0x4
#define NODE_RR 0x5

/// AMK motor controll parameters
#define NOMINAL_TORQUE 9.8 //Units: N*m
#define MOTOR_KT 0.26 //Units: N*m/Arms
#define CONVERTER_PEAK_CURRENT 107.2 //Units: [A], ID 110	(not used)
#define MOTOR_NOMINAL_CURRENT 41 //Units: [A], ID 111
#define MOTOR_PEAK_CURRENT 109 //Units: [A], ID 109	(not used)
#define TIME_MAX_CURRENT_MOTOR 1.24 //Units: [s], ID34168	(not used)

// AMK motor maximum ratings
#define MOTOR_MAX_TORQUE 21000 //Units: [mNM]
#define MOTOR_MAX_RPM 20000 //Units: [RPM]

typedef enum {
    ETAS_MODE,
    DEFAULT_MODE, 
    EXTERNAL_INPUT_MODE,  
    ERROR_MODE,
    REVERSE_MODE
} _IIB_MODES_;

/// IIB mission modes
#define DFT_MISSION_MODE 0
#define ENDURANCE_MODE 1 

/// ENDURANCE MODE STUFF    
#define ENDURANCE_LAPS 22
#define SECTORS 3 
#define DFT_FINAL_SOC 15
#define MAX_FINAL_SOC 30

/// AMK controll modes
#define SPEED_CONTROL 0
#define TORQUE_CONTROL 1
#define CONTROL_MODE SPEED_CONTROL /// default

#define WTD_TE_LIMIT 500
#define WTD_EXTERNAL_LIMIT 1000
#define AXIL_ERROR_TIME_LIMIT 1000

#define IIB_PARAMETER_NUMBER CFG_IIB_SIZE

#define I2T_VOLUME 16000 /** Default volume */

#define FIFO_SIZE 64

#define ADD(X, Y) (X) + (Y)

/// Represents the car status, important info about the car will be stored in this structure
typedef struct {

    union {

        struct {

            unsigned te_ok : 1; /** every thing is ok with the Torque encoder */
            unsigned ext_wtd_exceeded : 1; /** every thing is ok with the ARM ccontroller */
            unsigned etas_stupid : 1; /** Arm is sending stupid message and we ignored ir */
            unsigned arm_recv_msg : 1; /** Indicates we received an arm setpoints message */
            unsigned sdc1 : 1; /** status of the shutdown circuit 1 */
            unsigned sdc2 : 1; /** status of the shutdown circuit 2 */
            unsigned car_rtd : 1; /** if 1 the rest of the car is "ready to drive" */
            unsigned inv_hv : 1; /** inverters have High voltage */
            unsigned tsal_status : 1;
        };

        uint16_t word;
    };

} CarStatus;

/// Status word containing all temperature status about the inverter pack
typedef struct {

    union {

        struct {

            /// Indicates if motor is in derating, and what derating caused it
            unsigned inv_derating_on : 1;
            unsigned inv_max_passed : 1;

            unsigned igbt_derating_on : 1;
            unsigned igbt_max_passed : 1;

            unsigned motor_derating_on : 1;
            unsigned motor_max_passed : 1;

            unsigned i2t_overload_active : 1; /** Motor as entered i2t time window */
            unsigned i2t_cooldown_on : 1; //i2t_cooldown_active	:1;	/** Motor is cooling down, limited to nominal current */
        };

        uint8_t word;
    };

} TempStatus;

/// Each inverter torque and power limit
typedef struct {

    /// Power:
    uint16_t max_sfty_pwr;
    uint16_t* max_op_pwr;

    /// Torque:
    uint16_t max_sfty_t;
    uint16_t* max_op_t;

    /** Regen torque */
    uint16_t max_sfty_reg_t;
    uint16_t* max_op_reg_t;

    uint16_t max_sfty_reg_pwr;
    uint16_t* max_op_reg_pwr;

} InverterLimits;

/**************************************************************************************************/ /**
*										MAIN STRUCTURES					 						      *
******************************************************************************************************/
/***************************/ /**
 CAR
*******************************/
/// As all information about the car, the status and other properties and more specific details about what the car is doing
typedef struct {

    float apps;
    float brake;
    uint16_t bms_voltage;
    uint8_t soc;
    uint32_t ISA_pwr;

    CarStatus status;

    int te_alive_again;

} Car;

/***************************/ /**
 Safety
*******************************/
/// Contains all safety parameters to check about the car and inverters
typedef struct {

    //unsigned power_derating		:1;		/** indicates if we are in power derating */	(TO BE USED)!!!
    InverterLimits inv_lim[4];
    TempStatus temp_status[4]; /** temperature derating status */
    uint16_t* max_rpm;

    uint8_t* max_pwr;       // [kW]
    uint8_t* max_regen_pwr;

    uint8_t* max_drt_soc; // max power for soc derating [kW]
    uint8_t* min_drt_soc; // min power for soc derating [kW]

    /// i2t controll, 1 parameter per inverter
    uint16_t max_i2t_current[4];
    float i2t_saturation_time[4];
    float cool_down_on_time[4];

    float i2t_volume[4];
    int16_t previous_i2t_current[4];
    uint16_t i2t_trigger_volume;

    bool error_latch[4];

} Safety;

typedef struct {

    uint16_t inv_msg_read_error_numb;

    /// For debugging show deratings
    uint16_t derating_torque_operation[4];
    uint16_t derating_torque_safety[4];
    uint16_t temperature_derating[4];
    uint16_t voltage_derating_torque[4];
    uint16_t total_power[4];
    uint16_t i2t_derating[4];

} Debug;

typedef struct {
    uint16_t* p;
    uint16_t max;
    uint16_t min;
    uint16_t dflt;
} Settables;

typedef struct{
    int16_t rpm;
    uint16_t t_p;
    int16_t t_n;
} DSR_Setpoints;

/***************************/ /**
 IIB (Inverter Interface Board)
*******************************/
/// Contains every thing the iib needs to controll, 4 inverters, the car input and also variables related to the IIB, for example its temperature
typedef struct {

    uint16_t* input_mode; /** IIB operating mode */
    int arm_input_mode; /** ARM currente controll mode */
    uint32_t ext_wtd_limit;
    uint16_t* debug_mode; /** Mode to get extra code information*/
    uint16_t* amk_control_mode;
    uint8_t mission_mode;

    /// Endurance mode stuff
    uint8_t* endurance_cycles; // could be laps or sectors*laps
    uint8_t* final_soc; // desired soc to complete the number of cycles


    int16_t air_temp; /** Ambient IIB temperature */
    int16_t discharge_temp; /** Discharge circuit temperature */
    uint16_t fan_speed; /** Speed of the fan that controlls the iib temperature */

    /// Status
    bool fan_on;
    bool cu_power_a;
    bool cu_power_b;
    bool inv_rtd; /** If inverters are ready */
    bool inv_on; /** Inverters are on */
    bool turning_on; /** CAR is turning on */
    bool turning_off; /** CAR is turning off */
    uint16_t* regen_on; /** Regeneration alowed */
    uint8_t* pl_on; /** Power Limiter */
    uint8_t* soc_derrating_on; /** Power Limiter */
    bool allow_reset_errors;

    int arm_message_number;

    DSR_Setpoints dsr[4];   /** Desired RPM and Torque Setpoints */
    AMK_Inverter inv[4]; /** The 4 inverters in the inverter pack */
    Car car;
    Safety iib_safety;

    uint16_t version[4];
    //Debug iib_debug;

    Settables sets[IIB_PARAMETER_NUMBER];

    uint16_t *diff_gain;
    float steer_angle;
} iib_t;

extern iib_t iib;

/// Functions

void send_to_amk(void);
void set_idle(int inverter);
void set_idle_all(void);
void set_turn_off(int inverter);
void set_turn_off_all(void);
void set_turn_on(int inveter);
void set_turn_on_all(void);
void set_reset(int inverter);
void set_reset_all();
void convert_unit(void);
void calculate_torque(int inverter);
void calculate_current(int inverter);
void update_inverter_actual_values_1(int inverter_num, uint16_t message_data[4]);
void update_inverter_actual_values_2(int inverter_num, uint16_t message_data[4]);
void process_amk_message(CANdata msg);

#endif
