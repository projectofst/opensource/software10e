/*! \file
 *  \brief  Safety functions
 */

#include "safety.h"
#include "iib.h"
#include <time.h>

/// Extern variables
extern uint32_t wtd_ext_input;
extern uint32_t wtd_isa;
extern uint32_t wtd_torque_encoder;
extern uint16_t good_arm_msg_timer;
extern uint32_t axil_error[2];
extern uint32_t axil_error_time[2];

extern uint16_t line;

/**
 *	Name: verify_shut_circuit;
 *	Description: Updates shutdown circuit status and turns nverters o
 *	n if something is wrong
 *	Input's: iib_t structure
 *	Output: void
 */
void verify_shut_circuit(void)
{

    /// Second SDC detection point
    if (sdc2_READ)
    { /** Good */
        iib.car.status.sdc2 = 1;
    }
    else
    { /** Not good */
        iib.car.status.sdc2 = 0;
    }

    /// First SDC detection point
    if (sdc1_READ)
    {
        iib.car.status.sdc1 = 1;
    }
    else
    { /** Not good */
        iib.car.status.sdc1 = 0;
    }

    update_iib_car();

    return;
}

/**
 *	Name: derating_linear_form;
 *	Description: Calulate y = y(x), linear equation
 *	Input's: equation slope m, curve displacement in y axis, b, and the x value
 *	Output: unsigned integer
 */
float derating_linear_form(float m, int16_t b, int16_t x)
{
    return m * x + b;
}

uint32_t soc_derating()
{
    float slope = (*(iib.iib_safety.max_drt_soc) - *(iib.iib_safety.min_drt_soc)) * 1.0 / (100 - 0);
    uint16_t b = *(iib.iib_safety.min_drt_soc);

    return 1000 * derating_linear_form(slope, b, iib.car.soc);
}

/**
 *	Name: derating_power_limiter;
 *	Description: Calulate the gain that should be multiplied by the torque to have a power < 80kw
 *	Input's:
 *	Output:
 */
float derating_power_limiter()
{
    can_t *can = get_can();
    uint8_t i = 0;
    uint16_t expected_mot_power[4] = {0};
    static uint32_t expected_power = 0;
    float power_limiter_gain = 1.0;
    float power_error = 0;
    uint16_t ISA_power = iib.car.ISA_pwr;
    uint32_t max_pwr = *(iib.iib_safety.max_pwr);
    int32_t min_pwr = -*(iib.iib_safety.max_regen_pwr) * 1000;

    if (*(iib.soc_derrating_on))
    {
        max_pwr = MIN(*(iib.iib_safety.max_pwr) * 1000, soc_derating());
    }

    can->iib.iib_status.max_car_pwr = max_pwr / 1000;

    // Watchdog for ISA CAN message -- Turn off power limiter after 100ms without ISA message
    if (wtd_isa > 100 || !*(iib.pl_on))
    {
        power_limiter_gain = (float)max_pwr / (*(iib.iib_safety.inv_lim[MOTOR_FL].max_op_pwr) + *(iib.iib_safety.inv_lim[MOTOR_FR].max_op_pwr) + *(iib.iib_safety.inv_lim[MOTOR_RL].max_op_pwr) + *(iib.iib_safety.inv_lim[MOTOR_RR].max_op_pwr));
        if (power_limiter_gain > 1)
        {
            return 1.0;
        }
        else
        {
            return power_limiter_gain;
        }
    }

    // Calculate error of previous power estimation //CONDITION VARIABLE DOESN'T SEEM TO BE CHANGED
    if (expected_power != 0)
    {
        power_error = -1 + ISA_power / expected_power; // - 1 + 85k / 80k = 0.06
    }
    else
    {
        power_error = 0;
    }

    /* SAFETY RESTRICTIONS */
    // regen
    // if (ISA_power < 0 || expected_power < 0) {
    //	return 1.0;
    //}

    // Saturation
    if (power_error > 0.8)
    {
        power_error = 0.8;
    }
    else if (power_error < -0.8)
    {
        power_error = -0.8;
    }

    // Calculate expected power consuption with the torques to be sent to the motors and current angula velocity
    for (i = 0; i < 4; i++)
    {
        if (iib.dsr[i].rpm > 0)
        { // accelerating
            expected_mot_power[i] = iib.dsr[i].t_p / 100 * RAD(iib.inv[i].actual_speed);
        }
        else if (iib.dsr[i].rpm == 0)
        { // travating
            expected_mot_power[i] = iib.dsr[i].t_n / 100 * RAD(iib.inv[i].actual_speed);
        }
    }
    expected_power = expected_mot_power[MOTOR_FL] + expected_mot_power[MOTOR_FR] + expected_mot_power[MOTOR_RL] + expected_mot_power[MOTOR_RR]; // 79k

    // Calculate the final gain that multiplied by the 4 torques should give a power under 80kw
    if (expected_power * (1 + power_error) > max_pwr)
    {                                                                        // 83.7k
        power_limiter_gain = max_pwr / (expected_power * (1 + power_error)); // = 0.955
    }
    else if (expected_power * (1 + power_error) < min_pwr)
    {
        power_limiter_gain = min_pwr / (expected_power * (1 + power_error));
    }
    else
    {
        power_limiter_gain = 1;
    }

    expected_power = expected_power * power_limiter_gain; // = 75k

    CANdata msg;
    msg.dev_id = 16;
    msg.msg_id = 26;
    msg.dlc = 8;
    msg.data[0] = (int16_t)(power_limiter_gain * 100);
    msg.data[1] = (int16_t)expected_power;
    msg.data[2] = (int16_t)ISA_power;
    msg.data[3] = (int16_t)power_error;

    send_can2(msg);

    return power_limiter_gain; // ------- agora logo aseguir multiplicar pelos torques antes de mandar para os AMK
}

/**
 *	Name: temperature_deratings;
 *	Description: Applys deratings concerning the 3 critical temperatures(Inverters, motors and IGBT)
 *	Input's: motor number, iib_t structure
 *	Output: Max torque if every thing is ok 21 N*m *1000, or the most limiting derating, meaning the lowest torque value
 */
uint16_t temperature_deratings(int motor)
{
    int i = motor;
    float slope = 10;
    /** initialize with the max available torque */

    /// Verify Inverter cold plate temperature
    if (iib.inv[i].temp_inverter > WARNING_INVERTER_TEMP && iib.inv[i].temp_inverter < MAX_INVERTER_TEMP)
    {

        /// We entered derating
        iib.iib_safety.temp_status[i].inv_derating_on = 1;

        slope = *(iib.iib_safety.inv_lim[i].max_op_t) * 1.0 / (MAX_INVERTER_TEMP - WARNING_INVERTER_TEMP);
        // printf("op_t: %d; slope: %f\n",*(iib.iib_safety.inv_lim[i].max_op_t),slope);

        iib.dsr[i].t_p = MIN(iib.dsr[i].t_p, *(iib.iib_safety.inv_lim[i].max_op_t) - slope * (iib.inv[i].temp_inverter - MAX_INVERTER_TEMP));
    }
    else if (iib.inv[i].temp_inverter >= MAX_INVERTER_TEMP)
    { /** We passed the max temperature */

        iib.iib_safety.temp_status[i].inv_derating_on = 0;
        iib.iib_safety.temp_status[i].inv_max_passed = 1;
        set_idle_all();
    }

    /// Verify IGBT temperature
    if (iib.inv[i].temp_IGBT > WARNING_IGBT_TEMP && iib.inv[i].temp_IGBT < MAX_IGBT_TEMP)
    {
        /// We entered derating
        iib.iib_safety.temp_status[i].igbt_derating_on = 1;

        slope = *(iib.iib_safety.inv_lim[i].max_op_t) * 1.0 / (MAX_IGBT_TEMP - WARNING_IGBT_TEMP);
        // printf("op_t: %d; slope: %f\n",*(iib.iib_safety.inv_lim[i].max_op_t),slope);

        iib.dsr[i].t_p = MIN(iib.dsr[i].t_p, *(iib.iib_safety.inv_lim[i].max_op_t) - slope * (iib.inv[i].temp_IGBT - MAX_IGBT_TEMP));
    }
    else if (iib.inv[i].temp_IGBT >= MAX_IGBT_TEMP)
    { /** We passed the max temperature */
        iib.iib_safety.temp_status[i].igbt_derating_on = 0;
        iib.iib_safety.temp_status[i].igbt_max_passed = 1;
        set_idle_all();
        // igbt_derating = 0;	/** Return 0 torque */
    }

    /// Verify Motor temperature
    if (iib.inv[i].temp_motor > WARNING_MOTOR_TEMP && iib.inv[i].temp_motor < MAX_MOTOR_TEMP)
    {

        /// We entered derating
        iib.iib_safety.temp_status[i].motor_derating_on = 1;

        slope = *(iib.iib_safety.inv_lim[i].max_op_t) * 1.0 / (MAX_MOTOR_TEMP - WARNING_MOTOR_TEMP);
        // printf("op_t: %d; slope: %f\n",*(iib.iib_safety.inv_lim[i].max_op_t),slope);

        iib.dsr[i].t_p = MIN(iib.dsr[i].t_p, *(iib.iib_safety.inv_lim[i].max_op_t) - slope * (iib.inv[i].temp_motor - WARNING_MOTOR_TEMP));
    }
    else if (iib.inv[i].temp_motor >= MAX_MOTOR_TEMP)
    { /** We passed the max temperature */

        iib.iib_safety.temp_status[i].motor_derating_on = 0;
        iib.iib_safety.temp_status[i].motor_max_passed = 1;
        set_idle_all();
    }

    update_iib_inv(i);

    return 0; /// if non of this conditions are meet, something is wrong with the code
}

/**
 *    Name: verify_TSAL
 *    Description: Updates TSAL status
 *    Input's: iib_t structure
 *    Output: void
 */

void verify_TSAL(void)
{

    iib.car.status.tsal_status = TSAL_INV_READ;
    update_iib_car();
}

void reset_amk_erros()
{

    int i = 0;

    if (iib.allow_reset_errors)
    {

        for (i = 0; i < 4; i++)
        {

            if (iib.inv[i].status.err != 0)
            {
                if (i > 1)
                    set_reset(i);
                iib.iib_safety.error_latch[i] = 1;
                axil_error[i > 1 ? 1 : 0] = 1;
                axil_error_time[i > 1 ? 1 : 0] = 0;
            }

            if (iib.iib_safety.error_latch[i] == 1)
            {
                if (iib.inv[i].status.err == 0 && iib.inv[i].status.sys_ready == 1)
                {

                    iib.iib_safety.error_latch[i] = 0;

                    if (iib.inv_on)
                    {

                        set_idle(i);
                        set_turn_on(i);
                    }
                    else if (!iib.inv_on)
                    {

                        set_turn_off(i);
                    }
                }
            }
        }

        update_iib_info();
    }

    return;
}

// Does not work
void check_hv_status()
{

    uint16_t hv_status = 0;
    uint16_t i = 0;

    // for (i = 0; i < N_MOTOR; i++) {

    //     if (iib.inv[i].status.quit_dc_on = 1) {
    //         hv_status++;
    //     }
    // }

    // if (hv_status != 0) {
    //     iib.car.status.inv_hv = 1;
    // } else {
    //     iib.car.status.inv_hv = 0;
    // }

    return;
}

void limit_rpm(void)
{

    uint16_t i = 0;

    for (i = 0; i < N_MOTOR; i++)
    {

        if (iib.dsr[i].rpm > (int16_t)(*iib.iib_safety.max_rpm))
        {

            iib.dsr[i].rpm = *(iib.iib_safety.max_rpm);
        }
    }

    return;
}

void sdc_check(void)
{

    if ((sdc1_READ == 0 || sdc2_READ == 0) && iib.inv_on == 1)
    {

        turn_off_sequence(WE_TURN_OFF);
    }

    return;
}

uint16_t limit_positive_torque(uint16_t limit, uint16_t setpoint)
{

    if (setpoint > limit)
    {
        return limit;
    }
    else
    {
        return setpoint;
    }
}

int16_t limit_negative_torque(int16_t limit, int16_t setpoint, uint16_t regen_active)
{

    if (!regen_active)
    {
        return 0;
    }
    else
    {
        if (setpoint < limit)
        {
            return limit;
        }
        else
        {
            return setpoint;
        }
    }
}

void ez_safety(void)
{
    can_t *can = get_can();

    int i = 0;
    uint16_t ola;

    // checks
    check_hv_status();
    verify_shut_circuit();

    if (*(iib.input_mode) != EXTERNAL_INPUT_MODE)
    {
        for (i = 0; i < 4; i++)
        {

            // set local torque setpoint and limits variables
            uint16_t *pos_setp = &iib.dsr[i].t_p;
            uint16_t max_torque = *(iib.iib_safety.inv_lim[i].max_op_t);

            int16_t *neg_setp = &iib.dsr[i].t_n;
            int16_t min_torque = *(iib.iib_safety.inv_lim[i].max_op_reg_t);

            // Wut is this
            can->iib.pre_eff.pre_eff_t_p[i] = iib.dsr[i].t_p;
            can->iib.pre_eff.pre_eff_t_n[i] = iib.dsr[i].t_n;
            send_can2(encode_pre_eff(can->iib.pre_eff, i));
            //

            // Torque limits
            *pos_setp = limit_positive_torque(max_torque, *pos_setp);
            *neg_setp = limit_negative_torque(-min_torque, *neg_setp, *(iib.regen_on));

            uint16_t curve_value = get_accel_curve(i);
            if (curve_value < *pos_setp)
            {
                *pos_setp = curve_value;
            }

            int16_t neg_curve_value = get_brake_curve(i);
            int aux = *neg_setp;
            if (neg_curve_value > *neg_setp)
            {
                *neg_setp = neg_curve_value;
            }

            // ola = temperature_deratings(i);
        }

        if (*(iib.pl_on))
        {
            float power_limiter_gain = derating_power_limiter();
            for (i = 0; i < 4; i++)
            {
                if (iib.dsr[i].t_p > 0)
                {
                    iib.dsr[i].t_p = iib.dsr[i].t_p * power_limiter_gain;
                }
                else if (iib.dsr[i].t_n < 0)
                {
                    iib.dsr[i].t_n = iib.dsr[i].t_n * power_limiter_gain;
                }
            }
        }

        limit_rpm();
    }
    else
    {
        for (i = 0; i < 4; i++)
        {

            // set local torque setpoint and limits variables
            uint16_t *pos_setp = &iib.dsr[i].t_p;
            int16_t *neg_setp = &iib.dsr[i].t_n;
            int16_t *rpm_setp = &iib.dsr[i].rpm;

            can->iib.pre_eff.pre_eff_t_p[i] = iib.dsr[i].t_p;
            can->iib.pre_eff.pre_eff_t_n[i] = iib.dsr[i].t_n;
            send_can2(encode_pre_eff(can->iib.pre_eff, i));

            // Torque limits
            *pos_setp = limit_positive_torque(MOTOR_MAX_TORQUE, *pos_setp);
            *neg_setp = limit_negative_torque(-MOTOR_MAX_TORQUE, *neg_setp, 1);
            *rpm_setp = (*rpm_setp > MOTOR_MAX_RPM) ? MOTOR_MAX_RPM : *rpm_setp;
        }
    }

    if (axil_error_time[0] >= AXIL_ERROR_TIME_LIMIT)
        axil_error[0] = 0;
    if (axil_error_time[1] >= AXIL_ERROR_TIME_LIMIT)
        axil_error[1] = 0;

    if (axil_error[0] == 1 && axil_error_time[0] <= AXIL_ERROR_TIME_LIMIT)
    {
        iib.dsr[0].t_p = 0;
        iib.dsr[1].t_n = 0;
    }
    else if (axil_error[1] == 1 && axil_error_time[1] <= AXIL_ERROR_TIME_LIMIT)
    {
        iib.dsr[2].t_p = 0;
        iib.dsr[3].t_n = 0;
    }

    if (*(iib.input_mode) == DEFAULT_MODE || *(iib.input_mode) == EXTERNAL_INPUT_MODE)
    {
        if (wtd_torque_encoder > WTD_TE_LIMIT)
        {
            iib.car.status.te_ok = 0;
            set_idle_all();
        }
        if (wtd_ext_input > WTD_EXTERNAL_LIMIT && *(iib.input_mode) == EXTERNAL_INPUT_MODE && iib.inv_on == 1)
        {
            *(iib.input_mode) = DEFAULT_MODE;
            iib.car.status.etas_stupid = 1;
            set_idle_all();
        }
        sdc_check();
    } /*  else if (*(iib.input_mode) == EXTERNAL_INPUT_MODE) {
        if (wtd_ext_input > iib.ext_wtd_limit && iib.inv_on == 1) { /// External input wtd timer is shorter in case the inverters are on
            iib.car.status.ext_wtd_exceeded = 1;
            turn_off_sequence(WE_TURN_OFF);
        }
    } */

    /* if (iib.car.status.inv_hv) { */
    iib.inv_rtd = 1;
    /* } else if (!iib.car.status.inv_hv) {
        iib.inv_rtd = 0;
    } */
    update_iib_info();

#if 0
    if (iib.turning_on == 1 || iib.turning_off == 1 || iib.inv_rtd == 0 || iib.car.status.car_rtd == 0) {
        set_idle_all();
    }
#endif

    if (iib.turning_on == 1 || iib.turning_off == 1 || iib.inv_rtd == 0 /*|| iib.car.status.car_rtd == 0*/)
    {
        set_idle_all();
    }

    // verify system ready
    for (i = 0; i < 4; i++)
    {
        if (!iib.inv[i].status.sys_ready)
        {
            set_idle(i);
        }
    }

    reset_amk_erros();

    // verify system ready
    /*
    for (i = 0; i < 4; i++) {
        if (!iib.inv[i].status.sys_ready) {
            set_idle(i);
        }
    }*/

    // Tem de ser meter um bit de status a controlar isso
    // reset_amk_erros();

    CANdata msg1;

    msg1.dev_id = 16;
    msg1.msg_id = 21;
    msg1.dlc = 8;

    msg1.data[0] = iib.dsr[0].t_p;
    msg1.data[1] = iib.dsr[0].t_n;
    msg1.data[2] = iib.dsr[0].rpm;
    msg1.data[3] = 2;

    send_can2(msg1);

    msg1.dev_id = 16;
    msg1.msg_id = 22;
    msg1.dlc = 8;

    msg1.data[0] = iib.dsr[1].t_p;
    msg1.data[1] = iib.dsr[1].t_n;
    msg1.data[2] = iib.dsr[1].rpm;
    msg1.data[3] = 2;

    send_can2(msg1);

    msg1.dev_id = 16;
    msg1.msg_id = 23;
    msg1.dlc = 8;

    msg1.data[0] = iib.dsr[2].t_p;
    msg1.data[1] = iib.dsr[2].t_n;
    msg1.data[2] = iib.dsr[2].rpm;
    msg1.data[3] = 2;

    send_can2(msg1);

    msg1.dev_id = 16;
    msg1.msg_id = 24;
    msg1.dlc = 8;

    msg1.data[0] = iib.dsr[3].t_p;
    msg1.data[1] = iib.dsr[3].t_n;
    msg1.data[2] = iib.dsr[3].rpm;
    msg1.data[3] = 2;

    send_can2(msg1);
    return;
}
