#include "car.h"

/// Extern variables
extern uint32_t wtd_ext_input;
extern uint32_t wtd_isa;
extern uint32_t wtd_torque_encoder;
extern uint32_t wtd_steer;
extern uint32_t wtd_revive_steer;
extern uint16_t flash[IIB_PARAMETER_NUMBER];

extern uint16_t te_alive_timer;
/// Global
uint16_t te_alive;

CANdata aux_rtd_msg;

COMMON_MSG_RTD_ON message_ON;

/**
 *	Name: turn_on_sequence;
 *	Description: Executes the inverter turn on sequence
 *	Input's: iib_t structure
 *	Output: void
 */
void turn_on_sequence(void)
{

    /// CAR is now RTD
    iib.car.status.car_rtd = 1;
    /// We are in turning on process
    iib.turning_on = 1;

    set_turn_on_all(); /** Sets controll word for turn on */
    set_idle_all(); /** need to set setp to 0 */

    send_can2(make_rtd_on_msg(DEVICE_ID_IIB, RTD_STEP_INV_GO));

    return;
}

/**
 *	Name: turn_off_sequence;
 *	Description: Executes the inverter turn off sequence
 *	Input's: iib_t structure, what triggered the turn off sequence (mode)
 *	Output: void
 */
void turn_off_sequence(int mode)
{
    CANdata msg_rtd;

    set_turn_off_all(); /** Sets controll word */

    /// CAR is no longer in RTD
    iib.car.status.car_rtd = 0;
    update_iib_car();
    /// We are in turning off process
    iib.turning_off = 1;

    /// Sees what made us turnoff and send the acknoledge message

    if (mode == TURN_OFF_REQUEST) {
        msg_rtd = make_rtd_off_msg(DEVICE_ID_IIB, TURN_OFF_REQUEST);
        msg_rtd.dlc = 2;

        send_can2(msg_rtd);
    } else if (mode == WE_TURN_OFF) {
        msg_rtd = make_rtd_off_msg(DEVICE_ID_IIB, WE_TURN_OFF);
        msg_rtd.dlc = 2;

        send_can2(msg_rtd);
    } else {
        return;
    }

    send_can2(logI(LOG_RTD_OFF, 0, 0, 0));

    return;
}

/**
 *	Name: handle_rtd_on_message;
 *	Description: handles an RTD on message, sees number of step and if iib is ok
 *	Input's: iib_t structure, msg data
 *	Output: void
 */
void handle_rtd_on_message(CANdata msg)
{

    parse_can_common_RTD(msg, &message_ON);

    /// TE ok step and iib is ready continue the RTD sequence
    if (message_ON.step == RTD_STEP_TE_OK) {
        /// Inverters ok
        if (iib.inv_rtd == 1)
            send_can2(make_rtd_on_msg(DEVICE_ID_IIB, RTD_STEP_INV_OK));
        /// Inverters not ok
        else if (iib.inv_rtd == 0)
            send_can2(make_rtd_on_msg(DEVICE_ID_IIB, RTD_STEP_INV_NOK));
    }

    /// Te is not ok
    if (message_ON.step == RTD_STEP_TE_NOK) {

        iib.car.status.car_rtd = 0; /** CAR is not in rtd condition */
        update_iib_car();

        /// If the inverters are on, turn off
        if (iib.inv_on == 1) {
            turn_off_sequence(10 /*TURN_OFF_REQUEST*/);
        }
    }

    /// DCU step
    else if (message_ON.step == RTD_STEP_DCU) {

        /// Inverters already on
        if (iib.inv_on == 1) {
            return;
        }

        /// Inverters ok
        if (iib.inv_rtd == 1) {
            /// Lets jardate motherfuckeeeer!!!(by: Pedro Santos)
            turn_on_sequence();
            send_can2(logI(LOG_RTD_ON, 0, 0, 0));
        }
        /// Inverters not ok
        else if (iib.inv_rtd == 0)
            send_can2(make_rtd_on_msg(DEVICE_ID_IIB, RTD_STEP_INV_NOK));
    }

    return;
}

void run_default_mode(void)
{
    can_t* can = get_can();
    /* braking ?
     * speed is 0, pos torque is 0, neg_torque is max_operation scaled
     * with brake_pressure
     */

    int i = 0;
    for (i = 0; i < 4; i++) {
        can->iib.iib_diff.iib_diff_value[i] = 0;
        can->iib.iib_diff.iib_diff_t_p[i] = 0;
        can->iib.iib_diff.iib_diff_steer_ang = 0;
        can->iib.iib_diff.iib_diff_gain = *(iib.diff_gain);
    }

    if (iib.car.brake > 0.05 && *(iib.regen_on) == 1) {
        for (i = 0; i < 4; i++) {
            DSR_Setpoints* setp = &iib.dsr[i];

            setp->rpm = 0;
            setp->t_p = 0;
            setp->t_n = (int16_t)-1 * (iib.car.brake * *(iib.iib_safety.inv_lim[i].max_op_reg_t));
        }
    }
    /* not braking ?
     * speed is max rpm, torque pos is max operation scaled with apps,
     * neg torque is 0
     */
    else {
        for (i = 0; i < 4; i++) {
            DSR_Setpoints* setp = &iib.dsr[i];

            setp->rpm = *(iib.iib_safety.max_rpm);
            setp->t_p = (uint16_t)(*(iib.iib_safety.inv_lim[i].max_op_t) * iib.car.apps);

            uint16_t diff_value = *(iib.diff_gain) * get_diff_curve_value(i, (int16_t)iib.steer_angle);
            int16_t tmp_torque = setp->t_p - diff_value;

            if (tmp_torque < 0) {
                setp->t_p = 0;
            } else if (((uint16_t)tmp_torque) < setp->t_p) {
                setp->t_p = tmp_torque;
            }

            can->iib.iib_diff.iib_diff_value[i] = (int16_t)diff_value;
            can->iib.iib_diff.iib_diff_t_p[i] = setp->t_p;
            can->iib.iib_diff.iib_diff_steer_ang = (int16_t)iib.steer_angle;
            can->iib.iib_diff.iib_diff_gain = *(iib.diff_gain);

            setp->t_n = 0;
        }
    }

    //! check update can and new diff gain
    send_can2(encode_iib_diff(can->iib.iib_diff, i));

    return;
}
void run_ext_input_mode(void)
{

    int i = 0;

    for (i = 0; i < N_MOTOR; i++) { //  redundant
        DSR_Setpoints* setp = &iib.dsr[i];

        setp->rpm = iib.dsr[i].rpm;
        setp->t_p = iib.dsr[i].t_p;
        setp->t_n = iib.dsr[i].t_n;
    }

    CANdata msg;

    msg.dev_id = 16;
    msg.msg_id = 20;
    msg.dlc = 8;

    msg.data[0] = iib.dsr[3].t_p;
    msg.data[1] = iib.dsr[3].t_n;
    msg.data[2] = iib.dsr[3].rpm;
    msg.data[3] = 4;

    send_can2(msg);

    return;
}

void run_reverse_mode(void)
{
    int i = 0;

    if (iib.car.brake > 0.05 && *(iib.regen_on) == 1) {
        for (i = 0; i < 4; i++) {
            DSR_Setpoints* setp = &iib.dsr[i];

            setp->rpm = 0;
            setp->t_p = (uint16_t)(*(iib.iib_safety.inv_lim[i].max_op_t) * iib.car.apps);
            setp->t_n = 0;
        }
    } else {
        for (i = 0; i < 4; i++) {
            DSR_Setpoints* setp = &iib.dsr[i];

            setp->rpm = 5000; // maybe -?
            setp->t_p = 0;
            setp->t_n = (int16_t)-1 * (*(iib.iib_safety.inv_lim[i].max_op_t) * iib.car.apps);
        }
    }

    return;
}

void run_burnout_mode(void)
{
    int i = 0;

    for (i = 0; i < 4; i++) {
        DSR_Setpoints* setp = &iib.dsr[i];

        if (*(iib.iib_safety.max_rpm) > 5000) {
            setp->rpm = 5000;
        } else {
            setp->rpm = *(iib.iib_safety.max_rpm);
        }

        if (i < 2) { // maybe put max_op_t equal in all wheels
            setp->t_n = (uint16_t)(*(iib.iib_safety.inv_lim[i].max_op_t) * iib.car.apps);
            setp->t_p = 0;
        } else if (i >= 2) {
            setp->t_p = (uint16_t)(*(iib.iib_safety.inv_lim[i].max_op_t) * iib.car.apps);
            setp->t_n = 0;
        }
    }

    return;
}

void setp_config(void)
{

    if (*(iib.input_mode) == DEFAULT_MODE) {
        run_default_mode();
    } else if (*(iib.input_mode) == EXTERNAL_INPUT_MODE) {
        run_ext_input_mode();
        // maybe check values before going full throttle
    } else if (*(iib.input_mode) == ERROR_MODE) {
        // maybe log
    } /* else if (*(iib.input_mode) == REVERSE_MODE){
        run_reverse_mode();
    } else if (*(iib.input_mode) == BURNOUT_MODE){
        run_burnout_mode();
    }*/

    return;
}

void endurance_mode(void)
{
    static uint8_t prev_soc;
    static float soc_per_cycle;
    static uint8_t cycle = 0;
    uint8_t delta_soc;
    float soc_error;

    if (iib.mission_mode != ENDURANCE_MODE) {
        iib.mission_mode = ENDURANCE_MODE;
        prev_soc = iib.car.soc;
        soc_per_cycle = (prev_soc - *(iib.final_soc)) / (1.0 * *(iib.endurance_cycles));
        return;
    }

    cycle++; // One lap passed by

    if (cycle >= *(iib.endurance_cycles)) {
        iib.mission_mode = DFT_MISSION_MODE;
        return;
    }

    delta_soc = prev_soc - iib.car.soc; // see wasted soc
    prev_soc = iib.car.soc; // save new soc

    if (delta_soc == 0) {
        soc_error = 1;
    } else {
        soc_error = soc_per_cycle / delta_soc;
    }

    *(iib.iib_safety.max_pwr) = *(iib.iib_safety.max_pwr) * soc_error; // corrects error

    // Truncating
    if (*(iib.iib_safety.max_pwr) > DFT_TOTAL_POWER) {
        *(iib.iib_safety.max_pwr) = DFT_TOTAL_POWER;
    } else if (*(iib.iib_safety.max_pwr) < *(iib.iib_safety.min_drt_soc)) {
        *(iib.iib_safety.max_pwr) = *(iib.iib_safety.min_drt_soc);
    }

    soc_per_cycle = (prev_soc - *(iib.final_soc)) / (1.0 * (*(iib.endurance_cycles) - cycle));

    return;
}

/**
 *	Name: save_TE;
 *	Description: Saves TE messages in iib_t structure
 *	Input's: iib_t structure, msg data
 *	Output: void
 */
void save_TE(void)
{
    can_t* can = get_can();

    wtd_torque_encoder = 0;
    iib.car.status.te_ok = 1;
    update_iib_car();

    if (*(iib.input_mode) == DEFAULT_MODE) {
        iib.car.apps = can->te.te_main.te_main_APPS / 100.0;
        // iib.car.brake = can->te.te_main.te_main_BPSp / 100.0;
        iib.car.brake = can->te.te_main.te_main_BPSe / 100.0;

        // printf("apps: %f, brake: %f\n", iib.car.apps, iib.car.brake);
    }

    return;
}
/**
 *	Name: save_dash;
 *	Description: Saves dash messages in iib_t structure
 *	Input's: iib_t structure
 *	Output: void
 */
void save_dash(void)
{
    can_t* can = get_can();

    float steer = (can->dash.dash_se.dash_se) / 10.0;

    bool msg_good = steer < 90 || steer > -90;
    if (msg_good && wtd_steer < 250) {
        iib.steer_angle = steer;
        wtd_steer = 0;
    } else if (msg_good) {
        wtd_revive_steer++;
        iib.steer_angle = 0;
    } else {
        wtd_revive_steer = 0;
        iib.steer_angle = 0;
    }

    if (wtd_revive_steer > 250) {
        wtd_steer = 0;
    }

    can->iib.iib_diff.iib_diff_steer_ang = (uint16_t)iib.steer_angle;

    return;
}
/**
 *	Name: save_master;
 *	Description: Saves BMS messages in iib_t structure
 *	Input's: iib_t structure
 *	Output: void
 */
void save_master(void)
{
    can_t* can = get_can();

    iib.car.status.inv_hv = can->master.master_status.master_status_air_p;

    //	Master
    if (iib.inv_on && !can->master.master_status.master_status_air_p) {
        turn_off_sequence(TURN_OFF_REQUEST); /** turnoff inverters */
    }

    /* if (iib.inv_on && can->master.master_status.master_status_sdc_open) {
        set_turn_off_all();
    } */

    //	Energy Meter
    iib.car.bms_voltage = (uint16_t)(can->master.master_em.master_em_voltage / 100.0); /* converts voltage units to [V] */
    iib.car.soc = (uint8_t)(can->master.master_em.master_em_soc / 10);

    return;
}
/**
 *	Name: save_isa;
 *	Description: Saves Isabel info in iib_t structure
 *	Input's: iib_t structure
 *	Output: void
 */
void save_isa(void)
{
    can_t* can = get_can();

    iib.car.ISA_pwr = can->isabel.isa_power.isa_power;

    wtd_isa = 0;

    return;
}
/**
 *	Name: process_car_message;
 *	Description: Processes which device sended a message, and handle it
 *	Input's: iib_t structure, msg data
 *	Output: void
 */
void process_car_message(CANdata msg)
{
    can_t* can = get_can();

    decode_can(msg, can);
    cfg_dispatch(msg);
    cmd_dispatch(msg);

    if (msg.dev_id == DEVICE_ID_TE && msg.msg_id == MSG_ID_TE_TE_MAIN) {
        save_TE();
    } else if (msg.dev_id == DEVICE_ID_DASH) {
        save_dash();
    } else if (msg.dev_id == DEVICE_ID_MASTER) {
        save_master();
    } else if (msg.dev_id == DEVICE_ID_ISABEL && msg.msg_id == MSG_ID_ISABEL_ISA_POWER) {
        save_isa();
    } else if (msg.msg_id == CMD_ID_COMMON_RTD_ON) { /// RTD ON MESSAGE
        handle_rtd_on_message(msg); /** process rtd on message */
    } else if (msg.msg_id == CMD_ID_COMMON_RTD_OFF) { /// RTD OFF MESSAGE
        turn_off_sequence(TURN_OFF_REQUEST); /// Turn car off, Start turnoff sequence, acknoledge the turnoff request
    }

    return;
}
