#ifndef __SAFETY_H__
#define __SAFETY_H__

#include "acceleration.h"
#include "car.h"
#include "differential.h"
#include "iib.h"
#include "io.h"

#include "can-ids-v2/can_ids.h"

/// Defines max and minimum values for diferent temperatures (0.1ºC), ex: 500 -> 50ºC

/// Inverter temperature
#define WARNING_INVERTER_TEMP 500
#define MAX_INVERTER_TEMP 660

/// IGBT  temperature
#define WARNING_IGBT_TEMP 1150
#define MAX_IGBT_TEMP 1250

/// Motore temperature
#define WARNING_MOTOR_TEMP 1250
#define MAX_MOTOR_TEMP 1400

/// IIB temperature
#define MAX_IIB_TEMP 850

/// Defines Voltage limits

#define WARNING_DEEP_OF_CHARGE 670 /** [V] */
#define MAX_VOLTAGE 720 /** [V] */
#define WARNING_DEEP_OF_DISCHARGE 300 /** [V] */
#define MIN_VOLTAGE 250 /** [V] */

/// Defines maximum torque, power values (operation and safety), and max speed

#define DFT_TOTAL_POWER 80 /** kW */
#define MAX_TOTAL_POWER 120 /** kW */
#define MAX_REGEN_TOTAL_POWER 120 /** kW */
#define DFT_REGEN_TOTAL_POWER 30 /** kW */
#define MIN_POWER_DRT_SOC 0 /** kW */
#define DFT_MIN_POWER_DRT_SOC 5 /** kW */
#define MAX_POWER_DRT_SOC 180 /** kW */
#define DFT_MAX_POWER_DRT_SOC 180 /** kW */

#define MAX_MOTOR_POWER_SAFETY_FL 35000 /** kW */
#define MAX_MOTOR_POWER_SAFETY_FR 35000 /** kW */
#define MAX_MOTOR_POWER_SAFETY_RL 35000 /** kW */
#define MAX_MOTOR_POWER_SAFETY_RR 35000 /** kW */

#define MAX_MOTOR_POWER_OP_FL 35000 /** KW */
#define MAX_MOTOR_POWER_OP_FR 35000 /** KW */
#define MAX_MOTOR_POWER_OP_RL 35000 /** KW */
#define MAX_MOTOR_POWER_OP_RR 35000 /** kW */

/** Normal torque limits */
#define MAX_MOTOR_TORQUE_SAFETY_FL 21000 /** 0.001 N*m */
#define MAX_MOTOR_TORQUE_SAFETY_FR 21000 /** 0.001 N*m */
#define MAX_MOTOR_TORQUE_SAFETY_RL 21000 /** 0.001 N*m */
#define MAX_MOTOR_TORQUE_SAFETY_RR 21000 /** 0.001 N*m */

#define MAX_MOTOR_TORQUE_OP_FL 21000 /** 0.001 N*m */
#define MAX_MOTOR_TORQUE_OP_FR 21000 /** 0.001 N*m */
#define MAX_MOTOR_TORQUE_OP_RL 21000 /** 0.001 N*m */
#define MAX_MOTOR_TORQUE_OP_RR 21000 /** 0.001 N*m */

/** Regen torque limits */
#define MAX_MOTOR_REGEN_TORQUE_SAFETY_FL 21000 /** 0.001 N*m */
#define MAX_MOTOR_REGEN_TORQUE_SAFETY_FR 21000 /** 0.001 N*m */
#define MAX_MOTOR_REGEN_TORQUE_SAFETY_RL 21000 /** 0.001 N*m */
#define MAX_MOTOR_REGEN_TORQUE_SAFETY_RR 21000 /** 0.001 N*m */

#define MAX_MOTOR_REGEN_TORQUE_OP_FL 21000 /** 0.001 N*m */
#define MAX_MOTOR_REGEN_TORQUE_OP_FR 21000 /** 0.001 N*m */
#define MAX_MOTOR_REGEN_TORQUE_OP_RL 21000 /** 0.001 N*m */
#define MAX_MOTOR_REGEN_TORQUE_OP_RR 21000 /** 0.001 N*m */

#define MAX_MOTOR_REGEN_POWER_OP_FL 35000 /** 0.001 N*m */
#define MAX_MOTOR_REGEN_POWER_OP_FR 35000 /** 0.001 N*m */
#define MAX_MOTOR_REGEN_POWER_OP_RL 35000 /** 0.001 N*m */
#define MAX_MOTOR_REGEN_POWER_OP_RR 35000 /** 0.001 N*m */

#define MAX_REGEN_POWER 30000 /** KW */

#define MAX_RPM 23000 /** rpm */
#define DEFAULT_RPM 20000 /** rpm */
#define MAX_CAR_POWER 80 /** max combined motor power [KW] */

#define KMH_5 1000

/// Other values
#define PI 3.14159265359

/** Computes the minimum of 2 values */
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

void verify_shut_circuit(void);
float derating_linear_form(float m, int16_t b, int16_t x);
uint32_t soc_derating();
uint16_t voltage_deratings(iib_t* iib);
float derating_power_limiter();
uint16_t temperature_deratings(int motor);
uint16_t motor_curve(int16_t rpm, uint16_t max_torque, uint16_t max_power);
uint16_t minimum(uint16_t a, uint16_t b, uint16_t c, uint16_t d, uint16_t e, uint16_t f, uint16_t g);
void apply_limits(iib_t* iib);
int max_temperature_check(iib_t* iib);
float convert_timercount_into_seconds(float period, uint16_t timer_count);
float trap_formula(int16_t h1, int16_t h2, float x);
uint16_t difference_squared(int16_t y1, int16_t y2);
uint16_t i2_t(iib_t* iib, int motor);
void safety_check(iib_t* iib);
uint16_t compute_ntc_temp(int a, int b, int c, int ntc_voltage);
uint16_t calculate_total_power(iib_t* iib);
void reset_amk_erros();
void handle_amk_speed_control(iib_t* iib);
void check_hv_status();
void sdc_check(void);
uint16_t limit_positive_torque(uint16_t limit, uint16_t setpoint);
int16_t limit_negative_torque(int16_t limit, int16_t setpoint, uint16_t regen_active);
void ez_safety(void);
void limit_rpm(void);
void verify_TSAL(void);

#endif