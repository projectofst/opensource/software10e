// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128 // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = OFF // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4 // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// NOTE: Always include timing.h
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"

#include "lib_pic33e/can.h"
#include "lib_pic33e/flash.h"
#include "lib_pic33e/pps.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/timing.h"
#include "lib_pic33e/trap.h"
#include "lib_pic33e/version.h"
#include "shared/fifo/fifo.h"
#include "shared/scheduler/scheduler.h"
#include <xc.h>

#include "acceleration.h"
#include "car.h"
#include "iib.h"
#include "io.h"
#include "safety.h"
#include "update_can.h"

/// Drive modes
#define ALL_WHEEL_DRIVE 1
#define TWO_WHEEL_DRIVE_REAR 2
#define TWO_WHEEL_DRIVE_FRONT 3
#define ISOLATE_FL 4
#define ISOLATE_FR 5
#define ISOLATE_RL 6
#define ISOLATE_RR 7

#define DRIVE_MODE ALL_WHEEL_DRIVE /// Default mode

volatile uint16_t iib_tasks_timer;
volatile uint16_t non_priority_tasks_timer;

/*
 * wtd_ext_input is the watchdog for missing messages from external input
 * wtd_torque_encoder is the watchdog for missing messages from TE
 * good_arm_msg_timer is the wathcdog for missing "good condition "arm messages
 */
uint32_t wtd_ext_input;
uint32_t wtd_isa;
uint32_t wtd_torque_encoder;
uint32_t wtd_lap_count;
uint32_t wtd_steer;
uint32_t wtd_revive_steer;
uint32_t good_arm_msg_timer;
uint32_t axil_error[2] = { 0 };
uint32_t axil_error_time[2] = { 0 };
uint32_t time;
uint16_t line;
uint16_t flash[IIB_PARAMETER_NUMBER];

Fifo _can_fifo;
Fifo* can_fifo = &_can_fifo;

static uint32_t param[CFG_IIB_SIZE] = { [0 ... CFG_IIB_SIZE - 1] = 0xFFFFFFFF };

/// Timer for when TE returns to life (to exit error mode to Default mode)
uint32_t te_alive_timer;

uint16_t dev_get_id()
{
    return DEVICE_ID_IIB;
}

void dev_send_msg(CANdata msg)
{
    append_fifo(can_fifo, msg);

    return;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{
    multiple_return_t arg = { 0 };

    if (id == CMD_IIB_ADD) {
        arg.arg1 = arg1 + arg2;
    } else if (id == CMD_IIB_SET_REGEN_ON) {
        *(iib.regen_on) = arg1;
    } else if (id == CMD_IIB_SAVE_FLASH) {
        arg.arg1 = write_flash(flash, 0, IIB_PARAMETER_NUMBER);
    } else if (id == CMD_IIB_EXT_INPUT0 && *(iib.input_mode) == EXTERNAL_INPUT_MODE) {
        iib.dsr[MOTOR_FL].t_p = arg1;
        iib.dsr[MOTOR_FL].t_n = (int16_t)-1 * arg2;
        iib.dsr[MOTOR_FL].rpm = arg3;
        wtd_ext_input = 0;
    } else if (id == CMD_IIB_EXT_INPUT1 && *(iib.input_mode) == EXTERNAL_INPUT_MODE) {
        iib.dsr[MOTOR_FR].t_p = arg1;
        iib.dsr[MOTOR_FR].t_n = (int16_t)-1 * arg2;
        iib.dsr[MOTOR_FR].rpm = arg3;
        wtd_ext_input = 0;
    } else if (id == CMD_IIB_EXT_INPUT2 && *(iib.input_mode) == EXTERNAL_INPUT_MODE) {
        iib.dsr[MOTOR_RL].t_p = arg1;
        iib.dsr[MOTOR_RL].t_n = (int16_t)-1 * arg2;
        iib.dsr[MOTOR_RL].rpm = arg3;
        wtd_ext_input = 0;
    } else if (id == CMD_IIB_EXT_INPUT3 && *(iib.input_mode) == EXTERNAL_INPUT_MODE) {
        iib.dsr[MOTOR_RR].t_p = arg1;
        iib.dsr[MOTOR_RR].t_n = (int16_t)-1 * arg2;
        iib.dsr[MOTOR_RR].rpm = arg3;
        wtd_ext_input = 0;
    } else if (id == CMD_IIB_INPUT_MODE) {
        *(iib.input_mode) = arg1;
        if (arg2 == 0) {
            send_can2(logD(LOG_IIB_WTD_EXT, 0, 0, 0));
            iib.ext_wtd_limit = 0xFFFFFFFF;
        } else {
            iib.ext_wtd_limit = arg2;
            wtd_ext_input = 0;
        }
    } else if (id == CMD_IIB_SET_PL) {
        *(iib.pl_on) = arg1;
    } else if (id == CMD_IIB_LAP_COUNT) {
        if (wtd_lap_count > 4000) {
            endurance_mode();
            wtd_lap_count = 0;
        }
    }

    return arg;
}

void cfg_set_callback(uint32_t* table)
{
    can_t* can = get_can();
    uint16_t i;

    can->iib.iib_info.regen_on = table[1];
    can->iib.iib_limits.max_op_torque[0] = table[2];
    can->iib.iib_limits.max_op_torque[1] = table[3];
    can->iib.iib_limits.max_op_torque[2] = table[4];
    can->iib.iib_limits.max_op_torque[3] = table[5];
    can->iib.iib_limits.max_op_power[0] = table[6];
    can->iib.iib_limits.max_op_power[1] = table[7];
    can->iib.iib_limits.max_op_power[2] = table[8];
    can->iib.iib_limits.max_op_power[3] = table[9];
    can->iib.iib_info.iib_debug_mode = table[10];
    can->iib.iib_car.iib_input_mode = table[11];
    can->iib.iib_info.max_sfty_rpm = table[12];
    can->iib.iib_regen_limits.regen_max_sfty_torque[0] = table[13];
    can->iib.iib_regen_limits.regen_max_sfty_torque[1] = table[14];
    can->iib.iib_regen_limits.regen_max_sfty_torque[2] = table[15];
    can->iib.iib_regen_limits.regen_max_sfty_torque[3] = table[16];
    // can->iib.iib_car.amk_control_mode = table[17];
    can->iib.iib_regen_limits.regen_max_sfty_pwr[0] = table[18];
    can->iib.iib_regen_limits.regen_max_sfty_pwr[1] = table[19];
    can->iib.iib_regen_limits.regen_max_sfty_pwr[2] = table[20];
    can->iib.iib_regen_limits.regen_max_sfty_pwr[3] = table[21];
    can->iib.iib_info.soc_derrating_on = table[30];
    can->iib.iib_info.pl_on = table[22];

    for (i = 0; i < IIB_PARAMETER_NUMBER; i++)
        flash[i] = table[i];

    return;
}

void fifo_dispatch()
{
    uint16_t i;

    for (i = 0; !fifo_empty(can_fifo) && can2_tx_available() > 0; i++)
        send_can2(*pop_fifo(can_fifo));

    return;
}

void init_params()
{
    uint16_t i;

    for (i = 0; i < CFG_IIB_SIZE; i++) {
        iib.sets[i].p = NULL;
        iib.sets[i].max = 65535;
        iib.sets[i].min = 0;
        iib.sets[i].dflt = 0;
    }

    param[0] = VERSION;

    iib.regen_on = (uint16_t*)&param[1];
    iib.sets[1].max = 1;
    iib.sets[1].dflt = 1;

    iib.iib_safety.inv_lim[0].max_op_t = (uint16_t*)&param[2];
    iib.sets[2].max = MAX_MOTOR_TORQUE_SAFETY_FL;
    iib.sets[2].dflt = 3000;

    iib.iib_safety.inv_lim[1].max_op_t = (uint16_t*)&param[3];
    iib.sets[3].max = MAX_MOTOR_TORQUE_SAFETY_FR;
    iib.sets[3].dflt = 3000;

    iib.iib_safety.inv_lim[2].max_op_t = (uint16_t*)&param[4];
    iib.sets[4].max = MAX_MOTOR_TORQUE_SAFETY_RL;
    iib.sets[4].dflt = 6000;

    iib.iib_safety.inv_lim[3].max_op_t = (uint16_t*)&param[5];
    iib.sets[5].max = MAX_MOTOR_TORQUE_SAFETY_RR;
    iib.sets[5].dflt = 6000;

    iib.iib_safety.inv_lim[0].max_op_pwr = (uint16_t*)&param[6];
    iib.sets[6].max = MAX_MOTOR_POWER_SAFETY_FL;
    iib.sets[6].dflt = 10000;

    iib.iib_safety.inv_lim[1].max_op_pwr = (uint16_t*)&param[7];
    iib.sets[7].max = MAX_MOTOR_POWER_SAFETY_FR;
    iib.sets[7].dflt = 10000;

    iib.iib_safety.inv_lim[2].max_op_pwr = (uint16_t*)&param[8];
    iib.sets[8].max = MAX_MOTOR_POWER_SAFETY_RL;
    iib.sets[8].dflt = 20000;

    iib.iib_safety.inv_lim[3].max_op_pwr = (uint16_t*)&param[9];
    iib.sets[9].max = MAX_MOTOR_POWER_SAFETY_RR;
    iib.sets[9].dflt = 20000;

    iib.debug_mode = (uint16_t*)&param[10];
    iib.sets[10].dflt = 1;

    iib.input_mode = (uint16_t*)&param[11];
    iib.sets[11].max = 4;
    iib.sets[11].dflt = DEFAULT_MODE;

    iib.iib_safety.max_rpm = (uint16_t*)&param[12];
    iib.sets[12].max = MAX_RPM;
    iib.sets[12].dflt = DEFAULT_RPM;

    iib.iib_safety.inv_lim[0].max_op_reg_t = (uint16_t*)&param[13];
    iib.sets[13].max = MAX_MOTOR_REGEN_TORQUE_SAFETY_FL;
    iib.sets[13].dflt = 9800;

    iib.iib_safety.inv_lim[1].max_op_reg_t = (uint16_t*)&param[14];
    iib.sets[14].max = MAX_MOTOR_REGEN_TORQUE_SAFETY_FR;
    iib.sets[14].dflt = 9800;

    iib.iib_safety.inv_lim[2].max_op_reg_t = (uint16_t*)&param[15];
    iib.sets[15].max = MAX_MOTOR_REGEN_TORQUE_SAFETY_RL;
    iib.sets[15].dflt = 4200;

    iib.iib_safety.inv_lim[3].max_op_reg_t = (uint16_t*)&param[16];
    iib.sets[16].max = MAX_MOTOR_REGEN_TORQUE_SAFETY_RR;
    iib.sets[16].dflt = 4200;

    iib.amk_control_mode = (uint16_t*)&param[17];
    iib.sets[17].max = 1;
    iib.sets[17].dflt = 1;

    iib.iib_safety.inv_lim[0].max_op_reg_pwr = (uint16_t*)&param[18];
    iib.sets[18].min = 0;
    iib.sets[18].max = MAX_MOTOR_POWER_SAFETY_FL;
    iib.sets[18].dflt = 12600;

    iib.iib_safety.inv_lim[1].max_op_reg_pwr = (uint16_t*)&param[19];
    iib.sets[19].min = 0;
    iib.sets[19].max = MAX_MOTOR_POWER_SAFETY_FR;
    iib.sets[19].dflt = 12600;

    iib.iib_safety.inv_lim[2].max_op_reg_pwr = (uint16_t*)&param[20];
    iib.sets[20].min = 0;
    iib.sets[20].max = MAX_MOTOR_POWER_SAFETY_RL;
    iib.sets[20].dflt = 5400;

    iib.iib_safety.inv_lim[3].max_op_reg_pwr = (uint16_t*)&param[21];
    iib.sets[21].min = 0;
    iib.sets[21].max = MAX_MOTOR_POWER_SAFETY_RR;
    iib.sets[21].dflt = 5400;

    iib.pl_on = (uint8_t*)&param[22];
    iib.sets[22].min = 0;
    iib.sets[22].max = 1;
    iib.sets[22].dflt = 0;

    iib.iib_safety.max_pwr = (uint8_t*)&param[23];
    iib.sets[23].min = 0;
    iib.sets[23].max = MAX_TOTAL_POWER;
    iib.sets[23].dflt = DFT_TOTAL_POWER;

    iib.iib_safety.max_regen_pwr = (uint8_t*)&param[24];
    iib.sets[24].min = -MAX_REGEN_TOTAL_POWER;
    iib.sets[24].max = 0;
    iib.sets[24].dflt = DFT_REGEN_TOTAL_POWER;

    iib.iib_safety.min_drt_soc = (uint8_t*)&param[25];
    iib.sets[25].max = MIN_POWER_DRT_SOC;
    iib.sets[25].dflt = DFT_MIN_POWER_DRT_SOC;

    iib.iib_safety.max_drt_soc = (uint8_t*)&param[26];
    iib.sets[26].max = MAX_POWER_DRT_SOC;
    iib.sets[26].dflt = DFT_MAX_POWER_DRT_SOC;

    iib.endurance_cycles = (uint8_t*)&param[27];
    iib.sets[27].max = ENDURANCE_LAPS * SECTORS;
    iib.sets[27].dflt = ENDURANCE_LAPS;

    iib.final_soc = (uint8_t*)&param[28];
    iib.sets[28].max = MAX_FINAL_SOC;
    iib.sets[28].dflt = DFT_FINAL_SOC;

    iib.diff_gain = (uint16_t*)&param[29];
    iib.sets[29].max = 1000;
    iib.sets[29].dflt = 200;

    iib.soc_derrating_on = (uint8_t*)&param[30];
    iib.sets[30].max = 1;
    iib.sets[30].dflt = 0;

    return;
}
/**
 *	Name: init_safety;
 *	Description: Inits safety parameters
 *	Input's: IIB structure
 *	Output: void
 */
void init_safety()
{
    int i = 0;

    for (i = 0; i < 4; i++) {
        /// temperature related derating start at 0
        iib.iib_safety.temp_status[i].word = 0;

        /// i2t parameters
        iib.iib_safety.max_i2t_current[i] = 0;
        iib.iib_safety.i2t_saturation_time[i] = 0.0;
        iib.iib_safety.cool_down_on_time[i] = 0.0;

        iib.iib_safety.i2t_volume[i] = 0.0;
        iib.iib_safety.previous_i2t_current[i] = 0;
        iib.iib_safety.error_latch[i] = 0;
    }

    /// FL
    iib.iib_safety.inv_lim[0].max_sfty_pwr = MAX_MOTOR_POWER_SAFETY_FL;
    *(iib.iib_safety.inv_lim[0].max_op_pwr) = MAX_MOTOR_POWER_OP_FL;
    iib.iib_safety.inv_lim[0].max_sfty_t = MAX_MOTOR_TORQUE_SAFETY_FL;
    *(iib.iib_safety.inv_lim[0].max_op_t) = MAX_MOTOR_TORQUE_OP_FL;
    iib.iib_safety.inv_lim[0].max_sfty_reg_t = MAX_MOTOR_REGEN_TORQUE_SAFETY_FL;
    *(iib.iib_safety.inv_lim[0].max_op_reg_t) = MAX_MOTOR_REGEN_TORQUE_OP_FL;

    /// FR
    iib.iib_safety.inv_lim[1].max_sfty_pwr = MAX_MOTOR_POWER_SAFETY_FR;
    *(iib.iib_safety.inv_lim[1].max_op_pwr) = MAX_MOTOR_POWER_OP_FR;
    iib.iib_safety.inv_lim[1].max_sfty_t = MAX_MOTOR_TORQUE_SAFETY_FR;
    *(iib.iib_safety.inv_lim[1].max_op_t) = MAX_MOTOR_TORQUE_OP_FR;
    iib.iib_safety.inv_lim[1].max_sfty_reg_t = MAX_MOTOR_REGEN_TORQUE_SAFETY_FR;
    *(iib.iib_safety.inv_lim[1].max_op_reg_t) = MAX_MOTOR_REGEN_TORQUE_OP_FR;

    /// RL
    iib.iib_safety.inv_lim[2].max_sfty_pwr = MAX_MOTOR_POWER_SAFETY_RL;
    *(iib.iib_safety.inv_lim[2].max_op_pwr) = MAX_MOTOR_POWER_OP_RL;
    iib.iib_safety.inv_lim[2].max_sfty_t = MAX_MOTOR_TORQUE_SAFETY_RL;
    *(iib.iib_safety.inv_lim[2].max_op_t) = MAX_MOTOR_TORQUE_OP_RL;
    iib.iib_safety.inv_lim[2].max_sfty_reg_t = MAX_MOTOR_REGEN_TORQUE_SAFETY_RL;
    *(iib.iib_safety.inv_lim[2].max_op_reg_t) = MAX_MOTOR_REGEN_TORQUE_OP_RL;

    /// RR
    iib.iib_safety.inv_lim[3].max_sfty_pwr = MAX_MOTOR_POWER_SAFETY_RR;
    *(iib.iib_safety.inv_lim[3].max_op_pwr) = MAX_MOTOR_POWER_OP_RR;
    iib.iib_safety.inv_lim[3].max_sfty_t = MAX_MOTOR_TORQUE_SAFETY_RR;
    *(iib.iib_safety.inv_lim[3].max_op_t) = MAX_MOTOR_TORQUE_OP_RR;
    iib.iib_safety.inv_lim[3].max_sfty_reg_t = MAX_MOTOR_REGEN_TORQUE_SAFETY_RR;
    *(iib.iib_safety.inv_lim[3].max_op_reg_t) = MAX_MOTOR_REGEN_TORQUE_OP_RR;

    /// Speed and I2t volume initialization
    *(iib.iib_safety.max_rpm) = DEFAULT_RPM; // MAX_RPM;
    iib.iib_safety.i2t_trigger_volume = I2T_VOLUME;
    *(iib.iib_safety.max_pwr) = DFT_TOTAL_POWER;
    *(iib.iib_safety.max_regen_pwr) = DFT_REGEN_TOTAL_POWER;
    *(iib.iib_safety.max_drt_soc) = DFT_MAX_POWER_DRT_SOC;
    *(iib.iib_safety.min_drt_soc) = DFT_MIN_POWER_DRT_SOC;

    update_iib_info();

    // Save info on can
    for (i = 0; i < N_MOTOR; i++) {
        update_iib_limits(i);
        update_iib_regen_limits(i);
        update_iib_debug1_info(i);
        update_iib_inv(i);
    }

    return;
}
/**
 *	Name: init_inverter;
 *	Description: Inits inverter parameters
 *	Input's: iib_t structure
 *	Output: void
 */
void init_inverter()
{
    int i = 0;
    /// Message id's

    /// FL
    iib.inv[0].node_address = NODE_FL;
    iib.inv[0].message1_id = BASE_ADRESS_ACTUAL_V1 + NODE_FL;
    iib.inv[0].message2_id = BASE_ADRESS_ACTUAL_V2 + NODE_FL;
    iib.inv[0].reference_id = BASE_ADRESS_SETPOINTS + NODE_FL;
    /// FR
    iib.inv[1].node_address = NODE_FR;
    iib.inv[1].message1_id = BASE_ADRESS_ACTUAL_V1 + NODE_FR;
    iib.inv[1].message2_id = BASE_ADRESS_ACTUAL_V2 + NODE_FR;
    iib.inv[1].reference_id = BASE_ADRESS_SETPOINTS + NODE_FR;
    /// RL
    iib.inv[2].node_address = NODE_RL;
    iib.inv[2].message1_id = BASE_ADRESS_ACTUAL_V1 + NODE_RL;
    iib.inv[2].message2_id = BASE_ADRESS_ACTUAL_V2 + NODE_RL;
    iib.inv[2].reference_id = BASE_ADRESS_SETPOINTS + NODE_RL;
    /// RR
    iib.inv[3].node_address = NODE_RR;
    iib.inv[3].message1_id = BASE_ADRESS_ACTUAL_V1 + NODE_RR;
    iib.inv[3].message2_id = BASE_ADRESS_ACTUAL_V2 + NODE_RR;
    iib.inv[3].reference_id = BASE_ADRESS_SETPOINTS + NODE_RR;

    for (i = 0; i < 4; i++) {
        /// Inverters
        iib.inv[i].temp_inverter = 0;
        iib.inv[i].temp_IGBT = 0;
        iib.inv[i].error_info = 0;
        iib.inv[i].BE_2 = 0;
        iib.inv[i].BE_1 = 0;
        iib.inv[i].inverter_active = 0;
        /// Moto
        iib.inv[i].actual_speed = 0;
        iib.inv[i].torque_current = 0;
        iib.inv[i].magn_curr = 0;
        iib.inv[i].torque = 0;
        iib.inv[i].current = 0;
        iib.inv[i].temp_motor = 0;
        iib.inv[i].status.word = 0;
        /// Setp
        iib.inv[i].setp.rpm = 0;
        iib.inv[i].setp.t_p = 0;
        iib.inv[i].setp.t_n = 0;
        iib.inv[i].setp.ctrl.word = 0;
        // DSR Setp
        iib.dsr[i].rpm = 0;
        iib.dsr[i].t_p = 0;
        iib.dsr[i].t_n = 0;
    }

    /// Decide drive mode
    if (DRIVE_MODE == ALL_WHEEL_DRIVE)
        for (i = 0; i < N_MOTOR; i++)
            iib.inv[i].inverter_active = 1;

    if (DRIVE_MODE == TWO_WHEEL_DRIVE_REAR) {
        iib.inv[2].inverter_active = 1;
        iib.inv[3].inverter_active = 1;
    }

    if (DRIVE_MODE == TWO_WHEEL_DRIVE_FRONT) {
        iib.inv[0].inverter_active = 1;
        iib.inv[1].inverter_active = 1;
    }

    if (DRIVE_MODE == ISOLATE_FL)
        iib.inv[0].inverter_active = 1;

    if (DRIVE_MODE == ISOLATE_FR)
        iib.inv[1].inverter_active = 1;

    if (DRIVE_MODE == ISOLATE_RL)
        iib.inv[2].inverter_active = 1;

    if (DRIVE_MODE == ISOLATE_RR)
        iib.inv[3].inverter_active = 1;

    for (i = 0; i < N_MOTOR; i++) { // update CAN info
        update_iib_debug1_info(i);
        update_iib_motor(i);
        update_iib_inv(i);
        update_iib_debug2_info(i);
    }

    return;
}
/**
 *	Name: init_car;
 *	Description: Inits car parameters
 *	Input's: iib_t structure
 *	Output: void
 */
void init_car()
{
    /// CAR status
    iib.car.status.word = 0;

    /// CAR parameters
    iib.car.apps = 0;
    iib.car.brake = 0;
    iib.car.bms_voltage = 0;
    iib.car.soc = 0;
    iib.car.te_alive_again = 0;

    return;
}
/**
 *	Name: init_iib;
 *	Description: init all iib parameters, output stages etc...
 *	Input's: iib_t structure
 *	Output: void
 */
void init_iib()
{
    uint16_t i;
    // Never remove this, prevents resets
    INTCON1bits.NSTDIS = 1;

    /// Init iib parameters

    init_params();
    iib.air_temp = 0;
    iib.discharge_temp = 0;
    iib.fan_speed = 0;
    iib.fan_on = 0;
    iib.cu_power_a = 0;
    iib.cu_power_b = 0;
    iib.inv_rtd = 0;
    iib.inv_on = 0;
    iib.turning_on = 0;
    iib.turning_off = 0;
    iib.mission_mode = DFT_MISSION_MODE;
    *(iib.regen_on) = 1;
    *(iib.pl_on) = 0;
    iib.allow_reset_errors = 1;
    iib.ext_wtd_limit = 50;
    iib.arm_message_number = 0;
    iib.car.status.etas_stupid = 0;

    line = 391;

    /// Initial ARM mode(ARM updates this)
    iib.arm_input_mode = 0;

    *(iib.debug_mode) = 1;
    *(iib.amk_control_mode) = CONTROL_MODE;
    *(iib.diff_gain) = 200;

    /// Config io pins
    io_config();

    /// Unlock CAN pins
    PPSUnLock;
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
    PPSOutput(OUT_FN_PPS_C2TX, OUT_PIN_PPS_RP120);
    PPSInput(IN_FN_PPS_C2RX, IN_PIN_PPS_RPI121);
    PPSLock;

    config_can1(NULL);
    config_can2(NULL);

    config_timer1(1, 5); // choose time and priority (1ms)
    // config_timer2(400,5); //choose time and priority (10ms)
    // config_timer3(250,6); //chosse time and priority (100ms)

    init_safety();
    init_inverter();
    init_car();
    line = 438;

    /** Reads all parameters stored in flash */
    // iib_flash_read(iib);

    /** Turn on control units power */
    EF_B_WRITE = 1;
    EF_A_WRITE = 1;
    iib.cu_power_a = EF_A_WRITE;
    iib.cu_power_b = EF_B_WRITE;

    /** Turn on individual inverters */

    /// INV FL
    if (iib.inv[0].inverter_active == 1) {
        BE2_INV1_WRITE = 1;
        BE1_INV1_WRITE = 1;
        iib.inv[0].BE_2 = BE2_INV1_WRITE;
        iib.inv[0].BE_1 = BE1_INV1_WRITE;
    }
    /// INV FR
    if (iib.inv[1].inverter_active == 1) {
        BE2_INV2_WRITE = 1;
        BE1_INV2_WRITE = 1;
        iib.inv[1].BE_2 = BE2_INV2_WRITE;
        iib.inv[1].BE_1 = BE1_INV2_WRITE;
    }
    /// INV RL
    if (iib.inv[2].inverter_active == 1) {
        BE2_INV3_WRITE = 1;
        BE1_INV3_WRITE = 1;
        iib.inv[2].BE_2 = BE2_INV3_WRITE;
        iib.inv[2].BE_1 = BE1_INV3_WRITE;
    }
    /// INV RR
    if (iib.inv[3].inverter_active == 1) {
        BE2_INV4_WRITE = 1;
        BE1_INV4_WRITE = 1;
        iib.inv[3].BE_2 = BE2_INV4_WRITE;
        iib.inv[3].BE_1 = BE1_INV4_WRITE;
    }

    /// Initial IIB mode
    *(iib.input_mode) = DEFAULT_MODE;
    wtd_ext_input = 0;
    wtd_lap_count = 0;
    wtd_isa = 0;

    read_flash(flash, 0, IIB_PARAMETER_NUMBER);

    for (i = 1; i < IIB_PARAMETER_NUMBER; i++) {
        if (flash[i] == 0xFFFF || flash[i] == 0x0 || flash[i] > iib.sets[i].max || flash[i] < iib.sets[i].min) {
            param[i] = iib.sets[i].dflt;
            flash[i] = iib.sets[i].dflt;
        } else
            param[i] = flash[i];
    }

    write_flash(flash, 0, IIB_PARAMETER_NUMBER);

    cfg_config(param, CFG_IIB_SIZE);

    return;
}

bool filter_can2(unsigned int sid)
{
    uint16_t dev_id = (sid)&0b11111;
    uint16_t msg_id = ((sid) >> 5) & 0b111111;

    if (dev_id == DEVICE_ID_MASTER && (msg_id == MSG_ID_MASTER_MASTER_STATUS || msg_id == MSG_ID_MASTER_MASTER_EM))
        return 1;
    if (dev_id == DEVICE_ID_DASH && (msg_id == MSG_ID_DASH_DASH_STATUS || msg_id == MSG_ID_DASH_DASH_SE))
        return 1;
    if (dev_id == DEVICE_ID_TE && msg_id == MSG_ID_TE_TE_MAIN)
        return 1;
    if (msg_id == MSG_ID_COMMON_REQ_SET || msg_id == MSG_ID_COMMON_REQ_GET || msg_id == MSG_ID_COMMON_SEND_CMD)
        return 1;
    if (msg_id == CMD_ID_COMMON_RTD_ON || msg_id == CMD_ID_COMMON_RTD_OFF)
        return 1;

    return 0;
}
/**
 *	Name: receive_messages;
 *	Description: See in wich can line we have a message
 *	Input's: iib_t structure
 *	Output: void
 */
void receive_messages(void)
{
    CANdata msg;

    /// Check for can1:
    if (!can1_rx_empty()) {
        msg = pop_can1();
        process_amk_message(msg);
    }

    /// Check for can2:
    if (!can2_rx_empty()) {
        msg = pop_can2();
        process_car_message(msg);
    }

    return;
}

/**
 *	Name: timer1_callback;
 *	Description: timer1 interrupt
 *	Input's: void
 *	Output: void
 */
void timer1_callback(void)
{
    time++;
    wtd_torque_encoder++;
    wtd_lap_count++;
    wtd_steer++;
    wtd_ext_input++;
    wtd_isa++;
    axil_error_time[0]++;
    axil_error_time[1]++;

    return;
}

int main(void)
{
    tasks_t iib_tasks[] = {
        { .period = 0, .func = verify_shut_circuit },
        { .period = 0, .func = verify_TSAL },
        { .period = 0, .func = fifo_dispatch },
        { .period = 0, .func = receive_messages },
        { .period = 10, .func = setp_config },
        { .period = 10, .func = ez_safety },
        { .period = 10, .func = send_to_amk }
    };

    can_t* can = get_can();
    CANdata msg[FIFO_SIZE];

    // Initialize
    fifo_init(can_fifo, msg, FIFO_SIZE);
    init_iib();

    send_can2(logI(LOG_RESET_MESSAGE, RCON, 0, 0)); /// Reset message, with RCON register

    scheduler_init(iib_tasks, sizeof(iib_tasks) / sizeof(tasks_t));
    update_iib_can(N_MOTOR);

    FAN_CONTROL_WRITE = 1;

    LATDbits.LATD1 = 1;
    LATDbits.LATD2 = 0;

    while (1) {
        scheduler(time);
        iib_send_msgs(can->iib, time);

        ClrWdt();
    }

    return 0;
}

void trap_handler(TrapType type)
{

    send_can2(logE(LOG_TRAP_HANDLER_TYPE, type, line, 0));
    LATDbits.LATD2 = 1;
    __delay_ms(1000);

    return;
}
