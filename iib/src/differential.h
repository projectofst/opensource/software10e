#ifndef DIFF_H
#define DIFF_H

#include <stdint.h>

float get_diff_curve_value(uint16_t motor, int16_t angle);

#endif
