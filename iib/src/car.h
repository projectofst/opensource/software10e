#ifndef __CAR1_H__
#define __CAR1_H__

#include "differential.h"
#include "iib.h"
#include "io.h"
#include "safety.h"
#include "update_can.h"

#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"
#include "lib_pic33e/flash.h"
#include "shared/rtd/rtd.h"

/// Who asked for turn-off Requests
#define WE_TURN_OFF 2
#define TURN_OFF_REQUEST 1

void turn_on_sequence(void);
void turn_off_sequence(int mode);
void handle_rtd_on_message(CANdata msg);
void save_dash(void);
void run_default_mode(void);
void run_reverse_mode(void);
void run_burnout_mode(void);
void setp_config(void);
void endurance_mode(void);
void save_TE(void);
void save_master(void);
void process_car_message(CANdata msg);

#endif
