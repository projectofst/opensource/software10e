#ifndef __UPDATE_CAN_H__
#define __UPDATE_CAN_H__

#include <stdint.h>

#include "iib.h"

can_t* get_can(void);
void update_iib_motor( uint8_t mux);
void update_iib_inv( uint8_t mux);
void update_iib_limits( uint8_t mux);
void update_iib_car(void);
void update_iib_info(void);
void update_iib_debug1_info( uint8_t mux);
void update_iib_debug2_info( uint8_t mux);
void update_iib_regen_limits( uint8_t mux);
void update_iib_amk_values1( uint8_t mux);
void update_iib_amk_values2( uint8_t mux);
void update_iib_debug_amk( uint8_t mux);
void update_iib_status(void);
void update_iib_can( uint8_t max);

#endif