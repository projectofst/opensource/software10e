#if 0
#include "ISA.h"
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/// Update isabelle current
void parse_ISA_current_message1(uint16_t data[4], int32_t *isa_current){
		
	uint32_t upper_word = 0, lower_word = 0;

	upper_word = data[2];
	upper_word = upper_word << 16;
	lower_word = data[1];

	*isa_current = (int32_t)(upper_word | lower_word);		/** [mA]    */
	return;
}

/// Update isabelle voltage 1
void parse_ISA_voltage1_message1(uint16_t data[4], int32_t *isa_voltage1){
	
	uint32_t upper_word = 0, lower_word = 0;

	upper_word = data[2];
	upper_word = upper_word << 16;
	lower_word = data[1];

	*isa_voltage1 = (int32_t)(upper_word | lower_word);		/** [mV]    */
	return;
}

/// Update isabelle voltage  
void parse_ISA_voltage2_message1(uint16_t data[4], int32_t *isa_voltage2){
	
	uint32_t upper_word = 0, lower_word = 0;

	upper_word = data[2];
	upper_word = upper_word << 16;
	lower_word = data[1];

	*isa_voltage2 = (int32_t)(upper_word | lower_word);		/** [mV]    */
	return;
}

/// Update isabelle voltage 3
void parse_ISA_voltage3_message1(uint16_t data[4], int32_t *isa_voltage3){
	
	uint32_t upper_word = 0, lower_word = 0;

	upper_word = data[2];
	upper_word = upper_word << 16;
	lower_word = data[1];

	*isa_voltage3 = (int32_t)(upper_word | lower_word);		/** [mV]    */
	return;
}

/// Update isabelle temperature
void parse_ISA_temperature_message1(uint16_t data[4], int32_t *isa_temperature){
	
	uint32_t upper_word = 0, lower_word = 0;

	upper_word = data[2];
	upper_word = upper_word << 16;
	lower_word = data[1];

	*isa_temperature = (int32_t)(upper_word | lower_word);		/** [0.1ºC] */
	return;
}

/// Update isabelle power
void parse_ISA_power_message1(uint16_t data[4], int32_t *isa_power){
	
	uint32_t upper_word = 0, lower_word = 0;

	upper_word = data[2];
	upper_word = upper_word << 16;
	lower_word = data[1];

	*isa_power = (int32_t)(upper_word | lower_word);		/** [W]     */
	return;
}

/// Update isabelle current count
void parse_ISA_current_counter_message1(uint16_t data[4], int32_t *isa_current_counter){
	
	uint32_t upper_word = 0, lower_word = 0;

	upper_word = data[2];
	upper_word = upper_word << 16;
	lower_word = data[1];

	*isa_current_counter = (int32_t)(upper_word | lower_word);		/** [As]    */
	return;
}

/// Update isabelle energy
void parse_ISA_energy_message1(uint16_t data[4], int32_t *isa_energy){
	
	uint32_t upper_word = 0, lower_word = 0;

	upper_word = data[2];
	upper_word = upper_word << 16;
	lower_word = data[1];

	*isa_energy = (int32_t)(upper_word | lower_word);		/** [Wh]    */
	return;
}


void parse_can_ISA1(CANdata message, ISA_CAN_data_32b *data){

	switch(message.msg_id){
		
		case MSG_ID_ISA_I: 	parse_ISA_current_message1(message.data, &(data->isa_current)); return;
		case MSG_ID_ISA_U1: parse_ISA_voltage1_message1(message.data, &(data->isa_voltage1)); return;
		case MSG_ID_ISA_U2: parse_ISA_voltage2_message1(message.data, &(data->isa_voltage2)); return;
		case MSG_ID_ISA_U3: parse_ISA_voltage3_message1(message.data, &(data->isa_voltage3)); return;
		case MSG_ID_ISA_T: 	parse_ISA_temperature_message1(message.data, &(data->isa_temperature)); return;
		case MSG_ID_ISA_W: 	parse_ISA_power_message1(message.data, &(data->isa_power)); return;
		case MSG_ID_ISA_As: parse_ISA_current_counter_message1(message.data, &(data->isa_current_counter)); return;
		case MSG_ID_ISA_Wh: parse_ISA_energy_message1(message.data, &(data->isa_energy)); return;

		default : return;
	}

	return;
}


CANdata compose_ISA_message(uint16_t dev_id, uint16_t msg_id , ISA_message msg){

	CANdata isa_msg;
	
	isa_msg.dev_id = dev_id;
    isa_msg.msg_id = msg_id;
    isa_msg.dlc = 8;

    isa_msg.data[0] = msg.DB1 << 8 | msg.DB0;
    isa_msg.data[1] = msg.DB3 << 8 | msg.DB2;
    isa_msg.data[2] = msg.DB5 << 8 | msg.DB4;
    isa_msg.data[3] = msg.DB7 << 8 | msg.DB6;

    return isa_msg;
}


CANdata set_can_id(uint8_t message, uint16_t dev_id, uint16_t msg_id, int ISA_number){

	uint16_t can_id = msg_id << 5 | dev_id;

	ISA_message msg;

	msg.DB0 = message;
	msg.DB1 = (can_id >> 8) & 0xFF;
	msg.DB2 = can_id & 0xFF;
	msg.DB3 = 0;
	msg.DB4 = 0;
	msg.DB5 = 0;
	msg.DB6 = 0;
	msg.DB7 = 0;
	
	if(ISA_number == 1){

		msg.DB3 = HIGH_BYTE_SN_ISA1;
		msg.DB4 = MIDH_BYTE_SN_ISA1;
		msg.DB5 = MIDL_BYTE_SN_ISA1;
		msg.DB6 = LOW_BYTE_SN_ISA1;
	}

	uint8_t DB7 = UNUSED;

	return compose_ISA_message(DEVICE_ID_ISA_CONFIG, MSG_COMMAND, msg);
}



int16_t convert_to_16bits(int32_t isa_value, int scale_factor){

	/* Checks if 32bit number conversion to 16bits is going to overflow */
	if(abs(isa_value/scale_factor) > 0x7FFF){

		if(isa_value < 0){

			return (int16_t)(-1*0x7FFF);
		
		}else{

			return (int16_t)(0x7FFF);
		}
	}

	/* Convert to 16 bits, update resolution */

	return (int16_t)(isa_value/scale_factor);


}

void update_EM_values(ISA_CAN_data_16b *ISA_data_16b, CANdata msg, ISA_CAN_data_32b ISA_data_32b){

	/* Verifies if the conversion will not overflow, if so limits the reading to the maximum value allowed */
	/* Maximum value is 7FFF when value is 16 bit int and FFFF when it is 16 bit unsigned */

	switch(msg.msg_id){
		
		case MSG_ID_ISA_I:  ISA_data_16b->current = convert_to_16bits(ISA_data_32b.isa_current, 10); 		 return;
		case MSG_ID_ISA_U1: ISA_data_16b->voltage1 = convert_to_16bits(ISA_data_32b.isa_voltage1, 10);  	 return;
		case MSG_ID_ISA_U2: ISA_data_16b->voltage2 = convert_to_16bits(ISA_data_32b.isa_voltage2, 10);  	 return;
		case MSG_ID_ISA_U3: ISA_data_16b->voltage3 = convert_to_16bits(ISA_data_32b.isa_voltage3, 10);  	 return;
		case MSG_ID_ISA_T: 	ISA_data_16b->temperature = convert_to_16bits(ISA_data_32b.isa_temperature, 10); return;
		case MSG_ID_ISA_W: 	ISA_data_16b->power = convert_to_16bits(ISA_data_32b.isa_power, 10);  			 return;
		case MSG_ID_ISA_As: ISA_data_16b->charge = convert_to_16bits(ISA_data_32b.isa_current_counter, 10);  return;
		case MSG_ID_ISA_Wh: ISA_data_16b->energy = convert_to_16bits(ISA_data_32b.isa_current_counter, 10);; return;

		default : return;
	}

	return;
}

#endif
