#ifndef _ENERGY_METER_H_
#define _ENERGY_METER_H_

#if 0
#include <stdint.h>

/* SET Comands, DB0*/
#define SET_CAN_ID_I    0x10
#define SET_CAN_ID_U1   0x11
#define SET_CAN_ID_U2   0x12
#define SET_CAN_ID_U3   0x13
#define SET_CAN_ID_T    0x14
#define SET_CAN_ID_W    0x15
#define SET_CAN_ID_As   0x16
#define SET_CAN_ID_Wh   0x17

#define SET_CAN_ID_Command  0x1D
#define SET_CAN_ID_Response 0x1F
#define SET_CONFIG_RESULT_I 0x20
#define SET_CONFIG_RESULT_U1 0x21
#define SET_CONFIG_RESULT_U2 0x22
#define SET_CONFIG_RESULT_U3 0x23
#define SET_CONFIG_RESULT_T 0x24
#define SET_CONFIG_RESULT_W 0x25
#define SET_CONFIG_RESULT_As 0x26
#define SET_CONFIG_RESULT_Wh 0x27


#define DEVICE_ID_ISA  		   29
#define DEVICE_ID_ISA_CONFIG   30

#define MSG_COMMAND     1
#define MSG_RESPONSE    2

/// ISA messages
#define MSG_ID_ISA_I        32
#define MSG_ID_ISA_U1       33
#define MSG_ID_ISA_U2       34
#define MSG_ID_ISA_U3       35
#define MSG_ID_ISA_T        36
#define MSG_ID_ISA_W        37
#define MSG_ID_ISA_As       38
#define MSG_ID_ISA_Wh       39

//#define MSG_ID_SET_ISA
//#define MSG_ID_GET_ISA

#define HIGH_BYTE_SN_ISA1 0x00
#define MIDH_BYTE_SN_ISA1 0x00
#define MIDL_BYTE_SN_ISA1 0x19
#define LOW_BYTE_SN_ISA1  0xCC

#define UNUSED  0x00

typedef struct{
    
    uint8_t DB0;
    uint8_t DB1;
    uint8_t DB2;
    uint8_t DB3;
    uint8_t DB4;
    uint8_t DB5;
    uint8_t DB6;
    uint8_t DB7;

}ISA_message;

typedef struct{

    uint8_t high_byte;
    uint8_t mid_high_byte;
    uint8_t mid_lower_byte;
    uint8_t low_byte;
    
}SERIAL_NUMBER;

typedef struct{

    int32_t isa_current;            /** [mA]    */
    int32_t isa_voltage1;           /** [mV]    */
    int32_t isa_voltage2;           /** [mV]    */
    int32_t isa_voltage3;           /** [mV]    */
    int32_t isa_temperature;        /** [0.1ºC] */
    int32_t isa_power;              /** [W]     */
    int32_t isa_current_counter;    /** [As]    */
    int32_t isa_energy;             /** [Wh]    */

}ISA_CAN_data_32b;

typedef struct Energy_meter_data{

    int16_t current;
    int16_t voltage1;
    int16_t voltage2;
    int16_t voltage3;
    int16_t temperature;
    int16_t power;
    int16_t charge;
    int16_t energy;

    uint16_t state_of_charge;

}ISA_CAN_data_16b;


/// Parse functions
void parse_ISA_current_message1(uint16_t data[4], int32_t *isa_current);
void parse_ISA_voltage1_message1(uint16_t data[4], int32_t *isa_voltage1);
void parse_ISA_voltage2_message1(uint16_t data[4], int32_t *isa_voltage2);
void parse_ISA_voltage3_message1(uint16_t data[4], int32_t *isa_voltage3);
void parse_ISA_temperature_message1(uint16_t data[4], int32_t *isa_temperature);
void parse_ISA_power_message1(uint16_t data[4], int32_t *isa_power);
void parse_ISA_current_counter_message1(uint16_t data[4], int32_t *isa_current_counter);
void parse_ISA_energy_message1(uint16_t data[4], int32_t *isa_energy);

CANdata compose_ISA_message(uint16_t dev_id, uint16_t msg_id , ISA_message msg);
CANdata set_can_id(uint8_t message, uint16_t dev_id, uint16_t msg_id, int ISA_number);


/// General parse function
void parse_can_ISA1(CANdata message, ISA_CAN_data_32b *data);

int16_t convert_to_16bits(int32_t isa_value, int scale_factor);
void update_EM_values(ISA_CAN_data_16b *ISA_data_16b, CANdata msg, ISA_CAN_data_32b ISA_data_32b);

#endif
#endif
