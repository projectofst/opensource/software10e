#ifndef __MAT_H__
#define __MAT_H__

#define max(x,y) (x)>(y) ? (x) : (y)
#define max3(x) max(x[1], max(x[2], x[3]))
#define max4(x) max(x[0], max(x[1], max(x[2], x[3]))) 

#endif
