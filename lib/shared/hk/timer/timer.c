#include "timer.h"

hk_timer_t hk_timer_init()
{
    return (hk_timer_t) {
        .time = 0,
    };
}

/*! \brief Wait timer for <time> milliseconds.
 *
 * \param timer timer object.
 * \param time waiting time.
 */
void hk_timer_wait_ms(hk_timer_t* timer, time_ms_t time)
{
    if (timer == NULL) {
        return;
    }

    timer->time += time;
}

/*! \brief Get time for timer in milliseconds.
 *
 * \param timer timer object.
 * \returns Time counted in milliseconds
 */
time_ms_t hk_timer_get_ms(hk_timer_t* timer)
{
    if (timer == NULL) {
        return 0;
    }
    return timer->time;
}
