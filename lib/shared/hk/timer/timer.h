#ifndef ____TIMER_H_JKPZUEFR__
#define ____TIMER_H_JKPZUEFR__

#include <stdint.h>
#include <stdlib.h>

#include <types/types.h>

typedef struct _hk_timer_t {
	uint32_t time;
} hk_timer_t;


hk_timer_t hk_timer_init();
void hk_timer_wait_ms(hk_timer_t *timer, time_ms_t time);
time_ms_t hk_timer_get_ms(hk_timer_t * timer);

#endif /* end of include guard: ____TIMER_H_JKPZUEFR */

