#include "amk.h"

void parse_amk_actual_values_1(uint16_t data[4], AMK_Inverter* amk_values_1)
{

    amk_values_1->status.word = data[0];
    amk_values_1->actual_speed = data[1];
    amk_values_1->torque_current = data[2];
    amk_values_1->magn_curr = data[3];

    return;
}

void parse_amk_actual_values_2(uint16_t data[4], AMK_Inverter* amk_values_2)
{

    amk_values_2->temp_motor = data[0];
    amk_values_2->temp_inverter = data[1];
    amk_values_2->error_info = data[2];
    amk_values_2->temp_IGBT = data[3];

    return;
}
