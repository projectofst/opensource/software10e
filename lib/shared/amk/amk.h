#ifndef __AMK_H__
#define __AMK_H__

#include <stdint.h>
#include <stdbool.h>

#define MSG_ID_AMK_ACTUAL_VALUES_1  50
#define MSG_ID_AMK_ACTUAL_VALUES_2   51

/// Controll word for the inverters, contains all the status bits we want to modify
typedef union {
	struct {
		unsigned b_reserve          :8;	/** not used */
		unsigned b_inverter_on      :1;
		unsigned b_dc_on            :1;
		unsigned b_enable           :1;
		unsigned b_error_reset      :1;
		unsigned b_reserve1         :4;	/** not used */
	};
	uint16_t word;
}AMK_Control_word;


/// Status bit array that we recieve, 
/// Similar to the control word but only tells us the inverter status
typedef struct {
			
	union {

		struct {	
					
			unsigned reserved          :8;	/** not used */
			unsigned sys_ready      :1;
			unsigned err             :1;
			unsigned warn              :1;
			unsigned quit_dc_on        :1;
			unsigned dc_on             :1;	
			unsigned quit_inverter_on  :1;
			unsigned inverter_on       :1;
			unsigned derating          :1;	
		};
				
		uint16_t word;
	};

}AMK_status;


/// Represents all the parameters of the AMK setpoints message
typedef struct {
		
	AMK_Control_word ctrl;	

	int16_t rpm;				/** Speed we want to achieve */
	uint16_t t_p;		/** The torque we want the motor to produce (positive) */
	int16_t t_n;		/** The torque we want the motor to produce (negative) */
	
}AMK_Setpoints;
		
/// Represents one inverter in the car, contains info about the interaction with the motors and the car
typedef struct {
	
	/// Information about id's
	unsigned node_address;			
	unsigned message1_id;
	unsigned message2_id;
	unsigned reference_id;
		   
	/// About inverter
	int16_t temp_inverter;
	int16_t temp_IGBT;
	uint16_t error_info;
	unsigned BE_2    			  :1;
	unsigned BE_1 				  :1;
	unsigned inverter_active      :1;	/** if 1 we want the inverter on */

	/// About associated motor
	int16_t actual_speed;
    int16_t torque_current;
   	int16_t magn_curr;
	int16_t temp_motor;
	int16_t torque;
    int16_t current;

	AMK_status status;			/** Inverter status word */
	
	AMK_Setpoints setp;	/** Setpoints to the associated motor */

}AMK_Inverter;


void parse_amk_actual_values_1(uint16_t data[4], AMK_Inverter *amk_values_1);
void parse_amk_actual_values_2(uint16_t data[4], AMK_Inverter *amk_values_2);


#endif