#ifndef __RUN_AVG_H__
#define __RUN_AVG_H__

typedef struct _run_avg_t {
	uint32_t n;
	uint32_t max_size;
	uint16_t read;
	uint16_t write;
	double *buffer;
} run_avg_t;

void run_avg_config(run_avg_t *run_avg);
void run_avg_add(run_avg_t *run_avg, double value);
double run_avg_average(run_avg_t *run_avg);
#endif
