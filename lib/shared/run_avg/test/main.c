#include <stdio.h>

#include "munit.h"
#include "run_avg.h"

static MunitResult test_run_avg_config(const MunitParameter params[], void* user_data) {
	
	double buffer[16] = {0};
	run_avg_t run_avg = {.max_size = 16, .buffer=buffer};
	run_avg_config(&run_avg);

	run_avg_add(&run_avg, 10);

	munit_assert_double(run_avg_average(&run_avg), ==, 10);
	return MUNIT_OK;
}

static MunitResult test_run_avg_constant(const MunitParameter params[], void* user_data) {
	
	double buffer[16] = {0};
	run_avg_t run_avg = {.max_size = 16, .buffer=buffer};
	run_avg_config(&run_avg);
	
	for (int i=0; i<33; i++) {
		run_avg_add(&run_avg, 10);
	}
	
	munit_assert_double(run_avg_average(&run_avg), ==, 10);
	return MUNIT_OK;
}

static MunitResult test_run_avg_circular16(const MunitParameter params[], void* user_data) {
	
	double buffer[16] = {0};
	run_avg_t run_avg = {.max_size = 16, .buffer=buffer};
	run_avg_config(&run_avg);
	
	for (int i=0; i<33; i++) {
		run_avg_add(&run_avg, i);
	}
	
	munit_assert_double(run_avg_average(&run_avg), ==, 24.5);
	return MUNIT_OK;
}

static MunitTest test_suite_tests[] = {
  	{ (char*) "/run_avg/add/base", test_run_avg_config, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/run_avg/add/constant", test_run_avg_constant, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/run_avg/add/circular16", test_run_avg_circular16, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
  	(char*) "", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

#include <stdlib.h>

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {


  	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
