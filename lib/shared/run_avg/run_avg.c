#include <stdint.h>

#include "run_avg.h"

void run_avg_config(run_avg_t* run_avg)
{
    run_avg->write = 0;
    run_avg->n = 0;
    run_avg->read = run_avg->max_size - 1;
}

void run_avg_add(run_avg_t* run_avg, double value)
{

    run_avg->buffer[run_avg->write] = value;
    run_avg->write = (run_avg->write + 1) % run_avg->max_size;

    if (run_avg->n < run_avg->max_size) {
        run_avg->n++;
    }
}

double run_avg_average(run_avg_t* run_avg)
{
    double acc = 0;
    int i = 0;
    for (i = 0; i < run_avg->n; i++) {
        acc += run_avg->buffer[i];
    }

    return acc / run_avg->n;
}
