#include "scheduler.h"

tasks_t tasks[TASKS_N];
uint32_t t_last[TASKS_N] = { 0 };
uint16_t t_shift[TASKS_N] = { 0 };
uint16_t tasks_size;

void scheduler_init(tasks_t* init_tasks, uint16_t size)
{
    uint16_t i = 0;

    for (i = 0; i < size; i++) {
        init_tasks[i].args_on = 0;
        init_tasks[i].args = NULL;
    }

    memcpy(tasks, init_tasks, size * sizeof(tasks_t)); /** Copies the user's tasks array to the scheduler's tasks array */
    tasks_size = size; /** Saves the number of tasks defined by the user */

    return;
}

void scheduler_init_args(tasks_t* init_tasks, uint16_t size)
{

    memcpy(tasks, init_tasks, size * sizeof(tasks_t)); /** Copies the user's tasks array to the scheduler's tasks array */
    tasks_size = size; /** Saves the number of tasks defined by the user */

    return;
}

void scheduler_config_phase_shift(uint16_t* time_shift, uint16_t size)
{

    if (time_shift != NULL)
        memcpy(t_shift, time_shift, size * sizeof(uint16_t)); /** Copies the user's time shifts for the tasks to the time shift array */

    return;
}

void scheduler(uint32_t time_ms)
{
    uint16_t i;
    static uint32_t last_time = 0;

    if (last_time == time_ms) { /** Checks if the time_ms has changed */
        for (i = 0; i < tasks_size; i++) {
            if (tasks[i].period == 0) { /** if period is 0, does task everytime */
                if (tasks[i].args_on) {
                    tasks[i].func(tasks[i].args);
                } else {
                    tasks[i].func();
                }
            }
        }
        return;
    }

    last_time = time_ms; /** Saves the new different time */

    /** Goes through every task to check if the time passed is >= to the period and if the period is != 0 */
    for (i = 0; i < tasks_size; i++)
        if ((time_ms - t_last[i] >= tasks[i].period + t_shift[i]) && (tasks[i].period != 0)) {
            t_last[i] = time_ms; /** Saves the "new" last time that the task was executed */
            if (tasks[i].args_on) { /** Executes the task function */
                tasks[i].func(tasks[i].args);
            } else {
                tasks[i].func();
            }
            t_shift[i] = 0;
        }

    return;
}

uint16_t scheduler_get_size()
{
    return tasks_size;
}

int16_t scheduler_find_task(char* name)
{
    uint16_t i;

    for (i = 0; i < tasks_size; i++) {
        if (strcmp(name, tasks[i].name) == 0)
            return i;
    }

    return -1;
}

tasks_t* scheduler_find_task_struct(char* name)
{
    uint16_t i;

    for (i = 0; i < tasks_size; i++) {
        if (strcmp(name, tasks[i].name) == 0)
            return &tasks[i];
    }

    return NULL;
}

void scheduler_change_period(char* name, uint16_t period)
{
    int16_t task_id = scheduler_find_task(name);
    if (task_id == -1) {
        // TODO: logE(?);
        return;
    }

    tasks[task_id].period = period;
    return;
}
