#include <stdio.h>

#include "munit.h"
#include "../scheduler.h"

#define TIME_INTERVAL 40

extern tasks_t tasks[TASKS_N];

int flags[TASKS_N]={0};

void task0(){

	flags[0]++;

	return;
}

void task1(){

	flags[1]++;

	return;
}
void task2(){

	flags[2]++;

	return;
}
void task3(){

	flags[3]++;

	return;
}
static void *init_tasks(const MunitParameter params[], void* user_datat){
	tasks_t *main_tasks = (tasks_t *) malloc(sizeof(tasks_t)*TASKS_N);

	main_tasks[0].period=10; main_tasks[0].func=&task0;
	main_tasks[1].period=20; main_tasks[1].func=&task1;
	main_tasks[2].period=30; main_tasks[2].func=&task2;
	main_tasks[3].period=41; main_tasks[3].func=&task3;
	for (int i=0; i<4; i++)
		flags[i] = 0;

	return main_tasks;
}

static MunitResult scheduler_base_case(const MunitParameter params[], void* user_data) {

	scheduler(0);

	return MUNIT_OK;
}

static MunitResult scheduler_init_working(const MunitParameter params[], void* user_data) {
	tasks_t *main_tasks = (tasks_t *) user_data;
	scheduler_init(main_tasks,4);
	int n=scheduler_get_size();

	for (int i = 0; i<n; i++){
		munit_assert_int(main_tasks[i].period, ==, tasks[i].period);
		munit_assert_ptr(main_tasks[i].func, ==, tasks[i].func);
	}

	return MUNIT_OK;
}

static MunitResult scheduler_task_working(const MunitParameter params[], void* user_data) {
	tasks_t *main_tasks = (tasks_t *) user_data;

	scheduler_init(main_tasks,4);

	for (int t = 0; t <= TIME_INTERVAL; t++){
		scheduler(t);
	}

	for (int i = 0; i<scheduler_get_size(); i++)
		munit_assert_int(flags[i], ==, TIME_INTERVAL/tasks[i].period);

	return MUNIT_OK;
}

static MunitResult scheduler_task_repeat_ms(const MunitParameter params[], void* user_data) {
	tasks_t *main_tasks = (tasks_t *) user_data;
	int repeat_time = 20;

	scheduler_init(main_tasks,4);

	for (int t = 0; t <= repeat_time; t++)
		scheduler(repeat_time);

	for (int i = 0; i<scheduler_get_size(); i++)
		if(tasks[i].period==repeat_time)
			munit_assert_int(flags[i], ==, 1);

	return MUNIT_OK;
}

static MunitResult scheduler_phase_shift(const MunitParameter params[], void* user_data) {
	tasks_t *main_tasks = (tasks_t *) user_data;
	uint16_t time_shift[4] = {0};

	for (int i = 1; i < 4; i++)
		time_shift[i] = main_tasks[i].period/4;

	scheduler_config_phase_shift(time_shift, 4);
	scheduler_init(main_tasks,4);

	for (int t = 0; t <= TIME_INTERVAL; t++){
		scheduler(t);
	}
	for (int i = 0; i<scheduler_get_size(); i++) {
		munit_assert_int(flags[i], ==, (TIME_INTERVAL-time_shift[i])/tasks[i].period);
	}

	return MUNIT_OK;
}

static MunitResult scheduler_change_freq(const MunitParameter params[], void* user_data) {
	tasks_t *main_tasks = (tasks_t *) user_data;

	main_tasks[0].name = "n1";
	main_tasks[1].name = "n2";
	main_tasks[2].name = "n3";
	main_tasks[3].name = "n4";

	scheduler_init(main_tasks,4);

	scheduler_change_period("n1",100);
	munit_assert_int(tasks[0].period, ==, 100);

	scheduler_change_period("n2",150);
	munit_assert_int(tasks[1].period, ==, 150);

	scheduler_change_period("n3",180);
	munit_assert_int(tasks[2].period, ==, 180);

	scheduler_change_period("n4",200);
	munit_assert_int(tasks[3].period, ==, 200);

	return MUNIT_OK;
}

static MunitTest test_suite_tests[] = {
  	{ (char*) "/scheduler/base_case", scheduler_base_case, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
	{ (char*) "/scheduler/init_working", scheduler_init_working, init_tasks, NULL, MUNIT_TEST_OPTION_NONE, NULL },
	{ (char*) "/scheduler/task_working", scheduler_task_working, init_tasks, NULL, MUNIT_TEST_OPTION_NONE, NULL },
	{ (char*) "/scheduler/task_repeat_ms", scheduler_task_repeat_ms, init_tasks, NULL, MUNIT_TEST_OPTION_NONE, NULL },
	{ (char*) "/scheduler/task_phase_shift", scheduler_phase_shift, init_tasks, NULL, MUNIT_TEST_OPTION_NONE, NULL },
	{ (char*) "/scheduler/change_freq", scheduler_change_freq, init_tasks, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
  	(char*) "", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

#include <stdlib.h>

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {


  	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
