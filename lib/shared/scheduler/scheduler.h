/****************************************************************************
 * Copyright (C) 2019 by FST Lisboa                                         *
 *                                                                          *
 * This file is part of Scheduler.                                          *
 *                                                                          *
 *   Scheduler is a cooperative scheduler that organizes and executes tasks *
 *   in defined period (ms).                                                *
 *                                                                          *
 *   Scheduler is distributed in hope that it will be useful in executing   *
 *   functions with certain periods.                                        *
 ****************************************************************************/

/**
 * @brief Scheduler
 *
 * This scheduler uses a struct containing the task info
 * (period and function), and an auxiliary array with the
 * containing the last time that the task was executed.
 * The scheduler needs to receive time info to execute tasks,
 * and time must be configured with a period of ms to work
 * properly.
 * It is limited to 16 tasks, but the
 * code is prepared to increase or decrease the number of tasks limit.
 * Any advice or sugestion feel free to talk to me.
 * @file scheduler.h
 * @author André Correia
 * @date October 2019
 *
 *
 * @see software10e/shared/scheduler/html/index.html
 */

#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define TASKS_N 16  /**< TASKS_N is the defined value of the maximum number of tasks */

/**
 * @brief Structure w/ Tasks info
 *
 * Structure containing tasks period [ms] and the function (func())
 * to execute
 */
typedef struct _tasks_t{
    char *name;
    uint16_t period;    /**< Contains the period the task. If it's 0 it doesn't execute the task */
    void (*func)();     /**< Contains a pointer to the function of the task */
	bool args_on;
	void *args;
}tasks_t;

/**
 * @brief Description of scheduler_init()
 *
 * Saves the tasks info in the scheduler own tasks struct (omitting args), and saves the number
 * of tasks present in the struct
 *
 * @param init_tasks pointer to the tasks_t struct containing the tasks info
 * @param size unsigned 16 bits integer containing the number of tasks
 */
void scheduler_init(tasks_t *init_tasks, uint16_t size);
/**
 * @brief Description of scheduler_init_args()
 *
 * Saves the tasks info in the scheduler own tasks struct with args, and saves the number
 * of tasks present in the struct
 *
 * @param init_tasks pointer to the tasks_t struct containing the tasks info
 * @param size unsigned 16 bits integer containing the number of tasks
 */
void scheduler_init_args(tasks_t *init_tasks, uint16_t size);
/**
 * @brief Description of scheduler_config_phase_shift()
 *
 * Sets the initial time shifts for each task. It is not mandatory to run this
 * function. In case this function is not run it will use 0ms of phase shift for
 * every task.
 *
 * @param time_shift pointer to the vector of uint16_t containing all the time shifts
 * @param size unsigned 16 bits integer containing the number of tasks
 */
void scheduler_config_phase_shift(uint16_t *time_shift, uint16_t size);
/**
 * @brief Description of scheduler()
 *
 * Starts by verifying if the time_ms has changed. If it was changed saves the
 * data in last_time variable (static variable) and starts the verification
 * process. In that process every task is verified if the period has passed,
 * and if yes it saves the time in the last_time of that task and executes the
 * function.
 *
 * @param time_ms variable containing time [ms]
 */
void scheduler(uint32_t time_ms);
/**
 * @brief Description of scheduler_get_size()
 *
 * If by any chance the user needs to know the number of tasks he can use
 * this function.
 *
 * @return returns the size saved in the tasks_size
 */
uint16_t scheduler_get_size();
/**
 * @brief Description of scheduler_find_task()
 *
 * If by any chance the user needs to know the number of the task, he can use
 * this function.
 *
 * @param *name contains the name of the task 
 * @return returns the number of the task 
 */
int16_t scheduler_find_task(char *name);
tasks_t *scheduler_find_task_struct(char* name);

/**
 * @brief Description of scheduler_change_period()
 *
 * If by any chance the user needs to change the period of determined task, for state
 * machines or something, he can use this function
 *
 * @param *name contains the name of the task 
 * @param period contains the new desired period
 * @return returns the number of the task 
 */
void scheduler_change_period(char *name, uint16_t period);


#endif
