# Watchdog

## Watchdog

A software watchdog is a incrementing timer that errors out when the timer is
over a certain threshold.

To stop the timer from erroring out you must `pet` the watchdog, this is, the
timer will be cleared.

Watchdogs are usually used in the context of message timeouts.
Simply put, if you consider that after 100ms a message is stale you must no
longer use its values.

## Notable uses

* watchdog was initially used in CCU.

## Example usage

### Initialization

The watchdogs can be initialized with `wdt_init`:

```c
wdt_id_t example_id = {.dev_id = DEVICE_ID_EXAMPLE, .msg_id = MSG_ID_EXAMPLE};
wdt_t watchdogs[] = {
	{.id = example_id, .err_time = 1000},
}
wdt_init(sizeof(watchdogs)/sizeof(wdt_t));
```

`wdt_id_t` is the identifier of the watchdog, you will need it later when
checking the state of the watchdog or incrementing its timer.

### Petting the watchdog

`wdt_pet(wdt_t *)` and `wdt_pet_candata(CANdata)` can be used to `pet` the watchdog.

`wdt_pet` can be used when you hold the `wdt_t *` object.

`wdt_pet_candata` can be used after you receive a CAN message.

### Waiting the watchdog

`wdt_wait(wdt_t *, uint32_t)` is used to increment the timer. The second argument
is the time that passed from the last time you waited a watchdog. 

You must wait the watchdog frequently, around 10/100x times less than the time
to error.

### The watchdog state

The watchdog can either be `WDT_BAD` or `WDT_GOOD`. All watchdogs start as
`WDT_GOOD` and go `WDT_BAD` after the error timer is over. 

If the watchdog is petted it will not transition into WDT_GOOD. For that you
must explicitly `wdt_clear`.

To check the current state of the watchdog you may use wdt_get_state that
returns `wdt_state_t`. You may also use `wdt_is_bad` or `wdt_is_good`, both
returning a boolean.


