#include "watchdog.h"

wdt_t* watchdogs;
u16 watchdogs_size = 0;

wdt_id_t wdt_id_init(u16 dev_id, u16 msg_id)
{
    return (wdt_id_t) {
        .dev_id = dev_id,
        .msg_id = msg_id,
    };
}

bool wdt_id_any(u16 id)
{
    return id == WDT_ID_ANY;
}

/* clang-format: off */
bool wdt_id_eq(wdt_id_t id1, wdt_id_t id2)
{
    bool dev_id = wdt_id_any(id1.dev_id)
        || wdt_id_any(id2.dev_id)
        || (id1.dev_id == id2.dev_id);

    bool msg_id = wdt_id_any(id1.msg_id)
        || wdt_id_any(id2.msg_id)
        || (id1.msg_id == id2.msg_id);

    return dev_id && msg_id;
}
/* clang-format: on */

void wdt_init(wdt_t* _watchdogs, u16 size)
{
    watchdogs = _watchdogs;
    watchdogs_size = size;
    int i = 0;
    for (i = 0; i < watchdogs_size; i++) {
        watchdogs[i].time = 0;
        watchdogs[i].state = WDT_GOOD;
    }
}

wdt_t* wdt_find_id(wdt_id_t id)
{
    int i = 0;
    for (i = 0; i < watchdogs_size; i++) {
        if (wdt_id_eq(watchdogs[i].id, id)) {
            return &watchdogs[i];
        }
    }

    return NULL;
}

wdt_t* wdt_find_candata(CANdata msg)
{
    wdt_id_t id = wdt_id_init(msg.dev_id, msg.msg_id);
    return wdt_find_id(id);
}

wdt_t* wdt_find(wdt_id_t id)
{
    int i = 0;
    for (i = 0; i < watchdogs_size; i++) {
        if (wdt_id_eq(watchdogs[i].id, id)) {
            return &watchdogs[i];
        }
    }

    return NULL;
}

void wdt_pet(wdt_t* watchdog)
{
    if (watchdog == NULL) {
        return;
    }
    watchdog->time = 0;
}

void wdt_pet_candata(CANdata msg)
{
    wdt_t* watchdog = wdt_find_candata(msg);
    if (watchdog == NULL) {
        return;
    }
    watchdog->time = 0;
}

wdt_state_t wdt_wait(wdt_t* watchdog, time_ms_t time)
{
    if (watchdog == NULL) {
        return WDT_BAD;
    }

    watchdog->time += time;
    if (watchdog->time > watchdog->err_time) {
        watchdog->recovery_counter = 0;
        watchdog->state = WDT_BAD;
    } else if (watchdog->mode == WDT_RECOVER && wdt_is_bad(watchdog)) {
        watchdog->recovery_counter += time;
        if (watchdog->recovery_counter > watchdog->recovery_time) {
            watchdog->state = WDT_GOOD;
        }
    }

    return watchdog->state;
}

wdt_state_t wdt_wait_id(wdt_id_t id, time_ms_t time)
{
    wdt_t* watchdog = wdt_find_id(id);
    return wdt_wait(watchdog, time);
}

void wdt_reset(wdt_t* watchdog)
{
    if (watchdog == NULL) {
        return;
    }
    watchdog->state = WDT_GOOD;
}

wdt_state_t wdt_get_state(wdt_t* watchdog)
{
    if (watchdog == NULL) {
        return WDT_BAD;
    }
    return watchdog->state;
}

bool wdt_is_bad(wdt_t* watchdog)
{
    if (watchdog == NULL) {
        return true;
    }

    return wdt_get_state(watchdog) == WDT_BAD;
}

bool wdt_is_bad_id(wdt_id_t id)
{
    wdt_t* watchdog = wdt_find_id(id);
    if (watchdog == NULL) {
        return true;
    }

    return wdt_get_state(watchdog) == WDT_BAD;
}

bool wdt_is_good(wdt_t* watchdog)
{
    if (watchdog == NULL) {
        return false;
    }

    return wdt_get_state(watchdog) == WDT_GOOD;
}

bool wdt_is_good_id(wdt_id_t id)
{
    wdt_t* watchdog = wdt_find_id(id);
    if (watchdog == NULL) {
        return false;
    }

    return wdt_get_state(watchdog) == WDT_GOOD;
}
