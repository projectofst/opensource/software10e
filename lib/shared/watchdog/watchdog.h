/* 
 * watchdog is a library that provides utilities for management of software watchdog timers .
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SHARED_WDT_H__
#define __SHARED_WDT_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <types/types.h>

#ifndef _CANDATA
#define _CANDATA

#include <stdint.h>


/** @struct CANdata
 *  @brief CAN frame.
 */
typedef struct {
    union {
        struct {
            uint16_t dev_id : 5; // Least significant
            // First bit of msg_id determines if msg is reserved or not.
            // 0 == reserved (higher priority) (0-31 decimal)
            uint16_t msg_id : 6; // Most significant
        };
        uint16_t sid;
    };
    uint16_t dlc : 4;
    uint16_t data[4];
} CANdata;
#endif

typedef struct _wdt_id_t {
	uint16_t dev_id;
	uint16_t msg_id;
} wdt_id_t;

#define WDT_ID_ANY 0xFFFF

typedef enum {WDT_BAD, WDT_GOOD} wdt_state_t;

/*
 * ONESHOT - requires explicit reset
 * RECOVER - will recover after recovery time has passed
 */
typedef enum{WDT_ONESHOT, WDT_RECOVER} wdt_mode_t;

typedef struct _wdt_t {
	wdt_id_t id;
    wdt_mode_t mode;
	u32 err_time;
    u32 recovery_time;
	u32 time;
    u32 recovery_counter;
	wdt_state_t state;
} wdt_t;

wdt_id_t wdt_id_init(u16 dev_id, u16 msg_id);
bool wdt_id_eq(wdt_id_t id1, wdt_id_t id2);

void wdt_init(wdt_t *_watchdogs, u16 size);
wdt_t *wdt_find(wdt_id_t id);
wdt_t *wdt_CANdata(CANdata msg);
wdt_t* wdt_find_id(wdt_id_t id);
void wdt_pet(wdt_t *watchdog);
void wdt_pet_candata(CANdata msg);
wdt_state_t wdt_wait(wdt_t *watchdog, time_ms_t time);
wdt_state_t wdt_wait_id(wdt_id_t id, time_ms_t time);
void wdt_reset(wdt_t *watchdog);
wdt_state_t wdt_get_state(wdt_t *watchdog);
bool wdt_is_bad(wdt_t *watchdog);
bool wdt_is_bad_id(wdt_id_t id);
bool wdt_is_good(wdt_t *watchdog);
bool wdt_is_good_id(wdt_id_t id);

#endif
