#include <stdio.h>

#include "munit.h"
#include "pedals.h"

uint32_t time = 0;
uint32_t get_time_ms() {
	return time;
}

static MunitResult test_pedals_check_cc_bound_upper(const MunitParameter params[], void* user_data) {
	
	uint16_t upper_bound = 3200;
	uint16_t lower_bound = 800;
	for (uint16_t i=upper_bound+1; i<4096; i++) {
		munit_assert_uint16(pedals_check_cc_bound(i, lower_bound, upper_bound), ==, PEDALS_CC_UPPER);
	}

	return MUNIT_OK;
}

static MunitResult test_pedals_check_cc_bound_lower(const MunitParameter params[], void* user_data) {

	uint16_t upper_bound = 3200;
	uint16_t lower_bound = 800;
	for (uint16_t i=0; i<lower_bound; i++) {
		munit_assert_uint16(pedals_check_cc_bound(i, lower_bound, upper_bound), ==, PEDALS_CC_LOWER);
	}

	return MUNIT_OK;
}

static MunitResult test_pedals_check_cc_bound_false(const MunitParameter params[], void* user_data) {
	
	uint16_t upper_bound = 3200;
	uint16_t lower_bound = 800;
	for (int i=lower_bound+1; i<upper_bound; i++) {
		munit_assert_uint16(pedals_check_cc_bound(i, lower_bound, upper_bound), ==, PEDALS_CC_FALSE);
	}

	return MUNIT_OK;
}


static MunitResult test_pedals_saturate_sensor(const MunitParameter params[], void* user_data) {
	uint16_t lower_limit = 1000;
	uint16_t upper_limit = 3000;

	for (uint16_t i=0; i<4096; i++) {
		float res = pedals_saturate_sensor(i, lower_limit, upper_limit);
		munit_assert_float(res, <=, 100.0);
		munit_assert_float(res, >=, 0.0);
		if (i < lower_limit) {
			munit_assert_float(res,<=,0.01);
		}
		else if (i > upper_limit) {
			munit_assert_float(res,>=,99.0);
		}
	}

	float res = pedals_saturate_sensor(upper_limit-1, lower_limit, upper_limit);
	munit_assert_float(res, >=, 99.9);


	return MUNIT_OK;
}

static MunitResult test_pedals_check_apps_cc(const MunitParameter params[], void* user_data) {

	float result;
	bool faulty;
	float apps[2] = {100, 100};
	pedals_cc_t cc[2] = {PEDALS_CC_FALSE, PEDALS_CC_FALSE};

	for (int i=0; i<3; i++) {
		for (int j=0; j<3; j++) {
			cc[0] = i;
			cc[1] = j;

			pedals_check_apps(&result, &faulty, apps, cc);

			if (i==0 && j==0) {
				munit_assert_float(result, ==, 100.0);
				munit_assert_int(faulty, ==, false);
			}
			else {
				munit_assert_float(result, ==, 0.0);
				munit_assert_int(faulty, ==, true);
			}
		}
	}
	

	return MUNIT_OK;
}

static MunitResult test_pedals_check_apps_mean(const MunitParameter params[], void* user_data) {

	float result;
	bool faulty;
	float apps[2] = {100, 100};
	pedals_cc_t cc[2] = {PEDALS_CC_FALSE, PEDALS_CC_FALSE};
	

	for (float i=0; i < 100; i+=0.1) {
		for (float j=0; j < 100; j+=0.1) {
			apps[0] = i;
			apps[1] = j;
			pedals_check_apps(&result, &faulty, apps, cc);

			if (fabs(i - j) > 10) {
				munit_assert_float(result, ==, 0.0);
				munit_assert_int(faulty, ==, 1);
			}
			else {
				munit_assert_float(result, <=, 1.1*(apps[0] + apps[1])/2);
				munit_assert_float(result, >=, 0.9*(apps[0] + apps[1])/2);
				munit_assert_int(faulty, ==, 0);
			}
		}
	}

	return MUNIT_OK;
}

static MunitResult test_pedals_check_conv_pressure_base(const MunitParameter params[], void* user_data) {
	
	float result = 0;
	bool faulty = 0;

	float bps = 100;
	pedals_cc_t cc = PEDALS_CC_FALSE;

	pedals_conv_pressure(&result, &faulty, bps, cc, 4.5, 0, 450);

	munit_assert_int(result, ==, 450);

	bps = 50;
	pedals_conv_pressure(&result, &faulty, bps, cc, 4.5, 0, 450);
	munit_assert_int(result, ==, 450.0/2);
	return MUNIT_OK;
}

static MunitResult test_pedals_check_apps_brake_base(const MunitParameter params[], void* user_data) {

	bool result = 0;
	float apps = 0;
	float pressure = 0;
	result = pedals_check_apps_brake(apps, pressure);

	munit_assert_int(result, ==, 0);
	return MUNIT_OK;
}

static MunitResult test_pedals_check_apps_brake_trigger(const MunitParameter params[], void* user_data) {

	bool result = 0;
	float apps = 50;
	float pressure = 50;

	result = pedals_check_apps_brake(apps, pressure);
	munit_assert_int(result, ==, 0);
	time = 501;
	result = pedals_check_apps_brake(apps, pressure);
	munit_assert_int(result, ==, 1);

	return MUNIT_OK;
}


static MunitResult test_pedals_check_apps_brake_untrigger(const MunitParameter params[], void* user_data) {

	bool result = 0;

	// brake and accelerate at the same time
	float apps = 50;
	float pressure = 50;
	result = pedals_check_apps_brake(apps, pressure);
	munit_assert_int(result, ==, 0);

	//wait for implausability
	time = 501;
	result = pedals_check_apps_brake(apps, pressure);
	munit_assert_int(result, ==, 1);
	
	// wiggle accelerator
	apps = 4;
	time = 1010;
	result = pedals_check_apps_brake(apps, pressure);
	munit_assert_int(result, ==, 1);
	
	apps = 50;
	time = 1520;
	result = pedals_check_apps_brake(apps, pressure);
	munit_assert_int(result, ==, 1);
	
	// stop braking, stop accelerator
	apps = 0;
	pressure = 2;
	time = 1520;
	result = pedals_check_apps_brake(apps, pressure);
	munit_assert_int(result, ==, 0);
	
	//accelerator
	apps = 100;
	time = 2030;
	result = pedals_check_apps_brake(apps, pressure);
	munit_assert_int(result, ==, 0);


	return MUNIT_OK;
}


static MunitTest test_suite_tests[] = {
  	{ (char*) "/pedals/check_cc_bound/upper", test_pedals_check_cc_bound_upper, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/pedals/check_cc_bound/lower", test_pedals_check_cc_bound_lower, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/pedals/check_cc_bound/false", test_pedals_check_cc_bound_false, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/pedals/saturate_sensor/false", test_pedals_saturate_sensor, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/pedals/check_apps/cc", test_pedals_check_apps_cc, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/pedals/check_apps/mean", test_pedals_check_apps_mean, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/pedals/conv_pressure/base", test_pedals_check_conv_pressure_base, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/pedals/check_apps_brake/base", test_pedals_check_apps_brake_base, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/pedals/check_apps_brake/trigger", test_pedals_check_apps_brake_trigger, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ (char*) "/pedals/check_apps_brake/untrigger", test_pedals_check_apps_brake_untrigger, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
  	(char*) "", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

#include <stdlib.h>

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {


  	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
