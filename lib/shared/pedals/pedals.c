#include <math.h>
#include <stdbool.h>

/** @dir pedals
 * @brief something
 */

/** @file
 * @brief task file
 */

#include "pedals.h"

/**
 * @brief Check the short circuit bounds of a sensor reading.
 * Returns the state of the sensor:
 *		CC to GND: PEDALS_CC_LOWER
 *		CC to VCC: PEDALS_CC_UPPER
 *		No short circuit: PEDALS_CC_FALSE
 */
pedals_cc_t pedals_check_cc_bound(uint16_t value, uint16_t l_bound, uint16_t u_bound)
{
    if (value < l_bound) {
        return PEDALS_CC_LOWER;
    } else if (value > u_bound) {
        return PEDALS_CC_UPPER;
    } else {
        return PEDALS_CC_FALSE;
    }
}

/**
 * Applies pedals_check_cc_bound to an array of one reading per installed sensor.
 */
void pedals_check_cc_bounds(uint16_t* cc, uint16_t values[5], uint16_t l_bounds[5],
    uint16_t u_bounds[5])
{
    int i = 0;
    for (i = 0; i < 5; i++) {
        cc[i] = pedals_check_cc_bound(values[i], l_bounds[i], u_bounds[i]);
    }

    return;
}

/*
 * Saturate the value of a sensor reading. Also convert the readings to 0 to
 * 100% floating point, no scaling.
 */

float pedals_saturate_sensor(uint16_t value, uint16_t l_bound, uint16_t u_bound)
{
    uint16_t range = u_bound - l_bound;

    if (value > u_bound) {
        return 100 * ((u_bound - l_bound) * 1.0) / range;
    } else if (value < l_bound) {
        return 0.0;
    } else {
        return 100 * (value - l_bound) * 1.0 / range;
    }
}

/*
 * Apply pedals_saturate_sensor to an array of one sensor reading per installed sensor.
 */
void pedals_saturate_sensors(float* result, uint16_t values[5], uint16_t l_bounds[5],
    uint16_t u_bounds[5])
{
    int i = 0;
    for (i = 0; i < 5; i++) {
        result[i] = pedals_saturate_sensor(values[i], l_bounds[i], u_bounds[i]);
    }
}

/** Compute apps value from two independent sensor readings.
 * Output is percentage of pedal travel 0% to 100%, no scaling.
 * If one sensor is faulty the result is 0% APPS.
 */
void pedals_check_apps(float* result, bool* faulty, float apps[2], pedals_cc_t cc[2])
{
    *faulty = cc[0] || cc[1];

    if (*faulty) {
        *result = 0.0;
        return;
    }

    if (fabs(apps[0] - apps[1]) > 10.0) {
        *faulty = true;
        *result = 0.0;
        return;
    }

    *faulty = false;
    *result = (apps[0] + apps[1]) / 2;
    // printf("\nresult: %f\n", *result);
}

void pedals_conv_pressure(float* result, bool* faulty, float bps,
    pedals_cc_t cc, float scaling, float offset, float max)
{

    if (cc) {
        *faulty = true;
        *result = max;
        return;
    }

    *faulty = false;
    *result = scaling * (bps - offset);

    if (*result > max) {
        *result = max;
    }

    return;
}

/* check apps/brake rule. Returns True if the rule is broken */
bool pedals_check_apps_brake(float apps, float pressure)
{
    static uint32_t time_triggered = 0;
    static bool triggered = 0;

    bool implausible = apps > 25.0 && pressure > 30.0;

    // 11
    if (triggered && implausible) {
        if (get_time_ms() - time_triggered > 500) {
            return 1;
        } else {
            return 0;
        }
    }

    // 10
    if (triggered && !implausible) {
        if (apps < 5) {
            triggered = false;
            return 0;
        } else {
            return 1;
        }
    }

    // 01
    if (!triggered && implausible) {
        triggered = true;
        time_triggered = get_time_ms();
        return 0;
    }

    return 0;
}

bool pedals_hardbraking(float pressure, float limit)
{
    return pressure > limit;
}
