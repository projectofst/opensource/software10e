#ifndef __PEDALS_H__
#define __PEDALS_H__

/** @dir src
 * @brief something
 */

/** @file
 * @brief task file
 */
#include <stdint.h>

/// sensors status
typedef enum {
	PEDALS_CC_FALSE = 0, 
	PEDALS_CC_UPPER = 1, 
	PEDALS_CC_LOWER = 2
} pedals_cc_t;

#if 0
/* te_main */
typedef struct _msg_te_te_main_t {
    uint8_t te_status_cc_apps0;
    uint8_t te_status_cc_apps1;
    uint8_t te_status_cc_bpse0;
    uint8_t te_status_cc_bpsp0;
    uint8_t te_status_cc_bpsp1;
    uint8_t te_status_os_apps0;
    uint8_t te_status_os_apps1;
    uint8_t te_status_os_bpse;
    uint8_t te_status_imp_apps_bps_timer_exceeded;
    uint8_t te_status_hb;
    uint8_t te_status_sdc;
    uint8_t te_status_verbose;
    uint8_t te_status_imp_apps_bps;
    uint8_t te_os_bpsp0;
    uint16_t te_main_APPS;
    uint16_t te_main_BPSp;
    uint16_t te_main_BPSe;
    uint8_t te_status_imp_apps_timer_exceeded;
    uint8_t te_status_imp_apps;
} te_main_t;
#endif

//te_main_t check_plausibility(double *values);

pedals_cc_t pedals_check_cc_bound(uint16_t value, uint16_t l_bound, uint16_t u_bound);
void pedals_check_cc_bounds(uint16_t *cc, uint16_t values[5], uint16_t l_bounds[5],
		uint16_t u_bounds[5]);
float pedals_saturate_sensor(uint16_t value, uint16_t l_bound, uint16_t u_bound);
void pedals_saturate_sensors(float *result, uint16_t values[5], uint16_t l_bounds[5], 
		uint16_t u_bounds[5]);
void pedals_check_apps(float *result, bool *faulty, float apps[2], pedals_cc_t cc[2]);
void pedals_conv_pressure(float *result, bool *faulty, float bps, 
		pedals_cc_t cc, float scaling, float offset, float max);
bool pedals_check_apps_brake(float apps, float pressure);
bool pedals_hardbraking(float pressure, float limit);
#endif
