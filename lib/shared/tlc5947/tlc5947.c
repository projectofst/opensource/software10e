#include "tlc5947.h"

tlc5947_t tlc5947_init(uint16_t total, uint16_t* intensity)
{
    return (tlc5947_t) {
        .total = total,
        .intensity = intensity,
    };
}

void tlc5947_clear_all(tlc5947_t* tlc)
{
    int i = 0;
    for (i = 0; i < tlc->total; i++) {
        tlc->intensity[i] = 0;
    }
}

void tlc5947_set_all(tlc5947_t* tlc, uint16_t luminosity)
{
    int i = 0;
    for (i = 0; i < tlc->total; i++) {
        tlc->intensity[i] = luminosity;
    }
}

void tlc5947_set_all_bitfield(tlc5947_t* tlc, uint16_t luminosity, uint32_t bitfield)
{
    int i = 0;
    for (i = 0; i < tlc->total; i++) {
        tlc->intensity[i] = ((bitfield >> (tlc->total - i)) & 0x1) ? 0 : luminosity;
    }
}

void tlc5947_set(tlc5947_t* tlc, uint16_t luminosity, uint16_t id)
{
    if (id >= tlc->total) {
        return;
    }
    tlc->intensity[id] = luminosity;
}

void tlc5947_clear(tlc5947_t* tlc, uint16_t id)
{
    tlc->intensity[id] = 0;
}

void tlc5947_write(tlc5947_t* tlc)
{
    int8_t bits;
    int16_t channel;

    tlc_xlat_set(0);

    // 24 channels per TLC5974
    for (channel = 24 - 1; channel >= 0; channel--)
        // 12 bits per channel, send MSB first
        for (bits = 11; bits >= 0; bits--) {
            tlc_sclk_set(0);
            if (tlc->intensity[channel] & (1 << bits)) {
                tlc_sin_set(1);
            } else {
                tlc_sin_set(0);
            }
            tlc_sclk_set(1);
        }

    tlc_xlat_set(0);
    tlc_xlat_set(1);
    tlc_xlat_set(0);

    return;
}

void tlc5947_begin(tlc5947_t* tlc)
{
    int i;

    tlc_tris_sclk_set(OUTPUT);
    tlc_tris_sin_set(OUTPUT);
    tlc_tris_xlat_set(OUTPUT);
    tlc_tris_blank_set(OUTPUT);

    // For now, blank should always be initialized with a LOW
    // PCB could have a pulldown for blank and this pin could be not used
    tlc_blank_set(0);

    // The data in the grayscale shift register are moved to the grayscale
    // data latch with a low-to-high transition on this pin
    tlc_xlat_set(0);

    // Initialize LEDs with OFF state
    for (i = 0; i < tlc->total; i++)
        tlc->intensity[i] = 0;

    return;
}

void __attribute__((weak)) tlc_xlat_set(bool state)
{
    return;
}
void __attribute__((weak)) tlc_sclk_set(bool state)
{
    return;
}
void __attribute__((weak)) tlc_sin_set(bool state)
{
    return;
}
void __attribute__((weak)) tlc_blank_set(bool state)
{
    return;
}
void __attribute__((weak)) tlc_tris_xlat_set(tlc_pin_dir_t state)
{
    return;
}
void __attribute__((weak)) tlc_tris_sclk_set(tlc_pin_dir_t state)
{
    return;
}
void __attribute__((weak)) tlc_tris_sin_set(tlc_pin_dir_t state)
{
    return;
}
void __attribute__((weak)) tlc_tris_blank_set(tlc_pin_dir_t state)
{
    return;
}
