#include <stdint.h>
#include <stdbool.h>

typedef struct _tlc5947 {
	uint16_t *intensity;
	uint16_t total;
} tlc5947_t;

//callbacks
typedef enum _tlc_pin_direction {OUTPUT, INPUT} tlc_pin_dir_t;
void tlc_xlat_set(bool state);
void tlc_sclk_set(bool state);
void tlc_sin_set(bool state);
void tlc_blank_set(bool state);
void tlc_tris_xlat_set(tlc_pin_dir_t state);
void tlc_tris_sclk_set(tlc_pin_dir_t state);
void tlc_tris_sin_set(tlc_pin_dir_t state);
void tlc_tris_blank_set(tlc_pin_dir_t state);

tlc5947_t tlc5947_init(uint16_t total, uint16_t *intensity);
void tlc5947_clear_all(tlc5947_t *tlc);
void tlc5947_set_all(tlc5947_t *tlc, uint16_t luminosity);
void tlc5947_set_all_bitfield(tlc5947_t *tlc, uint16_t luminosity, uint32_t bitfield);
void tlc5947_set(tlc5947_t *tlc, uint16_t luminosity, uint16_t id);
void tlc5947_clear(tlc5947_t *tlc, uint16_t id);
void tlc5947_write(tlc5947_t *tlc);
void tlc5947_begin(tlc5947_t* tlc);
