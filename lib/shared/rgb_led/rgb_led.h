#ifndef _RGB_LED_H_
#define _RGB_LED_H_

#include "shared/lib_hk/hk.h"

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


typedef struct _rgb_t{
	int (*red_led_pwm_update)(float);
	int (*green_led_pwm_update)(float);
	int (*blue_led_pwm_update)(float);
} rgb_t;


typedef enum {
	LED_SQUARE = 0,      //typical pwm with user defined duty_cycle and period
	LED_TRIANGLE = 1,    //triangular wave
	LED_SIN = 2,
	LED_SAWTOOTH = 3,
} rgb_wave_t;

typedef struct _rgb_profile_t {
	const char *name;
	rgb_wave_t wave;
	double period; // seconds	
	double duty_cycle; // [0:1]
	uint32_t color; //hex
} rgb_profile_t;

void rgb_indicator_task(void *arg);
int rgb_config(rgb_profile_t *profiles, int profile_num, const char* current_profile);
void rgb_pwm_update(rgb_t *led, float red_dc, float green_dc, float blue_dc);
int rgb_set_profile(rgb_profile_t *_profiles, const char* _current_profile);


#endif
