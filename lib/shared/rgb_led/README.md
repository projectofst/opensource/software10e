## RGB_LED
### USAGE

Configure all led profiles (default set at 8, the cap can be changed in makefile with -DNUM_PROFIFLES n).
Led profiles must have an id string, type of wave, wave period, duty_cycle (only important in LED_SQUARE),
and color. Example:

```C
rgb_profile_t profiles[] = {
        {.name = "ams_error", .wave = LED_SQUARE, .period = 1.0, .duty_cycle = 0.5, .color = 0xFF0000}, //medium brightness continuous red wave
        {.name = "charging", .wave = LED_TRIANGLE, .period = 2.0, .duty_cycle = 1.0, .color = 0xFFFF00} //yellow triangular wave
     	{.name = "ams_on", .wave = LED_SIN, .period = 0.5, .duty_cycle = 1.0, .color = 0xFFFFFF} //rapid pulsing sin white wave
     	...
     };
```
__Note:__ The color is defined by a Hex color code. This code defines the intesnity of red, green and blue in a scale from 0 to 255. In the example above, the number is writen in hexadecimal code, therefore the two most significant bits correspond to the red intensity, the two middle bits correspond to the green intensity and the two less significant bits correspond to the blue intensity. To check which code corresponds to the color that you want, check this [link](https://www.rapidtables.com/web/color/RGB_Color.html).

Initialize a variable with the type `rgb_t`. Attribute `pwmX_update_duty_cycle` to the leds that can be used. Example:

```C
rgb_t rgb_led1 = {.red_led_pwm_update = NULL, .green_led_pwm_update = NULL, .blue_led_pwm_update = NULL};

rgb_led1.red_led_pwm_update = pwm1_update_duty_cycle;
rgb_led1.green_led_pwm_update = pwm1_update_duty_cycle;
rgb_led1.blue_led_pwm_update = pwm1_update_duty_cycle;
```

You can now configure the rgb_led with `rgb_config`. 

Finally, you can call the `rgb_indicator_task` in the scheduler like this:

```C
{ .name = "rgb_indicator_task", .period = 1, .func = rgb_indicator_task, .args_on = 1, .args = (void *) &rgb_led1} 
```
__Note:__ .period must be set to 1 to avoid timing issues (setting it to zero will deform the desired waves).

If you want to update the profile led, call function `rgb_set_profile`, where the second argument is the identifying 
string of the new profile.

### FUNCTIONS 

```
void rgb_indicator_task(void *arg)

Task that implements the selected profile.

args    : arg - inputed led
returns : (void)
```

```
int rgb_config(rgb_profile_t *_profiles, int _num_profiles, const char* current_profile)

Configures the rgb profile to be implemented.

args    : _profiles       - rgb profile list
		  _num_profiles   - total number of profiles in profile list
		  current_profile - string that identifies profile to be implemented 
returns :  0 			  - config was well executed
          -1              - either _num_profiles exceeds maximum NUM_PROFILES or inputed string doesn't correspond to any profile 
```

```
int rgb_set_profile(rgb_profile_t *_profiles, const char* _current_profile)

Sets rgb profile. Can be used to change the profile being currently used.

args    : _profiles        - rgb porfile lĩst
          _current_profile - string that identifies profile to be implemented
returns : 0                - profile was successfully set
		  1                - _current_profile doesn't correspond to any existing profile
```
