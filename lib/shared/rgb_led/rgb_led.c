#include "rgb_led.h"

#define NUM_PROFILES 8
#define PI 3.141593
#define SIGN(x) x < 0 ? 0 : 1

rgb_profile_t profiles[NUM_PROFILES];
int num_profiles, current_profile;

void rgb_indicator_task(void* arg)
{
    rgb_t* _led = (rgb_t*)arg;
    rgb_t led = *_led;
    uint32_t time = get_time_ms();
    int i;

    float red_dc, green_dc, blue_dc;

    float r = ((profiles[current_profile].color >> 16) & 0xFF) / 255.0;
    float g = ((profiles[current_profile].color >> 8) & 0xFF) / 255.0;
    float b = ((profiles[current_profile].color >> 0) & 0xFF) / 255.0;

    if (profiles[current_profile].wave == LED_SQUARE) {
        // pulse wave (0 to 1)
        if (((time / 1000.0) / profiles[current_profile].period - floor((time / 1000.0) / profiles[current_profile].period)) < profiles[current_profile].duty_cycle) {
            red_dc = r;
            green_dc = g;
            blue_dc = b;
        }

        else {
            red_dc = 0;
            green_dc = 0;
            blue_dc = 0;
        }

        rgb_pwm_update(&led, red_dc, green_dc, blue_dc);

    } else if (profiles[current_profile].wave == LED_TRIANGLE) {
        red_dc = r * fabs(2 * (((uint32_t)(time / profiles[current_profile].period) % 1000) / 1000.0) - 1); // triangular wave (0 to 1)
        green_dc = g * fabs(2 * (((uint32_t)(time / profiles[current_profile].period) % 1000) / 1000.0) - 1);
        blue_dc = b * fabs(2 * (((uint32_t)(time / profiles[current_profile].period) % 1000) / 1000.0) - 1);

        rgb_pwm_update(&led, red_dc, green_dc, blue_dc);

    } else if (profiles[current_profile].wave == LED_SIN) {
        red_dc = r * 0.5 * ((float)sin(((2 * PI) / profiles[current_profile].period) * time / 1000.0) + 1.0); // sin wave (0 to 1)
        green_dc = g * 0.5 * ((float)sin(((2 * PI) / profiles[current_profile].period) * time / 1000.0) + 1.0);
        blue_dc = b * 0.5 * ((float)sin(((2 * PI) / profiles[current_profile].period) * time / 1000.0) + 1.0);

        rgb_pwm_update(&led, red_dc, green_dc, blue_dc);

    }

    else if (profiles[current_profile].wave == LED_SAWTOOTH) {
        red_dc = r * ((time / 1000.0) / profiles[current_profile].period - floor(0.5 + (time / 1000.0) / profiles[current_profile].period) + 0.5);
        green_dc = g * ((time / 1000.0) / profiles[current_profile].period - floor(0.5 + (time / 1000.0) / profiles[current_profile].period) + 0.5);
        blue_dc = b * ((time / 1000.0) / profiles[current_profile].period - floor(0.5 + (time / 1000.0) / profiles[current_profile].period) + 0.5);

        rgb_pwm_update(&led, red_dc, green_dc, blue_dc);
    }
}

int rgb_config(rgb_profile_t* _profiles, int _num_profiles, const char* current_profile)
{
    num_profiles = _num_profiles;
    int i;
    if (num_profiles > NUM_PROFILES)
        return -1;

    memset(profiles, 0, NUM_PROFILES * sizeof(rgb_profile_t));
    for (i = 0; i < num_profiles; i++)
        profiles[i] = _profiles[i];

    if (rgb_set_profile(profiles, current_profile))
        return -1;
    // memcpy(profiles, _profiles, num_profiles * sizeof(rgb_profile_t));

    return 0;
}

void rgb_pwm_update(rgb_t* led, float red_dc, float green_dc, float blue_dc)
{
    if (led->red_led_pwm_update != NULL)
        led->red_led_pwm_update(red_dc);

    if (led->green_led_pwm_update != NULL)
        led->green_led_pwm_update(green_dc);

    if (led->blue_led_pwm_update != NULL)
        led->blue_led_pwm_update(blue_dc);
}

int rgb_set_profile(rgb_profile_t* _profiles, const char* _current_profile)
{
    int i;

    for (i = 0; i < num_profiles; i++)
        if (_profiles[i].name == _current_profile) {
            current_profile = i;
            return 0;
        }

    return 1;
}
