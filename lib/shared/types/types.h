#ifndef __TYPE_DEFS_H__
#define __TYPE_DEFS_H__

#include <stdint.h>

typedef uint32_t time_ms_t;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;

typedef float f32;
typedef double f64;

#endif
