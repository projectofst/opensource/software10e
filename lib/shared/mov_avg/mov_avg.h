#ifndef __MOV_AVG_H__
#define __MOV_AVG_H__

#include <stdint.h>
#include <stdio.h>

#ifndef N_MOVING_AVERAGE
#define N_MOVING_AVERAGE 16
#endif

typedef struct _ma_channel {
    uint16_t acc;
    uint32_t N;
} ma_channel;

void moving_average(uint16_t value, uint16_t i);
void reset_moving_average(void);
uint16_t get_ma_value(uint16_t i);

#endif
