#include "mov_avg.h"

ma_channel ma[N_MOVING_AVERAGE];

void moving_average(uint16_t value, uint16_t i)
{

    ma[i].acc = (ma[i].N * ma[i].acc + value) / (ma[i].N + 1);
    ma[i].N++;

    return;
}
void reset_moving_average(void)
{
    uint16_t i;

    for (i = 0; i < N_MOVING_AVERAGE; i++) {
        ma[i].acc = 0;
        ma[i].N = 0;
    }

    return;
}
uint16_t get_ma_value(uint16_t i)
{

    return ma[i].acc;
}
