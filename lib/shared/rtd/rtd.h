#ifndef __RTD_H__
#define __RTD_H__

#include <stdint.h>

#define CMD_ID_COMMON_RTD_ON		16
#define CMD_ID_COMMON_RTD_OFF       17

#ifndef _CANDATA
#define _CANDATA
typedef struct {
	union {
        struct {
			uint16_t dev_id:5; // Least significant
			// First bit of msg_id determines if msg is reserved or not.
			// 0 == reserved (higher priority) (0-31 decimal)
			uint16_t msg_id:6; // Most significant
		};
		uint16_t sid;
	};
	uint16_t dlc:4;
	uint16_t data[4];
} CANdata;
#endif

typedef enum{
    RTD_STEP_DASH_BUTTON = 0b0000,
    RTD_STEP_TE_OK       = 0b0001,
    RTD_STEP_TE_NOK      = 0b1001,
    RTD_STEP_INV_OK      = 0b0010,
    RTD_STEP_INV_NOK     = 0b1010,
    RTD_STEP_DCU         = 0b0011,
    RTD_STEP_INV_GO      = 0b0100,
    RTD_STEP_DASH_LED    = 0b0101,  
} RTD_ON;

typedef enum{
    RTD_DASH_OFF        = 0,
    RTD_INV_CONFIRM     = 1,
	RTD_INV_OFF         = 2,
} RTD_OFF;

typedef struct{
    RTD_ON step;
//    RTD_ERROR error;
} COMMON_MSG_RTD_ON;

void parse_can_common_RTD(CANdata message, COMMON_MSG_RTD_ON *RTD);
CANdata make_rtd_on_msg(uint8_t dev_id, RTD_ON step);
CANdata make_rtd_off_msg(uint8_t dev_id, RTD_OFF step);


#endif