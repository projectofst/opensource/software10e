#include "rtd.h"

void __attribute__((weak)) parse_can_common_RTD(CANdata message, COMMON_MSG_RTD_ON* RTD)
{

    RTD->step = (RTD_ON)((message.data[0]) & 0b1111);

    return;
}

CANdata __attribute__((weak)) make_rtd_on_msg(uint8_t dev_id, RTD_ON step)
{

    CANdata message;

    message.dev_id = dev_id;
    message.msg_id = CMD_ID_COMMON_RTD_ON;

    message.dlc = 2;

    message.data[0] = step;

    return message;
}

CANdata __attribute__((weak)) make_rtd_off_msg(uint8_t dev_id, RTD_OFF step)
{

    CANdata message;

    message.dev_id = dev_id;
    message.msg_id = CMD_ID_COMMON_RTD_OFF;

    message.dlc = 2;

    message.data[0] = step;

    return message;
}