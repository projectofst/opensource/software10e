#include "fifo.h"
#include <stdint.h>
#include <stdlib.h>

/* Init fifo with read and write pointers, message buffers and size of buffer */
void fifo_init(Fifo* fifo, CANdata* msgs, uint16_t size)
{
    fifo->size = size;
    fifo->read_index = 0;
    fifo->write_index = 0;
    fifo->msgs = msgs;
    fifo->n = 0;
}

/* Append message to the fifo */
__attribute__((deprecated)) void append_fifo(Fifo* fifo, CANdata msg)
{
    fifo_push(fifo, msg);
}

/* Append message to the fifo */
void fifo_push(Fifo* fifo, CANdata msg)
{
    fifo->msgs[fifo->write_index] = msg;
    fifo->write_index = (fifo->write_index + 1) % fifo->size;

    if (/*(fifo->n % fifo->size) != 0 || fifo->n == 0*/ fifo->n < fifo->size) {
        fifo->n++;
    }
}

__attribute__((deprecated)) CANdata* pop_fifo(Fifo* fifo)
{
    return fifo_pop(fifo);
}

CANdata* fifo_pop(Fifo* fifo)
{
    CANdata* aux;

    if (fifo_empty(fifo)) {
        return NULL;
    }

    aux = &(fifo->msgs[fifo->read_index]);
    fifo->read_index = (fifo->read_index + 1) % fifo->size;
    fifo->n--;

    return aux;
}

// returns 1 if empty and 0 if not empty
bool fifo_empty(Fifo* fifo)
{
    return fifo->n == 0;
}

// returns the number of items in the buffer
uint16_t fifo_occupied(Fifo* fifo)
{
    return fifo->n;
}
