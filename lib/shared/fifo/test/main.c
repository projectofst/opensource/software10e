#include <stdio.h>
#include <stdint.h>
#include "munit.h"
#include "fifo.h"
#define CAN1_SIZE 64

static MunitResult fifo_occupied_ok(const MunitParameter params[], void* user_data) {

    CANdata msgs[CAN1_SIZE];
    CANdata test_msg;
    uint8_t i;

    Fifo _fifo;
    Fifo *fifo = &_fifo;

    fifo_init(fifo, msgs, CAN1_SIZE);

	munit_assert_int(fifo_occupied(fifo), ==, 0);

    append_fifo(fifo, test_msg);

    munit_assert_int(fifo_occupied(fifo), ==, 1);

    for(i = 0; i < 5; i++){
        append_fifo(fifo, test_msg);
    }


    munit_assert_int(fifo_occupied(fifo), == ,6);

    pop_fifo(fifo);

    munit_assert_int(fifo_occupied(fifo), == ,5);

    for(i = 0; i < 5; ++i){
        pop_fifo(fifo);
    }

    munit_assert_int(fifo_occupied(fifo), == ,0);

    for(i = 0; i < (CAN1_SIZE+1); ++i){
        append_fifo(fifo, test_msg);
    }

    munit_assert_int(fifo_occupied(fifo), ==, CAN1_SIZE);

    pop_fifo(fifo);

    munit_assert_int(fifo_occupied(fifo), ==, (CAN1_SIZE-1));

    for(i = 0; i < CAN1_SIZE-1; ++i){
        pop_fifo(fifo);
    }

	munit_assert_int(fifo_occupied(fifo), ==, 0);

	return MUNIT_OK;
}


static MunitResult fifo_empty_ok(const MunitParameter params[], void* user_data) {
    CANdata msgs[CAN1_SIZE];
    CANdata test_msg;
    uint8_t i;

    Fifo _fifo;
    Fifo *fifo = &_fifo;

    fifo_init(fifo, msgs, CAN1_SIZE);

	munit_assert_int(fifo_empty(fifo), ==, 1);

    append_fifo(fifo, test_msg);

    munit_assert_int(fifo_empty(fifo), ==, 0);

    for(i = 0; i < 5; i++){
        append_fifo(fifo, test_msg);
    }


    munit_assert_int(fifo_empty(fifo), == ,0);

    pop_fifo(fifo);

    munit_assert_int(fifo_empty(fifo), == ,0);

    for(i = 0; i < 5; ++i){
        pop_fifo(fifo);
    }

    munit_assert_int(fifo_empty(fifo), == ,1);

    for(i = 0; i < (CAN1_SIZE+1); ++i){
        append_fifo(fifo, test_msg);
    }

    munit_assert_int(fifo_empty(fifo), ==, 0);

    pop_fifo(fifo);

    munit_assert_int(fifo_empty(fifo), ==, 0);

    for(i = 0; i < CAN1_SIZE-1; ++i){
        pop_fifo(fifo);
    }

	munit_assert_int(fifo_empty(fifo), ==, 1);

	return MUNIT_OK;
}

static MunitResult fifo_order_ok(const MunitParameter params[], void* user_data) {
    CANdata msgs[CAN1_SIZE];
    CANdata test_msg;
    CANdata test_msg_test;
    CANdata *test_msg_result;
    test_msg.data[0]=1;
    test_msg_test.data[0]= 2;


    Fifo _fifo;
    Fifo *fifo = &_fifo;

    fifo_init(fifo, msgs, 5);

    munit_assert_int(fifo_empty(fifo), ==, 1);

    append_fifo(fifo, test_msg);

    munit_assert_int(fifo_empty(fifo), ==, 0);

    append_fifo(fifo, test_msg);

    munit_assert_int(fifo_empty(fifo), ==, 0);

    append_fifo(fifo, test_msg);

    munit_assert_int(fifo_empty(fifo), ==, 0);

    append_fifo(fifo, test_msg);

    munit_assert_int(fifo_empty(fifo), ==, 0);

    append_fifo(fifo, test_msg);

    munit_assert_int(fifo_empty(fifo), ==, 0);

    append_fifo(fifo, test_msg_test);

    munit_assert_int(fifo_empty(fifo), == ,0);

    test_msg_result = pop_fifo(fifo);

    munit_assert_int(test_msg_result->data[0] , == ,2);

    munit_assert_int(fifo_empty(fifo), == ,0);

    test_msg_result = pop_fifo(fifo);

    munit_assert_int(test_msg_result->data[0] , == ,1);

    munit_assert_int(fifo_empty(fifo), ==, 0);

    test_msg_result = pop_fifo(fifo);

    munit_assert_int(test_msg_result->data[0] , == ,1);

    munit_assert_int(fifo_empty(fifo), ==, 0);

    test_msg_result = pop_fifo(fifo);

    munit_assert_int(test_msg_result->data[0] , == ,1);

    munit_assert_int(fifo_empty(fifo), ==, 0);

    test_msg_result = pop_fifo(fifo);

    munit_assert_int(test_msg_result->data[0] , == ,1);

    munit_assert_int(fifo_empty(fifo), ==, 1);

    test_msg_result = pop_fifo(fifo);

    munit_assert_int(test_msg_result, == , NULL);

    munit_assert_int(fifo_empty(fifo), ==, 1);

    return MUNIT_OK;
}
static MunitTest test_suite_tests[] = {
  	{ (char*) "/fifo/fifo_occupied_ok", fifo_occupied_ok, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
      { (char*) "/fifo/fifo_order_ok", fifo_order_ok, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	 { (char*) "/fifo/fifo_empty_ok", fifo_empty_ok, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
  	(char*) "", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

#include <stdlib.h>

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {

  	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
