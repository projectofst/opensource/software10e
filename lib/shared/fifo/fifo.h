#ifndef __FIFO_H__
#define __FIFO_H__

#include <stdint.h>
#include <stdbool.h>

#ifndef _CANDATA
#define _CANDATA
typedef struct {
	union {
		struct {
			uint16_t dev_id:5; // Least significant
			uint16_t msg_id:6; // Most significant
		};
		uint16_t sid;
	};
	uint8_t dlc:4;
	uint16_t data[4];
} CANdata;
#endif

typedef struct _Fifo {
	uint16_t size;
	uint16_t read_index;
	uint16_t write_index;
	CANdata *msgs;
	uint8_t n;
} Fifo;


void fifo_init(Fifo *fifo, CANdata *msgs, uint16_t size);
void append_fifo(Fifo *fifo, CANdata msg);
void fifo_push(Fifo *fifo, CANdata msg);
CANdata *pop_fifo(Fifo *fifo);
CANdata *fifo_pop(Fifo *fifo);
bool fifo_empty(Fifo *fifo);
uint16_t fifo_occupied(Fifo *fifo);
#endif
