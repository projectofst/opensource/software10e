#include "CAN_IDs.h"

CANmessage can_data_to_can_message(CANdata msg, uint32_t timestamp)
{
    CANmessage aux;
    aux.candata = msg;
    aux.timestamp = timestamp;

    return aux;
}
