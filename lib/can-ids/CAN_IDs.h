/**********************************************************************
 *	 CAN IDs for FST 08e
 *
 *	 log:
 *	 ______________________________________________________________
 *	 2017 - Projecto FST Lisboa
 **********************************************************************/

#ifndef __CAN_IDS_H__
#define __CAN_IDS_H__

#include <stdint.h>

#ifndef _CANDATA
#define _CANDATA
typedef struct {
	union {
        struct {
			uint16_t dev_id:5; // Least significant
			// First bit of msg_id determines if msg is reserved or not.
			// 0 == reserved (higher priority) (0-31 decimal)
			uint16_t msg_id:6; // Most significant
		};
		uint16_t sid;
	};
	uint16_t dlc:4;
	uint16_t data[4];
} CANdata;
#endif

typedef struct {
	CANdata candata;
    uint64_t timestamp;
} CANmessage;

CANmessage can_data_to_can_message(CANdata msg, uint32_t timestamp);

#define CAN_GET_MSG_ID(sid) (((sid) >> 5) & 0b111111)
#define CAN_GET_DEV_ID(sid) ((sid) & 0b11111)
#define CAN_GET_SID(msg_id,dev_id) ((((msg_id) & 0b0111111)<<5) | ((dev_id) & 0b011111))

/* DEVICE IDs (0-31) */
#define DEVICE_ID_DCU				8
#define DEVICE_ID_TE				9
#define DEVICE_ID_DASH				10
#define DEVICE_ID_LOGGER			11
#define DEVICE_ID_STEERING_WHEEL		12
#define DEVICE_ID_ARM				13
#define DEVICE_ID_BMS_MASTER			14
#define DEVICE_ID_BMS_VERBOSE			14
#define DEVICE_ID_IIB				16
#define DEVICE_ID_TELEMETRY			17
#define DEVICE_ID_AHRS_FRONT			20
#define DEVICE_ID_AHRS_REAR			21
#define DEVICE_ID_GPS_SUPOTS_FRONT		22
#define DEVICE_ID_GPS_SUPOTS_REAR		23
#define DEVICE_ID_CCU_LEFT			24
#define DEVICE_ID_CCU_RIGHT			25
#define DEVICE_ID_STEER				26
#define DEVICE_ID_TIRETEMP          27
#define DEVICE_ID_ISABELLE			29
#define DEVICE_ID_INTERFACE			31

/* RESERVED MESSAGE IDS (0-31)
 * Place your unique high priority message IDs here */
// Digital Control Unit
// Torque Encoder
#define MSG_ID_ARM_TORQUE			8
#define MSG_ID_ARM_SPEED			9
#define MSG_ID_TE_MAIN				10
#define MSG_ID_IIB_SPEED 			11

#define MSG_ID_MASTER_TS_STATE		12
#define MSG_ID_MASTER_IMD_ERROR		13
#define MSG_ID_MASTER_AMS_ERROR		14
#define MSG_ID_MASTER_STATUS		15

/* Common message ids */

//RTD ACTIVATION SEQUENCE
//pls fix nomenclatura
#define CMD_ID_COMMON_RTD_ON		16
#define CMD_ID_COMMON_RTD_OFF       17
#define CMD_ID_COMMON_TS 			18

#define MSG_ID_COMMON_ERROR	        26
#define MSG_ID_COMMON_RESET         29
#define MSG_ID_COMMON_TRAP          30
#define MSG_ID_COMMON_PARSE_ERROR   31

#define MSG_ID_SEND_POWER			32

#define CMD_ID_COMMON_READ_FLASH		23
#define CMD_ID_COMMON_WRITE_FLASH		24
#define CMD_ID_COMMON_WRITE			25
#define CMD_ID_COMMON_GET			28
#define CMD_ID_COMMON_SET			27


// LOG (TRAP/ERROR) (common to all devices)


#endif
