#include "telemetry.h"
#include "../CAN_IDs.h"

void parse_Telemetry_status_message(uint16_t data[4], Telemetry_status* status)
{
    status->status = data[0];
}

void parse_Telemetry_flash_message(uint16_t data[4], Flash_message* flash)
{
    flash->device = data[0];
    flash->parameter = data[1];
    flash->value = data[2];
}

void parse_can_Telemetry(CANdata message, Telemetry_CAN_data* telemetry)
{
    switch (message.msg_id) {
    case MSG_ID_TELEMETRY_STATUS:
        parse_Telemetry_status_message(message.data, &(telemetry->status));
        return;
    case MSG_ID_TELEMETRY_FLASH:
        parse_Telemetry_flash_message(message.data, &(telemetry->flash));
        return;
    default:
        return;
    }
}
