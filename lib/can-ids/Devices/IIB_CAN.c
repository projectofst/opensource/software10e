#include "IIB_CAN.h"
#include "../CAN_IDs.h"
#include <stdbool.h>
#include <stdint.h>

void parse_IIB_limits_message(uint16_t data[4], IIB_limits* limits)
{

    limits->motor_num = (data[0] & 0b01100000000000000) >> 14;
    limits->max_safety_power = (data[0] & 0b00000000011111111);
    limits->max_operation_power = data[1];
    limits->max_safety_torque = data[2];
    limits->max_operation_torque = data[3];
}

void parse_IIB_regen_limits_message(uint16_t data[4], IIB_regen_limits* regen_limits)
{

    regen_limits->motor_num = data[0];
    regen_limits->max_safety_regen_torque = data[1];
    regen_limits->max_operating_regen_torque = data[2];
}

void parse_IIB_car_info_message(uint16_t data[4], IIB_CAR_info* car_info)
{

    car_info->status.TE_OK = (data[0] & 0b01);
    car_info->status.ARM_OK = (data[0] & 0b010) >> 1;
    car_info->status.ARM_STUPID = (data[0] & 0b0100) >> 2;
    car_info->status.arm_received_msg = (data[0] & 0b01000) >> 3;
    car_info->status.SDC1 = (data[0] & 0b010000) >> 4;
    car_info->status.SDC2 = (data[0] & 0b0100000) >> 5;
    car_info->status.CAR_RTD = (data[0] & 0b01000000) >> 6;
    car_info->status.INV_HV = (data[0] & 0b010000000) >> 7;
    car_info->status.TSAL_status = (data[0] & 0b0100000000) >> 8;

    car_info->TE_percentage = data[1];
    car_info->BMS_voltage = data[3];
}

void parse_IIB_motor_info_message(uint16_t data[4], IIB_MOTOR_info* motor_info)
{

    motor_info->motor_num = (data[0] & 0b01100000000000000) >> 14;
    motor_info->temp_motor = (data[0] & 0b00000000011111111);
    motor_info->actual_speed = data[1];
    motor_info->current = data[2];
    motor_info->torque = data[3];
}

void parse_IIB_inv_info_message(uint16_t data[4], IIB_INV_info* inv_info)
{

    inv_info->inverter_active = (data[0] & 0b01);
    inv_info->BE_1 = (data[0] & 0b010) >> 1;
    inv_info->BE_2 = (data[0] & 0b0100) >> 2;
    inv_info->i = (data[0] & 0b011000) >> 3;

    inv_info->temperature_status.inverter_min_passed = (data[0] & 0b0100000) >> 5;
    inv_info->temperature_status.inverter_max_passed = (data[0] & 0b01000000) >> 6;
    inv_info->temperature_status.IGBT_min_passed = (data[0] & 0b010000000) >> 7;
    inv_info->temperature_status.IGBT_max_passed = (data[0] & 0b0100000000) >> 8;
    inv_info->temperature_status.motor_min_passed = (data[0] & 0b01000000000) >> 9;
    inv_info->temperature_status.motor_max_passed = (data[0] & 0b010000000000) >> 10;
    inv_info->temperature_status.iib_air_min_passed = (data[0] & 0b0100000000000) >> 11;
    inv_info->temperature_status.iib_air_max_passed = (data[0] & 0b01000000000000) >> 12;

    inv_info->temp_IGBT = data[1];
    inv_info->temp_inverter = data[2];
    inv_info->error_info = data[3];
}

void parse_IIB_iib_info_message(uint16_t data[4], IIB_info* iib_info)
{

    iib_info->air_temp = data[0];
    iib_info->temp_2 = data[1];
    iib_info->max_rpm = data[2];

    iib_info->fan_on = (data[3] & 0b01);
    iib_info->CU_power_A = (data[3] & 0b010) >> 1;
    iib_info->CU_power_B = (data[3] & 0b0100) >> 2;
    iib_info->INV_RTD = (data[3] & 0b01000) >> 3;
    iib_info->INV_ON = (data[3] & 0b010000) >> 4;
    iib_info->REGEN_ON = (data[3] & 0b0100000) >> 5;
    iib_info->DEBUG_ON = (data[3] & 0b01000000) >> 6;

    return;
}

void parse_IIB_iib_power_messsage(uint16_t data[4], IIB_total_power* iib_power)
{

    iib_power->power = data[0];

    return;
}

void parse_IIB_iib_debug1_message(uint16_t data[4], IIB_DEBUG1* iib_debug1)
{

    iib_debug1->i = (data[0] & 0b01);
    iib_debug1->arm_received_msg = (data[0] & 0b0100) >> 2;
    iib_debug1->node_address = (data[0] & 0b01111000) >> 3;
    iib_debug1->te_alive_again = (data[0] & 0b010000000) >> 7;

    iib_debug1->max_i2t_current = data[1];
    iib_debug1->i2t_saturation_time = data[2];
    iib_debug1->cool_down_on_time = data[3];
}

void parse_IIB_iib_debug2_message(uint16_t data[4], IIB_DEBUG2* iib_debug2)
{

    iib_debug2->amk_status_word = data[0];
    iib_debug2->message1_id = data[1];
    iib_debug2->message2_id = data[2];
    iib_debug2->reference_id = data[3];
}

void parse_IIB_amk_actual_values_1(uint16_t data[4], IIB_AMK_VALUES_1* amk_values_1)
{

    amk_values_1->control_word = data[0];
    amk_values_1->actual_speed = data[1];
    amk_values_1->torque_current = data[2];
    amk_values_1->magnetizing_current = data[3];
}

void parse_IIB_amk_actual_values_2(uint16_t data[4], IIB_AMK_VALUES_2* amk_values_2)
{

    amk_values_2->temp_motor = data[0];
    amk_values_2->temp_inverter = data[1];
    amk_values_2->error_info = data[2];
    amk_values_2->temp_IGBT = data[3];
}

void parse_can_IIB(CANdata message, IIB_CAN_data* data)
{

    switch (message.msg_id) {
    case MSG_ID_IIB_INFO:
        parse_IIB_iib_info_message(message.data, &(data->iib_info));
        return;
    case MSG_ID_IIB_REGEN_LIMITS:
        parse_IIB_regen_limits_message(message.data, &(data->regen_limits));
        return;
    case MSG_ID_IIB_MOTOR_INFO:
        parse_IIB_motor_info_message(message.data, &(data->motor_info));
        return;
    case MSG_ID_IIB_INV_INFO:
        parse_IIB_inv_info_message(message.data, &(data->inv_info));
        return;
    case MSG_ID_IIB_LIMITS:
        parse_IIB_limits_message(message.data, &(data->limits));
        return;
    case MSG_ID_IIB_CAR_INFO:
        parse_IIB_car_info_message(message.data, &(data->car_info));
        return;
    case MSG_ID_IIB_POWER:
        parse_IIB_iib_power_messsage(message.data, &(data->iib_power));
        return;
    case MSG_ID_IIB_DEBUG1_INFO:
        parse_IIB_iib_debug1_message(message.data, &(data->iib_debug1));
        return;
    case MSG_ID_IIB_DEBUG2_INFO:
        parse_IIB_iib_debug2_message(message.data, &(data->iib_debug2));
    case MSG_ID_IIB_AMK_ACTUAL_VALUES_1:
        parse_IIB_amk_actual_values_1(message.data, &(data->amk_values_1));
        return;
    case MSG_ID_IIB_AMK_ACTUAL_VALUES_2:
        parse_IIB_amk_actual_values_2(message.data, &(data->amk_values_2));
        return;

    default:
        return;
    }
}
