#ifndef __DASH_STEER_H
#define __DASH_STEER_H
#include "../CAN_IDs.h"
#include <stdint.h>
#include <stdbool.h>


#define MSG_ID_MOTOR_OFF                30
#define CMD_ID_RESET_INV_ERROR          50
#define CMD_ID_TSAL_BLUE                51
#define CMD_ID_TOGGLE_YAW               52
#define MSG_ID_ACCUMULATOR	            53
#define MSG_ID_APPS_BPS_IMPLAUSIBILITY  54
#define MSG_ID_PERIPHERALS              55
#define MSG_ID_MODE                     56
#define MSG_ID_TQ_MODE                  57
#define MSG_ID_STEERING_BUTTONS			58 

typedef enum _buttons {
	BLUE,
	YELLOW,
	RED,
	GREEN,
	RIGHT_BLACK,
	LEFT_BLACK
} Button;

typedef struct _SteerButtons {
	Button button;
} SteerButtons;

typedef enum _motor{
    REAR,
    FRONT
}MOTOR;

typedef enum _modes {
    _ACCEL_MODE,
    _SP_MODE
 }_MODES;

typedef struct{
    MOTOR motor;
}DASH_MSG_Motor;

typedef struct {
    uint8_t max_temp;
    uint8_t mean_temp;
    uint16_t min_voltage;
    uint8_t percentage;
} DASH_MSG_Accumulator;

typedef struct{

    bool pump2 :1;
    bool pump1 :1;
    bool fans :1;

}DASH_MSG_Peripherals;


// MAIN STRUCT
typedef struct {
    DASH_MSG_Peripherals peripherals;
	DASH_MSG_Accumulator accumulator;
    DASH_MSG_Motor motor;
	SteerButtons buttons;
	
} DASH_STEER_CAN_Data;

void parse_can_message_motor_off(uint16_t data[4],DASH_MSG_Motor *motor);
void parse_can_message_peripherals(uint16_t data[4], DASH_MSG_Peripherals *peripherals);
void parse_can_message_accumulator(uint16_t data[4],DASH_MSG_Accumulator *info);
void parse_can_message_mode(uint16_t data[4], _MODES *mode);
void parse_can_message_tq_mode(uint16_t data[4], uint8_t *tq_mode);

#endif
