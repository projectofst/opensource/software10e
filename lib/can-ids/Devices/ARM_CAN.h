#ifndef __ARM_CAN_H__
#define __ARM_CAN_H__

/*Can interface for IIB*/

#include <stdint.h>
#include "../CAN_IDs.h"

//Message ids

typedef struct{

    int16_t FL; // [0.001Nm]
    int16_t FR; // [0.001Nm]
    int16_t RL; // [0.001Nm]
    int16_t RR; // [0.001Nm]


}ARM_Torque;

typedef struct{

    uint16_t motor_num;
    int16_t  torque;
    int16_t  speed_pos;
    uint16_t speed_neg;

}ARM_Torque_Setpoint;

typedef struct{

    uint16_t motor_num;
    int16_t  speed;
    int16_t  torque_pos;
    uint16_t torque_neg;

}ARM_Speed_Setpoint;

#define MSG_ID_ARM_DRS 32

typedef struct{

    uint16_t bottom_flap;
    uint16_t top_flap;
    uint16_t state;         //[0: CLOSE, 1: OPEN]

}ARM_DRS_Angle;

#define MSG_ID_ARM_STATUS 35

typedef struct{

    uint8_t ARM_control_ok;

}ARM_status;

typedef struct{
	int16_t integral_state;
	int16_t desired_yaw_rate;
	int16_t error_yaw_rate;
}YMC_Feedback;

typedef struct{
    
    int8_t init_power_estimation;   // [kW]
    int8_t final_power_estimation;  // [kW]
    float scaling_factor;
    
}Power_Limiter;

typedef struct{

    ARM_Torque      TL;
    ARM_Torque      LC;
    ARM_Torque      max_power_regen;
    ARM_Torque      max_braking_regen;
    ARM_Torque      basic_regen;
    ARM_Torque      distributor;
    YMC_Feedback    YMC_feedback;
    Power_Limiter   PL;
    int16_t yaw_moment_steering;
    int16_t yaw_moment_feedback;

}ARM_Control_Action_Log;

typedef struct{

    ARM_Torque      temp_motor;
    ARM_Torque      temp_inverter;
    ARM_Torque      temp_IGBT;
    ARM_Torque      motor_power;
    ARM_Torque      i2t;
    ARM_Torque      net_derating;
    uint16_t        inverter_voltage_charge;
    uint16_t        inverter_voltage_discharge;

}ARM_Torque_Derating_Log;

typedef struct{

    uint8_t battery_SoC;         // [kW]
    uint8_t cell_temp_discharge; // [kW]
    uint8_t cell_temp_charge;    // [-kW]
    uint8_t cell_min_voltage;    // [kW]
    uint8_t cell_max_voltage;    // [-kW]
    uint8_t net_power;           // [kW]
    uint8_t net_regen_power;     // [-kW]

}ARM_Power_Derating_Log;

#define MSG_ID_ARM_CONTROL_ACTION_TL                        40      // TL[4]
#define MSG_ID_ARM_CONTROL_ACTION_LC                        41      // LC[4]
#define MSG_ID_ARM_CONTROL_ACTION_BRAKING_REGEN             42      // Max Braking Regen[4]
#define MSG_ID_ARM_CONTROL_ACTION_POWER_BASIC_REGEN         43      // 0: Torque Max Power Regen Front, 0: Torque Max Power Regen Rear, 0: Torque Basic Regen Front, 0: Torque Basic Regen Rear
#define MSG_ID_ARM_CONTROL_ACTION_DISTRIBUTOR               44      // Torque Distributor[4]
#define MSG_ID_ARM_CONTROL_ACTION_POWER_LIMITER_YAW_MOMENT  45      // 0: PL Scaling Factor, 1: PL Initial Power Estimation, 2: Mz steering, 3: Mz feedback
#define MSG_ID_ARM_CONTROL_ACTION_YMC_STRAIGHT_CORNER_FLAG  46      // 0: YMC feedback integral_state, 1: YMC feedback desired_yaw_rate, 2: YMC feedback error_yaw_rate, 3: STRAIGHT/CORNER flag
#define MSG_ID_ARM_SAFETY_INVERTER_VOLT_TORQUE_DERATING     47      // 0: Inverter voltage charge, 1: Inverter voltage discharge
#define MSG_ID_ARM_SAFETY_MOTOR_TEMP_TORQUE_DERATING        48      // torque_temp_motor_derating[4]
#define MSG_ID_ARM_SAFETY_INVERTER_TEMP_TORQUE_DERATING     49      // torque_temp_inverter_derating[4]
#define MSG_ID_ARM_SAFETY_IGBT_TEMP_TORQUE_DERATING         50      // torque_temp_IGBT_derating[4]
#define MSG_ID_ARM_SAFETY_MOTOR_POWER_TORQUE_DERATING       51      // torque_motor_power_derating[4]
#define MSG_ID_ARM_SAFETY_I2T_TORQUE_DERATING               52      // torque_i2t_derating[4]
#define MSG_ID_ARM_SAFETY_POWER_DERATING                    53      // 0: Cell Temp Charge, 1: Cell Temp Discharge, 2: Cell Min Volt, 3: Cell Max Volt
#define MSG_ID_ARM_SAFETY_NET_TORQUE_DERATING               54      // net_torque_derating[4]
#define MSG_ID_ARM_SAFETY_NET_POWER_DERATING                55      // 0: net_power_derating, 1: net_regen_power_derating
#define MSG_ID_ARM_SAFETY_I2T_VOLUME                        56      // i2t volume[4]
#define MSG_ID_ARM_SAFETY_I2T_TRIGGER_TIME                  57      // i2t trigger_time[4]


#define MSG_ID_ARM_DEBUG_GENERIC                62

// ARM set parameters ids
#define P_ARM_VERSION_0                         0
#define P_ARM_VERSION_1                         1
#define P_ARM_VERSION_2                         2
#define P_ARM_VERSION_3                         3
#define P_ARM_CONTROL_STATE                     4
#define P_ARM_OPERATIONAL_TORQUE_FRONT          5
#define P_ARM_OPERATIONAL_TORQUE_REAR           6
#define P_ARM_OPERATIONAL_REGEN_TORQUE_FRONT    7
#define P_ARM_OPERATIONAL_REGEN_TORQUE_REAR     8
#define P_ARM_OPERATIONAL_POWER                 9
#define P_ARM_OPERATIONAL_POWER_REGEN           10
#define P_ARM_OPERATIONAL_MOTOR_SPEED           11
#define P_ARM_TRACTION_MODE                     13
#define P_ARM_TL_STATE                          14
#define P_ARM_LATERAL_CONTROL_STATE             15
#define P_ARM_LAUNCH_CONTROL_STATE              16
#define P_ARM_LATERAL_CONTROL_MODE              17
#define P_ARM_POWER_LIMITER_STATE               18
#define P_ARM_REGEN_MODE                        19
#define P_ARM_DRS_MODE                          20
#define P_ARM_DYNAMIC_EVENT                     21
#define P_ARM_TL_GAIN                           22
#define P_ARM_YRC_K_UNDERSTEER                  23
#define P_ARM_YRC_Kp_SCALING_FACTOR             24
#define P_ARM_YRC_Ki_SCALING_FACTOR             25
#define P_ARM_YRC_DELTA_GAIN                    26
#define P_ARM_LAUNCH_CONTROL_GAIN_BETA          27
#define P_ARM_LAUNCH_CONTROL_GAIN_Ks            28
#define P_ARM_LAUNCH_CONTROL_GAIN_DESIRED_SR    29
#define P_ARM_DIST_DELTA_Mz_GAIN                30
#define P_ARM_DIST_DELTA_Fx_SATURATION          31
#define P_ARM_STRAIGHT_STEERING_RANGE           32
#define P_ARM_STRAIGHT_PEDAL_THRESHOLD          33
#define P_ARM_STRAIGHT_EXTRA                    34  // Not used atm
#define P_ARM_DERATING_SoC_W                    35
#define P_ARM_DERATING_SoC_MIN                  36
#define P_ARM_DERATING_TEMP_CELL_DISCHARGE_W    37
#define P_ARM_DERATING_TEMP_CELL_DISCHARGE_MAX  38
#define P_ARM_DERATING_TEMP_CELL_CHARGE_W       39
#define P_ARM_DERATING_TEMP_CELL_CHARGE_MAX     40
#define P_ARM_DERATING_VOLT_CELL_DISCHARGE_W    41
#define P_ARM_DERATING_VOLT_CELL_DISCHARGE_MIN  42
#define P_ARM_DERATING_VOLT_CELL_CHARGE_W       43
#define P_ARM_DERATING_VOLT_CELL_CHARGE_MAX     44
#define P_ARM_DERATING_INV_VOLT_CHARGE_W        45
#define P_ARM_DERATING_INV_VOLT_CHARGE_MAX      46
#define P_ARM_DERATING_INV_VOLT_DISCHARGE_W     47
#define P_ARM_DERATING_INV_VOLT_DISCHARGE_MIN   48
#define P_ARM_DERATING_TEMP_MOTOR_W             49
#define P_ARM_DERATING_TEMP_MOTOR_MAX           50
#define P_ARM_DERATING_TEMP_INVERTER_W          51
#define P_ARM_DERATING_TEMP_INVERTER_MAX        52
#define P_ARM_DERATING_TEMP_IGBT_W              53
#define P_ARM_DERATING_TEMP_IGBT_MAX            54
#define P_ARM_REGEN_BASIC_TORQUE_FRONT          55
#define P_ARM_REGEN_BASIC_TORQUE_REAR           56
#define P_ARM_REGEN_POWER                       57
#define P_ARM_REGEN_BRAKE_BIAS                  58
#define P_ARM_ENDURANCE_FINISHER_TOTAL_LAPS     59
#define P_ARM_ENDURANCE_FINISHER_TOTAL_ENERGY   60
#define P_ARM_ENDURANCE_FINISHER_LAP_N          61


typedef enum{

    IDLE = 0,
    ACTIVE = 1,

}CONTROLLER_STATE;

typedef enum{

    RWD,
    FWD,
    AWD,

}TRACTION_MODE;

typedef enum{

    FEEDBACK,
    STEERING_PROPORTIONAL,

}LATERAL_CONTROL_MODE;

typedef enum{

    NO_REGEN,
    MAX_REGEN,
    MAX_BRAKING,
    BASIC_REGEN,

}REGEN_MODE;

typedef enum{

    ACCELERATION,
    SKIDPAD,
    AUTOCROSS,
    ENDURANCE,

}DYNAMIC_EVENT;

typedef enum{

    ARM,
    STEERING_WHEEL,

}DRS_MODE;


//CAN structure
typedef struct{

    ARM_Torque_Setpoint     torque_setpoint;
    ARM_Speed_Setpoint      speed_setpoint;
    ARM_DRS_Angle           drs_angle;
    ARM_status              status;
    ARM_Control_Action_Log  control_log;
    ARM_Torque_Derating_Log torque_derating_log;
    ARM_Power_Derating_Log  power_derating_log;

}ARM_CAN_data;

//parse functions
void parse_ARM_torque_setpoint_message(uint16_t data[4], ARM_Torque_Setpoint *torque_setpoint);
void parse_ARM_speed_setpoint_message(uint16_t data[4], ARM_Speed_Setpoint *speed_setpoint);
void parse_ARM_DRS_state(uint16_t data[4], ARM_DRS_Angle *drs_angle);
void parse_ARM_status_message(uint16_t data[4], ARM_status *status);
void parse_ARM_CA_TL(uint16_t data[4], ARM_Control_Action_Log *control_log);
void parse_ARM_CA_LC(uint16_t data[4], ARM_Control_Action_Log *control_log);
void parse_ARM_CA_max_braking_regen(uint16_t data[4], ARM_Control_Action_Log *control_log);
void parse_ARM_CA_power_basic_regen(uint16_t data[4], ARM_Control_Action_Log *control_log);
void parse_ARM_CA_distributor(uint16_t data[4], ARM_Control_Action_Log *control_log);
void parse_ARM_CA_power_limiter_yaw_moment(uint16_t data[4], ARM_Control_Action_Log *control_log);
void parse_ARM_CA_YMC_feedback(uint16_t data[4], ARM_Control_Action_Log *control_log);
void parse_ARM_TD_inverter_voltage(uint16_t data[4], ARM_Torque_Derating_Log *torque_derating_log);
void parse_ARM_TD_temp_motor(uint16_t data[4], ARM_Torque_Derating_Log *torque_derating_log);
void parse_ARM_TD_temp_inverter(uint16_t data[4], ARM_Torque_Derating_Log *torque_derating_log);
void parse_ARM_TD_temp_IGBT(uint16_t data[4], ARM_Torque_Derating_Log *torque_derating_log);
void parse_ARM_TD_motor_power(uint16_t data[4], ARM_Torque_Derating_Log *torque_derating_log);
void parse_ARM_TD_i2t(uint16_t data[4], ARM_Torque_Derating_Log *torque_derating_log);
void parse_ARM_TD_net_derating(uint16_t data[4], ARM_Torque_Derating_Log *torque_derating_log);
void parse_ARM_power_derating(uint16_t data[4],ARM_Power_Derating_Log *power_derating_log);
void parse_ARM_net_power_derating(uint16_t data[4], ARM_Power_Derating_Log *power_derating_log);
void parse_can_ARM(CANdata message, ARM_CAN_data *data);


#endif
