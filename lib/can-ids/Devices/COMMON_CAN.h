#ifndef __COMMON_CAN_H__
#define __COMMON_CAN_H__

#include <stdbool.h>
#include <stdint.h>
#include "../CAN_IDs.h"


/* Common msg ids are declared in CAN-IDs since they are global to all devices
 */

typedef enum{
    RTD_STEP_DASH_BUTTON = 0b0000,
    RTD_STEP_TE_OK       = 0b0001,
    RTD_STEP_TE_NOK      = 0b1001,
    RTD_STEP_INV_OK      = 0b0010,
    RTD_STEP_INV_NOK     = 0b1010,
    RTD_STEP_DCU         = 0b0011,
    RTD_STEP_INV_GO      = 0b0100,
    RTD_STEP_DASH_LED    = 0b0101,  
} RTD_ON;

typedef enum{
    RTD_DASH_OFF        = 0,
    RTD_INV_CONFIRM     = 1,
	RTD_INV_OFF         = 2,
} RTD_OFF;

typedef enum{
    TS_DASH_BUTTON  = 0,
    TS_MASTER_ON    = 1,
    TS_MASTER_OFF   = 2,
} TS_STEP;

/*enum RTD_ERROR{
};*/

typedef struct{
    uint8_t RCON;

} COMMON_MSG_RESET;

typedef struct{
    uint8_t type;
}COMMON_MSG_TRAP;


typedef struct{
    RTD_ON step;
//    RTD_ERROR error;
} COMMON_MSG_RTD_ON;

typedef struct{
    
    uint8_t wrong_dev_id;
    uint8_t right_dev_id;

} COMMON_MSG_PARSE_ERROR;

typedef struct{
    RTD_OFF step;

} COMMON_MSG_RTD_OFF;

typedef struct{
    TS_STEP step;
} COMMON_MSG_TS;

typedef struct{
    unsigned int type;
    unsigned int parse_error_id; /*find out this shit*/
} COMMON_MSG_LOG;

typedef struct {
    uint16_t inverters_power;
} INVERTERS_POWER;

void parse_can_common_trap(CANdata message, COMMON_MSG_TRAP *trap);
void parse_can_common_reset(CANdata message, COMMON_MSG_RESET *RESET);
void parse_can_common_parse_error(CANdata msg, COMMON_MSG_PARSE_ERROR *p);
void parse_can_common_TS(CANdata message, COMMON_MSG_TS *TS);
void parse_can_common_RTD(CANdata message, COMMON_MSG_RTD_ON *RTD_ON);
void parse_can_common_RTD_OFF(CANdata msg, COMMON_MSG_RTD_OFF *rtd_off_msg);
void parse_can_power_inverters(CANdata message, INVERTERS_POWER *inv_power);

CANdata make_reset_msg(uint8_t dev_id, uint8_t RCON);
CANdata make_parse_error_msg(uint8_t dev_id, uint8_t wrong_id, uint8_t right_id);
CANdata make_rtd_on_msg(uint8_t dev_id, RTD_ON step);
CANdata make_rtd_off_msg(uint8_t dev_id, RTD_OFF step);
CANdata make_ts_step_msg(uint8_t dev_id, TS_STEP step);

#endif
