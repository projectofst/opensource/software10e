#include "STEER_CAN.h"

#include <stdint.h>

#include "../CAN_IDs.h"

void parse_can_message_steering_wheel_status(uint16_t data[4], STEERING_WHEEL_MSG_status* steer)
{
    steer->ME0Pos = data[0] & 15; // bits [3:0]
    steer->ME1Pos = data[0] & 240; // bits [7:4]
    steer->ME2Pos = data[0] & 3840; // bits [11:8]

    return;
}

void parse_can_steering_wheel(CANdata message, STEERING_WHEEL_MSG_status* steer)
{

    if (message.dev_id != DEVICE_ID_STEERING_WHEEL) {
        /*FIXME: send info for error logging*/
        return;
    } else {
        switch (message.msg_id) {
        case MSG_ID_STEERING_WHEEL_STATUS:
            parse_can_message_steering_wheel_status(message.data, steer);
            break;
        }
    }
    return;
}
