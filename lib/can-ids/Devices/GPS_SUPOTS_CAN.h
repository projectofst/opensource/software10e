#ifndef __GPS_SUPOTS_CAN_H__
#define __GPS_SUPOTS_CAN_H__

#include "../CAN_IDs.h"

/* SET and GET parameters */
#define P_GPS_SUPOTS_VERSION_0                      0       /* NEVER CHANGE THIS 4 DEFINES */
#define P_GPS_SUPOTS_VERSION_1                      1
#define P_GPS_SUPOTS_VERSION_2                      2
#define P_GPS_SUPOTS_VERSION_3                      3
#define P_GPS_SUPOTS_DATE_CAN_FREQ                  4
#define P_GPS_SUPOTS_UTC_TIME_CAN_FREQ              5
#define P_GPS_SUPOTS_LATITUDE_CAN_FREQ              6
#define P_GPS_SUPOTS_LONGITUDE_CAN_FREQ             7
#define P_GPS_SUPOTS_GROUND_SPEED_CAN_FREQ          8
#define P_GPS_SUPOTS_POT_ADC_VALUE_CAN_FREQ         9
#define P_GPS_SUPOTS_VELOCITY_CAN_FREQ              10
#define P_GPS_SUPOTS_NORMAL_LOAD_CAN_FREQ           11
#define P_GPS_SUPOTS_ADC_ACQUISITION_FREQ           12
#define P_GPS_SUPOTS_GPS_STATE                      13

typedef enum{
    NOT_PLUGGED_IN = 0,
    PLUGGED_IN = 1
}GpsStatus;

#define MSG_ID_GPS_SUPOTS_DATE              32
#define MSG_ID_GPS_SUPOTS_UTC_TIME          33
#define MSG_ID_GPS_SUPOTS_LATITUDE          34
#define MSG_ID_GPS_SUPOTS_LONGITUDE         35
#define MSG_ID_GPS_SUPOTS_GROUND_SPEED      36
#define MSG_ID_GPS_SUPOTS_POT_ADC_VALUE     37
#define MSG_ID_GPS_SUPOTS_VELOCITY          38
#define MSG_ID_GPS_SUPOTS_NORMAL_LOAD       39
#define MSG_ID_GPS_SUPOTS_GPS_ERROR_STATE   40



typedef struct{
    uint16_t day;
    uint16_t month;
    uint16_t year;
}GPS_SUPOTS_MSG_Date;

typedef struct{
    uint16_t hours;
    uint16_t minutes;
    uint16_t seconds;
    uint16_t miliseconds;
}GPS_SUPOTS_MSG_Time;

typedef struct{
    uint16_t dg;
    uint16_t min;
    uint16_t dec_min;
}GPS_SUPOTS_MSG_Coordenates;

typedef struct{
    uint16_t pot1;
    uint16_t pot2;
}GPS_SUPOTS_MSG_Pot_adc_value;

typedef struct{
    uint16_t FL;
    uint16_t FR;
    uint16_t RL;
    uint16_t RR;
}GPS_SUPOTS_MSG_Normal_load;


typedef struct{
    GPS_SUPOTS_MSG_Date date;
    GPS_SUPOTS_MSG_Time time;
    GPS_SUPOTS_MSG_Coordenates latitude;
    GPS_SUPOTS_MSG_Coordenates longitude;
    uint16_t ground_speed;
    GPS_SUPOTS_MSG_Pot_adc_value pot_adc_value;
    uint16_t velocity;
    GPS_SUPOTS_MSG_Normal_load normal_load;
}GPS_SUPOTS_PARSED_Data;

void parse_can_gps_supots(CANdata message, GPS_SUPOTS_PARSED_Data *data);
void parse_gps_supots_message_date(uint16_t data[4], GPS_SUPOTS_MSG_Date *date);
void parse_gps_supots_message_utc_time(uint16_t data[4], GPS_SUPOTS_MSG_Time *time);
void parse_gps_supots_message_latitude(uint16_t data[4], GPS_SUPOTS_MSG_Coordenates *latitude); 
void parse_gps_supots_message_longitude(uint16_t data[4], GPS_SUPOTS_MSG_Coordenates *longitude); 
void parse_gps_supots_message_ground_speed(uint16_t data[4], uint16_t *ground_speed); 
void parse_gps_supots_message_pot_adc_value(uint16_t data[4], GPS_SUPOTS_MSG_Pot_adc_value *pot_adc_value); 
void parse_gps_supots_message_velocity(uint16_t data[4], uint16_t *velocity);
void parse_gps_supots_message_normal_load(uint16_t data[4], GPS_SUPOTS_MSG_Normal_load *normal_load);



#endif
