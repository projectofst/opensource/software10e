#ifndef __INTERFACE_CAN_H__
#define __INTERFACE_CAN_H__

#include "../CAN_IDs.h"
#include "TE_CAN.h"
//#include "can-ids/Devices/STEER_CAN.h"

#define CMD_ID_INTERFACE_DCU_TOGGLE				32
#define CMD_ID_INTERFACE_DEBUG_TOGGLE			33
#define CMD_ID_INTERFACE_SET_PEDAL_THRESHOLD	34
#define CMD_ID_INTERFACE_TOGGLE_BMS_FANS		35
#define CMD_ID_INTERFACE_TOGGLE_BMS_VERBOSE		36
#define CMD_ID_INTERFACE_TOGGLE_BMS_CHARGING	37
#define CMD_ID_INTERFACE_BMS_FAKE_ERROR			38
#define CMD_ID_INTERFACE_BMS_RESET_ERROR		39
#define CMD_ID_INTERFACE_TE_TOGGLE_RAW_MODE     40
#define MSG_ID_INTERFACE_INVERTERS              41
#define CMD_ID_INTERFACE_SET_STEER_THRESHOLD    42
#define CMD_ID_INTERFACE_START_LOG              43
#define CMD_ID_INTERFACE_SET_DRS                44
#define MSG_ID_INTERFACE_ARM1                   45
#define MSG_ID_INTERFACE_TE_LIMITS_REQUEST		46



/*enums*/
typedef enum {
    DCU_FANS      = 0,
    DCU_PUMPS1    = 1,
    DCU_PUMPS2    = 2,
    DCU_CUA       = 3,
    DCU_CUB       = 4,
    DCU_SC        = 5,
    DCU_BLUE_TSAL = 6,
    DCU_DCDC_DRS  = 7,
}DCU_TOGGLE;

typedef enum{
    DEBUG_TOGGLE_TORQUE_ENCODER  = 0,
    DEBUG_TOGGLE_DCU             = 1,
    DEBUG_TOGGLE_DASH            = 2,
}DEBUG_TOGGLE;


typedef struct{
    DCU_TOGGLE code;

} INTERFACE_MSG_DCU_TOGGLE;


typedef struct{
    DEBUG_TOGGLE device;
} INTERFACE_MSG_DEBUG_TOGGLE;

typedef struct{
	uint16_t max_rpm;
	int16_t max_torque_front;
    int16_t max_torque_rear;
    uint16_t reset;
}INTERFACE_MSG_INVERTERS;

typedef struct{
    uint16_t mode;
    int16_t gain;
    float tl;
    uint16_t power;
}INTERFACE_MSG_ARM1;

typedef struct{
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
}INTERFACE_MSG_START_LOG_DATE;


typedef struct {
    uint16_t drs_sig;
} INTERFACE_MSG_SET_DRS;

typedef struct {
    uint16_t limit;
    uint16_t sensor;
} INTERFACE_MSG_TE_LIMIT;


typedef struct
{
    uint16_t hard_breaking_press_limit;
}INTERFACE_MSG_HARD_BREAKING;

// MAIN STRUCT
typedef struct {
    INTERFACE_MSG_DCU_TOGGLE dcu_toggle;
    INTERFACE_MSG_DEBUG_TOGGLE debug_toggle;
    INTERFACE_MSG_PEDAL_THRESHOLDS pedal_thresholds;
    INTERFACE_MSG_INVERTERS inverters_limitation;
    //INTERFACE_MSG_STEER_THRESHOLDS steer_thresholds;
    INTERFACE_MSG_START_LOG_DATE start_log;
    INTERFACE_MSG_SET_DRS set_drs;
    INTERFACE_MSG_ARM1 arm1;
	INTERFACE_MSG_TE_LIMIT limit_request;
    INTERFACE_MSG_HARD_BREAKING hard_breaking;
} INTERFACE_CAN_Data;

void parse_can_interface(CANdata message, INTERFACE_CAN_Data *data);
void parse_can_message_debug_toggle(uint16_t data0, INTERFACE_MSG_DEBUG_TOGGLE *debug_toggle);
void parse_can_message_dcu_toggle(uint16_t data[4], INTERFACE_MSG_DCU_TOGGLE *dcu_toggle);
void parse_can_message_set_pedal_thresholds(CANdata *message, INTERFACE_CAN_Data *data);
//void parse_can_message_set_steer_thresholds(CANdata message, INTERFACE_CAN_Data *data);
void parse_can_message_interface_inverters(uint16_t data[3], INTERFACE_MSG_INVERTERS *inverters_limitation);
void parse_can_message_interface_start_log(CANdata message, INTERFACE_CAN_Data *data);
void parse_can_message_interface_arm1 (CANdata msg, INTERFACE_MSG_ARM1 *arm1);
void parse_can_message_interface_te_limits(CANdata message, INTERFACE_CAN_Data *data);

#endif
