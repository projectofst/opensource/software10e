#ifndef __BMS_MASTER_CAN_H__
#define __BMS_MASTER_CAN_H__

#include <stdbool.h>
#include <stdint.h>

#include "../CAN_IDs.h"

#define P_BMS_VERSION_0 0
#define P_BMS_VERSION_1 1
#define P_BMS_VERSION_2 2
#define P_BMS_VERSION_3 3
#define P_BMS_CELL_LIMIT_MAX_T 4 // SET
#define P_BMS_CELL_LIMIT_MIN_T 5 // SET
#define P_BMS_CELL_LIMIT_MAX_V 6 // SET
#define P_BMS_CELL_LIMIT_MIN_V 7 // SET
#define P_BMS_BAT_LIMIT_MAX_V 8  // SET
#define P_BMS_BAT_CHARGING_DELTA 9	 // SET
#define P_BMS_MAX_DISCH_CHANNEL_TEMP 10 // SET


#define MSG_ID_MASTER_CELL_TEMPERATURE_INFO	56
#define MSG_ID_MASTER_CELL_VOLTAGE_INFO		57
#define MSG_ID_MASTER_ENERGY_METER			58
#define MSG_ID_MASTER_SC_OPEN				62
/*
 * Prioritary IDs defined in CAN_ID.h
#define MSG_ID_MASTER_TS_STATE				58
#define MSG_ID_MASTER_IMD_ERROR				59
#define MSG_ID_MASTER_AMS_ERROR				60
#define MSG_ID_MASTER_STATUS				63
*/

// BMS MASTER
#define P_BMS_MASTER_VERSION_ID				0
#define P_BMS_MASTER_CELL_LIMIT_MAX_V		1
#define P_BMS_MASTER_CELL_LIMIT_MIN_V		2

/**
 * @brief Reasons for the TS to be turned OFF
 *
 * Possible values of "reason" field in MASTER_MSG_TS
 */
typedef enum {
    TS_OFF_REASON_DASH_BUTTON,
    TS_OFF_REASON_OVERVOLTAGE,
    TS_OFF_REASON_UNDERVOLTAGE,
    TS_OFF_REASON_OVERCURRENT_IN,
    TS_OFF_REASON_OVERCURRENT_OUT,
    TS_OFF_REASON_OVERTEMPERATURE,
    TS_OFF_REASON_UNDERTEMPERATURE,
	TS_OFF_REASON_MIN_CELL_LTC,
    TS_OFF_REASON_IMD,
    TS_OFF_REASON_FAKE_ERROR,
    TS_OFF_REASON_OTHER,
	TS_OFF_REASON_NO_ERROR,
} MASTER_MSG_TS_OFF_Reason;

/**
 * @brief Message contents for ID MSG_ID_MASTER_TS_STATE
 *
 * This message serves to let other modules know that the TS has been turned ON
 * or OFF and the reason why it did (only relevant for turning the TS OFF).
 */
typedef struct {
	bool state     : 1;
    MASTER_MSG_TS_OFF_Reason reason : 7;
} MASTER_MSG_TS;

/**
 * @brief Message contents for ID MSG_ID_MASTER_ENERGY_METER
 *
 * In this message the BMS master sends energy meter data.
 */
typedef struct {
	uint16_t voltage;   /**< [mV*10] Total accumulator voltage */

    /**
     * @brief [mA*10] Accumulator Current
     *
     * Positive if current flowing out of the accumulator (discharging).
     * Negative if current flowing into the accumulator (regeneration/charging).
     */
	int16_t current;

    /**
     * @brief [W*10] Accumulator Power
     *
     * Positive if power is being sourced by the accumulator (discharging).
     * Negative if power is being received by the accumulator
     * (regeneration/charging).
     */
	int16_t power;

	uint16_t SoC; /**< [%*0.1] Accumulator State of Charge (SoC) */
} MASTER_MSG_Energy_Meter;

/**
 * @brief Message contents for ID  MSG_ID_MASTER_STATUS
 */
typedef struct {
	union {
		struct {
            /**
             * @name AMS and IMD signals state
             * @{
             */
			bool AMS_OK                 : 1;
			bool IMD_OK                 : 1;
            /**@}*/

            /**
             * @name AMS and IMD latch state
             * @{
             */
			bool IMD_LATCH              : 1;
			bool AMS_LATCH              : 1;
            /**@}*/

            /**
             * @name Relay states
             * @{
             */
			bool AIR_positive           : 1;
			bool AIR_negative           : 1;
			bool PreChK                 : 1;
			bool DisChK                 : 1;
            /**@}*/

            /**
             * @name Shutdown circuit detection points
             * @{
             */
			bool SC_DCU_IMD             : 1;
			bool SC_IMD_AMS             : 1;
			bool SC_AMS_DCU             : 1;
			bool SC_TSMS_Relays         : 1;
            /**@}*/

            /**
             * @name Shutdown circuit delay circuit signals
             * @{
             */
			bool shutdown_above_minimum : 1;
			bool shutdown_open          : 1;
            /**@}*/

			bool verbose				: 1;
			bool ts_on					: 1;
		};
		uint16_t status;
	};
} MASTER_MSG_Status;

/**
 * @brief Message contents for MSG_ID_MASTER_CELL_VOLTAGE_INFO
 *
 * Summary of cell information about voltages
 */
typedef struct {
	uint16_t min_voltage;
	uint16_t mean_voltage;
	uint16_t max_voltage;
	uint8_t balance_target[2];
} MASTER_MSG_Battery_Voltage_Info;


/**
 * @brief Message contents for MSG_ID_MASTER_CELL_TEMPERATURE_INFO
 *
 * Summary of cell information about temperature
 */
typedef struct {
	uint16_t min_temperature;
	uint16_t mean_temperature;
	uint16_t max_temperature;
	uint8_t hottest_cell[2];
} MASTER_MSG_Battery_Temp_Info;

/**
 * @brief Structure holding all master messages information
 */
typedef struct {
	MASTER_MSG_Status status;
	MASTER_MSG_TS TS;
	MASTER_MSG_Energy_Meter energy_meter;
	bool imd_error;
	bool ams_error;
	bool sc_open;
	MASTER_MSG_Battery_Voltage_Info cell_voltage_info;
	MASTER_MSG_Battery_Temp_Info cell_temp_info;
} MASTER_MSG_Data;

/*
 * Parse Function Declaration
 */
void parse_can_message_master(CANdata msg, MASTER_MSG_Data *data);
void parse_can_message_master_ts(CANdata msg, MASTER_MSG_TS *ts);
void parse_can_message_master_energy_meter(CANdata msg, MASTER_MSG_Energy_Meter
		*energy_meter);
void parse_can_message_master_cell_voltage_info(CANdata msg,
		MASTER_MSG_Battery_Voltage_Info *cell_voltage_info);
void parse_can_message_master_cell_temperature_info(CANdata msg,
		MASTER_MSG_Battery_Temp_Info *cell_temp_info);

#endif
