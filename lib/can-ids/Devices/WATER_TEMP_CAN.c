#include "WATER_TEMP_CAN.h"
#include "../CAN_IDs.h"
#include <stdint.h>

void parse_can_message_wt_temperature(CANdata msg, WT_Temperature* temperature)
{
    temperature->timestamp = msg.data[0];
    temperature->temp1 = msg.data[1];
    temperature->temp2 = msg.data[2];
    temperature->temp3 = msg.data[3];
}

void parse_can_message_wt_flow_rate(CANdata msg, WT_Flow_Rate* flow_rate)
{
    flow_rate->flow_rate_1 = msg.data[0];
    flow_rate->flow_rate_2 = msg.data[1];
}

void parse_can_message_wt(CANdata msg, WT_Data* data)
{
    switch (msg.msg_id) {
    case MSG_ID_WATER_TEMP_TEMPERATURE1:
        parse_can_message_wt_temperature(msg, &(data->temperature1));
        break;
    case MSG_ID_WATER_TEMP_TEMPERATURE2:
        parse_can_message_wt_temperature(msg, &(data->temperature2));
        break;
    case MSG_ID_WATER_TEMP_FLOW_RATE:
        parse_can_message_wt_flow_rate(msg, &(data->flow_rate));
        break;
    }
}
