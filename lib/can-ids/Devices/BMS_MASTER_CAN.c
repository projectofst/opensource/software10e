#include <stdint.h>

#include "../CAN_IDs.h"
#include "BMS_MASTER_CAN.h"

void parse_can_message_master_ts(CANdata msg, MASTER_MSG_TS* ts)
{
    ts->state = msg.data[0] & 1;
    ts->reason = (MASTER_MSG_TS_OFF_Reason)(msg.data[0] >> 1);
}

void parse_can_message_master_energy_meter(CANdata msg, MASTER_MSG_Energy_Meter* energy_meter)
{
    energy_meter->voltage = msg.data[0];
    energy_meter->current = (int16_t)msg.data[1];
    energy_meter->power = (int16_t)msg.data[2];
    energy_meter->SoC = msg.data[3];
}

void parse_can_message_master_cell_voltage_info(CANdata msg,
    MASTER_MSG_Battery_Voltage_Info* cell_voltage_info)
{

    cell_voltage_info->min_voltage = msg.data[0];
    cell_voltage_info->mean_voltage = msg.data[1];
    cell_voltage_info->max_voltage = msg.data[2];
    cell_voltage_info->balance_target[0] = (msg.data[3] & 0xFF);
    cell_voltage_info->balance_target[1] = ((msg.data[3] >> 8) & 0xFF);
}

void parse_can_message_master_cell_temperature_info(CANdata msg,
    MASTER_MSG_Battery_Temp_Info* cell_temp_info)
{
    cell_temp_info->min_temperature = msg.data[0];
    cell_temp_info->mean_temperature = msg.data[1];
    cell_temp_info->max_temperature = msg.data[2];
    cell_temp_info->hottest_cell[0] = (msg.data[3] & 0xFF);
    cell_temp_info->hottest_cell[1] = ((msg.data[3] >> 8) & 0xFF);
}

void parse_can_message_master_status(CANdata msg, MASTER_MSG_Status* status)
{
    status->status = msg.data[0];
    return;
}

void parse_can_message_master(CANdata msg, MASTER_MSG_Data* data)
{
    switch (msg.msg_id) {
    case MSG_ID_MASTER_CELL_TEMPERATURE_INFO:
        parse_can_message_master_cell_temperature_info(msg,
            &(data->cell_temp_info));
        break;
    case MSG_ID_MASTER_CELL_VOLTAGE_INFO:
        parse_can_message_master_cell_voltage_info(msg,
            &(data->cell_voltage_info));
        break;
    case MSG_ID_MASTER_TS_STATE:
        parse_can_message_master_ts(msg, &(data->TS));
        break;
    case MSG_ID_MASTER_IMD_ERROR:
        data->imd_error = 1;
        break;
    case MSG_ID_MASTER_AMS_ERROR:
        data->ams_error = 1;
        break;
    case MSG_ID_MASTER_ENERGY_METER:
        parse_can_message_master_energy_meter(msg, &(data->energy_meter));
        break;
    case MSG_ID_MASTER_SC_OPEN:
        data->sc_open = 1;
        break;
    case MSG_ID_MASTER_STATUS:
        parse_can_message_master_status(msg, &(data->status));
        break;
    }
}
