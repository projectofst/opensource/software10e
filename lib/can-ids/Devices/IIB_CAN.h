#ifndef __IIB_CAN_H__
#define __IIB_CAN_H__

/*Can interface for IIB*/

#include <stdint.h>
#include "../CAN_IDs.h"

#define IIB_PARAMETER_NUMBER			26

//IIB FLASH parameters
#define P_IIB_VERSION_1                 0
#define P_IIB_VERSION_2                 1
#define P_IIB_VERSION_3                 2
#define P_IIB_VERSION_4                 3
#define P_IIB_REGEN                     4
#define P_IIB_FL_MAX_TORQUE_OP          5
#define P_IIB_FR_MAX_TORQUE_OP          6
#define P_IIB_RL_MAX_TORQUE_OP          7
#define P_IIB_RR_MAX_TORQUE_OP          8
#define P_IIB_FL_MAX_POWER_OP           9
#define P_IIB_FR_MAX_POWER_OP           10
#define P_IIB_RL_MAX_POWER_OP           11
#define P_IIB_RR_MAX_POWER_OP           12
#define P_IIB_DEBUG_MODE                13
#define P_IIB_I2T_SCALE_FACTOR          14
#define P_IIB_OPERATING_MODE            15
#define P_IIB_MAX_RPM                   16
#define P_IIB_FL_MAX_REGEN_TORQUE_OP    17
#define P_IIB_FR_MAX_REGEN_TORQUE_OP    18
#define P_IIB_RL_MAX_REGEN_TORQUE_OP    19
#define P_IIB_RR_MAX_REGEN_TORQUE_OP    20
#define P_IIB_AMK_CONTROL_MODE          21
#define P_IIB_FL_MAX_REGEN_POWER_OP		22
#define P_IIB_FR_MAX_REGEN_POWER_OP     23
#define P_IIB_RL_MAX_REGEN_POWER_OP     24
#define P_IIB_RR_MAX_REGEN_POWER_OP     25

#define IIB_SAVE_FLASH                  26

#define MSG_ID_IIB_DASH_SPEED			60
#define MSG_ID_IIB_DEBUG				50

#define MSG_ID_IIB_AMK_ACTUAL_VALUES_1  50

typedef struct{

    uint16_t    control_word;
    int16_t     actual_speed;
    int16_t     torque_current;
    int16_t     magnetizing_current;

}IIB_AMK_VALUES_1;

#define MSG_ID_IIB_AMK_ACTUAL_VALUES_2   51

typedef struct{

    int16_t     temp_motor;
    int16_t     temp_inverter;
    uint16_t    error_info;
    int16_t     temp_IGBT;

}IIB_AMK_VALUES_2;


#define MSG_ID_IIB_LIMITS       40

typedef struct{

    int motor_num;
    uint16_t max_operation_torque;
    uint16_t max_operation_power;
    uint16_t max_safety_torque;
    uint8_t max_safety_power;

}IIB_limits;

#define MSG_ID_IIB_REGEN_LIMITS 45

typedef struct {

    int motor_num;
    int16_t max_operating_regen_torque;
    int16_t max_safety_regen_torque;

}IIB_regen_limits;

#define MSG_ID_IIB_MOTOR_INFO   38

typedef struct {

    int     motor_num;
    uint8_t  temp_motor;
    int16_t actual_speed;
    int16_t current;
    int16_t torque;

}IIB_MOTOR_info;

#define MSG_ID_IIB_CAR_INFO     41

typedef struct{

    unsigned TE_OK            :1;
    unsigned ARM_OK           :1;
    unsigned ARM_STUPID       :1;
    unsigned arm_received_msg :1;
    unsigned SDC1             :1;
    unsigned SDC2             :1;
    unsigned CAR_RTD          :1;
    unsigned INV_HV           :1;
    unsigned TSAL_status      :1;

}IIB_CAR_status;

typedef struct {

    IIB_CAR_status status;
    uint16_t TE_percentage;
    uint16_t BMS_voltage;
    uint16_t ARM_stupid_msg_counter;

}IIB_CAR_info;

#define MSG_ID_IIB_INV_INFO     39

typedef struct{

    unsigned inverter_min_passed    :1;
    unsigned inverter_max_passed    :1;
    unsigned IGBT_min_passed        :1;
    unsigned IGBT_max_passed        :1;
    unsigned motor_min_passed       :1;
    unsigned motor_max_passed       :1;
    unsigned iib_air_min_passed     :1;
    unsigned iib_air_max_passed     :1;

}IIB_TEMP_status;


typedef struct{

    int i;
    unsigned BE_1               :1;
    unsigned BE_2               :1;
    unsigned inverter_active    :1;
    uint16_t temp_IGBT;
    uint16_t temp_inverter;
    uint16_t error_info;
    IIB_TEMP_status temperature_status;

}IIB_INV_info;

#define MSG_ID_IIB_INFO         37

typedef struct{

    uint16_t air_temp;
    uint16_t temp_2;
    uint16_t max_rpm;

    unsigned fan_on     :1;
    unsigned CU_power_A :1;
    unsigned CU_power_B :1;
    unsigned INV_RTD    :1;
    unsigned INV_ON     :1;
    unsigned REGEN_ON   :1;
    unsigned DEBUG_ON   :1;

}IIB_info;

#define MSG_ID_IIB_POWER    42

typedef struct{

    uint16_t power;

}IIB_total_power;


#define MSG_ID_IIB_DEBUG1_INFO   43

typedef struct {

    int i;
    int arm_received_msg;
    int te_alive_again;
    unsigned node_address;
    uint16_t max_i2t_current;
    uint16_t i2t_saturation_time;
    uint16_t cool_down_on_time;

}IIB_DEBUG1;

#define MSG_ID_IIB_DEBUG2_INFO   44

typedef struct {

    uint16_t amk_status_word;
    unsigned message1_id;
    unsigned message2_id;
    unsigned reference_id;

}IIB_DEBUG2;


typedef struct{

    IIB_limits limits;
    IIB_regen_limits regen_limits;
    IIB_MOTOR_info motor_info;
    IIB_CAR_info car_info;
    IIB_INV_info inv_info;
    IIB_info iib_info;
    IIB_total_power iib_power;
    IIB_DEBUG1 iib_debug1;
    IIB_DEBUG2 iib_debug2;
    IIB_AMK_VALUES_1 amk_values_1;
    IIB_AMK_VALUES_2 amk_values_2;

}IIB_CAN_data;


void parse_IIB_limits_message(uint16_t data[4], IIB_limits *limits);
void parse_IIB_regen_limits_message(uint16_t data[4], IIB_regen_limits *regen_limits);
void parse_IIB_motor_info_message(uint16_t data[4], IIB_MOTOR_info *motor_info);
void parse_IIB_car_info_message(uint16_t data[4], IIB_CAR_info *car_info);
void parse_IIB_inv_info_message(uint16_t data[4], IIB_INV_info *inv_info);
void parse_IIB_iib_info_message(uint16_t data[4], IIB_info *iib_info);
void parse_IIB_iib_power_messsage(uint16_t data[4], IIB_total_power *iib_power);
void parse_IIB_iib_debug1_message(uint16_t data[4], IIB_DEBUG1 *iib_debug1);
void parse_IIB_iib_debug2_message(uint16_t data[4], IIB_DEBUG2 *iib_debug2);
void parse_IIB_amk_actual_values_1(uint16_t data[4], IIB_AMK_VALUES_1 *amk_values_1);
void parse_IIB_amk_actual_values_2(uint16_t data[4], IIB_AMK_VALUES_2 *amk_values_2);
void parse_can_IIB(CANdata message, IIB_CAN_data *data);

#endif
