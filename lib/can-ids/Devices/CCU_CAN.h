#ifndef __CCU_CAN_H__
#define __CCU_CAN_H__

#include <stdint.h>

#include "../CAN_IDs.h"

#define MSG_ID_CCU_RESET						49
#define MSG_ID_CCU_STATUS						50
#define MSG_ID_CCU_PWM_FANS						51
#define MSG_ID_CCU_PWM_PUMP						52
#define MSG_ID_CCU_NTC_1                        53
#define MSG_ID_CCU_NTC_2                        55
#define MSG_ID_CCU_PRESSURE                     56
#define MSG_ID_CCU_FLOW                         57

#define CCU_SET_COOLING                         54

#endif
