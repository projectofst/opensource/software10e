#include "ARM_CAN.h"
#include "../CAN_IDs.h"
#include <stdint.h>

void parse_ARM_torque_setpoint_message(uint16_t data[4], ARM_Torque_Setpoint* torque_setpoint)
{

    torque_setpoint->motor_num = data[0];
    torque_setpoint->torque = (int16_t)(data[1]);
    torque_setpoint->speed_pos = data[2];
    torque_setpoint->speed_neg = (int16_t)(data[3]);
}

void parse_ARM_speed_setpoint_message(uint16_t data[4], ARM_Speed_Setpoint* speed_setpoint)
{

    speed_setpoint->motor_num = data[0];
    speed_setpoint->speed = (int16_t)(data[1]);
    speed_setpoint->torque_pos = data[2];
    speed_setpoint->torque_neg = (int16_t)(data[3]);
}

void parse_ARM_DRS_state(uint16_t data[4], ARM_DRS_Angle* drs_angle)
{

    drs_angle->bottom_flap = data[0]; // [deg]
    drs_angle->top_flap = data[1]; // [deg]
    drs_angle->state = data[2];
}

void parse_ARM_CA_TL(uint16_t data[4], ARM_Control_Action_Log* control_log)
{

    control_log->TL.FL = (int16_t)(data[0]);
    control_log->TL.FR = (int16_t)(data[1]);
    control_log->TL.RL = (int16_t)(data[2]);
    control_log->TL.RR = (int16_t)(data[3]);
}

void parse_ARM_CA_LC(uint16_t data[4], ARM_Control_Action_Log* control_log)
{

    control_log->LC.FL = (int16_t)(data[0]);
    control_log->LC.FR = (int16_t)(data[1]);
    control_log->LC.RL = (int16_t)(data[2]);
    control_log->LC.RR = (int16_t)(data[3]);
}

void parse_ARM_CA_max_braking_regen(uint16_t data[4], ARM_Control_Action_Log* control_log)
{

    control_log->max_braking_regen.FL = (int16_t)(data[0]);
    control_log->max_braking_regen.FR = (int16_t)(data[1]);
    control_log->max_braking_regen.RL = (int16_t)(data[2]);
    control_log->max_braking_regen.RR = (int16_t)(data[3]);
}

void parse_ARM_CA_distributor(uint16_t data[4], ARM_Control_Action_Log* control_log)
{

    control_log->distributor.FL = (int16_t)(data[0]);
    control_log->distributor.FR = (int16_t)(data[1]);
    control_log->distributor.RL = (int16_t)(data[2]);
    control_log->distributor.RR = (int16_t)(data[3]);
}

void parse_ARM_CA_power_basic_regen(uint16_t data[4], ARM_Control_Action_Log* control_log)
{

    control_log->max_power_regen.FL = (int16_t)(data[0]);
    control_log->max_power_regen.FR = (int16_t)(data[0]);
    control_log->max_power_regen.RL = (int16_t)(data[1]);
    control_log->max_power_regen.RR = (int16_t)(data[1]);
    control_log->basic_regen.FL = (int16_t)(data[2]);
    control_log->basic_regen.FR = (int16_t)(data[2]);
    control_log->basic_regen.RL = (int16_t)(data[3]);
    control_log->basic_regen.RR = (int16_t)(data[3]);
}

void parse_ARM_CA_power_limiter_yaw_moment(uint16_t data[4], ARM_Control_Action_Log* control_log)
{

    int16_t pwr_estimation = (int16_t)data[1];

    control_log->PL.scaling_factor = (float)(data[0] / 1000.0);

    control_log->PL.init_power_estimation = pwr_estimation & 0b011111111;

    control_log->PL.final_power_estimation = (pwr_estimation & 0b011111111) >> 8;

    control_log->yaw_moment_steering = (int16_t)(data[2]);

    control_log->yaw_moment_feedback = (int16_t)(data[3]);
}

void parse_ARM_CA_YMC_feedback(uint16_t data[4], ARM_Control_Action_Log* control_log)
{

    control_log->YMC_feedback.integral_state = (int16_t)(data[0]);
    control_log->YMC_feedback.desired_yaw_rate = (int16_t)(data[1]);
    control_log->YMC_feedback.error_yaw_rate = (int16_t)(data[2]);
}

void parse_ARM_TD_inverter_voltage(uint16_t data[4], ARM_Torque_Derating_Log* torque_derating_log)
{

    torque_derating_log->inverter_voltage_charge = data[0];
    torque_derating_log->inverter_voltage_discharge = data[1];
}

void parse_ARM_TD_temp_motor(uint16_t data[4], ARM_Torque_Derating_Log* torque_derating_log)
{

    torque_derating_log->temp_motor.FL = data[0];
    torque_derating_log->temp_motor.FR = data[1];
    torque_derating_log->temp_motor.RL = data[2];
    torque_derating_log->temp_motor.RR = data[3];
}

void parse_ARM_TD_temp_inverter(uint16_t data[4], ARM_Torque_Derating_Log* torque_derating_log)
{

    torque_derating_log->temp_inverter.FL = data[0];
    torque_derating_log->temp_inverter.FR = data[1];
    torque_derating_log->temp_inverter.RL = data[2];
    torque_derating_log->temp_inverter.RR = data[3];
}

void parse_ARM_TD_temp_IGBT(uint16_t data[4], ARM_Torque_Derating_Log* torque_derating_log)
{

    torque_derating_log->temp_IGBT.FL = data[0];
    torque_derating_log->temp_IGBT.FR = data[1];
    torque_derating_log->temp_IGBT.RL = data[2];
    torque_derating_log->temp_IGBT.RR = data[3];
}

void parse_ARM_TD_motor_power(uint16_t data[4], ARM_Torque_Derating_Log* torque_derating_log)
{

    torque_derating_log->motor_power.FL = data[0];
    torque_derating_log->motor_power.FR = data[1];
    torque_derating_log->motor_power.RL = data[2];
    torque_derating_log->motor_power.RR = data[3];
}

void parse_ARM_TD_i2t(uint16_t data[4], ARM_Torque_Derating_Log* torque_derating_log)
{

    torque_derating_log->i2t.FL = data[0];
    torque_derating_log->i2t.FR = data[1];
    torque_derating_log->i2t.RL = data[2];
    torque_derating_log->i2t.RR = data[3];
}

void parse_ARM_TD_net_derating(uint16_t data[4], ARM_Torque_Derating_Log* torque_derating_log)
{

    torque_derating_log->net_derating.FL = data[0];
    torque_derating_log->net_derating.FR = data[1];
    torque_derating_log->net_derating.RL = data[2];
    torque_derating_log->net_derating.RR = data[3];
}

void parse_ARM_power_derating(uint16_t data[4], ARM_Power_Derating_Log* power_derating_log)
{

    power_derating_log->cell_temp_discharge = data[0];
    power_derating_log->cell_temp_charge = data[1];
    power_derating_log->cell_min_voltage = data[2];
    power_derating_log->cell_max_voltage = data[3];
}

void parse_ARM_net_power_derating(uint16_t data[4], ARM_Power_Derating_Log* power_derating_log)
{

    power_derating_log->net_power = data[0];
    power_derating_log->net_regen_power = data[1];
}

void parse_ARM_status_message(uint16_t data[4], ARM_status* status)
{
    status->ARM_control_ok = data[0];
}

void parse_can_ARM(CANdata message, ARM_CAN_data* data)
{

    switch (message.msg_id) {
    case (MSG_ID_ARM_TORQUE):
        parse_ARM_torque_setpoint_message(message.data, &data->torque_setpoint);
        return;
    case (MSG_ID_ARM_SPEED):
        parse_ARM_speed_setpoint_message(message.data, &data->speed_setpoint);
        return;
    case (MSG_ID_ARM_DRS):
        parse_ARM_DRS_state(message.data, &(data->drs_angle));
        return;
    case (MSG_ID_ARM_STATUS):
        parse_ARM_status_message(message.data, &(data->status));
        return;
    case (MSG_ID_ARM_CONTROL_ACTION_TL):
        parse_ARM_CA_TL(message.data, &(data->control_log));
        return;
    case (MSG_ID_ARM_CONTROL_ACTION_LC):
        parse_ARM_CA_LC(message.data, &(data->control_log));
        return;
    case (MSG_ID_ARM_CONTROL_ACTION_BRAKING_REGEN):
        parse_ARM_CA_max_braking_regen(message.data, &(data->control_log));
        return;
    case (MSG_ID_ARM_CONTROL_ACTION_POWER_BASIC_REGEN):
        parse_ARM_CA_power_basic_regen(message.data, &(data->control_log));
        return;
    case (MSG_ID_ARM_CONTROL_ACTION_DISTRIBUTOR):
        parse_ARM_CA_distributor(message.data, &(data->control_log));
        return;
    case (MSG_ID_ARM_CONTROL_ACTION_POWER_LIMITER_YAW_MOMENT):
        parse_ARM_CA_power_limiter_yaw_moment(message.data, &(data->control_log));
        return;
    case (MSG_ID_ARM_CONTROL_ACTION_YMC_STRAIGHT_CORNER_FLAG):
        parse_ARM_CA_YMC_feedback(message.data, &(data->control_log));
        return;
    case (MSG_ID_ARM_SAFETY_INVERTER_VOLT_TORQUE_DERATING):
        parse_ARM_TD_inverter_voltage(message.data, &(data->torque_derating_log));
        return;
    case (MSG_ID_ARM_SAFETY_MOTOR_TEMP_TORQUE_DERATING):
        parse_ARM_TD_temp_motor(message.data, &(data->torque_derating_log));
        return;
    case (MSG_ID_ARM_SAFETY_INVERTER_TEMP_TORQUE_DERATING):
        parse_ARM_TD_temp_inverter(message.data, &(data->torque_derating_log));
        return;
    case (MSG_ID_ARM_SAFETY_IGBT_TEMP_TORQUE_DERATING):
        parse_ARM_TD_temp_IGBT(message.data, &(data->torque_derating_log));
        return;
    case (MSG_ID_ARM_SAFETY_MOTOR_POWER_TORQUE_DERATING):
        parse_ARM_TD_motor_power(message.data, &(data->torque_derating_log));
        return;
    case (MSG_ID_ARM_SAFETY_I2T_TORQUE_DERATING):
        parse_ARM_TD_i2t(message.data, &(data->torque_derating_log));
        return;
    case (MSG_ID_ARM_SAFETY_NET_TORQUE_DERATING):
        parse_ARM_TD_net_derating(message.data, &(data->torque_derating_log));
        return;
    case (MSG_ID_ARM_SAFETY_POWER_DERATING):
        parse_ARM_power_derating(message.data, &(data->power_derating_log));
        return;
    case (MSG_ID_ARM_SAFETY_NET_POWER_DERATING):
        parse_ARM_net_power_derating(message.data, &(data->power_derating_log));
        return;

    default:
        return;
    }
}
