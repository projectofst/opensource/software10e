#include "GPS_SUPOTS_CAN.h"

void parse_gps_supots_message_date(uint16_t data[4], GPS_SUPOTS_MSG_Date* date)
{

    date->day = data[0];
    date->month = data[1];
    date->year = data[2];

    return;
}

void parse_gps_supots_message_utc_time(uint16_t data[4], GPS_SUPOTS_MSG_Time* time)
{

    time->hours = data[0];
    time->minutes = data[1];
    time->seconds = data[2];
    time->miliseconds = data[3];

    return;
}

void parse_gps_supots_message_latitude(uint16_t data[4], GPS_SUPOTS_MSG_Coordenates* latitude)
{

    latitude->dg = (int16_t)data[0];
    latitude->min = data[1];
    latitude->dec_min = data[2];

    return;
}

void parse_gps_supots_message_longitude(uint16_t data[4], GPS_SUPOTS_MSG_Coordenates* longitude)
{

    longitude->dg = (int16_t)data[0];
    longitude->min = data[1];
    longitude->dec_min = data[2];

    return;
}

void parse_gps_supots_message_ground_speed(uint16_t data[4], uint16_t* ground_speed)
{

    *ground_speed = data[0];

    return;
}

void parse_gps_supots_message_pot_adc_value(uint16_t data[4], GPS_SUPOTS_MSG_Pot_adc_value* pot_adc_value)
{

    pot_adc_value->pot1 = data[0];
    pot_adc_value->pot2 = data[1];

    return;
}

void parse_gps_supots_message_velocity(uint16_t data[4], uint16_t* velocity)
{

    *velocity = data[0];

    return;
}

void parse_gps_supots_message_normal_load(uint16_t data[4], GPS_SUPOTS_MSG_Normal_load* normal_load)
{

    normal_load->FL = data[0];
    normal_load->FR = data[1];
    normal_load->RL = data[2];
    normal_load->RR = data[3];

    return;
}

void parse_can_gps_supots(CANdata message, GPS_SUPOTS_PARSED_Data* data)
{
    if ((message.dev_id != DEVICE_ID_GPS_SUPOTS_FRONT) && (message.dev_id != DEVICE_ID_GPS_SUPOTS_REAR)) {
        /*FIXME: send info for error logging*/
        return;
    }

    switch (message.msg_id) {

    case MSG_ID_GPS_SUPOTS_DATE:
        parse_gps_supots_message_date(message.data, &(data->date));
        break;
    case MSG_ID_GPS_SUPOTS_UTC_TIME:
        parse_gps_supots_message_utc_time(message.data, &(data->time));
        break;
    case MSG_ID_GPS_SUPOTS_LATITUDE:
        parse_gps_supots_message_latitude(message.data, &(data->latitude));
        break;
    case MSG_ID_GPS_SUPOTS_LONGITUDE:
        parse_gps_supots_message_longitude(message.data, &(data->longitude));
        break;
    case MSG_ID_GPS_SUPOTS_GROUND_SPEED:
        parse_gps_supots_message_ground_speed(message.data, &(data->ground_speed));
        break;
    case MSG_ID_GPS_SUPOTS_POT_ADC_VALUE:
        parse_gps_supots_message_pot_adc_value(message.data, &(data->pot_adc_value));
        break;
    case MSG_ID_GPS_SUPOTS_VELOCITY:
        parse_gps_supots_message_velocity(message.data, &(data->velocity));
        break;
    case MSG_ID_GPS_SUPOTS_NORMAL_LOAD:
        parse_gps_supots_message_normal_load(message.data, &(data->normal_load));
    }
}
