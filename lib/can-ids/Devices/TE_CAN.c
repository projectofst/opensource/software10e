#include "TE_CAN.h"
#include "../CAN_IDs.h"
#include <stdbool.h>
#include <stdint.h>

void parse_te_main_message(uint16_t data[4], TE_MESSAGE* main_message)
{
    main_message->status.TE_status = data[0];
    main_message->APPS = data[1];
    main_message->BPS_pressure = data[2];
    main_message->BPS_electric = data[3];

    return;
}

void parse_te_verbose_1_message(uint16_t data[4], TE_VERBOSE_MESSAGE* verbose_message)
{

    verbose_message->apps_0 = data[0];
    verbose_message->apps_1 = data[1];
    verbose_message->pressure_0 = data[2];
    verbose_message->pressure_1 = data[3];

    return;
}

void parse_te_verbose_2_message(uint16_t data[4], TE_VERBOSE_MESSAGE* verbose_message)
{

    verbose_message->electric = data[0];
    verbose_message->hard_braking = data[1];
    verbose_message->imp_apps = data[2];
    verbose_message->imp_apps_bps = data[3];

    return;
}

void parse_can_te_limit(CANdata message, TE_LIMIT* limit_message)
{
    limit_message->header = message.data[0];
    limit_message->data_limit = message.data[1];
    return;
}

void parse_brake_pressure(CANdata message, TE_PRESSURE_BAR* pressure_bar)
{
    pressure_bar->pressure_F_bar = message.data[0];
    pressure_bar->pressure_R_bar = message.data[1];
    return;
}

void parse_can_te(CANdata message, TE_CAN_Data* data)
{
    if (message.dev_id != DEVICE_ID_TE) {
        return;
    }

    switch (message.msg_id) {
    case MSG_ID_TE_MAIN:
        parse_te_main_message(message.data, &(data->main_message));
        break;
    case MSG_ID_TE_VERBOSE_1:
        parse_te_verbose_1_message(message.data, &data->verbose_message);
        break;
    case MSG_ID_TE_VERBOSE_2:
        parse_te_verbose_2_message(message.data, &data->verbose_message);
        break;
    case MSG_ID_TE_LIMIT_ANSWER:
        parse_can_te_limit(message, &(data->limit_message));
        break;
    case MSG_ID_TE_BAR_PRESSURE:
        parse_brake_pressure(message, &(data->pressure_bar));
        break;
    }
    return;
}

uint16_t TE_OK(uint16_t Word)
{

    return !(Word & TE_OK_MASK);
}
