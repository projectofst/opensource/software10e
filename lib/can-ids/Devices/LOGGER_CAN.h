#include <stdbool.h>
#include <stdint.h>
#include "../CAN_IDs.h"


typedef enum {
	NO_CARD = 0,		/* 0: Successful */
	WAITING_FOR_START,		/* 1: R/W Error */
	WRITING,		/* 2: Write Protected */
} LOG_STATE;

typedef struct{
    LOG_STATE state;
}LOGGER_MSG_STATUS;

typedef struct {
	LOGGER_MSG_STATUS status;
} LOGGER_CAN_Data;

void parse_logger_message_status(uint16_t data[4], LOGGER_MSG_STATUS *status);
void parse_can_logger(CANdata message, LOGGER_CAN_Data *data);
