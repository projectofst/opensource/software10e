#ifndef __WATER_TEMP_CAN_H__
#define __WATER_TEMP_CAN_H__

#include <stdint.h>

#include "../CAN_IDs.h"


#define MSG_ID_WATER_TEMP_TEMPERATURE1			57
#define MSG_ID_WATER_TEMP_TEMPERATURE2			56
#define MSG_ID_WATER_TEMP_FLOW_RATE				54

typedef struct {
	unsigned int timestamp;
	unsigned int temp1;
	unsigned int temp2;
	unsigned int temp3;
} WT_Temperature;

typedef struct {
	unsigned int flow_rate_1;
	unsigned int flow_rate_2;
} WT_Flow_Rate;

typedef struct {
	WT_Temperature temperature1;
	WT_Temperature temperature2;
	WT_Flow_Rate flow_rate;
} WT_Data;

void parse_can_message_wt(CANdata msg, WT_Data *data);
void parse_can_message_wt_temperature(CANdata msg, WT_Temperature *temperature);
void parse_can_message_wt_flow_rate(CANdata msg, WT_Flow_Rate *flow_rate);

#endif
