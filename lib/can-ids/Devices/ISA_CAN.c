#include "ISA_CAN.h"
#include "../CAN_IDs.h"
#include <stdbool.h>
#include <stdint.h>

/// Update isabelle current
void parse_ISA_current_message(uint16_t data[4], uint32_t* isa_current)
{

    uint32_t upper_word = 0, lower_word = 0;

    upper_word = data[2];
    upper_word = upper_word << 16;
    lower_word = data[1];

    *isa_current = upper_word | lower_word; /** [mA]    */
    return;
}

/// Update isabelle voltage 1
void parse_ISA_voltage1_message(uint16_t data[4], uint32_t* isa_voltage1)
{

    uint32_t upper_word = 0, lower_word = 0;

    upper_word = data[2];
    upper_word = upper_word << 16;
    lower_word = data[1];

    *isa_voltage1 = upper_word | lower_word; /** [mV]    */
    return;
}

/// Update isabelle voltage
void parse_ISA_voltage2_message(uint16_t data[4], uint32_t* isa_voltage2)
{

    uint32_t upper_word = 0, lower_word = 0;

    upper_word = data[2];
    upper_word = upper_word << 16;
    lower_word = data[1];

    *isa_voltage2 = upper_word | lower_word; /** [mV]    */
    return;
}

/// Update isabelle voltage 3
void parse_ISA_voltage3_message(uint16_t data[4], uint32_t* isa_voltage3)
{

    uint32_t upper_word = 0, lower_word = 0;

    upper_word = data[2];
    upper_word = upper_word << 16;
    lower_word = data[1];

    *isa_voltage3 = upper_word | lower_word; /** [mV]    */
    return;
}

/// Update isabelle temperature
void parse_ISA_temperature_message(uint16_t data[4], uint32_t* isa_temperature)
{

    uint32_t upper_word = 0, lower_word = 0;

    upper_word = data[2];
    upper_word = upper_word << 16;
    lower_word = data[1];

    *isa_temperature = upper_word | lower_word; /** [0.1ºC] */
    return;
}

/// Update isabelle power
void parse_ISA_power_message(uint16_t data[4], uint32_t* isa_power)
{

    uint32_t upper_word = 0, lower_word = 0;

    upper_word = data[2];
    upper_word = upper_word << 16;
    lower_word = data[1];

    *isa_power = upper_word | lower_word; /** [W]     */
    return;
}

/// Update isabelle current count
void parse_ISA_current_counter_message(uint16_t data[4], uint32_t* isa_current_counter)
{

    uint32_t upper_word = 0, lower_word = 0;

    upper_word = data[2];
    upper_word = upper_word << 16;
    lower_word = data[1];

    *isa_current_counter = upper_word | lower_word; /** [As]    */
    return;
}

/// Update isabelle energy
void parse_ISA_energy_message(uint16_t data[4], uint32_t* isa_energy)
{

    uint32_t upper_word = 0, lower_word = 0;

    upper_word = data[2];
    upper_word = upper_word << 16;
    lower_word = data[1];

    *isa_energy = upper_word | lower_word; /** [Wh]    */
    return;
}

void parse_can_ISA(CANdata message, ISA_CAN_data* data)
{

    switch (message.msg_id) {

    case MSG_ID_ISA_I:
        parse_ISA_current_message(message.data, &(data->isa_current));
        return;
    case MSG_ID_ISA_U1:
        parse_ISA_voltage1_message(message.data, &(data->isa_voltage1));
        return;
    case MSG_ID_ISA_U2:
        parse_ISA_voltage2_message(message.data, &(data->isa_voltage2));
        return;
    case MSG_ID_ISA_U3:
        parse_ISA_voltage3_message(message.data, &(data->isa_voltage3));
        return;
    case MSG_ID_ISA_T:
        parse_ISA_temperature_message(message.data, &(data->isa_temperature));
        return;
    case MSG_ID_ISA_W:
        parse_ISA_power_message(message.data, &(data->isa_power));
        return;
    case MSG_ID_ISA_As:
        parse_ISA_current_counter_message(message.data, &(data->isa_current_counter));
        return;
    case MSG_ID_ISA_Wh:
        parse_ISA_energy_message(message.data, &(data->isa_energy));
        return;

    default:
        return;
    }

    return;
}
