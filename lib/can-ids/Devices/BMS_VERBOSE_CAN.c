#ifndef __BMS_VERBOSE
#define __BMS_VERBOSE

#include "BMS_VERBOSE_CAN.h"
#include "../CAN_IDs.h"

/**
 * Per message parse functions are hard to apply here due to array of equal
 * things
 */
#if 0
void parse_can_message_master_cell_info(CANdata msg, MASTER_MSG_Cell_Info *info) {
	uint16_t id = msg.data[0];
	if (id > 143) {
		return;
	}
	info[id].cell_id = id;
	info[id].voltage = msg.data[1];
	info[id].temperature = msg.data[2];
	info[id].SoC = msg.data[3];

	return;
}

void parse_can_message_master_slave_info_1(CANdata msg, MASTER_MSG_Slave_Info_1 *slave) {
	uint16_t id = msg.data[0];
	if (id > 11) {
		return;
	}
	info.slave_id = id;
	info.discharge_temperature_connection_OK_mask = msg.data[1];

	return;
}
#endif

void parse_can_message_master_verbose(CANdata msg, MASTER_VERBOSE_MSG_Data* master)
{
    uint16_t id;
    switch (msg.msg_id) {

    case MSG_ID_MASTER_SLAVE_INFO_1:
        id = msg.data[0];
        if (id >= 12) {
            return;
        }
        master->slave.slave_info_1.slave_id = msg.data[0];
        master->slave.slave_info_1.discharge_channel_fault_mask = msg.data[1];
        master->slave.slave_info_1.cell_connection_fault_mask = msg.data[2];
        master->slave.slave_info_1.cell_temp_fault_mask = msg.data[3] & 0xFFF;
        master->slave.slave_info_1.slave_temp1_fault = (msg.data[3] > 11) & 1;
        master->slave.slave_info_1.slave_temp2_fault = (msg.data[3] > 12) & 1;
        master->slave.slave_info_1.heatsink_temp1_fault = (msg.data[3] > 14) & 1;
        master->slave.slave_info_1.heatsink_temp2_fault = (msg.data[3] > 15) & 1;

        break;

    case MSG_ID_MASTER_SLAVE_INFO_2:
        id = msg.data[0];
        if (id >= 12) {
            return;
        }
        master->slave.slave_info_2.slave_id = msg.data[0];
        master->slave.slave_info_2.discharge_channel_state = msg.data[1];
        master->slave.slave_info_2.heatsink_temperature1 = msg.data[2];
        master->slave.slave_info_2.heatsink_temperature2 = msg.data[3];
        break;

    case MSG_ID_MASTER_SLAVE_INFO_3:
        id = msg.data[0];
        if (id >= 12) {
            return;
        }
        master->slave.slave_info_3.slave_id = msg.data[0];
        master->slave.slave_info_3.slave_temperature1 = msg.data[1];
        master->slave.slave_info_3.slave_temperature2 = msg.data[2];
        master->slave.slave_info_3.BM_temperature = msg.data[3];
        break;

    case MSG_ID_MASTER_CELL_INFO:
        id = msg.data[0] & 0xFF;
        if (id >= 144) {
            return;
        }
        master->cell.cell_id = (uint8_t)msg.data[0];
        master->cell.voltage_state = (msg.data[0] >> 8) & 0b1111;
        master->cell.temperature_state = (msg.data[0] >> 12) & 0b1111;
        master->cell.voltage = msg.data[1];
        master->cell.temperature = msg.data[2];
        master->cell.SoC = msg.data[3];
        break;
    }
}

#endif
