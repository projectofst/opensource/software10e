#ifndef __BMS_VERBOSE_CAN_H__
#define __BMS_VERBOSE_CAN_H__

#include "../CAN_IDs.h"
#include "../Devices/BMS_MASTER_CAN.h"

#define MSG_ID_MASTER_CELL_INFO  63
#define MSG_ID_MASTER_SLAVE_INFO_1 62
#define MSG_ID_MASTER_SLAVE_INFO_2 61
#define MSG_ID_MASTER_SLAVE_INFO_3 60
#define MSG_ID_MASTER_SLAVE_INFO_4 59

/**
 * @brief Message contents for ID MSG_ID_MASTER_CELL_INFO
 *
 * Contains information about a single battery cell
 */
typedef struct {
	uint8_t cell_id;
    uint8_t voltage_state : 4;
    uint8_t temperature_state : 4;
	uint16_t voltage;
	uint16_t temperature;
	uint16_t SoC;
} MASTER_MSG_Cell_Info;

/**
 * @brief Message information for ID MSG_MASTER_SLAVE_INFO_1
 *
 * Contains the first part of the information about a BMS slave (temperature and
 * voltage connections state)
 */
typedef struct {
	uint16_t slave_id;
    uint16_t discharge_channel_fault_mask;
    uint16_t cell_connection_fault_mask;
    uint16_t cell_temp_fault_mask;
	bool slave_temp1_fault;
	bool slave_temp2_fault;
	bool heatsink_temp1_fault;
	bool heatsink_temp2_fault;
} MASTER_MSG_Slave_Info_1;

/**
 * @brief Message information for ID MSG_MASTER_SLAVE_INFO_2
 *
 * Contains the second part of the information about a BMS slave (discharge
 * channels information)
 */
typedef struct {
	uint16_t slave_id;
    uint16_t discharge_channel_state;
	uint16_t heatsink_temperature1;
    uint16_t heatsink_temperature2;
} MASTER_MSG_Slave_Info_2;

/**
 * @brief Message information for ID MSG_MASTER_SLAVE_INFO_3
 *
 * Contains the second part of the information about a BMS slave (discharge
 * channels information)
 */
typedef struct {
	uint16_t slave_id;
	uint16_t slave_temperature1;
    uint16_t slave_temperature2;
	uint16_t BM_temperature;
} MASTER_MSG_Slave_Info_3;

/**
 * @brief Structure containing all information taken from master's verbose
 * messages
 *
 * Information about 12 stacks (slave + 12 cells)
 */
typedef struct {
    struct{
        MASTER_MSG_Slave_Info_1 slave_info_1;
        MASTER_MSG_Slave_Info_2 slave_info_2;
        MASTER_MSG_Slave_Info_3 slave_info_3;
    } slave;
    MASTER_MSG_Cell_Info cell;

} MASTER_VERBOSE_MSG_Data;

void parse_can_message_master_verbose(CANdata, MASTER_VERBOSE_MSG_Data *);

#endif /* end include guard */
