#include "DCU_CAN.h"
#include "../CAN_IDs.h"
#include <stdbool.h>
#include <stdint.h>

void parse_dcu_message_status(uint16_t data[4], DCU_MSG_Status* status)
{
    status->Detection_SC_Front_BSPD = (data[0] >> 0) & 1;
    status->Detection_SC_MH_Front = (data[0] >> 1) & 1;
    status->Detection_SC_BSPD_AMS = (data[0] >> 2) & 1;
    status->Detection_SC_Origin_MH = (data[0] >> 3) & 1;
    status->Detection_VCC_AMS = (data[0] >> 4) & 1;
    status->Detection_VCC_FANS_AMS = (data[0] >> 5) & 1;
    status->Detection_VCC_TSAL = (data[0] >> 6) & 1;
    status->Detection_VCC_CUA = (data[0] >> 7) & 1;
    status->Detection_VCC_EM = (data[0] >> 8) & 1;
    status->Detection_VCC_PUMPS = (data[0] >> 9) & 1;
    status->Detection_VCC_FANS = (data[0] >> 10) & 1;
    status->Detection_VCC_CUB = (data[0] >> 11) & 1;
    status->Detection_VCC_DRS = (data[0] >> 12) & 1;
    status->Detection_VCC_CAN_E = (data[0] >> 13) & 1;
    status->Detecion_VCC_BL = (data[0] >> 14) & 1;
    status->Detection_VCC_DATA = (data[0] >> 15) & 1;

    status->DrsB_sig = (data[1] >> 0) & 1;
    status->DrsT_sig = (data[1] >> 1) & 1;
    status->Blue_sig = (data[1] >> 2) & 1;
    status->Dcdc_switch = (data[1] >> 3) & 1;
    status->BL_sig = (data[1] >> 4) & 1;
    status->Buzz_sig = (data[1] >> 5) & 1;
}

void parse_dcu_message_current_voltage(uint16_t data[4], DCU_MSG_Current* current)
{
    current->LV_current = data[0];
    current->HV_current = data[1];
    current->LV_voltage = data[2];
}

void parse_can_dcu(CANdata message, DCU_CAN_Data* data)
{
    if (message.dev_id != DEVICE_ID_DCU) {
        /*FIXME: send info for error logging*/

        return;
    }

    switch (message.msg_id) {
    case MSG_ID_DCU_STATUS:
        parse_dcu_message_status(message.data, &(data->status));
        break;
    case MSG_ID_DCU_CURRENT_VOLTAGE:
        parse_dcu_message_current_voltage(message.data, &(data->current));
        break;
    }
}
