#ifndef __ISA_CAN_H__
#define __ISA_CAN_H__

/*Can interface for IVT Energy meter*/

#include <stdint.h>
#include "../CAN_IDs.h"

/// ISA messages
#define MSG_ID_ISA_I        32
#define MSG_ID_ISA_U1       33
#define MSG_ID_ISA_U2       34
#define MSG_ID_ISA_U3       35
#define MSG_ID_ISA_T        36
#define MSG_ID_ISA_W        37
#define MSG_ID_ISA_As       38
#define MSG_ID_ISA_Wh       39


typedef struct{

    uint32_t    isa_current;            /** [mA]    */
    uint32_t    isa_voltage1;           /** [mV]    */
    uint32_t    isa_voltage2;           /** [mV]    */
    uint32_t    isa_voltage3;           /** [mV]    */
    uint32_t    isa_temperature;        /** [0.1ºC] */
    uint32_t    isa_power;              /** [W]     */
    uint32_t    isa_current_counter;    /** [As]    */
    uint32_t    isa_energy;             /** [Wh]    */

}ISA_CAN_data;

/// Parse functions
void parse_ISA_current_message(uint16_t data[4], uint32_t *isa_current);
void parse_ISA_voltage1_message(uint16_t data[4], uint32_t *isa_voltage1);
void parse_ISA_voltage2_message(uint16_t data[4], uint32_t *isa_voltage2);
void parse_ISA_voltage3_message(uint16_t data[4], uint32_t *isa_voltage3);
void parse_ISA_temperature_message(uint16_t data[4], uint32_t *isa_temperature);
void parse_ISA_power_message(uint16_t data[4], uint32_t *isa_power);
void parse_ISA_current_counter_message(uint16_t data[4], uint32_t *isa_current_counter);
void parse_ISA_energy_message(uint16_t data[4], uint32_t *isa_energy);

/// General parse function
void parse_can_ISA(CANdata message, ISA_CAN_data *data);

#endif