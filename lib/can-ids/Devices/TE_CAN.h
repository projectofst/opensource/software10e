#ifndef __TE_CAN_H__
#define __TE_CAN_H__

/* CAN Interface for Torque Encoder */

#include <stdbool.h>
#include <stdint.h>
#include "../CAN_IDs.h"

/* Message IDs */
#define MSG_ID_TE_VERBOSE_1 33
#define MSG_ID_TE_VERBOSE_2 34
#define MSG_ID_TE_LIMIT_ANSWER 35
#define MSG_ID_TE_BAR_PRESSURE 36
#define MSG_ID_TE_FORCES       37

/* Parameters */
#define P_TE_VERSION_ID_0						0
#define P_TE_VERSION_ID_1						1
#define P_TE_VERSION_ID_2						2
#define P_TE_VERSION_ID_3						3

#define P_TE_HARD_BRAKING_PRESS_THRS 			4
#define P_TE_UPDATE_PEDAL_THRS	   				5
#define P_TE_LIMIT_RESQUEST 					6

#define TE_VERSION_ID					0
#define TE_HARD_BRAKING_PRESS_THRS 		1	
#define TE_UPDATE_PEDAL_THRS	   		2
#define TE_LIMIT_RESQUEST 				3

#define TE_OK_MASK 0x31B

/*Sub Structs*/

/*! \var typedef struct TE_CORE
    \brief Struct with every status bit related to the sensors, implausibility event and shutdown circuit. 
*/
typedef struct {
	union {
		struct {
			bool CC_APPS_0: 1;
			bool CC_APPS_1: 1;
			bool CC_BPS_electric_0: 1;
			bool CC_BPS_pressure_0: 1;
			bool CC_BPS_pressure_1: 1;
			bool Overshoot_APPS_0: 1;
			bool Overshoot_APPS_1: 1;
			bool Overshoot_BPS_electric: 1;
			bool Implausibility_APPS_Timer_Exceeded: 1;
			bool Implausibility_APPS_BPS_Timer_Exceeded: 1;
			bool Hard_braking: 1;
			bool SC_Motor_Interlock_Front: 1;
			bool Verbose_Mode: 1;
			bool Implausibility_APPS: 1;
			bool Implausibility_APPS_BPS: 1;
			bool Overshoot_BPS_pressure_0: 1;
			bool Overshoot_BPS_pressure_1: 1;
			bool Imp_APPS_25: 1;
			bool Imp_APPS_5:  1;
		};
		uint16_t TE_status;
	};
} TE_CORE; 

typedef enum{ 
    BRAKE_ZERO,
    BRAKE_MAX,
    ACCELERATOR_ZERO,
    ACCELERATOR_MAX,
}INTERFACE_MSG_PEDAL_THRESHOLDS;


/*Message Structs*/
/*MSG_ID_TE_MAIN*/
typedef struct{

	TE_CORE status;
	uint16_t APPS;
	uint16_t BPS_pressure;
	uint16_t BPS_electric;

}TE_MESSAGE;

typedef struct{

	uint16_t apps_0;
	uint16_t apps_1;
	uint16_t electric;
	uint16_t pressure_0;
	uint16_t pressure_1;
	uint16_t hard_braking;
	bool imp_apps;
	bool imp_apps_bps;
}TE_VERBOSE_MESSAGE;

/* MSG_ID_TE_LIMIT */
typedef struct {
	uint16_t header;
	uint16_t data_limit;
}TE_LIMIT;

typedef struct{
	uint16_t pressure_F_bar;
	uint16_t pressure_R_bar;
}TE_PRESSURE_BAR;

/*MAIN STRUCT*/
typedef struct{
	TE_MESSAGE main_message;
	TE_VERBOSE_MESSAGE verbose_message;
	TE_LIMIT limit_message;
	TE_PRESSURE_BAR pressure_bar;
}TE_CAN_Data;


typedef enum {
    GND,
    VCC,
    ZERO_FORCE,
    MAX_FORCE,
}TE_THRESHOLD_LIMIT;


typedef enum{
    APPS_0,
    APPS_1,
    BPS_PRESSURE_0,
    BPS_PRESSURE_1,
    BPS_ELECTRIC,
}SENSOR_SELECT;


void parse_te_main_message (uint16_t data[4], TE_MESSAGE *main_message);
void parse_te_verbose_1_message(uint16_t data[4], TE_VERBOSE_MESSAGE *verbose_message);
void parse_te_verbose_2_message(uint16_t data[4], TE_VERBOSE_MESSAGE *verbose_message);
void parse_can_te_limit(CANdata message, TE_LIMIT *data_limit);
void parse_can_te(CANdata message, TE_CAN_Data *data);
void parse_brake_pressure(CANdata message, TE_PRESSURE_BAR *pressure_bar);
uint16_t TE_OK(uint16_t Word);




#endif
