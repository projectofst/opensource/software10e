#ifndef __DASH_CAN_H__
#define __DASH_CAN_H__

#include "../CAN_IDs.h"
#include <stdint.h>
#include <stdbool.h>

#define MSG_ID_DASH_STATUS	32
//Message ID for steering encoder value
#define MSG_ID_DASH_SE      34

/* Parameters */
#define P_DASH_VERSION_ID_0       0
#define P_DASH_VERSION_ID_1		    1
#define P_DASH_VERSION_ID_2		    2
#define P_DASH_VERSION_ID_3			  3
#define P_DASH_SE_OFFSET          4
#define P_DASH_SE_UPDATE_FREQ     5
#define P_DASH_BUT_UPDATE_FREQ    6
#define P_DASH_LCD_UPDATE_FREQ    7

typedef struct {
  bool SC:1;
	bool debug_mode;
} DASH_MSG_status;

typedef struct {
  int16_t value;
} DASH_MSG_SE;

// MAIN STRUCT
typedef struct {
	DASH_MSG_status status;
	DASH_MSG_SE SE;
} DASH_CAN_Data;

void parse_can_message_dash_status(uint16_t data[4], DASH_MSG_status *status);
void parse_can_message_dash_SE(uint16_t data[4], DASH_MSG_SE *SE);
void parse_can_dash(CANdata message, DASH_CAN_Data *data);

#endif
