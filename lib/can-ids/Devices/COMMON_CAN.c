#include "COMMON_CAN.h"
#include "../CAN_IDs.h"
#include <stdbool.h>
#include <stdint.h>

void parse_can_common_RTD(CANdata message, COMMON_MSG_RTD_ON* RTD)
{

    RTD->step = (RTD_ON)((message.data[0]) & 0b1111);

    return;
}

void parse_can_common_trap(CANdata message, COMMON_MSG_TRAP* trap)
{

    trap->type = message.data[0];

    return;
}

void parse_can_common_reset(CANdata message, COMMON_MSG_RESET* RESET)
{

    RESET->RCON = message.data[0];

    return;
}

void parse_can_common_parse_error(CANdata message, COMMON_MSG_PARSE_ERROR* PARSE)
{

    PARSE->wrong_dev_id = message.data[0] & 0b11111;
    PARSE->right_dev_id = (message.data[0] >> 5) & 0b11111;

    return;
}

void parse_can_common_TS(CANdata message, COMMON_MSG_TS* TS)
{

    TS->step = (TS_STEP)message.data[0];
    return;
}

void parse_can_common_RTD_OFF(CANdata message, COMMON_MSG_RTD_OFF* rtd_off_msg)
{
    rtd_off_msg->step = (RTD_OFF)message.data[0];

    return;
}

CANdata make_reset_msg(uint8_t dev_id, uint8_t RCON)
{

    CANdata message;

    message.dev_id = dev_id;
    message.msg_id = MSG_ID_COMMON_RESET;

    message.dlc = 2;

    message.data[0] = RCON;

    return message;
}

CANdata make_parse_error_msg(uint8_t dev_id, uint8_t wrong_id, uint8_t right_id)
{

    CANdata message;

    message.dev_id = dev_id;
    message.msg_id = MSG_ID_COMMON_PARSE_ERROR;

    message.dlc = 2;

    message.data[0] = wrong_id | (right_id << 5);

    return message;
}

CANdata make_rtd_on_msg(uint8_t dev_id, RTD_ON step)
{

    CANdata message;

    message.dev_id = dev_id;
    message.msg_id = CMD_ID_COMMON_RTD_ON;

    message.dlc = 2;

    message.data[0] = step;

    return message;
}

// reason only applicable to IIB
CANdata make_rtd_off_msg(uint8_t dev_id, RTD_OFF step)
{

    CANdata message;

    message.dev_id = dev_id;
    message.msg_id = CMD_ID_COMMON_RTD_OFF;

    message.dlc = 2;

    message.data[0] = step;

    return message;
}

CANdata make_ts_step_msg(uint8_t dev_id, TS_STEP step)
{

    CANdata message;

    message.dev_id = dev_id;
    message.msg_id = CMD_ID_COMMON_TS;

    message.dlc = 2;

    message.data[0] = step;

    return message;
}
