#ifndef __SNIFFER_CAN_H__
#define __SNIFFER_CAN_H__

#include "../CAN_IDs.h"

#define MSG_ID_SNIFFER_STATS 63

typedef struct {
	uint16_t msgs_read;
} SNIFFER_MSG_Data;

void parse_sniffer_message_stats(CANdata msg, SNIFFER_MSG_Data *data);
void parse_can_sniffer(CANdata msg, SNIFFER_MSG_Data *data);
#endif
