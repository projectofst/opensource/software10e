#ifndef __DCU_CAN_H__
#define __DCU_CAN_H__

/* CAN Interface for Digital Control Unit */

#include <stdbool.h>
#include <stdint.h>
#include "../CAN_IDs.h"

/* Message IDs */
#define MSG_ID_DCU_STATUS					32
#define MSG_ID_DCU_CURRENT_VOLTAGE 			33
#define MSG_ID_EEPROM_DATA					34

/* Parameters */
#define P_DCU_VERSION_ID_0		0
#define P_DCU_VERSION_ID_1		1
#define P_DCU_VERSION_ID_2		2
#define P_DCU_VERSION_ID_3		3

//Parameters ids
#define P_DRS_MIN_ANGLE_CALI    0
#define P_DRS_MAX_ANGLE_CALI    1
#define P_DRS_MIN_ANGLE_FINAL   2
#define P_DRS_MAX_ANGLE_FINAL   3

#define DRS_ANGLE 				0
#define DRS_DUTY_CYCLE 			1


/* Sub Structs */


/* Message Structs */
// MSG_ID_DCU_STATUS
// Sends Fuse Detection, 3 point Shutdown Circuit detection, RTR, BL and RTDS state


typedef struct {
	// data[0]
	union {
		struct {
			bool Detection_SC_Front_BSPD: 1; //lsb
            bool Detection_SC_MH_Front 	: 1;
            bool Detection_SC_BSPD_AMS  : 1;
            bool Detection_SC_Origin_MH : 1;
            bool Detection_VCC_AMS      : 1;
            bool Detection_VCC_FANS_AMS : 1;
            bool Detection_VCC_TSAL     : 1;
            bool Detection_VCC_CUA      : 1;
            bool Detection_VCC_EM       : 1;
            bool Detection_VCC_PUMPS    : 1;
            bool Detection_VCC_FANS     : 1;
            bool Detection_VCC_CUB      : 1;
            bool Detection_VCC_DRS      : 1;
            bool Detection_VCC_CAN_E    : 1;
            bool Detecion_VCC_BL        : 1; 
            bool Detection_VCC_DATA	    : 1; //msb
            			
		};uint16_t FB_SC_Detect; // FuseBox and Shutdown Circuit Detect
	};

	// data[1]
	union {
		struct {
			bool DrsT_sig	 : 1;
			bool DrsB_sig	 : 1;
			bool Blue_sig	 : 1;
			bool Dcdc_switch : 1;
			bool BL_sig      : 1;
			bool Buzz_sig 	 : 1;
		};
		uint16_t Signals;
	};
} DCU_MSG_Status;


// DCU_MSG_ID_CURRENT

typedef struct {
	/*data[0]*/
 	uint16_t LV_current;

	/*data[1]*/
	uint16_t HV_current;

	/*data[2]*/
	uint16_t LV_voltage; 
} DCU_MSG_Current;

// MAIN STRUCT
typedef struct {
	DCU_MSG_Status status;
	DCU_MSG_Current current;
} DCU_CAN_Data;

void parse_dcu_message_status(uint16_t data[4], DCU_MSG_Status *status);
void parse_dcu_message_current_voltage(uint16_t data[4], DCU_MSG_Current *current);
void parse_can_dcu(CANdata message, DCU_CAN_Data *data);

#endif
