#include <stdbool.h>

#include "AHRS_CAN.h"

/* parse fuctions */

void parse_ahrs_message_health(uint16_t data[4], AHRS_MSG_Health* health)
{

    health->health_data = 0b00111111 & data[0];

    return;
}
void parse_ahrs_message_accel(uint16_t data[4], AHRS_MSG_Accel* accel)
{

    accel->value = (int)data[0] / ACCEL_PRECISION;
    accel->time = data[1];
    return;
}

void parse_ahrs_message_gyro(uint16_t data[4], AHRS_MSG_Gyro* gyro)
{

    gyro->value = (int)data[0] / ANGLE_PRECISION;
    gyro->time = data[1];
    return;
}

void parse_ahrs_message_euler(uint16_t data[4], AHRS_MSG_Euler* euler)
{

    euler->value = (int)data[0] / ANGLE_PRECISION;
    euler->time = data[1];
    return;
}

void parse_ahrs_message_normal_load(uint16_t data[4], AHRS_MSG_Normal_load* normal_load)
{

    normal_load->FL = data[0];
    normal_load->FR = data[1];
    normal_load->RL = data[2];
    normal_load->RR = data[3];

    return;
}

void parse_can_ahrs(CANdata message, AHRS_CAN_Data* data)
{
    if ((message.dev_id != DEVICE_ID_AHRS_FRONT) && (message.dev_id != DEVICE_ID_AHRS_REAR)) {
        /*FIXME: send info for error logging*/
        return;
    }

    switch (message.msg_id) {

    case MSG_ID_AHRS_HEALTH:
        parse_ahrs_message_health(message.data, &(data->health));
        break;
    case MSG_ID_AHRS_RAW_ACCEL_X:
        parse_ahrs_message_accel(message.data, &(data->raw_accel_x));
        break;
    case MSG_ID_AHRS_RAW_ACCEL_Y:
        parse_ahrs_message_accel(message.data, &(data->raw_accel_y));
        break;
    case MSG_ID_AHRS_RAW_ACCEL_Z:
        parse_ahrs_message_accel(message.data, &(data->raw_accel_z));
        break;
    case MSG_ID_AHRS_ACCEL_X:
        parse_ahrs_message_accel(message.data, &(data->accel_x));
        break;
    case MSG_ID_AHRS_RAW_GYRO_X:
        parse_ahrs_message_gyro(message.data, &(data->raw_gyro_x));
        break;
    case MSG_ID_AHRS_RAW_GYRO_Y:
        parse_ahrs_message_gyro(message.data, &(data->raw_gyro_y));
        break;
    case MSG_ID_AHRS_RAW_GYRO_Z:
        parse_ahrs_message_gyro(message.data, &(data->raw_gyro_z));
        break;
    case MSG_ID_AHRS_RAW_ROLL:
        parse_ahrs_message_euler(message.data, &(data->raw_euler_roll));
        break;
    case MSG_ID_AHRS_RAW_PITCH:
        parse_ahrs_message_euler(message.data, &(data->raw_euler_pitch));
        break;
    case MSG_ID_AHRS_RAW_YAW:
        parse_ahrs_message_euler(message.data, &(data->raw_euler_yaw));
        break;
    case MSG_ID_AHRS_RAW_ROLL_RATE:
        parse_ahrs_message_euler(message.data, &(data->raw_euler_roll_rate));
        break;
    case MSG_ID_AHRS_RAW_PITCH_RATE:
        parse_ahrs_message_euler(message.data, &(data->raw_euler_pitch_rate));
        break;
    case MSG_ID_AHRS_RAW_YAW_RATE:
        parse_ahrs_message_euler(message.data, &(data->raw_euler_yaw_rate));
        break;
    case MSG_ID_AHRS_YAW_RATE:
        parse_ahrs_message_euler(message.data, &(data->euler_yaw_rate));
        break;
    case MSG_ID_AHRS_NORMAL_LOAD:
        parse_ahrs_message_normal_load(message.data, &(data->normal_load));
        break;
    }
}