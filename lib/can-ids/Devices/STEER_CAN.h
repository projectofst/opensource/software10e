#ifndef _STEER_CAN_H
#define _STEER_CAN_H

#include "../CAN_IDs.h"

#include <stdint.h>

#define MSG_ID_STEERING_WHEEL_STATUS   32
//MSG_IDs for Steering Wheel mechanical encoder: 40 + ID mechanical encoder
#define MSG_ID_STEERING_WHEEL_ME_0     40
#define MSG_ID_STEERING_WHEEL_ME_1     41
#define MSG_ID_STEERING_WHEEL_ME_2     42
//MSG_IDs for Steering Wheel Buttons: 50 + ID button
#define MSG_ID_STEERING_WHEEL_BUT_0    50
#define MSG_ID_STEERING_WHEEL_BUT_1    51
#define MSG_ID_STEERING_WHEEL_BUT_2    52
#define MSG_ID_STEERING_WHEEL_BUT_3    53
#define MSG_ID_STEERING_WHEEL_BUT_4    54
#define MSG_ID_STEERING_WHEEL_BUT_5    55
#define MSG_ID_STEERING_WHEEL_BUT_6    56
#define MSG_ID_STEERING_WHEEL_BUT_7    57
#define MSG_ID_STEERING_WHEEL_BUT_8    58
#define MSG_ID_STEERING_WHEEL_BUT_9    59

/* Parameters */
//Version
#define P_STEERING_WHEEL_VERSION_ID_0   0
#define P_STEERING_WHEEL_VERSION_ID_1	  1
#define P_STEERING_WHEEL_VERSION_ID_2	  2
#define P_STEERING_WHEEL_VERSION_ID_3	  3
//LEDs Maximum Intensity
#define P_STEERING_WHEEL_LED_INTENSITY  4
//LEDs - on/off and intensity
#define P_STEERING_WHEEL_LED_0          5
#define P_STEERING_WHEEL_LED_1          6
#define P_STEERING_WHEEL_LED_2          7
#define P_STEERING_WHEEL_LED_3          8
#define P_STEERING_WHEEL_LED_4          9
#define P_STEERING_WHEEL_LED_5          10
#define P_STEERING_WHEEL_LED_6          11
#define P_STEERING_WHEEL_LED_7          12
#define P_STEERING_WHEEL_LED_8          13
#define P_STEERING_WHEEL_LED_9          14
#define P_STEERING_WHEEL_LED_10         15
#define P_STEERING_WHEEL_LED_11         16
#define P_STEERING_WHEEL_LED_12         17
#define P_STEERING_WHEEL_LED_13         18
#define P_STEERING_WHEEL_LED_14         19
#define P_STEERING_WHEEL_LED_15         20
#define P_STEERING_WHEEL_LED_16         21
#define P_STEERING_WHEEL_LED_17         22
#define P_STEERING_WHEEL_LED_18         23
#define P_STEERING_WHEEL_LED_19         24
#define P_STEERING_WHEEL_LED_20         25
#define P_STEERING_WHEEL_LED_21         26
#define P_STEERING_WHEEL_LED_22         27
#define P_STEERING_WHEEL_LED_23         28


typedef struct{
    uint8_t ME0Pos;
    uint8_t ME1Pos;
    uint8_t ME2Pos;
}STEERING_WHEEL_MSG_status;

void parse_can_message_steering_wheel_status(uint16_t data[4], STEERING_WHEEL_MSG_status *steer);
void parse_can_steering_wheel(CANdata message, STEERING_WHEEL_MSG_status *steer);

#endif
