#ifndef __AHRS_CAN_H__
#define __AHRS_CAN_H__

#include "../CAN_IDs.h"
#include <stdint.h>

/* SET and GET parameters */
#define P_AHRS_VERSION_0                    0       /* NEVER CHANGE THIS 4 DEFINES */
#define P_AHRS_VERSION_1                    1
#define P_AHRS_VERSION_2                    2
#define P_AHRS_VERSION_3                    3


#define MSG_ID_AHRS_HEALTH          	    32
#define MSG_ID_AHRS_RAW_ACCEL_X             33
#define MSG_ID_AHRS_RAW_ACCEL_Y             34
#define MSG_ID_AHRS_RAW_ACCEL_Z             35
#define MSG_ID_AHRS_RAW_GYRO_X              36
#define MSG_ID_AHRS_RAW_GYRO_Y             	37
#define MSG_ID_AHRS_RAW_GYRO_Z              38
#define MSG_ID_AHRS_RAW_ROLL                39
#define MSG_ID_AHRS_RAW_PITCH               40
#define MSG_ID_AHRS_RAW_YAW                 41
#define MSG_ID_AHRS_RAW_ROLL_RATE           42
#define MSG_ID_AHRS_RAW_PITCH_RATE          43
#define MSG_ID_AHRS_RAW_YAW_RATE            44

#define MSG_ID_AHRS_ACCEL_X                 45
#define MSG_ID_AHRS_YAW_RATE                46
#define MSG_ID_AHRS_NORMAL_LOAD             47

#define ANGULAR_RATE_ABSURD                 330
#define ACCEL_ABSURD                        50
#define ACCEL_PRECISION                     1000.0
#define ANGLE_PRECISION                     100.0

/* DREG_HEALTH -    reports the current status of the GPS
                    module and the other sensors on the UM7 */
typedef struct {
    union{
        struct{
            uint8_t OVF :1;     /* This bit is set if the UM7 is attempting to transmit data over
                                the serial port faster than is allowed given the baud-rate */
            uint8_t MG_N :1;    /*This bit is set if the sensor detects that the norm of the
                                magnetometer measurement is too far away from 1.0 to be
                                trusted. Usually indicates bad calibration, local field
                                distortions, or both. */
            uint8_t ACC_N :1;   /*This bit is set if the sensor detects that the norm of the
                                accelerometer measurement is too far away from 1G to be
                                used */
            uint8_t ACCEL :1;   /*accelerometer fails to initialize on startup */
            uint8_t GYRO :1;    /* rate gyro fails to initialize on startup */
            uint8_t MAG :1;     /* magnetometer fails to initialize on startup */
        };
        uint8_t health_data;
    };

}AHRS_MSG_Health;

/* ACCEL -          measured acceleration in m/s/s
                    after calibration has been applied */
typedef struct{
    float value;                     /*  acceleration in m/s/s */
    uint16_t time;                     /* time at witch last value was acquired */
}AHRS_MSG_Accel;

/* GYRO -           measured angular rate in degrees/s
                    after calibration has been applied */
typedef struct{
    float value;                     /* angular rates in degrees per second */
    uint16_t time;                     /* time at witch last value was acquired */
}AHRS_MSG_Gyro;


/* EULER -            */
typedef struct{
    float value;                     /* angle/rate estimation */
    uint16_t time;                     /* time at witch last value was acquired */
}AHRS_MSG_Euler;

typedef struct{
    uint16_t FL;
    uint16_t FR;
    uint16_t RL;
    uint16_t RR;
}AHRS_MSG_Normal_load;


// MAIN STRUCT
typedef struct {
    AHRS_MSG_Health health;
    AHRS_MSG_Accel raw_accel_x;
    AHRS_MSG_Accel raw_accel_y;
    AHRS_MSG_Accel raw_accel_z;
    AHRS_MSG_Accel accel_x;
    AHRS_MSG_Gyro raw_gyro_x;
    AHRS_MSG_Gyro raw_gyro_y;
    AHRS_MSG_Gyro raw_gyro_z;
    AHRS_MSG_Euler raw_euler_roll;
    AHRS_MSG_Euler raw_euler_pitch;
    AHRS_MSG_Euler raw_euler_yaw;
    AHRS_MSG_Euler raw_euler_roll_rate;
    AHRS_MSG_Euler raw_euler_pitch_rate;
    AHRS_MSG_Euler raw_euler_yaw_rate;
    AHRS_MSG_Euler euler_yaw_rate;
    AHRS_MSG_Normal_load normal_load;
}AHRS_CAN_Data;


void parse_ahrs_message_euler(uint16_t data[4], AHRS_MSG_Euler *euler);
void parse_ahrs_message_gyro(uint16_t data[4], AHRS_MSG_Gyro *gyro);
void parse_ahrs_message_accel(uint16_t data[4], AHRS_MSG_Accel *accel);
void parse_ahrs_message_health(uint16_t data[4], AHRS_MSG_Health *health);
void parse_ahrs_message_normal_load(uint16_t data[4], AHRS_MSG_Normal_load *normal_load);
void parse_can_ahrs(CANdata message, AHRS_CAN_Data *data);


#endif
