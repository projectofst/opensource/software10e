#ifndef __TELEMETRY_H__
#define __TELEMETRY_H__

#include <stdint.h>
#include "../CAN_IDs.h"

typedef struct{
	uint16_t status;
}Telemetry_status;

#define MSG_ID_TELEMETRY_STATUS 		32

typedef struct{
	uint16_t device;
	uint16_t parameter;
	uint16_t value;
}Flash_message;

#define MSG_ID_TELEMETRY_FLASH                  33

typedef struct{
	Telemetry_status status;
	Flash_message flash;
}Telemetry_CAN_data;


void parse_Telemetry_status_message(uint16_t data[4], Telemetry_status* status);
void parse_Telemetry_flash_message(uint16_t data[4], Flash_message* flash);
void parse_can_Telemetry(CANdata message, Telemetry_CAN_data* telemetry);


#endif
