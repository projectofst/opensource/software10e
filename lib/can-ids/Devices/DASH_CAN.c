#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "../CAN_IDs.h"
#include "DASH_CAN.h"

void parse_can_message_dash_status(uint16_t data[4], DASH_MSG_status* status)
{

    status->SC = data[0] & 1;
    // status->debug_mode = data[1];

    return;
}

void parse_can_message_dash_SE(uint16_t data[4], DASH_MSG_SE* SE)
{

    SE->value = (int16_t)data[0];

    return;
}

void parse_can_dash(CANdata message, DASH_CAN_Data* data)
{
    if (message.dev_id != DEVICE_ID_DASH) {
        /*FIXME: send info for error logging*/
        return;
    }

    switch (message.msg_id) {
    case MSG_ID_DASH_STATUS:
        parse_can_message_dash_status(message.data, &(data->status));
        break;
    case MSG_ID_DASH_SE:
        parse_can_message_dash_SE(message.data, &(data->SE));
        break;
    }

    return;
}
