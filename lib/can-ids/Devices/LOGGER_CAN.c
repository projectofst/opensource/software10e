#include "LOGGER_CAN.h"
#include "../CAN_IDs.h"
#include "COMMON_CAN.h"
#include <stdbool.h>
#include <stdint.h>

#define MSG_ID_LOGGER_STATUS 32

void parse_logger_message_status(uint16_t data[4], LOGGER_MSG_STATUS* status)
{

    status->state = (LOG_STATE)data[0];

    return;
}
void parse_can_logger(CANdata message, LOGGER_CAN_Data* data)
{
    if (message.dev_id != DEVICE_ID_LOGGER) {
        // make_parse_error_msg(DEVICE_ID_LOGGER, message.dev_id, DEVICE_ID_LOGGER);
        return;
    }

    switch (message.msg_id) {
    case MSG_ID_LOGGER_STATUS:
        parse_logger_message_status(message.data, &(data->status));
        break;
    }
}
