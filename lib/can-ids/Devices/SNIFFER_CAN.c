#include "SNIFFER_CAN.h"

void parse_sniffer_message_stats(CANdata msg, SNIFFER_MSG_Data* data)
{
    data->msgs_read = msg.data[0];
    return;
}

void parse_can_sniffer(CANdata msg, SNIFFER_MSG_Data* data)
{
    if (msg.msg_id == MSG_ID_SNIFFER_STATS) {
        parse_sniffer_message_stats(msg, data);
    }
    return;
}
