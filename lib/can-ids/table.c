#include "table.h"

const char* nomes_placas[] = {
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "dcu",
    "te",
    "dash",
    "logger",
    "sw",
    "arm",
    "master",
    "15",
    "iib",
    "telemetry",
    "18",
    "hw",
    "20",
    "21",
    "22",
    "23",
    "ccu_left",
    "ccu_right",
    "26",
    "27",
    "28",
    "isabel",
    "30",
    "interface",
    "strain_gauges",

};
