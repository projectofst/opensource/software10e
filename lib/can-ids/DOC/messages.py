#!/usr/bin/env python3

import sys, os
from pprint import pprint
from collections import namedtuple

from copy import deepcopy as copy


def open_file(file):
    file_pair = namedtuple("FilePair", ["filename", "contents"])
    file_pair.filename = file
    with open(file) as f:
        file_pair.contents = f.read()

    return file_pair


def get_files(folder):
    devs = os.path.join(folder, "Devices")
    can_ids_h = os.path.join(folder, "CAN_IDs.h")

    devices_h = [os.path.join(devs, f) for f in os.listdir(devs) if ".h" in f]
    devices_c = [os.path.join(devs, f) for f in os.listdir(devs) if ".c" in f]

    aux = {}
    aux["can-ids-h"] = open_file(can_ids_h)

    aux["devices_h"] = []
    aux["devices_c"] = []

    for dev in devices_h:
        aux["devices_h"].append(open_file(dev))

    for dev in devices_c:
        aux["devices_c"].append(open_file(dev))

    return aux


def process_device(line):
    to_remove = len("DEVICE_ID_")
    sp = line.split()
    return sp[1][to_remove:], int(sp[2])


def process_common_cmd(line):
    to_remove = len("CMD_ID_")
    sp = line.split()
    return sp[1], int(sp[2])


def process_common_msg(line):
    to: remove = len("MSG_ID_")
    sp = line.split()
    return sp[1], int(sp[2])


def process_can_ids_h(files):
    filename, contents = files["can-ids-h"].filename, files["can-ids-h"].contents

    dev_ids = []
    cmds = []
    msgs = []

    for line in contents.split("\n"):
        if len(line.split()) > 3:
            continue
        if "define" in line:
            if "DEVICE" in line:
                dev_ids.append(process_device(line))
            if "CMD" in line:
                cmds.append(process_common_cmd(line))
                continue
            if "MSG" in line:
                msgs.append(process_common_msg(line))
                # msg message
                continue

    return dev_ids, cmds, msgs


def process_msg(msg):
    sp = msg.split()
    to_remove = len("MSG_ID_")
    return sp[1][to_remove:], sp[2]


def process_file(file):
    msgs = []

    for line in file.split("\n"):
        if "define" in line:
            if "MSG" in line:
                msgs.append(process_msg(line))
    return msgs


def process_filename(filename):
    to_remove = len("_CAN")
    return filename.split("/")[2].split(".")[0][:-to_remove]


def process_other_devices(files):
    msgs_device = {}
    for filepair in files["devices_h"]:
        filename = filepair.filename
        contents = filepair.contents
        msgs = process_file(contents)
        name = process_filename(filename)
        msgs_device[name] = msgs

    return msgs_device


def merge_ids(dev_ids, data):
    matches = {}

    for dev_id, dev_id_number in dev_ids:
        for device_name, messages in data.items():
            if device_name in dev_id.split("_") or device_name == dev_id:
                matches[dev_id] = copy((dev_id_number, data[device_name]))
                break

    return matches


def match_common(matches, commons):

    for dev_id, payload in matches.items():
        message_number, messages = payload
        for common in commons:
            if "COMMON" in common[0]:
                matches[dev_id][1].append(common)

    return matches


def match_priority(mathces, commons):
    return matches


def count_char(line, char):
    count = 0
    for l in line:
        if l == char:
            count += 1

    return count


def found_function(file, i):
    stack = 0

    for line in range(i, i + 2):
        for c in file[line]:
            if c == "{":
                stack += 1

    n = 2
    while stack != 0:
        stack += count_char(file[i + n], "{")
        stack -= count_char(file[i + n], "}")

        n += 1

    return i + n


def parse_data_line(line):
    def handle_right_side(rs):
        n = 0
        for i, r in enumerate(rs):
            if "data" == rs[i : i + 4]:
                n = i + 4
                break

        if not ("[" in rs[n:] or "]" in rs[n:]):
            return

    print(line)
    ls, rs = line.split("=")

    handle_right_side(rs)


def parse_function(lines):
    # check if function is device parse
    for l in lines[2:]:
        if "parse" in l:
            print(lines[0])
            return

    for l in lines[1:]:
        if "data" in l:
            print(l)
            parse_data_line(l)


def process_can_c(file):
    lines = file.split("\n")
    for i, line in enumerate(lines):
        if "parse" in line and "void" in line:
            n = found_function(lines, i)
            # print("=======================================================")
            # print(i, n, lines[i])
            parse_function(lines[i:n])
            # print(line)


def process_datas(files):
    for file in files["devices_c"]:
        process_can_c(file.contents)


def main(arg):
    if len(arg) != 2:
        print("Usage: messages can-ids")
        exit()

    can_ids = arg[1]

    files = get_files(can_ids)

    dev_ids, cmds, msgs = process_can_ids_h(files)

    process_datas(files)

    data = process_other_devices(files)

    matches = merge_ids(dev_ids, data)
    # pprint(matches)
    matches = match_common(matches, cmds + msgs)

    for dev_id, payload in matches.items():
        device_number, messages = payload
        for message_name, message_number in messages:
            continue
            print(
                "{0},{1},{2},{3}".format(
                    dev_id, message_name, device_number, message_number
                ).expandtabs(30)
            )

    return


main(sys.argv)
