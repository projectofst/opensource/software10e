#ifndef __IO_H__
#define __IO_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define get_port(PARAMS) get_port2(PARAMS)
#define set_port(PARAMS, VALUE) set_port2(PARAMS, VALUE)
#define xstr(s) str(s)
#define str(s) #s

#ifndef LINUX
#define get_port2(P, PIN) PORT ## P ## bits.R ## P ## PIN
#else
#define get_port2(P, PIN) get_port_fake(str(P), PIN)
#endif

#ifndef LINUX
#define set_port2(P, PIN, VALUE) PORT ## P ## bits.R ## P ## PIN = VALUE
#else
#define set_port2(P, PIN, VALUE) set_port_fake(str(P), PIN, VALUE)
#endif

#ifndef LINUX
#define get_lat2(P, PIN) LAT ## P ## bits.R ## P ## PIN
#else
#define get_lat2(P, PIN) get_port_fake(str(P), PIN)
#endif

#ifndef LINUX
#define set_lat2(P, PIN, VALUE) LAT ## P ## bits.R ## P ## PIN = VALUE
#else
#define set_lat2(P, PIN, VALUE) set_port_fake(str(P), PIN, VALUE)
#endif

#ifdef LINUX
uint16_t get_port_fake(const char *group, uint16_t pin);
void set_port_fake(const char *group, uint16_t pin, uint16_t value);
#endif

#endif


