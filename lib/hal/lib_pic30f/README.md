# Module libraries for dsPCI30F

## TIMER
* `timer1_config`: timer 1 configuration
* `timer2_config`: timer 2 configuration
* `timer3_config`: timer 3 configuration
* `timer4_config`: timer 4 configuration
* `timer5_config`: timer 5 configuration
<br><br>
* `timer1_callback`: function called inside timer 1 interrupt
* `timer2_callback`: function called inside timer 2 interrupt
* `timer3_callback`: function called inside timer 3 interrupt
* `timer4_callback`: function called inside timer 4 interrupt
* `timer5_callback`: function called inside timer 5 interrupt


## CAN
* `config_can1_filters`: configs up to 6 acceptance filters, to call after config can
* `config_can1`: configures CAN module 1
* `send_can1`: sends CANdata to CAN 1
* `pop_can1`: get CANdata from CAN buffer 1
* `filter_can1`: software filter for CAN message id, returns bool, defined as weak
<br><br>
* `config_can2_filters`: configs up to 6 acceptance filters, to call after config can
* `config_can2`: configures CAN module 2
* `send_can2`: sends CANdata to CAN 2
* `pop_can2`: get CANdata from CAN buffer 2
* `filter_can2`: software filter for CAN message id, returns bool, defined as weak

The `CAN_buffer`, `CAN_write` and `CAN_read` variables for both CAN modules are defined in `can.h`, there is no reason for you to read or write to these variables. Prefer to use the provided accessor functions.

`CAN1_BUFFER_LENGTH` and `CAN2_BUFFER_LENGTH` define the size of the message buffers, since no proper soluction was found the buffers default to the size of 64 messages. These constants are defined in `can.h` .


## EEPROM

Read the instructions present in `EEPROM.c`. There you will have all the instructions on how to use the eeprom.
To use the eeprom, the funtions you will need are:
* `EEPROM_init_addr`: initiates the variable address in the eeprom
* `eeprom_store_value`: stores the given value in the given eeprom address
* `eeprom_read_word`: reads the value stored in the given eeprom address
* `eeprom_erase_word`: erases the value stored in the given eeprom address

Once again it's important that you read the instructions in `EEPROM.c` in order to use the eeprom properly.

