# How to contribute 
To create a new module create a new feature branch and create a merge request to master.
To improve an existing module commit your changes to the existing feature branch and create a merge request to master.

# Documentation
Module usage should be documented in a README.md file in the module directory.

# General code standarts apply
[Writing-C](https://www.gnu.org/prep/standards/html_node/Writing-C.html)

