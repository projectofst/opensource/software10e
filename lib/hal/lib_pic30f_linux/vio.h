#ifndef __VIO_H__
#define __VIO_H__

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>


#include "Queue/Queue.h"
#include "sts_queue/sts_queue.h"

#define VIO_BUFFER_MAX 1024

typedef enum {ADC, INPUT} Vio_type;

typedef struct _Vio{
	Vio_type type; 
	int value;
	int len;
	char *id;
	int pin;
	char group;
} Vio;


typedef struct _Connection {
	struct sockaddr_in addr;
	int socket;
	Queue_t queue;
	StsHeader *adc;
	StsHeader *input;
} Connection;

Connection vio_open_conn(const char *host, unsigned port);
void *vio_recv_msg(void *conn);

Vio *vio_unpack(unsigned char *buffer, int  n);


#endif
