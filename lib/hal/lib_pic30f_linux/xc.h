#ifndef __XC_H__
#define __XC_H__
#ifdef LINUX

#include <unistd.h>
#include <stdint.h>

#include <config.h>

// SFR

typedef struct tagPORTGBITS {
  uint16_t RG0:1;
  uint16_t RG1:1;
  uint16_t RG2:1;
  uint16_t RG3:1;
  uint16_t RG4:1;
  uint16_t RG5:1;
  uint16_t RG6:1;
  uint16_t RG7:1;
} PORTGBITS;

static PORTGBITS PORTGbits;

typedef struct tagLATgBITS {
  uint16_t LATG0:1;
  uint16_t LATG1:1;
  uint16_t LATG2:1;
  uint16_t LATG3:1;
  uint16_t LATG4:1;
  uint16_t LATG5:1;
  uint16_t LATG6:1;
  uint16_t LATG7:1;
  uint16_t LATG8:1;
  uint16_t LATG9:1;
  uint16_t LATG10:1;
  uint16_t LATG11:1;
  uint16_t LATG12:1;
  uint16_t LATG13:1;
  uint16_t LATG14:1;
  uint16_t LATG15:1;
  uint16_t LATG16:1;
} LATGBITS;

static LATGBITS LATGbits;

typedef struct tagLATEBITS {
  uint16_t LATE0:1;
  uint16_t LATE1:1;
  uint16_t LATE2:1;
  uint16_t LATE3:1;
  uint16_t LATE4:1;
  uint16_t LATE5:1;
  uint16_t LATE6:1;
  uint16_t LATE7:1;
} LATEBITS;

static LATEBITS LATEbits;

typedef struct tagTRISEBITS {
  uint16_t TRISE0:1;
  uint16_t TRISE1:1;
  uint16_t TRISE2:1;
  uint16_t TRISE3:1;
  uint16_t TRISE4:1;
  uint16_t TRISE5:1;
  uint16_t TRISE6:1;
  uint16_t TRISE7:1;
} TRISEBITS;

static TRISEBITS TRISEbits;

typedef struct tagPORTEBITS {
  uint16_t RE0:1;
  uint16_t RE1:1;
  uint16_t RE2:1;
  uint16_t RE3:1;
  uint16_t RE4:1;
  uint16_t RE5:1;
  uint16_t RE6:1;
  uint16_t RE7:1;
} PORTEBITS;

static PORTEBITS PORTEbits;


typedef struct tagLATDBITS {
  uint16_t LATD0:1;
  uint16_t LATD1:1;
  uint16_t LATD2:1;
  uint16_t LATD3:1;
  uint16_t LATD4:1;
  uint16_t LATD5:1;
  uint16_t LATD6:1;
  uint16_t LATD7:1;
  uint16_t LATD8:1;
  uint16_t LATD9:1;
  uint16_t LATD10:1;
  uint16_t LATD11:1;
} LATDBITS;

static LATDBITS LATDbits;

typedef struct tagPORTDBITS {
  uint16_t RD0:1;
  uint16_t RD1:1;
  uint16_t RD2:1;
  uint16_t RD3:1;
  uint16_t RD4:1;
  uint16_t RD5:1;
  uint16_t RD6:1;
  uint16_t RD7:1;
  uint16_t RD8:1;
  uint16_t RD9:1;
  uint16_t RD10:1;
  uint16_t RD11:1;
} PORTDBITS;

static PORTDBITS PORTDbits;

typedef struct tagTRISDBITS {
  uint16_t TRISD0:1;
  uint16_t TRISD1:1;
  uint16_t TRISD2:1;
  uint16_t TRISD3:1;
  uint16_t TRISD4:1;
  uint16_t TRISD5:1;
  uint16_t TRISD6:1;
  uint16_t TRISD7:1;
  uint16_t TRISD8:1;
  uint16_t TRISD9:1;
  uint16_t TRISD10:1;
  uint16_t TRISD11:1;
} TRISDBITS;

static TRISDBITS TRISDbits;

typedef struct tagLATBBITS {
  uint16_t LATB0:1;
  uint16_t LATB1:1;
  uint16_t LATB2:1;
  uint16_t LATB3:1;
  uint16_t LATB4:1;
  uint16_t LATB5:1;
  uint16_t LATB6:1;
  uint16_t LATB7:1;
  uint16_t LATB8:1;
  uint16_t LATB9:1;
  uint16_t LATB10:1;
  uint16_t LATB11:1;
  uint16_t LATB12:1;
  uint16_t LATB13:1;
  uint16_t LATB14:1;
  uint16_t LATB15:1;
} LATBBITS;

static LATBBITS LATBbits;

typedef struct tagPORTBBITS {
  uint16_t RB0:1;
  uint16_t RB1:1;
  uint16_t RB2:1;
  uint16_t RB3:1;
  uint16_t RB4:1;
  uint16_t RB5:1;
  uint16_t RB6:1;
  uint16_t RB7:1;
  uint16_t RB8:1;
  uint16_t RB9:1;
  uint16_t RB10:1;
  uint16_t RB11:1;
  uint16_t RB12:1;
  uint16_t RB13:1;
  uint16_t RB14:1;
  uint16_t RB15:1;
} PORTBBITS;

static PORTBBITS PORTBbits;

typedef struct tagTRISBBITS {
  uint16_t TRISB0:1;
  uint16_t TRISB1:1;
  uint16_t TRISB2:1;
  uint16_t TRISB3:1;
  uint16_t TRISB4:1;
  uint16_t TRISB5:1;
  uint16_t TRISB6:1;
  uint16_t TRISB7:1;
  uint16_t TRISB8:1;
  uint16_t TRISB9:1;
  uint16_t TRISB10:1;
  uint16_t TRISB11:1;
  uint16_t TRISB12:1;
  uint16_t TRISB13:1;
  uint16_t TRISB14:1;
  uint16_t TRISB15:1;
} TRISBBITS;

static TRISBBITS TRISBbits;

typedef struct tagLATCBITS {
  uint16_t :12;
  uint16_t LATC12:1;
  uint16_t LATC13:1;
  uint16_t LATC14:1;
  uint16_t LATC15:1;
} LATCBITS;

static LATCBITS LATCbits;

typedef struct tagTRISCBITS {

    uint16_t TRISC1:1;
    uint16_t :11;
    uint16_t TRISC12:1;
    uint16_t TRISC13:1;
    uint16_t TRISC14:1;
    uint16_t TRISC15:1;
} TRISCBITS;

static TRISCBITS TRISCbits;

typedef struct tagPORTCBITS {
  uint16_t :12;
  uint16_t RC12:1;
  uint16_t RC13:1;
  uint16_t RC14:1;
  uint16_t RC15:1;
} PORTCBITS;

static PORTCBITS PORTCbits;

typedef struct tagLATFBITS {
  uint16_t LATF0:1;
  uint16_t LATF1:1;
  uint16_t :1;
  uint16_t LATF3:1;
  uint16_t LATF4:1;
  uint16_t LATF5:1;
} LATFBITS;

static LATFBITS LATFbits;

typedef struct tagTRISFBITS {
  uint16_t TRISF0:1;
  uint16_t TRISF1:1;
  uint16_t TRISF2:1;
  uint16_t :1;
  uint16_t TRISF3:1;
  uint16_t TRISF4:1;
  uint16_t TRISF5:1;
  uint16_t TRISF6:1;
} TRISFBITS;

static TRISFBITS TRISFbits;

typedef struct tagPORTFBITS {
  uint16_t RF0:1;
  uint16_t RF1:1;
  uint16_t RF2:1;
  uint16_t RF3:1;
  uint16_t RF4:1;
  uint16_t RF5:1;
  uint16_t RF6:1;
} PORTFBITS;

static PORTFBITS PORTFbits;

typedef struct tagTRISGBITS {
  uint16_t TRISG0:1;
  uint16_t TRISG1:1;
  uint16_t TRISG2:1;
  uint16_t TRISG3:1;
  uint16_t TRISG4:1;
  uint16_t TRISG5:1;
  uint16_t TRISG6:1;
  uint16_t TRISG7:1;
  uint16_t TRISG8:1;
  uint16_t TRISG9:1;
  uint16_t TRISG10:1;
  uint16_t TRISG11:1;
  uint16_t TRISG12:1;
  uint16_t TRISG13:1;
  uint16_t TRISG14:1;
  uint16_t TRISG15:1;
} TRISGBITS;

static TRISGBITS TRISGbits;

typedef struct tagINTCON1BITS {
  uint16_t :1;
  uint16_t OSCFAIL:1;
  uint16_t STKERR:1;
  uint16_t ADDRERR:1;
  uint16_t MATHERR:1;
  uint16_t DMACERR:1;
  uint16_t DIV0ERR:1;
  uint16_t SFTACERR:1;
  uint16_t COVTE:1;
  uint16_t OVBTE:1;
  uint16_t OVATE:1;
  uint16_t COVBERR:1;
  uint16_t COVAERR:1;
  uint16_t OVBERR:1;
  uint16_t OVAERR:1;
  uint16_t NSTDIS:1;
} INTCON1BITS;

static INTCON1BITS INTCON1bits;


typedef struct tagOC1CON2BITS {
  union {
    struct {
      uint16_t SYNCSEL:5;
      uint16_t OCTRIS:1;
      uint16_t TRIGSTAT:1;
      uint16_t OCTRIG:1;
      uint16_t OC32:1;
      uint16_t :3;
      uint16_t OCINV:1;
      uint16_t FLTTRIEN:1;
      uint16_t FLTOUT:1;
      uint16_t FLTMD:1;
    };
    struct {
      uint16_t SYNCSEL0:1;
      uint16_t SYNCSEL1:1;
      uint16_t SYNCSEL2:1;
      uint16_t SYNCSEL3:1;
      uint16_t SYNCSEL4:1;
      uint16_t :9;
      uint16_t FLTMODE:1;
    };
  };
} OC1CON2BITS;

static OC1CON2BITS OC1CON2bits;

static uint16_t OC1CON2;

typedef struct tagOC1CON1BITS {
  union {
    struct {
      uint16_t OCM:3;
      uint16_t TRIGMODE:1;
      uint16_t OCFLTA:1;
      uint16_t OCFLTB:1;
      uint16_t OCFLTC:1;
      uint16_t ENFLTA:1;
      uint16_t ENFLTB:1;
      uint16_t ENFLTC:1;
      uint16_t OCTSEL:3;
      uint16_t OCSIDL:1;
    };
    struct {
      uint16_t OCM0:1;
      uint16_t OCM1:1;
      uint16_t OCM2:1;
      uint16_t :1;
      uint16_t OCFLT:3;
      uint16_t ENFLT:3;
      uint16_t OCTSEL0:1;
      uint16_t OCTSEL1:1;
      uint16_t OCTSEL2:1;
    };
    struct {
      uint16_t :4;
      uint16_t OCFLT0:1;
      uint16_t OCFLT1:1;
      uint16_t OCFLT2:1;
      uint16_t ENFLT0:1;
      uint16_t ENFLT1:1;
      uint16_t ENFLT2:1;
    };
  };
} OC1CON1BITS;

static OC1CON1BITS OC1CON1bits;

static uint16_t OC1CON1;

static uint16_t OC1RS;
static uint16_t OC1R;

typedef struct tagIFS0BITS {
  uint16_t INT0IF:1;
  uint16_t IC1IF:1;
  uint16_t OC1IF:1;
  uint16_t T1IF:1;
  uint16_t DMA0IF:1;
  uint16_t IC2IF:1;
  uint16_t OC2IF:1;
  uint16_t T2IF:1;
  uint16_t T3IF:1;
  uint16_t SPI1EIF:1;
  uint16_t SPI1IF:1;
  uint16_t U1RXIF:1;
  uint16_t U1TXIF:1;
  uint16_t AD1IF:1;
  uint16_t DMA1IF:1;
  uint16_t NVMIF:1;
} IFS0BITS;

static IFS0BITS IFS0bits;

static uint16_t CNPDB;

#define ClrWdt() usleep(SIM_DELTA_T_US)
#define Idle()

static uint16_t RCON;

static uint16_t ADCBUF0;
static uint16_t ADCBUF1;
static uint16_t ADCBUF2;
static uint16_t ADCBUF3;
static uint16_t ADCBUF4;
static uint16_t ADCBUF5;


typedef struct tagIEC0BITS {
	uint16_t INT0IE:1;
  	uint16_t IC1IE:1;
  	uint16_t OC1IE:1;
  	uint16_t T1IE:1;
  	uint16_t IC2IE:1;
  	uint16_t OC2IE:1;
  	uint16_t T2IE:1;
  	uint16_t T3IE:1;
  	uint16_t SPI1IE:1;
  	uint16_t U1RXIE:1;
  	uint16_t U1TXIE:1;
  	uint16_t ADIE:1;
  	uint16_t NVMIE:1;
  	uint16_t SI2CIE:1;
  	uint16_t MI2CIE:1;
  	uint16_t CNIE:1;
} IEC0BITS;
static IEC0BITS IEC0bits;

#endif
#endif
