#include <stdint.h>

#ifndef LINUX
#define _EEDATA(N)   __attribute__((space(eedata),aligned(N)))
#else
#define _EEDATA(N)
#endif
#define _init_prog_address(EE_address, initial_EE_thresholds)

typedef uint16_t _prog_addressT;
