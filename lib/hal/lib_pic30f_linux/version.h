#ifndef __VERSION_H__
#define __VERSION_H__

/**
 *! \file version.h
 *  \brief Provides functions to deal with project versions
 *
 *   Copyright 2018 David Ribeiro
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdint.h>

typedef enum{
    VEMPTY       = 0,
    VSUCCESS		= 1,
} VERSIONerror;

/*! \fn get_version(uint64_t version, uint16_t *data)
 * \brief Obtains your current code version from your commit log
 * \param[in] version 64 bit number with version number
 * \param[out] data 16 bit array to fill with the version number
 * Fills the buffer with |char7char6|char5char4|char3char2|char1char0|
 */
VERSIONerror get_version(uint64_t version, uint16_t *data);

#endif
