#include "lib_pic30f/io.h"


#ifdef LINUX

uint16_t pin_value [7] = {0, 0, 0, 0, 0, 0, 0}; 

uint16_t get_port_fake(const char *group, uint16_t pin){
    
    uint16_t port_value = 0;    
    
    port_value = (pin_value[group[0] - 'A'] >> pin) & 1; 
    return port_value;
}


void set_port_fake(const char *group, uint16_t pin, uint16_t value) {
    if (value == 0) {
        pin_value[group[0] - 'A'] &= ~(1 << pin);
    }
    else if(value == 1) {
        pin_value[group[0] - 'A'] |= (1 << pin);
    }
}
 

#endif





