#include "timer.h"

#include <stdint.h>
#include <stdlib.h>

#include <unistd.h>
#include <pthread.h>


void *timer1_thread(void *ptr) {
	unsigned time = *((unsigned *) ptr);
	while (1) {
		//printf("timer\n");
		timer1_callback();
		usleep(time*1000);
	}
}

void *timer2_thread(void *ptr) {
	unsigned time = *((unsigned *) ptr);
	while (1) {
		timer2_callback();
		usleep(time*1000);
	}
}

void *timer3_thread(void *ptr) {
	unsigned time = *((unsigned *) ptr);
	while (1) {
		timer3_callback();
		usleep(time*1000);
	}
}

void config_timer1(unsigned int time, unsigned int priority) {
	pthread_t thread1;

	unsigned *time_value = (unsigned *) malloc(sizeof(unsigned));
	*time_value = time;

    pthread_create(&thread1, NULL, timer1_thread, (void *) time_value);
	return;
}

void config_timer2(unsigned int time, unsigned int priority) {
	pthread_t thread1;

	unsigned *time_value = (unsigned *) malloc(sizeof(unsigned));
	*time_value = time;

    pthread_create(&thread1, NULL, timer2_thread, (void *) time_value);
	return;
}

void config_timer3(unsigned int time, unsigned int priority) {
	pthread_t thread1;

	unsigned *time_value = (unsigned *) malloc(sizeof(unsigned));
	*time_value = time;

    pthread_create(&thread1, NULL, timer3_thread, (void *) time_value);
	return;
}

void config_timer1_us(unsigned int time, unsigned int priority) {
	return;
}

void config_timer2_us(unsigned int time, unsigned int priority) {
	return;
}

void config_timer3_us(unsigned int time, unsigned int priority) {
	return;
}

void config_timer4_us(unsigned int time, unsigned int priority) {
	return;
}

void config_timer5_us(unsigned int time, unsigned int priority) {
	return;
}

void config_timer6_us(unsigned int time, unsigned int priority) {
	return;
}

void config_timer7_us(unsigned int time, unsigned int priority) {
	return;
}

void config_timer8_us(unsigned int time, unsigned int priority) {
	return;
}

void config_timer9_us(unsigned int time, unsigned int priority) {
	return;
}

void enable_timer1(void) {
	return;
}

void enable_timer2(void) {
	return;
}

void enable_timer3(void) {
	return;
}

void enable_timer4(void) {
	return;
}

void enable_timer5(void) {
	return;
}

void enable_timer6(void) {
	return;
}

void enable_timer7(void) {
	return;
}

void enable_timer8(void) {
	return;
}

void enable_timer9(void) {
	return;
}

void disable_timer1(void) {
	return;
}

void disable_timer2(void) {
	return;
}

void disable_timer3(void) {
	return;
}

void disable_timer4(void) {
	return;
}

void disable_timer5(void) {
	return;
}

void disable_timer6(void) {
	return;
}

void disable_timer7(void) {
	return;
}

void disable_timer8(void) {
	return;
}

void disable_timer9(void) {
	return;
}

void reset_timer1(void) {
	return;
}

void reset_timer2(void) {
	return;
}

void reset_timer3(void) {
	return;
}

void reset_timer4(void) {
	return;
}

void reset_timer5(void) {
	return;
}

void reset_timer6(void) {
	return;
}

void reset_timer7(void) {
	return;
}

void reset_timer8(void) {
	return;
}

void reset_timer9(void) {
	return;
}

void __attribute__((weak)) timer1_callback(void) {
	return;
}

void __attribute__((weak)) timer2_callback(void) {
	return;
}

void __attribute__((weak)) timer3_callback(void) {
	return;
}

void __attribute__((weak)) timer4_callback(void) {
	return;
}

void __attribute__((weak)) timer5_callback(void) {
	return;
}

void __attribute__((weak)) timer6_callback(void) {
	return;
}

void __attribute__((weak)) timer7_callback(void) {
	return;
}

void __attribute__((weak)) timer8_callback(void) {
	return;
}

void __attribute__((weak)) timer9_callback(void) {
	return;
}
