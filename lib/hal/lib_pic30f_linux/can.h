#ifndef __CAN_H__
#define __CAN_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#ifndef _CANDATA
#define _CANDATA

typedef struct {
	union {
		struct {
			uint16_t dev_id:5; // Least significant
			uint16_t msg_id:6; // Most significant
		};
		uint16_t sid;
	};
	uint8_t dlc:4;
	uint16_t data[4];
} CANdata;
#endif

typedef struct _CANconfig {
	unsigned long baudrate;
	unsigned sync_jump_width:2;
	unsigned prop_seg:3;
	unsigned phase1_seg:3;
	unsigned phase2_seg:3;
	unsigned triple_sampling:1;
	unsigned fifo_size:3;
	unsigned fifo_start:5;

	unsigned interrupt_priority;
	unsigned rx_interrupt_priority;

	unsigned number_masks;
	unsigned filter_masks[3];

	unsigned number_filters;
	unsigned filter_sids[16];
	unsigned filter_mask_source[16];
} CANconfig;

typedef enum {
	SUCCESS      = 0,
	ENULL        = -1,
	ECONFIG      = -2,
	ETIMING      = -3,
	ESENDFAILURE = -4,
} CANerror;

void send_can1_buffer();
void write_to_can1_buffer(CANdata CANmessage, bool priority);

void send_can2_buffer();
void write_to_can2_buffer(CANdata CANmessage, bool priority);

CANerror send_can2(CANdata msg);
CANerror send_can1(CANdata msg);

CANdata *pop_can1();
CANdata *pop_can2();

bool can1_rx_empty();
bool can2_rx_empty();

CANerror config_can1();
CANerror config_can2();


uint16_t can1_tx_available();
uint16_t can2_tx_available();
#endif
