#ifndef FAKE
#include <xc.h>
#endif

#include <stdint.h>
#include <stdlib.h>
#include "version.h"

/**********************************************************************
 * Name:    get_version
 * Args:    -
 * Return:  -
 * Desc:    Obtains your current code version from your commit log.
 **********************************************************************/

VERSIONerror get_version(uint64_t version, uint16_t *data){
	// version is defined in the Makefile as VERSION.
    
	if (data == NULL)
		return VEMPTY;

    data[3] = (version & 0xF) + ((version) & 0xF0);
	data[2] = ((version >> 8) & 0xF) + ((version >> 8 & 0xF0));
	data[1] = ((version >> 16) & 0xF) + ((version >> 16) & 0xF0);
	data[0] = ((version >> 24) & 0xF) + ((version >> 24) & 0xF0);

	return VSUCCESS;
}
