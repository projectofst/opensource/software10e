#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <config.h>
#include "shared/fifo/fifo.h"
#include "can.h"
#include "json.h"

#define PORT1 57005
#define PORT2 48879
#define MAXLINE 1024

int sock_can1;
int sock_can2;

struct sockaddr_can addr;
//struct sockaddr_in servaddr_2;
struct ifreq ifr;

Fifo _fifo1;
Fifo *fifo1 = &_fifo1;
Fifo _fifo2;
Fifo *fifo2 = &_fifo2;

#define CAN1_SIZE 64
#define CAN2_SIZE 64

CANdata msgs1[CAN1_SIZE];
CANdata msgs2[CAN2_SIZE];

//CANdata msgs1_send[CAN1_SIZE];
//CANdata msgs2_send[CAN2_SIZE];

void send_can1_buffer() {
  return;
}

void write_to_can1_buffer(CANdata msg, bool priority) {
  struct can_frame frame;
  frame.can_id = msg.sid;
	frame.can_dlc = msg.dlc;
  memcpy(frame.data, msg.data, 8);

	if (write(sock_can1, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
		perror("Write");
		return;
	}
  return;
}

void write_to_can2_buffer(CANdata msg, bool priority) {
  struct can_frame frame;
  frame.can_id = msg.sid;
	frame.can_dlc = msg.dlc;
  memcpy(frame.data, msg.data, 8);

	if (write(sock_can2, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
		perror("Write");
		return;
	}
  return;
}

void send_can2_buffer() {
  return;
}

CANerror send_can2(CANdata msg) {
  write_to_can2_buffer(msg, 0);
  return 0;
}

CANerror send_can1(CANdata msg) {
  write_to_can1_buffer(msg, 0);
  return 0;
}

CANdata *pop_can1() { return pop_fifo(fifo1); }

CANdata *pop_can2() { return pop_fifo(fifo2); }

bool can1_rx_empty() { return fifo_empty(fifo1); }

bool can2_rx_empty() { return fifo_empty(fifo2); }

bool __attribute__((weak)) filter_can1(unsigned int sid) { return 1; }

bool __attribute__((weak)) filter_can2(unsigned int sid) { return 1; }

uint16_t can1_tx_available() { return 1; }

uint16_t can2_tx_available() { return 1; }

void *recv_can1(void *arg) {
  	struct can_frame frame;
    while (1) {
	    int nbytes = read(sock_can1, &frame, sizeof(struct can_frame));
 	    if (nbytes < 0) {
		    perror("Read");
		    return NULL;
	    }

        if (filter_can1(frame.can_id)) {
            CANdata msg = {
                .sid = frame.can_id,
                .dlc = frame.can_dlc,
                .data = {0,0,0,0},
            };

            memcpy(msg.data, frame.data, 8);
            append_fifo(fifo1, msg);
        }
    }
}

CANerror config_can1() {

  if ((sock_can1 = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
      perror("Socket");
      return 1;
  }

  strcpy(ifr.ifr_name, CAN1_SOCKET_CAN );
	ioctl(sock_can1, SIOCGIFINDEX, &ifr);
	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(sock_can1, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}

  fifo_init(fifo1, msgs1, CAN1_SIZE);
  pthread_t thread1;
  pthread_create(&thread1, NULL, recv_can1, NULL);

  return 0;
}

void *recv_can2(void *arg) {

	struct can_frame frame;
    while (1) {
	    int nbytes = read(sock_can2, &frame, sizeof(struct can_frame));

 	    if (nbytes < 0) {
		    perror("Read");
		    return NULL;
	    }
        if (!filter_can2(frame.can_id)){ //carefull, edited to test only receiving messages from its own id (linux)
            CANdata msg = {
                .sid = frame.can_id,
                .dlc = frame.can_dlc,
                .data = {0,0,0,0},
            };

            memcpy(msg.data, frame.data, 8);
            append_fifo(fifo2, msg);
        }
    }
}

CANerror config_can2() {
  if ((sock_can2 = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
      perror("Socket");
      return 1;
  }

	strcpy(ifr.ifr_name, CAN2_SOCKET_CAN );
	ioctl(sock_can2, SIOCGIFINDEX, &ifr);
	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(sock_can2, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}

  fifo_init(fifo2, msgs2, CAN1_SIZE);
  pthread_t thread1;
  pthread_create(&thread1, NULL, recv_can2, NULL);

  return 0;
}
