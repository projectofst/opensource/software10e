#include <xc.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "vio.h"
#include "lib_pic30f/io.h"
#include "adc.h"
#include "timing.h"
#include "sts_queue/sts_queue.h"


typedef struct _vio_adc_channel_t {
	const char *name;
	uint16_t value;
} vio_adc_channel_t;

vio_adc_channel_t *adc_channels;

vio_adc_channel_t *vio_adc_find_channel(vio_adc_channel_t *adc_channels, 
		const char *name) {
	for (int i=0; adc_channels[i].name != NULL; i++) {
		if (strcmp(adc_channels[i].name, name) == 0) {
			return adc_channels + i;
		}
	}

	return NULL;
}

void *protobuf_handler(void *_conn) {
	Connection conn = *((Connection *) _conn);
	
	char *buffer = NULL;
	Vio *vio_aux = NULL;
	Vio *vio = NULL;
	
	
	printf("Start protobuf handler\n");
	while (true) {
		if((vio_aux = StsQueue.pop(conn.adc)) != NULL) {
			vio = vio_aux;
		}
		if((vio_aux = StsQueue.pop(conn.input)) != NULL) {
			vio = vio_aux;
  		}
		//printf("Received adc \n");
		if ((vio != NULL)&&(vio->type == ADC)) {
			//printf("adc_id: %s\n", vio->id);
			vio_adc_channel_t *aux = vio_adc_find_channel(adc_channels, vio->id);
			if (aux == NULL) {
				continue;
			}
			aux->value = vio->value;	
			adc_callback();	
		}

		if ((vio != NULL)&&(vio->type == INPUT)){
			set_port_fake( &(vio->group), vio->pin, vio->value);
		} 
		
	
	}

} 	

/**********************************************************************
 * Name:    config_adc
 * Args:    ADC_parameters filled with the configuracions for the adc module
 * Return:  -
 * Desc:    Configures adc module
 **********************************************************************/
void config_adc (ADC_parameters parameter){
	vio_adc_channel_t aux_adc_channels[] = VIO_ADC1_CONFIG;
	int size = 0;
	for (size=0; aux_adc_channels[size].name != NULL; size++) ;


	adc_channels = (vio_adc_channel_t *) malloc(sizeof(vio_adc_channel_t) * size);
	memcpy(adc_channels, aux_adc_channels, size*sizeof(vio_adc_channel_t));
	
	Connection *conn = (Connection *) malloc(sizeof(Connection));
	*conn = vio_open_conn(VIO_HOST, VIO_PORT);
	pthread_t thread;
	pthread_create(&thread, NULL, protobuf_handler, conn);
	return;
}



void __attribute__((weak)) adc_callback(void) {
	return;
}

/**********************************************************************
 * Assign End of ADC conversion interruption
 **********************************************************************/

/**********************************************************************
 * Name:    get_adc_value
 * Args:    the sample you want to retrive (the firs one is sample_number 0,
 *          the second one is sample_number one...)
 * Return:  the value read by the adc
 * Desc:    gets the adc value from the buffer and returns it
 **********************************************************************/

unsigned int get_adc_value(unsigned int sample_number){
    return 0;
}

/**********************************************************************
 * Name:    turn_on_adc
 * Args:    -
 * Return:  -
 * Desc:    turns on the adc module
 **********************************************************************/
void turn_on_adc(){
    return;
}

/**********************************************************************
 * Name:    turn_off_adc
 * Args:    -
 * Return:  -
 * Desc:    turns off the adc module
 **********************************************************************/
void turn_off_adc(){
    return;
}

/**********************************************************************
 * Name:    start_samp
 * Args:    -
 * Return:  -
 * Desc:    starts a new sample
 * IMPORTANTE: only use this fuction if your sampling_type is set to MANUAL
 **********************************************************************/
void start_samp(){
    return;
}

/**********************************************************************
 * Name:    start_conversion
 * Args:    -
 * Return:  -
 * Desc:    ends sampling and starts conversion
 * IMPORTANTE: only use this fuction if your conversion_trigger is set to SAMP
 **********************************************************************/
void start_conversion(){
    return;
}

/**********************************************************************
 * Name:    wait_for_done
 * Args:    -
 * Return:  -
 * Desc:    waits until conversion is done
 **********************************************************************/
void wait_for_done(){
    return;
}

/**********************************************************************
 * Name:    set_default_parameters
 * Args:    ADC_parameters * (empty struct to be filled)
 * Return:  -
 * Desc:    fills a ADC_parameters struct with default configuration
 **********************************************************************/
void set_default_parameters_adc(ADC_parameters *parameters){
    return;

}

/**********************************************************************
 * Name:    select_input_pin
 * Args:    number of the pin you want to scan
 * Return:  -
 * Desc:    defines what pin to scan next
 **********************************************************************/
void select_input_pin(unsigned int pin){
    return;
}

uint16_t get_adcbuf0() {
	return adc_channels[0].value;		
}


uint16_t get_adcbuf1() {
	return adc_channels[1].value;		
}


uint16_t get_adcbuf2() {
	return adc_channels[2].value;		
}


uint16_t get_adcbuf3() {
	return adc_channels[3].value;		
}


uint16_t get_adcbuf4() {
	return adc_channels[4].value;		
}


uint16_t get_adcbuf5() {
	return adc_channels[5].value;		
}
