#include <stdio.h>
#include <stdlib.h>

#include "eeprom.h"


#define FILENAME "flash.txt"
#define ASCII_BYTES 6

//IMPORTANT: all variables representing the information on eeprom must be declares in this file!
//IMPORTANT: all variables must be declared has vectors, even if only one variable is needed!
//IMPORTANT: the value on _EEDATA must me 2x the number of variables in the declared vector!
//IMPORTANT: the body of the vector must be: unsigned int _EEDATA(2xsize of the vector) name_of_vector[] = {value of 1, value of 2, ... , value of N}
//IMPORTANT: every vector must have a corresponding address in the eeprom
//IMPORTANT: to do so initialize an address using _prog_addressT name_of_addr
//IMPORTANT: declare the address as extern in the corresponding .h file
//IMPORTANT: to finish, a bind between the vector and the corresponding address must be made
//IMPORTANT: to do so use _init_prog_address(name_of_addr, name_of_vector)
//IMPORTANT: the steps above must be done for every vector declared


void EEPROM_init_addr ()
{

    //For every vector declared copy the above, changing the arguments for the corresponding vector and name

    return;
}

//In the following functions eeprom_address refers to the address binded with the vector

void eeprom_store_value (_prog_addressT eeprom_address, volatile unsigned int value)
{
    return;
}

unsigned int eeprom_read_word (_prog_addressT eeprom_address)
{
    unsigned int temp[1];

    return temp[0];
}

void eeprom_read_row (_prog_addressT EE_address, uint16_t EE_data_vector[]){


    return;
}
//There´s the possibility to copy an entire row using: _memcpy_p2d16(vector containing the variables, address initialized for the vector, _EE_ROW);

void eeprom_erase_word (_prog_addressT eeprom_address)
{

    return;
}

void eeprom_erase_row (_prog_addressT eeprom_address)
{

    return;
}

void eeprom_write_word (_prog_addressT eeprom_address, volatile unsigned int value)
{

    return;
}

void eeprom_write_row (_prog_addressT EE_address, uint16_t data_vector[]){

	FILE *f;
	int i;

	if((f = fopen(FILENAME,"w+")) == NULL){
		printf("Erro opening flash");
		return;
	}

	fseek(f,0,0);

	for(i = 0 ; i < sizeof(data_vector)/sizeof(uint16_t) ; i++)
		fprintf(f,"%5d\n", data_vector[i]);

	
	fclose(f);
	return;
}

uint16_t read_eeprom(uint16_t *buf, uint16_t len) {
	FILE *f;
	int i, ret;

	if((f = fopen(FILENAME,"w+")) == NULL){
		printf("Erro opening flash");
		return 1;
	}

	fseek(f,0,0);

	for(i = 0 ; i < len ; i++)
		ret=fscanf(f,"%5hd\n", &buf[i]);

	return fclose(f);
}
