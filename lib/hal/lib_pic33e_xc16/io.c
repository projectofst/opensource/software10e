#include "io.h"
#include <xc.h>

void pinB_set(uint8_t n, uint8_t type)
{
    ANSELB &= (0xFFFF ^ (1 << n)); //clear bit at position
    ANSELB |= type << n; //assign bit with value from type

    TRISB |= 1 << n; //forcing bit to input (should be input default)
    return;
}
void pinC_set(uint8_t n, uint8_t type)
{
    ANSELC &= (0xFFFF ^ (1 << n)); //clear bit at position
    ANSELC |= type << n; //assign bit with value from type

    TRISC |= 1 << n; //forcing bit to input (should be input default)
    return;
}
void pinD_set(uint8_t n, uint8_t type)
{
    ANSELD &= (0xFFFF ^ (1 << n)); //clear bit at position
    ANSELD |= type << n; //assign bit with value from type

    TRISD |= 1 << n; //forcing bit to input (should be input default)
    return;
}
void pinE_set(uint8_t n, uint8_t type)
{
    ANSELE &= (0xFFFF ^ (1 << n)); //clear bit at position
    ANSELE |= type << n; //assign bit with value from type

    TRISE |= 1 << n; //forcing bit to input (should be input default)
    return;
}
void pinG_set(uint8_t n, uint8_t type)
{
    ANSELG &= (0xFFFF ^ (1 << n)); //clear bit at position
    ANSELG |= type << n; //assign bit with value from type

    TRISG |= 1 << n; //forcing bit to input (should be input default)
    return;
}

void pinC_high(uint8_t n)
{
    TRISC &= (0xFFFF ^ (1 << n));
    LATC |= 1 << n;
    return;
}
void pinB_high(uint8_t n)
{
    TRISB &= (0xFFFF ^ (1 << n));
    LATB |= 1 << n;
    return;
}
void pinD_high(uint8_t n)
{
    TRISD &= (0xFFFF ^ (1 << n));
    LATD |= 1 << n;
    return;
}
void pinE_high(uint8_t n)
{
    TRISE &= (0xFFFF ^ (1 << n));
    LATE |= 1 << n;
    return;
}
void pinF_high(uint8_t n)
{
    TRISF &= (0xFFFF ^ (1 << n));
    LATF |= 1 << n;
    return;
}
void pinG_high(uint8_t n)
{
    TRISG &= (0xFFFF ^ (1 << n));
    LATG |= 1 << n;
    return;
}

void pinB_low(uint8_t n)
{
    TRISB &= (0xFFFF ^ (1 << n));
    LATB &= (0xFFFF ^ (1 << n));
    return;
}
void pinC_low(uint8_t n)
{
    TRISC &= (0xFFFF ^ (1 << n));
    LATC &= (0xFFFF ^ (1 << n));
    return;
}
void pinD_low(uint8_t n)
{

    TRISD &= (0xFFFF ^ (1 << n));
    LATD &= (0xFFFF ^ (1 << n));
    return;
}
void pinE_low(uint8_t n)
{
    TRISE &= (0xFFFF ^ (1 << n));
    LATE &= (0xFFFF ^ (1 << n));
    return;
}
void pinF_low(uint8_t n)
{
    TRISF &= (0xFFFF ^ (1 << n));
    LATF &= (0xFFFF ^ (1 << n));
    return;
}
void pinG_low(uint8_t n)
{
    TRISG &= (0xFFFF ^ (1 << n));
    LATG &= (0xFFFF ^ (1 << n));
    return;
}
