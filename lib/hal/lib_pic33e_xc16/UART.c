#ifndef NOUART

#include "UART.h"

unsigned int UART1_BUFFER[UART_BUFFER_LENGHT];
unsigned int UART2_BUFFER[UART_BUFFER_LENGHT];

unsigned int uart1_read = 0;
unsigned int uart1_write = 0;
unsigned int uart2_read = 0;
unsigned int uart2_write = 0;

UART_errors uart1_errors_status;
UART_errors uart2_errors_status;

/**********************************************************************
 * Name:    baudrate_generator
 * Args:    *parameters (refer to the .h for more information on the data)
 * Return:  -1 if you get a error of more then 1% with your desired baudrate/FOSC
 *          0 if the parameters struct was nicely filled
 * Desc:    determines how to configure the BRGH bit and the UxBRG acording t
 *          o the wanted baudrate
 **********************************************************************/
int baudrate_generator(UART_parameters* parameters)
{
    unsigned int BRG_16;
    unsigned int BRG_4;
    unsigned int BR_16;
    unsigned int BR_4;
    float error_16;
    float error_4;

    //calculate UxBRG  -  Baud Rate Divisor
    BRG_16 = ((Fp / parameters->BaudRate) / 16) - 1;
    BRG_4 = ((Fp / parameters->BaudRate) / 4) - 1;

    //calculate baudrate that you get by using the previous BRG
    BR_16 = (Fp / (BRG_16 + 1)) / 16;
    BR_4 = (Fp / (BRG_4 + 1)) / 4;

    //calculate the error that you get by using the previous baudrate
    error_16 = ((BR_16 - parameters->BaudRate) / parameters->BaudRate) * 100;
    error_4 = ((BR_4 - parameters->BaudRate) / parameters->BaudRate) * 100;

    if ((error_16 < 1) || (error_4 < 1)) {
        if (error_16 < error_4) {
            parameters->UxBRG = BRG_16;
            parameters->BRGH = 0;
            return 0;
        } else {
            parameters->UxBRG = BRG_4;
            parameters->BRGH = 1;
            return 0;
        }
    }
    return -1;
}

/**********************************************************************
 * Name:    config_uart1
 * Args:    parameters (refer to the .h for more information on the data)
 * Return:  -
 * Desc:    uses the parameters struct to configurate the UART1 module
 **********************************************************************/
int config_uart1(UART_parameters* parameters)
{

    if (baudrate_generator(parameters) == -1) {
        return -1;
    }
    U1MODEbits.UARTEN = 1; /*UART enable                                                                   */
    U1MODEbits.USIDL = 0; /*continue operation in idle                                                    */
    U1MODEbits.IREN = 0; /*IrDA disabled                                                                 */
    U1MODEbits.RTSMD = 0; /*Simplex mode                                                                  */
    U1MODEbits.UEN0 = 0; /*UxTX and UxRX are enabled and UxCTS and UxRTS are controled by port latches   */
    U1MODEbits.UEN1 = 0;
    U1MODEbits.WAKE = 1; /*wake from sleep mode when star bit is detected is enabled                     */
    U1MODEbits.LPBACK = 0; /*Loopback mode is disabled                                                     */
    U1MODEbits.ABAUD = 0; /*baud rate measurement is disabled                                             */
    U1MODEbits.URXINV = !(parameters->UxRX_idle_state); /*UxRX idle state is '1'                                                        */
    U1MODEbits.BRGH = parameters->BRGH; /*BRG generates 16/4 clocks per bit  second                                       */
    U1MODEbits.PDSEL0 = (parameters->pdsel & 0b001); /*8 bit data, no parity                                                         */
    U1MODEbits.PDSEL1 = ((parameters->pdsel & 0b010) >> 1);
    U1MODEbits.STSEL = (parameters->number_stop_bits - 1); /*1 stop bit                                                                    */

    U1STAbits.UTXISEL0 = 0; /*interrupt is generated when any charater is tranferred                        */ //CHANGE THIS ONE WHEN CONFIGURATING UART TRANSMISSION
    U1STAbits.UTXISEL1 = 0;
    U1STAbits.UTXINV = !(parameters->UxTX_idle_state); /*UxTX idle state 0 for 1 and 1 for 0                                           */
    U1STAbits.UTXEN = (!parameters->transmit); /*transmiter is enabled, UxTX is controled by UARTx                             */
    U1STAbits.UTXBRK = 0; /*Sync Break transmission is disabled or complete                               */ //CHANGE THIS ONE WHEN CONFIGURATING UART TRANSMISSION

    /*receive configiuration*/
    U1STAbits.ADDEN = 0; /*bit has no effect - 9-bit mode not selected                                   */ //NO IDEA WHAT THIS DOES

    /*defining baudrate*/
    U1BRG = parameters->UxBRG;

    /*receive interrupt configuration*/
    U1STAbits.URXISEL = 0; /*interrupt flag bit is set when  caracter is received                          */
    IFS0bits.U1RXIF = 0; /*clear interrupt flag                                                          */
    IEC0bits.U1RXIE = 1; /*interrupt request enable                                                      */
    IPC2bits.U1RXIP = 5; /*interrupt priority to 5                                                       */

    /*receive error interrupt configuration*/
    IFS4bits.U1EIF = 0; /*clear interrupt flag                                                          */
    IEC4bits.U1EIE = 1; /*interrupt enable                                                              */
    IPC16bits.U1EIP = 7; /*interrupt priority - KEEP IT HIGH                                             */

    return 0;
}

/**********************************************************************
 * Name:    config_uart2
 * Args:    parameters (refer to the .h for more information on the data)
 * Return:  -
 * Desc:    uses the parameters struct to configurate the UART2 module
 **********************************************************************/
int config_uart2(UART_parameters* parameters)
{

    if (baudrate_generator(parameters) == -1) {
        return -1;
    }
    U2MODEbits.UARTEN = 1; /*UART enable                                                                   */
    U2MODEbits.USIDL = 0; /*continue operation in idle                                                    */
    U2MODEbits.IREN = 0; /*IrDA disabled                                                                 */
    U2MODEbits.RTSMD = 0; /*Simplex mode                                                                  */
    U2MODEbits.UEN0 = 0; /*UxTX and UxRX are enabled and UxCTS and UxRTS are controled by port latches   */
    U2MODEbits.UEN1 = 0;
    U2MODEbits.WAKE = 1; /*wake from sleep mode when star bit is detected is enabled                     */
    U2MODEbits.LPBACK = 0; /*Loopback mode is disabled                                                     */
    U2MODEbits.ABAUD = 0; /*baud rate measurement is disabled                                             */
    U2MODEbits.URXINV = !(parameters->UxRX_idle_state); /*UxRX idle state is '1'                                                        */
    U2MODEbits.BRGH = parameters->BRGH; /*BRG generates 16/4 clocks per bit  second                                       */
    U2MODEbits.PDSEL0 = (parameters->pdsel & 0b001); /*8 bit data, no parity                                                         */
    U2MODEbits.PDSEL1 = ((parameters->pdsel & 0b010) >> 1);
    U2MODEbits.STSEL = (parameters->number_stop_bits - 1); /*1 stop bit                                                                    */

    U2STAbits.UTXISEL0 = 0; /*interrupt is generated when any charater is tranferred                        */ //CHANGE THIS ONE WHEN CONFIGURATING UART TRANSMISSION
    U2STAbits.UTXISEL1 = 0;
    U2STAbits.UTXINV = !(parameters->UxTX_idle_state); /*UxTX idle state 0 for 1 and 1 for 0                                           */
    U2STAbits.UTXEN = (!parameters->transmit); /*transmiter is enabled, UxTX is controled by UARTx                             */
    U2STAbits.UTXBRK = 0; /*Sync Break transmission is disabled or complete                               */ //CHANGE THIS ONE WHEN CONFIGURATING UART TRANSMISSION

    /*receive configiuration*/
    U2STAbits.ADDEN = 0; /*bit has no effect - 9-bit mode not selected                                   */ //NO IDEA WHAT THIS DOES

    /*defining baudrate*/
    U2BRG = parameters->UxBRG;

    /*receive interrupt configuration*/
    U2STAbits.URXISEL = 0; /*interrupt flag bit is set when  caracter is received                          */
    IFS1bits.U2RXIF = 0; /*clear interrupt flag                                                          */
    IEC1bits.U2RXIE = 1; /*interrupt request enable                                                      */
    IPC7bits.U2RXIP = 5; /*interrupt priority to 5                                                       */

    /*receive error interrupt configuration*/
    IFS4bits.U2EIF = 0; /*clear interrupt flag                                                          */
    IEC4bits.U2EIE = 1; /*interrupt enable                                                              */
    IPC16bits.U2EIP = 7; /*interrupt priority - KEEP IT HIGH                                             */

    return 0;
}

/**********************************************************************
 * Name:    enable_uart1
 * Args:    -
 * Return:  -
 * Desc:    enables the UART1 module
 **********************************************************************/
void enable_uart1(void)
{
    U1MODEbits.UARTEN = 1; /*UART1 enable*/
    return;
}

/**********************************************************************
 * Name:    enable_uart2
 * Args:    -
 * Return:  -
 * Desc:    enables the UART2 module
 **********************************************************************/
void enable_uart2(void)
{
    U2MODEbits.UARTEN = 1; /*UART2 enable*/
    return;
}

/**********************************************************************
 * Name:    disable_uart1
 * Args:    -
 * Return:  -
 * Desc:    disables the UART1 module
 **********************************************************************/
void disable_uart1(void)
{
    U1MODEbits.UARTEN = 0; /*UART1 disable*/
    return;
}

/**********************************************************************
 * Name:    disable_uart2
 * Args:    -
 * Return:  -
 * Desc:    disables the UART2 module
 **********************************************************************/
void disable_uart2(void)
{
    U2MODEbits.UARTEN = 0; /*UART2 disable*/
    return;
}

/**********************************************************************
 * Name:    receive_uart1_buffer_empty
 * Args:    -
 * Return:  bool (TRUE - buffer is empty)
 * Desc:    checks if the receive buffer is empty. Use this fuction before
 *          disabling the module if you dont want to lose any characters
 **********************************************************************/
bool receive_uart1_buffer_empty(void)
{
    if (U1STAbits.URXDA == 1) {
        return false;
    }
    return true;
}

/**********************************************************************
 * Name:    receive_uart2_buffer_empty
 * Args:    -
 * Return:  bool (TRUE - buffer is empty)
 * Desc:    checks if the receive buffer is empty. Use this fuction before
 *          disabling the module if you dont want to lose any characters
 **********************************************************************/
bool receive_uart2_buffer_empty(void)
{
    if (U2STAbits.URXDA == 1) {
        return false;
    }
    return true;
}

/**********************************************************************
 * Name:    get_FIFO_data
 * Args:    uart_number - refers to the what uart you are using (uart1/uart2)
 * Return:  char - the first vharacter in the FIFO
 * Desc:    gets the first caracter from the FIFO
 **********************************************************************/
char get_FIFO_data(unsigned int uart_number)
{
    char aux;
    switch (uart_number) {
    case 1:
        aux = U1RXREG;
        return aux;
        break;
    case 2:
        aux = U2RXREG;
        return aux;
        break;
    }
    return 0;
}

/**********************************************************************
 * Name:    U1RX receive interrupt
 * Args:    -
 * Return:  -
 * Desc:    call upon UART1 receiving (interrupts every char received)
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _U1RXInterrupt(void)
{
    //check for parity errors
    if (U1STAbits.PERR) {
        uart1_errors_status.parity = 1;
    }
    //check for framing error - stop bits
    if (U1STAbits.FERR) {
        uart1_errors_status.framing = 1;
    }

    //get message from the fifo and put it in the UART1_BUFFER
    UART1_BUFFER[uart1_write] = get_FIFO_data(1);
    uart1_write = (uart1_write + 1) % UART_BUFFER_LENGHT;

    IFS0bits.U1RXIF = 0; /*clear interrupt flag                                                          */
}

/**********************************************************************
 * Name:    U2RX receive interrupt
 * Args:    -
 * Return:  -
 * Desc:    call upon UART2 receiving (interrupts every char received)
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _U2RXInterrupt(void)
{
    //check for parity errors
    if (U2STAbits.PERR) {
        uart2_errors_status.parity = 1;
    }
    //check for framing error - stop bits
    if (U2STAbits.FERR) {
        uart2_errors_status.framing = 1;
    }
    //get message from the fifo and put it in the UART2_BUFFER
    UART2_BUFFER[uart2_write] = get_FIFO_data(2);
    uart2_write = (uart2_write + 1) % UART_BUFFER_LENGHT;

    IFS1bits.U2RXIF = 0; /*clear interrupt flag                                                           */
}

/**********************************************************************
 * Name:    U1RX receive error interrupt
 * Args:    -
 * Return:  -
 * Desc:    called upon uart1 receiving error
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _U1ErrInterrupt(void)
{
    int i;
    //check for overflow error
    if (U1STAbits.OERR == 1) {
        uart1_errors_status.overflow = 1;
        //in case of overflow get every message from the FIFO before continuing
        for (i = 0; i < 4; i++) {
            UART1_BUFFER[uart1_write] = get_FIFO_data(1);
            uart1_write = (uart1_write + 1) % UART_BUFFER_LENGHT;
        }
        //clear error - THIS NEEDS TO BE DONE MANUALLY
        U1STAbits.OERR = 0;
    }
    IFS4bits.U1EIF = 0; /*clear interrupt flag                                                          */
}

/**********************************************************************
 * Name:    U2RX receive error interrupt
 * Args:    -
 * Return:  -
 * Desc:    called upon uart1 receiving error
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _U2ErrInterrupt(void)
{
    int i;
    //check for overflow error
    if (U2STAbits.OERR == 1) {
        uart2_errors_status.overflow = 1;
        //in case of overflow get every message from the FIFO before continuing
        for (i = 0; i < 4; i++) {
            UART2_BUFFER[uart2_write] = get_FIFO_data(2);
            uart2_write = (uart2_write + 1) % UART_BUFFER_LENGHT;
        }
        //clear error - THIS NEEDS TO BE DONE MANUALLY
        U2STAbits.OERR = 0;
    }
    IFS4bits.U2EIF = 0; /*clear interrupt flag                                                           */
}

/**********************************************************************
 * Name:    get_uart1_errors
 * Args:    -
 * Return:  uart erros struct
 * Desc:    -
 **********************************************************************/
UART_errors get_uart1_errors(void)
{
    return uart1_errors_status;
}

/**********************************************************************
 * Name:    get_uart2_errors
 * Args:    -
 * Return:  uart erros struct
 * Desc:    -
 **********************************************************************/
UART_errors get_uart2_errors(void)
{
    return uart2_errors_status;
}

/**********************************************************************
 * Name:    pop_uart1
 * Args:    -
 * Return:  char (uart message from the UART1_BUFFER)
 * Desc:    -
 **********************************************************************/
char pop_uart1(void)
{
    unsigned int aux;
    aux = uart1_read;
    uart1_read = (uart1_read + 1) % UART_BUFFER_LENGHT;
    return UART1_BUFFER[aux];
}

/**********************************************************************
 * Name:    pop_uart2
 * Args:    -
 * Return:  char (uart message from the UART2_BUFFER)
 * Desc:    -
 **********************************************************************/
char pop_uart2(void)
{
    unsigned int aux;
    aux = uart2_read;
    uart2_read = (uart2_read + 1) % UART_BUFFER_LENGHT;
    return UART2_BUFFER[aux];
}

/**********************************************************************
 * Name:    uartRX1_empty
 * Args:    -
 * Return:  bool (TRUE if UART1_BUFFER is empty)
 * Desc:    checks if there is anything to read in the UART1_BUFFER
 **********************************************************************/
bool uartRX1_empty(void)
{
    if (uart1_read == uart1_write) {
        return true;
    } else {
        return false;
    }
}

/**********************************************************************
 * Name:    uartRX2_empty
 * Args:    -
 * Return:  bool (TRUE if UART2_BUFFER is empty)
 * Desc:    checks if there is anything to read in the UART2_BUFFER
 **********************************************************************/
bool uartRX2_empty(void)
{
    if (uart2_read == uart2_write) {
        return true;
    } else {
        return false;
    }
}

#endif
