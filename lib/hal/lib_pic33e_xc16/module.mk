MODULE_C_SOURCES:=SPI.c
MODULE_C_SOURCES+=UART.c
MODULE_C_SOURCES+=adc.c
MODULE_C_SOURCES+=can.c
MODULE_C_SOURCES+=external.c
MODULE_C_SOURCES+=flash.c
MODULE_C_SOURCES+=io.c
MODULE_C_SOURCES+=led.c
MODULE_C_SOURCES+=pin_assignment.c
MODULE_C_SOURCES+=priority.c
MODULE_C_SOURCES+=pwm.c
MODULE_C_SOURCES+=timer.c
MODULE_C_SOURCES+=timing.c
MODULE_C_SOURCES+=trap.c
MODULE_C_SOURCES+=usb_lib.c
MODULE_C_SOURCES+=utils.c
MODULE_C_SOURCES+=version.c
MODULE_C_SOURCES+=usb/app_device_cdc_basic.c
MODULE_C_SOURCES+=usb/system.c
MODULE_C_SOURCES+=usb/usb_descriptors.c
MODULE_C_SOURCES+=usb/usb_device.c
MODULE_C_SOURCES+=usb/usb_device_cdc.c
MODULE_C_SOURCES+=usb/usb_events.c
MODULE_C_SOURCES+=usb/usb_hal_dspic33e.c

MODULE_S_SOURCES:=flash_pic.s

PWD:=lib/lib_pic33e_xc16/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))
S_SOURCES += $(call abs_path,$(MODULE_S_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
