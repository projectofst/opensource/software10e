#ifndef NOTRAP

#include "trap.h"
#include <stdio.h>
#include <xc.h>

/*
 * The handler is defined as weak so it can be defined outside the library
 */
void __attribute__((weak)) trap_handler(TrapType type)
{
    switch (type) {
    case HARD_TRAP_OSCILATOR_FAIL:
        return;
    case HARD_TRAP_ADDRESS_ERROR:
        return;
    case HARD_TRAP_STACK_ERROR:
        return;
    case HARD_TRAP_MATH_ERROR:
        return;
    case CUSTOM_TRAP_PARSE_ERROR:
        return;
    }

    return;
}

void __attribute__((interrupt, no_auto_psv)) _OscillatorFail(void)
{

    trap_handler(HARD_TRAP_OSCILATOR_FAIL);

    INTCON1bits.OSCFAIL = 0; /*clear trap flag*/

    //RESET();
    return;
}

void __attribute__((interrupt, no_auto_psv)) _AddressError(void)
{

    trap_handler(HARD_TRAP_ADDRESS_ERROR);

    INTCON1bits.ADDRERR = 0; /*clear trap flag*/

    //RESET();
    return;
}

void __attribute__((interrupt, no_auto_psv)) _StackError(void)
{

    trap_handler(HARD_TRAP_STACK_ERROR);

    INTCON1bits.STKERR = 0; /*clear trap flag*/

    //RESET();
    return;
}

void __attribute__((interrupt, no_auto_psv)) _MathError(void)
{

    trap_handler(HARD_TRAP_MATH_ERROR);

    INTCON1bits.MATHERR = 0; /*clear trap flag*/

    //RESET();
    return;
}

#endif
