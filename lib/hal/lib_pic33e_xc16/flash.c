#include "flash.h"

static __prog__ uint8_t page[FLASH_ERASE_PAGE_SIZE_IN_PC_UNITS] __attribute__((space(prog), aligned(FLASH_ERASE_PAGE_SIZE_IN_PC_UNITS)));

uint8_t write_flag = 0;

uint16_t read_flash(uint16_t* buf, uint16_t offset, uint16_t len)
{

    uint32_t flash_address = FLASH_GetErasePageAddress((uint32_t)&page[0]);
    uint16_t i;

    for (i = offset; i < len + offset; i++) {
        buf[i - offset] = FLASH_ReadWord16(flash_address + 2 * i);
    }

    return 0;
}

uint16_t write_flash(uint16_t* buf, uint16_t offset, uint16_t len)
{

    uint32_t flash_address = FLASH_GetErasePageAddress((uint32_t)&page[0]);
    uint16_t aux[FLASH_WRITE_ROW_SIZE_IN_PC_UNITS];
    uint16_t i;

    if (write_flag == 0) {
        write_flag = 1;
        FLASH_Unlock(FLASH_UNLOCK_KEY);

        read_flash(aux, 0, FLASH_WRITE_ROW_SIZE_IN_PC_UNITS);

        if (FLASH_ErasePage(flash_address) == false) {
            return 1;
        }

        for (i = offset; i < len + offset; i++) {
            aux[i] = buf[i - offset];
        }

        FLASH_WriteRow16(flash_address, aux);

        FLASH_Lock();
        write_flag = 0;
    } else {
        return 1;
    }

    return 0;
}