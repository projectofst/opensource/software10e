#ifndef NOADC

#include "adc.h"
#include "utils.h"
#include <stdlib.h>
#include <xc.h>

static uint16_t size_pin_cycling_vector = 0;

void config_adc1(ADC_parameters parameter)
{

    AD1CON1bits.ADSIDL = ~parameter.idle;
    AD1CON1bits.AD12B = 1;
    AD1CON1bits.FORM = parameter.form;
    AD1CON1bits.SSRCG = 0;
    AD1CON1bits.SSRC = parameter.conversion_trigger;
    AD1CON1bits.ASAM = parameter.sampling_type;

    AD1CON2bits.VCFG = parameter.voltage_ref;
    AD1CON2bits.CSCNA = parameter.sample_pin_select_type;
    AD1CON2bits.SMPI = parameter.smpi;
    AD1CON2bits.BUFM = 0; // Always starts filling the buffer from the Start address
    AD1CON2bits.ALTS = 0; /* always use MUX A                              */

    AD1CON3bits.SAMC = parameter.auto_sample_time; // Auto-Sample Time (31 is MAX)
    AD1CON3bits.ADRC = 0;                          // Clock derived from system clock
    AD1CON3bits.ADCS = parameter.conversion_time;  // ADC Conversion Clock 256.TAD (255 is MAX)

    AD1CON4bits.ADDMAEN = 0;

    ANSELB = parameter.pin_select;  /*set pins as analog inputs                       */
    AD1CSSL = parameter.pin_select; /*select which pins to scan                       */

    AD1CON1bits.ADON = parameter.turn_on;

    /* interrupts */
    IFS0bits.AD1IF = 0;                                  /*clear interrupt flag                            */
    IEC0bits.AD1IE = (parameter.interrupt_priority > 0); /*enable interrupt if the priority is more then 0 */
    IPC3bits.AD1IP = parameter.interrupt_priority;
}

void __attribute__((weak)) adc_callback(void)
{
    return;
}

/**********************************************************************
 * Assign End of ADC conversion interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _AD1Interrupt(void)
{
    adc_callback();
    IFS0bits.AD1IF = 0; /*clear interrupt flag */
    return;
}

/**********************************************************************
 * Name:    get_adc_value
 * Args:    the sample you want to retrive (the firs one is sample_number 0,
 *          the second one is sample_number one...)
 * Return:  the value read by the adc
 * Desc:    gets the adc value from the buffer and returns it
 **********************************************************************/

unsigned int get_adc_value(unsigned int sample_number)
{
    if (sample_number > 15)
    {
        return -1;
    }
    else
    {
        return *(&ADC1BUF0 + sample_number);
    }
}

/**********************************************************************
 * Name:    turn_on_adc
 * Args:    -
 * Return:  -
 * Desc:    turns on the adc module
 **********************************************************************/
void turn_on_adc()
{
    AD1CON1bits.ADON = 1;
}

/**********************************************************************
 * Name:    turn_off_adc
 * Args:    -
 * Return:  -
 * Desc:    turns off the adc module
 **********************************************************************/
void turn_off_adc()
{
    AD1CON1bits.ADON = 0;
}

/**********************************************************************
 * Name:    start_samp
 * Args:    -
 * Return:  -
 * Desc:    starts a new sample
 * IMPORTANTE: only use this fuction if your sampling_type is set to MANUAL
 **********************************************************************/
void start_samp()
{
    AD1CON1bits.SAMP = 1;
}

/**********************************************************************
 * Name:    start_conversion
 * Args:    -
 * Return:  -
 * Desc:    ends sampling and starts conversion
 * IMPORTANTE: only use this fuction if your conversion_trigger is set to SAMP
 **********************************************************************/
void start_conversion()
{
    AD1CON1bits.SAMP = 0;
}

/**********************************************************************
 * Name:    wait_for_done
 * Args:    -
 * Return:  -
 * Desc:    waits until conversion is done
 **********************************************************************/
void wait_for_done()
{
    while (!AD1CON1bits.DONE)
        ;
    return;
}

/**********************************************************************
 * Name:    set_default_parameters
 * Args:    ADC_parameters * (empty struct to be filled)
 * Return:  -
 * Desc:    fills a ADC_parameters struct with default configuration
 **********************************************************************/
void set_default_parameters(ADC_parameters *parameters)
{

    parameters->idle = 1;                       /*works in idle mode                       */
    parameters->form = 0;                       /*form set to unsigned integer             */
    parameters->conversion_trigger = AUTO_CONV; /*auto conversion                          */
    parameters->sampling_type = AUTO;           /*auto sampling                            */
    parameters->voltage_ref = INTERNAL;         /*use internal voltage reference           */
    parameters->sample_pin_select_type = AUTO;  /*auto change between differrent inputs    */
    parameters->smpi = 0;                       /*1 sample per interrupt                   */
    parameters->pin_select = 1;                 /*scan pin AN0                             */
    parameters->interrupt_priority = 5;         /*interrupt priority set to 5              */
    parameters->turn_on = 1;                    /*turn the module on                       */
    return;
}

/**********************************************************************
 * Name:    select_input_pin
 * Args:    number of the pin you want to scan
 * Return:  -
 * Desc:    defines what pin to scan next
 **********************************************************************/
void select_input_pin(unsigned int pin)
{
    AD1CHS0bits.CH0SA = pin;
    return;
}

/**********************************************************************
 * Name:    config_pin_cycling
 * Args:    -16bit number where bit x is high if the corresponding ANx pin is
            being used. Example: you want to read AN0, AN2 and AN15.
            pin_vector = AN0|AN2|AN15 where AN0=2⁰, AN2=2² and AN15=2¹⁵;
            -The sampling frequency. 250kHz is the maximum value. The minimum
            frequency depends on the systems instruction frequency, for 16MHz
            you can do 1Hz. The frequency at which you refresh the ADC buffer
            is equal to the sampling frequency (the value you input) divided by
            the numbers of channels sampling.
            Example: 125000UL, Hz units and with unsigned long type;
            -The system instruction frequency;
            -The priority you desire for ADC interrupts;
            -ADC_parameters struct to be filled.
 * Return:  Error value.
 * Desc:    Fuction used to easily configure several pins for ADC reading:
            -Configures pins for sequential reading using only channel0.
            -Uses channel scanning to automatically go from one pin to the next
            one.
            -Reading is done by numerical order AN0>...>AN15;
            -The ADC buffer (filled) size = number os channels being read;
            -Receives a large range of frequency values [fmin,250kHz];
            -fmin  = floor(sys_clock[Hz]/(256*2¹⁶)) + 1;
            -Since the PRx buffer can only take whole numbers, if
            sys_clock/(frequency*prescaler) is not a whole number the real
            frequency is not precisely what you insert;
            -freal = sys_clocK/[floor(sys_clock/(frequency*ps))*ps] where
            ps=prescaler={1,8,64,256}.
 **********************************************************************/
Pin_Cycling_Error config_pin_cycling(uint16_t pin_vector, uint32_t frequency,
                                     uint32_t sys_clock, unsigned int priority, ADC_parameters *parameters)
{
    // VERIFY INPUTS////////////////////////////////////////////////////////////
    // Minimum acquisition frequency for a given FCY. FCY is defined in the
    // Makefile. For more information on FCY check timing library!
    uint32_t fmin = ((sys_clock * 596U) / (10000U)) + 1;

    if (parameters == NULL)
    {
        return POINTER_NULL_ADC;
    }
    if (sys_clock > 106 || 16 > sys_clock)
    {
        return WRONG_CLOCK_ADC;
    }

    if (frequency < fmin)
    {
        return TOO_SLOW_ADC;
    }
    if (frequency > 250000UL)
    {
        return TOO_FAST_ADC;
    }
    ///////////////////////////////////////////////////////////////////////////

    // Timer5 CONFIGURATION FOR ADC CLOCK SOURCE////////////////////////////////
    // minimum prescaler for a given FCY and frequency. (multiplied by 1000)
    uint32_t Prescaler = ((sys_clock * 15259) / (frequency));
    uint16_t period;

    TMR5 = 0x0000;
    if (Prescaler > 0U && Prescaler < 1000U)
    {
        T5CONbits.TCKPS = 0b00; // Prescaler=1
        period = ((sys_clock * 1000000UL) / (frequency)) - 1;
        PR5 = period;
    }
    else if (Prescaler >= 1000U && Prescaler < 8000U)
    {
        T5CONbits.TCKPS = 0b01; // Prescaler=8
        period = ((sys_clock * 125000UL) / (frequency)) - 1;
        PR5 = period;
    }
    else if (Prescaler >= 8000U && Prescaler < 64000U)
    {
        T5CONbits.TCKPS = 0b10; // Prescaler=64
        period = ((sys_clock * 15625UL) / (frequency)) - 1;
        PR5 = period;
    }
    else if (Prescaler >= 64000U && Prescaler < 256000UL)
    {
        T5CONbits.TCKPS = 0b11; // Prescaler=256
        period = ((sys_clock * 1000000UL) / (256 * frequency)) - 1;
        PR5 = period;
    }
    IFS1bits.T5IF = 0; // Clear Timer5 interrupt flag;
    IEC1bits.T5IE = 0; // Disable Timer5 interrupt;
    ///////////////////////////////////////////////////////////////////////////

    // ADC CONFIGURATION FOR 12 bit MODE USING CHANNEL SCANNING/////////////////
    uint16_t i = 0;
    uint16_t clock_factor = 140 * sys_clock / 1000;

    AD1CON1 = 0;
    AD1CON2 = 0;
    AD1CON3 = 0;
    AD1CON4 = 0;
    AD1CHS0 = 0;
    AD1CSSH = 0;
    AD1CSSL = 0;
    parameters->idle = 1;
    parameters->form = 0;
    parameters->conversion_trigger = TIMER5;
    parameters->sampling_type = AUTO;
    parameters->voltage_ref = INTERNAL;
    parameters->sample_pin_select_type = AUTO;
    // Get number of pins in pin_vector. Size is global and is used to set the
    // number of conversions per interrupt (SMPI).
    for (i = 0; i < 16; i++)
    {
        if (RD_BITFLD(pin_vector, i) != 1)
        {
            size_pin_cycling_vector++;
        }
    }
    parameters->smpi = size_pin_cycling_vector - 1;
    // clock_factor multiples by the system clock period. Calculated for 140ns
    // ADC clock (Tad) but can be higher due to rounding down:
    parameters->conversion_time = clock_factor;
    parameters->pin_select = pin_vector;
    parameters->interrupt_priority = priority;
    parameters->turn_on = 1;
    T5CONbits.TON = 1; // Turn on Timer 5
    ///////////////////////////////////////////////////////////////////////////

    return SUCESS_ADC;
}

/**********************************************************************
 * Name:    get_pin_cycling_values
 * Args:    -The array adress to where you want to save the values;
            -The position of the ADC buffer where you want to start
            reading. Example: you sample 8 different channels and want to read
            only from the 6th to the 8th, your starting position is 6-1 (since
            the buffer starts in position 0);
            -How many values, startin from "position" do you want to read.
 * Return:  Error value.
 * Desc:    Gets the ADC values from the buffer and returns them. Verifies if
            your inputs are within the buffer's size.
 **********************************************************************/
uint16_t get_pin_cycling_values(uint16_t *value, uint16_t position,
                                uint16_t length)
{

    uint16_t adc_aquisitions[16];
    int i;
    int j;
    volatile uint16_t *ptr = &ADC1BUF0;

    for (i = 0; i < size_pin_cycling_vector; i++)
    {
        adc_aquisitions[i] = *(ptr++);
    }

    if (position > size_pin_cycling_vector || position + length > (size_pin_cycling_vector + 1) || value == NULL)
    {
        return 1;
    }

    for (i = 0, j = position; j < length; i++, j++)
    {
        value[j] = adc_aquisitions[i];
    }

    return 0;
}

#endif
