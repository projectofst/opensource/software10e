#ifndef NOUSB

#include "usb_lib.h"
#include "can.h"
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned volatile write_index_usb = 0;
unsigned volatile read_index_usb = BUFFER_SIZE_USB - 1;
unsigned volatile inner_write_index_usb = 0;

uint8_t gpacket_buffer_usb[BUFFER_SIZE_USB][BULK_SIZE_USB];
uint8_t gpacket_last_pos_usb[BUFFER_SIZE_USB];

__attribute__((weak)) void receive_handler_usb(char* readString)
{
    return;
}

void USBInit()
{
    USBDeviceInit();
    USBDeviceAttach();
}

/* Check if there is a packet to send 
 * There is if the write_index_usb is 2 positions in front of read_index_usb
 */
bool usb_full()
{
    return !((read_index_usb + 1) % BUFFER_SIZE_USB == write_index_usb);
}

void USBTasks()
{
    SYSTEM_Tasks();

#if defined(USB_BLUE_LED)
    if (USBDeviceState == CONFIGURED_STATE) {
        LATEbits.LATE7 = 1;
    } else {
        LATEbits.LATE7 = 0;
    }
#endif

    //Application specific tasks
    APP_DeviceCDCBasicDemoTasks();
}

void append_string_usb(uint8_t* buffer, size_t len)
{
    unsigned int i;
    /* if it doesn't fit in a packet then fuck you */
    if (len > BULK_SIZE_USB) {
        return;
    }

    /* If we have to much data to place on a buffer write it on the next one
	 * Save the last position on the buffer in another buffer.
	 * Update inner_write_index_usb to start from the beginning in the next buffer.
	 * Set the next buffer to zero so we don't send garbage.
	 */
    if (len > (BULK_SIZE_USB - inner_write_index_usb)) {
        gpacket_last_pos_usb[write_index_usb] = inner_write_index_usb;
        write_index_usb = (write_index_usb + 1) % BUFFER_SIZE_USB;
        inner_write_index_usb = 0;
        memset(gpacket_buffer_usb[write_index_usb], 0, BULK_SIZE_USB);
    }

    /* copy the buffer to the gpacket_buffer_usb */
    for (i = 0; i < len; i++, inner_write_index_usb++) {
        gpacket_buffer_usb[write_index_usb][inner_write_index_usb] = buffer[i];
    }
}

/**********************************************************************
 * Name:    uprintf
 * Args:	char *format and ... - same as printf
 * Return:  Error signalling
 *			0  - successful exit
 *			-1 - string too long
 * Desc:	Get CAN message from CAN 1 buffer
 *			Returns Null if there aren't any messages available
 **********************************************************************/
int uprintf(char* format, ...)
{
    /*TODO: must increase cpu priority, to make sure this doesn't get interrupted*/
    va_list aptr;

    va_start(aptr, format);

    const char buffer[256];

   /* vsprintf(buffer, format, aptr); */

    va_end(va);

    int bsize = strlen(buffer);

    if (bsize > BULK_SIZE_USB - 1)
        return -1;

    append_string_usb((uint8_t*)buffer, bsize);

    return 0;
}
#endif
