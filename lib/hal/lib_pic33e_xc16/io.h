#include <stdint.h>

#define DIGITAL 0
#define ANALOG 1

void pinB_set(uint8_t n, uint8_t type);
void pinC_set(uint8_t n, uint8_t type);
void pinD_set(uint8_t n, uint8_t type);
void pinE_set(uint8_t n, uint8_t type);
void pinG_set(uint8_t n, uint8_t type);

void pinB_high(uint8_t n);
void pinC_high(uint8_t n);
void pinD_high(uint8_t n);
void pinE_high(uint8_t n);
void pinF_high(uint8_t n);
void pinG_high(uint8_t n);

void pinB_low(uint8_t n);
void pinC_low(uint8_t n);
void pinD_low(uint8_t n);
void pinE_low(uint8_t n);
void pinF_low(uint8_t n);
void pinG_low(uint8_t n);
