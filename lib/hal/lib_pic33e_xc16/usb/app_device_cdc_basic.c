#ifndef NOUSB
/*******************************************************************************
Copyright 2016 Microchip Technology Inc. (www.microchip.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

To request to license the code under the MLA license (www.microchip.com/mla_license), 
please contact mla_licensing@microchip.com
*******************************************************************************/

/** INCLUDES *******************************************************/
#include "lib_pic33e/usb/system.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "lib_pic33e/usb/app_device_cdc_basic.h"
#include "lib_pic33e/usb/usb.h"
#include "lib_pic33e/usb/usb_config.h"

#include "lib_pic33e/usb_lib.h"

/** VARIABLES ******************************************************/

//static bool buttonPressed;
//static char buttonMessage[] = "Button pressed.\r\n";
static uint8_t readBufferUSB[CDC_DATA_OUT_EP_SIZE];
uint8_t outBufferUSB[BULK_SIZE_USB];

/*********************************************************************
* Function: void APP_DeviceCDCBasicDemoInitialize(void);
*
* Overview: Initializes the demo code
*
* PreCondition: None
*
* Input: None
*
* Output: None
*
********************************************************************/
void APP_DeviceCDCBasicDemoInitialize()
{
    line_coding.bCharFormat = 0;
    line_coding.bDataBits = 8;
    line_coding.bParityType = 0;
    line_coding.dwDTERate = 9600;
}

/*********************************************************************
* Function: void APP_DeviceCDCBasicDemoTasks(void);
*
* Overview: Keeps the demo running.
*
* PreCondition:pThe demo should have been initialized and started via
*   the APP_DeviceCDCBasicDemoInitialize() and APP_DeviceCDCBasicDemoStart() demos
*   respectively.
*
* Input: None
*
* Output: None
*
********************************************************************/
void APP_DeviceCDCBasicDemoTasks()
{
    unsigned int aux, i, j;
    /* If the USB device isn't configured yet, we can't really do anything
     * else since we don't have a host to talk to.  So jump back to the
     * top of the while loop. */
    if (USBGetDeviceState() < CONFIGURED_STATE) {
        return;
    }

    /* If we are currently suspended, then we need to see if we need to
     * issue a remote wakeup.  In either case, we shouldn't process any
     * keyboard commands since we aren't currently communicating to the host
     * thus just continue back to the start of the while loop. */
    if (USBIsDeviceSuspended() == true) {
        return;
    }

    /*Deleted code that sent a message everytime a button was pressed, 
    using functions mUSBUSARTIsTxTrfReady() and putrsUSBUSART(String message)*/

    /* Check to see if there is a transmission in progress, if there isn't, then
     * we can see about performing an echo response to data received.
     */
    if (USBUSARTIsTxTrfReady()) {
        uint8_t numBytesRead = 0;

        char readString[CDC_DATA_OUT_EP_SIZE];

        numBytesRead = getsUSBUSART(readBufferUSB, sizeof(readBufferUSB));

        /* For every byte that was read... */
        for (i = 0; i < numBytesRead; i++) {
            //echo everything that is read
            readString[i] = readBufferUSB[i];
        }
        readString[i] = '\0';

        if (numBytesRead > 0) {
            /* After processing all of the received data, we need to send out
                * the "echo" data now.
            */

            receive_handler_usb(readString);
            return;
        }
        //if nothing is read, send pending messages
        else {
            /* If we are right behind the write pointer we might have something
			 * to read.
			 *
			 * If the inner_write_index_usb is 0 then nothing has been written.
			 *
			 * Else there is stuff to send, even if it is not a full package.
			 * The last position is pointed by the inner_write_index_usb.
			 *
			 * Otherwise there is a full package to send. And the last position
			 * is pointed by the gpacket_last_pos_usb[read_index_usb}
			 * */
            if ((read_index_usb + 1) % BUFFER_SIZE_USB == write_index_usb) {
                if (inner_write_index_usb == 0) {
                    return;
                } else {
                    memcpy(outBufferUSB, gpacket_buffer_usb[(read_index_usb + 1) % BUFFER_SIZE_USB], BULK_SIZE_USB);
                    i = inner_write_index_usb;
                    inner_write_index_usb = 0;
                }
            } else {
                memcpy(outBufferUSB, gpacket_buffer_usb[read_index_usb], BULK_SIZE_USB);
                i = gpacket_last_pos_usb[read_index_usb];
                read_index_usb = (read_index_usb + 1) % BUFFER_SIZE_USB;
            }

            putUSBUSART(outBufferUSB, i);
        }
    }
    CDCTxService();
}
#endif
