#include "pwm.h"


//=====================================OC1===============================================
int pwm1_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC1CON1 = 0;
    
    OC1CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC1CON2 = 0x8B; // 1000 1011
    
    //PWM period
    OC1RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC1R = (uint16_t) (duty_cycle * OC1RS);


    pwm1_start();

    return 0;
}

int pwm1_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC1R = (uint16_t) (duty_cycle * OC1RS);
    
    return 0;
}

void pwm1_stop( void ){
    //Edge-alligned mode bits 
    OC1CON1bits.OCM = 0;
}

void pwm1_start( void ){
    //Edge-alligned mode bits 
    OC1CON1bits.OCM = 0b110;
}



//=====================================OC2===============================================
int pwm2_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC2CON1 = 0;
    
    OC2CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC2CON2 = 0x8B; // 1000 1011


    //PWM period
    OC2RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC2R = (uint16_t) (duty_cycle * OC1RS);


    pwm2_start();

    return 0;
}

int pwm2_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC2R = (uint16_t) (duty_cycle * OC2RS);
    
    return 0;
}

void pwm2_stop( void ){
    //Edge-alligned mode bits 
    OC2CON1bits.OCM = 0;
}

void pwm2_start( void ){
    //Edge-alligned mode bits 
    OC2CON1bits.OCM = 0b110;
}

//=====================================OC3===============================================

int pwm3_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC3CON1 = 0;
    
    OC3CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC3CON2 = 0x8B; // 1000 1011


    //PWM period
    OC3RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC3R = (uint16_t) (duty_cycle * OC3RS);


    pwm3_start();

    return 0;
}

int pwm3_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC3R = (uint16_t) (duty_cycle * OC3RS);
    
    return 0;
}

void pwm3_stop( void ){
    //Edge-alligned mode bits 
    OC3CON1bits.OCM = 0;
}

void pwm3_start( void ){
    //Edge-alligned mode bits 
    OC3CON1bits.OCM = 0b110;
}

//=====================================OC4===============================================

int pwm4_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC4CON1 = 0;
    
    OC4CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC4CON2 = 0x8B; // 1000 1011


    //PWM period
    OC4RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC4R = (uint16_t) (duty_cycle * OC4RS);


    pwm4_start();

    return 0;
}

int pwm4_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC4R = (uint16_t) (duty_cycle * OC4RS);
    
    return 0;
}

void pwm4_stop( void ){
    //Edge-alligned mode bits 
    OC4CON1bits.OCM = 0;
}

void pwm4_start( void ){
    //Edge-alligned mode bits 
    OC4CON1bits.OCM = 0b110;
}

//=====================================OC5===============================================

int pwm5_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC5CON1 = 0;
    
    OC5CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC5CON2 = 0x8B; // 1000 1011


    //PWM period
    OC5RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC5R = (uint16_t) (duty_cycle * OC5RS);


    pwm5_start();

    return 0;
}

int pwm5_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC5R = (uint16_t) (duty_cycle * OC5RS);
    
    return 0;
}

void pwm5_stop( void ){
    //Edge-alligned mode bits 
    OC5CON1bits.OCM = 0;
}

void pwm5_start( void ){
    //Edge-alligned mode bits 
    OC5CON1bits.OCM = 0b110;
}

//=====================================OC6===============================================

int pwm6_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC6CON1 = 0;
    
    OC6CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC6CON2 = 0x8B; // 1000 1011


    //PWM period
    OC6RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC6R = (uint16_t) (duty_cycle * OC6RS);

    pwm6_start();

    return 0;
}

int pwm6_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC6R = (uint16_t) (duty_cycle * OC6RS);
    
    return 0;
}

void pwm6_stop( void ){
    //Edge-alligned mode bits 
    OC6CON1bits.OCM = 0;
}

void pwm6_start( void ){
    //Edge-alligned mode bits 
    OC6CON1bits.OCM = 0b110;
}

//=====================================OC7===============================================

int pwm7_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;
    OC7CON1 = 0;
    
    OC7CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC7CON2 = 0x8B; // 1000 1011


    //PWM period
    OC7RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC7R = (uint16_t) (duty_cycle * OC7RS);


    pwm7_start();

    return 0;
}

int pwm7_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC7R = (uint16_t) (duty_cycle * OC7RS);
    
    return 0;
}

void pwm7_stop( void ){
    //Edge-alligned mode bits 
    OC7CON1bits.OCM = 0;
}

void pwm7_start( void ){
    //Edge-alligned mode bits 
    OC7CON1bits.OCM = 0b110;
}

//=====================================OC7===============================================

int pwm8_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC8CON1 = 0;
    
    OC8CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC8CON2 = 0x8B; // 1000 1011


    //PWM period
    OC8RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC8R = (uint16_t) (duty_cycle * OC8RS);


    pwm8_start();

    return 0;
}

int pwm8_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC8R = (uint16_t) (duty_cycle * OC8RS);
    
    return 0;
}

void pwm8_stop( void ){
    //Edge-alligned mode bits 
    OC8CON1bits.OCM = 0;
}

void pwm8_start( void ){
    //Edge-alligned mode bits 
    OC8CON1bits.OCM = 0b110;
}

//=====================================OC9===============================================

int pwm9_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC9CON1 = 0;
    
    OC9CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC9CON2 = 0x8B; // 1000 1011


    //PWM period
    OC9RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC9R = (uint16_t) (duty_cycle * OC9RS);


    pwm9_start();

    return 0;
}

int pwm9_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC9R = (uint16_t) (duty_cycle * OC9RS);
    
    return 0;
}

void pwm9_stop( void ){
    //Edge-alligned mode bits 
    OC9CON1bits.OCM = 0;
}

void pwm9_start( void ){
    //Edge-alligned mode bits 
    OC9CON1bits.OCM = 0b110;
}

//=====================================OC10===============================================

int pwm10_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC10CON1 = 0;
    
    OC10CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch thi9
    OC10CON2 = 0x8B; // 1000 1011


    //PWM period
    OC10RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC10R = (uint16_t) (duty_cycle * OC10RS);


    pwm10_start();

    return 0;
}

int pwm10_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC10R = (uint16_t) (duty_cycle * OC10RS);
    
    return 0;
}

void pwm10_stop( void ){
    //Edge-alligned mode bits 
    OC10CON1bits.OCM = 0;
}

void pwm10_start( void ){
    //Edge-alligned mode bits 
    OC10CON1bits.OCM = 0b110;
}

//=====================================OC11===============================================

int pwm11_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC11CON1 = 0;
    
    OC11CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC11CON2 = 0x8B; // 1000 1011


    //PWM period
    OC11RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC11R = (uint16_t) (duty_cycle * OC11RS);


    pwm11_start();

    return 0;
}

int pwm11_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC11R = (uint16_t) (duty_cycle * OC11RS);
    
    return 0;
}

void pwm11_stop( void ){
    //Edge-alligned mode bits 
    OC11CON1bits.OCM = 0;
}

void pwm11_start( void ){
    //Edge-alligned mode bits 
    OC11CON1bits.OCM = 0b110;
}

//=====================================OC12===============================================

int pwm12_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC12CON1 = 0;
    
    OC12CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC12CON2 = 0x8B; // 1000 1011


    //PWM period
    OC12RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC12R = (uint16_t) (duty_cycle * OC12RS);


    pwm12_start();

    return 0;
}

int pwm12_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC12R = (uint16_t) (duty_cycle * OC12RS);
    
    return 0;
}

void pwm12_stop( void ){
    //Edge-alligned mode bits 
    OC12CON1bits.OCM = 0;
}

void pwm12_start( void ){
    //Edge-alligned mode bits 
    OC12CON1bits.OCM = 0b110;
}

//=====================================OC13===============================================

int pwm13_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC13CON1 = 0;
    
    OC13CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC13CON2 = 0x8B; // 1000 1011


    //PWM period
    OC13RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC13R = (uint16_t) (duty_cycle * OC13RS);


    pwm13_start();

    return 0;
}

int pwm13_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC13R = (uint16_t) (duty_cycle * OC13RS);
    
    return 0;
}

void pwm13_stop( void ){
    //Edge-alligned mode bits 
    OC13CON1bits.OCM = 0;
}

void pwm13_start( void ){
    //Edge-alligned mode bits 
    OC13CON1bits.OCM = 0b110;
}

//=====================================OC14===============================================

int pwm14_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC14CON1 = 0;
    
    OC14CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC14CON2 = 0x8B; // 1000 1011


    //PWM period
    OC14RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC14R = (uint16_t) (duty_cycle * OC14RS);


    pwm14_start();

    return 0;
}

int pwm14_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC14R = (uint16_t) (duty_cycle * OC14RS);
    
    return 0;
}

void pwm14_stop( void ){
    //Edge-alligned mode bits 
    OC14CON1bits.OCM = 0;
}

void pwm14_start( void ){
    //Edge-alligned mode bits 
    OC14CON1bits.OCM = 0b110;
}

//=====================================OC15===============================================

int pwm15_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC15CON1 = 0;
    
    OC15CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC15CON2 = 0x8B; // 1000 1011


    //PWM period
    OC15RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC15R = (uint16_t) (duty_cycle * OC15RS);


    pwm15_start();

    return 0;
}

int pwm15_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC15R = (uint16_t) (duty_cycle * OC15RS);
    
    return 0;
}

void pwm15_stop( void ){
    //Edge-alligned mode bits 
    OC15CON1bits.OCM = 0;
}

void pwm15_start( void ){
    //Edge-alligned mode bits 
    OC15CON1bits.OCM = 0b110;
}

//=====================================OC16===============================================

int pwm16_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    uint32_t clock_frequency = FCY / 256;
    
    float pwm_period = period * clock_frequency;

    if (pwm_period > 0xFFFF || pwm_period < 1)
        return -1; 

    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC16CON1 = 0;
    
    OC16CON1bits.OCTSEL = pwm_clock_source;
    // Don't touch this
    OC16CON2 = 0x8B; // 1000 1011


    //PWM period
    OC16RS = (uint16_t) (period * clock_frequency);
    // PWM duty cycle 
    OC16R = (uint16_t) (duty_cycle * OC16RS);


    pwm16_start();

    return 0;
}

int pwm16_update_duty_cycle(float duty_cycle){
    if (duty_cycle > 1 || duty_cycle < 0)
        return -2;

    OC16R = (uint16_t) (duty_cycle * OC16RS);
    
    return 0;
}

void pwm16_stop( void ){
    //Edge-alligned mode bits 
    OC16CON1bits.OCM = 0;
}

void pwm16_start( void ){
    //Edge-alligned mode bits 
    OC16CON1bits.OCM = 0b110;
}
 
