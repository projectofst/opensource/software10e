/**********************************************************************
 *   lib_pic30f
 *
 *   Timer
 *      - parameters
 *      - functions prototypes
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __TIMER_H__
#define __TIMER_H__
#include "timing.h"
/*
 * Prototypes
 */

void config_timer1(unsigned int time, unsigned int priority);
void config_timer2(unsigned int time, unsigned int priority);
void config_timer3(unsigned int time, unsigned int priority);
void config_timer4(unsigned int time, unsigned int priority);
void config_timer5(unsigned int time, unsigned int priority);
void config_timer6(unsigned int time, unsigned int priority);
void config_timer7(unsigned int time, unsigned int priority);
void config_timer8(unsigned int time, unsigned int priority);
void config_timer9(unsigned int time, unsigned int priority);

void config_timer1_us(unsigned int time, unsigned int priority);
void config_timer2_us(unsigned int time, unsigned int priority);
void config_timer3_us(unsigned int time, unsigned int priority);
void config_timer4_us(unsigned int time, unsigned int priority);
void config_timer5_us(unsigned int time, unsigned int priority);
void config_timer6_us(unsigned int time, unsigned int priority);
void config_timer7_us(unsigned int time, unsigned int priority);
void config_timer8_us(unsigned int time, unsigned int priority);
void config_timer9_us(unsigned int time, unsigned int priority);

void enable_timer1(void);
void enable_timer2(void);
void enable_timer3(void);
void enable_timer4(void);
void enable_timer5(void);
void enable_timer6(void);
void enable_timer7(void);
void enable_timer8(void);
void enable_timer9(void);

void disable_timer1(void);
void disable_timer2(void);
void disable_timer3(void);
void disable_timer4(void);
void disable_timer5(void);
void disable_timer6(void);
void disable_timer7(void);
void disable_timer8(void);
void disable_timer9(void);

void reset_timer1(void);
void reset_timer2(void);
void reset_timer3(void);
void reset_timer4(void);
void reset_timer5(void);
void reset_timer6(void);
void reset_timer7(void);
void reset_timer8(void);
void reset_timer9(void);

void timer1_callback(void);
void timer2_callback(void);
void timer3_callback(void);
void timer4_callback(void);
void timer5_callback(void);
void timer6_callback(void);
void timer7_callback(void);
void timer8_callback(void);
void timer9_callback(void);

#endif
