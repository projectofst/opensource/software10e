#ifndef _PWM_H_
#define _PWM_H_
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "timing.h"


typedef enum {
	CLK_TIMER2 = 0,
	CLK_TIMER3 = 1,
	CLK_TIMER4 = 2,
	CLK_TIMER5 = 3,
	CLK_TIMER1 = 4
} pwm_clock_source_t;


int pwm1_config(float duty_cycle, float period, pwm_clock_source_t pwm_clock_source);
void pwm1_start( void );
void pwm1_stop( void );
int pwm1_update_duty_cycle(float duty_cycle);

int pwm2_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm2_start( void );
void pwm2_stop( void );
int pwm2_update_duty_cycle(float duty_cycle);

int pwm3_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm3_start( void );
void pwm3_stop( void );
int pwm3_update_duty_cycle(float duty_cycle);

int pwm4_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm4_start( void );
void pwm4_stop( void );
int pwm4_update_duty_cycle(float duty_cycle);

int pwm5_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm5_start( void );
void pwm5_stop( void );
int pwm5_update_duty_cycle(float duty_cycle);

int pwm5_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm5_start( void );
void pwm5_stop( void );
int pwm5_update_duty_cycle(float duty_cycle);

int pwm6_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm6_start( void );
void pwm6_stop( void );
int pwm6_update_duty_cycle(float duty_cycle);

int pwm7_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm7_start( void );
void pwm7_stop( void );
int pwm7_update_duty_cycle(float duty_cycle);

int pwm8_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm8_start( void );
void pwm8_stop( void );
int pwm8_update_duty_cycle(float duty_cycle);

int pwm9_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm9_start( void );
void pwm9_stop( void );
int pwm9_update_duty_cycle(float duty_cycle);

int pwm10_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm10_start( void );
void pwm10_stop( void );
int pwm10_update_duty_cycle(float duty_cycle);

int pwm11_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm11_start( void );
void pwm11_stop( void );
int pwm11_update_duty_cycle(float duty_cycle);

int pwm12_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm12_start( void );
void pwm12_stop( void );
int pwm12_update_duty_cycle(float duty_cycle);

int pwm13_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm13_start( void );
void pwm13_stop( void );
int pwm13_update_duty_cycle(float duty_cycle);

int pwm14_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm14_start( void );
void pwm14_stop( void );
int pwm14_update_duty_cycle(float duty_cycle);

int pwm15_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm15_start( void );
void pwm15_stop( void );
int pwm15_update_duty_cycle(float duty_cycle);

int pwm16_config(float duty_cycle, float period,  pwm_clock_source_t pwm_clock_source);
void pwm16_start( void );
void pwm16_stop( void );
int pwm16_update_duty_cycle(float duty_cycle);


#endif
 
