#ifndef _UTILS_H
#define _UTILS_H

#include <stdint.h>

#define TOGGLE_PIN(PIN)	(PIN ^= 1)

#define WRT_BITFLD(bitfield,position,value) ((bitfield) >> (position)) = \
	(value) & 1

#define RD_BITFLD(bitfield,position) (((bitfield) >> (position)) & 1)

#ifndef SET_CPU_IPL
#define SET_CPU_IPL(ipl) {       \
  int DISI_save;                 \
                                 \
  DISI_save = DISICNT;           \
  asm volatile ("disi #0x3FFF"); \
  SRbits.IPL = ipl;              \
  __builtin_write_DISICNT( DISI_save); } (void) 0;
#endif

uint16_t popcount(uint16_t x);

#endif
