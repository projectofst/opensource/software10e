#include "external.h"
#include <stdbool.h>
#include <xc.h>

/**********************************************************************
 * Name:    config_external0
 * Args:    polarity (0/1), priority (0-7)
 * Return:  -
 * Desc:    Configures and enables INT0 module.
 **********************************************************************/

void config_external0(bool polarity, unsigned int priority)
{
    TRISFbits.TRISF6 = 1; /*define INT0 as input                       */
    INTCON2bits.INT0EP = polarity; /*interrupt on 1=negative or 0=positive edge */
    IFS0bits.INT0IF = 0; /* clear interrupt flag                      */
    IEC0bits.INT0IE = 1; /* External 0 Interrupt Enable               */
    IPC0bits.INT0IP = priority; /* External 0 Interrupt Priority 0-7         */
    return;
}

/**********************************************************************
 * Name:    config_external1
 * Args:    polarity (0/1), priority (0-7)
 * Return:  -
 * Desc:    Configures and enables INT1 module.
 **********************************************************************/

void config_external1(bool polarity, unsigned int priority)
{
    TRISDbits.TRISD8 = 1; /*define INT1 as input                       */
    INTCON2bits.INT1EP = polarity; /*interrupt on 1=negative or 0=positive edge */
    IFS1bits.INT1IF = 0; /* clear interrupt flag                      */
    IEC1bits.INT1IE = 1; /* External 1 Interrupt Enable               */
    IPC4bits.INT1IP = priority; /* External 1 Interrupt Priority 0-7         */
    return;
}

/**********************************************************************
 * Name:    config_external2
 * Args:    polarity (0/1), priority (0-7)
 * Return:  -
 * Desc:    Configures and enables INT2 module.
 **********************************************************************/

void config_external2(bool polarity, unsigned int priority)
{
    TRISDbits.TRISD9 = 1; /*define INT2 as input                       */
    INTCON2bits.INT2EP = polarity; /*interrupt on 1=negative or 0=positive edge */
    IFS1bits.INT2IF = 0; /* clear interrupt flag                      */
    IEC1bits.INT2IE = 1; /* External 2 Interrupt Enable               */
    IPC5bits.INT2IP = priority; /* External 2 Interrupt Priority 0-7         */
    return;
}

/**********************************************************************
 * Name:    config_external3
 * Args:    polarity (0/1), priority (0-7)
 * Return:  -
 * Desc:    Configures and enables INT3 module.
 **********************************************************************/

void config_external3(bool polarity, unsigned int priority)
{
    TRISDbits.TRISD10 = 1; /*define INT3 as input                      */
    INTCON2bits.INT3EP = polarity; /*interrupt on 1=negative or 0=positive edge */
    IFS2bits.INT3IF = 0; /* clear interrupt flag                      */
    IEC2bits.INT3IE = 1; /* External 3 Interrupt Enable               */
    IPC9bits.INT3IP = priority; /* External 3 Interrupt Priority 0-7         */
    return;
}

/**********************************************************************
 * Name:    config_external4
 * Args:    polarity (0/1), priority (0-7)
 * Return:  -
 * Desc:    Configures and enables INT4 module.
 **********************************************************************/

void config_external4(bool polarity, unsigned int priority)
{
    TRISDbits.TRISD11 = 1; /*define INT4 as input                      */
    INTCON2bits.INT4EP = polarity; /*interrupt on 1=negative or 0=positive edge */
    IFS2bits.INT4IF = 0; /* clear interrupt flag                      */
    IEC2bits.INT4IE = 1; /* External 0 Interrupt Enable               */
    IPC9bits.INT4IP = priority; /* External 0 Interrupt Priority 0-7         */
    return;
}

void __attribute__((weak)) external0_callback(void)
{
    return;
}

void __attribute__((weak)) external1_callback(void)
{
    return;
}

void __attribute__((weak)) external2_callback(void)
{
    return;
}

void __attribute__((weak)) external3_callback(void)
{
    return;
}

void __attribute__((weak)) external4_callback(void)
{
    return;
}

/**********************************************************************
 * Assign INT0 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _INT0Interrupt(void)
{
    external0_callback();
    IFS0bits.INT0IF = 0; /* clear interrupt flag                      */
}

/**********************************************************************
 * Assign INT1 interruption
 **********************************************************************/

void __attribute__((interrupt, auto_psv, shadow)) _INT1Interrupt(void)
{
    external1_callback();
    IFS1bits.INT1IF = 0; /* clear interrupt flag                      */

    return;
}

/**********************************************************************
 * Assign INT2 interruption
 **********************************************************************/

void __attribute__((interrupt, auto_psv, shadow)) _INT2Interrupt(void)
{
    external2_callback();
    IFS1bits.INT2IF = 0; /* clear interrupt flag                      */

    return;
}

/**********************************************************************
 * Assign INT3 interruption
 **********************************************************************/

void __attribute__((interrupt, auto_psv, shadow)) _INT3Interrupt(void)
{
    external3_callback();
    IFS2bits.INT3IF = 0; /* clear interrupt flag                      */

    return;
}

/**********************************************************************
 * Assign INT4 interruption
 **********************************************************************/

void __attribute__((interrupt, auto_psv, shadow)) _INT4Interrupt(void)
{
    external4_callback();
    IFS2bits.INT4IF = 0; /* clear interrupt flag                      */

    return;
}
