/**********************************************************************
 *   FST CAN tools --- translator
 *
 *   CAN
 *      - data structures
 *      - functions prototypes
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __CAN_H__
#define __CAN_H__

#include <stdbool.h>
#include <stdint.h>

#ifndef _CANDATA
#define _CANDATA
typedef struct {
	union {
		struct {
			uint16_t dev_id:5; // Least significant
			uint16_t msg_id:6; // Most significant
		};
		uint16_t sid;
	};
	uint16_t dlc:4;
	uint16_t data[4];
} CANdata;
#endif


typedef struct _CANfilter {
	unsigned masks[2];
	unsigned filters1[2];
	unsigned filters2[4];
} CANfilter;

#define CAN1_BUFFER_LENGTH 64
#define CAN2_BUFFER_LENGTH 64
#define CAN_BUFFER_LENGTH_SEND 20


void send_can1_buffer();
void write_to_can1_buffer(CANdata CANmessage, bool priority);

void send_can2_buffer();
void write_to_can2_buffer(CANdata CANmessage, bool priority);

int set_can1_filter(uint16_t filter, uint16_t filter_number);
int set_can1_mask(uint16_t mask, uint16_t mask_number);
void config_can1(void);
int send_can1(CANdata msg);
uint16_t can1_tx_available();
CANdata *pop_can1(void);

int set_can2_filter(uint16_t filter, uint16_t filter_number);
int set_can2_mask(uint16_t mask, uint16_t mask_number);
void config_can2(void);
int send_can2(CANdata msg);
uint16_t can2_tx_available();
CANdata *pop_can2(void);

bool can1_rx_empty();
bool can2_rx_empty();

int set_CPU_priority(unsigned int n);

#endif
