#include <stdint.h>
#include <xc.h>

#include "adc.h"
#include "timing.h"

/**********************************************************************
 * Name:    config_adc
 * Args:    ADC_parameters filled with the configuracions for the adc module
 * Return:  -
 * Desc:    Configures adc module
 **********************************************************************/
void config_adc(ADC_parameters parameter)
{

    ADCON1bits.ADSIDL = ~parameter.idle;
    ADCON1bits.FORM = parameter.form;
    ADCON1bits.SSRC = parameter.conversion_trigger;
    ADCON1bits.ASAM = parameter.sampling_type;

    ADCON2bits.VCFG = parameter.voltage_ref;
    ADCON2bits.CSCNA = parameter.sample_pin_select_type;
    ADCON2bits.SMPI = parameter.smpi;
    ADCON2bits.BUFM = 0; /* 16-word buffer                                */
    ADCON2bits.ALTS = 0; /* always use MUX A                              */

    ADCON3bits.SAMC = parameter.sampling_time; /*auto sample time =  31 TAD                     */
    ADCON3bits.ADRC = 0; /*use system clock                               */
    ADCON3bits.ADCS = parameter.conversion_time; /*AD conversion clock = 32/Fcy                   */

    ADPCFG = ~parameter.pin_select; /*set pins as analog inputs                       */
    ADCSSL = parameter.pin_select; /*select which pins to scan                       */

    /* interrupts */
    IFS0bits.ADIF = 0; /*clear interrupt flag                            */
    IEC0bits.ADIE = (parameter.interrupt_priority > 0); /*enable interrupt if the priority is more then 0 */
    IPC2bits.ADIP = parameter.interrupt_priority;

    ADCON1bits.ADON = parameter.turn_on;

    return;
}

void __attribute__((weak)) adc_callback(void)
{
    return;
}

/**********************************************************************
 * Assign End of ADC conversion interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _ADCInterrupt(void)
{
    adc_callback();
    IFS0bits.ADIF = 0; /*clear interrupt flag */
    return;
}

/**********************************************************************
 * Name:    get_adc_value
 * Args:    the sample you want to retrive (the firs one is sample_number 0,
 *          the second one is sample_number one...)
 * Return:  the value read by the adc
 * Desc:    gets the adc value from the buffer and returns it
 **********************************************************************/

unsigned int get_adc_value(unsigned int sample_number)
{
    if (sample_number > 15) {
        return -1;
    } else {
        return *(&ADCBUF0 + sample_number);
    }
}

/**********************************************************************
 * Name:    turn_on_adc
 * Args:    -
 * Return:  -
 * Desc:    turns on the adc module
 **********************************************************************/
void turn_on_adc()
{
    ADCON1bits.ADON = 1;
}

/**********************************************************************
 * Name:    turn_off_adc
 * Args:    -
 * Return:  -
 * Desc:    turns off the adc module
 **********************************************************************/
void turn_off_adc()
{
    ADCON1bits.ADON = 0;
}

/**********************************************************************
 * Name:    start_samp
 * Args:    -
 * Return:  -
 * Desc:    starts a new sample 
 * IMPORTANTE: only use this fuction if your sampling_type is set to MANUAL
 **********************************************************************/
void start_samp()
{
    ADCON1bits.SAMP = 1;
}

/**********************************************************************
 * Name:    start_conversion
 * Args:    -
 * Return:  -
 * Desc:    ends sampling and starts conversion
 * IMPORTANTE: only use this fuction if your conversion_trigger is set to SAMP
 **********************************************************************/
void start_conversion()
{
    ADCON1bits.SAMP = 0;
}

/**********************************************************************
 * Name:    wait_for_done
 * Args:    -
 * Return:  -
 * Desc:    waits until conversion is done
 **********************************************************************/
void wait_for_done()
{
    while (!ADCON1bits.DONE)
        ;
    return;
}

/**********************************************************************
 * Name:    set_default_parameters
 * Args:    ADC_parameters * (empty struct to be filled)
 * Return:  -
 * Desc:    fills a ADC_parameters struct with default configuration
 **********************************************************************/
void set_default_parameters_adc(ADC_parameters* parameters)
{

    parameters->idle = 1; /*works in idle mode                       */
    parameters->form = 0; /*form set to unsigned integer             */
    parameters->conversion_trigger = AUTO_CONV; /*auto conversion                          */
    parameters->sampling_type = AUTO; /*auto sampling                            */
    parameters->voltage_ref = INTERNAL; /*use internal voltage reference           */
    parameters->sample_pin_select_type = AUTO; /*auto change between differrent inputs    */
    parameters->smpi = 0; /*1 sample per interrupt                   */
    parameters->pin_select = 1; /*scan pin AN0                             */
    parameters->interrupt_priority = 5; /*interrupt priority set to 5              */
    parameters->turn_on = 1; /*turn the module on                       */
    return;
}

/**********************************************************************
 * Name:    select_input_pin
 * Args:    number of the pin you want to scan
 * Return:  -
 * Desc:    defines what pin to scan next
 **********************************************************************/
void select_input_pin(unsigned int pin)
{
    ADCHSbits.CH0SA = pin;
    return;
}

uint16_t get_adcbuf0()
{
    return ADCBUF0;
}

uint16_t get_adcbuf1()
{
    return ADCBUF1;
}

uint16_t get_adcbuf2()
{
    return ADCBUF2;
}

uint16_t get_adcbuf3()
{
    return ADCBUF3;
}

uint16_t get_adcbuf4()
{
    return ADCBUF4;
}

uint16_t get_adcbuf5()
{
    return ADCBUF5;
}
