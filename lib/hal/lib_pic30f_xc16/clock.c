#include "clock.h"

void test_clock(void)
{
    // Configure RD9 as output
    TRISDbits.TRISD9 = 0;
    // Generate a square wave at 1/12 of FCY
    asm("loop:");
    asm("btg LATD, #9"); // 1 cycle
    asm("nop"); // 1 cycle
    asm("nop"); // 1 cycle
    asm("nop"); // 1 cycle
    asm("bra loop"); // 2 cycles
}
