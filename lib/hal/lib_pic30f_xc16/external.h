#ifndef __EXTERNAL_H__
#define __EXTERNAL_H__

#include <stdbool.h>

void config_external0(bool polarity, unsigned int priority);
void config_external1(bool polarity, unsigned int priority);
void config_external2(bool polarity, unsigned int priority);
void config_external3(bool polarity, unsigned int priority);
void config_external4(bool polarity, unsigned int priority);

void external0_callback(void);
void external1_callback(void);
void external2_callback(void);
void external3_callback(void);
void external4_callback(void);

#endif