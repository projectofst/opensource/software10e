MODULE_C_SOURCES:=adc.c
MODULE_C_SOURCES+=can.c
MODULE_C_SOURCES+=clock.c
MODULE_C_SOURCES+=eeprom.c
MODULE_C_SOURCES+=external.c
MODULE_C_SOURCES+=timer.c
MODULE_C_SOURCES+=trap.c
MODULE_C_SOURCES+=version.c

PWD:=lib/lib_pic30f_xc16/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
