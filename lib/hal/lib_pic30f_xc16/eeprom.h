#ifndef __EEPROM_H__
#define __EEPROM_H__

#include <xc.h>
#include <libpic30.h>

#include <stdint.h>

void     EEPROM_init_addr   (void);
void     eeprom_store_value (_prog_addressT eeprom_address, volatile unsigned int value);
void     eeprom_erase_word  (_prog_addressT eeprom_address);
void     eeprom_erase_row   (_prog_addressT eeprom_address);
void     eeprom_write_word  (_prog_addressT eeprom_address, volatile unsigned int value);
void     eeprom_read_row    (_prog_addressT EE_address, uint16_t EE_data_vector[]);
void     eeprom_write_row   (_prog_addressT EE_address, uint16_t data_vector[]);
uint16_t eeprom_read_word   (_prog_addressT eeprom_address);

#endif
