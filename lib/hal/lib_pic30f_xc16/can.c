/**********************************************************************
 *   FST CAN tools --- translator
 *
 *   CAN
 *      - device configuration
 *      - interruption assignement
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <xc.h>

#include "can.h"

static CANdata CAN1_buffer[CAN1_BUFFER_LENGTH];
volatile static unsigned int CAN1_write = 0, CAN1_read = 0;

CANdata static CAN2_buffer[CAN2_BUFFER_LENGTH];
volatile static unsigned int CAN2_write = 0, CAN2_read = 0;

/* P* stands for the priority of the buffer - P1 messages should be send before P2 messages*/
static CANdata CAN1_buffer_P1[CAN_BUFFER_LENGTH_SEND];
static CANdata CAN1_buffer_P2[CAN_BUFFER_LENGTH_SEND];
volatile static unsigned int CAN1_write_P1 = 0, CAN1_write_P2 = 0, CAN1_send_P1 = 0, CAN1_send_P2 = 0;

/* P* stands for the priority of the buffer - P1 messages should be send before P2 messages*/
static CANdata CAN2_buffer_P1[CAN_BUFFER_LENGTH_SEND];
static CANdata CAN2_buffer_P2[CAN_BUFFER_LENGTH_SEND];
volatile static unsigned int CAN2_write_P1 = 0, CAN2_write_P2 = 0, CAN2_send_P1 = 0, CAN2_send_P2 = 0;

/**********************************************************************
 * Name:    config_can1
 * Args:    -
 * Return:  -
 * Desc:    Configures CAN 1 module.
 **********************************************************************/
void config_can1(void)
{
    /* assuming FOSC 32MHz */
    TRISFbits.TRISF0 = 1; /* CANRX input */
    TRISFbits.TRISF1 = 0; /* CANTX output */

    C1CTRLbits.REQOP = 4; /* request configuration mode */
    while (C1CTRLbits.OPMODE != 4)
        ; /* wait for mode to be set */

    /* control */
    C1CTRLbits.CANCAP = 0; /* disable CAN capture */
    C1CTRLbits.CSIDL = 0; /* continue on Idle mode */
    C1CTRLbits.CANCKS = 1; /* master clock FCY */

    /* baudrate */
    C1CFG1bits.BRP = 0; //7			/* TQ = x/FCAN */
    C1CFG2bits.SEG2PHTS = 1; /* Freely programmable */
    C1CFG2bits.PRSEG = 1; //4		/* propagation time segment bits length 2 x TQ */
    C1CFG1bits.SJW = 0; /* re-synchronization jump width time is 1 x TQ */
    C1CFG2bits.SEG1PH = 2; //7		/* phase 1 segment length 3 x TQ */
    C1CFG2bits.SEG2PH = 1; /* phase 2 segment length 2 x TQ <= phase1+prop.seg. && > resync jump */

    /* receive buffers */
    C1RX0CONbits.DBEN = 1; /* RX0 full will write to RX1 */
    C1RXF0SID = 0; /* probably both wrong (filter set but not used) and unnecessary ???!!!! */
    C1RXF1SID = 0; /* " */
    C1RXM0SID = 0; /* match all messages */
    C1RXM1SID = 0; /* " */

    /* interrupts */
    C1INTEbits.RX0IE = 1; /* RX0 interrupt enabled */
    C1INTEbits.RX1IE = 1; /* RX1 interrupt enabled */
    IFS1bits.C1IF = 0; /* Clear CAN2 Flag */
    IPC6bits.C1IP = 6; /* CAN1 priority is 6 */
    IEC1bits.C1IE = 1; /* CAN1 interrupts enabled */
    C1TX0CONbits.TXPRI = 3;
    C1RX0CONbits.RXFUL = 0;
    C1RX1CONbits.RXFUL = 0;

    C1CTRLbits.REQOP = 0; /* request normal mode */
    while (C1CTRLbits.OPMODE != 0)
        ; /* wait for normal mode to be set */

    return;
}

/**********************************************************************
 * Name:    config_can2
 * Args:    -
 * Return:  -
 * Desc:    Configures CAN 2 module.
 **********************************************************************/
void config_can2(void)
{

    /* assuming FOSC 32MHz */
    TRISGbits.TRISG0 = 1; /* CANRX input */
    TRISGbits.TRISG1 = 0; /* CANTX output */
    C2CTRLbits.REQOP = 4; /* request configuration mode */
    while (C2CTRLbits.OPMODE != 4)
        ; /* wait for mode to be set */

    /* control */
    C2CTRLbits.CANCAP = 0; /* disable CAN capture */
    C2CTRLbits.CSIDL = 0; /* continue on Idle mode */
    C2CTRLbits.CANCKS = 1; /* master clock FCY */

    /* baudrate */
    C2CFG1bits.BRP = 0; //7			/* TQ = x/FCAN */
    C2CFG2bits.SEG2PHTS = 1; /* Freely programmable */
    C2CFG2bits.PRSEG = 1; //4		/* propagation time segment bits length 2 x TQ */
    C2CFG1bits.SJW = 0; /* re-synchronization jump width time is 1 x TQ */
    C2CFG2bits.SEG1PH = 2; //7		/* phase 1 segment length 3 x TQ */
    C2CFG2bits.SEG2PH = 1; /* phase 2 segment length 2 x TQ <= phase1+prop.seg. && > resync jump */

    /* receive buffers */
    C2RX0CONbits.DBEN = 1; /* RX0 full will write to RX1 */
    C2RXF0SID = 0; /* probably both wrong (filter set but not used) and unnecessary ???!!!! */
    C2RXF1SID = 0; /* " */
    C2RXM0SID = 0; /* match all messages */
    C2RXM1SID = 0; /* " */

    /* interrupts */
    C2INTEbits.RX0IE = 1; /* RX0 interrupt enabled */
    C2INTEbits.RX1IE = 1; /* RX1 interrupt enabled */
    IFS2bits.C2IF = 0; /* Clear CAN2 Flag */
    IPC9bits.C2IP = 6; /* CAN2 priority is 6 */
    IEC2bits.C2IE = 1; /* CAN2 interrupts enabled */
    C2TX0CONbits.TXPRI = 3;
    C2RX0CONbits.RXFUL = 0;
    C2RX1CONbits.RXFUL = 0;
    C2CTRLbits.REQOP = 0; /* request normal mode */
    while (C2CTRLbits.OPMODE != 0)
        ; /* wait for normal mode to be set */

    return;
}

/**********************************************************************
 * Name:    send_can1
 * Args:    CANdata *msg
 * Return:  exit status
 * Desc:    Sends a CANdata structure through CAN 1
 **********************************************************************/
int send_can1(CANdata msg)
{

    unsigned int int_sid = 0;

    int_sid = msg.sid << 5;
    int_sid = int_sid & (0b1111100000000000);
    int_sid = int_sid | ((msg.sid << 2) & (0b0000000011111100));

    if (!C1TX0CONbits.TXREQ) {
        C1TX0SID = int_sid;
        C1TX0SIDbits.TXIDE = 0;
        C1TX0SIDbits.SRR = 0;
        C1TX0DLC = 0;
        C1TX0DLCbits.DLC = msg.dlc;
        C1TX0B1 = msg.data[0];
        C1TX0B2 = msg.data[1];
        C1TX0B3 = msg.data[2];
        C1TX0B4 = msg.data[3];
        C1TX0CONbits.TXREQ = 1;
    } else if (!C1TX1CONbits.TXREQ) {
        C1TX1SID = int_sid;
        C1TX1SIDbits.TXIDE = 0;
        C1TX1SIDbits.SRR = 0;
        C1TX1DLC = 0;
        C1TX1DLCbits.DLC = msg.dlc;
        C1TX1B1 = msg.data[0];
        C1TX1B2 = msg.data[1];
        C1TX1B3 = msg.data[2];
        C1TX1B4 = msg.data[3];
        C1TX1CONbits.TXREQ = 1;
    } else if (!C1TX2CONbits.TXREQ) {
        C1TX2SID = int_sid;
        C1TX2SIDbits.TXIDE = 0;
        C1TX2SIDbits.SRR = 0;
        C1TX2DLC = 0;
        C1TX2DLCbits.DLC = msg.dlc;
        C1TX2B1 = msg.data[0];
        C1TX2B2 = msg.data[1];
        C1TX2B3 = msg.data[2];
        C1TX2B4 = msg.data[3];
        C1TX2CONbits.TXREQ = 1;

    } else {
        return -1;
    }

    return 0;
}

uint16_t can1_tx_available()
{
    return !C1TX0CONbits.TXREQ + !C1TX1CONbits.TXREQ + !C1TX2CONbits.TXREQ;
}

/**********************************************************************
 * Name:    send_can2
 * Args:    CANdata *msg
 * Return:  exit status
 * Desc:    Sends a CANdata structure through CAN 2
 **********************************************************************/
int send_can2(CANdata msg)
{

    unsigned int int_sid = 0;

    int_sid = msg.sid << 5;
    int_sid = int_sid & (0b1111100000000000);
    int_sid = int_sid | ((msg.sid << 2) & (0b0000000011111100));

    if (!C2TX0CONbits.TXREQ) {
        C2TX0SID = int_sid;
        C2TX0SIDbits.TXIDE = 0;
        C2TX0SIDbits.SRR = 0;
        C2TX0DLC = 0;
        C2TX0DLCbits.DLC = msg.dlc;
        C2TX0B1 = msg.data[0];
        C2TX0B2 = msg.data[1];
        C2TX0B3 = msg.data[2];
        C2TX0B4 = msg.data[3];
        C2TX0CONbits.TXREQ = 1;
    } else if (!C2TX1CONbits.TXREQ) {
        C2TX1SID = int_sid;
        C2TX1SIDbits.TXIDE = 0;
        C2TX1SIDbits.SRR = 0;
        C2TX1DLC = 0;
        C2TX1DLCbits.DLC = msg.dlc;
        C2TX1B1 = msg.data[0];
        C2TX1B2 = msg.data[1];
        C2TX1B3 = msg.data[2];
        C2TX1B4 = msg.data[3];
        C2TX1CONbits.TXREQ = 1;
    } else if (!C2TX2CONbits.TXREQ) {
        C2TX2SID = int_sid;
        C2TX2SIDbits.TXIDE = 0;
        C2TX2SIDbits.SRR = 0;
        C2TX2DLC = 0;
        C2TX2DLCbits.DLC = msg.dlc;
        C2TX2B1 = msg.data[0];
        C2TX2B2 = msg.data[1];
        C2TX2B3 = msg.data[2];
        C2TX2B4 = msg.data[3];
        C2TX2CONbits.TXREQ = 1;
    } else {
        return -1;
    }

    return 0;
}

uint16_t can2_tx_available()
{
    return !C2TX2CONbits.TXREQ + !C2TX1CONbits.TXREQ + !C2TX0CONbits.TXREQ;
}

bool can1_rx_empty()
{
    return CAN2_read == CAN2_write;
}
/**********************************************************************
 * Name:    pop_can1
 * Args:    void
 * Return:  CANdata msg pointer
 * Desc:	Get CAN message from CAN 1 buffer
 *			Returns Null if there aren't any messages available
 **********************************************************************/
CANdata* pop_can1(void)
{
    if (CAN1_read == CAN1_write) {
        return NULL;
    }

    int aux = CAN1_read;
    CAN1_read = (CAN1_read + 1) % CAN1_BUFFER_LENGTH;

    return &CAN1_buffer[aux];
}

bool can2_rx_empty()
{
    return CAN2_read == CAN2_write;
}

/**********************************************************************
 * Name:    pop_can2
 * Args:    void
 * Return:  CANdata msg pointer
 * Desc:	Get CAN message from CAN 2 buffer
 *			Returns Null if there aren't any messages available
 **********************************************************************/
CANdata* pop_can2(void)
{
    if (CAN2_read == CAN2_write) {

        return NULL;
    }

    int aux = CAN2_read;
    CAN2_read = (CAN2_read + 1) % CAN2_BUFFER_LENGTH;

    return &(CAN2_buffer[aux]);
}
/**********************************************************************
 * Name:    filter_can1
 * Args:    uint16_t sid
 * Return:  boolean
 * Desc:	Software filters for CAN 1 module
 *			Defaults to true for all message
 *			This function is defined as weak, you can and should refine it in your code to only allow the messages you need.
*			Be aware that this function will run inside a interrupt every time you receive a CAN message. General performance considerations should be taken into account.
 **********************************************************************/
bool __attribute__((weak)) filter_can1(uint16_t sid)
{
    return true;
}

/**********************************************************************
 * Name:    filter_can2
 * Args:    uint16_t sid
 * Return:  boolean
 * Desc:	Software filters for CAN 1 module
 *			Defaults to true for all message
 *			This function is defined as weak, you can and should refine it in your code to only allow the messages you need.
*			Be aware that this function will run inside a interrupt every time you receive a CAN message. General performance considerations should be taken into account.
 **********************************************************************/
bool __attribute__((weak)) filter_can2(uint16_t sid)
{
    return true;
}

/**********************************************************************
 * Assign CAN1 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv)) _C1Interrupt(void)
{

    if (C1INTFbits.RX0IF) {
        if (filter_can1(C1RX0SIDbits.SID)) {
            CAN1_buffer[CAN1_write].sid = C1RX0SIDbits.SID;
            CAN1_buffer[CAN1_write].dlc = C1RX0DLCbits.DLC;
            CAN1_buffer[CAN1_write].data[0] = C1RX0B1;
            CAN1_buffer[CAN1_write].data[1] = C1RX0B2;
            CAN1_buffer[CAN1_write].data[2] = C1RX0B3;
            CAN1_buffer[CAN1_write].data[3] = C1RX0B4;
            C1RX0CONbits.RXFUL = 0;
            C1INTFbits.RX0IF = 0;

            CAN1_write = (CAN1_write + 1) % CAN1_BUFFER_LENGTH;
        } else {
            C1RX0CONbits.RXFUL = 0;
            C1INTFbits.RX0IF = 0;
        }
    }

    if (C1INTFbits.RX1IF) {
        if (filter_can1(C1RX1SIDbits.SID)) {
            CAN1_buffer[CAN1_write].sid = C1RX1SIDbits.SID;
            CAN1_buffer[CAN1_write].dlc = C1RX1DLCbits.DLC;
            CAN1_buffer[CAN1_write].data[0] = C1RX1B2;
            CAN1_buffer[CAN1_write].data[1] = C1RX1B2;
            CAN1_buffer[CAN1_write].data[2] = C1RX1B3;
            CAN1_buffer[CAN1_write].data[3] = C1RX1B4;
            C1RX1CONbits.RXFUL = 0;
            C1INTFbits.RX1IF = 0;

            CAN1_write = (CAN1_write + 1) % CAN1_BUFFER_LENGTH;
        } else {
            C1RX1CONbits.RXFUL = 0;
            C1INTFbits.RX0IF = 0;
        }
    }

    IFS1bits.C1IF = 0;
}

/**********************************************************************
 * Assign CAN2 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv)) _C2Interrupt(void)
{

    if (C2INTFbits.RX0IF) {
        if (filter_can2(C2RX0SIDbits.SID)) {
            CAN2_buffer[CAN2_write].sid = C2RX0SIDbits.SID;
            CAN2_buffer[CAN2_write].dlc = C2RX0DLCbits.DLC;
            CAN2_buffer[CAN2_write].data[0] = C2RX0B1;
            CAN2_buffer[CAN2_write].data[1] = C2RX0B2;
            CAN2_buffer[CAN2_write].data[2] = C2RX0B3;
            CAN2_buffer[CAN2_write].data[3] = C2RX0B4;
            C2RX0CONbits.RXFUL = 0;
            C2INTFbits.RX0IF = 0;

            CAN2_write = (CAN2_write + 1) % CAN2_BUFFER_LENGTH;
        } else {
            C2INTFbits.RX0IF = 0;
            C2RX0CONbits.RXFUL = 0;
        }
    }

    if (C2INTFbits.RX1IF) {
        if (filter_can2(C2RX1SIDbits.SID)) {
            CAN2_buffer[CAN2_write].sid = C2RX1SIDbits.SID;
            CAN2_buffer[CAN2_write].dlc = C2RX1DLCbits.DLC;
            CAN2_buffer[CAN2_write].data[0] = C2RX1B1;
            CAN2_buffer[CAN2_write].data[1] = C2RX1B2;
            CAN2_buffer[CAN2_write].data[2] = C2RX1B3;
            CAN2_buffer[CAN2_write].data[3] = C2RX1B4;
            C2RX1CONbits.RXFUL = 0;
            C2INTFbits.RX1IF = 0;

            CAN2_write = (CAN2_write + 1) % CAN2_BUFFER_LENGTH;
        } else {
            C2RX1CONbits.RXFUL = 0;
            C2INTFbits.RX1IF = 0;
        }
    }

    IFS2bits.C2IF = 0;
}

/**********************************************************************
 * Name:    write_to_can1_buffer
 * Args:    CANdata CANmessage, bool priority (0 - no priority 1- prioritary message)
 * Return:  ---
 * Desc:	writes messages to the CAN1 buffers to be send by CAN2
 *			
 **********************************************************************/

void write_to_can1_buffer(CANdata CANmessage, bool priority)
{
    int ipl;
    ipl = set_CPU_priority(7);
    switch (priority) {
    case 1:
        CAN1_buffer_P1[CAN1_write_P1].sid = CANmessage.sid;
        CAN1_buffer_P1[CAN1_write_P1].dlc = CANmessage.dlc;
        CAN1_buffer_P1[CAN1_write_P1].data[0] = CANmessage.data[0];
        CAN1_buffer_P1[CAN1_write_P1].data[1] = CANmessage.data[1];
        CAN1_buffer_P1[CAN1_write_P1].data[2] = CANmessage.data[2];
        CAN1_buffer_P1[CAN1_write_P1].data[3] = CANmessage.data[3];

        /* implements circular buffer logic */
        CAN1_write_P1 = (CAN1_write_P1 + 1) % CAN_BUFFER_LENGTH_SEND;
        break;
    case 0:
        CAN1_buffer_P2[CAN1_write_P2].sid = CANmessage.sid;
        CAN1_buffer_P2[CAN1_write_P2].dlc = CANmessage.dlc;
        CAN1_buffer_P2[CAN1_write_P2].data[0] = CANmessage.data[0];
        CAN1_buffer_P2[CAN1_write_P2].data[1] = CANmessage.data[1];
        CAN1_buffer_P2[CAN1_write_P2].data[2] = CANmessage.data[2];
        CAN1_buffer_P2[CAN1_write_P2].data[3] = CANmessage.data[3];

        /* implements circular buffer logic */
        CAN1_write_P2 = (CAN1_write_P2 + 1) % CAN_BUFFER_LENGTH_SEND;
        break;
    }
    set_CPU_priority(ipl);

    return;
}

/**********************************************************************
 * Name:    write_to_can2_buffer
 * Args:    CANdata CANmessage, bool priority (0 - no priority 1- prioritary message)
 * Return:  ---
 * Desc:	writes messages to the CAN2 buffers to be send by CAN2
 *			
 **********************************************************************/

void write_to_can2_buffer(CANdata CANmessage, bool priority)
{
    int ipl;
    ipl = set_CPU_priority(7);
    switch (priority) {
    case 1:
        CAN2_buffer_P1[CAN2_write_P1].sid = CANmessage.sid;
        CAN2_buffer_P1[CAN2_write_P1].dlc = CANmessage.dlc;
        CAN2_buffer_P1[CAN2_write_P1].data[0] = CANmessage.data[0];
        CAN2_buffer_P1[CAN2_write_P1].data[1] = CANmessage.data[1];
        CAN2_buffer_P1[CAN2_write_P1].data[2] = CANmessage.data[2];
        CAN2_buffer_P1[CAN2_write_P1].data[3] = CANmessage.data[3];

        /* implements circular buffer logic */
        CAN2_write_P1 = (CAN2_write_P1 + 1) % CAN_BUFFER_LENGTH_SEND;
        break;
    case 0:
        CAN2_buffer_P2[CAN2_write_P2].sid = CANmessage.sid;
        CAN2_buffer_P2[CAN2_write_P2].dlc = CANmessage.dlc;
        CAN2_buffer_P2[CAN2_write_P2].data[0] = CANmessage.data[0];
        CAN2_buffer_P2[CAN2_write_P2].data[1] = CANmessage.data[1];
        CAN2_buffer_P2[CAN2_write_P2].data[2] = CANmessage.data[2];
        CAN2_buffer_P2[CAN2_write_P2].data[3] = CANmessage.data[3];

        /* implements circular buffer logic */
        CAN2_write_P2 = (CAN2_write_P2 + 1) % CAN_BUFFER_LENGTH_SEND;
        break;
    }

    set_CPU_priority(ipl);

    return;
}

/**********************************************************************
 * Name:    send_can1_buffer
 * Args:    --
 * Return:  ---
 * Desc:	sends one message from one of the buffers
 *			only sends from buffer 2 is buffer 1 is empty
 **********************************************************************/

void send_can1_buffer()
{
    uint16_t err;

    if (CAN1_write_P1 != CAN1_send_P1) {
        if (CAN1_send_P1 >= CAN_BUFFER_LENGTH_SEND) /*I think this does nothing but just in case*/
            CAN1_send_P1 = 0;

        err = send_can1(CAN1_buffer_P1[CAN1_send_P1]);
        if (err == 0) {
            /* implements circular buffer logic */
            CAN1_send_P1 = (CAN1_send_P1 + 1) % CAN_BUFFER_LENGTH_SEND;
        }
    } else {
        if (CAN1_write_P2 != CAN1_send_P2) {
            if (CAN1_send_P2 >= CAN_BUFFER_LENGTH_SEND) /*I think this does nothing but just in case*/
                CAN1_send_P2 = 0;

            err = send_can1(CAN1_buffer_P2[CAN1_send_P2]);
            if (err == 0) {
                /* implements circular buffer logic */
                CAN1_send_P2 = (CAN1_send_P2 + 1) % CAN_BUFFER_LENGTH_SEND;
            }
        }
    }

    return;
}

/**********************************************************************
 * Name:    send_can2_buffer
 * Args:    --
 * Return:  ---
 * Desc:	sends one message from one of the buffers
 *			only sends from buffer 2 is buffer 1 is empty
 **********************************************************************/

void send_can2_buffer()
{
    uint16_t err;

    if (CAN2_write_P1 != CAN2_send_P1) {
        if (CAN2_send_P1 >= CAN_BUFFER_LENGTH_SEND) /*I think this does nothing but just in case*/
            CAN2_send_P1 = 0;

        err = send_can2(CAN2_buffer_P1[CAN2_send_P1]);
        if (err == 0) {
            /* implements circular buffer logic */
            CAN2_send_P1 = (CAN2_send_P1 + 1) % CAN_BUFFER_LENGTH_SEND;
        }

    } else {
        if (CAN2_write_P2 != CAN2_send_P2) {
            if (CAN2_send_P2 >= CAN_BUFFER_LENGTH_SEND) /*I think this does nothing but just in case*/
                CAN2_send_P2 = 0;

            err = send_can2(CAN2_buffer_P2[CAN2_send_P2]);

            if (err == 0) {
                /* implements circular buffer logic */
                CAN2_send_P2 = (CAN2_send_P2 + 1) % CAN_BUFFER_LENGTH_SEND;
            }
        }
    }

    return;
}

/**********************************************************************
 * Name:	set_CPU_priority
 * Args:    unsigned int n
 * Return:	previous priority OR error code
 * Desc:	Sets CPU priority to n, provided it is between 0 and 7.
 *          Provides a way to disable interruptions in critical parts
 *          of the code.
 *          Needs to be set to 6 or lower to allow interruptions,
 *          depending on interruptions' priorities.
 *          Previous priority returned for restoring purposes.
 **********************************************************************/
int set_CPU_priority(unsigned int n)
{

    unsigned int prevIPL;

    if (n <= 7) {
        prevIPL = SRbits.IPL;
        SRbits.IPL = n; /* Status Register . CPU priority 0-7 */
        return prevIPL;
    }
    return -1;
}
