#ifndef __FLASH_H__
#define __FLASH_H__

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

uint16_t read_flash(uint16_t *buf,uint16_t offset, uint16_t len);
uint16_t write_flash(uint16_t *buf,uint16_t offset, uint16_t len);

#endif
