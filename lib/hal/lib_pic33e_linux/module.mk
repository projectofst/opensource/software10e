MODULE_C_SOURCES:=SPI.c
MODULE_C_SOURCES+=adc.c
MODULE_C_SOURCES+=can.c
MODULE_C_SOURCES+=external.c
MODULE_C_SOURCES+=flash.c
MODULE_C_SOURCES+=io.c
MODULE_C_SOURCES+=json.c
MODULE_C_SOURCES+=pwm.c
MODULE_C_SOURCES+=timer.c
MODULE_C_SOURCES+=timing.c
MODULE_C_SOURCES+=trap.c
MODULE_C_SOURCES+=utils.c
MODULE_C_SOURCES+=version.c
MODULE_C_SOURCES+=vio.c
MODULE_C_SOURCES+=sts_queue/sts_queue.c


PWD:=lib/lib_pic33e_linux/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
