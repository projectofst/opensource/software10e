#ifndef _SPI_H_
#define _SPI_H_

#include "xc.h"
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>


/* KEEP IN MIND: Select (PPS) feature, the SCKx pin must
be mapped as both input and output in Master mode. */

/**
 * @brief Data to send during receives
 */
#define SPI_DUMMY_DATA 0xFF

/**
 * @brief Number of FIFO positions
 */
#define SPI_FIFO_FILL_LIMIT 8

/**
 * @brief Modes of operation for SPI
 */
typedef enum {
    SPI_TRANSFER_MODE_8BIT   = 0,
    SPI_TRANSFER_MODE_16BIT  = 1,
} SPI_TRANSFER_MODE;

typedef enum {
    SPI_DATA_SAMPLE_PHASE_MIDDLE = 0,
    SPI_DATA_SAMPLE_PHASE_END = 1,
} SPI_DATA_SAMPLE_PHASE;

typedef enum {
    SPI_MODE_0 = 0,
    SPI_MODE_1 = 1,
    SPI_MODE_2 = 2,
    SPI_MODE_3 = 3,
} SPI_MODE;

/**
 * @brief Structure to pass to SPI_initialize.
 */
typedef struct SPI_PERIPHERAL_CONFIGURATION{
    bool enabled :1;    // Choose whether to start enabled or not
    bool stop_in_idle :1;
    bool disable_SCK_pin    :1;
    bool disable_SDO_pin    :1;
    SPI_TRANSFER_MODE transfer_mode  :1;
    SPI_DATA_SAMPLE_PHASE data_sample_phase :1;
    SPI_MODE SPI_mode :2;
    uint8_t enable_SS_pin  :1;
    bool enable_master_mode :1;
    uint32_t baudrate;
}SPI_Peripheral_Configuration;

/* Interface */
uint32_t config_SPI1(SPI_Peripheral_Configuration config);
uint32_t config_SPI2(SPI_Peripheral_Configuration config);
uint32_t config_SPI3(SPI_Peripheral_Configuration config);
uint32_t config_SPI4(SPI_Peripheral_Configuration config);
void SPI1_enable_module();
void SPI2_enable_module();
void SPI3_enable_module();
void SPI4_enable_module();
void SPI1_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData );
void SPI2_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData );
void SPI3_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData );
void SPI4_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData );
uint16_t SPI1_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData);
uint16_t SPI2_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData);
uint16_t SPI3_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData);
uint16_t SPI4_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData);
#endif
