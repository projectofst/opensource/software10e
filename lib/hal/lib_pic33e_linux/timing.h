#ifndef __TIMING_H__
#define __TIMING_H__

#ifndef __delay_ms
#define __delay_ms(d) do {} while(0);
#endif

#ifndef __delay_us
#define __delay_us(d) do {} while(0);
#endif

#endif
