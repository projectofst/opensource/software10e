#ifndef NOTRAP

#include "trap.h"

/*
 * The handler is defined as weak so it can be defined outside the library
 */
void __attribute__((weak)) trap_handler(TrapType type)
{
    return;
}

#endif
