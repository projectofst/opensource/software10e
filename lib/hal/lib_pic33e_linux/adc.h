#ifndef __ADC_H__
#define __ADC_H__

#include <stdint.h>
#include "xc.h"

typedef enum {MANUAL, AUTO} Sampling;    
typedef enum {SAMP, INT0, TIMER3, PWM_P, TIMER5, PWM_S, AUTO_CONV=7} SSRC;
typedef enum {INTERNAL=0, POSITIVE_EXTERNAL, NEGATIVE_EXTERNAL, EXTERNAL} Voltage;
enum {AN0=1, AN1=2, AN2=4, AN3=8, AN4=16, AN5=32, AN6=64, AN7=128, AN8=256, AN9=512, 
                AN10=1024, AN11=2048, AN12=4096, AN13=8192, AN14=16384, AN15=32768, ALL=65535};
typedef enum
{
	TOO_FAST_ADC =0, 		//Max 100kHz
	TOO_SLOW_ADC =1,
	POINTER_NULL_ADC =2,
	WRONG_CLOCK_ADC =3, 	//belongs to [16,106]
	SUCESS_ADC =4 			//congrats
} Pin_Cycling_Error; 

typedef struct {                       /* refer to the family reference for more detail                                             */
    unsigned int idle :1;              /* work in idle? 1=yes 0=no                                                                  */
    unsigned int form :2;              /* 0- Integer 1- Signed integer 2- Fractional 3- Signed fractional:                          */
                                 
    SSRC conversion_trigger :3;        /* defines how to trigger the conversion: SAMP=clearing samp bit                             */
                                       /* INT0=transition of INT0, TIMER=timer 3,                                                   */
                                       /* PWM=Motor Control PWM, AUTO_CONV=auto-convert                                             */
    Sampling sampling_type :1;         /* write MANUAL or AUTO to select sampling metode                                            */
    Voltage voltage_ref :2;            /* INTERNAL- AVdd>AVss, POSITIVE_EXTERNAL- External Vref(+)>Avss                             */
                                       /* NEGATIVE_EXTERNAL- AVdd> External Vref(-), EXTERNAL- External Vref(+)>External Vref(-)    */
    Sampling sample_pin_select_type :1;/* AUTO to alternate between all the input ports,                                            */
                                       /* MANUAL to chose the port you want to sample every time                                    */
    unsigned int smpi :4;              /* number of samples per interrupt 0-> 1 sample ... 15-> 16 samples                          */
    unsigned int pin_select :16;       /* 16 bit mask.  Select ANx for input scan                                                    */
                                       /* 0- Skip ANx for input scan                                                                */
                    
    unsigned int auto_sample_time;
    unsigned int conversion_time;
                            
    unsigned int interrupt_priority :3;/* AD conversion complete interrupt priority, put 0 if no interrupt is needed                */
    unsigned int turn_on :1;           /*set to 1- if you want to turn de ADC ON, 0- otherwise                                      */
    //char port;                         // B OR E - select port where analog pins are situated, FOR NOW USE ONLY B!!!!!!!!!

} ADC_parameters;

void config_adc1(ADC_parameters parameter);
void adc_callback(void);
unsigned int get_adc_value(unsigned int sample_number);
void turn_on_adc();
void turn_off_adc();
void start_samp();
void start_conversion();
void wait_for_done();
void set_default_parameters(ADC_parameters *parameters);
void select_input_pin(unsigned int pin); /*write only the number of the pin ex: for AN0 write 0*/
Pin_Cycling_Error config_pin_cycling(uint16_t pin_vector, uint32_t frequency, 
	uint32_t sys_clock, unsigned int priority, ADC_parameters *parameters);
uint16_t get_pin_cycling_values(uint16_t *value, uint16_t position, 
    uint16_t length);
//void config_adc (void);


uint16_t get_adcbuf0();
uint16_t get_adcbuf1();
uint16_t get_adcbuf2();
uint16_t get_adcbuf3();
uint16_t get_adcbuf4();
uint16_t get_adcbuf5();
#endif
