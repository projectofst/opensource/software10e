/*
 * lib_pic33e_linux is a HAL for the dspic33e series of PIC microcontrollers.
 * Copyright © 2021 Projecto FST
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*
  * The implementation of fake can is based on a virtual socket can.
  *
  * vcan setup:
  * sudo modprobe can
  * sudo modprobe vcan
  *
  * sudo ip link add dev vcan0 type vcan
  * sudo ip link set up vcan0
  * sudo ip link add dev vcan1 type vcan
  * sudo ip link set up vcan1
  */


#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <sys/ioctl.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <config.h>

#include "can.h"
#include "json.h"
#include "shared/fifo/fifo.h"

#define MAXLINE 1024


int sock_can1;
int sock_can2;

struct sockaddr_can addr;
struct ifreq ifr;

#define CAN1_SIZE 64
#define CAN2_SIZE 64

Fifo _fifo1;
Fifo* fifo1 = &_fifo1;
Fifo _fifo2;
Fifo* fifo2 = &_fifo2;

CANdata msgs1[CAN1_SIZE];
CANdata msgs2[CAN2_SIZE];

CANerror default_can_config(CANconfig* config)
{
    return SUCCESS;
}

CANerror send_can2(CANdata msg)
{
	struct can_frame frame;
    frame.can_id = msg.sid;
	frame.can_dlc = msg.dlc;
    memcpy(frame.data, msg.data, 8);


	if (write(sock_can2, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
		perror("Write");
		return 1;
	}

    return 0;
}

CANerror send_can1(CANdata msg)
{
	struct can_frame frame;
    frame.can_id = msg.sid;
	frame.can_dlc = msg.dlc;
    memcpy(frame.data, msg.data, 8);


	if (write(sock_can1, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
		perror("Write");
		return 1;
	}

}

CANdata pop_can1()
{
    return *pop_fifo(fifo1);
}

CANdata pop_can2()
{
    return *pop_fifo(fifo2);
}

bool can1_rx_empty()
{
    return fifo_empty(fifo1);
}

bool can2_rx_empty()
{
    return fifo_empty(fifo2);
}

bool __attribute__((weak)) filter_can1(unsigned int sid)
{
    return 1;
}

bool __attribute__((weak)) filter_can2(unsigned int sid)
{
    return 1;
}

void* recv_can1(void* ptr)
{

	struct can_frame frame;
    while (1) {
	    int nbytes = read(sock_can1, &frame, sizeof(struct can_frame));

 	    if (nbytes < 0) {
		    perror("Read");
		    return 1;
	    }

        if (filter_can1(frame.can_id)) {
            CANdata msg = {
                .sid = frame.can_id,
                .dlc = frame.can_dlc,
                .data = {0,0,0,0},
            };

            memcpy(msg.data, frame.data, 8);
            append_fifo(fifo1, msg);
        }
    }
}

CANerror config_can1(CANconfig* config)
{
    if ((sock_can1 = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("Socket");
        return 1;
    }

	strcpy(ifr.ifr_name, "vcan1" );
	ioctl(sock_can1, SIOCGIFINDEX, &ifr);
	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(sock_can1, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}

    fifo_init(fifo1, msgs1, CAN1_SIZE);
    pthread_t thread1;
    pthread_create(&thread1, NULL, recv_can1, NULL);

    return 0;
}

void* recv_can2(void* ptr)
{

	struct can_frame frame;
    while (1) {
	    int nbytes = read(sock_can2, &frame, sizeof(struct can_frame));

 	    if (nbytes < 0) {
		    perror("Read");
		    return 1;
	    }

        if (filter_can2(frame.can_id)) {
            CANdata msg = {
                .sid = frame.can_id,
                .dlc = frame.can_dlc,
                .data = {0,0,0,0},
            };

            memcpy(msg.data, frame.data, 8);
            append_fifo(fifo2, msg);
        }
    }
}

CANerror config_can2(CANconfig* config)
{

    if ((sock_can2 = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("Socket");
        return 1;
    }

	strcpy(ifr.ifr_name, "vcan0" );
	ioctl(sock_can2, SIOCGIFINDEX, &ifr);
	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(sock_can2, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}

    fifo_init(fifo2, msgs2, CAN1_SIZE);
    pthread_t thread1;
    pthread_create(&thread1, NULL, recv_can2, NULL);

    return 0;
}

uint16_t can1_tx_available()
{
    return 8;
}

uint16_t can2_tx_available()
{
    return 8;
}
