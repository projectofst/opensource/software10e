#include "flash.h"

#include <stdio.h>
#include <stdlib.h>

#define FILENAME "flash.txt"
#define ASCII_BYTES 6

uint16_t read_flash(uint16_t* buf, uint16_t offset, uint16_t len)
{

    FILE* f;
    int i, ret;

    if ((f = fopen(FILENAME, "r")) == NULL) {
        printf("Error opening flash \n");
        for (int i = 0; i<len; i++) {
            buf[i+offset] = 0xFFFF;
        }
        return 1;
    }

    fseek(f, ASCII_BYTES * offset, 0);

    for (i = 0; i < len; i++) {
        ret = fscanf(f, "%5hd\n", &buf[i]);
        printf("reading, %5d\n", buf[i]);
    }
    return fclose(f);
}

uint16_t write_flash(uint16_t* buf, uint16_t offset, uint16_t len)
{

    FILE* f;
    int i;

    if ((f = fopen(FILENAME, "w+")) == NULL) {
        printf("Error opening flash \n");
        return 1;
    }

    fseek(f, ASCII_BYTES * offset, 0);

    for (i = 0; i < len; i++) {
        fprintf(f, "%5d\n", buf[i]);
        printf("writing, %5d\n", buf[i]);
    }
    return fclose(f);
}
