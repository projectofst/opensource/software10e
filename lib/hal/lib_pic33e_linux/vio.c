#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "Queue/Queue.h"
#include "sts_queue/sts_queue.h"


#include "vio.h"

Connection vio_open_conn(const char *host, unsigned port) {
  Connection *conn = (Connection *) malloc(sizeof(Connection));
  conn->socket = 0;

  char *hello = "ola\n";

  //Create a  queue for INPUT / ADC
  StsHeader *head_input = StsQueue.create();
  conn->input = head_input;

  StsHeader *head_adc = StsQueue.create();
  conn->adc = head_adc;  


  if ((conn->socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    perror("Error(vio) socket creation failed\n");
    exit(EXIT_FAILURE);
  }

  memset(&conn->addr, 0, sizeof(conn->addr));

  conn->addr.sin_family = AF_INET;
  conn->addr.sin_port = htons(port);
  conn->addr.sin_addr.s_addr = INADDR_ANY;
  // inet_pton(AF_INET, host, &(conn.addr.sin_addr));

  size_t n = sendto(conn->socket, (const char *)hello, strlen(hello), 0,
                    (const struct sockaddr *)&(conn->addr), sizeof(conn->addr));

  pthread_t thread;
	pthread_create(&thread, NULL, vio_recv_msg, conn);

//Creating a new thread for vio_recv_msg
  return *conn;
}

void *vio_recv_msg(void *_conn) 
{
  Connection conn = *((Connection *) _conn);
  Vio *vio;
  char buffer[VIO_BUFFER_MAX] = {0};

  socklen_t len;
  while(true){

    int n = recvfrom(conn.socket, (char *)buffer, VIO_BUFFER_MAX, MSG_WAITALL,
                    (struct sockaddr *)&(conn.addr), &len);
    if (n == -1) {
      printf("Error: %d %s\n", errno, strerror(errno));
      return NULL;
    
    }


    //printf("n: %d\n", n);
    vio = vio_unpack(buffer, n);   

  //Verifies if it is ADC
    if(buffer[0] == ADC){

    //printf("ADC");
    StsQueue.push(conn.adc, vio);

    } 

    //Verifies if it is input
    if(buffer[0] == INPUT){
      //printf("INPUT");
      StsQueue.push(conn.input, vio);
    }

  }

    return NULL;
 }

Vio *vio_unpack(unsigned char *buffer, int  n){
  
  /*printf("buffer: ");
  for(int i=0; i<n; i++){
  printf("%d/", buffer[i]);
  }
  printf("\n");
  */
 //printf("entrei no vio unpack\n");

  Vio *vio = (Vio *) malloc(sizeof(Vio));
  vio->type = buffer[0];
  //printf("\n %d", buffer[0]);
  

  if(vio->type == ADC)
  {
    //printf("entrei");
    vio->value = (int)buffer[4]+((int)buffer[3]<<8)+((int)buffer[2]<<16)+((int)buffer[1]<<24);
    vio->len = (int)buffer[5]+1;
    vio->id = (char *) malloc(sizeof(char)*vio->len);
    strncpy(vio->id, buffer + 6, vio->len - 1);   
    vio->id[vio->len - 1]= '\0';
    //printf("value ADC:%d \n", vio->value);
    
  } 

  if(vio->type == INPUT)
  {
     /* printf("buffer: ");
      for(int i=0; i<n; i++){
      printf("%d/", buffer[i]);
      }*/
     // printf("\n");

    vio->value = (int)buffer[1];
    //vio->len = (int)buffer[2]+1;
    //vio->id = (char *) malloc(sizeof(char)*vio->len);
    vio->pin = (int)buffer[2];
    vio->group = (char)buffer[3];
   // printf("pin: %d, group: %c value: %d\n", vio->pin, vio->group, vio->value);
  }
 // printf("sai");
  return vio;
}





