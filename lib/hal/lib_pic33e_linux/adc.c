#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <unistd.h>

#include <config.h>

#include "vio.h"
#include "lib_pic33e/io.h"
#include "adc.h"
#include "timing.h"
#include "sts_queue/sts_queue.h"


typedef struct _vio_adc_channel_t {
    const char* name;
    uint16_t value;
} vio_adc_channel_t;

vio_adc_channel_t* adc1_channels;
vio_adc_channel_t* adc2_channels;

vio_adc_channel_t* vio_adc_find_channel(vio_adc_channel_t* adc_channels,
    const char* name)
{
    for (int i = 0; adc_channels[i].name != NULL; i++) {
        if (strcmp(adc_channels[i].name, name) == 0) {
            return adc_channels + i;
        }
    }

    return NULL;
}

void *protobuf_handler(void *_conn) {
	Connection conn = *((Connection *) _conn);
	
	char *buffer = NULL;
	Vio *vio_aux = NULL;
	Vio *vio = NULL;
	
	
	printf("Start protobuf handler\n");
	while (true) {
		if((vio_aux = StsQueue.pop(conn.adc)) != NULL) {
			vio = vio_aux;
		}
		if((vio_aux = StsQueue.pop(conn.input)) != NULL) {
			vio = vio_aux;
  		}
		//printf("Received adc \n");
		if ((vio != NULL)&&(vio->type == ADC)) {
			//printf("adc_id: %s\n", vio->id);
			vio_adc_channel_t *aux = vio_adc_find_channel(adc1_channels, vio->id);
			if (aux == NULL) {
				continue;
			}
			aux->value = vio->value;	
			adc_callback();	
		}

		if ((vio != NULL)&&(vio->type == INPUT)){
			set_port_fake(&(vio->group), vio->pin, vio->value);
		} 
		
	
	}

} 	

/**********************************************************************
 * Name:    config_adc
 * Args:    ADC_parameters filled with the configuracions for the adc module
 * Return:  -
 * Desc:    Configures adc module
 **********************************************************************/
void config_adc1(ADC_parameters parameter)
{
    vio_adc_channel_t aux_adc_channels[] = VIO_ADC1_CONFIG;
    int size = 0;
    for (size = 0; aux_adc_channels[size].name != NULL; size++)
        ;

    adc1_channels = (vio_adc_channel_t*)malloc(sizeof(vio_adc_channel_t) * size);
    memcpy(adc1_channels, aux_adc_channels, size * sizeof(vio_adc_channel_t));

    Connection* conn = (Connection*)malloc(sizeof(Connection));
    *conn = vio_open_conn(VIO_HOST, VIO_PORT);
    pthread_t thread;
    pthread_create(&thread, NULL, protobuf_handler, conn);
    return;
}

void __attribute__((weak)) adc_callback(void)
{
    return;
}

unsigned int get_adc_value(unsigned int sample_number)
{
    return adc1_channels[0].value;
}

void turn_on_adc()
{
    return;
}

void turn_off_adc()
{
    return;
}

void start_samp()
{
    return;
}

void start_conversion()
{
    return;
}

void wait_for_done()
{
    return;
}

void set_default_parameters(ADC_parameters* parameters)
{
    return;
}

void select_input_pin(unsigned int pin)
{
    return;
}

Pin_Cycling_Error config_pin_cycling(uint16_t pin_vector, uint32_t frequency,
    uint32_t sys_clock, unsigned int priority, ADC_parameters* parameters)
{
    return 0;
}
uint16_t get_pin_cycling_values(uint16_t* value, uint16_t position,
    uint16_t length)
{

    for (int i = 0; i < length; i++) {
        if (adc1_channels[position + i].name == NULL) {
            printf("Error: reached end of adc1_channels\n");
            return -1;
        }

        value[i] = adc1_channels[position + i].value;
        //printf("name[%d] = %s\n", i, adc1_channels[i].name);
        //printf("value[%d] = %d\n", i, adc1_channels[i].value);
    }

    return 0;
}


uint16_t get_adcbuf0() {
	return adc1_channels[0].value;		
}


uint16_t get_adcbuf1() {
	return adc1_channels[1].value;		
}


uint16_t get_adcbuf2() {
	return adc1_channels[2].value;		
}


uint16_t get_adcbuf3() {
	return adc1_channels[3].value;		
}


uint16_t get_adcbuf4() {
	return adc1_channels[4].value;		
}


uint16_t get_adcbuf5() {
	return adc1_channels[5].value;		
}