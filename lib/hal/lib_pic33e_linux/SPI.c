#ifndef NOSPI

#include "xc.h"
#include <stdlib.h>

#include "SPI.h"
#include "timing.h"

#pragma GCC diagnostic ignored "-Wall"

inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI1_TransferModeGet(void);
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI2_TransferModeGet(void);
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI3_TransferModeGet(void);
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI4_TransferModeGet(void);

/**********************************************************************
 * Name:    get_prescales
 * Args:    desired baudrate, *primary prescale, *secondary prescale
 * Return:  best baudrate you can get for the defined FCY
 * Desc:    calculates the prescales that will get you the closest baudrate
 **********************************************************************/
uint32_t get_prescales(uint32_t baudrate, uint8_t* p_presc, uint8_t* s_presc)
{

    return 0;
}

/**********************************************************************
 * Name:    config_SPI1
 * Args:    SPI_Peripheral_Configuration config - configuration struct
 * Return:  best baudrate you can get for the defined FCY
 * Desc:    config SPI module
 **********************************************************************/
uint32_t config_SPI1(SPI_Peripheral_Configuration config)
{

    return 0;
}

/**********************************************************************
 * Name:    config_SPI2
 * Args:    SPI_Peripheral_Configuration config - configuration struct
 * Return:  best baudrate you can get for the defined FCY
 * Desc:    config SPI module
 **********************************************************************/
uint32_t config_SPI2(SPI_Peripheral_Configuration config)
{

    return 0;
}

/**********************************************************************
 * Name:    config_SPI3
 * Args:    SPI_Peripheral_Configuration config - configuration struct
 * Return:  best baudrate you can get for the defined FCY
 * Desc:    config SPI module
 **********************************************************************/
uint32_t config_SPI3(SPI_Peripheral_Configuration config)
{

    return 0;
}

/**********************************************************************
 * Name:    config_SPI4
 * Args:    SPI_Peripheral_Configuration config - configuration struct
 * Return:  best baudrate you can get for the defined FCY
 * Desc:    config SPI module
 **********************************************************************/
uint32_t config_SPI4(SPI_Peripheral_Configuration config)
{

    return 0;
}

/**********************************************************************
 * Name:    SPI1_enable_module
 * Args:    -
 * Return:  -
 * Desc:    enables SPI1 module
 **********************************************************************/
void SPI1_enable_module()
{

    return;
}

/**********************************************************************
 * Name:    SPI2_enable_module
 * Args:    -
 * Return:  -
 * Desc:    enables SPI2 module
 **********************************************************************/
void SPI2_enable_module()
{

    return;
}

/**********************************************************************
 * Name:    SPI3_enable_module
 * Args:    -
 * Return:  -
 * Desc:    enables SPI3 module
 **********************************************************************/
void SPI3_enable_module()
{

    return;
}

/**********************************************************************
 * Name:    SPI4_enable_module
 * Args:    -
 * Return:  -
 * Desc:    enables SPI4 module
 **********************************************************************/
void SPI4_enable_module()
{

    return;
}

/**
 * @brief Exchange one element of data. Transmit or receive
 *
 * @param pTransmitData Data element to send. NULL when receiving
 * only.
 * @param pReceiveData  Data element to receive. NULL when
 * transmitting only.
 */
void SPI1_Exchange(uint8_t* pTransmitData, uint8_t* pReceiveData)
{

    return;
}

/**
 * @brief Exchange one element of data. Transmit or receive
 *
 * @param pTransmitData Data element to send. NULL when receiving
 * only.
 * @param pReceiveData  Data element to receive. NULL when
 * transmitting only.
 */
void SPI2_Exchange(uint8_t* pTransmitData, uint8_t* pReceiveData)
{

    return;
}

/**
 * @brief Exchange one element of data. Transmit or receive
 *
 * @param pTransmitData Data element to send. NULL when receiving
 * only.
 * @param pReceiveData  Data element to receive. NULL when
 * transmitting only.
 */
void SPI3_Exchange(uint8_t* pTransmitData, uint8_t* pReceiveData)
{

    return;
}

/**
 * @brief Exchange one element of data. Transmit or receive
 *
 * @param pTransmitData Data element to send. NULL when receiving
 * only.
 * @param pReceiveData  Data element to receive. NULL when
 * transmitting only.
 */
void SPI4_Exchange(uint8_t* pTransmitData, uint8_t* pReceiveData)
{

    return;
}

/**
 * @brief Exchange an entire buffer of data. Either transmit or
 * receive.
 *
 * @param pTransmitData Data array to send. NULL when receiving data
 * only.
 * @param byteCount     Number of bytes in the array.
 * @param pReceiveData  Array to receive the data. NULL when
 * transmitting data only.
 *
 * @return Number of data elements transmitted
 */
uint16_t SPI1_ExchangeBuffer(uint8_t* pTransmitData, uint16_t byteCount, uint8_t* pReceiveData)
{

    return 0;
}

/**
 * @brief Exchange an entire buffer of data. Either transmit or
 * receive.
 *
 * @param pTransmitData Data array to send. NULL when receiving data
 * only.
 * @param byteCount     Number of bytes in the array.
 * @param pReceiveData  Array to receive the data. NULL when
 * transmitting data only.
 *
 * @return Number of data elements transmitted
 */
uint16_t SPI2_ExchangeBuffer(uint8_t* pTransmitData, uint16_t byteCount, uint8_t* pReceiveData)
{

    return 0;
}
/**
 * @brief Exchange an entire buffer of data. Either transmit or
 * receive.
 *
 * @param pTransmitData Data array to send. NULL when receiving data
 * only.
 * @param byteCount     Number of bytes in the array.
 * @param pReceiveData  Array to receive the data. NULL when
 * transmitting data only.
 *
 * @return Number of data elements transmitted
 */
uint16_t SPI3_ExchangeBuffer(uint8_t* pTransmitData, uint16_t byteCount, uint8_t* pReceiveData)
{

    return 0;
}
/**
 * @brief Exchange an entire buffer of data. Either transmit or
 * receive.
 *
 * @param pTransmitData Data array to send. NULL when receiving data
 * only.
 * @param byteCount     Number of bytes in the array.
 * @param pReceiveData  Array to receive the data. NULL when
 * transmitting data only.
 *
 * @return Number of data elements transmitted
 */
uint16_t SPI4_ExchangeBuffer(uint8_t* pTransmitData, uint16_t byteCount, uint8_t* pReceiveData)
{

    return 0;
}
/**
 * @brief Get SPI1 transfer mode
 *
 */
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI1_TransferModeGet(void)
{

    return 0;
}

/**
 * @brief Get SPI1 transfer mode
 *
 */
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI2_TransferModeGet(void)
{
    return 0;
}

/**
 * @brief Get SPI1 transfer mode
 *
 */
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI3_TransferModeGet(void)
{
    return 0;
}

/**
 * @brief Get SPI1 transfer mode
 *
 */
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI4_TransferModeGet(void)
{
    return 0;
}
#endif
