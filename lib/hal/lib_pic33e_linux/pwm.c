
#include "pwm.h"
#include <stdlib.h>
#include <stdio.h>




//=====================================OC1===============================================
int pwm1_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    //printf("pwm1: %f %f\n", duty_cycle, period);
    
    return 0;
}

int pwm1_update_duty_cycle(float duty_cycle){
    //printf("%f\n", duty_cycle);
    fflush(stdout);
    
    return 0;
}

void pwm1_stop( void ){
    return;
}

void pwm1_start( void ){
    return;
}



//=====================================OC2===============================================
int pwm2_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    //printf("pwm2: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm2_update_duty_cycle(float duty_cycle){
    //printf("pwm2: %f\n", duty_cycle);
    
    return 0;
}

void pwm2_stop( void ){
    return;
}

void pwm2_start( void ){
    return;
}


//=====================================OC3===============================================

int pwm3_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    //printf("pwm3: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm3_update_duty_cycle(float duty_cycle){
    //printf("pwm3: %f\n", duty_cycle);
    
    return 0;
}

void pwm3_stop( void ){
    return;
}

void pwm3_start( void ){
    return;
}

//=====================================OC4===============================================

int pwm4_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm4: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm4_update_duty_cycle(float duty_cycle){
    printf("pwm4: %f\n", duty_cycle);
    
    return 0;
}

void pwm4_stop( void ){
    return;
}

void pwm4_start( void ){
    return;
}


//=====================================OC5===============================================

int pwm5_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm5: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm5_update_duty_cycle(float duty_cycle){
    printf("pwm5: %f\n", duty_cycle);
    
    return 0;
}

void pwm5_stop( void ){
    return;
}

void pwm5_start( void ){
    return;
}


//=====================================OC6===============================================

int pwm6_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm6: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm6_update_duty_cycle(float duty_cycle){
    printf("pwm6: %f\n", duty_cycle);
    
    return 0;
}

void pwm6_stop( void ){
    return;
}

void pwm6_start( void ){
    return;
}


//=====================================OC7===============================================

int pwm7_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm7: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm7_update_duty_cycle(float duty_cycle){
    printf("pwm7: %f\n", duty_cycle);
    
    return 0;
}

void pwm7_stop( void ){
    return;
}

void pwm7_start( void ){
    return;
}


//=====================================OC8===============================================

int pwm8_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm8: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm8_update_duty_cycle(float duty_cycle){
    printf("pwm8: %f\n", duty_cycle);
    
    return 0;
}

void pwm8_stop( void ){
    return;
}

void pwm8_start( void ){
    return;
}


//=====================================OC9===============================================

int pwm9_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm9: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm9_update_duty_cycle(float duty_cycle){
    printf("pwm9: %f\n", duty_cycle);
    
    return 0;
}

void pwm9_stop( void ){
    return;
}

void pwm9_start( void ){
    return;
}


//=====================================OC10===============================================

int pwm10_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm10: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm10_update_duty_cycle(float duty_cycle){
    printf("pwm10: %f\n", duty_cycle);
    
    return 0;
}

void pwm10_stop( void ){
    return;
}

void pwm10_start( void ){
    return;
}


//=====================================OC11===============================================

int pwm11_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm11: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm11_update_duty_cycle(float duty_cycle){
    printf("pwm11: %f\n", duty_cycle);
    
    return 0;
}

void pwm11_stop( void ){
    return;
}

void pwm11_start( void ){
    return;
}


//=====================================OC12===============================================

int pwm12_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm12: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm12_update_duty_cycle(float duty_cycle){
    printf("pwm12: %f\n", duty_cycle);
    
    return 0;
}

void pwm12_stop( void ){
    return;
}

void pwm12_start( void ){
    return;
}


//=====================================OC13===============================================

int pwm13_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm13: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm13_update_duty_cycle(float duty_cycle){
    printf("pwm13: %f\n", duty_cycle);
    
    return 0;
}

void pwm13_stop( void ){
    return;
}

void pwm13_start( void ){
    return;
}


//=====================================OC14===============================================

int pwm14_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm14: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm14_update_duty_cycle(float duty_cycle){
    printf("pwm14: %f\n", duty_cycle);
    
    return 0;
}

void pwm14_stop( void ){
    return;
}

void pwm14_start( void ){
    return;
}


//=====================================OC15===============================================

int pwm15_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm15: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm15_update_duty_cycle(float duty_cycle){
    printf("pwm15: %f\n", duty_cycle);
    
    return 0;
}

void pwm15_stop( void ){
    return;
}

void pwm15_start( void ){
    return;
}


//=====================================OC16===============================================

int pwm16_config (float duty_cycle, float period, pwm_clock_source_t pwm_clock_source)
{
    printf("pwm16: %f %f\n", duty_cycle, period);

    return 0;
}

int pwm16_update_duty_cycle(float duty_cycle){
    printf("pwm16: %f\n", duty_cycle);
    
    return 0;
}

void pwm16_stop( void ){
    return;
}

void pwm16_start( void ){
    return;
}
 
