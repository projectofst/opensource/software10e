/*
 * lib_pic33e_linux is a HAL for the dspic33e series of PIC microcontrollers.
 * Copyright © 2021 Projecto FST
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __CAN_H__
#define __CAN_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#ifndef _CANDATA
#define _CANDATA
typedef struct {
	union {
		struct {
			uint16_t dev_id:5; // Least significant
			uint16_t msg_id:6; // Most significant
		};
		uint16_t sid;
	};
	uint8_t dlc:4;
	uint16_t data[4];
} CANdata;
#endif

typedef struct _CANconfig {
	unsigned long baudrate;
	unsigned sync_jump_width:2;
	unsigned prop_seg:3;
	unsigned phase1_seg:3;
	unsigned phase2_seg:3;
	unsigned triple_sampling:1;
	unsigned fifo_size:3;
	unsigned fifo_start:5;

	unsigned interrupt_priority;
	unsigned rx_interrupt_priority;
	
	unsigned number_masks;
	unsigned filter_masks[3];	

	unsigned number_filters;
	unsigned filter_sids[16];	
	unsigned filter_mask_source[16];
} CANconfig;

typedef enum {
	SUCCESS      = 0,
	ENULL        = -1,
	ECONFIG      = -2,
	ETIMING      = -3,
	ESENDFAILURE = -4,
} CANerror;

CANerror send_can2(CANdata msg);
CANerror send_can1(CANdata msg);

CANdata pop_can1();
CANdata pop_can2();

bool can1_rx_empty();
bool can2_rx_empty();

CANerror config_can1(CANconfig *config);
CANerror config_can2(CANconfig *config);

uint16_t can1_tx_available();
uint16_t can2_tx_available();

CANerror default_can_config(CANconfig *config);
#endif 
