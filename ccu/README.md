# PIC33E Project Template

## Submodules
This template includes the `can-ids` and `lib_pic33e` submodules inside the
`lib` directory. 

Run `make submodules` at any time to update your submodules.

Run `make add-submodules` to add the submodules to you repository.

## Makefile
The makefile includes by default only the `lib` and `src` directories. Please use 
paths relative to these directories in your `#include` directives.
(e.g., to include `lib/lib_pic33e/CAN.h`, do `#include "lib_pic33e/CAN.h"`).

Run `make` to compile your code. Run `make flash` to compile and program the 
PIC.

Run `make init URL=git@gitlab.com:projectofst/your-repo-name-here` to create a
new ready to go repo, based on the template.

Run `make verbose` to show the memory layout after linking.

The default pic is dsPIC33E256MU806. Please change the `MCU` variable if you're
using a different pic in the 33E family.

Remember to change the `PROGNAME` variable.

## `src/main.c`
A minimal `main.c` is provided, with generic configuration bits and `timing.h`
already included.

## `lib_pic33e/timing.h`
This header file configures the clock and defines `FOSC` and `FCY`.
The default configuration is FCY=16MHz, assuming the primary oscillator at 16Mhz.

Two more configurations are provided, FCY=64MHz and FCY=70Mhz. Define the macros
"FCY64" and "FCY70" respectively in the Makefile to enable them (e.g.
CPPFLAGS := -DFCY64).

Always include this file, as its required for clock configuration and parts of
the lib_pic33e depend on it.

## `AUTHORS`
This file includes the project's authors, just for cute historical purposes
for those unwilling to `git log`.
