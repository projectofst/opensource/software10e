import sys, time, click, socket

from threading import Thread
import queue


def adc_pack(id, value):
    bs = []
    bs.append((value >> 24) & 0xFF)
    bs.append((value >> 16) & 0xFF)
    bs.append((value >> 8) & 0xFF)
    bs.append((value >> 0) & 0xFF)
    bs.append(len(id))
    for i, c in enumerate(id):
        bs.append(ord(c))
    return bytearray(bs)


def read_input_thread(name, q):
    while True:
        r = input("> ")
        print(f"Cmd: {r}")
        q.put(r)


def send_adcs_thread(period, sock, host, port, q):
    values = [1758, 2337, 410, 410, 410, 1000, 1000, 1000]

    def update_value(var, index, value):
        var[index] += value

    key_actions = {
        "q": lambda x: update_value(values, 0, x),
        "a": lambda x: update_value(values, 0, -x),
        "w": lambda x: update_value(values, 1, x),
        "s": lambda x: update_value(values, 1, -x),
        "e": lambda x: update_value(values, 2, x),
        "d": lambda x: update_value(values, 2, -x),
        "r": lambda x: update_value(values, 3, x),
        "f": lambda x: update_value(values, 3, -x),
        "t": lambda x: update_value(values, 4, x),
        "g": lambda x: update_value(values, 4, -x),
        "y": lambda x: update_value(values, 5, x),
        "h": lambda x: update_value(values, 5, -x),
        "u": lambda x: update_value(values, 6, x),
        "j": lambda x: update_value(values, 6, -x),
        "i": lambda x: update_value(values, 7, x),
        "k": lambda x: update_value(values, 7, -x),
    }

    last_cmd = "q"
    last_jump = 10
    while True:
        if not q.empty():
            user = q.get()
            user = user.split(" ")

            if len(user) == 0 or (len(user) == 1 and user[0] == ""):
                cmd = last_cmd
                jump = last_jump
            else:
                cmd = user[0]

            if len(user) > 1:
                try:
                    jump = int(user[1])
                except Exception as e:
                    jump = 10
            else:
                jump = last_jump

            v = key_actions.get(cmd)

            if v is not None:
                v(jump)
                last_cmd = cmd
                last_jump = jump

        id = "ntc[0]"
        value = values[0]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "ntc[1]"
        value = values[1]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "ntc[2]"
        value = values[2]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "ntc[3]"
        value = values[3]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "ntc[4]"
        value = values[4]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "ntc[5]"
        value = values[5]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "ntc[6]"
        value = values[6]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        id = "ntc[7]"
        value = values[7]

        msg = adc_pack(id, value)
        sock.sendto(msg, (host, port))

        time.sleep(period / 1000)


@click.group(invoke_without_command=True)
@click.argument("host")
@click.argument("port", type=int)
@click.argument("period", type=float)
def main(host, port, period):
    q = queue.Queue()

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    send_thread = Thread(target=send_adcs_thread, args=(period, sock, host, port, q))
    read_input = Thread(target=read_input_thread, args=("read_input", q))

    print(
        """
    Command syntax: <cmd> <increment>
    A single return will repeat the last command.

    q - increment ntc[0]
    a - decrement ntc[0]
    w - increment ntc[1]
    s - decrement ntc[1]
    e - increment ntc[2]
    d - decrement ntc[2]
    r - increment ntc[3]
    f - decrement ntc[3]
    t - increment ntc[4]
    g - decrement ntc[4]
    y - increment ntc[5]
    h - decrement ntc[5]
    u - increment ntc[6]
    j - decrement ntc[6]
    i - increment ntc[7]
    k - decrement ntc[7]

    """
    )

    send_thread.start()
    read_input.start()

    send_thread.join()
    read_input.join()


if __name__ == "__main__":
    main()
