#ifndef __SENSORS_H__
#define __SENSORS_H__

#include <stdint.h>

#define B_PARAMETER 3971

uint16_t ntc_conv(uint16_t adc);
uint16_t pressure_conv(uint16_t adc);
uint16_t flow_conv(uint16_t flow_counter);
void get_frequency(void);

#endif
