#ifndef NOCOMMUNICATION

#include <stdlib.h>

#include "can-ids-v2/can_cfg.h"
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "ccu.h"
#include "communication.h"
#include "lib_pic33e/can.h"

can_t can_global;
extern uint16_t flash_mem[FLASH_SIZE];

wdt_id_t iib_motor_id = {
    .dev_id = DEVICE_ID_IIB,
    .msg_id = MSG_ID_IIB_IIB_MOTOR,
};

void receive_can(void)
{
    if (can1_rx_empty()) {
        return;
    }

    CANdata msg = pop_can1();
    decode_can(msg, &can_global);
    cmd_dispatch(msg);
    cfg_dispatch(msg);

    wdt_pet_candata(msg);

    return;
}

void dev_send_msg(CANdata msg)
{
    can_update_flash(&can_global, flash_mem);
    send_can1(msg);

    return;
}

uint16_t dev_get_id()
{
    return DEVICE_ID_CCU;
}

can_t* get_can()
{
    return &can_global;
}

#endif
