#ifndef NOCOOLING

#include "ccu.h"
#include "lib_pic33e/io.h"
#include "lib_pic33e/pwm.h"
#include "lib_pic33e/timer.h"
#include <xc.h>

#include "shared/mat/mat.h"
#include "shared/scheduler/scheduler.h"

#include "communication.h"
#include "cooling.h"

extern wdt_id_t iib_motor_id;

typedef enum _request_t {
    REQ_NONE,
    REQ_OFF,
    REQ_ON,
} request_t;

request_t fan_req = REQ_NONE;
request_t pump_req = REQ_NONE;

fan_t fan_init()
{
    return (fan_t) {
        .state = FAN_INITIAL,
        .timer = 0,
    };
}

void fan_set_state(fan_t* fan, fan_state_t state)
{
    fan->state = state;
}

pump_t pump_init()
{
    return (pump_t) {
        .state = PUMP_INITIAL,
        .timer = 0,

    };
}

void pump_set_state(pump_t* pump, pump_state_t state)
{
    pump->state = state;
}

extern uint16_t flash_mem[FLASH_SIZE];

void turn_on_cooling(void)
{
    // pinE_high(5);
    // pinE_high(7);
    // printf("*Turn on* \n");
    fan_req = REQ_ON;
    pump_req = REQ_ON;
    // pwm2_update_duty_cycle(1.0); // FAN1
    // pwm3_update_duty_cycle(1.0); // FAN2
    // pwm4_update_duty_cycle(1.0); // FAN3
    // LATEbits.LATE4 = 1;
    // printf("*Turn on* \n");

    return;
}

void turn_off_cooling(void)
{
    // pinE_low(5);
    // pinE_low(7);
    // printf("*Turn off* \n");
    fan_req = REQ_OFF;
    pump_req = REQ_OFF;
    // pwm2_update_duty_cycle(0.0); // FAN1
    // pwm3_update_duty_cycle(0.0); // FAN2
    // pwm4_update_duty_cycle(0.0); // FAN3
    // LATEbits.LATE4 = 0;
    // printf("*Turn off* \n");

    return;
}

cooling_t init_cooling(void)
{
    cooling_t cooling;
    cooling.fan = fan_init();
    cooling.pump = pump_init();

    int cfg_err = 0;

    cfg_err += pwm2_config(1, PWM_T, CLK_TIMER2);
    cfg_err += pwm3_config(1, PWM_T, CLK_TIMER2);
    // cfg_err += pwm4_config(1, PWM_T, CLK_TIMER2);

    if (cfg_err != 0) {
        // send_can1(logD(LOG_RESET_MESSAGE, 1, 2, 3));
        // LATEbits.LATE4 = 1;
        printf("Cfg_err: %d \n", cfg_err);
    }

    // motors_warmup();

    /*if (flash_mem[MOTOR_STATE] == 0) {
        turn_off_cooling();
        printf("starting off \n");
    } else {
        turn_on_cooling();
        printf("starting on \n");
    }
    */

    return cooling;
}

void pump_control(pump_t* pump)
{
    printf("pump_state: %d\n", pump->state);
    /* handle external command via requests */
    if (pump->state == PUMP_INITIAL) {
        // printf("pump INITIAL\n");
        if (pump->timer > VISUAL_CHECK_TIME) {
            pump_set_state(pump, PUMP_STARTED);
        } else {
            pwm2_update_duty_cycle(1);
        }
    } else if (pump->state == PUMP_STARTED) {
        pump_set_state(pump, PUMP_ON);
        return;
        // printf("pump STARTED");
        if (flash_mem[MOTOR_STATE] == 0) {
            pump_set_state(pump, PUMP_OFF);
        } else {
            pump_set_state(pump, PUMP_ON);
        }
    } else if (pump->state == PUMP_OFF) {
        if (pump_req == REQ_ON) {
            pump_set_state(pump, PUMP_ON);
            pump_req = REQ_NONE;
            return;
        }
        // printf("pump OFF\n");
        pwm2_update_duty_cycle(0);
    } else if (pump->state == PUMP_ON) {
        /*
        if (pump_req == REQ_OFF) {
            pump_set_state(pump, PUMP_OFF);
            pump_req = REQ_NONE;
            return;
        }
        */
        // printf("pump ON\n");
        pwm2_update_duty_cycle(1);
    }
}

/*                            OFF
 *                          ┌──────┐
 *                          │      │
 *                       ┌──►      │◄──┐
 *                       │  │      │   │
 *                       │  └───┬──┘   │
 * ┌──────┐    ┌──────┐  │      │      │
 * │      │    │      │  │      │      │
 * │      ├────►      ├──┤      │      │
 * │      │    │      │  │      │      │
 * └──────┘    └──────┘  │      ▼      │
 * INITIAL     STARTED   │  ┌──────┐   │
 *                       │  │      │   │
 *                       └──►      ├───┘
 *                          │      │
 *                          └──────┘
 *                             ON
 *
 * CCU control is based on a simple state machine.
 *
 * In the Initial state the CCU performs fan visual check. Once the check is
 * finished we move to the STARTED state.
 *
 * In the STARTED state the state machine decides between the ON or the OFF
 * state depending on values written to flash memory.
 *
 * In the ON|OFF state the state machine can move to OFF|ON state depending on
 * an external request.
 *
 * The ON state performs control over the fan. This control is based on a
 * look up table. The table addresses are PWM levels (20 atm) and its contents
 * the temperature threshold for the PWM level.
 * Searching over the table with the current temperature yields the intended PWM
 * level for that temperature. A higher PWM level is always preferred over a
 * lower one.
 */

/* clang-format: off */
u16 fan_temp_table[] = {
    30,
    32,
    34,
    36,
    38,
    40,
    42,
    44,
    46,
    48,
    50,
    52,
    54,
    56,
    58,
    60,
    62,
    64,
    66,
    68,
};
/* clang-format: on */

f32 fan_duty_cycle_level(u16 temp)
{
    int i = 0;
    for (i = 0; i < sizeof(fan_temp_table) / sizeof(u16); i++) {
        if (fan_temp_table[i] > temp) {
            return i / 20.0;
        }
    }

    return 1.0;
}

void fan_control(fan_t* fan)
{
    can_t* can = get_can();

    uint16_t max_motor_temp = max4(can->iib.iib_motor.temp_motor);
    can->ccu_left.ccu_status_left.max_motor_temp_left = max_motor_temp;

    wdt_t* iib_motor_wdt = wdt_find(iib_motor_id);
    if (wdt_is_bad(iib_motor_wdt)) {
        max_motor_temp = 1000;
    }

    printf("fan_control: %d\n", fan->state);
    if (fan->state == FAN_INITIAL) {
        if (fan->timer > VISUAL_CHECK_TIME) {
            fan_set_state(fan, FAN_STARTED);
        } else {
            pwm3_update_duty_cycle(1);
        }
    } else if (fan->state == FAN_STARTED) {
        if (flash_mem[MOTOR_STATE] == 0) {
            fan_set_state(fan, FAN_OFF);
        } else {
            fan_set_state(fan, FAN_ON);
        }
    } else if (fan->state == FAN_OFF) {
        if (fan_req == REQ_ON) {
            fan_set_state(fan, FAN_OFF);
            fan_req = REQ_NONE;
            return;
        }
        pwm3_update_duty_cycle(0);
    } else if (fan->state == FAN_ON) {
        if (fan_req == REQ_OFF) {
            fan_set_state(fan, FAN_ON);
            fan_req = REQ_NONE;
            return;
        }

        f32 dc = fan_duty_cycle_level(max_motor_temp);
        can->ccu_left.ccu_status_left.fan_pwm_level_left = dc;
        pwm3_update_duty_cycle(dc);
    }
}

void cooling_control(void* _cooling)
{
    can_t* can = get_can();
    cooling_t* cooling = (cooling_t*)_cooling;

    pump_control(&cooling->pump);
    fan_control(&cooling->fan);
    ccu_encode_pump_status(can, cooling->pump.state);
    ccu_encode_fan_status(can, cooling->fan.state);

    /* find task period */
    tasks_t* task = scheduler_find_task_struct("cooling_control");
    uint16_t period = (task == NULL) ? 10 : task->period;

    cooling->pump.timer += period;
    cooling->fan.timer += period;

    return;
}

#endif
