#include <math.h>

#include "can-ids-v2/can_ids.h"

#include "hk/timer/timer.h"

#include "ccu.h"
#include "ccu_app.h"
#include "communication.h"
#include "sensors.h"

static uint32_t last_flag = 0;
extern can_t can_global;
volatile uint16_t tach_counter = 0;
extern uint16_t flash_mem[FLASH_SIZE];

uint16_t pressure_conv(uint16_t adc)
{
    if (adc < 620)
        return 20;

    return (((adc - 620) * 4.0) / (4095.0 - 620.0) * 100);
}

uint16_t ntc_conv(uint16_t adc)
{

    if (adc < 935 || adc > 4083)
        return 5000;

    return (1 / ((log((4096.0 - adc) / adc) / B_PARAMETER) + 1 / 298.15) - 273.15) * 10;
}

uint16_t flow_conv(uint16_t flow_frequency)
{
    return (0.12121 * ((float)flow_frequency) + 0.05349) * 10;
}

void get_frequency(void)
{
    // TRISEbits.TRISE4 = 0;
    // LATEbits.LATE4 ^= 1;
    if (flash_mem[FLOW_STATE] != 0) {
        time_ms_t time = hk_timer_get_ms(get_timer());

        uint16_t period = time - last_flag;
        last_flag = time;

        CANdata msg;

        msg = ccu_encode_tach(can_global);
        dev_send_msg(msg);

        tach_counter = 0;
    }

    return;
}
