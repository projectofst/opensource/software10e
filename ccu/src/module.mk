MODULE_C_SOURCES:=adc_config.c
MODULE_C_SOURCES+=ccu.c
MODULE_C_SOURCES+=communication.c
MODULE_C_SOURCES+=cooling.c
MODULE_C_SOURCES+=main.c
MODULE_C_SOURCES+=sensors.c

PWD:=src/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk