/*
 * main.c is part of the CCU firmware, the cooling system DAQ and controller for FST Lisboa vehicles.
 * Copyright © 2021 Projecto FST
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF  // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF  // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS    // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF  // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128    // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON       // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF      // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON       // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4  // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON    // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE   // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF  // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF  // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

#include <stdint.h>
#include <xc.h>

// NOTE: Always include timing.h
#include "lib_pic33e/timer.h"
#include "lib_pic33e/timing.h"

// Comment this line if USB is not needed
//#include "lib_pic33e/usb_lib.h"

// Trap handling
#include "lib_pic33e/trap.h"

// PIC Libraries
#include "lib_pic33e/adc.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/external.h"
#include "lib_pic33e/flash.h"
#include "lib_pic33e/pps.h"
#include "lib_pic33e/pwm.h"

// Shared Libraries
#include "hk/timer/timer.h"
#include "rgb_led/rgb_led.h"
#include "scheduler/scheduler.h"
#include "watchdog/watchdog.h"

// App Libraries
#include "ccu_app.h"

// FCP Libraries
#include "can-ids-v2/can_cfg.h"
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_log.h"

// CCU Libraries
#include "adc_config.h"
#include "ccu.h"
#include "communication.h"
#include "cooling.h"
#include "sensors.h"

extern uint16_t tach_counter;
extern uint16_t flash_mem[FLASH_SIZE];
uint32_t miliseconds = 0;

extern wdt_id_t iib_motor_id;

void wait_watchdogs()
{
    tasks_t *task = scheduler_find_task_struct("wait_watchdogs");
    if (task == NULL)
    {
        return;
    }
    uint16_t period = task->period;

    wdt_wait_id(iib_motor_id, period);

    can_t *can = get_can();

#ifdef LEFT
    can->ccu_left.ccu_status_left.iib_motor_wdt_left = wdt_is_good_id(iib_motor_id);
#else
    can->ccu_right.ccu_status_right.iib_motor_wdt_right = wdt_is_good_id(iib_motor_id);
#endif
}

void timer1_callback(void)
{
    hk_timer_wait_ms(get_timer(), 1);
    return;
}

uint32_t get_time_ms()
{
    return hk_timer_get_ms(get_timer());
}

void external1_callback(void)
{
    tach_counter++;

    return;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{
    multiple_return_t mt = {0};

    if (id == CMD_CCU_SET_FANS)
    {

        if (arg1 == 0)
        {
            turn_off_cooling();
        }
        else
        {
            turn_on_cooling();
        }

        flash_mem[MOTOR_STATE] = arg1;
        write_flash(&flash_mem[MOTOR_STATE], MOTOR_STATE, 1);
        send_can1(logI(LOG_MOTOR_STATE, DEVICE_ID_CCU, arg1, 0));
    }

    else if (id == CMD_CCU_SET_NTCS)
    {
        if (arg1 > 8)
        {
            send_can1(logE(LOG_NTC_NUMBER, DEVICE_ID_CCU, arg1, 0));
        }
        else
        {

            flash_mem[NTC_N] = arg1;

            write_flash(&flash_mem[NTC_N], NTC_N, 1);
            send_can1(logI(LOG_NTC_NUMBER, DEVICE_ID_CCU, arg1, 0));
        }
    }

    else if (id == CMD_CCU_SET_FLOW)
    {
        flash_mem[FLOW_STATE] = arg1;

        write_flash(&flash_mem[FLOW_STATE], FLOW_STATE, 1);
        send_can1(logI(LOG_FLOW_STATE, DEVICE_ID_CCU, arg1, 0));
    }

    else if (id == CMD_CCU_SET_PWM_1)
    {
        if (arg1 >= 0 && arg1 <= 100)
        {
            pwm2_update_duty_cycle(arg1 / 100.0);
        }
    }
    else if (id == CMD_CCU_SET_PWM_2)
    {
        if (arg1 >= 0 && arg1 <= 100)
        {
            pwm3_update_duty_cycle(arg1 / 100.0);
        }
    }
    else if (id == CMD_CCU_SET_PWM_3)
    {
        if (arg1 >= 0 && arg1 <= 100)
        {
            pwm4_update_duty_cycle(arg1 / 100.0);
        }
    }

    else
    {
        send_can1(logE(LOG_WRONG_CMD_ID, DEVICE_ID_CCU, id, 0));
    }

    return mt;
}

void ccu_config()
{
    int cfg_err = 0;

    // Unlock Pins for CAN and Output compare for PWM

    PPSUnLock;
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP97); // CAN1TX in RP97
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI96);   // CAN1RX in RP96
    PPSOutput(OUT_FN_PPS_OC1, OUT_PIN_PPS_RP84);  // Debug Led OC1
    PPSOutput(OUT_FN_PPS_OC2, OUT_PIN_PPS_RP85);  // OC2 in RP85 FAN1
    PPSOutput(OUT_FN_PPS_OC3, OUT_PIN_PPS_RP87);  // OC3 in RP87 FAN2
    PPSOutput(OUT_FN_PPS_OC4, OUT_PIN_PPS_RP118); // OC4 in RP118 FAN3
    PPSInput(IN_FN_PPS_INT1, IN_PIN_PPS_RPI86);   // RPI36 set as INT1
    PPSLock;

    // Timers
    config_timer1(1, 4);   // 1ms period
    config_timer2(100, 5); // 1ms period

    config_external1(1, 5);

    // CAN bus
    config_can1(NULL);
    send_can1(logD(LOG_RESET_MESSAGE, 0, 0, 0));

    // ADC's
    adc_init(AN13 | AN12 | AN11 | AN10 | AN9 | AN8 | AN7 | AN6);

    uint32_t table[CFG_CCU_SIZE] = {VERSION};

    cfg_config(table, CFG_CCU_SIZE);

    cfg_err += pwm1_config(0.0, PWM_T, CLK_TIMER2);

    pwm1_update_duty_cycle(1.0);
}

rgb_t cfg_debug_led()
{

    rgb_profile_t profiles[] = {
        {.name = "ams_error", .wave = LED_SIN, .period = 1.0, .duty_cycle = 1.0, .color = 0xFFFFFF}};

    rgb_t led = {
        .red_led_pwm_update = NULL,
        .green_led_pwm_update = NULL,
        .blue_led_pwm_update = NULL};

    led.red_led_pwm_update = pwm1_update_duty_cycle;

    rgb_config(profiles, 1 /*sizeof(profiles) / sizeof(rgb_profile_t)*/, "ams_error");

    return led;
}

int main()
{
    can_t *can_global = get_can();
    ccu_config();
    ccu_init();

    cooling_t cooling = init_cooling();

    tasks_t ccu_tasks[] = {
        {.name = "ntc_val", .period = 1500, .func = ntc_val},
        {.name = "get_frequency", .period = 1000, .func = get_frequency},
        {.name = "adc_average", .period = 50, .func = adc_average},
        {.name = "receive_can", .period = 0, .func = receive_can},
        {.name = "wait_watchdogs", .period = 10, .func = wait_watchdogs},
        {.name = "cooling_control", .period = 10, .func = cooling_control, .args_on = true, .args = (void *)&cooling},
    };
    scheduler_init_args(ccu_tasks, sizeof(ccu_tasks) / sizeof(tasks_t));

    wdt_t watchdogs[] = {
        {.id = iib_motor_id, .mode = WDT_RECOVER, .err_time = 1000, .recovery_time = 10000},
    };

    wdt_init(watchdogs, sizeof(watchdogs) / sizeof(wdt_t));

    hk_timer_t *timer = get_timer();

    while (1)
    {
        time_ms_t time = hk_timer_get_ms(timer);
        scheduler(time);
        ccu_send_msgs(time);

        // Clear watchdog timer
        ClrWdt();
    } // end while

    return 0;
} // end main

void trap_handler(TrapType type)
{

    switch (type)
    {
    case HARD_TRAP_OSCILATOR_FAIL:
        break;
    case HARD_TRAP_ADDRESS_ERROR:
        break;
    case HARD_TRAP_STACK_ERROR:
        break;
    case HARD_TRAP_MATH_ERROR:
        break;
    case CUSTOM_TRAP_PARSE_ERROR:
        break;
    default:
        break;
    }

    send_can1(logE(LOG_TRAP_HANDLER_TYPE, type, 0, 0));

    __delay_ms(500);

    return;
}
