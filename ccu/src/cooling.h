#ifndef __COOLING_H__
#define __COOLING_H__

#define ms(x) x
#define s(x) x*1000

#define VISUAL_CHECK_TIME ms(2000)


typedef enum {FAN_INITIAL, FAN_STARTED, FAN_OFF, FAN_ON} fan_state_t;

typedef struct _fan_t {
    uint32_t timer;
    fan_state_t state;
} fan_t;

typedef enum {PUMP_INITIAL, PUMP_STARTED, PUMP_OFF, PUMP_ON} pump_state_t;

typedef struct _pump_t {
    uint32_t timer;
    pump_state_t state;
} pump_t;

typedef struct _cooling_t {
    fan_t fan;
    pump_t pump;
} cooling_t;

cooling_t init_cooling(void);
void turn_on_cooling(void);
void turn_off_cooling(void);
void config_pwm(uint32_t period, uint16_t duty_cycle);


void fan_set_state(fan_t *fan, fan_state_t state);
void pump_set_state(pump_t *pump, pump_state_t state);
void cooling_control(void* _cooling);

#endif
