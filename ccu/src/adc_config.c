#ifndef NOADC

#include "adc_config.h"
#include "can-ids-v2/can_ids.h"
#include "ccu.h"
#include "communication.h"
#include "lib_pic33e/adc.h"
#include "sensors.h"
#include <stdio.h>

uint16_t adc_data[N_SENSORS];
adc_channel sensor[N_SENSORS];
extern uint16_t flash_mem[FLASH_SIZE];
extern can_t can_global;

void adc_init(uint16_t vector)
{
    ADC_parameters adc_config;

    config_pin_cycling(vector, 10UL, 64, 4, &adc_config);
    config_adc1(adc_config);

    CNPDB = vector;

    return;
}

void adc_average(void)
{
    uint32_t i;

    get_pin_cycling_values(adc_data, 0, N_SENSORS);

    for (i = 0; i < N_SENSORS; i++) {
        sensor[i].acc = (sensor[i].N * sensor[i].acc + adc_data[i]) / (sensor[i].N + 1);
        sensor[i].N++;
    }

    return;
}

void reset_adc(void)
{
    uint16_t i;

    for (i = 0; i < N_SENSORS; i++) {
        sensor[i].acc = 0;
        sensor[i].N = 0;
    }

    return;
}

void ntc_val(void)
{
    printf("CCU Running... \n");

    uint8_t i;
    uint16_t ntc_vals[8] = { [0 ... 7] = 6000 };

    for (i = 0; i < flash_mem[NTC_N]; i++) {
        ntc_vals[i] = ntc_conv(sensor[i].acc);
    }

#ifdef LEFT
    can_global.ccu_left.temp1_left.ntc1_left = ntc_vals[0];
    can_global.ccu_left.temp1_left.ntc2_left = ntc_vals[1];
    can_global.ccu_left.temp1_left.ntc3_left = ntc_vals[2];
    can_global.ccu_left.temp1_left.ntc4_left = ntc_vals[3];
    can_global.ccu_left.temp2_left.ntc5_left = ntc_vals[4];
    can_global.ccu_left.temp2_left.ntc6_left = ntc_vals[5];
    can_global.ccu_left.temp2_left.ntc7_left = ntc_vals[6];
    can_global.ccu_left.temp2_left.ntc8_left = ntc_vals[7];
#else
    can_global.ccu_right.temp1_right.ntc1_right = ntc_vals[0];
    can_global.ccu_right.temp1_right.ntc2_right = ntc_vals[1];
    can_global.ccu_right.temp1_right.ntc3_right = ntc_vals[2];
    can_global.ccu_right.temp1_right.ntc4_right = ntc_vals[3];
    can_global.ccu_right.temp2_right.ntc5_right = ntc_vals[4];
    can_global.ccu_right.temp2_right.ntc6_right = ntc_vals[5];
    can_global.ccu_right.temp2_right.ntc7_right = ntc_vals[6];
    can_global.ccu_right.temp2_right.ntc8_right = ntc_vals[7];
#endif

    CANdata msg;

    if (flash_mem[NTC_N] != 0) {
#ifdef LEFT
        msg = encode_temp1_left(can_global.ccu_left.temp1_left);
#else
        msg = encode_temp1_right(can_global.ccu_right.temp1_right);
#endif
        dev_send_msg(msg);

        if (flash_mem[NTC_N] > 4) {
#ifdef LEFT
            msg = encode_temp2_left(can_global.ccu_left.temp2_left);
#else
            msg = encode_temp2_right(can_global.ccu_right.temp2_right);
#endif
            dev_send_msg(msg);
        }
    }

    reset_adc();

    return;
}
#endif
