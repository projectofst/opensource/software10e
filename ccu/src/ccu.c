#include "ccu.h"
#include "communication.h"
#include "lib_pic33e/flash.h"

uint16_t flash_mem[FLASH_SIZE];
volatile hk_timer_t timer;

hk_timer_t* get_timer()
{
    return &timer;
}

void ccu_init(void)
{
    timer = hk_timer_init();

    read_flash(flash_mem, 0, FLASH_SIZE);
    flash_mem[0] = 8;

    return;
}
