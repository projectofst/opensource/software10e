#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include "can-ids-v2/can_ids.h"
#include "shared/watchdog/watchdog.h"

void receive_can(void);
can_t *get_can();

#endif
