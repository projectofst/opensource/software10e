#ifndef __ADC_CONFIG_H__
#define __ADC_CONFIG_H__

#include <stdint.h>

#define N_SENSORS 8

typedef struct _adc {
    uint16_t acc;
    uint32_t N;
} adc_channel;

void adc_init(uint16_t vector);
void adc_average(void);
void reset_adc(void);
void ntc_val(void);

#endif
