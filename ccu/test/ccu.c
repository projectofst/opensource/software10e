#include "ccu.h"
#include "communication.h"
#include "lib_pic33e/flash.h"

uint16_t flash_mem[FLASH_SIZE];

void ccu_init(void)
{
    read_flash(flash_mem, 0, FLASH_SIZE);
	flash_mem[0] = 8;

    return;
}
