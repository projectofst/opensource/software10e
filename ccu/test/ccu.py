from matplotlib import pyplot as plt
from math import log, floor


def convertion(counts, counts_range, volt_range, beta, R):
    v = counts / counts_range
    T0 = 25 + 273.15
    r = R / v - R

    return (1 / (1 / T0 + (1 / beta) * log(r / R)) - 273.15) * 10


beta = 3971  # 25 - 50

table = [convertion(i, 4096, 3.3, beta, 10e3) for i in range(1000, 4096)]
# plt.plot(table)
# plt.show()
for i, t in enumerate(table):
    print(f"{floor(t)},")
