#ifndef __LIB_PIC33E_H__
#define __LIB_PIC33E_H__

#include <stdint.h>

uint16_t read_flash(uint16_t *buf,uint16_t offset, uint16_t len);
uint16_t write_flash(uint16_t *buf,uint16_t offset, uint16_t len);

#endif
