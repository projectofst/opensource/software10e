#ifndef _LIB_PIC33E_TIMER_H
#define _LIB_PIC33E_TIMER_H

#include <stdint.h>
#include <stdbool.h>
#include <time.h>

void config_time1(unsigned int time, unsigned int priority);
void __attribute__((weak)) timer1_callback(void); 

#endif