#ifndef _LIB_PIC33E_CAN_H
#define _LIB_PIC33E_CAN_H

#include <stdint.h>
#include <stdbool.h>

typedef enum {
	SUCCESS      = 0,
	ENULL        = -1,
	ECONFIG      = -2,
	ETIMING      = -3,
	ESENDFAILURE = -4,
} CANerror;

/* A CAN message contains an id, a dlc and a array of 4 16 bit unsigned
 * integers
 *
 * The id is a 11 bit unsigned integer, separated into a 5 bit device id and a
 * 6 bit message id
 *
 * The first bit of the message id signals a reserved message. 0 means a
 * reserved id.
 */

#ifndef _CANDATA
#define _CANDATA
typedef struct {
	union {
		struct {
			uint16_t dev_id:5; // Least significant
			uint16_t msg_id:6; // Most significant
		};
		uint16_t sid;
	};
	uint16_t dlc:4;
	uint16_t data[4];
} CANdata;
#endif

CANerror send_can1(CANdata msg);
CANdata pop_can1();
bool can1_rx_empty();

#endif