#ifndef __TRAP_H__
#define __TRAP_H__


typedef enum trap_types{
    HARD_TRAP_OSCILATOR_FAIL,
    HARD_TRAP_ADDRESS_ERROR,
    HARD_TRAP_STACK_ERROR,
    HARD_TRAP_MATH_ERROR,
    CUSTOM_TRAP_PARSE_ERROR
} TrapType;

#endif
