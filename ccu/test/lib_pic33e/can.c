#include "can.h"

CANerror send_can1(CANdata msg){
    return (CANerror) 0;
}

CANdata pop_can1(){
    //send wanted message
    CANdata msg;
    msg.sid = 0;
    msg.dlc = 0;
    msg.data[0] = 0;
    msg.data[1] = 0;
    msg.data[2] = 0;
    msg.data[3] = 0;

    return msg;
}

bool can1_rx_empty(){
    return 0;
}
