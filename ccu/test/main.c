#include <stdio.h>
#include <stdint.h>
#include "munit/munit.h"
#include "math.h"
#include "table.h"
#include "sensors.h"
#include "can-ids/can_cmd.h"

#define B_PARAMETER 3971
#define ADC_MAX 3097

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{
    multiple_return_t mt = { 0 };
	return mt;
}

static MunitResult ccu_temp_ok(const MunitParameter params[], void* user_data) {
	int temp_c[ADC_MAX];
	int T;

	for(int i = 2000; i < ADC_MAX; i++){
		//printf("\nT=%d,i=%d,table=%d\n",ntc_conv(i),i,table[i-1000]);
		munit_assert_int(ntc_conv(i), ==, table[i-1000]);
	}

	return MUNIT_OK;
}

static MunitResult ccu_basic_temp(const MunitParameter params[], void* user_data) {

		munit_assert_int(ntc_conv(2048), ==, 250);

	return MUNIT_OK;
}

static MunitResult ccu_basic_pressure(const MunitParameter params[], void* user_data) {

		munit_assert_int(pressure_conv(2048), ==, 164);
		munit_assert_int(pressure_conv(0), ==, 20);
		munit_assert_int(pressure_conv(4095), ==, 400);

	return MUNIT_OK;
}

static MunitTest test_suite_tests[] = {
  	{ (char*) "/scheduler/temp_ok", ccu_temp_ok, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
	  { (char*) "/scheduler/basic_temp", ccu_basic_temp, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
	  	{ (char*) "/scheduler/ccu_basic_pressure", ccu_basic_pressure, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
  	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
  	(char*) "", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

#include <stdlib.h>

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {


  	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
