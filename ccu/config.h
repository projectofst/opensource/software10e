#ifndef __CONFIG_H__
#define __CONFIG_H__

#define VIO_PORT 8888
#define VIO_HOST "127.0.0.1"

#define VIO_ADC1_CONFIG {{"ntc[0]", 0}, {"ntc[1]", 0}, {"ntc[2]", 0}, {"ntc[3]", 0}, {"ntc[4]", 0}, {"ntc[5]", 0}, {"ntc[6]", 0}, {"ntc[7]", 0}, {NULL, 0}}

#define CAN_ESSENTIAL	57005
#define CAN_SENSOR		48879
#define CAN_AMK			43962

#define PORT1			CAN_SENSOR
#define PORT2			CAN_ESSENTIAL

#define	SIM_DELTA_T_US	100
#endif
