#ifndef __CCU_APP_H__
#define __CCU_APP_H__

#include <stdint.h>
#include "can-ids/can_ids.h"

#define RIGHT

#define ccu_send_msgs(time) ccu_right_send_msgs(can_global->ccu_right, time)
void can_update_flash(can_t *can_global, uint16_t *flash_mem);
void can_update_tach(can_t *can_global, uint16_t tach_counter, uint16_t period);

#define ccu_encode_tach(can_global) encode_tach_right(can_global.ccu_right.tach_right);
#define ccu_encode_pump_status(can, status) can->ccu_right.ccu_status_right.ccu_right_pump_status = status
#define ccu_encode_fan_status(can, status) can->ccu_right.ccu_status_right.ccu_right_fan_status = status
#endif
