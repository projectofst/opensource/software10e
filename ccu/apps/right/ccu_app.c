#include "ccu_app.h"
#include "ccu.h"

void can_update_flash(can_t* can_global, uint16_t* flash_mem)
{
    can_global->ccu_right.ccu_status_right.motor_status_right = flash_mem[MOTOR_STATE];
    //can_global->ccu_right.ccu_status_right.flow_status_right = flash_mem[FLOW_STATE];
    can_global->ccu_right.ccu_status_right.ntc_status_right = flash_mem[NTC_N];
}

void can_update_tach(can_t* can_global, uint16_t tach_counter, uint16_t period)
{
    can_global->ccu_right.tach_right.frequency_right = tach_counter;
    can_global->ccu_right.tach_right.period_right = period;
    can_global->ccu_left.tach_left.flow_left = flow_conv(tach_counter);
}
