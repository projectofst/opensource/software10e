#include "ccu_app.h"
#include "ccu.h"

void can_update_flash(can_t* can_global, uint16_t* flash_mem)
{
    can_global->ccu_left.ccu_status_left.motor_status_left = flash_mem[MOTOR_STATE];
    // can_global->ccu_left.ccu_status_left.flow_status_left = flash_mem[FLOW_STATE];
    can_global->ccu_left.ccu_status_left.ntc_status_left = flash_mem[NTC_N];
}

void can_update_tach(can_t* can_global, uint16_t tach_counter, uint16_t period)
{
    can_global->ccu_left.tach_left.frequency_left = tach_counter;
    can_global->ccu_left.tach_left.period_left = period;
    can_global->ccu_left.tach_left.flow_left = flow_conv(tach_counter);
}
