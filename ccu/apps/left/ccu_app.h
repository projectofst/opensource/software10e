#ifndef __CCU_APP_H__
#define __CCU_APP_H__

#include <stdint.h>
#include "can-ids-v2/can_ids.h"

#define LEFT

#define ccu_send_msgs(time) ccu_left_send_msgs(can_global->ccu_left, time)
void can_update_flash(can_t *can_global, uint16_t *flash_mem);
void can_update_tach(can_t *can_global, uint16_t tach_counter, uint16_t period);

#define ccu_encode_tach(can_global) encode_tach_left(can_global.ccu_left.tach_left);
#define ccu_encode_pump_status(can, status) can->ccu_left.ccu_status_left.ccu_left_pump_status = status
#define ccu_encode_fan_status(can, status) can->ccu_left.ccu_status_left.ccu_left_fan_status = status

#endif
