#include "adc_config.h"
#include "can-ids/can_ids.h"
#include "lib_pic33e/adc.h"
#include "shared/run_avg/run_avg.h"

#define AVGSIZE 10

int16_t adc_data[N_SENSORS];
extern du_data du;
extern can_t can_global;
run_avg_t run_avgs[N_SENSORS];
uint16_t threshold[N_SENSORS] = { 0, 0 };

/*
 *  WARNING: 
 *  AN2 -> ADC LEFT 
 *  AN3 -> ADC RIGHT
 */
void adc_init(uint16_t vector)
{
    uint16_t i = 0;

    ADC_parameters adc_config;

    config_pin_cycling(vector, 3200UL, 64, 4, &adc_config);
    config_adc1(adc_config);

    CNPDB = vector;

    for (i = 0; i < N_SENSORS; i++) {
        run_avgs[i].max_size = 16;
        run_avg_config(&run_avgs[i]);
    }
    return;
}

#ifndef FAKE
void adc_average(void* du_)
{
    du_data* _du = (du_data*)du_;
    uint32_t i;
    static int n;

    get_pin_cycling_values(adc_data, 0, N_SENSORS);

    /* for (i = 0; i < N_SENSORS; i++) {
        run_avg_add(&run_avgs[i], adc_data[i]);
    }
    //update values left

    _du->adc_read_left = run_avg_average(&run_avgs[LEFT]) - threshold[LEFT];

    //update values right

    _du->adc_read_right = run_avg_average(&run_avgs[RIGHT]) - threshold[RIGHT];
  */

    //update values left

    _du->adc_read_left = adc_data[LEFT];

    //update values right

    _du->adc_read_right = adc_data[RIGHT];

    //-------
    update_adc_values(_du);
    return;
}
#endif

#ifdef FAKE
void adc_average(void* du_)
{
    du_data* _du = (du_data*)du_;
    _du->adc_read_left = get_adcbuf0() - threshold[LEFT];
    _du->adc_read_right = get_adcbuf1() - threshold[RIGHT];
    printf("LEFT:%d\n", _du->adc_read_left);
    printf("RIGHT:%d\n", _du->adc_read_right);
    update_adc_values(_du);
    return;
}
#endif

void update_adc_values(du_data* du_)
{
    can_global.du_front.adc_values_front.adc_front_left = du_->adc_read_left;
    can_global.du_front.adc_values_front.adc_front_right = du_->adc_read_right;
    return;
}

void update_threshold_left()
{
    threshold[LEFT] = du.adc_read_left;
}

void update_threshold_right()
{
    threshold[RIGHT] = du.adc_read_right;
}
