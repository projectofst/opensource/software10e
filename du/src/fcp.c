#include "fcp.h"
#include "can-ids/can_cmd.h"
#include "can-ids/can_ids.h"
#include "can-ids/can_log.h"
#include "lib_pic33e/flash.h"
#include "shared/fifo/fifo.h"
#include "signals.h"
#include <stdint.h>

extern Fifo* fifo;

//extern uint16_t flash[CFG_DU_SIZE];

uint16_t dev_get_id()
{
    //return DEVICE_ID_DU;
    return DEVICE_ID_DU_FRONT;
}

void dev_send_msg(CANdata msg)
{
    //append_fifo(fifo, msg);
    send_can1(msg);
    return;
}

void can_dispatch(void* fifo1)
{
    CANdata* msg;
    Fifo* fifo = (Fifo*)fifo1;
    uint16_t available = can1_tx_available();
    CANdata msg1;

    msg1.dev_id = 22;
    msg1.msg_id = 1;
    msg1.dlc = 4;
    msg1.data[0] = 1;
    msg1.data[1] = 2;
    msg1.data[2] = 3;
    msg1.data[3] = 4;

    //send_can1(msg1);

    /*msg_du_front_adc_values_front_t adc_front_values = {.adc_front_left = can_global};
    if (available) {

        encode_adc_values_front
    };*/
    /*for (; available > 0 && !fifo_empty(fifo); available--) {
        //LED5 ^= 1;
        msg = fifo_pop(fifo);
        if (msg == NULL) {*/
    /* Should never happen */
    /*break;
            ;
        }
        send_can1(*msg);
    }*/

    return;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{

    multiple_return_t mt;
    if (id == CMD_DU_FRONT_FRONT_LEFT_REFERENCE) {
        update_threshold_left();
    }

    if (id == CMD_DU_FRONT_FRONT_RIGHT_REFERENCE) {
        update_threshold_right();
    }

    return mt;
}

/*
void cfg_set_callback(uint32_t* table)
{
    uint16_t i;

    for (i = 0; i < CFG_DU_SIZE; i++)
        flash[i] = table[i];

    return;
}
*/
