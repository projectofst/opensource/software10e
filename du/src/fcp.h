#ifndef __DU_FCP_H__
#define __DU_FCP_H__

#include "can-ids/can_ids.h"
#include <stdlib.h>
#include "adc_config.h"



void can_dispatch(void*);
void dev_send_msg(CANdata);

#endif