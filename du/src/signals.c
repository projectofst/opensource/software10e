#include "signals.h"

void do_the_pps(void)
{
    //CAN
    PPSUnLock;
    // Output configuration based in the README
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
    PPSOutput(OUT_FN_PPS_C2TX, OUT_PIN_PPS_RP120);
    // Input configuration based in the README
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
    PPSInput(IN_FN_PPS_C2RX, IN_PIN_PPS_RPI121);
    PPSLock;

    return;
}

void config_outputs(void)
{
    /* Outputs */
    TRIS_LED_R = DU_OUTPUT;
    TRIS_LED_RTD = DU_OUTPUT;
    TRIS_LED_TS_ON = DU_OUTPUT;
    /* Set output pin's status */
    LED_R = 0;
    LED_RTD = 0;
    LED_TS_ON = 0;

    return;
}