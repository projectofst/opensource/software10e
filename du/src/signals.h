#ifndef __SIGNALS_H
#define __SIGNALS_H

#include <xc.h>
#include <stdbool.h>

#include "lib_pic33e/pps.h"

#define DU_OUTPUT 0
#define DU_INPUT 1

/* config */

/* outputs */
#define TRIS_LED_R TRISDbits.TRISD1
#define TRIS_LED_RTD TRISBbits.TRISB10
#define TRIS_LED_TS_ON TRISBbits.TRISB4
/* outputs */
#define LED_R LATDbits.LATD1
#define LED_RTD LATBbits.LATB10
#define LED_TS_ON LATBbits.LATB4

void do_the_pps(void);
void config_outputs(void);
#endif