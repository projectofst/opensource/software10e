#ifndef __ADC_CONFIG_H__
#define __ADC_CONFIG_H__

#include <stdint.h>

#include "can-ids/can_ids.h"
#include "lib_pic33e/adc.h"
#include "data_struct.h"

#define N_SENSORS 2
#define LEFT 0
#define RIGHT 1

void adc_init(uint16_t vector);
void adc_average(void*);
void reset_adc(void);
void update_adc_values (du_data*);
void update_threshold_left();
void update_threshold_right();
#endif