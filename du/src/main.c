// FST Lisboa
// Dynamic Unit

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128 // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4 // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <stdbool.h>
#include <stdint.h>
#include <xc.h>
// NOTE: Always include timing.h
#include "adc_config.h"
#include "can-ids/can_cfg.h"
#include "can-ids/can_cmd.h"
#include "can-ids/can_ids.h"
#include "can-ids/common.h"
#include "data_struct.h"
#include "fcp.h"
#include "lib_pic33e/adc.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/timing.h"
#include "lib_pic33e/trap.h"
#include "shared/fifo/fifo.h"
#include "shared/scheduler/scheduler.h"
#include "signals.h"

#define FIFO_SIZE 64
uint32_t timer = 0;
can_t can_global;
du_data du;
Fifo _fifo;
Fifo* fifo = &_fifo;

void timer1_callback(void)
{
    timer++;

    return;
}

#ifdef FAKE
void control_rtd_led(uint16_t status)
{
}
#endif

#ifndef FAKE
void control_rtd_led(uint16_t status)
{
    LED_RTD = status;
}
#endif

#ifdef FAKE
void control_ts_on_led(uint16_t status)
{
}
#endif

#ifndef FAKE
void control_ts_on_led(uint16_t status)
{
    LED_TS_ON = status;
}
#endif

#ifdef FAKE
void toggle_led(void)
{
}
#endif

#ifndef FAKE
void toggle_led(void)
{
    LED_R ^= 1;
    LED_RTD ^= 1;
    LED_TS_ON ^= 1;

    return;
}
#endif

void CAN_messaging(void)
{
    uint16_t status_rtd = 0;
    uint16_t status_ts_on = 0;
    msg_iib_iib_car_t iib_car;
    msg_master_master_status_t master_status;
    if (!can1_rx_empty()) {

        CANdata msg = pop_can1();
        if ((msg.msg_id == MSG_ID_COMMON_SEND_CMD) || (msg.msg_id == MSG_ID_COMMON_REQ_GET) || (msg.msg_id == MSG_ID_COMMON_REQ_SET)) {
            printf("enviar can 2\n");
            send_can2(msg);
            cmd_dispatch(msg);
        }
        if (msg.dev_id == DEVICE_ID_IIB) {
            if (msg.msg_id == MSG_ID_IIB_IIB_CAR) {
                //LED RTD position 6 size 1
                iib_car = decode_iib_car(msg);
                status_rtd = iib_car.car_rtd;
                control_rtd_led(status_rtd);
                //printf("\nstatus_rtd: %d\n", status_rtd);
            }
        }

        if (msg.dev_id == DEVICE_ID_MASTER) {
            if (msg.msg_id == MSG_ID_MASTER_MASTER_STATUS) {
                //LED TS ON position 12 size 1
                master_status = decode_master_status(msg);
                status_ts_on = master_status.master_status_sdc_above;
                control_ts_on_led(status_ts_on);
                //printf("\nstatus_Ts_on: %d\n", status_ts_on);
            }
        }
    }

    if (!can2_rx_empty()) {
        CANdata msg = pop_can2();
        printf("recebi %d \n", msg.msg_id);
        send_can1(msg);
    }

    return;
}

/*
 * Initialize scheduler, I/O and CAN
 */

void du_init(void)
{
    tasks_t du_tasks[] = {
        { .period = 1000, .func = toggle_led, .args_on = 0 },
        { .period = 0, .func = CAN_messaging, .args_on = 0 },
        { .period = 100, .func = adc_average, .args_on = 1, .args = (void*)&du },
        { .period = 1, .func = can_dispatch, .args_on = 1, .args = (void*)fifo }
    };

    scheduler_init_args(du_tasks, sizeof(du_tasks) / sizeof(tasks_t));
    config_timer1(1, 4);

    config_outputs();
    do_the_pps();
    /*
  *  WARNING: 
  *  AN2 -> ADC LEFT 
  *  AN3 -> ADC RIGHT
  */
    adc_init(AN2 | AN3);

    config_can1(NULL);
    config_can2(NULL); // IN DEFAULT -> default_can_config (it means that it will work)

    return;
}

int main()
{
    CANdata msgs[FIFO_SIZE];
    CANdata* msg;
    fifo_init(fifo, msgs, FIFO_SIZE);
    du_init();

    /*msg->dev_id = 22;
    msg->msg_id = 1;
    msg->dlc = 4;
    msg->data[0] = 1;
    msg->data[1] = 2;
    msg->data[2] = 3;
    msg->data[3] = 4;

    send_can1(msg);*/
    while (1) {

        scheduler(timer);
        du_front_send_msgs(can_global.du_front, timer);

        ClrWdt(); //Clear watchdog timer

    } //end while

    return 0;
} //end main

void trap_handler(TrapType type)
{
    switch (type) {
    case HARD_TRAP_OSCILATOR_FAIL:
        break;
    case HARD_TRAP_ADDRESS_ERROR:
        break;
    case HARD_TRAP_STACK_ERROR:
        break;
    case HARD_TRAP_MATH_ERROR:
        break;
    case CUSTOM_TRAP_PARSE_ERROR:
        break;
    default:
        break;
    }

    return;
}
