/* Include Guard */
#ifndef _DATA_STRUCT_H_
#define _DATA_STRUCT_H_

/*
 * Data structures representing the entire battery current state.
 */
#include <stdbool.h>
#include <stdint.h>

typedef struct du_struct{
    int16_t adc_read_left;
    int16_t adc_read_right;
} du_data;

#endif