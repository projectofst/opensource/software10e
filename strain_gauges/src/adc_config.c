#ifndef NOADC

#include "adc_config.h"
#include "can-ids/can_ids.h"
#include "lib_pic33e/adc.h"
#include "shared/run_avg/run_avg.h"

uint16_t adc_data[N_SENSORS];
extern can_t can_global;
run_avg_t run_avgs[N_SENSORS];

void adc_init(uint16_t vector)
{
    uint32_t i;
    ADC_parameters adc_config;

    config_pin_cycling(vector, 10UL, 64, 4, &adc_config);
    config_adc1(adc_config);

    CNPDB = vector;
    for (i = 0; i < N_SENSORS; i++) {
        run_avgs[i].max_size = 16;
        run_avg_config(&run_avgs[i]);
    }

    return;
}

#ifndef FAKE
void adc_average(void* sg_adcs)
{
    uint32_t i;
    sg_data* _sg_adcs = (sg_data*)sg_adcs;

    get_pin_cycling_values(adc_data, 0, N_SENSORS);

    for (i = 0; i < N_SENSORS; i++) {
        run_avg_add(&run_avgs[i], adc_data[i]);
    }
    _sg_adcs->adc_read_1 = run_avg_average(&run_avgs[0]);
    _sg_adcs->adc_read_2 = run_avg_average(&run_avgs[1]);
    _sg_adcs->adc_read_3 = run_avg_average(&run_avgs[2]);
    _sg_adcs->adc_read_4 = run_avg_average(&run_avgs[3]);
    _sg_adcs->adc_read_5 = run_avg_average(&run_avgs[4]);
    _sg_adcs->adc_read_6 = run_avg_average(&run_avgs[5]);

    update_adc_values(_sg_adcs);

    return;
}
#endif

#ifdef FAKE
void adc_average(void* sg_adcs)
{
    uint32_t i;
    sg_data* _sg_adcs = (sg_data*)sg_adcs;

    _sg_adcs->adc_read_1 = 400;
    _sg_adcs->adc_read_2 = 400;
    _sg_adcs->adc_read_3 = 400;
    _sg_adcs->adc_read_4 = 400;
    _sg_adcs->adc_read_5 = 400;
    _sg_adcs->adc_read_6 = 400;

    update_adc_values(_sg_adcs);

    return;
}
#endif

void update_adc_values(sg_data* sg_adcs)
{
    can_global.strain_gauges.sg_adc_values_1.adc_values_1 = sg_adcs->adc_read_1;
    can_global.strain_gauges.sg_adc_values_1.adc_values_2 = sg_adcs->adc_read_2;
    can_global.strain_gauges.sg_adc_values_1.adc_values_3 = sg_adcs->adc_read_3;
    can_global.strain_gauges.sg_adc_values_1.adc_values_4 = sg_adcs->adc_read_4;
    can_global.strain_gauges.sg_adc_values_2.adc_values_5 = sg_adcs->adc_read_5;
    can_global.strain_gauges.sg_adc_values_2.adc_values_6 = sg_adcs->adc_read_6;
}

#endif
