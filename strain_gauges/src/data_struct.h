/* Include Guard */
#ifndef _DATA_STRUCT_H_
#define _DATA_STRUCT_H_

/*
 * Data structures representing the entire battery current state.
 */
#include <stdbool.h>
#include <stdint.h>

#define N_TASKS 3

typedef struct sg_struct{
    uint16_t adc_read_1;
    uint16_t adc_read_2;
    uint16_t adc_read_3;
    uint16_t adc_read_4;
    uint16_t adc_read_5;
    uint16_t adc_read_6;
} sg_data;

#endif