// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128 // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4 // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <stdbool.h>
#include <stdint.h>
#include <xc.h>
// NOTE: Always include timing.h
#include "lib_pic33e/timing.h"

// Trap handling
#include "adc_config.h"
#include "can-ids/can_cfg.h"
#include "can-ids/can_cmd.h"
#include "can-ids/can_ids.h"
#include "can-ids/common.h"
#include "data_struct.h"
#include "lib_pic33e/adc.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/pps.h" // added this
#include "lib_pic33e/timer.h"
#include "lib_pic33e/trap.h"
#include "shared/fifo/fifo.h"
#include "shared/scheduler/scheduler.h"

#define FIFO_SIZE 64

uint32_t timer = 0;

can_t can_global;
sg_data sg_adcs;
Fifo _fifo;
Fifo* fifo = &_fifo;

uint16_t dev_get_id()
{

    return DEVICE_ID_STRAIN_GAUGES;
}

void dev_send_msg(CANdata msg)
{
    append_fifo(fifo, msg);
}

void can_dispatch(Fifo* fifo1)
{
    CANdata* msg;
    Fifo* fifo = (Fifo*)fifo1;

    uint16_t available = can2_tx_available();
    for (; available > 0 && !fifo_empty(fifo); available--) {
        //LED5 ^= 1;
        msg = fifo_pop(fifo);
        if (msg == NULL) {
            /* Should never happen */
            break;
            ;
        }
        printf("Send\n");
        send_can2(*msg);
    }

    return;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{

    multiple_return_t mt;

    return mt;
}

void timer1_callback(void)
{
    timer++;

    return;
}

void ligh_led(void)
{

    return;
}

void receive_CAN_message(void)
{

    if (!can2_rx_empty()) {
        CANdata msg = pop_can2();
        printf("teste1\n%d\n", msg.msg_id);
    }

    return;
}

int main()
{
    CANdata msgs[FIFO_SIZE];
    fifo_init(fifo, msgs, FIFO_SIZE);

    tasks_t strain_gauges_tasks[] = {
        { .period = 1000, .func = ligh_led, .args_on = 0 },
        { .period = 0, .func = receive_CAN_message, .args_on = 0 },
        { .period = 50, .func = adc_average, .args_on = 1, .args = (void*)&(sg_adcs) },
        { .period = 1, .func = can_dispatch, .args_on = 1, .args = (void*)fifo }
    };

    scheduler_init_args(strain_gauges_tasks, sizeof(strain_gauges_tasks) / sizeof(tasks_t));
    config_timer1(1, 4);

#ifndef FAKE
    //CAN
    PPSUnLock;
    // Output configuration based in the README
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
    // Input configuration based in the README
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
    PPSLock;

    adc_init(AN10 | AN11 | AN12 | AN13 | AN14 | AN15);
#endif
    config_can2(NULL); // IN DEFAULT -> default_can_config (it means that it will work)

    while (1) {

        scheduler(timer);

        strain_gauges_send_msgs(can_global.strain_gauges, timer);

        //Clear watchdog timer
        ClrWdt();
    } //end while

    return 0;
} //end main

void trap_handler(TrapType type)
{

    switch (type) {
    case HARD_TRAP_OSCILATOR_FAIL:
        break;
    case HARD_TRAP_ADDRESS_ERROR:
        break;
    case HARD_TRAP_STACK_ERROR:
        break;
    case HARD_TRAP_MATH_ERROR:
        break;
    case CUSTOM_TRAP_PARSE_ERROR:
        break;
    default:
        break;
    }

    return;
}
