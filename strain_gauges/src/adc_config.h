#ifndef __ADC_CONFIG_H__
#define __ADC_CONFIG_H__

#include <stdint.h>
#include "can-ids/can_ids.h"
#include "lib_pic33e/adc.h"
#include "data_struct.h"

#define N_SENSORS 6

void adc_init(uint16_t vector);
void adc_average(void*);
void update_adc_values (sg_data*);

#endif
