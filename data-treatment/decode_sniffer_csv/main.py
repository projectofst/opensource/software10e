import json
import click
from fst_cantools import Message
from fcp import Fcp, Spec


def get_fcp(fcp_json):
    with open(fcp_json) as f:
        j = f.read()

    j = json.loads(j)
    spec = Spec()
    spec.decompile(j)
    fcp = Fcp(spec)

    return fcp


@click.group(invoke_without_command=True)
@click.argument("file")
@click.argument("fcp_json")
@click.option("--verbose/--noverbose", default=False)
def main(file, fcp_json, verbose):

    fcp = get_fcp(fcp_json)
    f = open(file)

    signals_d = {}

    for line in f.readlines():
        data = []
        sp = line.split(",")
        if len(sp) != 7:
            continue
        sid = int(sp[0])
        dlc = int(sp[1])
        data.append(int(sp[2]))
        data.append(int(sp[3]))
        data.append(int(sp[4]))
        data.append(int(sp[5]))
        timestamp = int(sp[6])

        msg = Message(int(sid), int(dlc), data, timestamp)

        fcp_msg = fcp.find_msg(msg)
        if fcp_msg == None:
            continue

        fcp_signals = fcp_msg.signals
        message, signals = fcp.decode_msg(msg)

        if message == "dash_se":
            print(msg)

        for name, sig in signals.items():
            if fcp_signals[name].mux_count > 1:
                mux_value = signals[fcp_signals[fcp_signals[name].mux].name]
                sig_name = name + str(int(mux_value))
            else:
                sig_name = name

            if sig_name not in signals_d.keys():
                signals_d[sig_name] = []

            signals_d[sig_name].append((timestamp, sig))

    for name, l in signals_d.items():
        with open("logs/" + name + ".csv", "w") as f:
            f.write(f"time, value\n")
            for time, value in l:
                f.write(f"{time}, {value}\n")


if __name__ == "__main__":
    main()
