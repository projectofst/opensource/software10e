file = uigetdir();
files = dir(file);

for i=1:length(files)
    if files(i).name == '.'
        files(i).name
        continue
    end
    table = readtable(fullfile(file, files(i).name));
    [~,name,~] = fileparts(files(i).name);
    eval(strcat(name,".time=table.time;"));
    eval(strcat(name,".value=table.value;"));

end

clear table
clear name
clear file
clear files
clear i
clear ans
save(datestr(datetime('now')))