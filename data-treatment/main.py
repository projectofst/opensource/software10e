import click
import logging
import json

from fcp.spec import Spec
from fcp.validator import validate

from fcp_mock import Fcp
from can import Message


def get_spec(json_file: str, logger: logging.Logger) -> Spec:
    """Create Spec from json file path.
    :param json_file: path to the json file.
    :param logger: logger.
    :return: Spec.
    """

    spec = Spec()
    with open(json_file) as f:
        j = json.loads(f.read())

    r, msg = validate(logger, j)
    if not r:
        print(msg)
        exit()

    spec.decompile(j)

    return spec


def setup_logging() -> logging.Logger:
    """Setup Logger.

    :return: logger object.
    """

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    handler = logging.FileHandler("fcp.log")
    handler.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


@click.group(invoke_without_command=True)
@click.argument("path")
@click.argument("fcp_json")
def main(path, fcp_json):
    logger = setup_logging()

    spec = get_spec(fcp_json, logger)

    fcp = Fcp(spec)

    f = open(path, "r")

    for line in f.readlines():
        try:
            msg = Message.decode_csv(line)
        except Exception as e:
            continue

        name, signals = fcp.decode_msg(msg)
        if name == "":
            continue

        ams_latch = 0
        imd_latch = 0
        apps_bps_timer = 0

        if name == "MASTER_STATUS":
            imd_latch += signals["master_status_imd_latch"]
            ams_latch += signals["master_status_ams_latch"]
        elif name == "te_main":
            apps_bps_timer += signals["te_status_imp_apps_bps"]
            print(signals["te_main_APPS"], signals["te_main_BPSp"])

    print("ams: ", ams_latch)
    print("imd: ", imd_latch)
    print("apps_bps_timer: ", apps_bps_timer)


if __name__ == "__main__":
    main()
