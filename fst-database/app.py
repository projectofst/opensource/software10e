#!/usr/bin/env python3
import tkinter as tk
from tkinter import filedialog, Text, messagebox
import os


# Fields present in the All Run Tables. Add a new field here, and it will appear in the input window.
fields = (
    "Location",
    "Date (YYYY-MM-DD HH:MM:SS)",
    "Pilot Name",
    "Test Type",
    "Youtube URL",
    "CommitCode",
)
filenames = []


# Class that concerns the Run information input
class insertWindow(tk.Toplevel):
    def __init__(self, master=None):
        super().__init__(master=master)

        ## kill master
        master.destroy()

        self = tk.Tk()
        self.title("Insert Log in FST Database")
        intro = tk.Text(self, height=2, width=100, bg=self.cget("bg"))
        intro.pack()
        intro.insert(
            tk.END, "To insert a log in the database please upload a Csv and Json File."
        )

        ### Responsible for parsing the var fields
        ents = insertWindow.makeform(self, fields)
        self.bind("<Return>", (lambda event, e=ents: tk.fetch(e)))

        ### Add and Insert Buttons
        b1 = tk.Button(
            self,
            text="Add File",
            command=(lambda e=ents: insertWindow.addFile(self, e)),
        )
        b1.pack(side=tk.LEFT, padx=5, pady=5)
        b2 = tk.Button(
            self, text="Insert", command=(lambda e=ents: insertWindow.insertDB(self, e))
        )
        b2.pack(side=tk.LEFT, padx=5, pady=5)

        ## check box
        self.exportMat = tk.IntVar()
        checkBox = tk.Checkbutton(
            self, text="Insert & Export .mat", variable=self.exportMat
        )
        checkBox.pack(side=tk.LEFT, padx=5, pady=5)

        self.mainloop()

    ### Function responsible for adding the  json and csv file
    def addFile(self, entries):

        filename = filedialog.askopenfilename(
            initialdir="./",
            title="CSV",
            filetypes=(
                ("csv files", "*.csv"),
                ("json Files", "*.json"),
                ("all files", "*."),
            ),
        )
        extension = filename.split("/")[-1].split(".")[-1]
        if extension == "csv":
            csv = tk.Label(
                self, text="Added Csv File:" + filename.split("/")[-1], bg="green"
            )
            csv.pack(side=tk.BOTTOM, padx=2, pady=5)
            filenames.append(filename)

        else:
            json = tk.Label(
                self, text="Added Json File: " + filename.split("/")[-1], bg="green"
            )
            json.pack(side=tk.BOTTOM, padx=2, pady=5)
            filenames.append(filename)

    """ Function that calls the insertDB.py script. It merges the csv and json files with the other inputs and passes them as arguments
    """

    def insertDB(self, entries):
        csv = ""
        jsonFile = ""

        ## parsed the csv and json files
        for f in filenames:
            extension = f.split("/")[-1].split(".")[-1]
            if extension == "csv":
                csv = f
            else:
                jsonFile = f

        ## Handling Missing Files
        if csv == "":
            messagebox.showerror("File Missing", "No Csv File")
        elif jsonFile == "":
            messagebox.showerror("File Missing", "No Json File")
        else:
            data = {}

            ## fill dictionary with input from the variable fields. They object entries has the value from the input
            for ent in entries:
                data[ent] = entries[ent].get()

            ## Adds the json and csv
            data["csvFile"] = csv.split("/")[-1].split(".")[0]
            data["jsonFile"] = jsonFile.split("/")[-1].split(".")[0]

            ## run insertDB.py (insert script)
            command = "python3 scripts/insertDB.py %s %s %s %s" % (
                csv,
                jsonFile,
                self.exportMat.get(),
                [str(data)],
            )
            os.system(command)

            # close window
            self.destroy()

    ### Function responsible for the input buttons (All Runs Info)
    def makeform(self, fields):
        entries = {}
        for field in fields:
            row = tk.Frame(self)
            lab = tk.Label(row, width=30, text=field + ": ")
            ent = tk.Entry(row)
            row.pack(side=tk.TOP, fill=tk.X, padx=5, pady=5)
            lab.pack(side=tk.LEFT)
            ent.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.X)
            entries[field] = ent
        return entries


# Class that Manages the database
class handleDatabase(tk.Toplevel):
    def __init__(self, master=None):
        super().__init__(master=master)

        ## kill master
        master.destroy()

        self = tk.Tk()
        self.title("Manage FST Database")

        ## Create Table Button
        b1 = tk.Button(self, text="Create Tables")
        b1.bind("<Button>", lambda e: handleDatabase.createTables(self, e))
        b1.pack(side=tk.LEFT, padx=5, pady=5)

        ## Delete All Table Button
        b2 = tk.Button(self, text="Delete All Tables")
        b2.bind("<Button>", lambda e: handleDatabase.deleteTables(self, e))
        b2.pack(side=tk.LEFT, padx=5, pady=5)

        ## Remove Log Button
        b3 = tk.Button(self, text="Remove Log")
        b3.bind("<Button>", lambda e: handleDatabase.handleRemoveFromDB(self, e))
        b3.pack(side=tk.LEFT, padx=5, pady=5)

        self.mainloop()

    ## Calls the create handleDatabase script to create the tables. Useful when the SQL is changed
    def createTables(self, e):

        command = "python3 scripts/handleDatabase.py %s %s" % ("create", "0")
        os.system(command)
        self.destroy()

    ## Calls the create handleDatabase script to delete the tables. Useful when the SQL is changed
    def deleteTables(self, e):

        command = "python3 scripts/handleDatabase.py %s %s " % ("delete", "0")
        os.system(command)

        self.destroy()

    def removeLogFromDb(self, entry):

        runID = entry.get()
        command = "python3 scripts/handleDatabase.py %s %s" % ("remove", str(runID))
        os.system(command)

        self.destroy()

    ## Calls the create handleDatabase script to remove a log from the Db.
    def handleRemoveFromDB(self, e):

        # destroy before
        self.destroy()

        self = tk.Tk()
        self.title("FST Database: Remove Log")

        tk.Label(self, text="Insert log ID to remove:").pack()
        entry = tk.Entry(self)
        entry.bind("<Return>", (lambda event, e=entry: tk.fetch(e)))
        entry.pack()
        res = tk.Label(self)
        res.pack()

        removeButton = tk.Button(self, text="Remove Log")
        removeButton.bind(
            "<Button>", lambda e: handleDatabase.removeLogFromDb(self, entry)
        )
        removeButton.pack(side=tk.LEFT, padx=5, pady=5)

        self.mainloop()


if __name__ == "__main__":

    # creates a Tk() bject
    master = tk.Tk()
    master.title("FST Database")

    insertButton = tk.Button(master, text="Open Insert Interface")
    manageButton = tk.Button(master, text="Manage Database")

    manageButton.pack(side=tk.LEFT, padx=5, pady=5)
    manageButton.bind("<Button>", lambda e: handleDatabase(master))

    insertButton.pack(side=tk.RIGHT, padx=5, pady=5)
    insertButton.bind("<Button>", lambda e: insertWindow(master))

    master.mainloop()
