Requirements to run the app:

    Run: ./requirements
    
Database Credentials: 

    user= ist187694 
    passwd= jcpw3847 
    host= db.tecnico.ulisboa.pt
    database= ist187694

To insert a log  or manage the database run:

    python3 app.py
    
Alternatively one can run the executable:
    
    appExec
    
Note: When running the app.py through ssh it may only work on Linux Distributions with the ssh -X flag.
On Windows it may have problems with the Display needed to lauch the GUI.
Running the script locally also works.

An interface will be prompted where the user will need to select the desired log(csv) and json.

![picture](structure/app1.png)

Clicking in the Open Insert Interface Button, it will display the following:

![picture](structure/insert.png)

Add a Csv File and the Json file. A Date it is also required. Additionally you can insert and export the data to a .mat file.

Clicking in the Manage Database Button, it will display the following:

![picture](structure/manageDB.png)

The Create Tables Button is useful when one has changed the SQL structure of the Database.
The Delete  All Tables deletes the content of all tables. To insert data in the tables, it needs first the tables to be created (Create Tables Button)

To remove a specific log click Remove Log:

![picture](structure/removeLog.png)

Insert the correspondent ID of the log. 


To acess Grafana (Please note that the IP is dinamic, change it accordingly):
    
    ssh lv_room@10.16.233.110 

Start Grafana Server:
    
     systemctl start grafana

Then on your Web Browser (IP + 3000):

    http://10.16.233.110:3000/


Grafana Credentials:

    user: admin
    password: same used to acess lv_room@ip

To Restart Grafana:

     systemctl restart grafana
     
To Stop Grafana:
    
     systemctl stop grafana

    
    

