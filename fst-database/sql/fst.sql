drop table IF EXISTS run_info cascade;
drop table IF EXISTS allRuns cascade;
drop table IF EXISTS run cascade;
create table allRuns (runID   integer not null,
    location  varchar(80),
    date timestamp,
    pilotName varchar(30),
    testType varchar(50),
    youtubeURL varchar(80),
    csvFile varchar(80) not null,
    jsonFile varchar(80) not null,
    commitCode varchar(180) not null,
    constraint pk_allRuns primary key(runID));
create table run ( runID integer not null,
    time_stamp 	float not null,
    signalName  varchar(60)	not null,
    val  float	not null,
    constraint pk_run primary key(runID, time_stamp, signalName));
create table run_info (runID integer not null,
    runID_allRuns integer not null,
    constraint pk_run_info primary key(runID),
    constraint fk_runID_1 foreign key(runID) references run(runID),
    constraint fk_rundID_2 foreign key(runID_allRuns) references allRuns(runID));

