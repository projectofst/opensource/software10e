import json
import click
from fcp import Fcp, Spec
import fcp
import pymysql
import ast
from scipy.io import savemat


def get_fcp(fcp_json):
    with open(fcp_json) as f:
        j = f.read()

    j = json.loads(j)
    spec = Spec()
    spec.decompile(j)
    fcp = Fcp(spec)
    return fcp


""" Run before 
sudo mysql --user=root
CREATE USER 'root'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION; """


## Reads the input received from app.py. It concerns the ALl Runs Info
def readInput(inputData):
    data = inputData.replace("[", "").replace("]", "")
    data = ast.literal_eval(data)

    location = data["Location"]
    date = data["Date (YYYY-MM-DD HH:MM:SS)"]
    pilotName = data["Pilot Name"]
    testType = data["Test Type"]
    csv = data["csvFile"]
    jsonFile = data["jsonFile"]
    youtubeURL = data["Youtube URL"]
    commitCode = data["CommitCode"]

    return location, date, pilotName, testType, csv, jsonFile, youtubeURL, commitCode


def insertDb(signals_d, inputData, exportmat):

    connection = pymysql.connect(
        user="g03285_logsDB",
        passwd="fstlisboa",
        host="db.tecnico.ulisboa.pt",
        database="g03285_logsDB",
    )

    try:
        cursor = connection.cursor()

        # prepare insert statements
        insertRunTable = "INSERT INTO run (runID, time_stamp, signalName, val) VALUES (%s, %s, %s, %s)"
        insertRunInfo = "INSERT INTO run_info (runID, runID_allRuns) VALUES (%s, %s)"
        insertAllRuns = "INSERT INTO allRuns (runID, location, date, pilotName, testType, csvFile, jsonFile, youtubeURL, commitCode) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"

        # read last log inserted information
        (
            location,
            date,
            pilotName,
            testType,
            csvFile,
            jsonFile,
            youtubeURL,
            commitCode,
        ) = readInput(inputData)

        ## Increment Run ID
        runId = cursor.execute("select distinct(runID) from run;")
        runId += 1

        # Add row in All Runs Table
        allRuns = (
            runId,
            location,
            date,
            pilotName,
            testType,
            csvFile,
            jsonFile,
            youtubeURL,
            commitCode,
        )
        cursor.execute(insertAllRuns, allRuns)
        print("Inserted in allRuns Table")

        runInfo = []
        exportMatInfo = {}
        exportmat = eval(exportmat)

        # parse run info to sql format
        print("Inserting in run Table:  Please Wait")
        for name, l in signals_d.items():

            if exportmat:
                exportMatInfo[name] = l

            for time, value in l:
                # Add row in run Table
                runInfo.append([runId, time, name, value])

        # add run in the run Table
        cursor.executemany(insertRunTable, runInfo)
        print("Inserted in run Table")

        # Add row in run_info Table
        row_info = (runId, runId)
        cursor.execute(insertRunInfo, row_info)
        print("Inserted in run_info Table")

        # Commit into the Database
        connection.commit()

        # Export to .Mat
        if exportmat:
            print("Exported to .mat")
            savemat("./matFiles/log.mat", exportMatInfo)

    except Exception as e:
        print("Exception occured while inserting main.py:{}".format(e))
        connection.rollback()

        return

    finally:
        print("Inserted in Database")
        connection.close()


@click.group(invoke_without_command=True)
@click.argument("file")
@click.argument("fcp_json")
@click.argument("exportMat")
@click.argument("data")
@click.option("--verbose/--noverbose", default=False)
def main(file, fcp_json, exportmat, data, verbose):
    fcp_handle = get_fcp(fcp_json)
    f = open(file)
    signals_d = {}

    ## Freitas Script
    for line in f.readlines():
        sp = line.split(",")
        timestamp = sp[0]
        sid = sp[2]
        dlc = sp[4]
        d8 = sp[5 : 5 + 8]
        d8 = [d if d != "" else 0 for d in d8]
        d4 = []
        for i in range(4):
            d4.append(int(d8[2 * i]) + (int(d8[2 * i + 1]) << 8))

        # added
        if sid == "" or dlc == "":
            continue

        msg = fcp.can.CANMessage(int(sid), int(dlc), timestamp, data16=d4)

        fcp_msg = fcp_handle.find_msg(msg)
        if fcp_msg == None:
            continue

        fcp_signals = fcp_msg.signals
        message, signals = fcp_handle.decode_msg(msg)

        if message == "dash_se":
            print("dash_se msg: ", msg)

        for name, sig in signals.items():
            if fcp_signals[name].mux_count > 1:
                mux_value = signals[fcp_signals[fcp_signals[name].mux].name]
                sig_name = name + str(int(mux_value))
            else:
                sig_name = name

            if sig_name not in signals_d.keys():
                signals_d[sig_name] = []

            signals_d[sig_name].append((timestamp, sig))

    ## Insert in the DB (new part)
    insertDb(signals_d, data, exportmat)


if __name__ == "__main__":
    main()
