import pymysql, sys
from configDatabase import read_config


## Responsible for creating the tables. Useful when the SQL is changed
def createTables(connection, databaseName, runID):

    cursor = connection.cursor()

    sql = "USE " + databaseName
    cursor.execute(sql)

    sql = ""
    for line in open("./sql/fst.sql"):
        sql += line.replace("\n", "")

    sql = sql.replace(";", "; \n")

    for cmd in sql.split("\n"):
        if cmd != "":
            cursor.execute(cmd)

    print("CREATED TABLES")


## Responsible for deleting the tables. Useful when the SQL is changed
def deleteTables(connection, databaseName, runID):

    cursor = connection.cursor()

    sql = "USE " + databaseName
    cursor.execute(sql)

    deleteQuery = "DELETE FROM run_info;"
    cursor.execute(deleteQuery)

    deleteQuery = "DELETE FROM run;"
    cursor.execute(deleteQuery)

    deleteQuery = "DELETE FROM allRuns;"
    cursor.execute(deleteQuery)

    connection.commit()
    print("Tables Deleted")


## Responsible for removing a log from the DB.
def removeFromDatabase(connection, databaseName, runID):

    cursor = connection.cursor()

    sql = "USE " + databaseName
    cursor.execute(sql)

    deleteQuery = "DELETE FROM run_info WHERE runID = " + runID + ";"
    cursor.execute(deleteQuery)

    deleteQuery = "DELETE FROM run WHERE runID = " + runID + ";"
    cursor.execute(deleteQuery)

    deleteQuery = "DELETE FROM allRuns WHERE runID = " + runID + ";"
    cursor.execute(deleteQuery)

    connection.commit()
    print("Log Removed")


## It calls the right function depending on the flag received.
def executeFunction(cmd, connection, databaseName, runID):

    functions = {
        "create": createTables,
        "delete": deleteTables,
        "remove": removeFromDatabase,
    }
    func = functions.get(cmd)

    func(connection, databaseName, runID)


def main():

    cmd = sys.argv[1]

    ## used only when removing
    runID = sys.argv[2]
    config = read_config()
    databaseName = config["database"]
    connection = pymysql.connect(
        user=config["user"],
        passwd=config["password"],
        host=config["host"],
        database=config["database"],
    )

    try:
        executeFunction(cmd, connection, databaseName, runID)

    except Exception as e:
        print("Exception {}".format(e))


if __name__ == "__main__":
    main()
