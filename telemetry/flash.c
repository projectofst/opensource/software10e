/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "lib/sockets.h"
#include "lib/can-ids/CAN_IDs.h"
#include "lib/can-ids/Devices/telemetry.h"
#include "lib/server-side-constants.h"
#include <signal.h>
#include "lib/errors.h"
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include "lib/hash_table.h"

char* can0 = "vcan0";
char* can1 = "vcan1";
int cansockfd0, cansockfd1;
FILE* flash_messages_file;
int flash_fd;
int stat;
/*sd keep track of weather or not the flash has been modified so that we dont write
  to the SD card that often*/
int flash_modified = 0;
node** nodes;

int parseArgs(int argc, char** argv){
	return 0;
}

void status(){
	CANmessage statusMessage;

	struct can_frame frame;

	statusMessage.candata.dev_id = DEVICE_ID_TELEMETRY;
	statusMessage.candata.msg_id = MSG_ID_TELEMETRY_STATUS;
	statusMessage.candata.dlc = 2;

	statusMessage.candata.data[0] = stat;
	statusMessage.candata.data[1] = 0;
	statusMessage.candata.data[2] = 0;
	statusMessage.candata.data[3] = 0;

	CANMessage_to_CANFrame(statusMessage, &frame);
	send_to_CAN_Bus_Line(cansockfd0, frame);
	send_to_CAN_Bus_Line(cansockfd1, frame);
	alarm(5);
	return;
}



/*This could be done in a more sophisticated way inside the lib/hash_table.c, but
  aint nobody got time for that. This is a simple enough way*/
void save_values(){

	status();
	if(flash_modified == 0){
		alarm(5);
		return;
	}

	/*open the file on write mode for it to be erased before writing*/
	flash_messages_file = fopen("dummy-flash.txt", "w");

	/*iterate through all of the entries*/
	for(int device_counter = 0; device_counter < 32; device_counter++){
		/*Get the first node of the current entry*/
		node* head_node = nodes[device_counter];


		/*If there is no value on the first node just skip*/
		if(head_node == NULL){
			continue;
		}
		else{
			node* current_node = head_node;
			/*keep going as long as there are nodes left*/
			while(current_node != NULL){
				fprintf(flash_messages_file,"%d,%d,%d\n", device_counter,
						current_node->parameter, current_node->value);
				current_node = current_node->next;
			}
		}
	}

	/*reset the flash_modified bit*/
	flash_modified = 0;
	fflush(flash_messages_file);
	fclose(flash_messages_file);

	alarm(5);
	return;
}


void send_all_stored_messages(){


	return;
}


void process_READ_operation(CANmessage message){
	node * node_found = find_node(message);
	struct can_frame frame;

	if(!node_found){
		return;
	}

	message.candata.dev_id = DEVICE_ID_TELEMETRY;
	message.candata.data[1] = node_found->value;
	message.candata.data[2] = 0;
	message.candata.data[3] = 0;


	CANMessage_to_CANFrame(message, &frame);
	send_to_CAN_Bus_Line(cansockfd0, frame);
	send_to_CAN_Bus_Line(cansockfd1, frame);
	return;
}

void line_to_CANMessage(char* line, CANmessage* message){
	const char delim[2] = ",";
	char* token;

	token = strtok(line, delim);
	message->candata.dev_id = atoi(token);

	token = strtok(NULL, delim);
	message->candata.data[0] = atoi(token);

	token = strtok(NULL, delim);
	message->candata.data[1] = atoi(token);

	return;
}

void insert_into_hash(CANmessage message){
	node* new_node = create_node(message);

	int key = message.candata.dev_id;
	nodes[key] = insert_node(key, new_node);

	return;
}

void process_WRITE_operation(CANmessage message){
	struct can_frame frame;
	node* node_found = find_node(message);

	if(node_found == NULL){
		insert_into_hash(message);
	}
	else{
		node_found->value = message.candata.data[1];
	}

	message.candata.dev_id = DEVICE_ID_TELEMETRY;
	CANMessage_to_CANFrame(message, &frame);
	send_to_CAN_Bus_Line(cansockfd0, frame);
	send_to_CAN_Bus_Line(cansockfd1, frame);
	return;
}


void process_general_telemetry_message(CANmessage message){
	if(message.candata.msg_id == MSG_ID_TELEMETRY_FLASH){
		send_all_stored_messages();
	}
}

void parse_CAN_message(CANmessage message){
	switch(message.candata.msg_id){
		case CMD_ID_COMMON_READ_FLASH :
			process_READ_operation(message);
			break;
		case CMD_ID_COMMON_WRITE_FLASH :
			flash_modified = 1;
			process_WRITE_operation(message);
			break;
		default:
			if(message.candata.dev_id == DEVICE_ID_TELEMETRY){
				process_general_telemetry_message(message);
			}
			return;
	}
	return;
}

void* read_messages(void* args){
	int nbytes;
	int sockfd = (*(int*) args);
	struct can_frame frame;
	CANmessage message;

	for(;;){
		nbytes = recieve_from_CAN_Bus_Line(sockfd, &frame);
		if(nbytes != sizeof(struct can_frame)){
			return NULL;
		}
		CANFrame_to_CANMessage(frame, &message);
		parse_CAN_message(message);
	}
	return NULL;
}



void send_stored_flash_messages_to_CAN(char* line, int sock0, int sock1){
	/*file lines: device,parameter,value*/
	CANmessage message;
	struct can_frame frame;

	/*TODO: CHECK IF SENDING SET MSG ID WORKS*/
	message.candata.msg_id = CMD_ID_COMMON_SET;
	message.candata.dlc = 4;
	message.candata.data[2] = 0;
	message.candata.data[3] = 0;

	line_to_CANMessage(line, &message);

	CANMessage_to_CANFrame(message, &frame);

	insert_into_hash(message);

	send_to_CAN_Bus_Line(sock0, frame);
	send_to_CAN_Bus_Line(sock1, frame);
	return;
}



void init_hash_table(){
	nodes = (node**)malloc(sizeof(node*) * 32);
	for(int i = 0; i < 32; i++){
		nodes[i] = NULL;
	}
	return;
}


int send_flash_messages(int sockfd0, int sockfd1){
	char* line = NULL;
	size_t len = 0;
	ssize_t read;


	init_hash_table();

	flash_messages_file = fopen("dummy-flash.txt", "r");

	if(flash_messages_file == NULL){
		return INIT_FLASH_MESSAGES_ERROR;
	}

	while((read = getline(&line, &len, flash_messages_file) != -1)){
		send_stored_flash_messages_to_CAN(line, sockfd0, sockfd1);
	}

	fclose(flash_messages_file);
	return 0;
}

int main(int argc, char* argv[]){
	if(parseArgs(argc, argv)){
		exit(0);
	}

	cansockfd0 = connect_to_CAN(can0);
	cansockfd1 = connect_to_CAN(can1);

	int ret = send_flash_messages(cansockfd0, cansockfd1);

	if(ret == 0){
		stat = 1;
	}
	else{
		stat = ret;
	}


	signal(SIGALRM, save_values);
	alarm(5);

	pthread_t * read0 = (pthread_t*) malloc(sizeof(pthread_t));
	pthread_t * read1 = (pthread_t*) malloc(sizeof(pthread_t));

	pthread_create(read0, NULL, read_messages, (void *) &cansockfd0);
	pthread_create(read1, NULL, read_messages, (void *) &cansockfd1);

	pthread_join(*read0, NULL);
	pthread_join(*read1, NULL);


	return 0;
}
