/*
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <string.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <time.h>
#include "lib/server-side-constants.h"
#include "lib/sockets.h"
#include <pthread.h>
#include "lib/can-ids/CAN_IDs.h"
#include "lib/errors.h"
#include "lib/connection-handler.h"
#include "lib/timer.h"

char *can0 = "can0";
char *can1 = "can1";
int cansockfd0, cansockfd1;
int listedfd;

address *clients[MAX_CONNECTIONS];

void show_usage(char *program_name)
{
	printf("usage: %s [--help] [<command>] [<args>]\n", program_name);
}

void show_help(char *program_name)
{
	show_usage(program_name);
	printf("Server Commands:\n\n");
	printf("To get the server running reading from canbus lines with names can0 and can1 just run without any flags\n");
	printf("Run the server in debug mode, meaning it will default to virtual canbus line named vcan0 and vcan1\n\n");
	printf("\t -t\n");
	printf("Run the server in debug mode, but with custom canbus names:\n");
	printf("\t -t <name_1> <name_2>\n");
	return;
}

void parseArgs(int argc, char *argv[])
{
	if (argc > 4)
	{
		printf("Invalid number of arguments\n");
		show_usage(argv[0]);
		exit(SIGHUP);
	}

	switch (argc)
	{
	case 1:
		return;
	case 2:
		if ((!strcmp(argv[1], "-h")) || (!strcmp(argv[1], "--help")))
		{
			show_help(argv[0]);
			exit(SIGHUP);
		}
		else if (strcmp(argv[1], "-t"))
		{
			printf("Invalid Command.\n");
			show_usage(argv[0]);
			exit(SIGHUP);
		}
		can0 = "vcan0";
		can1 = "vcan1";
		return;
	case 3:
		printf("invalid number of arguments\n");
		show_usage(argv[0]);
		exit(SIGHUP);
	case 4:
		if (strcmp(argv[1], "-t"))
		{
			printf("Invalid Command.\n");
			show_usage(argv[0]);
			exit(SIGHUP);
		}
		can0 = argv[2];
		can1 = argv[3];
		return;
	default:
		printf("unknown option: %s\n", argv[1]);
		show_usage(argv[0]);
		exit(SIGHUP);
	}
}

void broadcast_CAN_message(CANmessage message)
{
	for (int i = 0; i < MAX_CONNECTIONS; i++)
	{
		/*skip null clients*/
		if (clients[i] == NULL)
		{
			continue;
		}
		send_CAN(listedfd, message, clients[i]);
	}
	return;
}

void *send_to_client(void *index)
{
	int sockfd;
	struct can_frame frame;
	CANmessage message;
	sockfd = (*(int *)index);
	struct timeval current_time2;
	struct timeval current_time1;
	unsigned long long int ms1;

	gettimeofday(&current_time1, NULL);
	ms1 = ((unsigned long long)current_time1.tv_sec * 1000 + (unsigned long long)current_time1.tv_usec / 1000);

	for (;;)
	{
		recieve_from_CAN_Bus_Line(sockfd, &frame);
		CANFrame_to_CANMessage(frame, &message);
		gettimeofday(&current_time2, NULL);
		message.timestamp += ((unsigned long long)current_time2.tv_sec * 1000 + (unsigned long long)current_time2.tv_usec / 1000) - ms1;

		broadcast_CAN_message(message);
	}
	printf("exit send_to_client\n");
	return NULL;
}

void *recieve_from_client()
{
	int nbytes;
	address *new_address;
	struct sockaddr_in client_address;
	int client_length = sizeof(client_address);

	struct can_frame frame;
	CANmessage message;
	for (;;)
	{
		nbytes = recieve_CAN(listedfd, &message, &client_address, &client_length);

		new_address = get_new_address(client_address, client_length);

		if (nbytes != sizeof(CANmessage))
		{
			printf("nbytes: %d\n", nbytes);
		}
		else
		{
			check_client(new_address);

			CANMessage_to_CANFrame(message, &frame);
			send_to_CAN_Bus_Line(cansockfd0, frame);
			send_to_CAN_Bus_Line(cansockfd1, frame);
			broadcast_CAN_message(message);
		}
	}
	return NULL;
}

void setup_clients(int cansockfd0, int cansockfd1)
{
	int error;
	pthread_t *senders_0 = (pthread_t *)malloc(sizeof(pthread_t));

	pthread_t *senders_1 = (pthread_t *)malloc(sizeof(pthread_t));
	pthread_t *recievers = (pthread_t *)malloc(sizeof(pthread_t));

	if (!senders_0 || !senders_1 || !recievers)
	{
		perror("SERVER: Error allocating memory. Exiting...\n");
		exit(EXIT_FAILURE);
	}

	error = pthread_create(senders_0, NULL, send_to_client, (void *)&cansockfd0);
	if (error != 0)
	{
		perror("SERVER: Error creating thread. Exiting...\n");
		exit(EXIT_FAILURE);
	}

	error = pthread_create(senders_1, NULL, send_to_client, (void *)&cansockfd1);
	if (error != 0)
	{
		perror("SERVER: Error creating thread. Exiting...\n");
		exit(EXIT_FAILURE);
	}

	error = pthread_create(recievers, NULL, recieve_from_client, (void *)&index);
	if (error != 0)
	{
		perror("SERVER: Error creating thread. Exiting...\n");
		exit(EXIT_FAILURE);
	}

	pthread_join(*senders_0, NULL);
	pthread_join(*senders_1, NULL);
	pthread_join(*recievers, NULL);
	return;
}

int main(int argc, char *argv[])
{

	parseArgs(argc, argv);

	signal(SIGALRM, check_active_connections);
	alarm(30);

	init_active_connections();
	/*get file drescriptors of both can BUS lines*/
	cansockfd0 = connect_to_CAN(can0);
	cansockfd1 = connect_to_CAN(can1);

	listedfd = init_AF_INET_socket();

	for (int i = 0; i < MAX_CONNECTIONS; i++)
	{
		clients[i] = NULL;
	}
	while (1)
		setup_clients(cansockfd0, cansockfd1);
	printf("Exiting Normally...\n");
	return 0;
}
