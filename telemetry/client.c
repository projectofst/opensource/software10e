/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 Client Example. This is an example of a simple client
 that only echos the messages it gets from the server using
 the functions in lib/client-api.h. If you are developing the
 telemetry and want to expand the client side functionalities, please
 extend the lib/client-api.* files.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "lib/can-ids/CAN_IDs.h"
#include "lib/client-api.h"


/*Function that checks argument validity*/
int parseArgs(int argc /*, char* argv[]*/){ /*uncomment if you want to validate the arguments*/
	if((argc != 3)){
		return 1;
	}
	return 0;
}

int show_usage(char* program_name){
	printf("Usage: %s <ip> <port>\n", program_name);
	exit(1);
}

int main (int argc, char* argv[]){

	int sockfd;
	if(parseArgs(argc)){
		show_usage(argv[1]);
		return 1;
	}
	/*Make a connection*/
	sockfd = connect_to_server(argv[1], argv[2]);

	/*Check for error to avoid unexpected behaviour*/
	if(sockfd < 0){
		printf("Error: %d\n", sockfd);
		exit(1);
	}
	CANmessage message;
	message.candata.dev_id = 1;

	send_message_to_server(sockfd, message);
	/*Echo the messages you are getting from the server*/
	for(;;){
		int return_value = read_message_from_server(sockfd, &message);
		/*Connection closed by the server*/
		if(return_value == DISCONNECT){
			printf("Connection closed by peer. Exiting...");
			break;
		}
		else if(return_value != 0){
			printf("Error reading message from server: Error code: %d\n", return_value);
			exit(1);
		}
		print_message(message);
	}
	int error = disconnect_from_server(sockfd);
	if(error != 0){
		printf("Error disconnecting from the server\n");
		return 1;
	}
	return 0;
}
