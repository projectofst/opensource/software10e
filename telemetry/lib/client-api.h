/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_API_H
#define CLIENT_API_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <string.h>
#include "can-ids/CAN_IDs.h"
#include "errors.h"

/*Function that takes an ip adress and a port and
returns the socket file descriptor derived from a connection.
Returns error otherwise. See errors.h.*/
int connect_to_server(char* ip_adress, char* portno);

/*Function that takes a file descriptor and a CANmessage
  and writes the last to the socket corresponding to the first.
  Returns 0 if the write was successful and error if not.*/
int send_message_to_server(int sockfd, CANmessage message);


/*Function that takes a file descriptor and pointer to a CANmessage
  and writes the content read from the file descriptor into the CANmessage
*/
int read_message_from_server(int sockfd, CANmessage* message);

/* Disconnects from the server associated with the given fd*/
int disconnect_from_server(int sockfd);

/*prints he given CANmessages*/
void print_message(CANmessage message);
#endif
