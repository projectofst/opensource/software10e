/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "connection-handler.h"

pthread_rwlock_t connection_lock;
int current_connections[MAX_CONNECTIONS];
int no_active_connections = 1;
extern address* clients[MAX_CONNECTIONS];


/* Thought process similar to kernel page swapping. Every function call we check what connections were active in the last time slice.*/
void check_active_connections(){
	pthread_rwlock_rdlock(&connection_lock);
	for(int i = 0; i < MAX_CONNECTIONS; i++){
		if(clients[i] == NULL){
			continue;
		}

		if(clients[i]->active == 0){
			free(clients[i]->addr);
			free(clients[i]);
			clients[i] = NULL;
		}
		else{
			clients[i]->active = 0;
		}
	}
	alarm(1);
	no_active_connections = 1;
	pthread_rwlock_unlock(&connection_lock);
	return;
}

int compare_addresses(address* addr1, address* addr2){
	if((addr1->addr->sin_addr.s_addr == addr2->addr->sin_addr.s_addr) &&
				(addr1->addr->sin_port == addr2->addr->sin_port)){
			return 1;
	}
	return 0;
}
void check_client(address* newaddress){
	for(int i = 0; i < MAX_CONNECTIONS; i++){
		if(clients[i] == NULL){
			continue;
		}

		if(compare_addresses(clients[i], newaddress)){
			clients[i]->active = 1;
			return;
		}

	}

	for(int i = 0; i < MAX_CONNECTIONS; i++){
		if(clients[i] == NULL){
			clients[i] = newaddress;
			clients[i]->active = 1;
			return;
		}
	}

}

address* get_new_address(struct sockaddr_in client_address, int client_length){

	address* new_address = (address*) malloc(sizeof(address));
	new_address->addr = (struct sockaddr_in*) malloc(sizeof(struct sockaddr_in));
	memcpy(new_address->addr, &client_address, sizeof(client_address));
	new_address->len = client_length;
	return new_address;

}

void remove_connection(address* addr){
	pthread_rwlock_wrlock(&connection_lock);
	for(int i = 0; i < MAX_CONNECTIONS; i++){
		if(clients[i] == NULL){
			continue;
		}
		if(compare_addresses(clients[i], addr)){
			free(clients[i]->addr);
			free(clients[i]);
			clients[i] = NULL;
			pthread_rwlock_unlock(&connection_lock);
			return;
		}
	}

	pthread_rwlock_unlock(&connection_lock);
	return;
}

void insert_connection(int newsockfd){
	pthread_rwlock_wrlock(&connection_lock);
	for(int i = 0; i < MAX_CONNECTIONS; i++){
		if(current_connections[i] == -1){
			current_connections[i] = newsockfd;
			pthread_rwlock_unlock(&connection_lock);
			no_active_connections = 0;
			return;
		}
	}

	pthread_rwlock_unlock(&connection_lock);
	return;
}
int get_lowest_con(){
	pthread_rwlock_rdlock(&connection_lock);

	for(int i = 0; i < MAX_CONNECTIONS; i++){
		if(current_connections[i] == -1){
			pthread_rwlock_unlock(&connection_lock);
			return i;
		}
	}

	pthread_rwlock_unlock(&connection_lock);
	return -1;
}

void init_active_connections(){
	int error = pthread_rwlock_init(&connection_lock, NULL);

	if(error != 0){
		perror("Error initializing mutex...\n");
		exit(EXIT_FAILURE);
	}
	no_active_connections = 1;

	for(int i = 0; i < MAX_CONNECTIONS; i++){
		current_connections[i] = -1;
	}

	return;
}

void destroy_connections(){
	pthread_rwlock_destroy(&connection_lock);
	return;
}

int getsockfd(int index){
	int to_return = -1;
	pthread_rwlock_rdlock(&connection_lock);
	if(index < MAX_CONNECTIONS){
		to_return = current_connections[index];
		pthread_rwlock_unlock(&connection_lock);
		return to_return;
	}
	pthread_rwlock_unlock(&connection_lock);
	return to_return;
}


int check_for_clients(){
	return no_active_connections;
}
