/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hash_table.h"

extern node** nodes;

node* insert_node(int key, node* new_node){
	if(key > 31) {
		return NULL;
	}
	if(nodes[key] == NULL){
		nodes[key] = new_node;
		return new_node;
	}

	node* current_node = nodes[key];
	while(nodes[key]->next != NULL){
		current_node = current_node->next;
	}

	current_node->next = new_node;
	new_node->next = NULL;
	return nodes[key];
}


node* create_node(CANmessage message){
	node* new_node = (node *)malloc(sizeof(node));
	new_node->parameter = message.candata.data[0];
	new_node->value = message.candata.data[1];
	new_node->next = NULL;
	return new_node;
}


node * find_node(CANmessage message){
	int index = message.candata.dev_id;

	node * current_node = nodes[index];

	while(current_node != NULL){
		if(current_node->parameter == message.candata.data[0]){
			return current_node;
		}
		current_node = current_node->next;
	}
	return NULL;
}

node* replace_value(CANmessage message){
	node* node_to_change = find_node(message);
	if(node_to_change == NULL){
		return NULL;
	}
	node_to_change->value = message.candata.data[1];

	return node_to_change;
}

