/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "client-api.h"
#include <stdio.h>
#include <strings.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include<netinet/in.h>

int size_server_addr = 0;

struct sockaddr_in serveraddr;

int connect_to_server(char* ip_adress, char* portno){
	int port, sockfd;

	port = atoi(portno);

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	printf("%s\n", ip_adress);

	if(sockfd < 0){
		return OPEN_SOCKET_ERROR;
	}

	bzero((char*) &serveraddr, sizeof(serveraddr));

    	    /* gethostbyname: get the server's DNS entry */
    	struct hostent* server = gethostbyname(ip_adress);
    	if (server == NULL) {
        	fprintf(stderr,"ERROR, no such host as %s\n", ip_adress);
        	exit(0);
    	}

    	/* build the server's Internet address */
    	bzero((char *) &serveraddr, sizeof(serveraddr));
    	serveraddr.sin_family = AF_INET;
    	bcopy((char *)server->h_addr,
	(char *)&serveraddr.sin_addr.s_addr, server->h_length);
    	serveraddr.sin_port = htons(port);

	size_server_addr = sizeof(serveraddr);

	return sockfd;
}

int send_message_to_server(int sockfd, CANmessage message){
	int nbytes;

	nbytes = sendto(sockfd, &message, sizeof(CANmessage), 0, (struct sockaddr *) &serveraddr, size_server_addr);

	/*When the value returned is 0, the connection was closed on the other side*/
	if(nbytes == 0){
		return DISCONNECT;
	}
	else if(nbytes != sizeof(CANmessage)){/*Make sure the message was all delivered*/
		return SOCKET_WRITE_ERROR;
	}
	return 0;
}

int read_message_from_server(int sockfd, CANmessage* message){

	int nbytes;

	nbytes = recvfrom(sockfd, message, sizeof(CANmessage), 0, (struct sockaddr *) &serveraddr, (socklen_t* )&size_server_addr);
	if(nbytes == 0){
		return DISCONNECT;
	}

	else if(nbytes != sizeof(CANmessage)){
		disconnect_from_server(sockfd);
		return SOCKET_READ_ERROR;
	}

	return 0;
}

int disconnect_from_server(int sockfd){
	return close(sockfd);
}

void print_message(CANmessage message){
	printf(" %02d %02d [%d] %04x %04x %04x %04x\n",
			message.candata.dev_id,
			message.candata.msg_id,
			message.candata.dlc,
			message.candata.data[0],
			message.candata.data[1],
			message.candata.data[2],
			message.candata.data[3]);
	return;
}
