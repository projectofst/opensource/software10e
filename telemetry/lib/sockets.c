/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sockets.h"

int init_AF_INET_socket(){
	int sockfd;
   	struct sockaddr_in serv_addr;

   	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

   	if (sockfd < 0) {
      		perror("ERROR opening socket");
      		exit(1);
   	}

   	bzero((char *) &serv_addr, sizeof(serv_addr));

   	serv_addr.sin_family = AF_INET;
   	serv_addr.sin_addr.s_addr = INADDR_ANY;
   	serv_addr.sin_port = htons(PORTNO);

   	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      		perror("ERROR on binding");
      		exit(EXIT_FAILURE);
   	}
	return sockfd;
}

int connect_to_CAN(char* ifname){
	/*See SocketCan*/
   	int cansockfd;
	struct sockaddr_can addr;
	struct ifreq ifr;

	cansockfd = socket(PF_CAN, SOCK_RAW, CAN_RAW);

	strcpy(ifr.ifr_name, ifname);
	ioctl(cansockfd, SIOCGIFINDEX, &ifr);

	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	bind(cansockfd, (struct sockaddr * ) &addr, sizeof(addr));

	return cansockfd;
}

/*
int connect_to_client(int sockfd, address* addr){
	int  clilen;
	CANmessage message;
	clilen = sizeof(cliaddr);

   	int n = recvfrom(sockfd, &message, sizeof(CANmessage), 0, (struct sockaddr *)cliaddr, (socklen_t *) &clilen);

	if (n < 0) {
      		perror("ERROR on accept");
		exit(1);
	}
	return n;
}
*/

/*
void send_error(int sockfd, int error_code){
	CANmessage message;

	message.candata.sid = 0;
	message.candata.data[0] = error_code;

	send_CAN(sockfd, message);
	return;
}
*/
int send_to_CAN_Bus_Line(int sockfd, struct can_frame frame){
	int bytes_sent;
	bytes_sent = write(sockfd, &frame, sizeof(struct can_frame));
	return bytes_sent;
}

int send_CAN(int sockfd, CANmessage message, address* addr){
	int bytes_sent;

	bytes_sent = sendto(sockfd, &message, sizeof(CANmessage), 0,(struct sockaddr*) addr->addr, addr->len);
	return bytes_sent;
}


int recieve_from_CAN_Bus_Line(int sockfd, struct can_frame* frame){
	int bytes_read;
	bytes_read = read(sockfd, frame, sizeof(struct can_frame));
	return bytes_read;
}

int recieve_CAN(int sockfd, CANmessage* message, struct sockaddr_in* addr, int* len){
	int bytes_read;

 	bytes_read = recvfrom(sockfd, message, sizeof(CANmessage),
            0, (struct sockaddr*) addr, (socklen_t*) len);
	return bytes_read;
}


/*TODO: the next two functions are not in the right place, get another file for them*/
void CANFrame_to_CANMessage(struct can_frame frame, CANmessage* message){
	message->candata.sid = frame.can_id;

	message->candata.dev_id = (frame.can_id & 0b011111);
	message->candata.msg_id = (frame.can_id >> 5);

	message->candata.dlc = frame.can_dlc;

	message->candata.data[0] = frame.data[0] + (frame.data[1] << 8);
	message->candata.data[1] = frame.data[2] + (frame.data[3] << 8);
	message->candata.data[2] = frame.data[4] + (frame.data[5] << 8);
	message->candata.data[3] = frame.data[6] + (frame.data[7] << 8);

	return;
}


void CANMessage_to_CANFrame(CANmessage message, struct can_frame* frame){

	frame->can_id = message.candata.sid;
	frame->can_dlc = message.candata.dlc;

	frame->data[0] = (unsigned char) (message.candata.data[0] & 0x00FF);
	frame->data[1] = (unsigned char) (message.candata.data[0] >> 8);

	frame->data[2] = (unsigned char) (message.candata.data[1] & 0x00FF);
	frame->data[3] = (unsigned char) (message.candata.data[1] >> 8);
	frame->data[4] = (unsigned char) (message.candata.data[2] & 0x00FF);
	frame->data[5] = (unsigned char) (message.candata.data[2] >> 8);
	frame->data[6] = (unsigned char) (message.candata.data[3] & 0x00FF);
	frame->data[7] = (unsigned char) (message.candata.data[3] >> 8);

	return;
}


