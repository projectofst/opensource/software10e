/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONNECTION_HANDLER_H
#define CONNECTION_HANDLER_H

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "server-side-constants.h"
#include <pthread.h>
#include <netinet/in.h>

void check_active_connections();
address* get_new_address(struct sockaddr_in client_address, int client_length);
void check_client(address* newaddress);
void remove_connection(address* addr);
void insert_connection(int insert);
int get_lowest_con();
void init_active_connections();
int getsockfd(int index);
int check_for_clients();
int compare_addresses(address* addr1, address* addr2);
#endif

