/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOCKETS_H
#define SOCKETS_H

#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <stdio.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include "can-ids/CAN_IDs.h"
#include "server-side-constants.h"

int init_AF_INET_socket();
int connect_to_client(int sockfd);
int connect_to_CAN(char* ifname);
int send_CAN(int sockfd, CANmessage msg, address* addr);
int send_to_CAN_Bus_Line(int sockfd, struct can_frame frame);
int recieve_CAN(int sockfd, CANmessage* message, struct sockaddr_in* addr, int* len);
int recieve_from_CAN_Bus_Line(int sockfd, struct can_frame* frame);
void send_error(int newsockfd, int code);
void CANMessage_to_CANFrame(CANmessage message, struct can_frame* frame);
void CANFrame_to_CANMessage(struct can_frame frame, CANmessage* message);
#endif
