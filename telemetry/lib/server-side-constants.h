/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SERVER_SIDE_CONSTANTS_H
#define SERVER_SIDE_CONSTANTS_H

typedef struct {
	struct sockaddr_in* addr;
	int len;
	int active;
}address;

#define MAX_CONNECTIONS 		5
#define MAX_INPUT_SIZE 			64
#define PORTNO 				5001
#define TELEMETRY_ON			1
#endif

