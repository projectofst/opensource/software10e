/* 
 * telemetry is a software used for wifi telemetry system in the Formula
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Project FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ERRORS_H
#define ERRORS_H

#define OTHER_ERROR 				-1
#define NO_MORE_CONNECTIONS_AVAILABLE_ERROR 	-2
#define OPEN_SOCKET_ERROR			-3
#define NO_SUCH_HOST_ERROR			-4
#define CONNECTION_ERROR			-5
#define SOCKET_WRITE_ERROR 			-6
#define SOCKET_READ_ERROR			-7
#define INIT_FLASH_MESSAGES_ERROR		-8
#define DISCONNECT				-31

#endif
