# FST Telemetry in C

## Software Requirements


### Packages
You will need to install the can-utils package.

On Ubuntu:
```sh
sudo apt-get install can-utils
```

On arch:
```sh
sudo pacman -S can-utils
```

### CAN Shield Configuration

If you want to run the server on the Raspberry Pi for the first time, you will also need to follow the steps described on **page 5** of the following document:

https://copperhilltech.com/content/PICAN2DUOUGB.pdf


## Compiling

A Makefile is provided to execute the above compilation steps.

Run
```sh
make
```
to compile both the client and the server.

```sh
make server
```
and

```sh
make client
```
compile the server and the client respectively.

## Running The Program
### Server
Run the server on the on-board Raspberry Pi.

After compiling, the server can be run by executing the command:

```sh
./server
```

Running the server with the -t flag, such as:
```sh
./server -t
```
will result on the CAN Bus Line names to default to vcan0 and vcan1.

You can choose the names of the CAN Bus lines by executing the server as follows:

```sh
./server -t <CAN_Name> <CAN_Name>
```



Running the server without any flags will result on both names defaulting to can0 and can1.

### Client
After compiling, the client can be run by executing the command:

```sh
./client <server_IP_Adress> <Port>
```
The server's port is defaulted to 5001.

If the server is running on your machine, the server IP Adress can be replaced by *localhost*.

## Testing

For testing purposes you can setup a virtual CAN Bus line on your machine and send test messages through it.

### Setting up a Virtual CAN Line

If you are on an Unix based system run the following commands on your terminal:

	1. sudo modprobe vcan
	2. sudo ip link add dev vcan0 type vcan
	3. sudo ip link set up vcan0

This will generate a line named vcan0. Run:
```sh
candump <can_bus_name>
```
on one terminal to be able to visualize all the messages running through the line.

To send messages run:
```sh
cansend <can_bus_name> <CAN_ID>#<DATA>
```
For Example:
```sh
cansend <can_bus_name> 123#AABBCCDDEEFF
```

It is necessary to setup 2 CAN Bus lines, in order to not get duplicate messages on the client.

A shell script is provided with all the commands needed to setup both Virtual CAN lines.

## Integration with the FST CAN Interface
The FST CAN Interface acts as a client for the server, so if you are not interested in developing the telemetry, use the FST CAN Interface as your client.
