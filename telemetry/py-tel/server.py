##
# telemetry is a software used for wifi telemetry system in the Formula
# Student cars developed by FST Lisboa.
# Copyright © 2021 Project FST.

# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.

# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
##

import socket
import os
import can
import trio

# setup

counter = 1

# Creates the UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

address = ("255.255.255.255", 5454)

sock.bind(address)

bus_s = can.interface.Bus(bustype="socketcan", channel="can0", bitrate=1000000)

bus_e = can.interface.Bus(bustype="socketcan", channel="can1", bitrate=1000000)

# msgr is the array of connections (5)
msgr = [("", "")] * 5

# awaits for a first connection message
password = ""
while password is "":
    print("espera")
    password, tmp = sock.recvfrom(1024)
    print(password)
    if password == b"].8Gtu$Bj@)zX[(3q)%4>{jx^yNS6L":
        msgr[0] = tmp
        print(msgr[0])
        break
    else:
        password = ""


# Receives messages from can and sends them to the users
async def data_from_can(msgr, bus, send_channel):
    while True:
        if msgr != None:
            msg = await trio.to_thread.run_sync(bus.recv, 1)
            await send_channel.send(msg)


async def data_to_udp(msgr, sock, receive_channel):
    async for msg in receive_channel:
        buffer = str(msg)
        buffer = buffer.split()
        # append free space to string
        string = "0x00x0"

        # timestamp vai dar problemas, nao esta a ser enviado ficara aqui
        string += "0x00x00x00x0"

        # unificacao de bits de ids e dlc
        ids = int(buffer[3], 16)
        ids <<= 4
        if str(buffer[5]) != "DLC:":
            num = 6
            # print(msg)
        else:
            num = 5
        ids = ids + int(buffer[num + 1], 16)
        tmp = ids
        tmp = tmp >> 8
        tmp = tmp & 255
        string += hex(tmp)
        tmp = ids & 255
        string += hex(tmp)

        # append data
        for x in range(int(buffer[num + 1], 16)):
            string += hex(int(buffer[num + x + 2], 16) & 255)

        # Sends the messagesn to all the connected users
        tosend = string.encode("utf-8")
        for i in range(5):
            if msgr[i][0] != "":
                sock.sendto(tosend, msgr[i])
            else:
                break


# receives the WIFI messages and ether connects to a new user or sends the message to c the can line
async def data_to_can(msgr, sock, bus_e, bus_s):
    while True:
        msg, nmsgr = await trio.to_thread.run_sync(sock.recvfrom, 4096)
        # print("raw: ", str(msg,'utf-8'))

        # se a mensagem for a de ligação estabelece novo user
        if str(msg, "utf-8") == "].8Gtu$Bj@)zX[(3q)%4>{jx^yNS6L":
            print("new user")
            counter = 0
            for pair in msgr:
                curr_id = pair[0]
                curr_port = pair[1]
                if counter > 4:
                    break
                if curr_id == nmsgr[0]:
                    msgr[counter] = nmsgr
                    break
                if msgr[counter][0] == "":
                    msgr[counter] = nmsgr
                    break
                counter += 1
            # print(msgr)
            continue

        import json

        buffer = json.loads(msg)
        sid = buffer[0]
        dlc = buffer[1]
        tmp = [0 for i in range(8)]
        for i, b in enumerate(buffer[2:]):
            tmp[2 * i + 1] = b >> 8
            tmp[2 * i] = b & 255

        msgcan = can.Message(
            data=tmp, arbitration_id=sid, dlc=dlc, is_extended_id=False
        )
        # print(msgcan)
        bus_e.send(msgcan)
        bus_s.send(msgcan)


# Runs everything async
async def com(msgr, sock, bus_e, bus_s):
    async with trio.open_nursery() as nursery:
        print("nursery")
        send_channel, receive_channel = trio.open_memory_channel(0)
        nursery.start_soon(data_from_can, msgr, bus_e, send_channel)
        nursery.start_soon(data_from_can, msgr, bus_s, send_channel)
        nursery.start_soon(data_to_udp, msgr, sock, receive_channel)
        nursery.start_soon(data_to_can, msgr, sock, bus_e, bus_s)


# actualy starts running
trio.run(com, msgr, sock, bus_e, bus_s)
