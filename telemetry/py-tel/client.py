##
# telemetry is a software used for wifi telemetry system in the Formula
# Student cars developed by FST Lisboa.
# Copyright © 2021 Project FST.

# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.

# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
##

import socket
import serial
import trio
import json

####   get the USB name from user ####

USBname = input("What is the USB name?\n")

#######################################


HOST = "255.255.255.255"
PORT = 5454


# setup

# Creates the UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

# Establishes conncetion with the car
sock.sendto(b"].8Gtu$Bj@)zX[(3q)%4>{jx^yNS6L", (HOST, PORT))

# Creates the USB connection
serial = serial.Serial(USBname)
print(serial.name)


##Receives the data from the car
async def data_from_udp(send_channel):
    while True:
        try:
            msg, _ = sock.recvfrom(1024)
        ##Exception guarantees connection
        except Exception as e:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            sock.settimeout(1.0)
            sock.sendto(b"].8Gtu$Bj@)zX[(3q)%4>{jx^yNS6L", (HOST, PORT))
            print("CAUGHT EXEPTION!")
            continue
        ##Receives the messages and sends them to a async message buffer
        else:
            await send_channel.send(msg)


# Receives the messages from the buffer and actualy sends them to the USB connection
async def data_to_serial(receive_channel):
    async for msg in receive_channel:
        msg = msg.decode("utf-8")
        string = msg.split("0x")
        tmp = (int(string[7], 16) << 8) + int(string[8], 16)
        sid = tmp >> 4
        dlc = tmp & 15
        data = [0, 0, 0, 0]
        for i in range(int(dlc / 2)):
            data[i] = (int(string[9 + (i * 2) + 1], 16) << 8) + int(
                string[9 + (i * 2)], 16
            )
        serialmsg = str.encode(f"{sid},{dlc},{data[0]},{data[1]},{data[2]},{data[3]}\n")
        await trio.to_thread.run_sync(serial.write, serialmsg)


# Receives messages from the interface and sends them to the car
async def data_from_serial():
    while True:
        line = await trio.to_thread.run_sync(serial.readline)
        buffer = line.split(b",")
        # print(buffer)

        buffer = [int(b) for b in buffer]
        sock.sendto(json.dumps(buffer).encode("utf-8"), (HOST, PORT))


# Runs everything async
async def com():
    async with trio.open_nursery() as nursery:
        send_channel, receive_channel = trio.open_memory_channel(0)
        nursery.start_soon(data_from_udp, send_channel)
        nursery.start_soon(data_to_serial, receive_channel)
        nursery.start_soon(data_from_serial)


# actualy starts running
trio.run(com)
