##
# telemetry is a software used for wifi telemetry system in the Formula
# Student cars developed by FST Lisboa.
# Copyright © 2021 Project FST.

# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.

# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
##


import os

os.system("sudo ip link set can0 up type can bitrate 1000000")
os.system("sudo ip link set can1 up type can bitrate 1000000")
