#include "pdu_config.h"
#include "cooling.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/pwm.h"
#include "shared/run_avg/run_avg.h"
#include "pdu.h"
#include "lib_pic33e/adc.h"
#include "fifo/fifo.h"

run_avg_t run_avgs[ADC_SIZE];
uint16_t threshold[ADC_SIZE] = {0};

extern uint16_t sio_port_conf[SIO_SIZE];
extern uint16_t sio_port[SIO_SIZE];
extern int16_t sio_data[SIO_SIZE];

void adc_setup()
{
    uint16_t i = 0;

    ADC_parameters adc_config;

    uint16_t vector = (AN6 | AN7 | AN8 | AN9 | AN10 | AN11 | AN12 | AN13 | AN5);

    config_pin_cycling(vector, 3200UL, 64, 4, &adc_config);
    config_adc1(adc_config);

    CNPDB = vector;

    for (i = 0; i < ADC_SIZE; i++)
    {
        run_avgs[i].max_size = 16;
        run_avg_config(&run_avgs[i]);
    }
    return;
}

void can_setup()
{
    PPSUnLock;
    // Output configuration based in the README
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
    PPSOutput(OUT_FN_PPS_C2TX, OUT_PIN_PPS_RP120);
    // Input configuration based in the README
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
    PPSInput(IN_FN_PPS_C2RX, IN_PIN_PPS_RPI121);
    PPSLock;

    config_can1(NULL);
    config_can2(NULL);
}

void pwm_setup()
{
    int cfg_err = 0;

    PPSUnLock;
    // I don't know if its this way(they use OC1 ... and in the pwm lib its also OC)
    // probably we need HW_SW also
    // Output
    // PPSOutput(OUT_FN_PPS_OC1, OUT_PIN_PPS_RPI74);
    // PPSOutput(OUT_FN_PPS_OC2, OUT_PIN_PPS_RPI75);
    PPSOutput(OUT_FN_PPS_OC3, OUT_PIN_PPS_RP64);
    /* PPSOutput(OUT_FN_PPS_OC4, OUT_PIN_PPS_RPI61);
    PPSOutput(OUT_FN_PPS_OC5, OUT_PIN_PPS_RPI62); */
    PPSOutput(OUT_FN_PPS_OC6, OUT_PIN_PPS_RP65);
    PPSOutput(OUT_FN_PPS_OC7, OUT_PIN_PPS_RP66);
    PPSOutput(OUT_FN_PPS_OC8, OUT_PIN_PPS_RP67);
    // Input

    PPSLock;

    cfg_err += pwm1_config(0.0, PWM_T, CLK_TIMER2);
    pwm1_update_duty_cycle(1.0);
}

void mux_setup()
{
    // MUX Select configuration
    SEL_0_CONFIG = OUTPUT;
    SEL_1_CONFIG = OUTPUT;
    SEL_2_CONFIG = OUTPUT;

    // MUX configuration
    MUX_1_CONFIG = INPUT;
    MUX_2_CONFIG = INPUT;
    MUX_3_CONFIG = INPUT;

    /*  PPSUnLock;
     // Output
     PPSOutput(OUT_FN_PPS_SEL_0, OUT_PIN_PPS_RP134);
     PPSOutput(OUT_FN_PPS_SEL_1, OUT_PIN_PPS_RP135);
     PPSOutput(OUT_FN_PPS_SEL_2, OUT_PIN_PPS_RP136);
     // Input
     PPSInput(IN_FN_PPS_MUX1_OUT, IN_PIN_PPS_RPI140);
     PPSInput(IN_FN_PPS_MUX2_OUT, IN_PIN_PPS_RPI141);
     PPSInput(IN_FN_PPS_MUX3_OUT, IN_PIN_PPS_RPI139);

     PPSLock; */

    // maybe initialize mux_data
}

void sio_setup(PDU_DATA pdu_data)
{
    uint16_t i;

    // SIO default configuration to input
    S_1_CONFIG = INPUT;
    S_2_CONFIG = INPUT;
    S_3_CONFIG = INPUT;
    // S_4_CONFIG = INPUT;
    // S_5_CONFIG = INPUT;
    S_6_CONFIG = INPUT;
    S_7_CONFIG = INPUT;
    // S_8_CONFIG = INPUT;
    S_9_CONFIG = INPUT;
    S_10_CONFIG = INPUT;
    S_11_CONFIG = INPUT;

    /*check wrong pins*/

    pdu_data.sio_port_conf[0] = S_0_CONFIG;
    pdu_data.sio_port_conf[1] = S_1_CONFIG;
    pdu_data.sio_port_conf[2] = S_2_CONFIG;
    pdu_data.sio_port_conf[3] = S_3_CONFIG;
    // pdu_data.sio_port_conf[4] = S_4_CONFIG;
    // pdu_data.sio_port_conf[5] = S_5_CONFIG;
    pdu_data.sio_port_conf[6] = S_6_CONFIG;
    pdu_data.sio_port_conf[7] = S_7_CONFIG;
    // pdu_data.sio_port_conf[8] = S_8_CONFIG;
    pdu_data.sio_port_conf[9] = S_9_CONFIG;
    pdu_data.sio_port_conf[10] = S_10_CONFIG;
    pdu_data.sio_port_conf[11] = S_11_CONFIG;

    pdu_data.sio_port[0] = S_0;
    pdu_data.sio_port[1] = S_1;
    pdu_data.sio_port[2] = S_2;
    pdu_data.sio_port[3] = S_3;
    // pdu_data.sio_port[4] = S_4;
    // pdu_data.sio_port[5] = S_5;
    pdu_data.sio_port[6] = S_6;
    pdu_data.sio_port[7] = S_7;
    // pdu_data.sio_port[8] = S_8;
    pdu_data.sio_port[9] = S_9;
    pdu_data.sio_port[10] = S_10;
    pdu_data.sio_port[11] = S_11;

    for (i = 0; i < 12; i++)
    {
        pdu_data.sio_data[i] = 0;
    }
}

void pdu_config(PDU_DATA pdu_data)
{
    // RGB LED's Configuration
    LED_0_CONFIG = OUTPUT; // Red
    LED_1_CONFIG = OUTPUT; // Green

    // Digital Outputs Configuration
    S_0_CONFIG = OUTPUT;
    S_1_CONFIG = OUTPUT;
    S_2_CONFIG = OUTPUT;
    S_3_CONFIG = OUTPUT;
    /* S_4_CONFIG = OUTPUT;
    S_5_CONFIG = OUTPUT; */
    S_6_CONFIG = OUTPUT;
    S_7_CONFIG = OUTPUT;
    // S_8_CONFIG = OUTPUT;
    S_9_CONFIG = OUTPUT;
    S_10_CONFIG = OUTPUT;
    S_11_CONFIG = OUTPUT;

    SW_0_CONFIG = OUTPUT;
    SW_1_CONFIG = OUTPUT;
    SW_2_CONFIG = OUTPUT;
    SW_3_CONFIG = OUTPUT;
    SW_4_CONFIG = OUTPUT;
    SW_5_CONFIG = OUTPUT;
    SW_6_CONFIG = OUTPUT;
    SW_7_CONFIG = OUTPUT;

    BL_CONFIG = OUTPUT;
    ASSI_Y_CONFIG = OUTPUT;
    ASSI_B_CONFIG = OUTPUT;

    config_timer1(1, 4);   // 1ms period
    config_timer2(100, 5); // 1ms period

    can_setup();
    adc_setup();
    pwm_setup();
    mux_setup();
    sio_setup(pdu_data);

    return;
}