#include "actions.h"

// Verify libraries added //

/// RTS DCU /// Ver onde colocar

void set_rtd_sequence(COMMON_MSG_RTD_ON rtd_message, PDU_STATUS *pdu_status)
{
    // star rtd sequence by starting the buzzer
    if ((rtd_message.step == RTD_STEP_INV_OK) && (pdu_status->rtd_mode == 0))
    {
        pdu_status->rtd_buzz = 1;
    }

    return;
}

void handle_rtd(PDU_STATUS *pdu_status)
{
    /* RTD */

    if (pdu_status->rtd_buzz == 1)
    {
        if (pdu_status->rtd_buzz_count < 2500)
        {
            SW_0 = 1;
        }
        else
        { // Ends buzzing
            SW_0 = 0;
            pdu_status->rtd_buzz_count = 0;
            pdu_status->rtd_buzz = 0;
            // send_rtd_message();
        }
    }
    else
    {
        SW_0 = 0;
        pdu_status->rtd_buzz_count = 0;
    }

    return;
}

void send_rtd_message()
{
    CANdata message;
    message.dev_id = DEVICE_ID_DCU;
    message.msg_id = CMD_ID_COMMON_RTD_ON;
    message.dlc = 2;
    message.data[0] = RTD_STEP_DCU;
    write_to_can1_buffer(message);
    return;
}

void update_adc_values(PDU_DATA *pdu_data)
{
    get_pin_cycling_values(pdu_data->adc_data, 0, ADC_SIZE);
    // check adc range in can messages
    send_pdu_adc(pdu_data->adc_data);
}

// turn on/off brake light acording to bps_pressure
void toggle_brake_light(PDU_STATUS *pdu_status)
{
    // if (pressure >  2.5% * max value): BL_SIG = 1
    BL_SIG = pdu_status->bps_pressure > (BPS_PRESSURE_MAX / 100);
}