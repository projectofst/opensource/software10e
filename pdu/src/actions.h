#ifndef __ACTIONS_H__
#define __ACTIONS_H__

#include "pdu.h"
#include "communication.h"

void set_rtd_sequence(COMMON_MSG_RTD_ON rtd_message, PDU_STATUS *pdu_status);
void handle_rtd(PDU_STATUS *pdu_status);
void send_rtd_message();
void update_adc_values(PDU_DATA *pdu_data);
void toggle_brake_light(PDU_STATUS *pdu_status);

#endif