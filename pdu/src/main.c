// FST Lisboa
// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF  // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF  // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS    // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF  // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128    // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON       // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF      // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON       // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4  // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON    // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE   // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF  // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF  // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.

#include "pdu.h"
#include "pdu_config.h"
#include "communication.h"
#include "actions.h"
#include "scheduler/scheduler.h"
#include "lib_pic33e/pwm.h"

PDU_STATUS pdu_status;
PDU_DATA pdu_data;

uint32_t timer = 0;

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{
    multiple_return_t mt = {0};
    return mt;
}

uint16_t dev_get_id()
{
    return DEVICE_ID_PDU;
}

void timer1_callback(void)
{
    timer++;

    if (pdu_status.rtd_buzz)
        pdu_status.rtd_buzz_count++;

    return;
}

void flash_leds(void)
{

    LED_0 ^= 1;
    LED_1 ^= 1;
    return;
}

void receive_mux()
{
    uint32_t mux1 = 0;
    uint32_t mux2 = 0;
    uint32_t mux3 = 0;

    static uint32_t select = 0;

    // selector for the position
    SEL_0 = select & 0b1;
    SEL_1 = (select >> 1) & 0b1;
    SEL_2 = (select >> 2) & 0b1;

    mux1 = select;
    mux2 = 8 + select;
    mux3 = 16 + select;

    // get the value and put it in the array
    pdu_data.mux_data[mux1] = MUX_1;
    pdu_data.mux_data[mux2] = MUX_2;
    pdu_data.mux_data[mux3] = MUX_3;

    select = (select + 1) % 8;

    send_pdu_detections(pdu_data);
}

void handle_sio(uint16_t *sio_args)
{
    uint16_t i;

    uint16_t sio_numb = sio_args[0];
    uint16_t type = sio_args[1];
    uint16_t out_value = sio_args[2];

    // maybe separate in 2 functions, one for output and one for input(and changing input to output)
    // more abstract send can function maybe

    // sio_numb -> index
    // sio_0 -> output or input
    // sio_port_conf -> config port to output or input
    // sio_data -> value on sio

    if (sio_numb < SIO_SIZE && sio_numb >= 0)
    {
        pdu_data.sio_port_conf[sio_numb] = type;
        pdu_data.sio_o[sio_numb] = !type;
        if (!type)
        {
            pdu_data.sio_port[sio_numb] = out_value;
            pdu_data.sio_data[sio_numb] = 0;
        }
    }
    for (i = 0; i < SIO_SIZE; i++)
    {
        if (pdu_data.sio_o[i] != 1)
        {
            // 11 in means output
            pdu_data.sio_data[i] = pdu_data.sio_port[i];
        }
    }

    send_pdu_io(pdu_data);
}

int main()
{
    int16_t sio_args[3] = {13, 0, 0};

    tasks_t pdu_tasks[] = {
        {.period = 500, .func = flash_leds},
        {.period = 500, .func = send_CAN_message},
        {.period = 60, .func = receive_mux},
        {.period = 60, .func = update_adc_values, .args_on = 1, .args = (void *)&pdu_data},
        {.period = 60, .func = handle_sio, .args_on = 1, .args = (void *)sio_args},
        {.period = 60, .func = toggle_brake_light, .args_on = 1, .args = (void *)&pdu_status},
        {.period = 0, .func = receive_can1, .args_on = 1, .args = (void *)&pdu_status},
        {.period = 0, .func = receive_can2, .args_on = 1, .args = (void *)&pdu_status},
    };

    scheduler_init(pdu_tasks, sizeof(pdu_tasks) / sizeof(tasks_t));

    LED_0_CONFIG = OUTPUT;
    LED_1_CONFIG = OUTPUT;

    pdu_config(pdu_data);

    while (1)
    {
        scheduler(timer);

        // Clear watchdog timer
        ClrWdt();
    }

    return 0;
}

void trap_handler(TrapType type)
{

    switch (type)
    {
    case HARD_TRAP_OSCILATOR_FAIL:
        break;
    case HARD_TRAP_ADDRESS_ERROR:
        break;
    case HARD_TRAP_STACK_ERROR:
        break;
    case HARD_TRAP_MATH_ERROR:
        break;
    case CUSTOM_TRAP_PARSE_ERROR:
        break;
    default:
        break;
    }

    return;
}
