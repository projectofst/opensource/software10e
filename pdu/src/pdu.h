#ifndef PDU_H
#define PDU_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <xc.h>

#include "lib_pic33e/timing.h"

#include "lib_pic33e/pps.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/trap.h"
#include "shared/scheduler/scheduler.h"
#include "lib_pic33e/adc.h"

#include "can-ids-v2/pdu_can.h"
#include "can-ids-v2/can_cfg.h"
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "lib_pic33e/can.h"
#include "shared/rtd/rtd.h"
#include "fifo/fifo.h"

#define IN_SIZE 2 // Voltage and Current sensors
#define AN_IO_SIZE 8
#define ADC_SIZE (IN_SIZE + AN_IO_SIZE) // ADC size includes 8 adc's + 2 IN (current sensor and voltage sensor)
#define MUX_SIZE 24
#define SIO_SIZE 12

#define BPS_PRESSURE_MAX 250000
#define BL_SIG LATBbits.LATB12 // To confirm

// pwm stuff
#define PWM_T 0.0001

// MUX Select
#define SEL_0_CONFIG TRISBbits.TRISB2
#define SEL_1_CONFIG TRISBbits.TRISB3
#define SEL_2_CONFIG TRISBbits.TRISB4

#define SEL_0 LATBbits.LATB2
#define SEL_1 LATBbits.LATB3
#define SEL_2 LATBbits.LATB4

// MUX
#define MUX_1_CONFIG TRISFbits.TRISF0
#define MUX_2_CONFIG TRISFbits.TRISF1
#define MUX_3_CONFIG TRISEbits.TRISE1

#define MUX_1 LATFbits.LATF0
#define MUX_2 LATFbits.LATF1
#define MUX_3 LATEbits.LATE1

// Digital IO
#define S_0_CONFIG TRISDbits.TRISD9
#define S_1_CONFIG TRISDbits.TRISD7
#define S_2_CONFIG TRISBbits.TRISB10
#define S_3_CONFIG TRISDbits.TRISD8
//#define S_4_CONFIG TRISGbits.TRISG3 --pin doesnt exist in our pic
//#define S_5_CONFIG TRISGbits.TRISG2 --pin doesnt exist in our pic
#define S_6_CONFIG TRISDbits.TRISD11
#define S_7_CONFIG TRISCbits.TRISC14
//#define S_8_CONFIG TRISCbits.TRISB13 --pin wrongly defined
#define S_9_CONFIG TRISEbits.TRISE3
#define S_10_CONFIG TRISBbits.TRISB14
#define S_11_CONFIG TRISBbits.TRISB15

#define S_0 LATDbits.LATD9
#define S_1 LATDbits.LATD7
#define S_2 LATBbits.LATB10
#define S_3 LATDbits.LATD8
//#define S_4 LATGbits.LATG3 --pin doesnt exist in our pic
//#define S_5 LATGbits.LATG2 --pin doesnt exist in our pic
#define S_6 LATDbits.LATD11
#define S_7 LATCbits.LATC14
//#define S_8 LATCbits.LATB13 --pin wrongly defined
#define S_9 LATEbits.LATE3
#define S_10 LATBbits.LATB14
#define S_11 LATBbits.LATB15

// LED's
#define LED_0_CONFIG TRISDbits.TRISD5
#define LED_1_CONFIG TRISDbits.TRISD6
#define LED_0 LATDbits.LATD5
#define LED_1 LATDbits.LATD6

// Switched O select
#define SW_0_CONFIG TRISEbits.TRISE7
#define SW_1_CONFIG TRISEbits.TRISE5
#define SW_2_CONFIG TRISEbits.TRISE4
#define SW_3_CONFIG TRISEbits.TRISE1
#define SW_4_CONFIG TRISDbits.TRISD3
#define SW_5_CONFIG TRISDbits.TRISD2
#define SW_6_CONFIG TRISDbits.TRISD1
#define SW_7_CONFIG TRISDbits.TRISD0

#define SW_0 LATEbits.LATE7
#define SW_1 LATEbits.LATE5
#define SW_2 LATEbits.LATE4
#define SW_3 LATEbits.LATE1
#define SW_4 LATDbits.LATD3
#define SW_5 LATDbits.LATD2
#define SW_6 LATDbits.LATD1
#define SW_7 LATDbits.LATD0

// LED Drivers
#define BL_CONFIG TRISFbits.TRISF3
#define ASSI_Y_CONFIG TRISFbits.TRISF4
#define ASSI_B_CONFIG TRISFbits.TRISF5

#define BL LATFbits.LATF3
#define ASSI_A LATFbits.LATF4
#define ASSI_B LATFbits.LATF5

typedef enum
{
    V_curr_meas1,
    AN_0,
    AN_1,
    AN_2,
    AN_3,
    AN_4,
    AN_5,
    AN_6,
    AN_7,
} ADC_DATA;

typedef enum
{
    OUTPUT,
    INPUT,
} IO_CONFIG;

// Struct that holds essential PDU mesurments/states
typedef struct
{
    bool rtd_buzz;
    bool rtd_mode;
    bool ts_on;
    bool as_emergency;
    uint32_t lv_voltage_value;
    unsigned int bps_pressure;
    unsigned int apps;
    unsigned int current_step;
    unsigned int rtd_buzz_count;
    unsigned int as_emergency_buzzer_count;
} PDU_STATUS;

typedef struct
{
    int32_t mux_data[MUX_SIZE];
    uint16_t sio_port_conf[SIO_SIZE];
    uint16_t sio_port[SIO_SIZE];
    uint16_t sio_data[SIO_SIZE];
    uint16_t sio_o[SIO_SIZE];
    int16_t adc_data[ADC_SIZE];
} PDU_DATA;
#endif
