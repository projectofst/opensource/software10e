#include "communication.h"

can_t global_can;

Fifo _fifo1_send;
Fifo *fifo1_send = &_fifo1_send;

void send_pdu_io(PDU_DATA pdu_data)
{
    CANdata msg = encode_pdu_io((msg_pdu_pdu_io_t){
        .io_0 = (pdu_data.sio_o[0] << 1) | pdu_data.sio_data[0],
        .io_1 = (pdu_data.sio_o[1] << 1) | pdu_data.sio_data[1],
        .io_2 = (pdu_data.sio_o[2] << 1) | pdu_data.sio_data[2],
        .io_3 = (pdu_data.sio_o[3] << 1) | pdu_data.sio_data[3],
        .io_4 = (pdu_data.sio_o[4] << 1) | pdu_data.sio_data[4],
        .io_5 = (pdu_data.sio_o[5] << 1) | pdu_data.sio_data[5],
        .io_6 = (pdu_data.sio_o[6] << 1) | pdu_data.sio_data[6],
        .io_7 = (pdu_data.sio_o[7] << 1) | pdu_data.sio_data[7],
        .io_8 = (pdu_data.sio_o[8] << 1) | pdu_data.sio_data[8],
        .io_9 = (pdu_data.sio_o[9] << 1) | pdu_data.sio_data[9],
        .io_10 = (pdu_data.sio_o[10] << 1) | pdu_data.sio_data[10],
        .io_11 = (pdu_data.sio_o[11] << 1) | pdu_data.sio_data[11],
    });
    write_to_can1_buffer(msg);
}

void send_pdu_detections(PDU_DATA pdu_data)
{
    CANdata msg = encode_pdu_detections((msg_pdu_pdu_detections_t){
        .fuse_0 = pdu_data.mux_data[0],
        .fuse_1 = pdu_data.mux_data[1],
        .fuse_2 = pdu_data.mux_data[2],
        .fuse_3 = pdu_data.mux_data[3],
        .fuse_4 = pdu_data.mux_data[4],
        .fuse_5 = pdu_data.mux_data[5],
        .fuse_6 = pdu_data.mux_data[6],
        .fuse_7 = pdu_data.mux_data[7],
        .fuse_8 = pdu_data.mux_data[8],
        .fuse_9 = pdu_data.mux_data[9],
        .fuse_10 = pdu_data.mux_data[10],
        .fuse_11 = pdu_data.mux_data[11],
        .fuse_12 = pdu_data.mux_data[12],
        .fuse_13 = pdu_data.mux_data[13],
        .fuse_14 = pdu_data.mux_data[14],
        .fuse_15 = pdu_data.mux_data[15],
        .fuse_16 = pdu_data.mux_data[16],
        .fuse_17 = pdu_data.mux_data[17],
        .shutdown_0 = pdu_data.mux_data[18],
        .shutdown_1 = pdu_data.mux_data[19],
        .shutdown_2 = pdu_data.mux_data[20],
        .shutdown_3 = pdu_data.mux_data[21],
        .shutdown_4 = pdu_data.mux_data[22],
        .shutdown_5 = pdu_data.mux_data[23],

    });
    write_to_can1_buffer(msg);
}

void write_to_can1_buffer(CANdata CANmessage)
{
    append_fifo(fifo1_send, CANmessage);
    return;
}

void dev_send_msg(CANdata msg)
{
    send_can1(msg);

    return;
}

can_t *get_can()
{
    return &global_can;
}

void receive_can1(PDU_STATUS *pdu_status)
{
    static char line = 0;
    CANdata message;
    COMMON_MSG_RTD_ON rtd_message;

    message = pop_can1();

    can_t *can = get_can();

    /*TE message to get the BPS pressure and Implausibilities*/
    if ((message.msg_id == MSG_ID_TE_TE_MAIN) && (message.dev_id == DEVICE_ID_TE))
    {
        pdu_status->bps_pressure = can->te.te_main.te_main_BPSp;
    }
}

void receive_can2(PDU_STATUS *pdu_status)
{
    static char line = 0;
    CANdata message;
    COMMON_MSG_RTD_ON rtd_message;

    message = pop_can2();

    can_t *can = get_can();

    /*TE message to get the BPS pressure and Implausibilities*/
    if ((message.msg_id == MSG_ID_TE_TE_MAIN) && (message.dev_id == DEVICE_ID_TE))
    {
        pdu_status->bps_pressure = can->te.te_main.te_main_BPSp;
    }
}

void send_CAN_message(void)
{
    if (!fifo_empty(fifo1_send))
    {
        CANdata msg = *pop_fifo(fifo1_send);
        send_can1(msg);
    }

    return;
}

void send_pdu_adc(uint16_t *adc_data)
{
    CANdata msg = encode_pdu_adc((msg_pdu_pdu_adc_t){
        .adc_0 = adc_data[0],
        .adc_1 = adc_data[1],
        .adc_2 = adc_data[2],
        .adc_3 = adc_data[3],
        .adc_4 = adc_data[4],
        .adc_5 = adc_data[5],
        .adc_6 = adc_data[6],
        .adc_7 = adc_data[7],
        .adc_8 = adc_data[8],
        .adc_9 = adc_data[9],
    });
    write_to_can1_buffer(msg);
}
