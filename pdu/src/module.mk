MODULE_C_SOURCES:=cooling.c
MODULE_C_SOURCES+=main.c
MODULE_C_SOURCES+=pdu_config.c
MODULE_C_SOURCES+=communication.c
MODULE_C_SOURCES+=actions.c

PWD:=src/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
