#ifndef COMMUNICATION
#define COMMUNICATION

#include "pdu.h"

void dev_send_msg(CANdata msg);
void receive_can1(PDU_STATUS *pdu_status);
void receive_can2(PDU_STATUS *pdu_status);
void write_to_can1_buffer(CANdata CANmessage);
void send_pdu_detections(PDU_DATA pdu_data);

void send_pdu_io();
void send_CAN_message();
void send_pdu_adc(uint16_t *adc_data);
#endif