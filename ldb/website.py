from bottle import route, jinja2_template, jinja2_view, template, static_file, request
import requests


@route("/")
@jinja2_view("index.jinja2")
def index():
    """Gets logs present in database and displays their timeframe,
    used Boardnet and name. Also displays a download link for the raw
    data values.

    :return: values obtained from API request for the logs
             and link to api used for request methods
    :rtype: dictionary
    """
    host = request.urlparts[1]
    api_link = "http://" + host.split(":")[0] + ":8180"

    r = requests.get(api_link + "/logs")
    logs = r.json()["logs"]
    return dict(logs=logs, api=api_link)


@route("/metadata")
@jinja2_view("metadata.jinja2")
def index_metadata():
    """Gets metadata present in database for every log present in the database.

    :return: values obtained from API request for the metadata
             and link to api used for request methods
    :rtype: dictionary
    """
    host = request.urlparts[1]
    api_link = "http://" + host.split(":")[0] + ":8180"

    r = requests.get(api_link + "/logs/metadata")
    metadata = r.json()["metadata"]
    return dict(metadata=metadata, api=api_link)


@route("/fcp")
@jinja2_view("fcp.jinja2")
def index_fcp():
    """Displays all Boardnets present with date of insertion and download link.

    :return: values obtained from API request for the Boardnets
             and link to api used for request methods
    :rtype: dictionary
    """
    host = request.urlparts[1]
    api_link = "http://" + host.split(":")[0] + ":8180"

    r = requests.get(api_link + "/fcps")
    fcps = r.json()["jsons"]
    return dict(fcps=fcps, api=api_link)


@route("/upload/logs", methods=["GET", "POST"])
@jinja2_view("upload.jinja2")
def upload_log():
    """Displays the Upload Log page.
    Has a form input for the name desired for the upload;
    a file browser to select and upload the log file;
    and a select element with all present Boardnets in the database
    that can be used for decoding.
    These last ones are passed through the return value

    :return: values obtained from API request for the Boardnets
             and link to api used for request methods
    :rtype: dictionary
    """
    host = request.urlparts[1]
    api_link = "http://" + host.split(":")[0] + ":8180"

    r_fcps = requests.get(api_link + "/fcps")
    r_tag = requests.get(api_link + "/tags")
    fcps = r_fcps.json()["jsons"]
    tags = r_tag.json()["tags"]
    return dict(fcps=fcps, tags=tags, api=api_link)


@route("/upload/fcps", methods=["GET", "POST"])
@jinja2_view("upload_fcp.jinja2")
def upload_fcp():
    """Displays the Upload Boardnet page.
    Has a form input for the name desired for the upload
    and a file browser to select and upload the file;

    :return: link to api used for request methods
    :rtype: None
    """
    host = request.urlparts[1]
    api_link = "http://" + host.split(":")[0] + ":8180"
    return dict(api=api_link)


@route("/upload/tags", methods=["GET", "POST"])
@jinja2_view("upload_tag.jinja2")
def upload_tags():
    """Displays the Tag page.
    Has a form input to insert a new tag
    and a table with all tags already present in the database

    :return: list of tags present in the database
             and link to api used for request methods
    :rtype: dictionary
    """
    host = request.urlparts[1]
    api_link = "http://" + host.split(":")[0] + ":8180"

    r = requests.get(api_link + "/tags")
    tags = r.json()["tags"]
    return dict(tags=tags, api=api_link)


@route("/download", methods=["GET", "POST"])
@jinja2_view("download.jinja2")
def download_log():
    """Displays the Download page.
    Has two date-time inputs to select time frame,
    a select element with two options for file extensions 'CSV' and 'MAT'
    and a multi-select element in which the user
    can select all signals to be exported.
    Finally has a selection with a drop down list to select
    specific tags with attributed values
    This multi-select element uses a search bar for easier signal selection.

    :return: List of signals present in database
             and link to api used for request methods
    :rtype: dictionary
    """
    host = request.urlparts[1]
    api_link = "http://" + host.split(":")[0] + ":8180"

    r_sig = requests.get(api_link + "/signals")
    r_tag = requests.get(api_link + "/tags")
    signals = r_sig.json()["signals"]
    tags = r_tag.json()["tags"]
    return dict(signals=signals, tags=tags, api=api_link)


# Static Routes
@route("/static/<filepath:path>")
def server_static(filepath):
    """Route used for every static file used by the website.
    This includes javascript, css and images.

    :return: desired image, code or stylization
    :rtype: static file
    """
    return static_file(filepath, root="./static/")
