import fcp
import json
import datetime
import pytz
from datetime import timedelta
from datetime import tzinfo
from connect import connect
from parse_line_data import get_timestart
from parse_line_data import get_values
from insert_values_toDB import insert_values_decoded
from insert_values_toDB import insert_values_raw
from insert_values_toDB import insert_log


def read_data(csv, json_file_pointer, log_name, fcp_name):
    data = open(csv)

    spec = fcp.Spec()
    spec.decompile(json_file_pointer)
    fcp_handle = fcp.Fcp(spec)

    conn = connect()
    new_list_decoded = []
    new_list_raw = []
    log_values = []
    start_time = None
    cnt_raw = 0
    cnt_decoded = 0

    log_values.append(log_name)

    for line in data.readlines():
        if line.count("Logging started") == 1:
            start_time = get_timestart(line)
            log_values.append(start_time)
        message = fcp.can.CANMessage.decode_kvaser_csv(line, start_time)
        if message == None:
            continue
        time = message.timestamp
        treated = fcp_handle.decode_msg(message)
        for (signal, value) in treated[1].items():
            with conn.cursor() as cursor:
                cursor.execute(f"select id from signals where name='{signal}'")
                signal_id = cursor.fetchone()[0]
            new_list_decoded.append((time, value, signal_id))
            cnt_decoded += 1
            if cnt_decoded % 100000 == 0:
                insert_values_decoded(new_list_decoded, "sql/insert_decoded.sql")
                new_list_decoded = []
        if (line.count(",") > 10) and (line.count("Time") != 1):
            raw_values = get_values(line, start_time)
            if raw_values != None:
                new_list_raw.append(raw_values)
                cnt_raw += 1
                if cnt_raw % 100000 == 0:
                    insert_values_raw(new_list_raw, "sql/insert_raw.sql")
                    new_list_raw = []

    if cnt_decoded % 100000 != 0:
        insert_values_decoded(new_list_decoded, "sql/insert_decoded.sql")

    if cnt_raw % 100000 != 0:
        insert_values_raw(new_list_raw, "sql/insert_raw.sql")

    parsed = line.split(",")
    sum_time = timedelta(seconds=float(parsed[0]))

    no_tz_time = start_time + sum_time
    # need to do a a more precise get timezone
    end_time = no_tz_time.replace(tzinfo=pytz.UTC)
    log_values.append(end_time)

    log_values.append(fcp_name)

    insert_log(log_values, "sql/insert_log.sql")

    data.close()


def read_decoded_data(csv, insert_sql, json_file):
    f = open(json_file)
    data = open(csv)

    load = json.loads(f.read())
    spec = fcp.Spec()
    spec.decompile(load)
    fcp_handle = fcp.Fcp(spec)

    new_list = []
    start_time = None
    cnt = 0

    for line in data.readlines():
        if line.count("Logging started") == 1:
            start_time = get_timestart(line)
        message = fcp.can.CANMessage.decode_kvaser_csv(line, start_time)
        if message == None:
            continue
        time = message.timestamp
        treated = fcp_handle.decode_msg(message)
        for (signal, value) in treated[1].items():
            new_list.append((time, value, signal))
            cnt += 1
            if cnt % 100000 == 0:
                insert_values_decoded(new_list, insert_sql)
                new_list = []

    if cnt % 100000 != 0:
        insert_values_decoded(new_list, insert_sql)

    f.close()
    data.close()


def read_raw_data(csv, insert_sql):
    data = open(csv)

    new_list = []
    cnt = 0
    for line in data.readlines():
        if line.count("Logging started") == 1:
            start_time = get_timestart(line)
        if (line.count(",") > 10) and (line.count("Time") != 1):
            values = get_values(line, start_time)
            if values != None:
                new_list.append(values)
                cnt += 1
                if cnt % 100000 == 0:
                    insert_values_raw(new_list, insert_sql)
                    new_list = []

    if cnt % 100000 != 0:
        insert_values_raw(new_list, insert_sql)

    data.close()


def read_log_data(csv, insert_sql, logname):
    data = open(csv)

    values = []
    start_time = None

    values.append(logname)

    for line in data.readlines():
        if line.count("Logging started") == 1:
            start_time = get_timestart(line)
            values.append(start_time)
        message = fcp.can.CANMessage.decode_kvaser_csv(line, start_time)
        if message == None:
            continue
        parsed = line.split(",")
        time = timedelta(seconds=float(parsed[0]))

    no_tz_time = start_time + time
    # need to do a more precise get timezone
    end_time = no_tz_time.replace(tzinfo=pytz.UTC)
    values.append(end_time)

    insert_log(values, insert_sql)

    data.close()
