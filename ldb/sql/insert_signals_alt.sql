PREPARE sigplan (smallint, text, text) AS
INSERT INTO signals(id,"name",first_parser) VALUES
    ($1,$2,
    (select json_name from jsons where json_name = $3));
EXECUTE sigplan(%s,%s,%s);