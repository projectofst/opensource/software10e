CREATE TABLE decoded_test (
    "time" timestamp with time zone,
    "value" double precision not null,
    signal_id smallserial not null,
    constraint fk_id foreign key (signal_id) references signals(id)
    )
