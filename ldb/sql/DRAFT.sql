SELECT logs.log_name
FROM logs
WHERE logs.start_time >= %(min_time)s
AND logs.end_time <= %(max_time)s;

SELECT metadata.log_name
FROM metadata
WHERE (metadata.tag = %(tag)s
AND (metadata.value = %(value)s OR %(value)s IS NULL)) for tag,value in value_list