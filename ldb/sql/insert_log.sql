PREPARE logplan (text, timestamp with time zone, timestamp with time zone, text) AS
INSERT INTO logs(log_name,start_time,end_time,parser_name) VALUES
    ($1,$2,$3,
    (select json_name from jsons where json_name = $4));
EXECUTE logplan(%s,%s,%s,%s);