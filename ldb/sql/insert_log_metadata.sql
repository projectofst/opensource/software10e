PREPARE metadataplan (text,text,text) AS
INSERT INTO metadata("value",tag,log_name) VALUES
    ($1,
    (select "tag" from tags where "tag" = $2),
    (select "log_name" from logs where "log_name" = $3));