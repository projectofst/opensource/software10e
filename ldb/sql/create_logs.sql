CREATE TABLE logs(
    log_name text primary key,
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    parser_name text,
    constraint fk_parser foreign key (parser_name) references jsons("json_name")
    )
