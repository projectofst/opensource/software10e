CREATE TABLE metadata (
    "value" text,
    tag text,
    log_name text,
    constraint fk_log foreign key (log_name) references logs(log_name),
    constraint fk_tag foreign key (tag) references tags(tag)
    );