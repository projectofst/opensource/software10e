from datetime import datetime
from datetime import timedelta
from datetime import tzinfo
import pytz


def get_timestart(time_line):
    initial_time = time_line.split(",")[1]
    test = datetime.strptime(initial_time, "%Y-%m-%d %H:%M:%S\n")
    return test


def get_values(values_line, timestart):
    parsed = values_line.split(",")

    values = [None] * 4

    # Time
    time = timedelta(seconds=float(parsed[0]))
    no_tz_time = timestart + time
    values[0] = no_tz_time.replace(tzinfo=pytz.UTC)

    # Id
    if parsed[2] == "":
        return None
    values[1] = parsed[2]

    # DLC
    if parsed[4] == "":
        return None
    values[2] = parsed[4]

    # Dt
    values[3] = 0
    for x in range(5, 12):
        if parsed[x] == "":
            parsed[x] = 0
        values[3] += int(parsed[x]) * pow(2, x)

    return values
