import numpy as np
from connect import connect


def download_csv(tags, values, signals, min_time, max_time, filename):

    tag_value_list = list(zip(tags, values))

    time_intervals = select_time_intervals(tag_value_list, min_time, max_time)

    conn = connect()
    with conn.cursor() as cursor:
        f = open("exports/" + filename, "w")
        if len(signals) == 0:
            cursor.copy_expert(
                f"""COPY (SELECT decoded_test.time, decoded_test.value, signals.name
                FROM decoded_test
                INNER JOIN signals
                ON decoded_test.signal_id = signals.id
                WHERE decoded_test.time
                BETWEEN '{min_time}' AND '{max_time}')
                TO STDOUT DELIMITER ',' CSV HEADER""",
                f,
            )
        elif len(signals) == 1:
            for init_time, end_time in time_intervals:
                cursor.copy_expert(
                    f"""COPY (SELECT decoded_test.time, decoded_test.value, signals.name
                    FROM decoded_test
                    INNER JOIN signals
                    ON decoded_test.signal_id = signals.id
                    WHERE decoded_test.signal = '{signals}'
                    AND decoded_test.time
                    BETWEEN '{init_time}' AND '{end_time}')
                    TO STDOUT DELIMITER ',' CSV HEADER""",
                    f,
                )
        elif len(signals) > 1:
            for init_time, end_time in time_intervals:
                cursor.copy_expert(
                    f"""COPY (SELECT decoded_test.time, decoded_test.value, signals.name
                    FROM decoded_test
                    INNER JOIN signals
                    ON decoded_test.signal_id = signals.id
                    WHERE decoded_test.signal IN '{signals}'
                    AND decoded_test.time
                    BETWEEN '{init_time}' AND '{end_time}')
                    TO STDOUT DELIMITER ',' CSV HEADER""",
                    f,
                )
        else:
            print("ERROR: invalid signal list")

        f.close()
    return


def download_mat(tags, values, signals, min_time, max_time):

    tag_value_list = list(zip(tags, values))

    time_intervals = select_time_intervals(tag_value_list, min_time, max_time)

    timeseries = signal_download(signals, time_intervals)

    list_dict = {}

    # append every time_value and value to the corresponding signal dictionary
    for time_value, value, signal in timeseries:
        list_dict[signal] = {"time": [], "value": []}
        list_dict[signal]["time"].append(
            (time_value - time_intervals[0][0]).total_seconds()
        )
        list_dict[signal]["value"].append(value)

    # now convert all values into one dictionary
    if len(signals) == 0:
        for time_value, value, signal in timeseries:
            print(list_dict[signal])
            # CHECK IF SIGNAL HAS NOT ALREADY BEEN ALTERED
            list_dict[signal] = {
                "time": np.array(list_dict[signal]["time"]),
                "signals": {"value": np.array(list_dict[signal]["value"])},
            }
    elif len(signals) == 1:
        list_dict[signals] = {
            "time": np.array(list_dict[signals]["time"]),
            "signals": {"value": np.array(list_dict[signals]["value"])},
        }
    else:
        for signal in signals:
            list_dict[signal] = {
                "time": np.array(list_dict[signal]["time"]),
                "signals": {"value": np.array(list_dict[signal]["value"])},
            }

    return list_dict


def signal_download(signals, time_intervals):
    timeseries = []
    conn = connect()
    with conn.cursor() as cursor:
        if len(signals) == 0:
            start_time = time_intervals[0][0]
            end_time = time_intervals[0][1]
            cursor.execute(
                f"""SELECT decoded_test.time, decoded_test.value, signals.name
                FROM decoded_test
                INNER JOIN signals
                ON decoded_test.signal_id = signals.id
                WHERE decoded_test.time 
                BETWEEN '{start_time}' 
                AND '{end_time}'"""
            )
            timeseries = cursor.fetchall()
        elif len(signals) == 1:
            for start_time, end_time in time_intervals:
                cursor.execute(
                    f"""SELECT decoded_test.time, decoded_test.value, signals.name
                    FROM decoded_test
                    INNER JOIN signals
                    ON decoded_test.signal_id = signals.id
                    WHERE decoded_test.signal = '{signals}'
                    AND decoded_test.time
                    BETWEEN '{start_time}' 
                    AND '{end_time}'"""
                )
                values = cursor.fetchall()
                timeseries.append(values)
        elif len(signals) > 1:
            for start_time, end_time in time_intervals:
                cursor.execute(
                    f"""SELECT decoded_test.time, decoded_test.value, signals.name
                    FROM decoded_test
                    INNER JOIN signals
                    ON decoded_test.signal_id = signals.id
                    WHERE signal IN '{signals}'
                    AND decoded_test.time
                    BETWEEN '{start_time}'
                    AND '{end_time}'"""
                )
            values = cursor.fetchall()
            timeseries.append(values)
        else:
            print("ERROR: invalid signal list")

    return timeseries


def select_time_intervals(tag_value_list, min_time, max_time):
    conn = connect()
    with conn.cursor() as cursor:
        if len(tag_value_list) == 0:
            cursor.execute(
                """SELECT logs.start_time, logs.end_time
                FROM logs
                WHERE logs.start_time >= %(min_time)s
                AND logs.end_time <= %(max_time)s""",
                {"min_time": min_time, "max_time": max_time},
            )
            time_intervals = cursor.fetchall()
        elif len(tag_value_list) == 1:
            print(tag_value_list)
            tag = tag_value_list[0][0]
            value = tag_value_list[0][1]
            cursor.execute(
                """SELECT logs.start_time, logs.end_time
                FROM logs
                INNER JOIN metadata
                ON logs.log_name = metadata.log_name
                WHERE metadata.tag = %(tag)s
                AND metadata.value = %(value)s
                AND logs.start_time >= %(min_time)s
                AND logs.end_time <= %(max_time)s""",
                {
                    "tag": tag,
                    "value": value,
                    "min_time": min_time,
                    "max_time": max_time,
                },
            )
            time_intervals = cursor.fetchall()
        elif len(tag_value_list) > 1:
            # TO DO
            cursor.execute(
                """(SELECT logs.log_name
                FROM logs
                WHERE logs.start_time >= %(min_time)s
                AND logs.end_time <= %(max_time)s)
                
                INNER JOIN metadata
                ON logs.log_name = metadata.log_name
                AND metadata.tag = %(tag)s
                AND metadata.value = %(value)s""",
                {
                    "tag": tag,
                    "value": value,
                    "min_time": min_time,
                    "max_time": max_time,
                },
            )
            time_intervals = cursor.fetchall()
        else:
            print("ERROR: invalid tag list")

    print(time_intervals)

    return time_intervals
