function addTag(tags_array){
    var li = document.createElement("li");
    var form = document.createElement("input");
    var tag_list = document.createElement("select");

    var form_label = document.createElement("label");
    var tag_label = document.createElement("label");

    tag_list.id = "new_tag_list"
    form.id = "new_form"

    tag_list.name = "tag"
    form.name = "values"

    form_label.setAttribute("for", "new_form");
    tag_label.setAttribute("for", "new_tag_list");
    form_label.appendChild(document.createTextNode("Value:"));
    tag_label.appendChild(document.createTextNode("Tag:"));

    tag_list.className = "new_tag_list";
    form.type = "form";
    li.className = "TagValuePair";

    for(var i = 0; i < tags_array.length; i++){
        var opt = tags_array[i]["tag"];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        tag_list.appendChild(el);
    }

    li.appendChild(tag_label);
    li.appendChild(tag_list);
    li.appendChild(form_label);
    li.appendChild(form);
    document.getElementById("lista").appendChild(li);
  }
