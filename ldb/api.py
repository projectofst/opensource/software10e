import re
from bottle import (
    Bottle,
    FormsDict,
    route,
    run,
    get,
    post,
    response,
    request,
    static_file,
)
from bottle_utils import flash
import os
import json
import datetime
import time
import numpy as np

from download_functions import *
from connect import connect
from insert_values_toDB import insert_log_meta
from read_csv import read_data
from scipy.io import savemat


@route("/")
def index():
    """ "Returns index page"""
    return "index"


@get("/logs")
def log_list():
    """Getter for test log contents of the database

    :return: List with the contents of the logs table inserted on the database
    :rtype: Json file
    """
    response.headers["Content-Type"] = "application/json"
    response.headers["Cache-Control"] = "no-cache"

    conn = connect()
    with conn.cursor() as cursor:
        cursor.execute("select log_name, start_time, end_time, parser_name from logs")
        logs = [
            {
                "log_name": log_name,
                "start_time": str(start_time),
                "end_time": str(end_time),
                "parser_name": parser_name,
            }
            for log_name, start_time, end_time, parser_name in cursor.fetchall()
        ]

    return json.dumps({"logs": logs})


@get("/tags")
def tags_list():
    """Getter for tags present in the log database

    :return: List with the contents of the tags table inserted on the database
    :rtype: Json file
    """
    response.headers["Content-Type"] = "application/json"
    response.headers["Cache-Control"] = "no-cache"

    conn = connect()
    with conn.cursor() as cursor:
        cursor.execute("select tag from tags")
        tags = [{"tag": tag[0]} for tag in cursor.fetchall()]

    return json.dumps({"tags": tags})


@get("/fcps")
def fcp_list():
    """Getter for Boardnets present in the log database

    :return: List of Boardnets present in the database
    :rtype: Json file
    """
    response.headers["Content-Type"] = "application/json"
    response.headers["Cache-Control"] = "no-cache"

    conn = connect()
    with conn.cursor() as cursor:
        cursor.execute("select json_name, date from jsons")
        jsons = [
            {"json_name": json_name, "date": str(date)}
            for json_name, date in cursor.fetchall()
        ]

    return json.dumps({"jsons": jsons})


@get("/signals")
def signals_list():
    """Getter for signals present in the log database

    :return: List of signals registered in the database
    :rtype: Json file
    """
    response.headers["Content-Type"] = "application/json"
    response.headers["Cache-Control"] = "no-cache"

    conn = connect()
    with conn.cursor() as cursor:
        cursor.execute("select id, name, first_parser from signals")
        signals = [
            {"signal_id": id, "signal_name": name, "first_fcp": first_parser}
            for id, name, first_parser in cursor.fetchall()
        ]

    return json.dumps({"signals": signals})


@get("/logs/metadata")
def metadata_list():
    """Getter for metadata present in the log database
    (tag with an associated value of a specific log)

    :return: List of metadata present in the database for every log
    :rtype: Json file
    """
    response.headers["Content-Type"] = "application/json"
    response.headers["Cache-Control"] = "no-cache"

    conn = connect()
    with conn.cursor() as cursor:
        cursor.execute("select value, tag, log_name from metadata")
        metadata = [
            {"value": value, "tag": tag, "log_name": log_name}
            for value, tag, log_name in cursor.fetchall()
        ]

    return json.dumps({"metadata": metadata})


@get("/get/raw/<name>")
def get_raw(name):
    """Download for log contents that have been previously
    inserted in the database in the same state

    :param name: Name of the test to be downloaded
    :in name: path
    :type name: string

    :return: Full log data from one test with no decodification done
    :rtype: CSV file
    """
    unixtime = time.mktime(datetime.datetime.now().timetuple())
    filename = name + str(int(unixtime)) + ".csv"
    print(filename)

    conn = connect()
    with conn.cursor() as cursor:
        f = open("exports/" + filename, "w")
        cursor.copy_expert("copy raw_test to STDOUT DELIMITER ',' CSV HEADER", f)
        f.close()

    return static_file(filename, root="exports", download=filename)


@get("/get/fcp/<fcp_name>")
def get_fcp(fcp_name):
    """Specific Boardnet download

    :param name: Name of the Boardnet to be downloaded
    :in name: path
    :type name: string

    :return: Boardnet that defines the FSTLisboa Communication Protocol (FCP)
    :rtype: json file
    """
    unixtime = time.mktime(datetime.datetime.now().timetuple())
    filename = fcp_name + str(int(unixtime)) + ".json"
    print(fcp_name)

    conn = connect()
    with conn.cursor() as cursor:
        f = open("exports/" + filename, "w")
        cursor.copy_expert(
            f"copy (select parser from jsons where json_name='{fcp_name}') to STDOUT", f
        )
        f.close()

    return static_file(filename, root="exports", download=filename)


@post("/fcps")
def insert_fcps():
    """Boardnet insertion into database;
    Also adds new signals present in the Boardnet to the database

    :param fcp: new Boardnet file
    :in fcp: query
    :type fcp: json file

    :param name: Name attributed to new Boardnet
    :in name: query
    :type name: string

    :return: None
    :rtype: None
    """
    fcp = request.files.get("fcp")
    name = request.forms.get("name")

    if fcp.filename.endswith(".json") == False:
        print("Error: invalid file format inserted")
        return

    op = open("sql/insert_signals.sql")
    command = op.read()

    save_path = "../ldb/{name}".format(name=name)
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    json_file_path = "{path}/{file}".format(path=save_path, file=fcp.filename)
    fcp.save(json_file_path, overwrite=True)

    f = open(json_file_path)
    load = json.loads(f.read())

    new_signals = []

    conn = connect()
    for device in load["devices"]:
        for message in load["devices"][device]["msgs"]:
            for signal in load["devices"][device]["msgs"][message]["signals"]:
                mux_count = load["devices"][device]["msgs"][message]["signals"][signal][
                    "mux_count"
                ]
                if mux_count > 1:
                    for i in range(0, mux_count):
                        new_signals.append(tuple([str(signal) + str(i), name]))
                else:
                    new_signals.append(tuple([signal, name]))

    for message in load["common"]["msgs"]:
        for signal in load["common"]["msgs"][message]["signals"]:
            mux_count = load["common"]["msgs"][message]["signals"][signal]["mux_count"]
            if mux_count > 1:
                for i in range(0, mux_count):
                    new_signals.append(tuple([str(signal) + str(i), name]))
            else:
                new_signals.append(tuple([signal, name]))

    with conn.cursor() as cursor:

        cursor.execute(
            "insert into jsons(parser, json_name, date) values (%s, %s, %s)",
            (json.dumps(load), name, datetime.datetime.now()),
        )

        args_str = ",".join(
            cursor.mogrify("(DEFAULT,%s,%s)", x).decode("utf-8") for x in new_signals
        )
        cursor.execute(command + args_str + "ON CONFLICT (name) DO NOTHING")
        altered_rows = cursor.rowcount
        print("Altered " + str(altered_rows) + " rows")
        if altered_rows < 0:
            print("No execute was performed, aborting...")
            f.close()
            op.close()
            conn.close()
            return
        elif altered_rows == 0:
            f.close()
            op.close()
            conn.close()
            print("No new signals found for new BoardNet, aborting file insertion!")
            print("Please verify the input file")
            return

        cursor.execute(
            "select exists(select 1 from information_schema.tables where table_name='signals')"
        )
        if cursor.fetchone()[0] == "False":
            cursor.execute(
                "ALTER TABLE signals add constraint fk_parser foreign key (first_parser) references jsons(json_name)"
            )
            print("Adding Foreign Key Constraint")

        conn.commit()

    f.close()
    op.close()

    return


@post("/tags")
def insert_tags():
    """Tag insertion into database

    :param tag: name of the tag
    :in tag: query
    :type tag: string

    :return: None
    :rtype: None
    """
    tag = request.forms.get("tag")
    print(tag)

    conn = connect()
    with conn.cursor() as cursor:
        cursor.execute("insert into tags(tag) values (%s)", [tag])

        conn.commit()

    return json.dumps({})


@post("/logs")
def insert_logs():
    """Log insertion into database;
    Values are added to 3 different tables in the database: decoded_test, raw_test and logs.
    All data goes directly to raw_test, which is decoded and inserted into decoded_test after.
    If the proccess is successful, the log information is inserted into logs table, otherwise
    the insertion process is aborted.

    :param csv: log file with raw test data
    :in csv: query
    :type csv: csv file

    :param name: name attributed to the log
    :in name: query
    :type name: string

    :param fcp_name: name of Boardnet chosen from ones present in the database
    :in fcp_name: query
    :type fcp_name: string

    :return: None
    :rtype: None
    """
    csv = request.files.get("csv")
    name = request.forms.get("name")
    fcp_name = request.forms.get("fcp")
    tags = request.forms.getall("tag")
    values = request.forms.getall("values")

    print(name, fcp_name, tags, values)

    if csv.filename.endswith(".csv") == False:
        print("Error: Invalid file format inserted")
        return

    save_path = "../ldb/{name}".format(name=name)
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    csv_file_path = "{path}/{file}".format(path=save_path, file=csv.filename)
    csv.save(csv_file_path, overwrite=True)

    conn = connect()
    with conn.cursor() as cursor:
        cursor.execute(f"select parser from jsons where json_name='{fcp_name}'")
        json_fp = cursor.fetchone()[0]

    read_data(csv_file_path, json_fp, name, fcp_name)

    insert_log_meta(name, values, tags, "sql/insert_log_metadata.sql")

    return


# Download Simple
@get("/download")
def download_logs():
    """Values download from a defined time frame and list of signals
    by the request

    :param init_time: initial time from which to get data
    :in init_time: query
    :type init_time: datetime

    :param end_time: finish time from which to get data
    :in end_time: query
    :type end_time: datetime

    :param file_ext: file extension desired for the file.
                     Has two possible values 'CSV' and 'MAT' depending
                     on what file format for download is desired.
    :in file_ext: query
    :type file_ext: string

    :param signals: list of signals desired
    :in signals: query
    :type signals: list

    :return: file from desired timeframe with only the values for all signals selected
    :rtype: .csv file if file_ext selected is 'CSV'
            .mat file if file_ext selected is 'MAT'
    """
    init_time = request.query["init_time"]
    end_time = request.query["end_time"]
    file_ext = request.query["file_extension"]
    signals = request.query.getlist("signal_list")
    tags = request.query.getlist("tag")
    values = request.query.getlist("values")

    unixtime = time.mktime(datetime.datetime.now().timetuple())

    print(init_time, end_time, file_ext, signals, tags, values)

    if file_ext == "CSV":
        filename = "FST" + str(int(unixtime)) + ".csv"
        print(filename)

        download_csv(tags, values, signals, init_time, end_time, filename)
    elif file_ext == "MAT":
        filename = "FST" + str(int(unixtime)) + ".mat"
        print(filename)

        list_dict = download_mat(tags, values, signals, init_time, end_time)

        savemat(filename, list_dict)
    else:
        print("ERROR: Invalid file extension")
        return

    return static_file(filename, root="exports", download=filename)
