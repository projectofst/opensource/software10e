import psycopg2
from config import config
from connect import connect as ldb_connect
from parse_line_data import get_values
from parse_line_data import get_timestart


def insert_values_raw(values_list, sql):
    op = open(sql)
    command = op.read()
    conn = ldb_connect()
    cur = conn.cursor()

    args_str = ",".join(
        cur.mogrify("(%s,%s,%s,%s)", x).decode("utf-8") for x in values_list
    )
    cur.execute(command + args_str)
    conn.commit()

    cur.close()
    op.close()
    if conn is not None:
        conn.close()


def insert_values_decoded(values_list, sql):
    op = open(sql)
    command = op.read()
    conn = ldb_connect()
    cur = conn.cursor()

    args_str = ",".join(
        cur.mogrify("(%s,%s,%s)", x).decode("utf-8") for x in values_list
    )
    cur.execute(command + args_str)
    conn.commit()

    cur.close()
    op.close()

    if conn is not None:
        conn.close()


def insert_log(values_list, sql):
    op = open(sql)
    command = op.read()
    conn = ldb_connect()

    cur = conn.cursor()
    cur.execute(command, values_list)
    conn.commit()

    cur.close()
    op.close()

    if conn is not None:
        conn.close()


def insert_log_meta(log_name, values, tags, sql):
    op = open(sql)
    command = op.read()
    conn = ldb_connect()

    log_meta = zip(values, tags)

    cur = conn.cursor()
    cur.execute(command)
    for val, tag in log_meta:
        cur.execute("EXECUTE metadataplan(%s,%s,%s)", (val, tag, log_name))
    conn.commit()

    cur.close()
    op.close()

    if conn is not None:
        conn.close()
