import sys
import click
import psycopg2
from scipy.io import savemat
import numpy as np
from connect import connect as ldb_connect
from create_table import create_table
from read_csv import read_log_data
from read_csv import read_data


@click.command()
def create_raw():
    create_tables()


@click.command()
@click.argument("path")
def export_raw_csv(path):
    conn = ldb_connect()
    with conn.cursor() as cursor:
        f = open(path, "w")
        cursor.copy_expert("copy raw_test to STDOUT DELIMITER ',' CSV HEADER", f)
        f.close()


@click.command()
@click.argument("path")
@click.argument("signal")
def export_decoded_csv(path, signal):
    conn = ldb_connect()
    with conn.cursor() as cursor:
        f = open(path, "w")
        cursor.copy_expert(
            f"copy (select * from decoded_test where signal='{signal}') to STDOUT DELIMITER ',' CSV HEADER",
            f,
        )


@click.command()
@click.argument("path")
@click.argument("signal")
@click.argument("test")
def export_decoded_mat(path, signal, test):
    conn = ldb_connect()
    with conn.cursor() as cursor:
        cursor.execute(f"select start_time from logs where log_name='{test}'")
        start_time = cursor.fetchone()[0]
        cursor.execute(f"select time, value from decoded_test where signal='{signal}'")
        timeseries = cursor.fetchall()

    time = np.array([(time - start_time).total_seconds() for time, value in timeseries])
    values = np.array([value for time, value in timeseries])
    savemat(path, {signal: {"time": time, "signals": {"values": values}}})


@click.command()
@click.argument("path")
@click.argument("json_file")
@click.argument("log_name")
def import_log(path, json_file, log_name):
    read_data(path, json_file, log_name)
    print("All logs imported")


@click.command()
def create_tables():
    def log(f, *arg):
        print(f"Creating table:", *arg)
        create_table(*arg)

    log(create_table, "sql/create_jsons.sql")
    log(create_table, "sql/create_signals.sql")
    log(create_table, "sql/create_decoded.sql")
    log(create_table, "sql/create_raw.sql")
    log(create_table, "sql/create_tag.sql")
    log(create_table, "sql/create_logs.sql")
    log(create_table, "sql/create_log_metadata.sql")
    print("Done")


@click.command()
def delete_tables():
    r = input("About to delete db tables do you want to proceed? [y/N]")
    if not (r == "y" or r == "Y"):
        return

    conn = ldb_connect()
    with conn.cursor() as cursor:
        cursor.execute("drop table decoded_test cascade")
        cursor.execute("drop table raw_test cascade")
        cursor.execute("drop table jsons cascade")
        cursor.execute("drop table metadata cascade")
        cursor.execute("drop table tags cascade")
        cursor.execute("drop table logs cascade")
        cursor.execute("drop table signals cascade")

    conn.commit()


@click.command()
def api():
    from bottle import run
    from api import fcp_list

    run(host="0.0.0.0", port=8180, debug=True)


@click.command()
def website():
    from bottle import run
    from website import index

    run(host="0.0.0.0", port=8181, debug=True)


@click.group(invoke_without_command=True)
@click.option("--version", is_flag=True, default=False)
def main(version):
    if len(sys.argv) == 1:
        print("ldb tool")


main.add_command(create_raw)
main.add_command(export_raw_csv)
main.add_command(export_decoded_csv)
main.add_command(export_decoded_mat)
main.add_command(create_tables)
main.add_command(import_log)
main.add_command(delete_tables)
main.add_command(api)
main.add_command(website)

if __name__ == "__main__":
    main()
