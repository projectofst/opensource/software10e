import psycopg2
from config import config
from connect import connect as ldb_connect


def create_table(sql):
    op = open(sql)
    command = op.read()
    conn = None
    try:
        conn = ldb_connect()
        cur = conn.cursor()
        cur.execute(command)
        cur.close()
        op.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
