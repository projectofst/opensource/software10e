## DATABASE

The database needs to have a .ini file that defines the connection to the local database. It must be something like this example:

```
[postgresql]
host=localhost
database=FSTDB
user=testuser
password=pass1234
```

## API

Run the api server

```
python main.py api
```

Make a get request to the server for the test names

```
curl http://10.16.233.206:8180/tests
```

##  WebSite

Run the website (needs api and database)

...
python main.py website
...

To Access the website, you must be connected to Tecnico api found in this link: [api install](https://si.tecnico.ulisboa.pt/en/servicos/redes-e-conetividade/vpn/)

Afterwards, you can connect directly through this link: (http://10.16.233.206:8181)
