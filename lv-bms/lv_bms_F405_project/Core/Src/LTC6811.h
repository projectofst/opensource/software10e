/* Include guard */
#ifndef _LTC6811_
#define _LTC6811_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <../Inc/spi.h>

/**
 * @name LTC6811 timings
 * @{
 */
#define TYP_T_WAKE  200 /**< [us] */
#define MAX_T_WAKE  400 /**< [us] */

#define MIN_T_SLEEP 1800000 /**< [us] */
#define TYP_T_SLEEP 2000000 /**< [us] */
#define MAX_T_SLEEP 2200000 /**< [us] */

#define MIN_T_REFUP 2700    /**< [us] */
#define TYP_T_REFUP 3500    /**< [us] */
#define MAX_T_REFUP 4400    /**< [us] */

#define MAX_T_READY 10  /**< [us] */

#define MIN_T_IDLE  4300    /**< [us] */
#define TYP_T_IDLE  5500    /**< [us] */
#define MAX_T_IDLE  6700    /**< [us] */

/* Conversion times */
#define T_CONV_CELL_ALL_7K 2300    /**< [us] */
#define T_CONV_CELL_ONE_7K 405     /**< [us] */
#define T_CONV_GPIO_ONE_7K  405 /** [us] */
#define T_CONV_STATUS_ALL_7K 1600   /** [us] */
#define T_CONV_STATUS_ONE_7K 405    /** [us] */

/** LTC 6812 */
#define T_CONV_GPIO_ALL_7K  4 /** [ms] */
#define T_CONV_GPIO_ALL_PLUS_REFERENCE_7K 2  /** [ms] */

/*@}*/

/**
 * @name LTC6812 commands
 * @{
 */
#define WRCFGA      0x0001  /** Write to Configuration Register (CFGR) */
#define WRCFGB      0x0024
#define RDCFGA      0x0002  /** Read the Configuration Register (CFGR) */
#define RDCFGB      0x0026
#define RDCVA       0x0004  /** Read the Cell Voltage Register A (CVAR) */
#define RDCVB       0x0006  /** Read the Cell Voltage Register B (CVBR) */
#define RDCVC       0x0008  /** Read the Cell Voltage Register C (CBCR) */
#define RDCVD       0x000A  /** Read the Cell Voltage Register D (CBDR) */
#define RDCVE       0x0009
#define RDAUXA      0x000C  /** Read the Auxiliary Register Group A (AVAR) */
#define RDAUXB      0x000E  /** Read the Auxiliary Register Group B (AVBR) */
#define RDAUXC      0x000D
#define RDAUXD      0x000F
#define RDSTATA     0x0010  /** Read the Status Register Group A (STAR) */
#define RDSTATB     0x0012  /** Read the Status Register Group B (STBR) */
#define WRSCTRL     0x0014  /** Write to the S Control Register Group (SCTRL) */
#define WRPWM       0x0020  /** Write to the PWM Register Group (PWMR) */
#define WRPSB       0x001C
#define RDSCTRL     0x0016  /** Read the S Control Register Group (SCTRL) */
#define RDPWM       0x0022  /** Read the PWM Register Group (PWMR) */
#define RDPSB       0x001E
#define STSCTRL     0x0019  /** Start S Control pulsing and poll status */
#define CLRSCTRL    0x0018  /** Clear S Control register */


/**
 * @brief Start Cell Voltage ADC Conversion and Poll Status
 */
#define ADCV(MD,DCP,CH)     (0x260 | (MD<<7) | (DCP<<4) | (CH))
/**
 * @brief Start Open Wire ADC Conversion and Poll Status
 */
#define ADOW(MD,PUP,DCP,CH) (0x228 | (MD<<7) | (PUP<<6) | (DCP<<4) | (CH))
/**
 * @brief Start Self Test Cell Voltage Conversion and Poll Status
 */
#define CVST(MD,ST)         (0x207 | (MD<<7) | (ST<<5))
/**
 * @brief Start Overlap Measurement of Cell 7 Voltage
 */
#define ADOL(MD,DCP)        (0x201 | (MD<<7) | (DCP<<4))
/**
 * @brief Start GPIOs ADC Conversion and Poll Status
 */
#define ADAX(MD,CHG)        (0x460 | (MD<<7) | (CHG))
/**
 * @brief Start GPIOs ADC Conversion With Digital Redundancy and Poll Status
 */
#define ADAXD(MD,CHG)       (0x400 | (MD<<7) | (CHG))
/**
 * @brief Start GPIOs Open Wire ADC COnversion and Poll Status
 */
#define AXOW(MD,PUP,CHG)    (0x410 | (MD<<7) | (CHG))
/**
 * @brief Start Self Test GPIOs Conversion and Poll Status
 */
#define AXST(MD,ST)         (0x407 | (MD<<7) | (ST<<5))
/**
 * @brief Start Status Group ADC Conversion and Poll Status
 */
#define ADSTAT(MD,CHST)     (0x468 | (MD<<7) | (CHST))
/**
 * @brief Start Status Group ADC Conversion With Digital Redundancy and Poll
 * Status
 */
#define ADSTATD(MD,CHST)    (0x408 | (MD<<7) | (CHST))
/**
 * @brief Start Self Test Status Group Conversion and Poll Status
 */
#define STATS(MD,ST)        (0x40F | (MD<<7) | (ST<<5))
/**
 * @brief Start Combined Cell Voltage and GPIO1, GPIO2 Conversion and Poll
 * Status
 */
#define ADCVAX(MD,DCP)      (0x46F | (MD<<7) | (DCP<<4))
/**
 * @brief Start Combined Cell Voltage and SC Conversion and Poll Status
 */
#define ADCVSC(MD,DCP)      (0x467 | (MD<<7) | (DCP<<4))


#define CLRCELL     0x0711 /** Clear Cell Voltage Register Groups */
#define CLRAUX      0x0712 /** Clear Auxiliary Register Groups */
#define CLRSTAT     0x0713 /** Clear Status Register Groups */
#define PLADC       0x0714 /** Poll ADC Conversion Status */
#define DIAGN       0x0715 /** Diagnose MUX and Poll Status */
#define WRCOMM      0x0721 /** Write COMM Register Group */
#define RDCOMM      0x0722 /** Read COMM Register Group */
#define STCOMM      0x0723 /** Start I2C /SPI Communication */
#define MUTE        0x0028 /** Mute Discharge */
#define UNMUTE      0x0029 /** Unmute Discharge */
/*@}*/


/**
 * @name LTC6811 command options
 * @{
 */

/* ADC Mode */
enum LTC6812_COMMAND_MD{
    MD_MODE_0   =   0b00,
    MD_MODE_1   =   0b01,
    MD_FAST     =   0b01,
    MD_MODE_2   =   0b10,
    MD_NORMAL   =   0b10,
    MD_MODE_3   =   0b11,
    MD_FILTERED =   0b11,
};

/* Discharge Permitted */
enum LTC6812_COMMAND_DCP{
    DCP_DISCHARGE_NOT_PERMITTED = 0,
    DCP_DISCHARGE_PERMITTED     = 1,
};

/* Cell Selection for ADC Conversion */
enum LTC6812_COMMAND_CH{
    CH_ALL_CELLS     = 0b000,
    CH_CELLS_1_6_11  = 0b001,
    CH_CELLS_2_7_12  = 0b010,
    CH_CELLS_3_8_13  = 0b011,
    CH_CELLS_4_9_14  = 0b100,
    CH_CELLS_5_10_15 = 0b101,
};

/* Pull-Up/Pull-Down Current for Open Wire Conversions */
enum LTC6812_COMMAND_PUP{
    PUP_PULL_DOWN   = 0,
    PUP_PULL_UP     = 1,
};

/* Self Test Mode Selection */
enum LTC6812_COMMAND_ST{
    ST_SELF_TEST1 = 0b01,
    ST_SELF_TEST2 = 0b10,
};

/* GPIO Selection for ADC Conversion */
enum LTC6812_COMMAND_CHG{
    CHG_ALL_GPIO        = 0b000,
    CHG_GPIO_1_6        = 0b001,
    CHG_GPIO_2_7        = 0b010,
    CHG_GPIO_3_8        = 0b011,
    CHG_GPIO_4_9        = 0b100,
    CHG_GPIO5          = 0b101,
    CHG_2ND_REFERENCE   = 0b110,
};

/* Status Group Selection */
enum LTC6812_COMMAND_CHST{
    CHST_SC_ITMP_VA_VD  = 0b000,
    CHST_SC             = 0b001,
    CHST_ITMP           = 0b010,
    CHST_VA             = 0b011,
    CHST_VD             = 0b100,
};
/**@}*/

/**
 * @name LTC6811 States
 * @{
 */
typedef enum LTC6811_core_state_enum{
    CORE_SLEEP_STATE,
    CORE_STANDBY_STATE,
    CORE_REFUP_STATE,
    CORE_MEASURE_STATE,
} LTC6811_Core_State;

typedef enum LTC6811_isoSPI_state_enum{
    ISOSPI_IDLE_STATE,
    ISOSPI_READY_STATE,
    ISOSPI_ACTIVE_STATE,
} LTC6811_isoSPI_State;
/**@}*/


/**
 * @brief Structure containing data needed for LTC6811 daisy chain communication
 *
 * One of this structures must be declared for each isoSPI daisy chain
 * connected.
 *
 * Pointers to enable_comm and disable_comm functions should be given to each
 * declared struct so that the slave select pins can be used.
 */
typedef struct LTC6811_daisy_chain{
    uint16_t n; /**< Number of LTC6811 ICs on the daisy chain */

    LTC6811_Core_State core_state;  /**< Current core state */
    LTC6811_isoSPI_State interface_state;   /**< Current isoSPI interface state */
    /**
     * @brief Function to enable (SS) communication with the LTC6811 daisy chain
     *
     * This usually involves pulling the Slave Select (SS) pin LOW.
     */
    void (*enable_comm)(void);
    /**
     * @brief Function to disable (SS) communication with the LTC6811 daisy chain
     *
     * This usually involves pulling the Slave Select (SS) pin HIGH.
     */
    void (*disable_comm)(void);

    /**
     * @brief Function to reset the watchdog monitoring when the isoSPI
     * interface goes to IDLE state or the core goes to SLEEP state.
     */
    void (*reset_interface_watchdog)(void);
} LTC6811_daisy_chain;


#define LTC6811_REG_SIZE    6 /**< LTC6811 register size in bytes */

/**
 * @brief Structure describing the LTC6811 register contents
 */
typedef struct LTC6811_struct{
    /**
     * @brief Contents of Configuration Register Group (CFGR)
     */
    union{
        struct{
            uint8_t ADCOPT : 1;
            uint8_t DTEN   : 1;
            uint8_t REFON  : 1;
            uint8_t GPIO   : 5;
            uint32_t       : 24;
            uint16_t DCC   : 12;
            uint8_t DCTO   : 4;
        };
        uint8_t CFGR[6];
    };

    /**
     * @brief Contents of Configuration Register Group (CFGRB)
     */
    union{
        struct{
            uint8_t GPIO_6_9: 4;
            uint8_t DCC_2   : 3;
            uint8_t RSVD1   : 3;
            uint8_t DCC0    : 1;
            uint8_t DTMEN   : 1;
            uint8_t PS      : 2;
            uint8_t FDRF    : 1;
            uint8_t MUTE_R  : 1;
            uint32_t        : 32;
        };
        uint8_t CFGRB[6];
    };
    /**
     * @brief Contents of Cell Voltage Register Group A (CVAR)
     */
    union{
        struct{
            uint16_t C1V;
            uint16_t C2V;
            uint16_t C3V;
        };
        uint8_t CVAR[6];
    };
    /**
     * @brief Contents of Cell Voltage Register Group B (CVBR)
     */
    union{
        struct{
            uint16_t C4V;
            uint16_t C5V;
            uint16_t C6V;
        };
        uint8_t CVBR[6];
    };
    /**
     * @brief Contents of Cell Voltage Register Group C (CVCR)
     */
    union{
        struct{
            uint16_t C7V;
            uint16_t C8V;
            uint16_t C9V;
        };
        uint8_t CVCR[6];
    };
    /**
     * @brief Contents of Cell Voltage Register Group D (CVDR)
     */
    union{
        struct{
            uint16_t C10V;
            uint16_t C11V;
            uint16_t C12V;
        };
        uint8_t CVDR[6];
    };

    /**
     * @brief Contents of Auxiliary Register Group A (AVAR)
     */
    union{
        struct{
            uint16_t G1V;
            uint16_t G2V;
            uint16_t G3V;
        };
        uint8_t AVAR[6];
    };
    /**
     * @brief Contents of Auxiliary Register Group B (AVBR)
     */
    union{
        struct{
            uint16_t G4V;
            uint16_t G5V;
            uint16_t REF;
        };
        uint8_t AVBR[6];
    };

    /**
     * @brief Contents of Status Register Group A (STAR)
     */
    union{
        struct{
            uint16_t SC;
            uint16_t ITMP;
            uint16_t VA;
        };
        uint8_t STAR[6];
    };
    /**
     * @brief Contents of Status Register Group B (STBR)
     */
    union{
        struct{
            uint16_t VD;
            uint32_t CUOV   : 24; /**< CxUV and CxOV bits interleaved */
            bool THSD   : 1;
            bool MUXFAIL    : 1;
            uint8_t RSVD    : 2;
            uint8_t REV : 4;
        };
        uint8_t STBR[6];
    };

    /**
     * @brief Array for COMM Register Group
     *
     * This register isn't used and its contents are very strange
     */
    uint8_t COMM[6];

    /**
     * @brief S Control Register Group (SCTRL)
     */
    union{
        struct{
            uint8_t SCTL1   : 4;
            uint8_t SCTL2   : 4;
            uint8_t SCTL3   : 4;
            uint8_t SCTL5   : 4;
            uint8_t SCTL4   : 4;
            uint8_t SCTL6   : 4;
            uint8_t SCTL7   : 4;
            uint8_t SCTL8   : 4;
            uint8_t SCTL9   : 4;
            uint8_t SCTL10  : 4;
            uint8_t SCTL11  : 4;
            uint8_t SCTL12  : 4;
        };
        uint8_t SCTRL[6];
    };

    /**
     * @brief PWM Register Group (PWMR)
     */
    union{
        struct{
            uint8_t PWM1   : 4;
            uint8_t PWM2   : 4;
            uint8_t PWM3   : 4;
            uint8_t PWM5   : 4;
            uint8_t PWM4   : 4;
            uint8_t PWM6   : 4;
            uint8_t PWM7   : 4;
            uint8_t PWM8   : 4;
            uint8_t PWM9   : 4;
            uint8_t PWM10  : 4;
            uint8_t PWM11  : 4;
            uint8_t PWM12  : 4;
        };
        uint8_t PWMR[6];
    };

} LTC6811;


/**
 * @brief Initializes a LTC6811 struct
 */
void LTC6811_init(LTC6811* ltc);

void daisy_chain_B_init(LTC6811_daisy_chain* daisy_chain_B);

/**
 * @brief Wakes up an entire daisy chain from core SLEEP state
 *
 * @warning
 * The function is blocking and takes daisy_chain_n * MAX_T_WAKE.
 * This is about 2.4ms for a 6 IC daisy chain and 4.8ms for a 12 IC daisy chain
 */
void wake_up_LTC6811_from_SLEEP(LTC6811_daisy_chain* daisy_chain);
/**
 * @brief Wakes up an entire daisy chain from isoSPI interface IDLE state
 *
 * @warning
 * The function is blocking and takes daisy_chain_n * MAX_T_READY.
 * This is about 60us for a 6 IC daisy chain and 120us for a 12 IC daisy chain
 */
void wake_up_LTC6811_from_IDLE(LTC6811_daisy_chain* daisy_chain);

/**
 * @brief Broadcasts a poll command nice
 *
 * @param [in]  daisy_chain LTC6811 daisy chain to broadcast the command
 * @param [in]  command     Command to broadcast
 */
int broadcast_poll(SPI_HandleTypeDef* hspi, LTC6811_daisy_chain* daisy_chain, unsigned int command);

/**
 * @brief Writes size*daisy_chain.n bytes from the location pointed by data and
 * sends them to the daisy_chain using command.
 *
 * @param   [in]    daisy_chain LTC6811 daisy chain to write to.
 * @param   [in]    command     Command to use to write
 * @param   [in]    size        Number of bytes to write to each LTC6811
 * @param   [in]    data        Location where the data to write is
 */
int broadcast_write(
        SPI_HandleTypeDef* hspi,
        LTC6811_daisy_chain* daisy_chain,
        unsigned int command,
        unsigned int size,
        unsigned char *data);

/**
 * @brief Reads size*daisy_chain.n bytes from the daisy_chain using command and
 * stores them in the location pointed by data
 *
 * @param   [in]    daisy_chain LTC6811 daisy chain from where to read.
 * @param   [in]    command     Command to use to read
 * @param   [in]    size        Number of bytes to read from each LTC6811
 * @param   [out]   data        Location to store the read data
 */
int broadcast_read(
        SPI_HandleTypeDef* hspi,
        LTC6811_daisy_chain* daisy_chain,
        unsigned int command,
        unsigned int size,
        unsigned char *data);

#endif /* End include guard */
