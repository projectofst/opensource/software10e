/* Include Guard */
/* Include Guard */
#ifndef _LV_BATTERY_H_
#define _LV_BATTERY_H_

/*
 * Data structure containing all available info about the LV Battery.
 */
#include <stdbool.h>
#include <stdint.h>

#define LTC_TEMP_SIZE 5
#define LTC_VOLT_SIZE 6 //Only 6 cells will be used.
#define LTC_TOTAL_SIZE LTC_TEMP_SIZE + LTC_TEMP_SIZE
#define NTC_TEMP_SIZE 3
#define CELL_SERIES_TOTAL 6
#define VOLT_SUMMARY_SIZE 2
#define TEMP_SUMMARY_SIZE 2
#define TOTAL_SUMMARY_SIZE VOLT_SUMMARY_SIZE + TEMP_SUMMARY_SIZE
#define ERROR_SIZE 8
#define ERROR_ACTIVE 5 //Until we have all errors implemented.
//Below values not final, only placeholders.
#define CELL_LIMIT_MAX_T 30
#define CELL_LIMIT_MIN_T 20
#define CELL_LIMIT_MAX_V 3799
#define CELL_LIMIT_MIN_V 3199 
#define CELL_LIMIT_MAX_I_CHRG_CONT (-145) /**< [mA*100] */
#define CELL_LIMIT_MAX_I_CHRG (-290) /**< [mA*100] */
#define CELL_LIMIT_MAX_I_DISCH_CONT 1000 /**< [mA*100] */
#define CELL_LIMIT_MAX_I_DISCH 1000 /**< [mA*100] */
#define NUM_VALID_NTC 5
#define OWC_ERROR_DIFF 400

#define OVERVOLTAGE_TIME 1000 //[ms]
#define UNDERVOLTAGE_TIME 1000 //[ms]
#define OVERTEMP_TIME 1000 //[ms]
#define UNDERTEMP_TIME 1000 //[ms]
#define OVERCURRENT_UP_TIME 1000 //[ms]
#define OVERCURRENT_DOWN_TIME 1000 //[ms]
#define LTC_ERROR_TIME 1000 //[ms]
#define FAKE_ERROR_TIME 1000 //[ms]

typedef struct set_struct {
    uint16_t* volt_max_limit;
    uint16_t* volt_min_limit;
    uint16_t* volt_bat_limit;
    uint16_t* temp_max_limit;
    uint16_t* temp_min_limit;
    uint16_t* charging_delta_limit;
    uint16_t* aparent_cell_nominal_capacity;
    //uint16_t defaults[CFG_LV_BMS_SIZE];
} Settables;

/**
 * @brief Structure containing all data gathered by LTC
*/
typedef struct ltc_info_struct {
    uint16_t cell_temp[LTC_TEMP_SIZE];
    uint16_t cell_volt[LTC_VOLT_SIZE]; //[mV]
    //Stores state of all ltc sensors.
    bool sensor_ok [LTC_TOTAL_SIZE];
    //Stores stage of OWC sequence.
    uint16_t OWC_sequence;

    /**
     * Bitfiled to keep faulty cell connections
     * Basically if this is not 0 then the TS must be turned off
     */
    uint16_t cell_connection_fault_mask; /**< [Fault - '1' / No fault - '0'] */

    /**
     * Bitfield to keep faulty cell NTC
     */
    uint16_t* temp_fault_mask; /**< [Fault - '1' / No fault - '0'] */

    /**
     * Number of valid NTC in battery
     */
    uint16_t n_valid_cell_NTC;
    
    /**
     * Array of cell voltage when performing OWC in pull up
     */
    uint16_t cell_pu[CELL_SERIES_TOTAL];

    /**
     * Array of cell voltage when performing OWC in pull down
     */
    uint16_t cell_pd[CELL_SERIES_TOTAL];

    /**
     * Difference from pull up and pull down measurements in OWC verification
     */
    int16_t OWC_diff[CELL_SERIES_TOTAL];

    /**
     * Bitfiled to keep status about cell connections
     * 0 - not open, 1 - connection opened
     */
    uint16_t OWC_cell_status; /**< [OPEN/CLOSED] */

} LTC_info;

/**
 * @brief Structure containing non-critical data aquired by NTCs
*/
typedef struct ntc_info_struct {
    //Stores Ambient temp + balancing circuit temp
    uint16_t temp[NTC_TEMP_SIZE];
} NTC_info;

/**
 * @brief Structure containing essential voltage info.
*/
typedef struct volt_summary_struct {
    //total battery output voltage
    uint16_t batt_volt;
    //maximum voltage amomg all cells
    uint16_t max_volt;
    //average voltage of all cells
    uint16_t mean_volt;
    //mainimum voltage amomg all cells
    uint16_t min_volt;
    //cell with the highest voltage
    uint8_t max_volt_cell;
    //cell with the lowest voltage
    uint8_t min_volt_cell;
    //delta between high and lowest cell voltages
    uint8_t max_min_volt_delta;
} Volt_summary;

typedef struct temp_summary_struct {
    uint16_t max_temp;
    uint16_t mean_temp;
    uint16_t min_temp;
    uint8_t max_temp_cell;
    uint8_t min_temp_cell;
} Temp_summary;

typedef struct lv_cell_info_struct {
    uint8_t id;
    uint16_t* voltage; //=&(Battery data->LTC_info->cell_volt[0])
    uint16_t* temperature;	//=&(Battery data->LTC_info->cell_temp[0])
} LV_cell_info;

typedef struct lv_bms_status_struct {
    bool bms_ok;
    bool verbose;
    bool charging;
    bool balancing_ok;
} LV_BMS_status;


typedef struct lv_bms_error_reason_struct {
    bool reason_overvoltage;
    bool reason_undervoltage;
    bool reason_overtemperature;
    bool reason_undertemperatue;
    bool reason_overcurrent_in;
    bool reason_overcurrent_out;
    bool reason_min_ltc;
    bool reason_fake_error;
} LV_BMS_ERROR_reason;

typedef struct lv_battery_struct {

  LTC_info ltc_info;
  NTC_info ntc_info;
  Volt_summary volt_summary;
  Temp_summary temp_summary;
  LV_cell_info lv_cell_info[CELL_SERIES_TOTAL];
  uint16_t current_sensor_in;
  uint16_t current_sensor_out;
  uint16_t state_of_charge;
  Settables settables;
  LV_BMS_status lv_bms_status;
  //LV_BMS_ERROR_reason lv_bms_error_reason;
  bool lv_bms_error_reason[ERROR_SIZE];
} LV_Battery;

//OLDvoid lv_battery_init(LV_Battery* lv_battery);

#endif /* End include guard */
