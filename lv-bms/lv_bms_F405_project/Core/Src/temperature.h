#ifndef __TEMP_H_
#define __TEMP_H_

#include <stdbool.h>
#include <stdint.h>
#include "utils.h"

#define MAX_VALID_TEMP_VOLT 27325 /**< [mv/10] */
#define MIN_VALID_TEMP_VOLT 1133 /**< [mv/10] */

/**
 * @brief Converts ntc voltage to temperature
 */
int16_t convert_ntc_temp(uint16_t v);

bool NTC_is_faulty(uint16_t voltage);
bool NTC_is_valid(uint8_t temp_fault_mask, uint8_t index);
uint8_t count_valid_NTC(uint8_t temp_fault_mask);
#endif
