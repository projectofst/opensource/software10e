//
//  unit_init.c
//  LV_BMS_struct_test
//
//  Created by João Ruas on 08/07/2020.
//  Copyright © 2020 João Ruas. All rights reserved.
//

#include "unit_init.h"

void random_ltc_temp_and_volt_input(LV_Battery* _lv_bms_data, int seed)
{
    srand(seed);
    int rand_volt = 0;
    int rand_temp = 0;
    int i = 0;
    for (i = 0; i < LTC_VOLT_SIZE; i++) {
        rand_volt = (rand() % 900 + 3000); //rand between3.0-3.9Volt
        _lv_bms_data->ltc_info.cell_volt[i] = rand_volt;
    }
    for (i = 0; i < LTC_TEMP_SIZE; i++) {
        rand_temp = (rand() % 25 + 15); //rand between15-40
        _lv_bms_data->ltc_info.cell_temp[i] = rand_temp;
    }
}

/*
void random_summary_values(LV_Battery* _lv_bms_data)
{
    //We'll generate some random values for summary values as
    //a placeholder until we write the proper data processing.
    srand((unsigned)99); //random seed
    int rand_volt[VOLT_SUMMARY_SIZE] = { 0 };
    int rand_temp[TEMP_SUMMARY_SIZE] = { 0 };
    int i = 0;
    for (i = 0; i < VOLT_SUMMARY_SIZE; i++) {
        rand_volt[i] = (rand() % 900 + 3000); //rand between3.0-3.9Volt
    }
    printf("summary volt: ");
    _lv_bms_data->volt_summary.max_volt = rand_volt[0];
    printf("%d ", _lv_bms_data->volt_summary.max_volt = rand_volt[0]);
    _lv_bms_data->volt_summary.max_volt = rand_volt[1];
    printf("%d ", _lv_bms_data->volt_summary.min_volt = rand_volt[1]);
    printf("\n");

    for (i = 0; i < TEMP_SUMMARY_SIZE; i++) {
        rand_temp[i] = (rand() % 25 + 15); //rand between15-40
    }
    printf("summary temp: ");
    _lv_bms_data->temp_summary.max_temp = rand_temp[0];
    printf("%d ", _lv_bms_data->temp_summary.max_temp = rand_temp[0]);
    _lv_bms_data->temp_summary.min_temp = rand_temp[1];
    printf("%d ", _lv_bms_data->temp_summary.min_temp = rand_temp[1]);
    printf("\n");
}
*/

void ltc_temp_and_volt_print(LV_Battery* _lv_bms_data)
{
    srand((unsigned)99); //random seed
    int i = 0;
    printf("temps: ");
    for (i = 0; i < LTC_TEMP_SIZE; i++) {
        printf("%d ", _lv_bms_data->ltc_info.cell_temp[i]);
    }
    printf("\n");

    printf("milivolts: ");
    for (i = 0; i < LTC_VOLT_SIZE; i++) {
        printf("%d ", _lv_bms_data->ltc_info.cell_volt[i]);
    }
    printf("\n");
}
