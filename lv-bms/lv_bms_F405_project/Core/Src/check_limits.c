
#include "check_limits.h"

/*Initilize error struct with defined constants*/
ERROR_STRUCT lv_bms_errors = {

    .error_limits = {
        CELL_LIMIT_MAX_V,
        CELL_LIMIT_MIN_V,
        CELL_LIMIT_MAX_T,
        CELL_LIMIT_MIN_T,
        NUM_VALID_NTC },

    .error_time_threshold = { OVERVOLTAGE_TIME, UNDERVOLTAGE_TIME, OVERTEMP_TIME, UNDERTEMP_TIME, OVERCURRENT_UP_TIME, OVERCURRENT_DOWN_TIME, LTC_ERROR_TIME, FAKE_ERROR_TIME },
};

bool below_u16(int16_t value, int16_t limit)
{
    return value < limit;
}

bool above_u16(int16_t value, int16_t limit)
{
    return value > limit;
}

bool (*cmp_functions[ERROR_SIZE])(int16_t value, int16_t limit) = {
    above_u16, //max_volt
    below_u16, //min_volt
    above_u16, //max_temp
    below_u16, //min_temp
    below_u16, //num_valid_NTC
};

/**
 * @brief checks input values for error.

 TODO: create a structue that holds all static arguments of this funtion (cmp, limit, reason).
       Implement error delay for each type of error.
       Implement set_bms_ok.
       Implement error state machine?
       Implement fcp log?
*/
bool check_error(bool (*cmp)(int16_t value, int16_t limit),
    int16_t value, int16_t limit)
{

    if (cmp(value, limit)) {
        //printf("error: %d: %d(value) %d(limit)\n", value, limit);
        return 1;
    } else {
        return 0;
    }
}

/**
 * @brief main function for check_limits

*/

void check_limits(can_t* can, LV_Battery* lv_bms_data)
{
    int i = 0;
    bool error = 0;

    int16_t* error_values[ERROR_ACTIVE] = {
        (int16_t*)&(can->lv_bms.lv_bms_summary_volt_1.max_volt),
        (int16_t*)&(can->lv_bms.lv_bms_summary_volt_2.min_volt),
        (int16_t*)&(can->lv_bms.lv_bms_summary_temp.max_temp),
        (int16_t*)&(can->lv_bms.lv_bms_summary_temp.min_temp),
        (int16_t*)&(can->lv_bms.lv_bms_ltc_info_2.num_valid_cell_ntc),
    };

    for (i = 0; i < ERROR_ACTIVE; i++) {
        error = check_error(cmp_functions[i], *(error_values[i]), lv_bms_errors.error_limits[i]);
        lv_bms_data->lv_bms_error_reason[i] = error;
        //Pass error_reason as a 8 bit can data.
    }

    return;
}
