//
//  data_aloc.h
//  LV_BMS_struct_test
//
//  Created by João Ruas on 07/07/2020.
//  Copyright © 2020 João Ruas. All rights reserved.
//

#ifndef data_aloc_h
#define data_aloc_h

#include <stdio.h>
#include <stdlib.h>
#include "data_structures.h"

LV_Battery* get_battery(void);

LV_Battery* init_lv_battery(void);
LTC_info* init_ltc_info(void);

NTC_info* init_ntc_info (void);
Volt_summary* init_volt_summary (void) ;
Settables* init_set (void) ;
LV_cell_info* init_lv_cell_info (void) ;
//LV_BMS_error_reason* init_lv_bms_error_reason (void) ;
Temp_summary* init_temp_summary (void) ;
LV_BMS_status* init_lv_bms_status (void) ;



#endif /* data_aloc_h */
