#include "battery_monitor.h"
#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "communication.h"
#include "check_limits.h"

#ifdef FAKE
void battery_monitor_routine(LTC6811_daisy_chain* ltc_daisy_chain, SPI_HandleTypeDef* hspi)
{

    LV_Battery* lv_bms_data = get_battery();
    unsigned int seed = 99;
    //fake ltc volt and temp
    random_ltc_temp_and_volt_input(lv_bms_data, seed);
    //fake OWC
    //Calculate Summary values
    calculate_summary_values(lv_bms_data);
    return;
}

#else

/**
 * @brief Main battery monitor routine, calls all sub-routines 
 * that aquire data from battery and processes said data.
 */
void battery_monitor_routine(LTC6811_daisy_chain* ltc_daisy_chain, SPI_HandleTypeDef* hspi, ADC_HandleTypeDef* hadc1, ADC_HandleTypeDef* hadc2, ADC_HandleTypeDef* hadc3)
{
    //Fetch global battery data structure.
    LV_Battery* lv_bms_data = get_battery();
    can_t* can = get_can();
    //Get cell voltages.
    get_voltages(can, ltc_daisy_chain, hspi);
    //Get cell temperatures.
    get_temperatures(can, ltc_daisy_chain, hspi);
    //Get additional temperatures from ADC.
    get_adc_temperatures(lv_bms_data, hadc1, hadc2, hadc3);
    //Open wire check for battery cells.
    open_wire_check(can, ltc_daisy_chain, hspi);
    //Process acquired data.
    calculate_summary_values(can);
    //Check if values are within safety limits
    check_limits(can, lv_bms_data);

    return;
}

#endif

/**
 * @brief Sets-up LTC, reads Voltages and saves to battery structure.
 */
void get_voltages(can_t* can, LTC6811_daisy_chain* ltc_daisy_chain, SPI_HandleTypeDef* hspi)
{

    osDelay(500);
    //uint8_t default_CFGR[6] = { 0b11111111, 123, 123, 123, 0, 0 };
    uint16_t voltages[LTC_VOLT_SIZE] = { 0 };
    uint16_t cell;

    wake_up_LTC6811_from_SLEEP(ltc_daisy_chain);
    //broadcast_write(hspi, ltc_daisy_chain, WRCFGA, LTC6811_REG_SIZE, default_CFGR);
    osDelay(10);

    broadcast_poll(hspi, ltc_daisy_chain, ADCV(MD_NORMAL, DCP_DISCHARGE_NOT_PERMITTED, CH_ALL_CELLS));
    osDelay(10);

    //Get voltages from 6 cells.
    broadcast_read(hspi, ltc_daisy_chain, RDCVA, LTC6811_REG_SIZE, (uint8_t*)voltages); //changed to RDCFGA
    for (cell = 0; cell < 3; ++cell) {
        can->lv_bms.lv_bms_cell_info_voltage.volt_value[cell] = voltages[cell];
    }

    //osDelay(5);
    /*
    broadcast_read(hspi, ltc_daisy_chain, RDCVB, LTC6811_REG_SIZE, (uint8_t*)voltages); //changed to RDCFGA
    for (cell = 0; cell < 3; ++cell) {
    	can->lv_bms.lv_bms_cell_info_voltage.volt_value[3 + cell] = voltages[cell];
    }
    */

    if (CELL_SERIES_TOTAL <= 6){
        	return;
    }

    //osDelay(5);
    broadcast_read(hspi, ltc_daisy_chain, RDCVC, LTC6811_REG_SIZE, (uint8_t*)voltages); //changed to RDCFGA
    for (cell = 0; cell < 3; ++cell) {
    	can->lv_bms.lv_bms_cell_info_voltage.volt_value[6 + cell] = voltages[cell];
    }

    //osDelay(5);

    broadcast_read(hspi, ltc_daisy_chain, RDCVD, LTC6811_REG_SIZE, (uint8_t*)voltages); //changed to RDCFGA
    for (cell = 0; cell < 3; ++cell) {
    	can->lv_bms.lv_bms_cell_info_voltage.volt_value[9 + cell] = voltages[cell];
    }


    return;
}

/**
 * @brief Main OWC routine.
 */
void open_wire_check(can_t* can, LTC6811_daisy_chain* ltc_daisy_chain, SPI_HandleTypeDef* hspi)
{
    //1st OWC sequence: Pull Up
    //lv_bms_data->ltc_info.OWC_sequence = 0;
    can->lv_bms.lv_bms_ltc_info_2.owc_sequence = 0;
    start_cell_open_wire_check_conversion(ltc_daisy_chain, hspi, PUP_PULL_UP);
    get_cell_open_wire_check_reading(can, ltc_daisy_chain, hspi);

    //2nd OWC sequence: Pull Down
    //lv_bms_data->ltc_info.OWC_sequence = 1;
    can->lv_bms.lv_bms_ltc_info_2.owc_sequence = 1;
    start_cell_open_wire_check_conversion(ltc_daisy_chain, hspi, PUP_PULL_DOWN);
    get_cell_open_wire_check_reading(can, ltc_daisy_chain, hspi);

    //Check delta between PU and PD.
    open_wire_check_calculation(can);

    return;
}

/**
 * @brief Commands the LTC6811 to start converting the cells voltages
 */
void start_cell_open_wire_check_conversion(LTC6811_daisy_chain* ltc_daisy_chain, SPI_HandleTypeDef* hspi, uint16_t PUP)
{
    broadcast_poll(hspi, ltc_daisy_chain,
        ADOW(MD_FILTERED, PUP, DCP_DISCHARGE_NOT_PERMITTED, CH_ALL_CELLS));

    return;
}

/**
 * @brief Reads PU or PD voltages from LTC, depending on stage of OWC sequence, 
 * and stores it in ltc_info.
 */
void get_cell_open_wire_check_reading(can_t* can, LTC6811_daisy_chain* ltc_daisy_chain, SPI_HandleTypeDef* hspi)
{
    uint16_t voltages[LTC_VOLT_SIZE] = { 0 };
    uint16_t cell;

    /*
     * Collect cells 0-2 (group A) from LTC6811
     */
    broadcast_read(hspi, ltc_daisy_chain, RDCVA, LTC6811_REG_SIZE, (uint8_t*)voltages);

    /*
    for (cell = 0; cell < 3; ++cell) {
        if (lv_bms_data->ltc_info.OWC_sequence == 0) {
            lv_bms_data->ltc_info.cell_pu[cell] = voltages[cell];
        } else if (lv_bms_data->ltc_info.OWC_sequence == 1) {
            lv_bms_data->ltc_info.cell_pd[cell] = voltages[cell];
        }
    }
    */

    for (cell = 0; cell < 3; ++cell) {
            if (can->lv_bms.lv_bms_ltc_info_2.owc_sequence == 0) {
                can->lv_bms.lv_bms_ltc_info_1.cell_pu_value[cell] = voltages[cell];
            } else if (can->lv_bms.lv_bms_ltc_info_2.owc_sequence == 1) {
            	can->lv_bms.lv_bms_ltc_info_1.cell_pd_value[cell] = voltages[cell];
            }
        }
    /*
     * Collect cells 3-5 (group B) from LTC6811
     */
    broadcast_read(hspi, ltc_daisy_chain, RDCVB, LTC6811_REG_SIZE, (uint8_t*)voltages);

    /*
    for (cell = 0; cell < 3; ++cell) {
        if (lv_bms_data->ltc_info.OWC_sequence == 0) {
            lv_bms_data->ltc_info.cell_pu[3 + cell] = voltages[cell];
        } else if (lv_bms_data->ltc_info.OWC_sequence == 1) {
            lv_bms_data->ltc_info.cell_pd[3 + cell] = voltages[cell];
        }
    }
    */



    for (cell = 0; cell < 3; ++cell) {
                if (can->lv_bms.lv_bms_ltc_info_2.owc_sequence == 0) {
                    can->lv_bms.lv_bms_ltc_info_1.cell_pu_value[cell + 3] = voltages[cell];
                } else if (can->lv_bms.lv_bms_ltc_info_2.owc_sequence == 1) {
                	can->lv_bms.lv_bms_ltc_info_1.cell_pd_value[cell + 3] = voltages[cell];
                }
            }

    if (CELL_SERIES_TOTAL <= 6){
    	return;
    }
    /*
     * Collect cells 6-8 (group C) from LTC6811
     */
    broadcast_read(hspi, ltc_daisy_chain, RDCVC, LTC6811_REG_SIZE, (uint8_t*)voltages);

    /*
    for (cell = 0; cell < 3; ++cell) {
        if (lv_bms_data->ltc_info.OWC_sequence == 0) {
            lv_bms_data->ltc_info.cell_pu[6 + cell] = voltages[cell];
        } else if (lv_bms_data->ltc_info.OWC_sequence == 1) {
            lv_bms_data->ltc_info.cell_pd[6 + cell] = voltages[cell];
        }
    }
    */

    for (cell = 0; cell < 3; ++cell) {
                if (can->lv_bms.lv_bms_ltc_info_2.owc_sequence == 0) {
                    can->lv_bms.lv_bms_ltc_info_1.cell_pu_value[cell + 6] = voltages[cell];
                } else if (can->lv_bms.lv_bms_ltc_info_2.owc_sequence == 1) {
                	can->lv_bms.lv_bms_ltc_info_1.cell_pd_value[cell + 6] = voltages[cell];
                }
            }
    /*
     * Collect cells 9-11 (group D) from LTC6811
     */
    broadcast_read(hspi, ltc_daisy_chain, RDCVD, LTC6811_REG_SIZE, (uint8_t*)voltages);
    /*
    for (cell = 0; cell < 3; ++cell) {
        if (lv_bms_data->ltc_info.OWC_sequence == 0) {
            lv_bms_data->ltc_info.cell_pu[9 + cell] = voltages[cell];
        } else if (lv_bms_data->ltc_info.OWC_sequence == 1) {
            lv_bms_data->ltc_info.cell_pd[9 + cell] = voltages[cell];
        }
    }
    */

    for (cell = 0; cell < 3; ++cell) {
                if (can->lv_bms.lv_bms_ltc_info_2.owc_sequence == 0) {
                    can->lv_bms.lv_bms_ltc_info_1.cell_pu_value[cell + 9] = voltages[cell];
                } else if (can->lv_bms.lv_bms_ltc_info_2.owc_sequence == 1) {
                	can->lv_bms.lv_bms_ltc_info_1.cell_pd_value[cell + 9] = voltages[cell];
                }
            }
    return;
}

/**
 * @brief Computes OWC_cell_status, based on the procedure 
 * described in the LTC6811 datasheet.
 */
void open_wire_check_calculation(can_t* can)
{

    uint8_t cell = 0;
    /*
    for (cell = 0; cell < CELL_SERIES_TOTAL; ++cell) {

        lv_bms_data->ltc_info.OWC_diff[cell] = ((lv_bms_data->ltc_info.cell_pu[cell] / 10)
        										- (lv_bms_data->ltc_info.cell_pd[cell] / 10));

        if (lv_bms_data->ltc_info.OWC_diff[cell] < (-1 * OWC_ERROR_DIFF)) {

            lv_bms_data->ltc_info.OWC_cell_status |= 1 << (cell + 1);
        } else {

            lv_bms_data->ltc_info.OWC_cell_status &= ~(1 << (cell + 1));
        }
    }
    */

    for (cell = 0; cell < CELL_SERIES_TOTAL; ++cell) {

            can->lv_bms.lv_bms_ltc_info_1.owc_diff_value[cell] = ((can->lv_bms.lv_bms_ltc_info_1.cell_pu_value[cell] / 10)
            													 - (can->lv_bms.lv_bms_ltc_info_1.cell_pd_value[cell] / 10));

            if (can->lv_bms.lv_bms_ltc_info_1.owc_diff_value[cell] < (-1 * OWC_ERROR_DIFF)) {

            	can->lv_bms.lv_bms_ltc_info_2.owc_cell_status |= 1 << (cell + 1);
            } else {

            	can->lv_bms.lv_bms_ltc_info_2.owc_cell_status &= ~(1 << (cell + 1));
            }
        }

    /* Check cell pole 0 C(0) */

    if (can->lv_bms.lv_bms_ltc_info_1.cell_pu_value[0] == 0) {

    	can->lv_bms.lv_bms_ltc_info_2.owc_cell_status |= 1;

    } else {
    	can->lv_bms.lv_bms_ltc_info_2.owc_cell_status &= 0xFFFE;
    }

    /* Check cell pole 11 C(11) */

    if (can->lv_bms.lv_bms_ltc_info_1.cell_pd_value[11] == 0) {

    	can->lv_bms.lv_bms_ltc_info_2.owc_cell_status |= 1 << 14;

    } else {
    	can->lv_bms.lv_bms_ltc_info_2.owc_cell_status &= 0x3FFF;
    }

    return;
}

void get_temperatures(can_t* can, LTC6811_daisy_chain* ltc_daisy_chain, SPI_HandleTypeDef* hspi)
{
	uint8_t i = 0;
    uint16_t register_contents[3];
    uint16_t temp_voltage = 0;
    bool NTC_fault;

    /** Read temperature from GPIO 1, 2, 3 */
    broadcast_read(hspi, ltc_daisy_chain, RDAUXA, LTC6811_REG_SIZE,
        (uint8_t*)register_contents);

    for (i = 0; i < 3; i++) {

        temp_voltage = register_contents[i]; //GPIO1/2/3
        NTC_fault = NTC_is_faulty(temp_voltage);
        //Confirm correct index.
        write_to_temp_fault_mask(&can->lv_bms.lv_bms_ltc_info_2.temp_fault_mask, NTC_fault, i);

        if (!NTC_fault) {
            can->lv_bms.lv_bms_cell_info_temps.temp_value[i] = convert_ntc_temp(temp_voltage);
        }
    }

    /** Read temperature from GPIO 4, 5 */
    broadcast_read(hspi, ltc_daisy_chain, RDAUXB, LTC6811_REG_SIZE,
        (uint8_t*)register_contents);

    for (i = 0; i < 2; i++) {

        temp_voltage = register_contents[i]; //GPIO4/5
        NTC_fault = NTC_is_faulty(temp_voltage);
        //Confirm correct index.
        write_to_temp_fault_mask(&can->lv_bms.lv_bms_ltc_info_2.temp_fault_mask, NTC_fault, (i + 3));


        if (!NTC_fault) {
        	can->lv_bms.lv_bms_cell_info_temps.temp_value[i + 3] = convert_ntc_temp(temp_voltage);
        }
    }

    return;
}

void write_to_temp_fault_mask(uint8_t *temp_fault_mask, bool value, uint8_t index)
{
    *temp_fault_mask &= (~(1 << index));
    *temp_fault_mask |= (value << index);

    return;
}

#ifndef FAKE
void get_adc_temperatures(LV_Battery* lv_bms_data, ADC_HandleTypeDef* hadc1, ADC_HandleTypeDef* hadc2, ADC_HandleTypeDef* hadc3)
{
    //NOTE!: This function interrupts the processor for a really long time. Direct memory access will be implemented later.

    return;
}
#endif

void calculate_summary_values(can_t* can)
{

    uint16_t lowest_V = UINT16_MAX;
    uint16_t highest_V = 0;
    uint32_t total_V = 0;
    uint16_t lowest_V_cell = 0;
    uint16_t highest_V_cell = 0;

    int16_t lowest_T = INT16_MAX;
    int16_t highest_T = INT16_MIN;
    int32_t total_T = 0;
    uint16_t lowest_T_cell = 0;
    uint16_t highest_T_cell = 0;

    uint16_t voltage = 0;
    int16_t temperature = 0;

    uint8_t valid_NTC = 0;
    uint8_t i = 0;

    //stack = &battery->stacks[stack_i];

    /* calculate stack values */

    /* voltage */
    for (i = 0; i < LTC_VOLT_SIZE; ++i) {

        //voltage = _lv_bms_data->ltc_info.cell_volt[i];
        voltage = can->lv_bms.lv_bms_cell_info_voltage.volt_value[i];

        total_V += voltage;
        if (voltage < lowest_V) {
            lowest_V = voltage;
            lowest_V_cell = i;
        }
        if (voltage > highest_V) {
            highest_V = voltage;
            highest_V_cell = i;
        }
    }

    /* temperature */

    for (i = 0; i < LTC_TEMP_SIZE; ++i) {

        //Check NTC first.
    	/*
        if (NTC_is_valid((int)_lv_bms_data->ltc_info.temp_fault_mask, i)) {

            temperature = _lv_bms_data->ltc_info.cell_temp[i];

            total_T += temperature;
            if (temperature < lowest_T) {
                lowest_T = temperature;
                lowest_T_cell = i;
            }
            if (temperature > highest_T) {
                highest_T = temperature;
                highest_T_cell = i;
            }
        }
        */

        if (NTC_is_valid(can->lv_bms.lv_bms_ltc_info_2.temp_fault_mask, i)) {

                    temperature = can->lv_bms.lv_bms_cell_info_temps.temp_value[i];

                    total_T += temperature;
                    if (temperature < lowest_T) {
                        lowest_T = temperature;
                        lowest_T_cell = i;
                    }
                    if (temperature > highest_T) {
                        highest_T = temperature;
                        highest_T_cell = i;
                    }
                }
    }

    //valid_NTC = count_valid_NTC((int)_lv_bms_data->ltc_info.temp_fault_mask); /* Compute number of valid ntc's */
    valid_NTC = count_valid_NTC(can->lv_bms.lv_bms_ltc_info_2.temp_fault_mask); /* Compute number of valid ntc's */


    //_lv_bms_data->ltc_info.n_valid_cell_NTC = valid_NTC;
    can->lv_bms.lv_bms_ltc_info_2.num_valid_cell_ntc = valid_NTC;

    /*
    if (valid_NTC != 0) {

        _lv_bms_data->temp_summary.mean_temp = total_T / valid_NTC; // Só para debug
    } else {
        _lv_bms_data->temp_summary.mean_temp = 0;
    }

    */
    /*
    _lv_bms_data->volt_summary.batt_volt = total_V;
    _lv_bms_data->volt_summary.max_volt = highest_V;
    _lv_bms_data->volt_summary.max_volt_cell = highest_V_cell;
    _lv_bms_data->volt_summary.min_volt = lowest_V;
    _lv_bms_data->volt_summary.min_volt_cell = lowest_V_cell;
    _lv_bms_data->volt_summary.max_min_volt_delta = (highest_V - lowest_V);
    _lv_bms_data->volt_summary.mean_volt = total_V / CELL_SERIES_TOTAL;

    _lv_bms_data->temp_summary.max_temp = highest_T;
    _lv_bms_data->temp_summary.max_temp_cell = highest_T_cell;
    _lv_bms_data->temp_summary.min_temp = lowest_T;
    _lv_bms_data->temp_summary.min_temp_cell = lowest_T_cell;
    _lv_bms_data->temp_summary.mean_temp = total_T / valid_NTC;
    */
    can->lv_bms.lv_bms_summary_volt_1.batt_volt = total_V;
    can->lv_bms.lv_bms_summary_volt_1.max_volt = highest_V;
    can->lv_bms.lv_bms_summary_volt_1.max_volt_cell = highest_V_cell;
    can->lv_bms.lv_bms_summary_volt_2.min_volt = lowest_V;
    can->lv_bms.lv_bms_summary_volt_2.min_volt_cell = lowest_V_cell;
    can->lv_bms.lv_bms_summary_volt_2.max_min_delta = (highest_V - lowest_V);
    can->lv_bms.lv_bms_summary_volt_2.mean_volt = total_V / CELL_SERIES_TOTAL;

    can->lv_bms.lv_bms_summary_temp.max_temp = highest_T;
    can->lv_bms.lv_bms_summary_temp.max_temp_cell = highest_T_cell;
    can->lv_bms.lv_bms_summary_temp.min_temp = lowest_T;
    can->lv_bms.lv_bms_summary_temp.min_temp_cell = lowest_T_cell;
    can->lv_bms.lv_bms_summary_temp.mean_temp= total_T / valid_NTC;



}
