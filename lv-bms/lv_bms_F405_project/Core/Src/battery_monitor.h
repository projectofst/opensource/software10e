
#ifndef battery_monitor_h
#define battery_monitor_h

#include <stdio.h>
#include <stdlib.h>
#include "data_structures.h"
#include "LTC6811.h"
#include "PEC.h"
#include "dwt_stm32_delay.h"
#include "data_aloc.h"
#include "unit_init.h"
#include "temperature.h"
#include "adc.h"
#include "lv_bms_can.h"
#include "communication.h"

void battery_monitor_routine(LTC6811_daisy_chain*, SPI_HandleTypeDef*, ADC_HandleTypeDef*, ADC_HandleTypeDef*, ADC_HandleTypeDef*);
void get_voltages(can_t*, LTC6811_daisy_chain*, SPI_HandleTypeDef*);
void open_wire_check(can_t*, LTC6811_daisy_chain*, SPI_HandleTypeDef*);
void start_cell_open_wire_check_conversion(LTC6811_daisy_chain*, SPI_HandleTypeDef*, uint16_t);
void get_cell_open_wire_check_reading(can_t*, LTC6811_daisy_chain*, SPI_HandleTypeDef*);
void open_wire_check_calculation(can_t*);
void get_temperatures(can_t*, LTC6811_daisy_chain*, SPI_HandleTypeDef*);
void write_to_temp_fault_mask(uint8_t* , bool , uint8_t);
void get_adc_temperatures(LV_Battery* lv_bms_data, ADC_HandleTypeDef*, ADC_HandleTypeDef*, ADC_HandleTypeDef*);
void calculate_summary_values(can_t*);

#endif /* battery_monitor_h */
 
