//
//  unit_init.h
//  LV_BMS_struct_test
//
//  Created by João Ruas on 08/07/2020.
//  Copyright © 2020 João Ruas. All rights reserved.
//

#ifndef unit_init_h
#define unit_init_h

#include <stdio.h>
#include "data_aloc.h"

void random_ltc_temp_and_volt_input(LV_Battery* , int seed);

void random_summary_values(LV_Battery* );

void ltc_temp_and_volt_print(LV_Battery* );

#endif /* unit_init_h */
