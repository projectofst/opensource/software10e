/**********************************************************************
 *   FST BMS --- slave
 *
 *   PEC
 *      - PEC calculator (see LTC datasheet)
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

#define __PEC
#include "PEC.h"
#undef __PEC
/**********************************************************************
 * Name:	PEC_calculate
 * Args:    unsigned int data
 * Return:	PEC byte
 * Desc:	Calculates PEC for data.
 **********************************************************************/
unsigned int PEC_calculate(unsigned char* data, int len)
{
    unsigned int remainder, address;
    remainder = 16; /*PEC seed*/
    int i;
    for (i = 0; i < len; i++) {
        address = ((remainder >> 7) ^ data[i]) & 0xff; /*calculate PEC table address*/
        remainder = (remainder << 8) ^ crc15Table[address];
    }
    return ((remainder * 2) & 0xffff); /*The CRC15 has a 0 in the LSB so the final value must be multiplied by 2*/
}

/**********************************************************************
 * Name:	PEC_verify
 * Args:    unsigned char data, unsigned int n, unsigned char PEC
 * Return:	exit status
 * Desc:	Verifies if PEC is correct for given data.
 **********************************************************************/
int PEC_verify(unsigned char* data, unsigned int n, unsigned int PEC)
{

    if (PEC_calculate(data, n) == PEC) {
        return 0;
    }
    return -1;
}
