/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "main.h"
#include "task.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "LTC6811.h"
#include "PEC.h"
#include "battery_monitor.h"
#include "can.h"
#include "communication.h"
#include "data_aloc.h"
#include "data_structures.h"
#include "dwt_stm32_delay.h"

#include "can_cfg.h"
#include "can_cmd.h"
#include "can_ids.h"
#include "can_log.h"
#include "lv_bms_can.h"

#include "fifo.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define FIFO_SIZE 1000
#define ADC_BUF_LEN 4095

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
CAN_HandleTypeDef hcan1;
CAN_HandleTypeDef hcan2;
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;
ADC_HandleTypeDef hadc3;

unsigned int ADCValues[5];
unsigned int ADCIndex = 0;
uint16_t dmabuff_status = 0;

uint16_t adc_buf[ADC_BUF_LEN];

CAN_TxHeaderTypeDef pHeader;

CAN_RxHeaderTypeDef RxHeader;
uint8_t TxData[8] = { 0 };
uint8_t RxData[8] = { 0 };

uint32_t TxMailbox;
uint32_t TxMailbox_2;

LTC6811 ltc;
LTC6811_daisy_chain daisy_chain_B;

CAN_FilterTypeDef sFilterConfig;

//LV_Battery* _lv_bms_data = NULL;

Fifo _can_fifo;
Fifo* can_fifo = &_can_fifo;
CANdata msg[FIFO_SIZE];

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
    .name = "defaultTask",
    .priority = (osPriority_t)osPriorityNormal,
    .stack_size = 128 * 4
};
/* Definitions for can_dispatch */
osThreadId_t can_dispatchHandle;
uint32_t can_dispatchBuffer[128];
osStaticThreadDef_t can_dispatchControlBlock;
const osThreadAttr_t can_dispatch_attributes = {
    .name = "can_dispatch",
    .stack_mem = &can_dispatchBuffer[0],
    .stack_size = sizeof(can_dispatchBuffer),
    .cb_mem = &can_dispatchControlBlock,
    .cb_size = sizeof(can_dispatchControlBlock),
    .priority = (osPriority_t)osPriorityNormal,
};
/* Definitions for monitor_routine */
osThreadId_t monitor_routineHandle;
uint32_t monitor_routineBuffer[128];
osStaticThreadDef_t monitor_routineControlBlock;
const osThreadAttr_t monitor_routine_attributes = {
    .name = "monitor_routine",
    .stack_mem = &monitor_routineBuffer[0],
    .stack_size = sizeof(monitor_routineBuffer),
    .cb_mem = &monitor_routineControlBlock,
    .cb_size = sizeof(monitor_routineControlBlock),
    .priority = (osPriority_t)osPriorityNormal,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void* argument);
void start_can_dispatch(void* argument);
void start_monitor_routine(void* argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void)
{
    /* USER CODE BEGIN Init */
    DWT_Delay_Init();
    LTC6811_init(&ltc);
    daisy_chain_B_init(&daisy_chain_B);

    HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc_buf, ADC_BUF_LEN);
    fifo_init(can_fifo, msg, FIFO_SIZE);

    /* USER CODE END Init */

    /* USER CODE BEGIN RTOS_MUTEX */
    /* add mutexes, ... */
    /* USER CODE END RTOS_MUTEX */

    /* USER CODE BEGIN RTOS_SEMAPHORES */
    /* add semaphores, ... */
    /* USER CODE END RTOS_SEMAPHORES */

    /* USER CODE BEGIN RTOS_TIMERS */
    /* start timers, add new ones, ... */
    /* USER CODE END RTOS_TIMERS */

    /* USER CODE BEGIN RTOS_QUEUES */
    /* add queues, ... */
    /* USER CODE END RTOS_QUEUES */

    /* Create the thread(s) */
    /* creation of defaultTask */
    defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

    /* creation of can_dispatch */
    can_dispatchHandle = osThreadNew(start_can_dispatch, NULL, &can_dispatch_attributes);

    /* creation of monitor_routine */
    monitor_routineHandle = osThreadNew(start_monitor_routine, NULL, &monitor_routine_attributes);

    /* USER CODE BEGIN RTOS_THREADS */
    /* add threads, ... */
    /* USER CODE END RTOS_THREADS */
}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void* argument)
{
    /* USER CODE BEGIN StartDefaultTask */
    /* Infinite loop */
    for (;;) {
        osDelay(1);
    }
    /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_start_can_dispatch */
/**
* @brief Function implementing the can_dispatch thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_start_can_dispatch */
void start_can_dispatch(void* argument)
{
    /* USER CODE BEGIN start_can_dispatch */
    /* Infinite loop */
    for (;;) {
        update_can();
        can_dispatch(&hcan2, pHeader, can_fifo, msg);
        osDelay(100);
    }
    /* USER CODE END start_can_dispatch */
}

/* USER CODE BEGIN Header_start_monitor_routine */
/**
* @brief Function implementing the monitor_routine thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_start_monitor_routine */
void start_monitor_routine(void* argument)
{
    /* USER CODE BEGIN start_monitor_routine */
    /* Infinite loop */
    for (;;) {

        battery_monitor_routine(&daisy_chain_B, &hspi1, &hadc1, &hadc2, &hadc3);

        osDelay(100);
    }
    /* USER CODE END start_monitor_routine */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc)
{
    dmabuff_status = 1;
    LV_Battery* lv_bms_data = get_battery();
    lv_bms_data->ntc_info.temp[0] = adc_buf[0];
    lv_bms_data->ntc_info.temp[1] = adc_buf[1];
    lv_bms_data->ntc_info.temp[2] = adc_buf[2];
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{

    dmabuff_status = 2;
}

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
