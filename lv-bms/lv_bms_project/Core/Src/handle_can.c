
#include "FreeRTOS.h"
#include "can.h"
#include "communication.h"
#include "lv_bms_can.h"
#include "task.h"

HAL_StatusTypeDef can_handle(void)
{

    can_t* can = get_can();

    lv_bms_send_msgs(can->lv_bms, xTaskGetTickCount());

    return HAL_OK;
}
