#include <stdint.h>
#include <stdio.h>

#include "can_cmd.h"
#include "can_ids.h"
#include "can_log.h"

#include "fifo.h"

extern Fifo* can_fifo;

uint16_t dev_get_id()
{
    return DEVICE_ID_LV_BMS;
}

void dev_send_msg(CANdata msg)
{

    append_fifo(can_fifo, msg);

    return;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{

    multiple_return_t arg = { 0 };
    /*
    printf("\ncmd_handle\n");
    if (id == CMD_LV_BMS_LV_TEST_CMD) {
        printf("\ncmd received\n");
    }
    */
    return arg;
}
