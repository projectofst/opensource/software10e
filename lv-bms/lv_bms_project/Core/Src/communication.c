#include "communication.h"
#include "cmsis_os.h"
#include <stdbool.h>
#include <stdint.h>

can_t global_can;

can_t* get_can()
{
    return &global_can;
}


void update_can()
{
/*
    can_t* can = get_can();
    LV_Battery* _lv_bms_data = get_battery();

    //Update can struct with data (ex: voltage from cell 0).
    int i = 0;

    //for (i=0;i<5;i++){

    can->lv_bms.lv_bms_cell_info_temps.temp_value[0] = _lv_bms_data->ntc_info.temp[0];
    can->lv_bms.lv_bms_cell_info_temps.temp_value[1] = _lv_bms_data->ntc_info.temp[1];
    can->lv_bms.lv_bms_cell_info_temps.temp_value[2] = _lv_bms_data->ntc_info.temp[2];

    //}

    for (i = 0; i < LTC_VOLT_SIZE; i++) {
        can->lv_bms.lv_bms_cell_info_voltage.volt_value[i] = _lv_bms_data->ltc_info.cell_volt[i];
    }

    can->lv_bms.lv_bms_status.lv_bms_charging = 1;

    can->lv_bms.lv_bms_status.lv_bms_verbose = 0;
    can->lv_bms.lv_bms_status.lv_bms_balance_ok = 1;
    //can->lv_bms.lv_bms_bat_volt.lv_bms_output_volt = _lv_bms_data->ltc_info.cell_volt[1];

    can->lv_bms.lv_bms_soc.lv_bms_soc_pct = 50;

    //can->lv_bms.lv_bms_min_volt;
    //can->lv_bms.lv_bms_delta_volt;
*/
    return;

}


#ifndef FAKE

void can_dispatch(CAN_HandleTypeDef* hcan, CAN_TxHeaderTypeDef pHeader,
    Fifo* can_fifo, CANdata* msg)
{

    static uint8_t data[8] = { 0 };
    static uint32_t TxMailbox = 0;
    can_t* can = get_can();

    //If needed fill fifo using FCP with scheduled messages
    lv_bms_send_msgs(can->lv_bms, xTaskGetTickCount());

    //If there are messsages in the fifo and device is able to send them
    while (!(fifo_empty(can_fifo)) && (HAL_CAN_GetTxMailboxesFreeLevel(hcan) != 0)) {

        //get one fcp can msg from fifo
        msg = pop_fifo(can_fifo);
        if (msg == NULL) {
            /* Should never happen */
            break;
        }

        //Convert to generic CAN msg type.
        pHeader.StdId = (msg->msg_id << 5) | (msg->dev_id & 0x1F);
        pHeader.DLC = msg->dlc;
        memcpy(data, msg->data, 8);

        //send msg
        if (HAL_CAN_AddTxMessage(hcan, &pHeader, data, &TxMailbox) != HAL_OK) {
            //Can tx failed, msg was dropped !
            //return to prevent further losses.
            return;
        }
    }

    return;
}

#else

void can_dispatch(void)
{
    return;
}

#endif
/*void can_receive_handler(Battery* battery)
{
    CANdata msg;
    can_t* can = get_can();

}
*/
