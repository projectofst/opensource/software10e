#ifndef check_limits_h
#define check_limits_h

#include <stdio.h>
#include "data_aloc.h"
#include "data_structures.h"

typedef struct{

    int16_t error_limits[ERROR_SIZE];
    int16_t error_time_threshold[ERROR_SIZE];
    int16_t error_detection_time[ERROR_SIZE];
    int16_t error_trigger_time[ERROR_SIZE];

} ERROR_STRUCT;

bool check_error(bool(*cmp)(int16_t value, int16_t limit), int16_t, int16_t);

void check_limits(LV_Battery* );


#endif /* check_limits_h */
