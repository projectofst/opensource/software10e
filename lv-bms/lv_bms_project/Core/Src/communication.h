#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#define CMD_ID_COMMON_TS 18

#include "FreeRTOS.h"
#include "data_structures.h"
#include "data_aloc.h"
#include "can.h"
#include "can_cfg.h"
#include "can_cmd.h"
#include "can_ids.h"
#include "can_log.h"
#include "lv_bms_can.h"
#include "fifo.h"
#include <stdbool.h>
#include <string.h>


can_t* get_can();

void update_can(void);

void can_dispatch(CAN_HandleTypeDef* hcan, CAN_TxHeaderTypeDef pHeader,
                  Fifo* can_fifo, CANdata* msg);

//void can_receive_handler(Battery* battery);


#endif
