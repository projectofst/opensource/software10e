//
//  data_aloc.c
//  LV_BMS_struct_test
//
//  Created by João Ruas on 07/07/2020.
//  Copyright © 2020 João Ruas. All rights reserved.
//

#include "data_aloc.h"

LV_Battery* get_battery()
{
    static LV_Battery global_battery;
    return &global_battery;
}

/*
LV_Battery* init_lv_battery () {
    // Try to allocate vector structure.
    LV_Battery* lv_battery = malloc (sizeof (LV_Battery));
    if (lv_battery == NULL)
        return NULL;

    //lv_battery->ltc_info = init_ltc_info();
    lv_battery->ntc_info = *init_ntc_info();
    lv_battery->volt_summary = *init_volt_summary();
    lv_battery->temp_summary = init_temp_summary();
    lv_battery->lv_cell_info[0] = init_lv_cell_info();
    lv_battery->current_sensor = UINT16_MAX;
    lv_battery->state_of_charge= UINT16_MAX;

    //lv_battery->settables = ();
    //Settables settables;
    lv_battery->lv_bms_status = init_lv_bms_status();
    lv_battery->lv_bms_error_reason = init_lv_bms_error_reason();

    return lv_battery;
}

*/

/*
struct ltc_info_struct* init_ltc_info()
{
    // Try to allocate vector structure.
    struct ltc_info_struct* ltc_info = malloc(sizeof(LTC_info));
    if (ltc_info == NULL)
        return NULL;
    int i = 0;
    for (i = 0; i < LTC_TEMP_SIZE; i++) {
        (ltc_info->cell_temp)[i] = UINT16_MAX; // set element at location i to i + 100
    }
    for (i = 0; i < LTC_VOLT_SIZE; i++) {
        (ltc_info->cell_volt)[i] = UINT16_MAX; // set element at location i to i + 100
    }
    for (i = 0; i < LTC_TOTAL_SIZE; i++) {
        (ltc_info->sensor_ok)[i] = 0; // set element at location i to i + 100
    }
    ltc_info->OWC_sequence = 0;

    return ltc_info;
}
*/
/*
struct ntc_info_struct* init_ntc_info()
{
    int i = 0;

    struct ntc_info_struct* ntc_info = malloc(sizeof(struct ntc_info_struct));
    if (ntc_info == NULL)
        return NULL;

    for (i = 0; i < NTC_TEMP_SIZE; i++) {
        ntc_info->temp[i] = 0;
    }

    return ntc_info;
}

struct volt_summary_struct* init_volt_summary()
{

    struct volt_summary_struct* volt_summary = malloc(sizeof(struct volt_summary_struct));
    if (volt_summary == NULL)
        return NULL;

    //total battery output voltage
    volt_summary->batt_volt = UINT16_MAX;
    volt_summary->max_volt = UINT16_MAX;
    volt_summary->mean_volt = UINT16_MAX;
    volt_summary->min_volt = UINT16_MAX;
    volt_summary->max_volt_cell = UINT8_MAX;
    volt_summary->min_volt_cell = UINT8_MAX;
    volt_summary->max_min_volt_delta = 0; //Não se pode fazer isto aqui. (lv_battery->volt_summary.max_volt - lv_battery->volt_summary.min_volt);

    return volt_summary;
}

struct set_struct* init_set()
{

    struct set_struct* set = malloc(sizeof(struct set_struct));
    if (set == NULL)
        return NULL;

    //total battery output voltage
    set->volt_max_limit = NULL;
    set->volt_min_limit = NULL;
    set->volt_bat_limit = NULL;
    set->temp_max_limit = NULL;
    set->temp_min_limit = NULL;
    set->charging_delta_limit = NULL;
    set->aparent_cell_nominal_capacity = NULL;

    return set;
}

struct lv_cell_info_struct* init_lv_cell_info()
{

    struct lv_cell_info_struct* lv_cell_info = malloc(sizeof(struct lv_cell_info_struct));
    if (lv_cell_info == NULL)
        return NULL;

    int i = 0;
    //We have to determine the correct correspondence between cells and sensores
    for (i = 0; i < CELL_SERIES_TOTAL; i++) {

        lv_cell_info[i].id = i;

        lv_cell_info[i].voltage = NULL; //&(lv_battery->ltc_info.cell_volt[i]);
        //We have 5 temp sensors. For now, we just assign all cell temperatures to sensor #0.
        lv_cell_info[i].temperature = NULL; // &(lv_battery->ltc_info.cell_temp[0]);
    }

    return lv_cell_info;
}

struct lv_bms_error_reason_struct* init_lv_bms_error_reason()
{

    struct lv_bms_error_reason_struct* lv_bms_error_reason = malloc(sizeof(struct lv_bms_error_reason_struct));
    if (lv_bms_error_reason == NULL)
        return NULL;

    //lv_bms_error_reason inits

    lv_bms_error_reason->reason_overvoltage = 0;
    lv_bms_error_reason->reason_overcurrent_in = 0;
    lv_bms_error_reason->reason_overcurrent_Out = 0;
    lv_bms_error_reason->reason_overtemperature = 0;
    lv_bms_error_reason->reason_undertemperatue = 0;
    lv_bms_error_reason->reason_min_ltc = 0;
    lv_bms_error_reason->reason_fake_error = 0;

    return lv_bms_error_reason;
}

struct temp_summary_struct* init_temp_summary()
{

    struct temp_summary_struct* temp_summary = malloc(sizeof(struct temp_summary_struct));
    if (temp_summary == NULL)
        return NULL;
    //Temp_summary inits
    temp_summary->max_temp = UINT16_MAX;
    temp_summary->mean_temp = UINT16_MAX;
    temp_summary->min_temp = UINT16_MAX;
    temp_summary->max_temp_cell = UINT8_MAX;
    temp_summary->min_temp_cell = UINT8_MAX;

    return temp_summary;
}

LV_BMS_status* init_lv_bms_status()
{

    struct lv_bms_status_struct* lv_bms_status = malloc(sizeof(struct lv_bms_status_struct));
    if (lv_bms_status == NULL)
        return NULL;

    //lv_bms_status
    lv_bms_status->bms_ok = 0;
    lv_bms_status->verbose = 0;
    lv_bms_status->charging = 0;
    lv_bms_status->balancing_ok = 0;
    return lv_bms_status;
}
*/
