#include <stdint.h>

uint8_t popcount(uint8_t x)
{
    uint8_t c = 0;
    for (; x > 0; x &= x - 1)
        c++;
    return c;
}
