import click
import os

from pathlib import Path


@click.group(invoke_without_command=True)
@click.argument("path", type=Path)
def main(path):
    files = path.rglob("*.c")

    functions = ["MX_CAN1_Init"]
    for file in files:
        with open(file) as f:
            lines = f.readlines()
            for i, line in enumerate(lines):
                for function in functions:
                    if function in line and not ";" in line:
                        print(line)
                        lines[i] = str("__attribute__ ((weak)) " + line)

        with open(file, "w") as f:
            f.write("".join(lines))


if __name__ == "__main__":
    main()
