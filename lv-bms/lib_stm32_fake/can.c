#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>

#include "shared/fifo/fifo.h"
#include "can.h"

#include "can-ids/can_cmd.h"
#include "can-ids/can_ids.h"
#include "can-ids/can_log.h"


#define PORT1    57005
#define PORT2    48879
#define MAXLINE 1024

#define CAN1_SIZE 64
#define CAN2_SIZE 64

int sock_can1;
int sock_can2;

struct sockaddr_in servaddr_1;
struct sockaddr_in servaddr_2;

Fifo _fifo1;
Fifo *fifo1 = &_fifo1;
Fifo _fifo2;
Fifo *fifo2 = &_fifo2;

CANdata msgs1[CAN1_SIZE];
CANdata msgs2[CAN2_SIZE];

bool __attribute__((weak))  filter_can1 (unsigned int sid){
	return 1;
}

bool __attribute__((weak)) filter_can2 (unsigned int sid){
	return 1;
}

void *recv_can1(void *ptr) {
	char buffer[MAXLINE] = {0};
	unsigned len = MAXLINE;
	//printf("recv1: %d\n", len);

	while (1) {
		unsigned n = recvfrom(sock_can1, (char *)buffer, MAXLINE,
					MSG_WAITALL, (struct sockaddr *) &servaddr_1,
					&len);


    if (n == 0 || n == -1){
			continue;
		}

		//printf("recv1: %s\n", buffer);

		CANdata msg = {0};
		json_value *jv = json_parse(buffer, n);
		json_object_entry *message = jv->u.object.values;
	
		msg.msg_id = message[0].value->u.integer >> 5;
		msg.dev_id = message[0].value->u.integer & 0x1F;
		msg.dlc = message[1].value->u.integer;
		

		//printf("recvcan1\n");
		for (int i=0; i<4; i++) {
			msg.data[i] = message[2].value->u.array.values[i]->u.integer;
		}

		if(filter_can1(message[0].value->u.integer))
			append_fifo(fifo1, msg);
	}
}

void *recv_can2(void *ptr) {

	char buffer[MAXLINE] = {0};
	unsigned len = MAXLINE;


	while(1) {
		unsigned n = recvfrom(sock_can2, (char *)buffer, MAXLINE,
					MSG_WAITALL, (struct sockaddr *) &servaddr_2,
					&len);

		if (n == 0 || n == -1){
				continue;
		}

		CANdata msg = {0};

		json_value *jv = json_parse(buffer, n);
		json_object_entry *message = jv->u.object.values;
		msg.msg_id = message[0].value->u.integer >> 5;
		msg.dev_id = message[0].value->u.integer & 0x1F;
		msg.dlc = message[1].value->u.integer;

		for (int i=0; i<4; i++) {
			msg.data[i] = message[2].value->u.array.values[i]->u.integer;
		}
		if(filter_can2(message[0].value->u.integer % 2048))
			append_fifo(fifo2, msg);
	}
}

void MX_CAN1_Init(void){
	char buffer[MAXLINE];
	char *hello = "{\"sid\": 0, \"dlc\": 8, \"data\": [1,2,3,4], \"timestamp\": 0}";

	// Creating socket file descriptor
    if ( (sock_can1 = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&servaddr_1, 0, sizeof(servaddr_1));

    // Filling server information
    servaddr_1.sin_family = AF_INET;
    servaddr_1.sin_port = htons(PORT1);
    servaddr_1.sin_addr.s_addr = INADDR_ANY;

    int n, len;

    sendto(sock_can1, (const char *)hello, strlen(hello),
        0, (const struct sockaddr *) &servaddr_1,
            sizeof(servaddr_1));

	fifo_init(fifo1, msgs1, CAN1_SIZE);
	printf("\ncan1_init\n");
	//return;
	pthread_t thread1;
	pthread_create(&thread1, NULL, recv_can1, NULL);

	return;
}

void MX_CAN2_Init(void) {
	char buffer[MAXLINE];
	char *hello = "{\"sid\": 0, \"dlc\": 8, \"data\": [1,2,3,4], \"timestamp\": 0}";

	// Creating socket file descriptor
    if ( (sock_can2 = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&servaddr_2, 0, sizeof(servaddr_2));

    // Filling server information
    servaddr_2.sin_family = AF_INET;
    servaddr_2.sin_port = htons(PORT2);
    servaddr_2.sin_addr.s_addr = INADDR_ANY;

    int n, len;

    sendto(sock_can2, (const char *)hello, strlen(hello),
        0, (const struct sockaddr *) &servaddr_2,
            sizeof(servaddr_2));

	fifo_init(fifo2, msgs2, CAN1_SIZE);

	return;
	pthread_t thread1;
	pthread_create(&thread1, NULL, recv_can2, NULL);

	return;
}


/**
  * @brief  Return Rx FIFO fill level.
  * @param  hcan pointer to an CAN_HandleTypeDef structure that contains
  *         the configuration information for the specified CAN.
  * @param  RxFifo Rx FIFO.
  *         This parameter can be a value of @arg CAN_receive_FIFO_number.
  * @retval Number of messages available in Rx FIFO.
  */
uint32_t HAL_CAN_GetRxFifoFillLevel(CAN_HandleTypeDef *hcan, uint32_t RxFifo)
{
	if (RxFifo == CAN_RX_FIFO0) {
		return fifo_occupied(fifo1);
	}
	else if (RxFifo == CAN_RX_FIFO1) {
		return fifo_occupied(fifo2);
	}

	return 0;
}


/**
  * @brief  Get an CAN frame from the Rx FIFO zone into the message RAM.
  * @param  hcan pointer to an CAN_HandleTypeDef structure that contains
  *         the configuration information for the specified CAN.
  * @param  RxFifo Fifo number of the received message to be read.
  *         This parameter can be a value of @arg CAN_receive_FIFO_number.
  * @param  pHeader pointer to a CAN_RxHeaderTypeDef structure where the header
  *         of the Rx frame will be stored.
  * @param  aData array where the payload of the Rx frame will be stored.
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_CAN_GetRxMessage(CAN_HandleTypeDef *hcan, uint32_t RxFifo, CAN_RxHeaderTypeDef *pHeader, uint8_t aData[])
{
	CANdata _msg;
	//CANdata* msg = &_msg;
	//CANdata _msg;
	CANdata* msg;

	if (RxFifo == CAN_RX_FIFO0) {
		msg = pop_fifo(fifo1);
	}
	else if (RxFifo == CAN_RX_FIFO1) {
		msg = pop_fifo(fifo2);
	}

	
	_msg = *msg;
	printf("dev_id 2: %d\n", (_msg.sid & 0x1F));
	printf("msg_id 2: %d\n", (_msg.sid >> 5));
	printf("data[0] 2: %d\n", _msg.data[0]);
	pHeader->StdId = _msg.sid;
	pHeader->DLC = _msg.dlc;
	memcpy(aData, _msg.data, 8);

	return 0;
}


/**
  * @brief  Add a message to the first free Tx mailbox and activate the
  *         corresponding transmission request.
  * @param  hcan pointer to a CAN_HandleTypeDef structure that contains
  *         the configuration information for the specified CAN.
  * @param  pHeader pointer to a CAN_TxHeaderTypeDef structure.
  * @param  aData array containing the payload of the Tx frame.
  * @param  pTxMailbox pointer to a variable where the function will return
  *         the TxMailbox used to store the Tx message.
  *         This parameter can be a value of @arg CAN_Tx_Mailboxes.
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_CAN_AddTxMessage(CAN_HandleTypeDef *hcan, CAN_TxHeaderTypeDef *pHeader, uint8_t aData[], uint32_t *pTxMailbox)
{
	CANdata msg;
	msg.dev_id = pHeader->StdId & 0x1F;
	msg.msg_id = pHeader->StdId >> 5;
	msg.dlc = pHeader->DLC;
	memcpy(msg.data, aData, 8);

	char buffer[MAXLINE] = {0};

	uint16_t sid = msg.dev_id + (msg.msg_id << 5);
	sprintf(buffer, "{\"sid\": %d, \"dlc\": %d, \"data\": [%d,%d,%d,%d],\
			\"timestamp\": 0}", sid, msg.dlc, msg.data[0], msg.data[1],
			msg.data[2], msg.data[3]);

    sendto(sock_can1, (const char *)buffer, strlen(buffer),
        0, (const struct sockaddr *) &servaddr_1,
            sizeof(servaddr_1));

    sendto(sock_can2, (const char *)buffer, strlen(buffer),
        0, (const struct sockaddr *) &servaddr_2,
            sizeof(servaddr_2));

	//printf("can1|can2 - %s\n", buffer);
}
