#include "flash.h"

#include <stdio.h>
#include <stdlib.h>

#define FILENAME "flash.txt"
#define ASCII_BYTES 6

uint16_t read_flash(uint16_t *buf,uint16_t offset, uint16_t len) {

	FILE *f;
	int i;

	if((f = fopen(FILENAME,"w+")) == NULL){
		printf("Erro opening flash");
		return 1;
	}

	fseek(f,ASCII_BYTES*offset,0);

	for(i = 0 ; i < len ; i++)
		fscanf(f,"%5hd\n", &buf[i]);

	return fclose(f);
}

uint16_t write_flash(uint16_t *buf,uint16_t offset, uint16_t len) {
	
	FILE *f;
	int i;

	if((f = fopen(FILENAME,"w+")) == NULL){
		printf("Erro opening flash");
		return 1;
	}

	fseek(f,ASCII_BYTES*offset,0);

	for(i = 0 ; i < len ; i++)
		fprintf(f,"%5d\n", buf[i]);

	
	return fclose(f);
}
