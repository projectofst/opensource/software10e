# PIC33E Project Template

## About
This repo contains the code of the 09e sniffer, also commonly used in bench tests.

The current version supports:
* CAN to USB retransmission
* USB to CAN retransmission
* USB to USB echo
* CAN reception statistics every 100ms

The message statistics can be enabled by setting the `-DSNIFFER_STATS` flag in
the makefile.

Currently CAN is configured to 1Mbit/s with `PROP_SEGMENT = 2`,
`PHASE1_SEGMENT=3`, `PHASE2_SEGMENT=2`. Triple sampling is not enabled.

These settings can only be changed in the source code.

## USB protocol

The CAN messages retransmitted to USB follow a simple yet quite bandwith taxing
protocol.

A message is transmitted as an comma separated ASCII string, ex: "1,8,100,100,100,100,6000\n\0"

The fields correspond to "sid,dlc,data0,data1,data2,data3,timestamp".  The sid
correspond to a CAN2.0a 11 bit id. dlc is the standard CAN dlc up to 8 bytes.
The datas are grouped in 4 16 bit integers. 

The timestamp has a resoluction of 100ms and is calculated from a hardware
timer in the microcontroller, good precision should not be expected.

## Submodules
This template includes the `can-ids` and `lib_pic33e` submodules inside the
`lib` directory. 

Run `make submodules` at any time to update your submodules.

Run `make add-submodules` to add the submodules to you repository.

## Makefile
The makefile includes by default only the `lib` and `src` directories. Please use 
paths relative to these directories in your `#include` directives.
(e.g., to include `lib/lib_pic33e/CAN.h`, do `#include "lib_pic33e/CAN.h"`).

Run `make` to compile your code. Run `make flash` to compile and program the 
PIC.

Run `make verbose` to show the memory layout after linking.


## `AUTHORS`
This file includes the project's authors, just for cute historical purposes
for those unwilling to `git log`.
