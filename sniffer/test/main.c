#include "munit/munit.h"

#include "message.h"

static MunitResult test_base10_to_string(const MunitParameter params[], void* data) {
	uint8_t buffer[256];
	base10_to_string(buffer, 10);
	munit_assert_string_equal(buffer, "10");

	base10_to_string(buffer, 70000);
	munit_assert_string_equal(buffer, "70000");

	return MUNIT_OK;
}

static MunitResult test_make_message(const MunitParameter params[], void* data) {
	uint8_t buffer[256];
	CANdata msg;
	msg.sid = 2047;
	msg.dlc=8;
	msg.data[0] = 65010;
	msg.data[1] = 65020;
	msg.data[2] = 65030;
	msg.data[3] = 65040;

	uint32_t timestamp = 100000UL;
	
	make_message(buffer, msg, timestamp);
	munit_assert_string_equal(buffer, "2047,8,65010,65020,65030,65040,100000\n");
	msg.dlc=6;
	make_message(buffer, msg, timestamp);
	munit_assert_string_equal(buffer, "2047,6,65010,65020,65030,0,100000\n");
	msg.dlc=4;
	make_message(buffer, msg, timestamp);
	munit_assert_string_equal(buffer, "2047,4,65010,65020,0,0,100000\n");
	msg.dlc=2;
	make_message(buffer, msg, timestamp);
	munit_assert_string_equal(buffer, "2047,2,65010,0,0,0,100000\n");
	msg.dlc=0;
	make_message(buffer, msg, timestamp);
	munit_assert_string_equal(buffer, "2047,0,0,0,0,0,100000\n");

	return MUNIT_OK;
}

static MunitTest test_suite_tests[] = {
  {
	  (char*) "/message/base10_to_string",
	  test_base10_to_string,
	  NULL,
	  NULL,
	  MUNIT_TEST_OPTION_NONE,
	  NULL
  },
  {
	  (char*) "/message/make_message",
	  test_make_message,
	  NULL,
	  NULL,
	  MUNIT_TEST_OPTION_NONE,
	  NULL
  },
  { 
	  NULL, 
	  NULL, 
	  NULL, 
	  NULL, 
	  MUNIT_TEST_OPTION_NONE, 
	  NULL 
  }
};

static const MunitSuite test_suite = {
  (char*) "",
  test_suite_tests,
  NULL,
  1,
  MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char *argv[])
{
	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
