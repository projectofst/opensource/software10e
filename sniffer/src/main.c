/*
 * This file bellongs to sniffer.
 * sniffer is a CAN to USB translator program used in the prototypes developed by FST Lisboa.
 *
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128 // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4 // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xc.h>

// NOTE: Always include timing.h
#include <lib_pic33e/timing.h>

// Comment this line if USB is not needed
#include <lib_pic33e/can.h>
#include <lib_pic33e/pps.h>
#include <lib_pic33e/timer.h>
#include <lib_pic33e/usb_lib.h>

#include "message.h"

#define SNIFFER_STATS

#define USB_RECEIVE_LENGTH 256
CANdata usb_receive_buffer[USB_RECEIVE_LENGTH];
uint16_t usb_rd = USB_RECEIVE_LENGTH - 1;
uint16_t usb_wr = 0;
#define NUM_OF_CAN_LINES 2

uint16_t g_send_stats = 0;

#define TIME_JUMP 100
uint32_t timestamp;

void timer1_callback(void)
{
#ifdef SNIFFER_STATS
    g_send_stats = 1;
#endif
    timestamp += TIME_JUMP;
    return;
}

void receive_handler_usb(char* readString)
{
    uint16_t sid;
    uint16_t dlc;
    uint16_t data[4];
    uint16_t error;

    /* Get next message buffer position */
    CANdata* msg = usb_receive_buffer + usb_wr;

    /* Try to convert the received string into the valid can fields */
    error = sscanf(readString, "%d,%d,%d,%d,%d,%d\n", &sid, &dlc, &data[0],
        &data[1], &data[2], &data[3]);

    /* Check if we were able to parse the string */
    if (error == EOF) {
        return;
    }
    /* Check if we were able to get the 6 CAN message elements */
    if (error < 6) {
        return;
    }

    /* Place the message on the buffer */
    msg->sid = sid;
    msg->dlc = dlc;
    msg->data[0] = data[0];
    msg->data[1] = data[1];
    msg->data[2] = data[2];
    msg->data[3] = data[3];

    usb_wr = (usb_wr + 1) % USB_RECEIVE_LENGTH;

    /* Wrapping around !! */
    if (usb_rd == usb_wr) {
        usb_rd = (usb_rd + 1) % USB_RECEIVE_LENGTH;
    }

    return;
}

int main()
{

    CANdata msg, stats_msg;
    uint8_t buffer[256];
    uint16_t new = 0, can_line = 0, i;
    uint16_t error;
    uint16_t need_to_receive = 0;
    uint16_t message_count = 0;

    TRISE = 0;

    LATEbits.LATE5 = 1;

    /* Assign can getter functions as needed */
    CANdata (*pop_can[NUM_OF_CAN_LINES])(void);
    pop_can[0] = &pop_can1;
    pop_can[1] = &pop_can2;

    /* Assign CAN check if buffer ready functions as needed */
    bool (*check_can[NUM_OF_CAN_LINES])(void);
    check_can[0] = &can1_rx_empty;
    check_can[1] = &can2_rx_empty;

    /* Stats timer */
    config_timer1(TIME_JUMP, 4);
    stats_msg.sid = 1;
    stats_msg.dlc = 2;

#if !defined(LINUX)
    /* Start the USB module */
    USBInit();
#endif

    int count = 0;
    int count2 = 60000;

    /* Configure the PPS for the CAN module.
     * Defaulting to the devboard pinout
     */
    PPSUnLock;
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
    PPSOutput(OUT_FN_PPS_C2TX, OUT_PIN_PPS_RP120);
    PPSInput(IN_FN_PPS_C2RX, IN_PIN_PPS_RPI121);
    PPSLock;

    /* Configure and start the CAN modules with the default config */
    config_can1(NULL);
    config_can2(NULL);

    while (1) {
        if (g_send_stats) {
            stats_msg.data[0] = message_count;
            message_count = 0;
            make_message(buffer, stats_msg, timestamp);
#if !defined(LINUX)
            append_string_usb(buffer, strlen(buffer));
#endif
            g_send_stats = 0;
        }

        /* If we received messages from USB we must send
         * them to CAN
         */
        if ((usb_rd + 1) % USB_RECEIVE_LENGTH != usb_wr) {

            usb_rd = (usb_rd + 1) % USB_RECEIVE_LENGTH;

            error = send_can1(usb_receive_buffer[usb_rd]);
            error += send_can2(usb_receive_buffer[usb_rd]);
            if (error != 0) {
                LATEbits.LATE3 ^= 1;
            }
            // Echo messages received from USB
            make_message(buffer, usb_receive_buffer[usb_rd], timestamp);
#if !defined(LINUX)
            append_string_usb(buffer, strlen((char*)buffer));
#endif
        }

        // Receive messages from CAN
        for (i = can_line; (i + 1) % NUM_OF_CAN_LINES != can_line;
             i = (i + 1) % NUM_OF_CAN_LINES) {
            if (!check_can[i]()) {
                msg = pop_can[i]();
                new ++;
                message_count++;
                break;
            }
        }
        can_line = (can_line + 1) % NUM_OF_CAN_LINES;

        if (new) {
            make_message(buffer, msg, timestamp);
#if !defined(LINUX)
            append_string_usb(buffer, strlen((char*)buffer));
#endif
            new --;
        }

#if !defined(LINUX)
        /* Exit if there aren't any messages to send */
        if (usb_full() || need_to_receive == 100) {
            USBTasks();
            need_to_receive = 0;
        }
#endif

        need_to_receive++;

        ClrWdt();
    }

    return 0;
}
