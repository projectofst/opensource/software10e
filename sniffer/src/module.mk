MODULE_C_SOURCES:=can.c
MODULE_C_SOURCES+=main.c
MODULE_C_SOURCES+=message.c
MODULE_C_SOURCES+=usb.c

PWD:=src/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
