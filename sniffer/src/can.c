#include <lib_pic33e/can.h>

#include <stdbool.h>

static CANdata msg;
bool dummy_can1_rx_empty()
{
    return 0;
}

CANdata dummy_pop_can1()
{
    msg.sid = 1;
    msg.dlc = 8;
    msg.data[0] = 1;
    msg.data[1] = 2;
    msg.data[2] = 3;
    msg.data[3] = 3;

    return msg;
}
