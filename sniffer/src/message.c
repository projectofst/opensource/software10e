/*
 * This file bellongs to sniffer.
 * sniffer is a CAN to USB translator program used in the prototypes developed by FST Lisboa.
 *
 * Student cars developed by FST Lisboa.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "message.h"

/* Thanks friend: https://stackoverflow.com/a/12386915
 * Yet, another good itoa implementation

 * returns: the length of the number string
 */
int base10_to_string(uint8_t* sp, uint32_t value)
{
    uint8_t tmp[16]; // be careful with the length of the buffer
    uint8_t* tp = tmp;
    int i;
    uint32_t v;
    int radix = 10;

    int sign = (radix == 10 && value < 0);
    if (sign)
        v = -value;
    else
        v = (uint32_t)value;

    while (v || tp == tmp) {
        i = v % radix;
        v /= radix; // v/=radix uses less CPU clocks than v=v/radix does
        if (i < 10)
            *tp++ = i + '0';
        else
            *tp++ = i + 'a' - 10;
    }

    int len = tp - tmp;

    if (sign) {
        *sp++ = '-';
        len++;
    }

    while (tp > tmp)
        *sp++ = *--tp;

    *sp++ = 0;

    return len;
}

/* CAN message to string
 * Relies on base10_to_string to convert CAN message fields into integers
 * encoded as strings.
 * The fields are separated using commas, a message is terminated with a \n.
 * Example with sid: 10, dlc: 8, data0: 1, data1: 111, data2: 11111, data4: 0
 * 10,8,1,111,11111,0\n
 *
 */
unsigned make_message(uint8_t* buffer, CANdata msg, uint32_t timestamp)
{
    int i = 0;
    int index = 0;
    unsigned size = 0;
    uint8_t aux[16];

    /*size = base10_to_string(aux, calculate_crc16(default_seed, msg, size));
     */

    // fill unused datas with 0's
    for (i = (msg.dlc + 1) / 2; i < 4; i++)
        msg.data[i] = 0;

    size = base10_to_string(aux, msg.sid);
    strcpy((char*)(buffer + index), (char*)aux);
    index += size;

    buffer[index++] = ',';

    buffer[index++] = msg.dlc + '0';

    buffer[index++] = ',';

    /* (msg.dlc+1)/2 is kinda magic but is the right operation.
     * msg.dlc/2 wouln't work since 1/2 = 0. So a message with dlc = 1 wouldn't
     * encode any datas.
     */
    for (i = 0; i < 4; i++) {
        size = base10_to_string(aux, msg.data[i]);
        strcpy((char*)(buffer + index), (char*)aux);
        index += size;
        buffer[index++] = ',';
    }

    size = base10_to_string(aux, timestamp);
    strcpy((char*)(buffer + index), (char*)aux);
    index += size;

    /* Remove the last comma and place a \n instead */
    buffer[index++] = '\n';
    buffer[index] = '\0';

    return index;
}
