/*! \file tasks.c
 *  \brief TE tasks
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

#include "lib_pic30f/adc.h"
#include "lib_pic30f/can.h"

#include "can-ids-v1/can_cfg.h"
#include "can-ids-v1/can_cmd.h"
#include "can-ids-v1/can_ids.h"

#include "shared/fifo/fifo.h"
#include "shared/pedals/pedals.h"
#include "shared/run_avg/run_avg.h"

#include "fcp.h"

extern bool adc_data_available;
extern run_avg_t run_avgs[5];
extern uint32_t param[CFG_TE_SIZE];

void get_all_adcs(void* arg)
{
    uint32_t* values = (uint32_t*)arg;

    if (adc_data_available == 0) {
        return;
    }

    adc_data_available = 0;

    values[0] = get_adcbuf0();
    values[1] = get_adcbuf1();
    values[2] = get_adcbuf2();
    values[3] = get_adcbuf3();
    values[4] = get_adcbuf4();

    return;
}

void fifo_dispatch(void* arg)
{
    Fifo* fifo = (Fifo*)arg;

    uint16_t i = 0;
    for (i = 0; !fifo_empty(fifo) && can2_tx_available() > 0; i++)
        send_can2(*pop_fifo(fifo));

    return;
}

void receive_msgs(void* arg)
{
    if (can2_rx_empty()) {
        return;
    }

    CANdata _msg;
    CANdata* msg = &_msg;

    msg = pop_can2();

    cfg_dispatch(*msg);
    cmd_dispatch(*msg);

    return;
}

double convert_pressure(double voltage_percentage)
{
    double voltage = 5.0 * (voltage_percentage / 100.0);

    if (voltage > 4.5) {
        return 160;
    } else if (voltage < 0.5) {
        return 0;
    } else {
        return 35.6 * (voltage - 0.5);
    }
}

void store_pedals(void* arg)
{
    int i = 0;
    uint32_t* values = (uint32_t*)arg;
    uint16_t pedals[5] = { 0 };

    can_t* can_global = get_can_global();

    for (i = 0; i < 5; i++) {
        run_avg_add(&run_avgs[i], values[i]);

        printf("values[%d]: %d\n", i, values[i]);

        pedals[i] = run_avg_average(&run_avgs[i]);

        printf("pedals %d %d\n", i, pedals[i]);
    }

    dev_te_t* te = &(can_global->te);

    te->te_apps.te_apps0 = pedals[0];
    te->te_apps.te_apps1 = pedals[1];
    te->te_brake.te_bps0 = pedals[2];
    te->te_brake.te_bps1 = pedals[3];
    te->te_brake.te_be = pedals[4];
    /*
    te->te_apps.te_apps0 = 100 * (pedals[0] / 4096);
    te->te_apps.te_apps1 = 100 * (pedals[1] / 4096);
    
    te->te_brake.te_bps0 = 100 * (run_avg_average(&run_avgs[2]) / 4096);
    te->te_brake.te_bps1 = 100 * (run_avg_average(&run_avgs[3]) / 4096);
    te->te_brake.te_be = 100 * (run_avg_average(&run_avgs[4]) / 4096);
    */

    return;
}
