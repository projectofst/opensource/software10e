#include <xc.h>

#include "lib_pic30f/adc.h"
#include "lib_pic30f/can.h"
#include "lib_pic30f/timer.h"

#include "can-ids-v1/can_ids.h"
#include "can-ids-v1/can_log.h"

#include "torque_encoder.h"

void init_adc(void)
{
    ADC_parameters config;

    config.idle = 1;
    config.form = 0;
    config.conversion_trigger = AUTO_CONV;
    config.sampling_type = AUTO;
    config.voltage_ref = INTERNAL;
    config.sample_pin_select_type = AUTO;
    config.smpi = 4;
    config.sampling_time = 31;
    config.conversion_time = 63;
    config.pin_select = AN3 | AN4 | AN9 | AN10 | AN13;
    config.interrupt_priority = 5;
    config.turn_on = 1;

    TRISBbits.TRISB3 = 1; //AN3 input     APPS_0
    TRISBbits.TRISB4 = 1; //AN4 input     APPS_1
    TRISBbits.TRISB9 = 1; //AN9 input     BPS_pressure_0
    TRISBbits.TRISB10 = 1; //AN10 input    BPS_pressure_1
    TRISBbits.TRISB13 = 1; //AN11 input    BPS_electric

    config_adc(config);

    return;
}

void config_all(void)
{
    /* config and log start up */
    config_can2();
    send_can2(logI(LOG_RESET_MESSAGE, 0, 0, 0));

    init_adc();

    TRISDbits.TRISD2 = 0;
    TRISBbits.TRISB12 = 0;
    TRIS_LED_THR = 0;
    SHUTDOWN_DETECTION_CONFIG = 1;

    config_timer1(1, 4);
}

uint32_t config_get_default_param(uint16_t index)
{
    uint32_t default_params[CFG_TE_SIZE] = { 0 };

    default_params[CFG_TE_VERSION] = VERSION;
    default_params[CFG_TE_HB_TRESHOLD] = 300;
    default_params[CFG_TE_APPS0_GND] = 877;
    default_params[CFG_TE_APPS0_VCC] = 3798;
    default_params[CFG_TE_APPS0_0N] = 877;
    default_params[CFG_TE_APPS0_MAXN] = 3798;
    default_params[CFG_TE_APPS1_GND] = 298;
    default_params[CFG_TE_APPS1_VCC] = 3220;
    default_params[CFG_TE_APPS1_0N] = 298;
    default_params[CFG_TE_APPS1_MAXN] = 3220;
    default_params[CFG_TE_BPS0_GND] = 409;
    default_params[CFG_TE_BPS0_VCC] = 3687;
    default_params[CFG_TE_BPS0_0N] = 409;
    default_params[CFG_TE_BPS0_MAXN] = 3687;
    default_params[CFG_TE_BPS1_GND] = 409;
    default_params[CFG_TE_BPS1_VCC] = 3687;
    default_params[CFG_TE_BPS1_0N] = 409;
    default_params[CFG_TE_BPS1_MAXN] = 3687;
    default_params[CFG_TE_BE_GND] = 407;
    default_params[CFG_TE_BE_VCC] = 3690;
    default_params[CFG_TE_BE_0N] = 407;
    default_params[CFG_TE_BE_MAXN] = 3690;

    return default_params[index];
}
