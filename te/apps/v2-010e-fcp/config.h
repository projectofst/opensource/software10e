#ifndef __TE_CONFIG_H__
#define __TE_CONFIG_H__

#include <stdint.h>

void config_all(void);
uint32_t config_get_default_param(uint16_t index);

#endif
