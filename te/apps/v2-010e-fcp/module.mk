MODULE_C_SOURCES:=config.c
MODULE_C_SOURCES+=config_functions.c
MODULE_C_SOURCES+=fcp.c
MODULE_C_SOURCES+=main.c
MODULE_C_SOURCES+=tasks.c

PWD:=apps/v2-010e-fcp/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
