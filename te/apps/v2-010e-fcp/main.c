// DSPIC30F6012A Configuration Bit Settings

// 'C' source line config statements

// FOSC
#pragma config FOSFPR = HS2_PLL8 // Oscillator (HS2 w/PLL 8x)
#pragma config FCKSMEN = CSW_FSCM_OFF // Clock Switching and Monitor (Sw Disabled, Mon Disabled)

// FWDT
#pragma config FWPSB = WDTPSB_16 // WDT Prescaler B (1:16)
#pragma config FWPSA = WDTPSA_512 // WDT Prescaler A (1:512)
#pragma config WDT = WDT_OFF // Watchdog Timer (Disabled)

// FBORPOR
#pragma config FPWRT = PWRT_OFF // POR Timer Value (Timer Disabled)
#pragma config BODENV = NONE // Brown Out Voltage (Reserved)
#pragma config BOREN = PBOR_OFF // PBOR Enable (Disabled)
#pragma config MCLRE = MCLR_EN // Master Clear Enable (Enabled)

// FBS
#pragma config BWRP = WR_PROTECT_BOOT_OFF // Boot Segment Program Memory Write Protect (Boot Segment Program Memory may be written)
#pragma config BSS = NO_BOOT_CODE // Boot Segment Program Flash Memory Code Protection (No Boot Segment)
#pragma config EBS = NO_BOOT_EEPROM // Boot Segment Data EEPROM Protection (No Boot EEPROM)
#pragma config RBS = NO_BOOT_RAM // Boot Segment Data RAM Protection (No Boot RAM)

// FSS
#pragma config SWRP = WR_PROT_SEC_OFF // Secure Segment Program Write Protect (Disabled)
#pragma config SSS = NO_SEC_CODE // Secure Segment Program Flash Memory Code Protection (No Secure Segment)
#pragma config ESS = NO_SEC_EEPROM // Secure Segment Data EEPROM Protection (No Segment Data EEPROM)
#pragma config RSS = NO_SEC_RAM // Secure Segment Data RAM Protection (No Secure RAM)

// FGS
#pragma config GWRP = GWRP_OFF // General Code Segment Write Protect (Disabled)
#pragma config GCP = GSS_OFF // General Segment Code Protection (Disabled)

// FICD
#pragma config ICS = ICS_PGD // Comm Channel Select (Use PGC/EMUC and PGD/EMUD)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <stdbool.h>
#include <stdint.h>
#include <xc.h>

#include "config_functions.h"
#include "torque_encoder.h"

#include "can-ids-v1/can_cfg.h"
#include "can-ids-v1/can_ids.h"
#include "can-ids-v1/can_log.h"

#include "lib_pic30f/lib_pic30f.h"

#include "shared/fifo/fifo.h"
#include "shared/run_avg/run_avg.h"
#include "shared/scheduler/scheduler.h"

#include "config.h"
#include "fcp.h"
#include "tasks.h"

volatile bool adc_data_available = 0;

volatile uint32_t time_ms = 0;

// CAN fifo
#define FIFO_SIZE 64
Fifo _fifo;
Fifo* fifo = &_fifo;

_prog_addressT eeprom_address;

double buffers[5][16];
run_avg_t run_avgs[5];

uint16_t _EEDATA(32) eeprom_data[CFG_TE_SIZE] = {
    APPS_0_ZERO_FORCE_THRS,
    APPS_0_MAX_FORCE_THRS,
    APPS_1_ZERO_FORCE_THRS,
    APPS_1_MAX_FORCE_THRS,
    BPS_PRESSURE_0_ZERO_FORCE_THRS,
    BPS_PRESSURE_0_MAX_FORCE_THRS,
    BPS_PRESSURE_1_ZERO_FORCE_THRS,
    BPS_PRESSURE_1_MAX_FORCE_THRS,
    BPS_ELECTRIC_ZERO_FORCE_THRS,
    BPS_ELECTRIC_MAX_FORCE_THRS,
    HARD_BRAKING_THRS,
};

uint32_t param[CFG_TE_SIZE] = { [0 ... CFG_TE_SIZE - 1] = 0xFFFFFFFF };

void adc_callback(void)
{
    // signal new adc aquisition ready for fetching
    adc_data_available = 1;
    return;
}

void timer1_callback(void)
{
    time_ms++;
    return;
}

uint32_t get_time_ms(void)
{
    // printf("time: %d", time_ms);
    return time_ms;
}

int main(void)
{

    param[CFG_TE_VERSION] = VERSION;
    // start hardware configurations
    config_all();

    can_t* can_global = get_can_global();

    // can fifo for sent messages
    CANdata msgs[FIFO_SIZE];
    fifo_init(fifo, msgs, FIFO_SIZE);

    // config configurations table
    cfg_config(param, CFG_TE_SIZE);

    // Binds the EE address to the EE data vector and loads the EE with the defined values
#ifndef FAKE
    _init_prog_address(eeprom_address, eeprom_data);
#else
    read_eeprom(eeprom_data, sizeof(eeprom_data) / sizeof(uint16_t));
#endif

    int i = 0;
    for (i = 0; i < CFG_TE_SIZE; i++) {
        if (param[i] == 0xFFFFFFFF) {
            param[i] = config_get_default_param(i);
            printf("param %d %d\n", i, param[i]);
        }
    }

    for (i = 0; i < 5; i++) {
        run_avgs[i].buffer = buffers[i];
        run_avgs[i].max_size = 16;
        run_avg_config(run_avgs + i);
    }

    // buffer of values read from the adc
    uint32_t aquisition[5] = { 0 };

    tasks_t tasks[] = {
        { .period = 10, .func = get_all_adcs, .args_on = 1, .args = (void*)aquisition },
        { .period = 0, .func = fifo_dispatch, .args_on = 1, .args = (void*)fifo },
        { .period = 0, .func = receive_msgs, .args_on = 1, .args = NULL },
        { .period = 10, .func = process_pedals, .args_on = 1, .args = (void*)aquisition },
    };

    scheduler_init_args(tasks, sizeof(tasks) / sizeof(tasks_t));

    while (1) {
        scheduler(time_ms);
        te_send_msgs(can_global->te, time_ms);
        ClrWdt();
    }

    return 0;
}
