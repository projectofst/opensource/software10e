/*! \file tasks.c
 *  \brief TE tasks
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

#include "lib_pic30f/adc.h"
#include "lib_pic30f/can.h"

#include "can-ids-v2/can_cfg.h"
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"

#include "shared/fifo/fifo.h"
#include "shared/pedals/pedals.h"
#include "shared/run_avg/run_avg.h"

#include "fcp.h"

extern bool adc_data_available;
extern run_avg_t run_avgs[5];
extern uint32_t param[CFG_TE_SIZE];

void get_all_adcs(void* arg)
{
    uint32_t* values = (uint32_t*)arg;

    if (adc_data_available == 0) {
        return;
    }

    adc_data_available = 0;

    values[0] = get_adcbuf0();
    values[1] = get_adcbuf1();
    values[2] = get_adcbuf2();
    values[3] = get_adcbuf3();
    values[4] = get_adcbuf4();

    return;
}

void fifo_dispatch(void* arg)
{
    Fifo* fifo = (Fifo*)arg;

    uint16_t i = 0;
    for (i = 0; !fifo_empty(fifo) && can2_tx_available() > 0; i++)
        send_can2(*pop_fifo(fifo));

    return;
}

void receive_msgs(void* arg)
{
    if (can2_rx_empty()) {
        return;
    }

    CANdata _msg;
    CANdata* msg = &_msg;

    msg = pop_can2();

    cfg_dispatch(*msg);
    cmd_dispatch(*msg);

    return;
}

double convert_pressure(double voltage_percentage)
{
    double voltage = 5.0 * (voltage_percentage / 100.0);

    if (voltage > 4.5) {
        return 160;
    } else if (voltage < 0.5) {
        return 0;
    } else {
        return 35.6 * (voltage - 0.5);
    }
}

void process_pedals(void* arg)
{
    int i = 0;
    uint32_t* values = (uint32_t*)arg;
    uint16_t pedals[5] = { 0 };
    float pedals_pct[5] = { 0 };
    uint16_t cc[5] = { 0 };
    uint16_t l_bound_addresses[5] = {
        CFG_TE_APPS0_GND,
        CFG_TE_APPS1_GND,
        CFG_TE_BPS0_GND,
        CFG_TE_BPS1_GND,
        CFG_TE_BE_GND
    };

    uint16_t u_bound_addresses[5] = {
        CFG_TE_APPS0_VCC,
        CFG_TE_APPS1_VCC,
        CFG_TE_BPS0_VCC,
        CFG_TE_BPS1_VCC,
        CFG_TE_BE_VCC
    };

    uint16_t l_sat_addresses[5] = {
        CFG_TE_APPS0_0N,
        CFG_TE_APPS1_0N,
        CFG_TE_BPS0_0N,
        CFG_TE_BPS1_0N,
        CFG_TE_BE_0N
    };

    uint16_t u_sat_addresses[5] = {
        CFG_TE_APPS0_MAXN,
        CFG_TE_APPS1_MAXN,
        CFG_TE_BPS0_MAXN,
        CFG_TE_BPS1_MAXN,
        CFG_TE_BE_MAXN
    };

    uint16_t l_bounds[5];
    uint16_t u_bounds[5];
    uint16_t l_sat[5];
    uint16_t u_sat[5];

    can_t* can_global = get_can_global();

    for (i = 0; i < 5; i++) {
        run_avg_add(&run_avgs[i], values[i]);

        printf("values[%d]: %d\n", i, values[i]);
        pedals[i] = run_avg_average(&run_avgs[i]);
        u_bounds[i] = param[u_bound_addresses[i]];
        l_bounds[i] = param[l_bound_addresses[i]];
        u_sat[i] = param[u_sat_addresses[i]];
        l_sat[i] = param[l_sat_addresses[i]];

        printf("pedals %d %d\n", i, pedals[i]);
        printf("u_bound %d %d\n", i, u_bounds[i]);
        printf("l_bound %d %d\n", i, l_bounds[i]);
    }

    // check short circuit bounds
    pedals_check_cc_bounds(cc, pedals, l_bounds, u_bounds);

    // saturate readings
    pedals_saturate_sensors(pedals_pct, pedals, l_sat, u_sat);

    for (i = 0; i < 5; i++) {
        printf("pedal_pct %d %f\n", i, pedals_pct[i]);
    }

    // compute apps value and fault signal
    float apps = 0;
    bool apps_faulty = 0;
    pedals_check_apps(&apps, &apps_faulty, pedals_pct, cc);

    printf("apps: %f\n", apps);

    float pressure = 0;
    float pressure0 = 0;
    float pressure1 = 0;
    bool pressure0_faulty = 0;
    bool pressure1_faulty = 0;

    pedals_conv_pressure(&pressure0, &pressure0_faulty, pedals_pct[2], cc[2], 2, 0, 200);
    pedals_conv_pressure(&pressure1, &pressure1_faulty, pedals_pct[3], cc[3], 2, 0, 200);
    // pressure media dos dois
    pressure = (pressure0 + pressure1) / 2;

    printf("pressure0: %f\n", pressure0);
    printf("pressure1: %f\n", pressure1);

    bool apps_brake_fault = pedals_check_apps_brake(apps, pressure);

    printf("implausibility: %d\n", apps_brake_fault);

    dev_te_t* te = &(can_global->te);
    te->te_apps.te_apps0 = 100 * (run_avg_average(&run_avgs[0]) / 4096);
    te->te_apps.te_apps1 = 100 * (run_avg_average(&run_avgs[1]) / 4096);
    te->te_brake.te_bps0 = 100 * (run_avg_average(&run_avgs[2]) / 4096);
    te->te_brake.te_bps1 = 100 * (run_avg_average(&run_avgs[3]) / 4096);
    te->te_brake.te_be = 100 * (run_avg_average(&run_avgs[4]) / 4096);

    te->te_press.te_press_f = convert_pressure(te->te_brake.te_bps0);
    te->te_press.te_press_r = convert_pressure(te->te_brake.te_bps1);

    // te->te_main.te_status_cc_apps0;
    // te->te_main.te_status_cc_apps1;
    // te->te_main.te_status_cc_bpse0;
    // te->te_main.te_status_cc_bpsp0;
    // te->te_main.te_status_cc_bpsp1;
    // te->te_main.te_status_os_apps0;
    // te->te_main.te_status_os_apps1;
    // te->te_main.te_status_os_bpse;
    te->te_main.te_status_imp_apps_bps_timer_exceeded = apps_brake_fault;
    te->te_main.te_status_hb = pedals_hardbraking(pressure0, param[CFG_TE_HB_TRESHOLD]);
    // te->te_main.te_status_sdc;
    // te->te_main.te_status_verbose;
    // te->te_main.te_status_imp_apps_bps;
    // te->te_main.te_os_bpsp0;
    if (apps_brake_fault == 1) {
        te->te_main.te_main_APPS = 0;
    } else {
        te->te_main.te_main_APPS = apps;
    }
    te->te_main.te_main_BPSp = (pedals_pct[2] + pedals_pct[3]) / 2;
    te->te_main.te_main_BPSe = pedals_pct[4];
    // te->te_main.te_status_imp_apps_timer_exceeded;
    // te->te_main.te_status_imp_apps;

    printf("apps_finish: %f\n", te->te_main.te_main_APPS);
    return;
}
