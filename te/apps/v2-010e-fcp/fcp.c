#include <stdint.h>

#include "can-ids-v1/can_cmd.h"
#include "can-ids-v1/can_ids.h"

#include "shared/fifo/fifo.h"

#include "lib_pic30f/lib_pic30f.h"

#include "torque_encoder.h"

can_t can_global;
extern Fifo* fifo;
extern _prog_addressT eeprom_addess;
extern uint32_t param[CFG_TE_SIZE];
extern uint16_t _EEDATA(32) eeprom_data[];

void dev_send_msg(CANdata msg)
{
    append_fifo(fifo, msg);
}

uint16_t dev_get_id()
{
    return DEVICE_ID_TE;
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{
    multiple_return_t mt = { 0 };

    if (id == CMD_TE_SAVE_EEPROM) {
        int i = 0;
        for (i = 0; i < CFG_TE_SIZE; i++) {
            eeprom_data[i] = param[i];
        }

        eeprom_write_row(eeprom_address, eeprom_data);
    }

    return mt;
}

can_t* get_can_global()
{
    return &can_global;
}
