#ifndef __TASKS_H__
#define __TASKS_H__

/*! \dir apps/v2-010e-fcp
 * \brief apps dir
 */

/*! \file tasks.h
 * \brief task file
 */

void get_all_adcs(void *arg);
void fifo_dispatch(void *arg);
void receive_msgs(void *arg);
void process_pedals(void *arg);
#endif
