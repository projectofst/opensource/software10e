#ifndef _CHECK_FUNCTIONS_H_
#define _CHECK_FUNCTIONS_H_

#include "can-ids-v1/Devices/TE_CAN.h"
#include "torque_encoder.h"

/*! \file check_functions.h
    \brief This file contains all functions that perform safety checks.
*/

// unsigned int    check_GND_VCC                   (unsigned int signal, unsigned int select_sensors);
// void            check_sensors                   (TE_VALUES *value, TE_CORE *status);
// unsigned int    check_sensors_integrity         (TE_VALUES *value, TE_CORE *status);

/*! \fn void check_implausibility (TE_VALUES *value, TE_CORE *status)
	\brief This function checks if the percentage average value of APPS0 differ from APPS1 more than 10 per cent.
	\param value Pointer to the TE_VALUES struct.
	\param status Pointer to the TE_CORE struct.
*/
void check_implausibility(TE_VALUES *value, TE_CORE *status);

/*! \fn unsigned int find (unsigned int signal1, unsigned int signal2, unsigned int select_sensor)
	\brief This function finds the biggest value of the two brake pressure sensors. Also, it finds the smallest value of the
	two throttle sensors.
	\param signal1 Sensor value.
	\param signal2 Sensor value.
	\param select_sensor Selects between APPS0, APPS1, BRAKE_pressure_0 and BRAKE_pressure_1 sensors.
	\return Biggest value of the two brake pressure sensors or smallest value of the two throttle sensors. 
*/
unsigned int find(unsigned int signal1, unsigned int signal2, unsigned int select_sensor);

/*! \fn void check_APPS_BPPS_plausibility (TE_VALUES *value, TE_CORE *status)
	\brief This function checks if there is an implausibility between throttle average values and brake pressure average values. 
    If there is an implausibility, then checks if brake average value is smaller than 500. If it is true, there is no more
    an implausibility state, an so implausibility_APPS_BPS flag is set to zero.
    Otherwise, we continue to have an implausibility state, therefore implausibility_APPS_BPS flags continues to be set to 1. 
	\param value Pointer to the TE_VALUES struct.
	\param status Pointer to the TE_CORE struct.
*/
void check_APPS_BPPS_plausibility(TE_VALUES *value, TE_THRESHOLDS *thresholds, TE_CORE *status);

/*! \fn void check_master (TE_VALUES *value, TE_CORE *status, COUNTERS *timer_counter)
	\brief This function performs multiple integrity checks, such as: implausibility between throttle sensors checks,
	counts the duration of implausibility event, checks if there is an implausibility between throttle sensors values and brake pressure sensors values and 
	checks the shutdown circuit status.  
	\param value Pointer to the TE_VALUES struct.
	\param status Pointer to the TE_CORE struct.
	\param timer_counter Pointer to the COUNTERS struct.
*/
void check_master(TE_VALUES *value, TE_THRESHOLDS *thresholds, TE_CORE *status, COUNTERS *timer_counter);

/*! \fn unsigned int increment_counter (unsigned int flag, unsigned int counter)
	\brief This function is executed every 25ms and counts the number of periods of 25ms 
	that implausibility between throttle sensors is detected. If implausibility between throttle sensors isn't detected 
	then the counter is reseted.
	\param flag Implausibility throttle sensors flag.
	\param counter Number of continuous periods of 25ms that implausibility has been registered between 
	throttle sensors.
	\return Number of continuous periods of 25ms that implausibility has been registered between 
	throttle sensors.
*/
void increment_counter(TE_CORE *status, COUNTERS *timer_counter);

/*! \fn unsigned int check_counter (unsigned int flag, unsigned int counter)
	\brief This function checks if has passed more than 100ms with continuous implausibility state between throttle
	sensors. 
	\param flag Implausibility throttle sensors flag.
	\param counter Number of continuous periods of 25ms that has been registered implausibility between 
	throttle sensors.
	\return Value 0 or 1.
*/
void check_counter(TE_CORE *status, COUNTERS *timer_counter);

/*! \fn void check_time (TE_CORE *status, COUNTERS *timer_counter)
	\brief This function updates COUNTERS struct with the current value of the counter. It also updates the value of
	the throttle implausibility bit. 
	\param status Pointer to the TE_CORE struct.
	\param timer_counter Pointer to the COUNTERS struct.
*/
void check_time(TE_CORE *status, COUNTERS *timer_counter);

/*! \fn void check_shut_circuit (TE_CORE *status)
	\brief This function checks if the shutdownt circuit has been open.
	\param status Pointer to the TE_CORE struct.
*/
void check_shut_circuit(TE_CORE *status);

/*! \fn bool check_TE_RTD_status (TE_CORE *status, TE_VALUES *value)
	\brief This function checks if all sensors aren't at a short circuit state. It also checks if implausibility between sensors isn't detected.
	At least, checks if throttle pedal isn't pressed and brake pedal is pressed. If everything mentioned before is true then READY-TO-DRIVE flag is set to 1. 
	Otherwise, the READY-TO-DRIVE flag is set to 0.
	\param status Pointer to the TE_CORE struct.
	\param value Pointer to the TE_VALUES struct.
*/
bool check_TE_RTD_status(TE_CORE *status, TE_VALUES *value);

unsigned int hard_break_detection(TE_VALUES *value, TE_THRESHOLDS *thresholds, TE_CORE *status);

void update_hard_braking_limit(TE_THRESHOLDS *thresholds, uint16_t pressure, uint16_t new_thresh[7]);

#endif