#include "send_functions.h"

#include "check_functions.h"
#include "update_functions.h"

#include "torque_encoder.h"

/******************************************************************************/
// Name: send_BPS_eletric
//       If BPS eletric is at short circuit state then send ZERO
//       OTHERWISE, send its average value
// TESTED: YES
/******************************************************************************/

void send_BPS_eletric(TE_VALUES* value, TE_CORE* status, TE_MESSAGE* main_message)
{
    if (status->CC_BPS_electric_0 == 1) {
        main_message->BPS_electric = 0;
    } else {
        main_message->BPS_electric = value->brake_electric_torque;
    }

    return;
}

/*************************************************************************/
/* Name: send_BPS_pressure
       If both pressure sensors are at short circuit then sent 0
       If one of the pressure sensors is at short circuit then sent the other sensor percentage value
       If both pressure sensors are ok then sent the average of the two percentages
 TESTED: YES
 */
/*************************************************************************/

void send_BPS_pressure(TE_VALUES* value, TE_CORE* status, TE_MESSAGE* main_message)
{
    if (status->CC_BPS_pressure_0 == 0) { // if everything is okay with the BPS PRESSURE 0 THEN
        main_message->BPS_pressure = value->brake_pressure_torque; // brake pressure 0 torque
    } else {
        main_message->BPS_pressure = 0;
    }
    return;
}

/******************************************************************************/
// NAME: send_APPS
//       if every check of plausability is positive then send the average of APPS0 AND APPS1
//       otherwise send zero :)
// TESTED: YES
/******************************************************************************/

void send_APPS(TE_VALUES* value, TE_CORE* status, TE_MESSAGE* main_message)
{
    if (status->CC_APPS_0 == 0 && status->CC_APPS_1 == 0 && status->Implausibility_APPS_Timer_Exceeded == 0 && status->Implausibility_APPS_BPS_Timer_Exceeded == 0 && status->CC_BPS_pressure_0 == 0) {

        main_message->APPS = value->accelerator_torque; // torque percentage
    } else {
        main_message->APPS = 0;
    }

    return;
}

void send_status(TE_CORE* status, TE_MESSAGE* main_message)
{
    main_message->status.TE_status = status->TE_status;

    return;
}

/*! \fn void send2CAN (TE_MESSAGE *main_message, TE_DEBUG_MESSAGE *debug_message)
    \brief This function is responsible for sending the main message and depending
    of the debug mode bit, this function can also send the debug message.
    \param main_message Pointer to the TE_MESSAGE struct.
    \param debug_message Pointer to the TE_DEBUG_MESSAGE struct.
*/
void send2CAN(TE_MESSAGE* main_message)
{
    CANdata main_msg;

    main_msg.dev_id = DEVICE_ID_TE;
    main_msg.msg_id = MSG_ID_TE_MAIN;
    main_msg.dlc = 8;
    main_msg.data[0] = main_message->status.TE_status;
    main_msg.data[1] = main_message->APPS;
    main_msg.data[2] = main_message->BPS_pressure;
    main_msg.data[3] = main_message->BPS_electric;

    bsp_write_can_essential_buffer(main_msg, 1);

    return;
}

/*! \fn void send_raw_values(TE_VALUES *values, COUNTERS *time_counters, TE_CORE *status)
    \brief This function is responsible for sending two raw messages via CAN. The first raw
    message has the raw average values of the two throttle sensors and the two brake pressure sensors.
    The second raw message has the raw average value of the brake eletric sensor and information
    relative to the implausibility time between APPS0 and APPS1.
    \param values Pointer to the TE_VALUES struct.
    \param time_counters Pointer to the COUNTERS struct.
    \param status Pointer to the TE_CORE struct.
*/
void send_verbose_values(TE_VALUES* values, TE_THRESHOLDS* thresholds, TE_CORE* status)
{
    static counter = 0;

    CANdata verbose_message_1;
    CANdata verbose_message_2;

    verbose_message_1.dev_id = DEVICE_ID_TE;
    verbose_message_1.msg_id = MSG_ID_TE_VERBOSE_1;
    verbose_message_1.dlc = 8;

    verbose_message_1.data[0] = values->raw_avg_APPS_0;
    verbose_message_1.data[1] = values->raw_avg_APPS_1;
    verbose_message_1.data[2] = values->raw_avg_BPS_pressure_0;
    verbose_message_1.data[3] = values->raw_avg_BPS_pressure_1;

    verbose_message_2.dev_id = DEVICE_ID_TE;
    verbose_message_2.msg_id = MSG_ID_TE_VERBOSE_2;
    verbose_message_2.dlc = 8;

    verbose_message_2.data[0] = values->raw_avg_BPS_electric;
    verbose_message_2.data[1] = thresholds->hard_braking_thresh;
    verbose_message_2.data[2] = status->Implausibility_APPS_Timer_Exceeded;
    verbose_message_2.data[3] = status->Implausibility_APPS_BPS_Timer_Exceeded;

    bsp_write_can_essential_buffer(verbose_message_1, 0);
    bsp_write_can_essential_buffer(verbose_message_2, 0);

    return;
}

void send_brake_pressure_bar(TE_VALUES* values, TE_CORE* status)
{
    CANdata msg_pressure_bar;

    msg_pressure_bar.dev_id = DEVICE_ID_TE;
    msg_pressure_bar.msg_id = MSG_ID_TE_BAR_PRESSURE;
    msg_pressure_bar.dlc = 8;

    msg_pressure_bar.data[0] = convert_voltage_to_bar(values->raw_avg_BPS_pressure_0);
    msg_pressure_bar.data[1] = convert_voltage_to_bar(values->raw_avg_BPS_pressure_1);
    msg_pressure_bar.data[2] = 0;
    msg_pressure_bar.data[3] = 0;

    bsp_write_can_essential_buffer(msg_pressure_bar, 0);
}

void send_main_forces(TE_VALUES* values)
{
    CANdata msg;

    msg.dev_id = DEVICE_ID_TE;
    msg.msg_id = MSG_ID_TE_FORCES;
    msg.dlc = 8;

    msg.data[0] = values->accelerator_torque;
    msg.data[1] = values->brake_pressure;
    msg.data[2] = values->brake_electric;
    msg.data[3] = 0;

    bsp_write_can_essential_buffer(msg, 0);
}

void send_messages(TE_VALUES* value, TE_THRESHOLDS* thresholds, TE_CORE* status)
{
    TE_MESSAGE main_message;

    // Prepares the main CAN message to be sent
    send_status(status, &main_message);
    send_APPS(value, status, &main_message);
    send_BPS_pressure(value, status, &main_message);
    send_BPS_eletric(value, status, &main_message);

    if (status->Verbose_Mode) {
        send_verbose_values(value, thresholds, status); // carefull, zybo
    }
    // TE now doesn't send the main message
    send2CAN(&main_message);
    send_brake_pressure_bar(value, status);
    send_main_forces(value);

    return;
}

void send_init_msg(void)
{
    CANdata init_msg;

    init_msg = make_reset_msg(DEVICE_ID_TE, RCON);

    bsp_write_can_essential_buffer(init_msg, 1);

    return;
}

// SENDS VERSION TO INTERFACE
void send_version(int version_id_num)
{
    CANdata version_data;
    uint16_t data[4];

    get_version(VERSION, &data);

    version_data.dev_id = DEVICE_ID_TE;
    version_data.msg_id = CMD_ID_COMMON_GET;
    version_data.dlc = 8;
    version_data.data[0] = DEVICE_ID_TE;

    version_data.data[1] = version_id_num;
    version_data.data[2] = data[version_id_num];
    bsp_write_can_essential_buffer(version_data, 1);
}

/*! \fn void send_TE_limit(TE_LIMIT *limit_message)
    \brief This function is responsible of sending the requested thresholds of the requested sensor via CAN.
    \param Pointer to the TE_LIMIT struct.
*/
void send_TE_limit(TE_LIMIT* limit_message)
{

    CANdata TE_limit_msg;

    TE_limit_msg.dev_id = DEVICE_ID_TE;
    TE_limit_msg.msg_id = MSG_ID_TE_LIMIT_ANSWER;
    TE_limit_msg.dlc = 4;

    TE_limit_msg.data[0] = limit_message->header;
    TE_limit_msg.data[1] = limit_message->data_limit;

    bsp_write_can_essential_buffer(TE_limit_msg, 1);
}

void send_ack(int DEVICE_ID, int PARAMETER_ID, uint16_t data)
{

    CANdata message_ack;

    message_ack.dev_id = DEVICE_ID;
    message_ack.msg_id = CMD_ID_COMMON_SET;
    message_ack.dlc = 8;

    message_ack.data[0] = DEVICE_ID_TE;
    message_ack.data[1] = PARAMETER_ID;
    message_ack.data[2] = data;
    message_ack.data[3] = 0;
    bsp_write_can_essential_buffer(message_ack, 1);
}

/*! \fn uint16_t get_limit(SENSOR_THRESHOLDS thresholds, TE_THRESHOLD_LIMIT limit_type )
    \brief This function is responsible of getting the requested threshold.
    \param threshold Selects one of the sensor limits.
    \param limit_type Selects one of the GND, VCC, ZERO_FORCE, MAX_FORCE, LOWER_MECHANICAL or UPPER_MECHANICAL limits.
    \return The requested threshold.
*/
uint16_t get_limit(SENSOR_THRESHOLDS thresholds, TE_THRESHOLD_LIMIT limit_type)
{

    switch (limit_type) {
    case GND:
        return thresholds.gnd;
        break;
    case VCC:
        return thresholds.vcc;
        break;
    case ZERO_FORCE:
        return thresholds.zero_force;
        break;
    case MAX_FORCE:
        return thresholds.max_force;
        break;
    }

    return 0;
}

/*! \fn void find_request_limit (TE_THRESHOLD_LIMIT limit_type, SENSOR_SELECT sensor, TE_THRESHOLDS *thresholds)
    \brief This function gets the requested threshold of the requested sensor.
    \param limit_type Selects one of the GND, VCC, ZERO_FORCE, MAX_FORCE, LOWER_MECHANICAL or UPPER_MECHANICAL limits.
    \param sensor Selects one of the 5 sensors.
    \param thresholds Pointer to the TE_THRESHOLDS struct.
*/
void find_request_limit(TE_THRESHOLD_LIMIT limit_type, SENSOR_SELECT sensor, TE_THRESHOLDS* thresholds)
{

    TE_LIMIT limit_message;

    limit_message.header = limit_type + (sensor << 3);

    switch (sensor) {
    case APPS_0:
        limit_message.data_limit = get_limit(thresholds->APPS_0, limit_type);
        break;
    case APPS_1:
        limit_message.data_limit = get_limit(thresholds->APPS_1, limit_type);
        break;
    case BPS_PRESSURE_0:
        limit_message.data_limit = get_limit(thresholds->BPS_pressure_0, limit_type);
        break;
    case BPS_PRESSURE_1:
        limit_message.data_limit = get_limit(thresholds->BPS_pressure_1, limit_type);
        break;
    case BPS_ELECTRIC:
        limit_message.data_limit = get_limit(thresholds->BPS_electric, limit_type);
        break;
    }

    send_TE_limit(&limit_message);
}

/*! \fn bool filter_can2(uint16_t sid)
    \brief This function enables or disables the messages that can be received by the Torque Encoder sent by CAN.
    \param sid Device identifier number.
    \return Boolean.
*/

bool filter_can2(unsigned sid)
{
    received_can_flag = 1;
    uint16_t dev_id = CAN_GET_DEV_ID(sid);
    uint16_t msg_id = CAN_GET_MSG_ID(sid);

    if (dev_id == DEVICE_ID_INTERFACE) {
        if (msg_id == CMD_ID_INTERFACE_DEBUG_TOGGLE || CMD_ID_INTERFACE_SET_PEDAL_THRESHOLD || CMD_ID_INTERFACE_TE_TOGGLE_RAW_MODE || MSG_ID_INTERFACE_TE_LIMITS_REQUEST)
            return true;
        else
            return false;
    }

    if (dev_id == DEVICE_ID_DASH) {
        if (msg_id == CMD_ID_COMMON_RTD_ON)
            return true;
        else
            return false;
    }
    if (msg_id == CMD_ID_COMMON_SET) {
        return true;
    }

    if (msg_id == CMD_ID_COMMON_GET) {
        return true;
    }

    if (dev_id == DEVICE_ID_TE) {
        if (msg_id == MSG_ID_TE_VERBOSE_1 || msg_id == MSG_ID_TE_VERBOSE_2)
            return true;
    }

    return false;
}

void process_can_message(CANdata* r_message,
    TE_CORE* status,
    TE_VALUES* values,
    TE_THRESHOLDS* thresholds,
    uint16_t new_thresh[7])
{

    INTERFACE_CAN_Data interface_data;

    switch (r_message->dev_id) {

    case DEVICE_ID_INTERFACE:

        parse_can_interface(*r_message, &interface_data);

        switch (r_message->msg_id) {

        case CMD_ID_INTERFACE_DEBUG_TOGGLE:

            if (r_message->data[0] == DEBUG_TOGGLE_TORQUE_ENCODER)
                break;

        case CMD_ID_INTERFACE_SET_PEDAL_THRESHOLD:

            update_selected_thresholds(interface_data.pedal_thresholds,
                values,
                thresholds,
                new_thresh);
            send_ack(DEVICE_ID_TE, P_TE_UPDATE_PEDAL_THRS, interface_data.pedal_thresholds);
            break;

        case CMD_ID_INTERFACE_TE_TOGGLE_RAW_MODE:
            break;

        case MSG_ID_INTERFACE_TE_LIMITS_REQUEST:

            find_request_limit(interface_data.limit_request.limit, interface_data.limit_request.sensor, thresholds);
            break;

        case CMD_ID_COMMON_SET:
            if (r_message->data[0] == DEVICE_ID_TE && r_message->data[1] == P_TE_HARD_BRAKING_PRESS_THRS) {
                update_hard_braking_limit(thresholds, interface_data.hard_breaking.hard_breaking_press_limit, new_thresh);
                send_ack(DEVICE_ID_TE, P_TE_HARD_BRAKING_PRESS_THRS, interface_data.hard_breaking.hard_breaking_press_limit);
            }
            break;

        case CMD_ID_COMMON_GET:
            if (r_message->data[0] == DEVICE_ID_TE) {
                switch (r_message->data[1]) {
                case P_TE_VERSION_ID_0:
                    send_version(P_TE_VERSION_ID_0);
                    break;
                case P_TE_VERSION_ID_1:
                    send_version(P_TE_VERSION_ID_1);
                    break;
                case P_TE_VERSION_ID_2:
                    send_version(P_TE_VERSION_ID_2);
                    break;
                case P_TE_VERSION_ID_3:
                    send_version(P_TE_VERSION_ID_3);
                    break;
                }
            }
        default:
            break;
        }
        break;

    case DEVICE_ID_DASH:

        if (r_message->msg_id == CMD_ID_COMMON_RTD_ON) {

            COMMON_MSG_RTD_ON RTD_sequence;
            parse_can_common_RTD(*r_message, &RTD_sequence);

            if (RTD_sequence.step == RTD_STEP_DASH_BUTTON) {

                CANdata RTD_TE_message;

                RTD_TE_message.dev_id = DEVICE_ID_TE;
                RTD_TE_message.msg_id = CMD_ID_COMMON_RTD_ON;
                RTD_TE_message.dlc = 2;

                if (check_TE_RTD_status(status, values)) {
                    RTD_TE_message.data[0] = RTD_STEP_TE_OK;
                } else {
                    RTD_TE_message.data[0] = RTD_STEP_TE_NOK;
                }

                bsp_write_can_essential_buffer(RTD_TE_message, 1);
            }
        }
        break;

    default:
        break;
    }
}
