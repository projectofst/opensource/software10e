#include "xc.h"

#include "config_functions.h"

uint16_t initial_EE_thresholds[11] = {
    APPS_0_ZERO_FORCE_THRS,
    APPS_0_MAX_FORCE_THRS,
    APPS_1_ZERO_FORCE_THRS,
    APPS_1_MAX_FORCE_THRS,
    BPS_PRESSURE_0_ZERO_FORCE_THRS,
    BPS_PRESSURE_0_MAX_FORCE_THRS,
    BPS_PRESSURE_1_ZERO_FORCE_THRS,
    BPS_PRESSURE_1_MAX_FORCE_THRS,
    BPS_ELECTRIC_ZERO_FORCE_THRS,
    BPS_ELECTRIC_MAX_FORCE_THRS,
    HARD_BRAKING_THRS,
};

void init_thresholds(TE_THRESHOLDS* thresholds, uint16_t EE_thresholds[11], uint16_t new_thesh[11])
{
    bsp_read_memory(EE_thresholds, sizeof(EE_thresholds) / sizeof(EE_thresholds[0]));

    thresholds->APPS_0.gnd = APPS_0_GND_THRS;
    thresholds->APPS_0.vcc = APPS_0_VCC_THRS;
    thresholds->APPS_0.zero_force = valid_threshold(initial_EE_thresholds, EE_thresholds, APPS_0_ZERO_FORCE);
    thresholds->APPS_0.max_force = valid_threshold(initial_EE_thresholds, EE_thresholds, APPS_0_MAX_FORCE);

    new_thesh[APPS_0_ZERO_FORCE] = valid_threshold(initial_EE_thresholds, EE_thresholds, APPS_0_ZERO_FORCE);
    new_thesh[APPS_0_MAX_FORCE] = valid_threshold(initial_EE_thresholds, EE_thresholds, APPS_0_MAX_FORCE);

    thresholds->APPS_1.gnd = APPS_1_GND_THRS;
    thresholds->APPS_1.vcc = APPS_1_VCC_THRS;
    thresholds->APPS_1.zero_force = valid_threshold(initial_EE_thresholds, EE_thresholds, APPS_1_ZERO_FORCE);
    thresholds->APPS_1.max_force = valid_threshold(initial_EE_thresholds, EE_thresholds, APPS_1_MAX_FORCE);
    new_thesh[APPS_1_ZERO_FORCE] = valid_threshold(initial_EE_thresholds, EE_thresholds, APPS_1_ZERO_FORCE);
    new_thesh[APPS_1_MAX_FORCE] = valid_threshold(initial_EE_thresholds, EE_thresholds, APPS_1_MAX_FORCE);

    thresholds->BPS_electric.gnd = BPS_ELECTRIC_GND_THRS;
    thresholds->BPS_electric.vcc = BPS_ELECTRIC_VCC_THRS;
    thresholds->BPS_electric.zero_force = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_ELECTRIC_ZERO_FORCE);
    thresholds->BPS_electric.max_force = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_ELECTRIC_MAX_FORCE);
    new_thesh[BPS_ELECTRIC_ZERO_FORCE] = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_ELECTRIC_ZERO_FORCE);
    new_thesh[BPS_ELECTRIC_MAX_FORCE] = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_ELECTRIC_MAX_FORCE);

    thresholds->BPS_pressure_0.gnd = BPS_PRESSURE_0_GND_THRS;
    thresholds->BPS_pressure_0.vcc = BPS_PRESSURE_0_VCC_THRS;
    thresholds->BPS_pressure_0.zero_force = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_0_ZERO_FORCE);
    thresholds->BPS_pressure_0.max_force = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_0_MAX_FORCE);

    new_thesh[BPS_0_ZERO_FORCE] = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_0_ZERO_FORCE);
    new_thesh[BPS_0_MAX_FORCE] = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_0_MAX_FORCE);

    thresholds->BPS_pressure_1.gnd = BPS_PRESSURE_1_GND_THRS;
    thresholds->BPS_pressure_1.vcc = BPS_PRESSURE_1_VCC_THRS;
    thresholds->BPS_pressure_1.zero_force = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_1_ZERO_FORCE);
    thresholds->BPS_pressure_1.max_force = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_1_MAX_FORCE);
    new_thesh[BPS_1_ZERO_FORCE] = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_1_ZERO_FORCE);
    new_thesh[BPS_1_MAX_FORCE] = valid_threshold(initial_EE_thresholds, EE_thresholds, BPS_1_MAX_FORCE);

    thresholds->APPS_0.range = thresholds->APPS_0.max_force - thresholds->APPS_0.zero_force;
    thresholds->APPS_1.range = thresholds->APPS_1.max_force - thresholds->APPS_1.zero_force;
    thresholds->BPS_pressure_0.range = thresholds->BPS_pressure_0.max_force - thresholds->BPS_pressure_0.zero_force;
    thresholds->BPS_pressure_1.range = thresholds->BPS_pressure_1.max_force - thresholds->BPS_pressure_1.zero_force;
    thresholds->BPS_electric.range = thresholds->BPS_electric.max_force - thresholds->BPS_electric.zero_force;

    thresholds->hard_braking_thresh = valid_threshold(initial_EE_thresholds, EE_thresholds, HARD_BRAKING_THRESHOLD); // initial value set to 30 bar
    new_thesh[HARD_BRAKING_THRESHOLD] = valid_threshold(initial_EE_thresholds, EE_thresholds, HARD_BRAKING_THRESHOLD);
    return;
}
