#include "update_functions.h"
#include "torque_encoder.h"

uint16_t update_torque_value(uint16_t value)
{

    uint16_t new_value;

    if (value > 9500) {
        new_value = 10000;
    } else if (value < 500) {
        new_value = 0;
    } else {
        new_value = ((value - 500) / 0.9);
    }
    return new_value;
}

void update_selected_thresholds(INTERFACE_MSG_PEDAL_THRESHOLDS select,
    TE_VALUES* value,
    TE_THRESHOLDS* thresholds,
    uint16_t new_thresholds[11])
{
    switch (select) {
    case BRAKE_ZERO:
        check_threshold_overlap(SAFE_ZONE_SELECT_LOWER, &thresholds->BPS_electric, &value->raw_avg_BPS_electric);
        check_threshold_overlap(SAFE_ZONE_SELECT_LOWER, &thresholds->BPS_pressure_0, &value->raw_avg_BPS_pressure_0);
        check_threshold_overlap(SAFE_ZONE_SELECT_LOWER, &thresholds->BPS_pressure_1, &value->raw_avg_BPS_pressure_1);
        new_thresholds[BPS_ELECTRIC_ZERO_FORCE] = thresholds->BPS_electric.zero_force;
        new_thresholds[BPS_0_ZERO_FORCE] = thresholds->BPS_pressure_0.zero_force;
        new_thresholds[BPS_1_ZERO_FORCE] = thresholds->BPS_pressure_1.zero_force;
        break;

    case BRAKE_MAX:
        check_threshold_overlap(SAFE_ZONE_SELECT_UPPER, &thresholds->BPS_electric, &value->raw_avg_BPS_electric);
        check_threshold_overlap(SAFE_ZONE_SELECT_UPPER, &thresholds->BPS_pressure_0, &value->raw_avg_BPS_pressure_0);
        check_threshold_overlap(SAFE_ZONE_SELECT_UPPER, &thresholds->BPS_pressure_1, &value->raw_avg_BPS_pressure_1);
        new_thresholds[BPS_ELECTRIC_MAX_FORCE] = thresholds->BPS_electric.max_force;
        new_thresholds[BPS_0_MAX_FORCE] = thresholds->BPS_pressure_0.max_force;
        new_thresholds[BPS_1_MAX_FORCE] = thresholds->BPS_pressure_1.max_force;
        break;

    case ACCELERATOR_ZERO:
        check_threshold_overlap(SAFE_ZONE_SELECT_LOWER, &thresholds->APPS_0, &value->raw_avg_APPS_0);
        check_threshold_overlap(SAFE_ZONE_SELECT_LOWER, &thresholds->APPS_1, &value->raw_avg_APPS_1);
        new_thresholds[APPS_0_ZERO_FORCE] = thresholds->APPS_0.zero_force;
        new_thresholds[APPS_1_ZERO_FORCE] = thresholds->APPS_1.zero_force;
        break;

    case ACCELERATOR_MAX:
        check_threshold_overlap(SAFE_ZONE_SELECT_UPPER, &thresholds->APPS_0, &value->raw_avg_APPS_0);
        check_threshold_overlap(SAFE_ZONE_SELECT_UPPER, &thresholds->APPS_1, &value->raw_avg_APPS_1);
        new_thresholds[APPS_0_MAX_FORCE] = thresholds->APPS_0.max_force;
        new_thresholds[APPS_1_MAX_FORCE] = thresholds->APPS_1.max_force;
        break;
    default:
        break;
    }

    update_ranges(thresholds);

    bsp_write_memory(new_thresholds, sizeof(new_thresholds));

    return;
}

void check_threshold_overlap(SAFE_ZONE_SELECT safe_zone, SENSOR_THRESHOLDS* thresholds, uint16_t* raw_average)
{

    switch (safe_zone) {
    case SAFE_ZONE_SELECT_LOWER:
        if (*raw_average <= thresholds->gnd)
            thresholds->zero_force = thresholds->gnd + 1; // just a little bit higher that what should be (i.e higher than GND THRS)
        else
            thresholds->zero_force = *raw_average;
        break;

    case SAFE_ZONE_SELECT_UPPER:
        if (*raw_average >= thresholds->vcc)
            thresholds->max_force = thresholds->vcc - 1; // just a little bit lower that what should be (i.e lower than VCC THRS)
        else
            thresholds->max_force = *raw_average;
        break;

    default:
        break;
    }
    return;
}

void update_ranges(TE_THRESHOLDS* thresholds)
{
    thresholds->APPS_0.range = thresholds->APPS_0.max_force - thresholds->APPS_0.zero_force;
    thresholds->APPS_1.range = thresholds->APPS_1.max_force - thresholds->APPS_1.zero_force;
    thresholds->BPS_pressure_0.range = thresholds->BPS_pressure_0.max_force - thresholds->BPS_pressure_0.zero_force;
    thresholds->BPS_pressure_1.range = thresholds->BPS_pressure_1.max_force - thresholds->BPS_pressure_1.zero_force;
    thresholds->BPS_electric.range = thresholds->BPS_electric.max_force - thresholds->BPS_electric.zero_force;
    return;
}
/*
*********************************************************************
|   Name:           update_values
|
|   Description:    Updates 5 arrays by adding one new value for each
|                   and removing the oldest one
|
|   Priority:       -
|
|   Arguments:      Pointer to the structure containing the values
|   Returns:        -
|
|   Tested:         Yes
*********************************************************************
*/
void update_values(TE_VALUES* value)
{

    value->BPS_electric_0[value->index] = value->new_BPS_electric_0;
    value->BPS_pressure_0[value->index] = value->new_BPS_pressure_0;
    value->BPS_pressure_1[value->index] = value->new_BPS_pressure_1;

    value->APPS_0[value->index] = value->new_APPS_0;

    value->APPS_1[value->index] = value->new_APPS_1;

    if (value->index >= (NUMBER_OF_AVERAGE_POINTS - 1))
        value->index = 0;
    else
        value->index++;
    return;
}

uint16_t average(uint16_t* buffer, uint16_t size)
{
    uint32_t acc = 0;
    int i;

    for (i = 0; i < size; i++) {
        acc += buffer[i];
    }

    return acc / size;
}

/*
*********************************************************************
|   Name:           update_raw_averages
|
|   Description:    Updates 5 averages using the values stored in the
|                   arrays corresponding to each variable
|
|   Priority:       -
|
|   Arguments:      Pointer to the structure containing the values
|   Returns:        -
|
|   Tested:         Yes
*********************************************************************
*/
void update_raw_averages(TE_VALUES* value)
{

    value->avg_BPS_electric_0 = average(value->BPS_electric_0,
        NUMBER_OF_AVERAGE_POINTS);
    value->avg_BPS_pressure_0 = average(value->BPS_pressure_0,
        NUMBER_OF_AVERAGE_POINTS);
    value->avg_BPS_pressure_1 = average(value->BPS_pressure_1,
        NUMBER_OF_AVERAGE_POINTS);

    value->avg_APPS_0 = average(value->APPS_0,
        NUMBER_OF_AVERAGE_POINTS);

    value->avg_APPS_1 = average(value->APPS_1,
        NUMBER_OF_AVERAGE_POINTS);

    value->raw_avg_APPS_0 = value->avg_APPS_0;
    value->raw_avg_APPS_1 = value->avg_APPS_1;
    value->raw_avg_BPS_electric = value->avg_BPS_electric_0;
    value->raw_avg_BPS_pressure_0 = value->avg_BPS_pressure_0;
    value->raw_avg_BPS_pressure_1 = value->avg_BPS_pressure_1;

    return;
}

/*
*********************************************************************
|   Name:           update_treated_averages
|
|   Description:    Subtracts the lower bound from the raw average
|                   and calculates the percentage for all 5 variables
|   Priority:       -
|
|   Arguments:      Pointer to the structure containing the values
|   Returns:        -
|
|   Tested:         No
*********************************************************************
*/
void update_treated_averages(TE_VALUES* value, TE_THRESHOLDS* thresholds, TE_CORE* status)
{

    update_sensor_value(APPS_0, &(value->avg_APPS_0), &(thresholds->APPS_0), status);
    update_sensor_value(APPS_1, &(value->avg_APPS_1), &(thresholds->APPS_1), status);
    update_sensor_value(BPS_ELECTRIC, &(value->avg_BPS_electric_0), &(thresholds->BPS_electric), status);
    update_sensor_value(BPS_PRESSURE_0, &(value->avg_BPS_pressure_0), &(thresholds->BPS_pressure_0), status);
    update_sensor_value(BPS_PRESSURE_1, &(value->avg_BPS_pressure_1), &(thresholds->BPS_pressure_1), status);

    value->accelerator = ((float)(value->avg_APPS_0 + value->avg_APPS_1) / 2); // percentage

    value->brake_pressure = value->avg_BPS_pressure_0; // percentage
    value->brake_electric = value->avg_BPS_electric_0; // percentage

    value->accelerator_torque = update_torque_value(value->accelerator);
    value->brake_pressure_torque = update_torque_value(value->brake_pressure);
    value->brake_electric_torque = update_torque_value(value->brake_electric);

    return;
}

void update_sensor_value(SENSOR_SELECT sensor,
    uint16_t* course,
    SENSOR_THRESHOLDS* thresholds,
    TE_CORE* status)
{

    if (*course <= thresholds->gnd) {
        *course = 0;
        update_sensor_status(sensor, POSITION_SELECT_SHORT_CIRCUIT, status);
    }

    else if (*course <= thresholds->zero_force) {
        *course = 0;
        update_sensor_status(sensor, POSITION_SELECT_OVERSHOOT, status);
    } else if (*course >= thresholds->vcc) {
        *course = 0;
        update_sensor_status(sensor, POSITION_SELECT_SHORT_CIRCUIT, status);
    }

    else if (*course >= thresholds->max_force) {
        *course = 10000;
        update_sensor_status(sensor, POSITION_SELECT_OVERSHOOT, status);
    } else {
        *course -= thresholds->zero_force;
        *course = (float)(((*course) * 10000.0) / (thresholds->range)); // percentage
        update_sensor_status(sensor, POSITION_SELECT_NORMAL, status);
    }
    return;
}

void update_sensor_status(SENSOR_SELECT sensor, POSITION_SELECT position, TE_CORE* status)
{

    switch (sensor) {
        /*******************************************/
    case APPS_0:
        switch (position) {

        case POSITION_SELECT_SHORT_CIRCUIT:
            status->CC_APPS_0 = true;
            status->Overshoot_APPS_0 = true;
            break;
        case POSITION_SELECT_OVERSHOOT:
            status->CC_APPS_0 = false;
            status->Overshoot_APPS_0 = true;
            break;
        case POSITION_SELECT_NORMAL:
            status->CC_APPS_0 = false;
            status->Overshoot_APPS_0 = false;
            break;
            break;
        }
        /*******************************************/
    case APPS_1:
        switch (position) {

        case POSITION_SELECT_SHORT_CIRCUIT:
            status->CC_APPS_1 = true;
            status->Overshoot_APPS_1 = true;
            break;
        case POSITION_SELECT_OVERSHOOT:
            status->CC_APPS_1 = false;
            status->Overshoot_APPS_1 = true;
            break;
        case POSITION_SELECT_NORMAL:
            status->CC_APPS_1 = false;
            status->Overshoot_APPS_1 = false;
            break;
            break;
        }
        /*******************************************/
    case BPS_ELECTRIC:
        switch (position) {

        case POSITION_SELECT_SHORT_CIRCUIT:
            status->CC_BPS_electric_0 = true;
            status->Overshoot_BPS_electric = true;
            break;
        case POSITION_SELECT_OVERSHOOT:
            status->CC_BPS_electric_0 = false;
            status->Overshoot_BPS_electric = true;
            break;
        case POSITION_SELECT_NORMAL:
            status->CC_BPS_electric_0 = false;
            status->Overshoot_BPS_electric = false;
            break;
            break;
        }
        /*******************************************/
    case BPS_PRESSURE_0:
        switch (position) {

        case POSITION_SELECT_SHORT_CIRCUIT:
            status->CC_BPS_pressure_0 = true;
            status->Overshoot_BPS_pressure_0 = true;
            break;
        case POSITION_SELECT_OVERSHOOT:
            status->CC_BPS_pressure_0 = false;
            status->Overshoot_BPS_pressure_0 = true;
            break;
        case POSITION_SELECT_NORMAL:
            status->CC_BPS_pressure_0 = false;
            status->Overshoot_BPS_pressure_0 = false;
            break;
            break;
        }
        /*******************************************/
    case BPS_PRESSURE_1:
        switch (position) {
        case POSITION_SELECT_SHORT_CIRCUIT:
            status->CC_BPS_pressure_1 = true;
            status->Overshoot_BPS_pressure_1 = true;
            break;
        case POSITION_SELECT_OVERSHOOT:
            status->CC_BPS_pressure_1 = false;
            status->Overshoot_BPS_pressure_1 = true;
            break;
        case POSITION_SELECT_NORMAL:
            status->CC_BPS_pressure_1 = false;
            status->Overshoot_BPS_pressure_1 = false;
            break;
            break;
        }
    }
    return;
}

uint16_t convert_voltage_to_bar(unsigned int value)
{ // value : 0-4095

    double voltage = (50000UL * value) / 4095; // value to voltage: 0V - 5x10V
    uint32_t pressure_bar;
    if (voltage > 45000) { // bigger than 4.5V
        pressure_bar = 1600; // 160 bar
    } else if (voltage < 5000) { // lower than 0.5V (margin)
        pressure_bar = 0; // 0 bar
    } else {
        pressure_bar = (voltage - 5000) / 25; // bar x 10
    }

    return (uint16_t)pressure_bar;
}
