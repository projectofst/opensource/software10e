#ifndef _TORQUE_ENCODER_H
#define _TORQUE_ENCODER_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "can-ids-v1/CAN_IDs.h"
#include "can-ids-v1/Devices/TE_CAN.h"

#include "bsp.h"

/*! \file torque_encoder.h
    \brief This file contains the initial thresholds to be stored at the EEPROM and structs to be used in all files of the project.
*/

#define APPS_0_GND_THRS 410
#define APPS_0_VCC_THRS 3694
#define APPS_0_ZERO_FORCE_THRS 850
#define APPS_0_MAX_FORCE_THRS 2950

#define APPS_1_GND_THRS 238
#define APPS_1_VCC_THRS 3856
#define APPS_1_ZERO_FORCE_THRS 1550
#define APPS_1_MAX_FORCE_THRS 3450

#define BPS_ELECTRIC_GND_THRS 410
#define BPS_ELECTRIC_VCC_THRS 3694
#define BPS_ELECTRIC_ZERO_FORCE_THRS 800
#define BPS_ELECTRIC_MAX_FORCE_THRS 1100

#define BPS_PRESSURE_0_GND_THRS 300
#define BPS_PRESSURE_0_VCC_THRS 3600
#define BPS_PRESSURE_0_ZERO_FORCE_THRS 400
#define BPS_PRESSURE_0_MAX_FORCE_THRS 3000

#define BPS_PRESSURE_1_GND_THRS 300
#define BPS_PRESSURE_1_VCC_THRS 3600
#define BPS_PRESSURE_1_ZERO_FORCE_THRS 400
#define BPS_PRESSURE_1_MAX_FORCE_THRS 3000

#define HARD_BRAKING_THRS 300 // 30 bar

#define NUMBER_OF_AVERAGE_POINTS 3
#define NUMBER_OF_COUNTERS_APPS_IMP 2
#define NUMBER_OF_COUNTERS_APPS_BPS_IMP 20 // 500ms / 25 ms =  20

#define TRIS_LED_THR TRISBbits.TRISB12
#define LED_THR LATBbits.LATB12

#define SHUTDOWN_DETECTION_CONFIG TRISDbits.TRISD4
#define SHUTDOWN_DET PORTDbits.RD4

/* FLAGS */

extern volatile int adc_flag;
extern volatile int timer1_flag;
extern volatile int received_can_flag;

/*! \var typedef struct COUNTERS
    \brief Stores BPS_electric, BPS_pressure and APPS values.
*/
typedef struct counters
{
    unsigned int BPS_electric;
    unsigned int BPS_pressure;
    unsigned int APPS;
    unsigned int APPS_BPS;
} COUNTERS;

/*! \var typedef struct TE_VALUES
    \brief Stores raw data, data arrays and averages for all sensors. Also, stores accelerator, brake_electric and brake_pressure values.
*/
typedef struct
{

    unsigned int BPS_electric_0[NUMBER_OF_AVERAGE_POINTS];
    unsigned int BPS_electric_1[NUMBER_OF_AVERAGE_POINTS];
    unsigned int BPS_pressure_0[NUMBER_OF_AVERAGE_POINTS];
    unsigned int BPS_pressure_1[NUMBER_OF_AVERAGE_POINTS];
    unsigned int APPS_0[NUMBER_OF_AVERAGE_POINTS];
    unsigned int APPS_1[NUMBER_OF_AVERAGE_POINTS];

    unsigned int new_BPS_electric_0;
    unsigned int new_BPS_pressure_0;
    unsigned int new_BPS_pressure_1;
    unsigned int new_APPS_0;
    unsigned int new_APPS_1;

    unsigned int avg_BPS_electric_0;
    unsigned int avg_BPS_electric_1;
    unsigned int avg_BPS_pressure_0;
    unsigned int avg_BPS_pressure_1;
    unsigned int avg_APPS_0;
    unsigned int avg_APPS_1;

    uint16_t accelerator;    // percentage
    uint16_t brake_electric; // percentage
    uint16_t brake_pressure; // percentage

    uint16_t accelerator_torque;
    uint16_t brake_electric_torque;
    uint16_t brake_pressure_torque;

    uint16_t raw_avg_APPS_0;
    uint16_t raw_avg_APPS_1;
    uint16_t raw_avg_BPS_electric;
    uint16_t raw_avg_BPS_pressure_0;
    uint16_t raw_avg_BPS_pressure_1;

    unsigned int index;

} TE_VALUES;

/*! \var typedef struct SENSOR_THRESHOLDS
    \brief Stores all sensor limits.
*/
typedef struct
{

    volatile bool new_thresholds;

    volatile uint16_t gnd;
    volatile uint16_t vcc;

    volatile uint16_t zero_force;
    volatile uint16_t max_force;

    volatile uint16_t range;

} SENSOR_THRESHOLDS;

/*! \var typedef struct TE_THRESHOLDS
    \brief Creates SENSOR_THRESHOLDS structs for all 5 sensors.
    \see SENSOR_THRESHOLDS
*/
typedef struct
{
    SENSOR_THRESHOLDS APPS_0;
    SENSOR_THRESHOLDS APPS_1;
    SENSOR_THRESHOLDS BPS_pressure_0;
    SENSOR_THRESHOLDS BPS_pressure_1;
    SENSOR_THRESHOLDS BPS_electric;
    uint16_t hard_braking_thresh;
} TE_THRESHOLDS;

/*! \var typedef enum THRESHOLD_SELECT
    \brief Selects between lower and upper mechanical limits for each of the 5 sensors.
    \see SENSOR_THRESHOLDS
*/
typedef enum
{
    APPS_0_ZERO_FORCE,
    APPS_0_MAX_FORCE,
    APPS_1_ZERO_FORCE,
    APPS_1_MAX_FORCE,
    BPS_0_ZERO_FORCE,
    BPS_0_MAX_FORCE,
    BPS_1_ZERO_FORCE,
    BPS_1_MAX_FORCE,
    BPS_ELECTRIC_ZERO_FORCE,
    BPS_ELECTRIC_MAX_FORCE,
    HARD_BRAKING_THRESHOLD,
} THRESHOLD_SELECT;

#endif
