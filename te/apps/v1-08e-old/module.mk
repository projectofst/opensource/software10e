MODULE_C_SOURCES := check_functions.c
MODULE_C_SOURCES += config_functions.c
MODULE_C_SOURCES += general_purpose_functions.c
MODULE_C_SOURCES += main.c
MODULE_C_SOURCES += send_functions.c
MODULE_C_SOURCES += update_functions.c

PWD:=apps/v1-08e-old/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk
