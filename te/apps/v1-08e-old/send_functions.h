#ifndef _SEND_FUNCTIONS_H
#define _SEND_FUNCTIONS_H

#include "torque_encoder.h"

#include "bsp.h"
#include "can-ids-v1/Devices/TE_CAN.h"
#include "can-ids-v1/Devices/COMMON_CAN.h"
#include "can-ids-v1/Devices/INTERFACE_CAN.h"

/*! \file send_functions.h
    \brief This file contains all functions that are capable of sending and receiving values via CAN.
*/

/*! \fn void send_BPS_eletric (TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message)
    \brief This function checks if brake eletric sensor isn't at a short circuit state. If so, puts the value at
    the sending struct. Otherwise, puts a zero value at the sending struct.

    \param value Pointer to the TE_VALUES struct.
    \param status Pointer to the TE_CORE struct.
    \param main_message Pointer to the TE_MESSAGE struct.
*/
void send_BPS_eletric(TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message);

/*! \fn void send_BPS_pressure (TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message)
    \brief This function puts a zero value at the sending struct, if both brake pressure sensors are at short circuit.
    If only one of the pressure sensors is at short circuit then sends the other sensor percentage value.
    If both pressure sensors aren't at a short circuit state, then sends the average of the two percentages.

    \param value Pointer to the TE_VALUES struct.
    \param status Pointer to the TE_CORE struct.
    \param main_message Pointer to the TE_MESSAGE struct.
*/
void send_BPS_pressure(TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message);

/*! \fn void send_APPS (TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message)
    \brief This struct sends the average of the two percentage
    values of throttle sensors if the throttle sensors aren't at a short circuit state, if it isn't detected an implausibility
    event between the throttle sensors for more than 100ms, if it isn't detected an implausibility event
    between accelerator and brake sensors and if both pressure sensors aren't at a short circuit state.
    Otherwise, sends zero.

    \param value Pointer to the TE_VALUES struct.
    \param status Pointer to the TE_CORE struct.
    \param main_message Pointer to the TE_MESSAGE struct.
*/
void send_APPS(TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message);

/*! \fn void send_status (TE_CORE *status, TE_MESSAGE *main_message)
    \brief This function sends the status struct via CAN.

    \param status Pointer to the TE_CORE struct.
    \param main_message Pointer to the TE_MESSAGE struct.
*/
void send_status(TE_CORE *status, TE_MESSAGE *main_message);

/*! \fn void send_debbug_message (TE_VALUES *value, TE_DEBUG_MESSAGE *debug_message)
    \brief This function puts at a sending structure the values of the two throttle sensors and of
    the two brake pressure sensors.
    \param value Pointer to the TE_VALUES struct.
    \param debug_message Pointer to the TE_DEBUG_MESSAGE struct.
*/

/*! \fn void send_messages (TE_VALUES *value, TE_CORE *status, COUNTERS *time_counters)
    \brief This function is responsible for preparing the main CAN message to be sent.

    \param value Pointer to the TE_VALUES struct.
    \param status Pointer to the TE_CORE struct.
    \param time_counters Pointer to the COUNTERS struct.
    \see  send2CAN()
*/
void send_messages(TE_VALUES *value, TE_THRESHOLDS *thresholds, TE_CORE *status);
/*! \fn void send_init_msg (void)
    \brief This function sends a initialization message via CAN in order to gives us information
    about when the Torque Encoder started sending values via CAN and/or if the Torque Encoder was rebooted.
*/
void send_init_msg(void);

/*! \fn void process_can_message (CANdata *r_message,
                             TE_CORE *status,
                             TE_VALUES *values,
                             _prog_addressT EE_address,
                             uint16_t EE_thresholds[16],
                             TE_THRESHOLDS *thresholds)
    \brief This function is responsible for executing the correct function capable of dealing with the information just received.
    \param r_message Pointer to the CANdata struct.
    \param status Pointer to the TE_CORE struct.
    \param values Pointer to the TE_VALUES struct.
    \param EE_address EEPROM address.
    \param EE_thresholds Values to be stored at EEPROM.
    \param thresholds Pointer to the TE_THRESHOLDS.
*/

void process_can_message(CANdata *r_message,
                         TE_CORE *status,
                         TE_VALUES *values,
                         TE_THRESHOLDS *thresholds,
                         uint16_t new_thresh[7]);

/*! \fn void send_BPS_pressure_0_analog (TE_CORE *status)
    \brief This function turns a led on if the brake pressure 0 is at a short circuit state.
    Otherwise, it turns the led off.

    \param status Pointer to the TE_CORE struct.
    
*/
void send_BPS_pressure_0_analog(TE_CORE *status);

/*! \fn void send2CAN (TE_MESSAGE *main_message, TE_DEBUG_MESSAGE *debug_message)
    \brief This function is responsible for sending the main message and depending
    of the debug mode bit, this function can also send the debug message.
    \param main_message Pointer to the TE_MESSAGE struct.
    \param debug_message Pointer to the TE_DEBUG_MESSAGE struct.
*/
void send2CAN(TE_MESSAGE *main_message);

/*! \fn void send_raw_values(TE_VALUES *values, COUNTERS *time_counters, TE_CORE *status)
    \brief This function is responsible for sending two raw messages via CAN. The first raw
    message has the raw average values of the two throttle sensors and the two brake pressure sensors.
    The second raw message has the raw average value of the brake eletric sensor and information
    relative to the implausibility time between APPS0 and APPS1.
    \param values Pointer to the TE_VALUES struct.
    \param time_counters Pointer to the COUNTERS struct.
    \param status Pointer to the TE_CORE struct.
*/
void send_verbose_values(TE_VALUES *values, TE_THRESHOLDS *thresholds, TE_CORE *status);

/*! \fn void send_TE_limit(TE_LIMIT *limit_message)
    \brief This function is responsible of sending the requested thresholds of the requested sensor via CAN.
    \param Pointer to the TE_LIMIT struct.
*/
void send_TE_limit(TE_LIMIT *limit_message);

/*! \fn uint16_t get_limit(SENSOR_THRESHOLDS thresholds, TE_THRESHOLD_LIMIT limit_type )
    \brief This function is responsible of getting the requested threshold.
    \param threshold Selects one of the sensor limits.
    \param limit_type Selects one of the GND, VCC, ZERO_FORCE, MAX_FORCE, LOWER_MECHANICAL or UPPER_MECHANICAL limits.
    \return The requested threshold.
*/
uint16_t get_limit(SENSOR_THRESHOLDS thresholds, TE_THRESHOLD_LIMIT limit_type);

/*! \fn void find_request_limit (TE_THRESHOLD_LIMIT limit_type, SENSOR_SELECT sensor, TE_THRESHOLDS *thresholds)
    \brief This function gets the requested threshold of the requested sensor.
    \param limit_type Selects one of the GND, VCC, ZERO_FORCE, MAX_FORCE, LOWER_MECHANICAL or UPPER_MECHANICAL limits.
    \param sensor Selects one of the 5 sensors.
    \param thresholds Pointer to the TE_THRESHOLDS struct.
*/
void find_request_limit(TE_THRESHOLD_LIMIT limit_type, SENSOR_SELECT sensor, TE_THRESHOLDS *thresholds);

/*! \fn bool filter_can2(uint16_t sid)
    \brief This function enables or disables the messages that can be received by the Torque Encoder sent by CAN.
    \param sid Device identifier number.
    \return Boolean.
*/
bool filter_can2(unsigned sid);

void send_ack(int DEVICE_ID, int PARAMETER_ID, uint16_t data);

void send_version(int version_id_num);

void send_main_forces(TE_VALUES *values);

void send_brake_pressure_bar(TE_VALUES *values, TE_CORE *status);

#endif
