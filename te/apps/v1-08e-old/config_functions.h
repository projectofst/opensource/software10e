#ifndef _CONFIG_FUNCTIONS_H_
#define _CONFIG_FUNCTIONS_H_

#include <xc.h>

#include "torque_encoder.h"

/*! \file config_functions.h
    \brief This file contains configuration functions.
*/

/*! \fn void init_adc (void)
	\brief This function is responsible of configuring the ADC module. 
	It has been considered that AN3 is connected to the APPS0 sensor; AN4 is connected to the BPS_pressure_1 sensor;
	AN10 is connected to the BPS_eletric_0 sensor and AN11 is connected to the BPS_pressure_0 sensor.	
*/

/*! \fn void init_thresholds (_prog_addressT EE_address, TE_THRESHOLDS *thresholds, uint16_t EE_thresholds[16])
	\brief This function is responsible for writing at the TE_THRESHOLDS struct the data stored in the EEPROM.
	\param EE_address EEPROM adress.
	\param thresholds Pointer to the TE_THRESHOLDS struct.
	\param EE_thresholds[16] Values to be stored at EEPROM.
	\see init_mechanical_thresholds()
*/
void init_thresholds(TE_THRESHOLDS *thresholds, uint16_t EE_thresholds[11], uint16_t new_thesh[11]);

#endif