#include <stdbool.h>
#include <stdint.h>
#include <xc.h>

#include "bsp.h"
#include "check_functions.h"
#include "config_functions.h"
#include "general_purpose_functions.h"
#include "send_functions.h"
#include "torque_encoder.h"
#include "update_functions.h"

#include "can-ids-v1/CAN_IDs.h"
#include "can-ids-v1/Devices/TE_CAN.h"

/**********************************************************************************/
//                              TORQUE ENCODER
//
/**********************************************************************************/

/*Flags*/

volatile int timer1_flag = 0;
volatile int received_can_flag = 0;

uint16_t stored_EE_thresholds[11] = { 0 };

void timer1_callback(void)
{
    timer1_flag = 1;
    return;
}

void timer2_callback(void)
{
    if (received_can_flag) {
        received_can_flag = 0;
        LED_THR ^= 1;
    } else {
        LED_THR ^= 1;
    }
}

/* Returns DEVICE ID of the Torque Encoder */
unsigned int device_id(void)
{
    return DEVICE_ID_TE;
}

int main(void)
{

    /* Structs initialization*/
    TE_CORE status;
    TE_VALUES value = { 0 };
    COUNTERS timer_counter;
    TE_THRESHOLDS threshold;

    CANdata received_message = { 0 };
    bool msg_check;
    uint16_t adc_data[N_SENSORS];

    TRISEbits.TRISE0 = 0;
    LATEbits.LATE0 = 1;

    uint16_t new_thresholds[11] = { 0 };

    status.Verbose_Mode = true;

    bsp_config_can_essential_buffer();

    send_init_msg();

    // Binds the EE address to the EE data vector and loads the EE with the defined values
    bsp_mem_config();

    // Initializes threshold structure with defined values and EE values
    init_thresholds(&threshold, stored_EE_thresholds, new_thresholds);

    bsp_init_adc();
    config_timer1(25, 4);
    config_timer2(500, 6); // 1 HZ

    value.index = 0;

    while (1) {
        // printf("André ZyboWare Master\n");

        acquire_values(&value, adc_data);

        proccess_values(&value, &status, &threshold, &timer_counter, timer1_flag);

        bsp_send_can_essential_buffer();
        msg_check = bsp_pop_can_essential_buffer(&received_message);

        if (msg_check == true) {
            process_can_message(&received_message,
                &status,
                &value,
                &threshold,
                new_thresholds);
        }

        ClrWdt();
        Idle();
    }

    return 0;
}
