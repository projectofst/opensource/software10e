#ifndef _UPDATE_FUNCTIONS_H
#define _UPDATE_FUNCTIONS_H

#include "torque_encoder.h"

#include "can-ids-v1/Devices/TE_CAN.h"

/*! \file update_functions.h
    \brief This file contains all functions that are capable of changing values stored in data structs during the 
execution of the code. 
*/

/*! \var typedef enum POSITION_SELECT
    \brief Selects one of the three possible positions of the pedal. 
*/
typedef enum
{
    POSITION_SELECT_SHORT_CIRCUIT,
    POSITION_SELECT_OVERSHOOT,
    POSITION_SELECT_NORMAL
} POSITION_SELECT;

/*! \var typedef enum SAFE_ZONE_SELECT
    \brief Selects between lower safe zone and upper safe zone. 
*/
typedef enum
{
    SAFE_ZONE_SELECT_LOWER,
    SAFE_ZONE_SELECT_UPPER
} SAFE_ZONE_SELECT;

/*! \fn void update_selected_mechanical_thresholds(INTERFACE_MSG_PEDAL_THRESHOLDS select, 
                                            TE_VALUES *value,
                                            _prog_addressT EE_address, 
                                            uint16_t new_mechanical_thresholds[16],
                                            TE_THRESHOLDS *thresholds)
    \brief Updates upper mechanical and/or lower mechanical thresholds for brake or accelerator pedals. 
    These new mechanical thresholds are the raw values read from the sensors at the moment this function is executed.
    Writes these new limits at EEPROM. 

    \param select Selects mechanical threshold.
    \param value Pointer to the TE_VALUES struct.
    \param EE_address EEPROM address.
    \param new_mechanical_thresholds 
    \param thresholds Pointer to the TE_THRESHOLDS struct.
    \see check_threshold_overlap ()
*/
void update_selected_thresholds(INTERFACE_MSG_PEDAL_THRESHOLDS select,
                                TE_VALUES *value,
                                TE_THRESHOLDS *thresholds,
                                uint16_t new_thresholds[11]);

/*! \fn void check_threshold_overlap (SAFE_ZONE_SELECT safe_zone, SENSOR_THRESHOLDS *thresholds, uint16_t *raw_average)
    \brief For the chosen sensor this function checks if its lower mechanical limit is lower than its GND limit. 
    If so, updates the lower mechanical limit with the value immediately above of GND limit.
    Also checks if its upper mechanical threshold is greater than its VCC limit. If so, updates its upper mechanical
    threshold with the immediately preceding value of the VCC limit.

    \param safe_zone The type of safe zone [lower safe zone or upper safe zone]. 
    \param thresholds Pointer to the SENSOR_THRESHOLDS struct.
    \param raw_average The raw average of one of the sensors. 
*/
void check_threshold_overlap(SAFE_ZONE_SELECT safe_zone, SENSOR_THRESHOLDS *thresholds, uint16_t *raw_average);

/*! \fn void init_mechanical_thresholds (SENSOR_THRESHOLDS *thresholds)
    \brief  For the chosen sensor this function checks if its lower mechanical limit is lower than its GND limit. 
    If so, updates the lower mechanical limit with the value immediately above of GND limit.
    Also checks if its upper mechanical limit is greater than its VCC limit. If so, updates its upper mechanical
    limit with the immediately preceding value of the VCC limit.

    \param thresholds Pointer to the SENSOR_THRESHOLDS. 
*/
void init_mechanical_thresholds(SENSOR_THRESHOLDS *thresholds);

/*! \fn void update_thresholds(INTERFACE_MSG_PEDAL_THRESHOLDS select, TE_THRESHOLDS *thresholds)
    \brief Updates all thresholds (gnd, lower mechanical, zero force, max force, upper mechanical and vcc) 
    and range for the chosen sensor. These new limits are given by the interface and are sent via CAN. 

    \param select Selects mechanical threshold.
    \param thresholds Pointer to the TE_THRESHOLDS struct.
*/
void update_thresholds(INTERFACE_MSG_PEDAL_THRESHOLDS select, TE_THRESHOLDS *thresholds);

/*! \fn void update_values (TE_VALUES *value)
    \brief Updates 5 arrays by adding one new value for each and removing the oldest one.
    \param value Pointer to the TE_VALUES struct.
*/
void update_values(TE_VALUES *value);

/*! \fn void update_raw_averages (TE_VALUES *value)
    \brief Updates all sensor averages using the values stored in the arrays corresponding to each sensor. 
    \param value Pointer to the TE_VALUES struct.
*/
void update_raw_averages(TE_VALUES *value);

/*! \fn void update_treated_averages (TE_VALUES *value, TE_THRESHOLDS *thresholds, TE_CORE *status)
    \brief Updates the pertentage values for all 5 sensors. Updates accelerator, brake pressure and brake eletric values.
    \param value Pointer to the TE_VALUES struct.
    \param thresholds Pointer to the TE_THRESHOLDS struct.
    \param status Pointer to the TE_CORE struct.
    \see update_sensor_value()
*/
void update_treated_averages(TE_VALUES *value, TE_THRESHOLDS *thresholds, TE_CORE *status);

/*! \fn void update_sensor_value (SENSOR_SELECT sensor,
                          uint16_t *course, 
                          SENSOR_THRESHOLDS *thresholds, 
                          TE_CORE *status)
    \brief Subtracts the lower bound (zero force threshold) from the raw average, calculates 
    the percentage for all 5 sensors and updates the sensor status (Short circuit, Normal or Overshoot state)
    \param sensor Selects one of the five sensors.
    \param course The average value of one sensor.
    \param thresholds Pointer to the SENSOR_THRESHOLDS struct.
    \param status Pointer to the TE_CORE struct.
    \see update_sensor_status()
*/
void update_sensor_value(SENSOR_SELECT sensor,
                         uint16_t *course,
                         SENSOR_THRESHOLDS *thresholds,
                         TE_CORE *status);

/*! \fn void update_sensor_status (SENSOR_SELECT sensor, POSITION_SELECT position ,TE_CORE *status)
    \brief For each sensor updates its status bits.
    \param sensor Selects one of the five sensors.
    \param position Selects one of the three possible positions of the pedal. 
    \param status Pointer to the TE_CORE struct.
*/
void update_sensor_status(SENSOR_SELECT sensor, POSITION_SELECT position, TE_CORE *status);

uint16_t convert_voltage_to_bar(unsigned int value);

uint16_t update_torque_value(uint16_t value);

void update_ranges(TE_THRESHOLDS *thresholds);

#endif