#ifndef _GENERAL_PURPOSE_FUNCTIONS_H
#define _GENERAL_PURPOSE_FUNCTIONS_H

#include "bsp.h"
#include "torque_encoder.h"

/*! \file general_purpose_functions.h
    \brief This file contains toggle functions, acquire and process values functions.
*/

/*! \fn void acquire_values  (volatile bool *flag, TE_VALUES *values)
  \brief This function gets the raw values of the sensors from the buffers of the ADC.
  \param flag ADC flag.
  \param values Pointer to the TE_VALUES struct.
*/

void acquire_values(TE_VALUES *values, uint16_t *adc_data);

/*! \fn void proccess_values (volatile bool *flag,
                      TE_VALUES *values,
                      TE_CORE *status,
                      TE_THRESHOLDS *thresholds,
                      COUNTERS *time_counters)
    \brief This function is only executed when the timer 1 flag is set to one.
    It executes the following steps: disables interruptions; updates raw and treated values; executes multiple integrity checks;
    sends data via CAN and enables interruptions.
    \param flag Pointer to the timer 1 flag.
    \param values Pointer to the TE_VALUES struct.
    \param status Pointer to the TE_CORE struct.
    \param thresholds Pointer to the TE_THRESHOLDS struct.
    \param time_counters Pointer to the COUNTERS struct.
*/
void proccess_values(TE_VALUES *values,
                     TE_CORE *status,
                     TE_THRESHOLDS *thresholds,
                     COUNTERS *time_counters,
                     volatile int timer1_flag);

/*! \fn void toggle_interrupts (INTERRUPT_TOGGLE toggle)
  \brief This function toggles the enable interruption bit.
  \param toggle
  \see toggle_adc_int()
  \see toggle_timer1_int()
*/
void toggle_interrupts(INTERRUPT_TOGGLE toggle);

/*! \fn void toggle_adc_int (INTERRUPT_TOGGLE toggle)
  \brief This function toggles the ADC enable interruption bit.
  \param toggle ??
*/
void toggle_adc_int(INTERRUPT_TOGGLE toggle);

/*! \fn void toggle_timer1_int (INTERRUPT_TOGGLE toggle)
  \brief This function toggles the timer 1 enable interruption bit.
  \param toggle ??
*/
void toggle_timer1_int(INTERRUPT_TOGGLE toggle);

void toggle_timer2_init(INTERRUPT_TOGGLE toggle);

#endif
