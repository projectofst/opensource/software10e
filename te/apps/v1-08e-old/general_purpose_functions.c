#include <xc.h>

#include "general_purpose_functions.h"

#include "check_functions.h"
#include "send_functions.h"
#include "update_functions.h"

void acquire_values(TE_VALUES* values, uint16_t* adc_data)
{
    if (bsp_new_values()) {

        bsp_turn_interrupts_off();

        values->new_APPS_0 = bsp_get_apps0();
        values->new_APPS_1 = bsp_get_apps1();
        values->new_BPS_pressure_0 = bsp_get_bps0();
        values->new_BPS_pressure_1 = bsp_get_bps1();
        values->new_BPS_electric_0 = bsp_get_be();

        bsp_set_flag_0();
        update_values(values);

        bsp_turn_interrupts_on();
    }

    return;
}

/*
*********************************************************************
|   Name:           proccess_values
|
|   Description:
|   Priority:       -
|
|   Arguments:      Pointer to the structure containing the values
|                   Pointer to the structure containing the status
|   Returns:        -
|
|   Tested:         No
*********************************************************************
*/

void proccess_values(TE_VALUES* values, TE_CORE* status, TE_THRESHOLDS* thresholds, COUNTERS* time_counters, volatile int timer1_flag)
{

    if (timer1_flag) {

        bsp_turn_interrupts_off();

        update_raw_averages(values);

        update_treated_averages(values, thresholds, status);
        check_master(values, thresholds, status, time_counters);
        send_messages(values, thresholds, status);

        timer1_flag = 0;

        bsp_turn_interrupts_on();
    }
    return;
}