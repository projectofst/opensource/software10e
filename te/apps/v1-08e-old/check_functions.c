#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

#include "bsp.h"
#include "check_functions.h"
#include "torque_encoder.h"
#include "update_functions.h"

/*
***************************************************************************************************************
|   Name:           check_implausibility
|
|   Description:    Checks if the absolute difference between APPS0 and APPS1 differ more than 1000 points.
|                   If implausibility is detected then Implausibility_APPS flag is set to 1, otherwise
|                 it is set to 0.
|
|   Priority:       -
|
|   Arguments:      Pointer to the structure containing the values
|                   Pointer to the status structure
|
|   Returns:        -
|
|   Tested:         YES
***************************************************************************************************************
*/

void check_implausibility(TE_VALUES* value, TE_CORE* status)
{
    if (abs(value->avg_APPS_0 - value->avg_APPS_1) >= 1000) {
        status->Implausibility_APPS = 1;
    } else {
        status->Implausibility_APPS = 0;
    }
    return;
}

/*
*********************************************************************
|   Name:           find
|
|   Description:    From two values:
|                       -Returns the smallest if it is an APPS
|                       -Returns the biggest if it is a BPS
|
|   Priority:       -
|
|   Arguments:      The value of two signals and the selection "bit"
|
|   Returns:        (In the description)
|
|   Tested:         YES
*********************************************************************
*/

unsigned int hard_break_detection(TE_VALUES* value, TE_THRESHOLDS* thresholds, TE_CORE* status)
{
    if (convert_voltage_to_bar(value->raw_avg_BPS_pressure_0) > thresholds->hard_braking_thresh) { // compares with 30 bar (if not changed)
        status->Hard_braking = 1;
        return 1;
    } else {
        status->Hard_braking = 0;
        return 0;
    }
}

/*
*******************************************************************************************
|   Name:           increment_counter
|
|   Description:    This function is executed every 25ms.
|                   Counts the number of times that in these periods of 25ms, implausibility
|               between APPS sensors is detected. If implausibility between APPS sensors
|               isn't detected then reset the counter.
|
|   Works with:     check_counter function
|
|   Priority:       -
|
|   Arguments:      variable that
|
|   Returns:        Number of periods of 25ms that implausibility between APPS sensors is
|                   detected.
|
|   Tested:         YES
********************************************************************************************
*/
void increment_counter(TE_CORE* status, COUNTERS* timer_counter)
{

    if (status->Implausibility_APPS == 1) {
        timer_counter->APPS++;
    } else if (timer_counter->APPS > 0) {
        timer_counter->APPS = 0;
    }

    if (status->Implausibility_APPS_BPS == 1) {
        timer_counter->APPS_BPS++;
    } else if (timer_counter->APPS_BPS > 0) {
        timer_counter->APPS_BPS = 0;
    }

    return;
}

void check_counter(TE_CORE* status, COUNTERS* timer_counter)
{

    if (status->Implausibility_APPS_BPS && timer_counter->APPS_BPS > NUMBER_OF_COUNTERS_APPS_BPS_IMP) {
        status->Implausibility_APPS_BPS_Timer_Exceeded = 1; // APPS BPS implausibility time >= 500ms
    } else if (status->Implausibility_APPS_BPS_Timer_Exceeded && status->Imp_APPS_5) {
        status->Implausibility_APPS_BPS_Timer_Exceeded = 1;
    } else {
        status->Implausibility_APPS_BPS_Timer_Exceeded = 0;
    }

    if (status->Implausibility_APPS && timer_counter->APPS > NUMBER_OF_COUNTERS_APPS_IMP) {
        status->Implausibility_APPS_Timer_Exceeded = 1;
    } else {
        status->Implausibility_APPS_Timer_Exceeded = 0;
    }

    return;
}

void check_time(TE_CORE* status, COUNTERS* timer_counter)
{
    increment_counter(status, timer_counter);

    check_counter(status, timer_counter);

    return;
}

/*
*******************************************************************************************
|   Name:           check_master
|
|   Description:    Checks APPS implausibility, APPS implausibility time exceed,
|                  APPS and BPPS plausibility and shutdown_circuit.
|
|   Works with:     check_counter function
|
|   Priority:       -
|
|   Arguments:      variable that
|
|   Returns:        Number of periods of 25ms that implausibility between APPS sensors is
|                   detected.
|
|   Tested:         YES
********************************************************************************************
*/

void check_master(TE_VALUES* value, TE_THRESHOLDS* thresholds, TE_CORE* status, COUNTERS* timer_counter)
{
    check_implausibility(value, status);

    check_APPS_BPPS_plausibility(value, thresholds, status);

    check_time(status, timer_counter);

    check_shut_circuit(status);

    return;
}

void check_APPS_BPPS_plausibility(TE_VALUES* value, TE_THRESHOLDS* thresholds, TE_CORE* status)
{

    hard_break_detection(value, thresholds, status);

    if (value->accelerator_torque > 500) { // regardless of whether the brakes are still actuated or not
        status->Imp_APPS_5 = 1;
    } else {
        status->Imp_APPS_5 = 0;
    }
    if (value->accelerator_torque > 2500 && status->Hard_braking) { // if hard braking and the APPS signals pedal travel equivalent to ≥25 % desired motor torque
        status->Implausibility_APPS_BPS = 1;
    } else {
        status->Implausibility_APPS_BPS = 0;
    }
}

///////////////////////////////////////////////////////////////////////////////////////

/*
*********************************************************************
|   Name:           check_AIR_line
|
|   Description:
|   Priority:       -
|
|   Arguments:      Pointer to the structure containing the values
|   Returns:        -
|
|   Tested:         No
*********************************************************************
*/

void check_shut_circuit(TE_CORE* status)
{
    if (SHUTDOWN_DET)
        status->SC_Motor_Interlock_Front = 1;
    else
        status->SC_Motor_Interlock_Front = 0;
    return;
}

/*
*********************************************************************
|   Name:           check_TE_RTD_status
|
|   Description:    If everything is okay with the car, throttle pedal isn't pressed,
|                 and brake pedal is pressed then READY-TO-DRIVE flag is set to 1.
|                   Otherwise, the READY-TO-DRIVE flag is set to 0.
|   Priority:       -
|
|   Arguments:      Pointer to the structure containing the values
|                   Pointer to the structure containing the status
|   Returns:        -
|
|   Tested:         No
*********************************************************************
*/

bool check_TE_RTD_status(TE_CORE* status, TE_VALUES* value)
{

    if (status->CC_APPS_0 == false && status->CC_APPS_1 == false && status->CC_BPS_electric_0 == false && status->CC_BPS_pressure_0 == false && status->Implausibility_APPS_Timer_Exceeded == false && status->Implausibility_APPS_BPS_Timer_Exceeded == false && value->accelerator_torque < 500 && status->Hard_braking == true) {

        return true;
    } else
        return false;
}

void update_hard_braking_limit(TE_THRESHOLDS* thresholds, uint16_t pressure, uint16_t new_thresholds[11])
{

    thresholds->hard_braking_thresh = pressure;

    new_thresholds[HARD_BRAKING_THRESHOLD] = thresholds->hard_braking_thresh;

    bsp_write_memory(new_thresholds, sizeof(new_thresholds));
    return;
}
