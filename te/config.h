#ifndef __CONFIG_H__
#define __CONFIG_H__

#define VIO_PORT 8888
#define VIO_HOST "127.0.0.1"

#define VIO_ADC1_CONFIG {{"apps0", 0}, {"apps1", 0}, {"pressure0", 0}, {"pressure1", 0}, {"eletric", 0}, {NULL, 0}}

#define CAN_ESSENTIAL	57005
#define CAN_SENSOR		48879
#define CAN_AMK			43962

#define CAN1_SOCKET_CAN "vcan0"
#define CAN2_SOCKET_CAN "vcan1"
#define PORT1			CAN_SENSOR
#define PORT2			CAN_ESSENTIAL

#define	SIM_DELTA_T_US	100
#endif
