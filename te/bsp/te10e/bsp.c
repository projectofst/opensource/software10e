// DSPIC30F6012A Configuration Bit Settings

// 'C' source line config statements

// FOSC
#pragma config FOSFPR = HS2_PLL8      // Oscillator (HS2 w/PLL 8x)
#pragma config FCKSMEN = CSW_FSCM_OFF // Clock Switching and Monitor (Sw Disabled, Mon Disabled)

// FWDT
#pragma config FWPSB = WDTPSB_16  // WDT Prescaler B (1:16)
#pragma config FWPSA = WDTPSA_512 // WDT Prescaler A (1:512)
#pragma config WDT = WDT_OFF      // Watchdog Timer (Disabled)

// FBORPOR
#pragma config FPWRT = PWRT_OFF // POR Timer Value (Timer Disabled)
#pragma config BODENV = NONE    // Brown Out Voltage (Reserved)
#pragma config BOREN = PBOR_OFF // PBOR Enable (Disabled)
#pragma config MCLRE = MCLR_EN  // Master Clear Enable (Enabled)

// FBS
#pragma config BWRP = WR_PROTECT_BOOT_OFF // Boot Segment Program Memory Write Protect (Boot Segment Program Memory may be written)
#pragma config BSS = NO_BOOT_CODE         // Boot Segment Program Flash Memory Code Protection (No Boot Segment)
#pragma config EBS = NO_BOOT_EEPROM       // Boot Segment Data EEPROM Protection (No Boot EEPROM)
#pragma config RBS = NO_BOOT_RAM          // Boot Segment Data RAM Protection (No Boot RAM)

// FSS
#pragma config SWRP = WR_PROT_SEC_OFF // Secure Segment Program Write Protect (Disabled)
#pragma config SSS = NO_SEC_CODE      // Secure Segment Program Flash Memory Code Protection (No Secure Segment)
#pragma config ESS = NO_SEC_EEPROM    // Secure Segment Data EEPROM Protection (No Segment Data EEPROM)
#pragma config RSS = NO_SEC_RAM       // Secure Segment Data RAM Protection (No Secure RAM)

// FGS
#pragma config GWRP = GWRP_OFF // General Code Segment Write Protect (Disabled)
#pragma config GCP = GSS_OFF   // General Segment Code Protection (Disabled)

// FICD
#pragma config ICS = ICS_PGD // Comm Channel Select (Use PGC/EMUC and PGD/EMUD)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>

#include "bsp.h"
#include "lib_pic30f/adc.h"


volatile int adc_flag = 0;

_prog_addressT EE_address;

uint16_t _EEDATA(32) EE_thresholds_init[11] = {
    APPS_0_ZERO_FORCE_THRS,
    APPS_0_MAX_FORCE_THRS,
    APPS_1_ZERO_FORCE_THRS,
    APPS_1_MAX_FORCE_THRS,
    BPS_PRESSURE_0_ZERO_FORCE_THRS,
    BPS_PRESSURE_0_MAX_FORCE_THRS,
    BPS_PRESSURE_1_ZERO_FORCE_THRS,
    BPS_PRESSURE_1_MAX_FORCE_THRS,
    BPS_ELECTRIC_ZERO_FORCE_THRS,
    BPS_ELECTRIC_MAX_FORCE_THRS,
    HARD_BRAKING_THRS,
};

void adc_callback(void)
{
    adc_flag = 1;
    return;
}

void bsp_init_adc(void)
{
    ADC_parameters config;

    config.idle = 1;
    config.form = 0;
    config.conversion_trigger = AUTO_CONV;
    config.sampling_type = AUTO;
    config.voltage_ref = INTERNAL;
    config.sample_pin_select_type = AUTO;
    config.smpi = 4;
    config.sampling_time = 31;
    config.conversion_time = 63;
    config.pin_select = AN3 | AN4 | AN9 | AN10 | AN13;
    config.interrupt_priority = 5;
    config.turn_on = 1;

    TRISBbits.TRISB3 = 1;  // AN3 input     APPS_0
    TRISBbits.TRISB4 = 1;  // AN4 input     APPS_1
    TRISBbits.TRISB9 = 1;  // AN9 input     BPS_pressure_0
    TRISBbits.TRISB10 = 1; // AN10 input    BPS_pressure_1
    TRISBbits.TRISB13 = 1; // AN11 input    BPS_electric

    config_adc(config);

    return;
}
void bsp_light_led()
{
    TRISBbits.TRISB15 = 0;
    LATBbits.LATB15 = 1;
}
void bsp_toggle_adc_int(INTERRUPT_TOGGLE toggle)
{
    if (toggle)

    {
        IEC0bits.ADIE = 1;
    }
    else
    {
        IEC0bits.ADIE = 0;
    }

    return;
}

uint16_t valid_threshold(uint16_t initial_EE_thresholds[11], uint16_t EE_thresholds[11], uint16_t position)
{
    if (EE_thresholds[position] == 000000000000)
    {
        return initial_EE_thresholds[position];
    }
    else
    {
        return EE_thresholds[position];
    }
}

void bsp_toggle_timer1_init(INTERRUPT_TOGGLE toggle)
{

    if (toggle)
    {
        IEC0bits.T1IE = 1;
    }
    else
    {
        IEC0bits.T1IE = 0;
    }
    return;
}

void bsp_toggle_timer2_init(INTERRUPT_TOGGLE toggle)
{
    if (toggle)
    {
        IEC0bits.T2IE = 1;
    }
    else
    {
        IEC0bits.T2IE = 0;
    }

    return;
}

uint16_t bsp_get_apps0()
{
    return ADCBUF0;
}

uint16_t bsp_get_apps1()
{
    return ADCBUF1;
}

uint16_t bsp_get_bps0()
{
    return ADCBUF2;
}

uint16_t bsp_get_bps1()
{
    return ADCBUF3;
}

uint16_t bsp_get_be()
{

    return ADCBUF4;
}

void bsp_turn_interrupts_on()
{
    bsp_toggle_adc_int(INTERRUPT_TOGGLE_ON);
    bsp_toggle_timer1_init(INTERRUPT_TOGGLE_ON);
    bsp_toggle_timer2_init(INTERRUPT_TOGGLE_ON);
    return;
}

void bsp_turn_interrupts_off()
{
    bsp_toggle_adc_int(INTERRUPT_TOGGLE_OFF);
    bsp_toggle_timer1_init(INTERRUPT_TOGGLE_OFF);
    bsp_toggle_timer2_init(INTERRUPT_TOGGLE_OFF);
    return;
}

void bsp_mem_config()
{
    _init_prog_address(EE_address, EE_thresholds_init);
}

void bsp_read_memory(uint16_t *EE_thresholds, int len)
{
    eeprom_read_row(EE_address, EE_thresholds);
}

void bsp_write_memory(uint16_t *new_thresholds, int len)
{
    eeprom_write_row(EE_address, new_thresholds);
}

void bsp_write_can_essential_buffer(CANdata main_msg, bool priority)
{
    write_to_can2_buffer(main_msg, priority);
}

void bsp_send_can_essential_buffer()
{
    send_can2_buffer();
}

void bsp_config_can_essential_buffer()
{
    config_can2();
}

bool bsp_pop_can_essential_buffer(CANdata *msg)
{
    if (msg == NULL)
    {
        return false;
    }

    CANdata *aux = pop_can2();
    if (aux == NULL)
    {
        return false;
    }
    *msg = *aux;
    return true;
}

bool bsp_new_values()
{
    if (adc_flag)
        return true;
    else
        return false;
}

void bsp_set_flag_0()
{
    adc_flag = 0;
}

