#ifndef _BSP_H
#define _BSP_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>


#include "lib_pic30f/can.h"
#include "lib_pic30f/version.h"
#include "src/torque_encoder.h"

#include "lib_pic30f/eeprom.h"
#include "lib_pic30f/timer.h"
#include "lib_pic30f/trap.h"
#include "can-ids-v1/Devices/TE_CAN.h"
#include "lib_pic30f/lib_pic30f.h"

#define N_SENSORS 5

typedef enum
{  
    INTERRUPT_TOGGLE_OFF,
    INTERRUPT_TOGGLE_ON
} INTERRUPT_TOGGLE;

void bsp_init_adc(void);
uint16_t valid_threshold(uint16_t initial_EE_thresholds[11], uint16_t EE_thresholds[11], uint16_t position);

void bsp_toggle_adc_int(INTERRUPT_TOGGLE toggle);
void bsp_toggle_timer1_init(INTERRUPT_TOGGLE toggle);
void bsp_toggle_timer2_init(INTERRUPT_TOGGLE toggle);

void bsp_write_can_essential_buffer(CANdata main_msg, bool priority);
void bsp_send_can_essential_buffer();

uint16_t bsp_get_apps0();
uint16_t bsp_get_apps1();
uint16_t bsp_get_bps0();
uint16_t bsp_get_bps1();
uint16_t bsp_get_be();

void bsp_turn_interrupts_on();
void bsp_turn_interrupts_off();
void bsp_read_memory(uint16_t *EE_thresholds, int len);
void bsp_write_memory(uint16_t *new_thresholds, int len);
void bsp_mem_config();
void bsp_config_can_essential_buffer();
bool bsp_pop_can_essential_buffer(CANdata *msg);
bool bsp_new_values();
void bsp_set_flag_0();

#endif
