#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "bsp.h"
#include "lib_pic30f/can.h"

static uint16_t apps0 = 0;
static uint16_t apps1 = 0;
static uint16_t bps0 = 0;
static uint16_t bps1 = 0;
static uint16_t be = 0;
#define THRS_LEN 11
#define BUFFMAX 6
#define SEND_VERBOSE 0
volatile int new_msg_flag = 0;

uint16_t  EE_thresholds_init[11] = {
    APPS_0_ZERO_FORCE_THRS,
    APPS_0_MAX_FORCE_THRS,
    APPS_1_ZERO_FORCE_THRS,
    APPS_1_MAX_FORCE_THRS,
    BPS_PRESSURE_0_ZERO_FORCE_THRS,
    BPS_PRESSURE_0_MAX_FORCE_THRS,
    BPS_PRESSURE_1_ZERO_FORCE_THRS,
    BPS_PRESSURE_1_MAX_FORCE_THRS,
    BPS_ELECTRIC_ZERO_FORCE_THRS,
    BPS_ELECTRIC_MAX_FORCE_THRS,
    HARD_BRAKING_THRS,
};

void bsp_init_adc(void)
{
    return;
}

uint16_t valid_threshold(uint16_t initial_EE_thresholds[11], uint16_t EE_thresholds[11], uint16_t position)
{
    if (EE_thresholds[position] == 0xFFFF) {
        return initial_EE_thresholds[position];
    } else
        return EE_thresholds[position];
}

void bsp_toggle_adc_int(INTERRUPT_TOGGLE toggle)
{
    return;
}

void bsp_toggle_timer1_init(INTERRUPT_TOGGLE toggle)
{
    return;
}

void bsp_toggle_timer2_init(INTERRUPT_TOGGLE toggle)
{
    return;
}

void bsp_update_adc_values(uint16_t* adc_data)
{
    // zybo doesnt need this
}

uint16_t bsp_get_apps0()
{
    return apps0;
}

uint16_t bsp_get_apps1()
{
    return apps1;
}

uint16_t bsp_get_bps0()
{
    return bps0;
}

uint16_t bsp_get_bps1()
{
    return bps1;
}

uint16_t bsp_get_be()
{
    return be;
}

void bsp_turn_interrupts_on()
{
    return;
}

void bsp_turn_interrupts_off()
{
    return;
}

void bsp_mem_config()
{
    // zybo doesn't need address config
    return;
}

void bsp_read_memory(uint16_t* EE_thresholds, int len)
{
    FILE* fp;
    char buffer[BUFFMAX];
    uint16_t aux;

    if ((fp = fopen("thresholds.txt", "r")) == NULL) {
        printf("Error opening file \n");
        for (int i = 0; i < THRS_LEN; i++) {
            EE_thresholds[i] = EE_thresholds_init[i];
        }
        return;
    } else {
        for (int i = 0; i < THRS_LEN; i++) {
            if (fscanf(fp, "%5hd", &aux) != 1) {
                fclose(fp);
                exit(-1);
            } else {
                EE_thresholds[i] = aux;
                printf("reading, %5d\n", aux);
            }
        }
    }
}

void bsp_write_memory(uint16_t* new_thresholds, int len)
{
    FILE* fp;

    if ((fp = fopen("thresholds.txt", "w")) == NULL) {
        printf("Error opening flash \n");
        exit(-1);
    } else {
        for (int i = 0; i < THRS_LEN; i++) {
            fprintf(fp, "%5hd\n", new_thresholds[i]);
        }
        fclose(fp);
    }
    return;
}

void bsp_write_can_essential_buffer(CANdata main_msg, bool priority)
{   
    write_to_can2_buffer(main_msg, priority);
}

void bsp_send_can_essential_buffer()
{
    send_can2_buffer();
}

void bsp_config_can_essential_buffer()
{
    config_can2();
}

bool bsp_pop_can_essential_buffer(CANdata* msg)
{
    TE_CAN_Data te_data = { 0 };

    if (msg == NULL) {
        return false;
    }

    CANdata* aux = pop_can2();
    if (aux == NULL) {
        return false;
    }
    *msg = *aux;
    if (msg->dev_id != DEVICE_ID_TE || (msg->msg_id != MSG_ID_TE_VERBOSE_1 && msg->msg_id != MSG_ID_TE_VERBOSE_2)) {
        new_msg_flag = 0;
        return false;
    }
    new_msg_flag = 1;
    parse_can_te(*msg, &te_data);

    if (msg->msg_id == MSG_ID_TE_VERBOSE_1) {
        apps0 = te_data.verbose_message.apps_0;
        apps1 = te_data.verbose_message.apps_1;
        bps0 = te_data.verbose_message.pressure_0;
        bps1 = te_data.verbose_message.pressure_1;
    } else if (msg->msg_id == MSG_ID_TE_VERBOSE_2) {
        be = te_data.verbose_message.electric;
    }

    return true;
}

bool bsp_new_values()
{
    if (new_msg_flag)
        return true;
    else
        return false;
}

void bsp_set_flag_0()
{
    new_msg_flag = 0;
}
