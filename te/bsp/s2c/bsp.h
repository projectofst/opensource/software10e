#ifndef _BSP_H
#define _BSP_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <xc.h>

#include "lib_pic33e/adc.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/version.h"
#include "lib_pic33e/flash.h"

#include "lib_pic33e/timer.h"
#include "lib_pic33e/trap.h"
#include "lib_pic33e/pps.h"
#include "shared/run_avg/run_avg.h"
#include "torque_encoder.h"
#define N_SENSORS 5

typedef enum
{
    INTERRUPT_TOGGLE_OFF,
    INTERRUPT_TOGGLE_ON
} INTERRUPT_TOGGLE;

typedef enum
{
    apps0,
    apps1,
    bps0,
    bps1,
    be
} TE_SENSORS;

void bsp_init_adc(void);
uint16_t valid_threshold(uint16_t initial_EE_thresholds[11], uint16_t EE_thresholds[11], uint16_t position);

void bsp_toggle_timer1_init(INTERRUPT_TOGGLE toggle);
void bsp_toggle_timer2_init(INTERRUPT_TOGGLE toggle);

void bsp_write_can_buffer(CANdata main_msg, bool priority);

uint16_t bsp_get_apps0();
uint16_t bsp_get_apps1();
uint16_t bsp_get_bps0();
uint16_t bsp_get_bps1();
uint16_t bsp_get_be();

void bsp_turn_interrupts_on();
void bsp_turn_interrupts_off();
void bsp_light_led();
void bsp_mem_config();
void bsp_send_can_essential_buffer();

void bsp_read_memory(uint16_t *EE_thresholds, int len);
void bsp_write_memory(uint16_t *new_thresholds, int len);
void bsp_config_can_essential_buffer();
bool bsp_pop_can_essential_buffer(CANdata *msg);
void bsp_update_adc_values(uint16_t *adc_data);
bool bsp_new_values();
void bsp_set_flag_0();

#endif
