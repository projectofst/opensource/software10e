// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF  // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF  // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS    // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF  // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128    // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON       // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF      // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = OFF      // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4  // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON    // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE   // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF  // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF  // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>

#include "bsp.h"
#include "lib_pic33e/adc.h"
run_avg_t run_avgs[N_SENSORS];

volatile int adc_flag = 0;

void adc_callback(void)
{
    adc_flag = 1;
    return;
}

void bsp_init_adc(void)
{
    ADC_parameters config;
    uint16_t vector = (AN4 | AN3 | AN2 | AN1 | AN5);

    // Freeing memory and setting pic pins as inputs
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 0;
    TRISBbits.TRISB4 = 0;

    LATBbits.LATB0 = 0;
    LATBbits.LATB0 = 0;
    LATBbits.LATB0 = 0;
    LATBbits.LATB0 = 0;
    LATBbits.LATB0 = 0;

    TRISBbits.TRISB0 = 1; // AN0 input     APPS_0
    TRISBbits.TRISB1 = 1; // AN1 input     APPS_1
    TRISBbits.TRISB2 = 1; // AN2 input     BPS_pressure_0
    TRISBbits.TRISB3 = 1; // AN3 input    BPS_pressure_1
    TRISBbits.TRISB4 = 1; // AN4 input    BPS_electric

    config_pin_cycling(vector, 250000UL, 64, 4, &config);
    config_adc1(config);

    CNPDB = vector;

    uint16_t i = 0;
    for (i = 0; i < N_SENSORS; i++)
    {
        run_avgs[i].max_size = 16;
        run_avg_config(&run_avgs[i]);
    }

    return;
}

void bsp_light_led()
{
    TRISEbits.TRISE0 = 0;
    LATEbits.LATE0 = 1;
}

uint16_t valid_threshold(uint16_t initial_EE_thresholds[11], uint16_t EE_thresholds[11], uint16_t position)
{
    if (EE_thresholds[position] == 0xFFFF)
    {
        return initial_EE_thresholds[position];
    }
    else
    {
        return EE_thresholds[position];
    }
}

void bsp_toggle_timer1_init(INTERRUPT_TOGGLE toggle)
{
    if (toggle)
    {
        enable_timer1();
    }
    else
    {
        disable_timer1();
    }

    return;
}

void bsp_toggle_timer2_init(INTERRUPT_TOGGLE toggle)
{
    if (toggle)
        enable_timer2();
    else
        disable_timer2();
    return;
}

void bsp_update_adc_values(uint16_t *adc_data)
{
    get_pin_cycling_values(adc_data, 0, N_SENSORS);
}

uint16_t bsp_get_apps0()
{
    return ADC1BUF5;
}

uint16_t bsp_get_apps1()
{
    return ADC1BUF1;
}

uint16_t bsp_get_bps0()
{
    return ADC1BUF2;
}

uint16_t bsp_get_bps1()
{
    return ADC1BUF3;
}

uint16_t bsp_get_be()
{
    return ADC1BUF4;
}

void bsp_write_can_essential_buffer(CANdata main_msg, bool priority)
{
    send_can1(main_msg);
}

void bsp_send_can_essential_buffer()
{
    // This function does nothing because send_can1 in s2c writes and sends the message
}

void bsp_turn_interrupts_on()
{
    turn_on_adc();
    bsp_toggle_timer1_init(INTERRUPT_TOGGLE_ON);
    bsp_toggle_timer2_init(INTERRUPT_TOGGLE_ON);
    return;
}

void bsp_turn_interrupts_off()
{
    turn_off_adc();
    bsp_toggle_timer1_init(INTERRUPT_TOGGLE_OFF);
    bsp_toggle_timer2_init(INTERRUPT_TOGGLE_OFF);
    return;
}

void bsp_mem_config()
{
    // S2C doesn't need adress config
    return;
}

void bsp_read_memory(uint16_t *EE_thresholds, int len)
{
    read_flash(EE_thresholds, 0, len);
}

void bsp_write_memory(uint16_t *new_thresholds, int len)
{
    write_flash(new_thresholds, 0, len);
}

void bsp_config_can_essential_buffer()
{
    // Unlock Pins for CAN and Output compare for PWM
    PPSUnLock;

    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP120); // CAN1TX in RP118
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI121);   // CAN1RX in RPI119
    PPSLock;
    config_can1(NULL);
}

bool bsp_pop_can_essential_buffer(CANdata *msg)
{
    if (msg == NULL)
    {
        return false;
    }
    *msg = pop_can1();
    // zybo receives the verboses but the TE shouldn't
    if (msg->dev_id == DEVICE_ID_TE) {
        return false;
    }
    return true;
}

bool bsp_new_values()
{
    if (adc_flag)
        return true;
    else
        return false;
}

void bsp_set_flag_0()
{
    adc_flag = 0;
}