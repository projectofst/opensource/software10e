#ifndef _PEDAL_ACQUIRER_H
#define _PEDAL_ACQUIRER_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "lib_pic33e/timing.h"
#include "can-ids-v2/can_ids.h"
#include "can-ids-v2/can_log.h"
#include "lib_pic33e/adc.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/version.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/trap.h"
#include "lib_pic33e/pps.h"
#include "shared/run_avg/run_avg.h"

#define N_AVG_POINTS 16
#define N_SENSORS 5

typedef enum
{
    INTERRUPT_TOGGLE_OFF,
    INTERRUPT_TOGGLE_ON
} INTERRUPT_TOGGLE;

typedef struct _pedal_t
{
    uint16_t new_apps0;
    uint16_t new_apps1;
    uint16_t new_bps0;
    uint16_t new_bps1;
    uint16_t new_be;

    uint16_t avg_apps0;
    uint16_t avg_apps1;
    uint16_t avg_bps0;
    uint16_t avg_bps1;
    uint16_t avg_be;
} pedal_t;

#endif