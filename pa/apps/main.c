// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF  // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC // Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF  // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS    // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF  // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768 // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128    // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON       // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF      // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON       // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4  // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON    // Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE   // ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF  // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF  // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
// NOTE: Always include timing.h
#include "lib_pic33e/timing.h"
#include "lib_pic33e/adc.h"

// Trap handling
#include "can-ids-v2/can_cmd.h"
#include "can-ids-v2/can_ids.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/pps.h" // added this
#include "lib_pic33e/timer.h"
#include "lib_pic33e/trap.h"
#include "shared/scheduler/scheduler.h"

#include "send_functions.h"
#include "value_acquisition.h"
#include "pedal_acquirer.h"
#include <time.h>

#define N_TASKS 3
#define N_SENSORS 5

run_avg_t run_avgs[N_SENSORS];
uint32_t timer = 0;
pedal_t values = {0};

uint16_t dev_get_id()
{
    return 1;
}

void dev_send_msg(CANdata msg)
{
}

multiple_return_t cmd_handle(uint16_t id, uint16_t arg1, uint16_t arg2, uint16_t arg3)
{

    multiple_return_t mt;

    return mt;
}

void timer1_callback(void)
{
    timer++;

    return;
}

void timer2_callback(void)
{
}

void ligh_led(void)
{

    LATEbits.LATE0 ^= 1; // flashes the LEDs in the outputs 3,5,7

    return;
}

void send_CAN_message(void)
{
    send_verbose_messages(&values);
    return;
}

void receive_CAN_message(void)
{

    if (!can1_rx_empty())
    {
        CANdata msg = pop_can1();
        printf("teste1\n%d\n", msg.msg_id);
    }

    return;
}

void init_adc()
{
    ADC_parameters config;
    uint16_t vector = (AN4 | AN3 | AN2 | AN1 | AN5);

    // Freeing memory and setting pic pins as inputs
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 0;
    TRISBbits.TRISB4 = 0;

    LATBbits.LATB0 = 0;
    LATBbits.LATB0 = 0;
    LATBbits.LATB0 = 0;
    LATBbits.LATB0 = 0;
    LATBbits.LATB0 = 0;

    TRISBbits.TRISB0 = 1; // AN0 input     APPS_0
    TRISBbits.TRISB1 = 1; // AN1 input     APPS_1
    TRISBbits.TRISB2 = 1; // AN2 input     BPS_pressure_0
    TRISBbits.TRISB3 = 1; // AN3 input    BPS_pressure_1
    TRISBbits.TRISB4 = 1; // AN4 input    BPS_electric

    config_pin_cycling(vector, 250000UL, 64, 4, &config);
    config_adc1(config);

    CNPDB = vector;

    uint16_t i = 0;
    for (i = 0; i < N_SENSORS; i++)
    {
        run_avgs[i].max_size = 16;
        run_avg_config(&run_avgs[i]);
    }

    return;
}
int main()
{

    tasks_t pic33_tasks[N_TASKS] = {
        {.period = 1000, .func = ligh_led},
        {.period = 5, .func = send_CAN_message},
        {.period = 0, .func = receive_CAN_message},

    };

    scheduler_init(pic33_tasks, N_TASKS);

    init_adc();
    config_timer1(1, 4);
    config_timer2(500, 6); // 1 HZ

    // Init pins E1,E2,E3 as inputs to protect pic
    LATEbits.LATE1 = 1;
    LATEbits.LATE2 = 1;
    LATEbits.LATE3 = 1;

    double sensors[N_SENSORS][N_AVG_POINTS] = {0};
    run_avg_t run_avg[N_SENSORS] = {
        {.max_size = N_AVG_POINTS, .buffer = sensors[0]},
        {.max_size = N_AVG_POINTS, .buffer = sensors[1]},
        {.max_size = N_AVG_POINTS, .buffer = sensors[2]},
        {.max_size = N_AVG_POINTS, .buffer = sensors[3]},
        {.max_size = N_AVG_POINTS, .buffer = sensors[4]},
    };
    int i;
    for (i = 0; i < N_SENSORS; i++)
    {
        run_avg_config(&(run_avg[i]));
    }

    // RGB LED Configuration
    TRISEbits.TRISE0 = 0; // Red
    LATEbits.LATE0 = 1;

    // CAN
    PPSUnLock;
    // Output configuration based in the README
    PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP120);
    // Input configuration based in the README
    PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI121);
    PPSLock;

    config_can1(NULL); // IN DEFAULT -> default_can_config (it means that it will work)

    send_init_msg();

    while (1)
    {

        scheduler(timer);

        acquire_values(&values);
        compute_averages(&values, run_avg);
        // Clear watchdog timer
        ClrWdt();
    } // end while

    return 0;
} // end main

void trap_handler(TrapType type)
{

    switch (type)
    {
    case HARD_TRAP_OSCILATOR_FAIL:
        break;
    case HARD_TRAP_ADDRESS_ERROR:
        break;
    case HARD_TRAP_STACK_ERROR:
        break;
    case HARD_TRAP_MATH_ERROR:
        break;
    case CUSTOM_TRAP_PARSE_ERROR:
        break;
    default:
        break;
    }

    return;
}
