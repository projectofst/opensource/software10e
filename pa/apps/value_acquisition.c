#include "value_acquisition.h"

run_avg_t run_avg;

int control[N_SENSORS] = {0};
volatile int adc_flag = 0;

static uint16_t apps0 = 1230;
static uint16_t apps1 = 2000;
static uint16_t bps0 = 3000;
static uint16_t bps1 = 2599;
static uint16_t be = 1500;

void adc_callback(void)
{
    adc_flag = 1;
    return;
}

void toggle_timer1_init(INTERRUPT_TOGGLE toggle)
{
    if (toggle)
    {
        enable_timer1();
    }
    else
    {
        disable_timer1();
    }
}

void toggle_timer2_init(INTERRUPT_TOGGLE toggle)
{
    if (toggle)
    {
        enable_timer2();
    }
    else
    {
        disable_timer2();
    }
}

#ifndef LINUX
void acquire_values(pedal_t *values)
{

    if (adc_flag)
    {

        turn_off_adc();
        toggle_timer1_init(INTERRUPT_TOGGLE_OFF);
        toggle_timer2_init(INTERRUPT_TOGGLE_OFF);

        values->new_apps0 = ADC1BUF4; // 5
        values->new_apps1 = ADC1BUF5; // 1
        values->new_bps0 = ADC1BUF2;  // 2
        values->new_bps1 = ADC1BUF3;  // 3
        values->new_be = ADC1BUF1;    // 4

        turn_on_adc();
        toggle_timer1_init(INTERRUPT_TOGGLE_ON);
        toggle_timer2_init(INTERRUPT_TOGGLE_ON);
        adc_flag = 0;
    }
}
#else
void acquire_values(pedal_t *values)
{
    values->new_apps0 = apps0;
    values->new_apps1 = apps1;
    values->new_bps0 = bps0;
    values->new_bps1 = bps1;
    values->new_be = be;
}
#endif

void compute_averages(pedal_t *values, run_avg_t *run_avg)
{
    // add values to their respective vectors
    run_avg_add(&(run_avg[0]), (double)values->new_apps0);
    run_avg_add(&(run_avg[1]), (double)values->new_apps1);
    run_avg_add(&(run_avg[2]), (double)values->new_bps0);
    run_avg_add(&(run_avg[3]), (double)values->new_bps1);
    run_avg_add(&(run_avg[4]), (double)values->new_be);

    // compute the corresponding average
    values->avg_apps0 = run_avg_average(&(run_avg[0]));
    values->avg_apps1 = run_avg_average(&(run_avg[1]));
    values->avg_bps0 = run_avg_average(&(run_avg[2]));
    values->avg_bps1 = run_avg_average(&(run_avg[3]));
    values->avg_be = run_avg_average(&(run_avg[4]));
}
