MODULE_C_SOURCES+=send_functions.c
MODULE_C_SOURCES+=value_acquisition.c
MODULE_C_SOURCES+=main.c

PWD:=apps/
C_SOURCES += $(call abs_path,$(MODULE_C_SOURCES),$(PWD))

-include $(PROJECT_ROOT)/build/arch_$(TARGET).mk