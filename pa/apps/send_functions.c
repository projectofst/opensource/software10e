#include "send_functions.h"

void send_init_msg(void)
{
    CANdata init_msg;

    send_can1(logI(LOG_RESET_MESSAGE, RCON, 0, 0));

    return;
}

void send_verbose_messages(pedal_t *values)
{
    CANdata verbose_message_1;
    CANdata verbose_message_2;

    msg_te_te_verbose1_t msg1 = {
        .te_apps0 = values->avg_apps0,
        .te_apps1 = values->avg_apps1,
        .te_bps0 = values->avg_bps0,
        .te_bps1 = values->avg_bps1};

    msg_te_te_verbose2_t msg2 = {
        .te_be = values->avg_be,
        .te_hard_braking = 300,
        .te_Implausibility_APPS_Timer_Exceeded = 0,
        .te_Implausibility_APPS_BPS_Timer_Exceeded = 0};

    verbose_message_1 = encode_te_verbose1(msg1);
    verbose_message_2 = encode_te_verbose2(msg2);

    send_can1(verbose_message_1);
    send_can1(verbose_message_2);
}