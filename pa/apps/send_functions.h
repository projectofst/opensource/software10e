#ifndef _SEND_FUNCTIONS_H
#define _SEND_FUNCTIONS_H

#include "pedal_acquirer.h"
#include "lib_pic33e/can.h"
#include "can-ids-v2/te_can.h"

void send_init_msg(void);
void send_verbose_messages(pedal_t* values);

#endif