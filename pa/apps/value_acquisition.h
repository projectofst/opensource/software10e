#include <xc.h>
#include "pedal_acquirer.h"
#include "shared/run_avg/run_avg.h"
#include "lib_pic33e/adc.h"

void toggle_timer1_init(INTERRUPT_TOGGLE toggle);
void toggle_timer2_init(INTERRUPT_TOGGLE toggle);
void acquire_values(pedal_t* values);
void compute_averages(pedal_t * values, run_avg_t* run_avg);

