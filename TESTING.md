# Testing

## Intro

[Unit Testing](https://en.wikipedia.org/wiki/Unit_testing) is the software
development practice of writing small, self contained automated tests. Unit
test verify the behaviour of individual units of source code.

Unit tests are supposed to be simple, fast tests with a very small scope. These
tests should be run frequently for example each time a source file is
saved to disk.

Example test run for `shared/pedals`:

![Running tests](docs/test_run.png)

## Munit

The testing framework used for testing microcontroller software is
[munit](https://nemequ.github.io/munit/).

Check the [getting started](https://nemequ.github.io/munit/#getting-started)
page on munit website as well as the
[example.c](https://github.com/nemequ/munit/blob/master/example.c) file in the munit source. 

See [build system # Invocation](tooling/build/README.md#Invocation) for
instructions on how to run the tests.
