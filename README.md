# FST Lisboa Software

This repository holds the code for all electronic modules deployed in the formula student prototypes developed by FST Lisboa, as well as other software tools, such as an interface for CAN bus data analysis and a database for storage of on track data.

FST Lisboa is a Formula Student team from Portugal, Lisbon. Since the start of the project, 2001, the team developed 9 cars, 3 internal combustion cars, and 6 electric. Formula Student is a 20-year-old engineering competition, backed by industry and high-profile engineers, that aims to provide real-world experience combined with management skills including business planning and project management. The motivation behind this repository is to uphold the formula student ideology of sharing and promoting the creation of new engineering solutions, does keeping transparency hoping that one day the hard work developed by the team throughout the years helps other teams to add value to their projects!


## Contributing
In order to contribute, you should follow these guidelines:

### Branching

There are three levels of branches:

* The Master Branch,
* The Develop Branch,
* The Work Branches.

#### The Master Branch

The master branch should only have code that has been tested on the car. Merges from dev to Master should be made by the repo managers.


#### The Develop Branch


The Develop branch should only have code that has been correctly tested (either on testbench / FakeLib Enviroment / car*).

*car testing only If authorized

#### The Work Branches

Development is done in feature branches. Bug fixes in fix branches.

All branches are merged into dev after review. Reviews are based on the merge request feature of Gitlab.


The testbench should be used to test the feature branches. Code should only be programmed on the car if it was accepted on the master branch unless individually authorized by Al or Dt.

Types of branches:
* Feature
* Fix

Branching naming examples (type/system/description) :
* feat/interface/log_replay
* feat/dcu/message_relay
* fix/dcu/rtds_cmd
* fix/repo/readme


### Commits

Commits should convey enough information about the changes commited.
The most important information is: time, author, subsystem, type of change.

Time and author is information that is given by git itself. Subsystem and type
of change is not, it should then be included in the commit message.

Changes can be: features, fixes, chores, documentation, tests.

Proposed syntax:

```
type(subsystem): message
```

Example of commit message:
* feat(te): open wire check
* fix(dcu): buzzer not working
* chore(interface): version changed to 9.2
* doc(bms): iso spi interface
* test(dash): test printing mock

## Documentation

Check out the [wiki](https://gitlab.com/projectofst/software10e/-/wikis/home)!

Documentation can be found in [https://projectofst.gitlab.io/software10e/](https://projectofst.gitlab.io/software10e/)

To add documents just place them in [docs](https://gitlab.com/projectofst/software10e/-/tree/7abc45e7/docs). 
They will be automatically added to the above page. Your document can be found in `https://projectofst.gitlab.io/software10e/docs/<path-to-your-document>`

### DSPIC33EP HAL

[lib_pic33e/README.md](lib_pic33e/README.md)

### Build system

[tooling/build/README.md](tooling/build/README.md)

