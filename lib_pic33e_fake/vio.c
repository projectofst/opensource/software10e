#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "Queue/Queue.h"
#include "protobuf-c/fake_lib.pb-c.h"

#include "vio.h"

Connection vio_open_conn(const char* host, unsigned port)
{
    Connection conn;
    conn.socket = 0;

    char* hello = "ola\n";

    if ((conn.socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("Error(vio) socket creation failed\n");
        exit(EXIT_FAILURE);
    }

    memset(&conn.addr, 0, sizeof(conn.addr));

    conn.addr.sin_family = AF_INET;
    conn.addr.sin_port = htons(port);
    conn.addr.sin_addr.s_addr = INADDR_ANY;
    //inet_pton(AF_INET, host, &(conn.addr.sin_addr));

    size_t n = sendto(conn.socket, (const char*)hello, strlen(hello),
        0, (const struct sockaddr*)&(conn.addr),
        sizeof(conn.addr));

    return conn;
}

ADC* vio_recv_msg(Connection conn)
{
    ADC* adc;
    char buffer[VIO_BUFFER_MAX] = { 0 };

    socklen_t len;

    int n = recvfrom(conn.socket, (char*)buffer, VIO_BUFFER_MAX,
        MSG_WAITALL, (struct sockaddr*)&(conn.addr),
        &len);

    adc = adc__unpack(NULL, n, buffer);

    return adc;
}
