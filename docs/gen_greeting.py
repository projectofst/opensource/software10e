import sys
from pathlib import Path


def main(argv):
    directory = {}

    def assign_dict(d, parts):
        if len(parts) == 0 or len(parts) == 1:
            return
        if len(parts) == 2:
            if not d.get(parts[0]):
                d[parts[0]] = []
            d[parts[0]].append(parts[1])

            return

        d[parts[0]] = {}
        assign_dict(d[parts[0]], parts[1:])

    def print_dir(d, key):
        if isinstance(d, dict):
            for key in d.keys():
                print(f"<h2>{key}</h2>")
                print_dir(d[key], key)
        elif isinstance(d, list):
            for l in d:
                file = key + "/" + l
                print(f'<a href="{str(file)}">{str(file)}</a>\n<br>\n')

    path = argv[1]

    files = list(Path(path).rglob("*"))

    print("<h1>Index</h1>")

    for file in files:
        if not file.is_file():
            continue

        assign_dict(directory, file.parts)

    print_dir(directory, "")
    # print(f'<a href="{str(file)}">{str(file)}</a>\n<br>\n')


if __name__ == "__main__":
    main(sys.argv)
