import click
from loguru import logger
from pathlib import Path
from pprint import pprint


def print_parts(f, root, d, n):
    print("root", root)
    if len(root.split("/")) < 2:
        f.write(f"<h{str(n)}>Index</h{str(n)}>")
    else:
        f.write(f'<h{str(n)}>{root.split("/")[-2]}</h{str(n)}>')
    for k in sorted(d.keys(), key=lambda x: len(d[x])):
        if len(d[k].keys()) != 0:
            print_parts(f, root + k + "/", d[k], n + 1)
        else:
            f.write(f'<a href="{root + str(k)}">{root + str(k)}</a>\n<br>\n')


@click.group(invoke_without_command=True)
@click.argument("path")
@click.argument("output")
def main(path, output):
    files = list(Path(path).rglob("*"))

    dir_tree = {}
    for file in files:
        d = dir_tree
        p = file.parts
        for p in p:
            if p not in d.keys():
                d[p] = {}
            d = d[p]

    pprint(dir_tree)

    f = open(output, "w")

    f.write("<html>\n")
    f.write("<head>\n")
    f.write('<link rel="stylesheet" href="style.css">')
    f.write("</head>")

    f.write("<body>")

    print_parts(f, "", dir_tree, 1)
    # with open(output, 'w') as f:
    #    for file in files:
    #        print(file.parts)
    #        #f.write(file+"\n")
    #        f.write(f'<a href="{str(file)}">{str(file)}</a>\n<br>\n')

    f.write("</body>")
    f.write("</html>")


if __name__ == "__main__":
    main()
