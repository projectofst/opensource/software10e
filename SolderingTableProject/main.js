const { app, BrowserWindow } = require('electron')
 const express = require('express')
 
 function createWindow () {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 1800,
    height: 800,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // load the app.
  win.loadFile('pages/mainUI.html')

  // debugger
  //win.webContents.openDevTools()
}

app.whenReady().then(createWindow)

// Quit when all windows are closed, except on macOS. There, it's common

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})
