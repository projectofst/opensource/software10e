const PImage = require('pureimage');
const fs = require('fs');
const convert = require('convert-length');

var boardSizeX = 2339
var boardSizeY = 1653

class componentLine {
    constructor(x, y, horizontal, name) {
        this.img = PImage.make(boardSizeX,boardSizeY);
        this.name = name;
        this.ctx = this.img.getContext('2d');
        this.ctx.fillStyle = 'rgba(255,0,0, 0.9)';
        if(horizontal) {
            this.width = 2;
            this.length = boardSizeX;
        } else {
            this.width = boardSizeY;
            this.length = 2;
        }
        this.x = x;
        this.y = y;
        this.ctx.fillRect(boardSizeX - this.x,boardSizeY - this.y,this.length,this.width);
    }
    
    getImage() {
        return this.img;
    }
    
    getName() {
        return this.name;
    }
}

var coordX = 7.675
var coordY = 12.875
var centerPointX = boardSizeX - convert(coordX, 'mm', 'px')
var centerPointY = convert(coordY, 'mm', 'px')


try {
  fs.unlinkSync('X.png')
  fs.unlinkSync('Y.png')
  //file removed
} catch(err) {
  console.error(err)
}

let axis = []
axis.push(new componentLine(boardSizeX, centerPointY, true, "X"));
axis.push(new componentLine(centerPointX, boardSizeY, false, "Y"));

axis.forEach(function(lineImg) {
    PImage.encodePNGToStream(lineImg.getImage(), fs.createWriteStream(lineImg.getName()+'.png')).then(() => {
        console.log("wrote out the png file to "+lineImg.getName()+".png");
    }).catch((e)=>{
        console.log("there was an error writing");
        console.log(e)
    });
})
