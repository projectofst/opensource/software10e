# Readme

This Lap Timer consists of a sensor that detects the presence of an object in front of it and a WiFi board that communicates with the team’s interface. It is conceived to trigger the lap timing function on the interface wirelessly if there is an object (vehicle) passing in front of the sensor.

## Warning

Don't change name of the directory "LapTimer" or it will brake the code

## Using the Lap Timer

1. Position Lap Timer on track.
2. Connect to power.
3. Wait for WiFi connection to be established (After connection is established WiFi status LED will start blinking once every 2 seconds).
4. Connect client to the Lap Timer's UDP Server (Lap Timer's IP adress and Port must be known, otherwise check documentation).
5. __Lap Timer is ready to go.__

## Configuration and adjustments
Several adjustments and configurations can be made by accessing the file __constants.h__


### Wifi
To set WiFi SSID and password change the following parameters:

```c
#define WIFI_SSID "[wifiNetworkName]"
#define WIFI_PASS "[wifiNetworkPassword]"
```

### UDP Server
UDP packets received are periodically checked, either to add new clients to the list or delete clients that are no longer active (clients that haven't send at least one message during the current check period). To change the periodicity of said checks and the port used for the server the following parameters can be changed:

```c
#define checkForNewClientTimeInterval [DesiredPeriod(seconds)] //Default is 5s
#define UDP_PORT [DesiredUdpPort] //Default is 4210
```

To set the maximum number of clients allowed and the maximum size of the packets that can be recieved by the clients change:

```c
#define maximumClients [NumberOfClientsAllowed] //Default is 10
#define maxPacketSize [MaximumPacketSize(bytes)] //Default is 255b
```



### Object Detection
For an object to be considered passing in front of the sensor it must be at a minimum distance from the sensor. This threshold distance can be set by changing:

```c
#define thresholdDistance [DesiredThresholdDistance(centimeters)] //Default is 50cm
```
To detect an object positioned in front of the sensor or a misplacement of the Lap Timer, periodic checks are made to how many distance measurements made during that period indicate an object if front of the sensor. If this number of object detections surpass a predifined limit, an error is flagged. To change the periodicity of this checks and how many object detections are allowed the following parameters can be changed:

```c
#define validityCheckInterval [DesiredCheckPeriod(seconds)] //Default is 3s
#define acceptableReadsPerCheckInterval [ObjectDetectionsLimit] //Default is 10
```