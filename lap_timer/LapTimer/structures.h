/* 
 * Lap Timer is a software with the purpose of signaling that an object passed 
 * by a ultrasonic sensor signaling a lap for instance.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/******************************************************************************
 *
 * File Name:   structures.h
 * 
 * Author:      Andre Santigago Mestre e Costa
 *
 * Description: Definition of structures used to store UDP server client details
 *
 *****************************************************************************/
typedef struct udpIpPort {
  byte ip[4];
  int port;
  int messageCount;
  struct udpIpPort *next;
} UdpIpPort;

typedef struct clientsList {
  UdpIpPort *ipPortsList;
  int clientsNumber;
  int listSize;
} ClientsList;
