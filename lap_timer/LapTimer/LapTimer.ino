/* 
 * Lap Timer is a software with the purpose of signaling that an object passed 
 * by a ultrasonic sensor signaling a lap for instance.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/******************************************************************************
 *
 * File Name:   LapTimer.ino
 * 
 * Author:      Andre Santigago Mestre e Costa
 *
 * Description: Main code for ESP32 connected to HC-SR04 to detect passing object 
 *              (car) and send trigger message to the team's interface
 *
 *****************************************************************************/
#include <WiFi.h>
#include <WiFiUdp.h>
#include <Ticker.h>
#include "constants.h"
#include "debug.h"
#include "structures.h"
#include "CAN.h"


WiFiUDP UDP;  //Create instance of UDP

Ticker timer1;  //Used to check number of distance reads within range
Ticker timer2;  //Used to flash LED in case of error
Ticker timer3;  //Used to turn Off LED in case of read within range
Ticker timer4;  //Used to measure distance
Ticker timer5;  //Used to check for new clients
Ticker timer6;  //Used to blink WiFi LED

void checkNumberOfReads();
void changeLedState();

boolean countingTime = false;
boolean error = false;
boolean distanceReady = false;
boolean measuringDistance = false;
boolean checkClients = false;
float elapsedTime = 0;
int distance = 0;
int validDistanceCheckCounter = 0;
int previousValidDistanceCheckCounter = 0;
int clientsNumber = 0;
char packet[maxPacketSize] = "";
char triggerInterface[] = "t";
ClientsList clientList {.ipPortsList = NULL, .clientsNumber = 0, .listSize = 0};
CANdata canData {{{.dev_id = DEV_ID, .msg_id = MSG_ID}}, .dlc = 4, .data = {0x1210,0x1310,0x1410,0x1510}};


void setup() {
  pinMode(echoPin, INPUT);
  pinMode(triggerPin, OUTPUT);
  digitalWrite(triggerPin, LOW);
  pinMode(sensorLedPin, OUTPUT);
  pinMode(wifiLedPin, OUTPUT);

  digitalWrite(sensorLedPin, HIGH);
  digitalWrite(wifiLedPin, HIGH);
  

  Debug_begin(9600);
  delay(1000);
  digitalWrite(sensorLedPin, LOW);
  digitalWrite(wifiLedPin, LOW);
  Debug_println("Serial begin");

  WiFi.begin(WIFI_SSID, WIFI_PASS); // Begin WiFi connection
  
  timer6.attach(wifiConnectingBlinkPeriod, blinkWifiLed); //Start blinking WiFi status LED while board is connecting to WiFi
  Debug_print("Connecting to ");
  Debug_print(WIFI_SSID);

  while (WiFi.status() != WL_CONNECTED) { //Waits for wifi connection to be established
    delay(100);
    Debug_print(".");
  }
  timer6.detach();
  
  Debug_println();
  Debug_print("Connected! IP address: ");
  Debug_println(WiFi.localIP());
 
  UDP.begin(UDP_PORT); //Begins listening to UDP port
  Debug_print("Listening on UDP port ");
  Debug_println(UDP_PORT);

  timer6.attach(waitingForClientBlinkPeriod, blinkWifiLed); //Start blinking WiFi status LED while board is waiting for first UDP client to connect
  waitForUDPClient(); //Waits for client to establish connection and reveal its Port and IP
  timer6.detach();

  digitalWrite(wifiLedPin, HIGH); //Set Wifi status LED to solid state

  timer1.attach(validityCheckInterval, checkNumberOfReads);
  timer5.attach(checkForNewClientTimeInterval, setCheckForNewClient);
}


void loop() {

  if(!measuringDistance && !distanceReady) getDistance(0); //Starts measuring distance to object

  if(distanceReady) { //If a distance measurment is ready to be taken
    getDistance(2); //Finishes distance measurment
    distanceReady = false; //This way one distance measure will only be considered once

    if (checkDistance(distance)) { //Checks if distance is within acceptable range (0 -> thresholdDistance)
      ++validDistanceCheckCounter;
      previousValidDistanceCheckCounter = validDistanceCheckCounter;

      if (!error && (validDistanceCheckCounter <= 1)) { //If not in error state and number of read distances within range doesn't exceed maximum allowed during current interval
        timer1.detach();
        Debug_print("Trigger ");
        sendMessageToClientList();
        flashLed();
        timer1.attach(validityCheckInterval, checkNumberOfReads);
      }

      validDistanceCheckCounter = previousValidDistanceCheckCounter;
    }
  }

  if (clientList.clientsNumber == 0) {
    timer6.attach(waitingForClientBlinkPeriod, blinkWifiLed);
    waitForUDPClient(); //Waits for client to establish connection and reveal its Port and IP
    timer6.detach();
    digitalWrite(wifiLedPin, HIGH);
  }

  if (checkClients) checkForNewClients();

}


/******************************************************************************
 * getDistance()
 *
 * Arguments: useCase ->  0 for starting to measure, 1 for setting flags and 2 for 
 *                        finishing measurment
 * 
 * Returns: None
 *
 * Side-Effects:  Changes value of variables measuringDistance, distanceReady and distance
 *                Starts and stops timer4
 *
 * Description: Measures distance with ultrasonic sensor (with echo and trigger pins)
 *              Distance is measured by sending ultrasound (by setting triggerPin to HIGH)  
 *              and measuring how much time it takes for said sound to be detected after 
 *              reflection on objet (dected on echoPin)
 *
 *****************************************************************************/
void getDistance(int useCase) {
  static int elapsedTime = 0;

  switch (useCase) {
    case 0: //Starts distance measurement
      distanceReady = false;
      digitalWrite(triggerPin, HIGH);
      measuringDistance = true;
      timer4.attach_ms(10, getDistance, 1);
      break;
    case 1: //Sets flags that indicate sensor is ready to finish distance measurment
      timer4.detach();
      distanceReady = true;
      measuringDistance = true;
      break;
    case 2: //Finishes distance measurment
      digitalWrite(triggerPin, LOW);
      elapsedTime = pulseIn(echoPin, HIGH); //Measures how much time the signal takes to echo on object and be read by the sensor
      distance = elapsedTime * 0.034 / 2; //elapsedTime * [sound speed in (cm/s)] / 2
      //Debug_print("Distance: ");
      //Debug_println(distance);
      measuringDistance = false;
      distanceReady = false;
      break;
    default: 
      break;
  }
}


/******************************************************************************
 * checkDistance()
 *
 * Arguments: int -> distance : Distance to check
 * 
 * Returns: boolean ->  true if if distance is less than defined threshold distance 
 *                      and false otherwise
 *
 * Side-Effects: None
 *
 * Description: Checks if provided distance is within accepted range (0 -> thresholdDistance)
 *
 *****************************************************************************/
boolean checkDistance(int distance) {
  if (distance < thresholdDistance) return true;
  return false;
}


/******************************************************************************
 * flashLed()
 *
 * Arguments: None
 * 
 * Returns: None
 *
 * Side-Effects: Lights up LED connected to Pin and starts timer3
 *
 * Description: Flashes Led on sensorLedPin once
 *
 *****************************************************************************/
void flashLed() {
  if (!error) {
    digitalWrite(sensorLedPin, HIGH);
    //Debug_println("[*]");
    timer3.attach(LedHoldTime, ledOff);
  }
}


/******************************************************************************
 * ledOff()
 *
 * Arguments: None
 * 
 * Returns: None
 *
 * Side-Effects: Lights down LED connected to sensorLedPin and stops timer3
 *
 * Description: Turns off LED on sensorLedPin
 *
 *****************************************************************************/
void ledOff() {
  if(!error) {
    digitalWrite(sensorLedPin, LOW);
    //Debug_println("[ ]");
    timer3.detach();
  }
}


/******************************************************************************
 * blinkWifiLed()
 *
 * Arguments: None
 * 
 * Returns: None
 *
 * Side-Effects: Lights up and down the Led on wifiLedPin
 *
 * Description: Used to flash LED itermitently
 *
 *****************************************************************************/
void blinkWifiLed() {
  static boolean state = false;

  if (!state) {
    digitalWrite(wifiLedPin, HIGH);
    //Debug_println("[*]");
    state = true;
    return;
  } else if (state) {
    digitalWrite(wifiLedPin, LOW);
    //Debug_println("[ ]");
    state = false;
  }
}


/******************************************************************************
 * checkNumberOfReads()
 *
 * Arguments: None
 * 
 * Returns: None
 *
 * Side-Effects: Starts and stops timer2 and changes value of variable validDistanceCheckCounter
 *
 * Description: Checks if the number of valid distance checks in that time interval 
 *              is more than the set limit
 *
 *****************************************************************************/
void checkNumberOfReads() {

  if(validDistanceCheckCounter > acceptableReadsPerCheckInterval) {
    if(!error) {
      timer2.attach(ledErrorFrequency, changeLedState);
    }
    error = true;

  } else if (error){
    error = false;
    timer2.detach();
    changeLedState();
  }

  validDistanceCheckCounter = 0;
}


/******************************************************************************
 * changeLedState()
 *
 * Arguments: None
 * 
 * Returns: None
 *
 * Side-Effects: Lights up and down the Led on sensorLedPin
 *
 * Description: Used to flash LED itermitently
 *
 *****************************************************************************/
void changeLedState() {
  static boolean state = false;

  if (!error) {
    digitalWrite(sensorLedPin, LOW);
    //Debug_println("[ ]");
    state = false;
    return;
  } else if (!state) {
    digitalWrite(sensorLedPin, HIGH);
    //Debug_println("[*]");
    state = true;
    return;
  } else if (state) {
    digitalWrite(sensorLedPin, LOW);
    //Debug_println("[ ]");
    state = false;
  }

}


/******************************************************************************
 * waitForUDPClient()
 *
 * Arguments: None
 * 
 * Returns: None
 *
 * Side-Effects: Changes UDP variable parameters
 *
 * Description: Waits for a client to connect to the server and adds client to 
 *              clients list
 *
 *****************************************************************************/
void waitForUDPClient() {
  byte ip[4];
  int i = 0;
  int packetSize = 0;

  Debug_print("Waiting for client .");
  while (packetSize == 0) {
    packetSize = UDP.parsePacket();
    Debug_print(" .");
    delay(100);
  }

  clientsNumber++;

  Debug_println();
  Debug_print("Client on port: ");
  Debug_print(UDP.remotePort());
  Debug_print(" with IP: ");
  Debug_println(UDP.remoteIP());

  packetSize = UDP.read(packet, 255);
  if(packetSize > 0) {
    packet[packetSize] = '\0';
  }
  Debug_print("Client sent: ");
  Debug_println(packet);

  Debug_print("Adding client...");
  for (i = 0; i < 4; i++) {
      ip[i] = UDP.remoteIP()[i];
    }
  addToClientList(ip, UDP.remotePort());
  Debug_println(" Done");
}


/******************************************************************************
 * checkForNewClients()
 *
 * Arguments: None
 * 
 * Returns: None
 *
 * Side-Effects: May free or allocate memory
 *
 * Description: Checks for the presence of UDP packets and adds the sender to the
 *              clients list or confirms its active state. If client doesn't send 
 *              confirmation packets it is removed from the clients list
 *
 *****************************************************************************/
void checkForNewClients() {
  byte ip[4];
  int i = 0;
  int packetSize = 0;

  packetSize = UDP.parsePacket();
  while (packetSize != 0) {
    /*
    Debug_println();
    Debug_print("New client on port: ");
    Debug_print(UDP.remotePort());
    Debug_print(" with IP: ");
    Debug_println(UDP.remoteIP());
    */

    packetSize = UDP.read(packet, 255);
    if(packetSize > 0) {
      packet[packetSize] = '\0';
    }
    /*
    Debug_print("Client sent: ");
    Debug_println(packet);
    */

    for (i = 0; i < 4; i++) {
      ip[i] = UDP.remoteIP()[i];
    }
    addToClientList(ip, UDP.remotePort());
    
    packetSize = UDP.parsePacket();
  }

  checkOldClients();
  checkClients = false;
}


/******************************************************************************
 * checkOldClients()
 *
 * Arguments: None
 * 
 * Returns: None
 *
 * Side-Effects: May free memory
 *
 * Description: Checks if clients are sending messages confirming their active
 *              state. If not active, clients are removed from clients list
 *
 *****************************************************************************/
void checkOldClients() {
  UdpIpPort *ipPort = NULL;

  ipPort = clientList.ipPortsList;
  while (ipPort != NULL) {
    if (ipPort->messageCount < 1) removeFromClientList(ipPort);
    else ipPort->messageCount = 0;
    ipPort = ipPort->next;
  }
}


/******************************************************************************
 * addToClientList()
 *
 * Arguments: byte* -> ip : IP of client to be added to the client list
 *            int -> port : Port to which the client to be added to the client list is 
 *                          listening to
 * 
 * Returns: None
 *
 * Side-Effects: Allocates memory for a new client
 *
 * Description: Checks if client specified through the function arguments already 
 *              exists on the clients list and, if not, adds it to the list
 *
 *****************************************************************************/
void addToClientList(byte ip[4], int port) {
  int j = 0, i = 0;
  boolean equal = true;
  UdpIpPort *newClient = NULL;
  UdpIpPort *ipPort = NULL;


  ipPort = clientList.ipPortsList;
  while (ipPort != NULL) {  //Checks if client is already registered (same IP, same port)
    if (ipPort->port == port) {
      for (i = 0; i < 4; i++) {
        if (ipPort->ip[i] != ip[i]) equal = false;
      }

      if (equal) {
        //Debug_println("Known Client");
        ipPort->messageCount++;
        return;
      }
    }
    ipPort = ipPort->next;
  }

  if (clientList.clientsNumber >= maximumClients) return;

  newClient = (UdpIpPort*) malloc (sizeof(UdpIpPort));
  newClient->next = NULL;
  newClient->port = port;
  newClient->messageCount = 1;
  for (i = 0; i < 4; i++) {
    newClient->ip[i] = ip[i];
  }

  if (clientList.ipPortsList != NULL) {
    ipPort = clientList.ipPortsList; //ipPort set on list head
    while (ipPort->next != NULL) {
      ipPort = ipPort->next;
    }
    ipPort->next = newClient;
  } else {
    clientList.ipPortsList = newClient;
  }

  Debug_println("");
  Debug_println("");
  Debug_println("!!!!!!!!!!!! Adding Client !!!!!!!!!!!!");

  clientList.clientsNumber++;
}


/******************************************************************************
 * removeFromClientList()
 *
 * Arguments: UdpIpPort* -> client : client to be deleted from clients list
 * 
 * Returns: None
 *
 * Side-Effects: Frees memory allocated for deleted client
 *
 * Description: Deletes and removes one client from the clients list
 *
 *****************************************************************************/
void removeFromClientList(UdpIpPort *client) {
  UdpIpPort *previous = NULL;
  int i;

  if ((clientList.ipPortsList == NULL) || (client == NULL)) {
    return;
  } else if (clientList.ipPortsList == client) {
    clientList.ipPortsList = NULL;
  } else {
    previous = clientList.ipPortsList;
    while (previous->next != NULL) { //Searches for client to be deleted
      if (previous->next == client) {
        break;
      }
      previous = previous->next;
    }

    previous->next = client->next;
  }

  Debug_println("");
  Debug_println("");
  Debug_println("!!!!!!!!!!! Removing Client !!!!!!!!!!!");
  free(client);

  clientList.clientsNumber--;
}


/******************************************************************************
 * sendMessageToClientList()
 *
 * Arguments: None
 * 
 * Returns: None
 *
 * Side-Effects: None
 *
 * Description: Sends CANdata (canData) in UDP packets to clients on clients list
 *
 *****************************************************************************/
void sendMessageToClientList() {
  int i = 0;
  UdpIpPort *ipPort = NULL;

  Debug_print("Sending message to ");
  Debug_println(clientList.clientsNumber);

  ipPort = clientList.ipPortsList;
  while (ipPort != NULL) {
    UDP.beginPacket(ipPort->ip, ipPort->port);
    UDP.write((uint8_t *) (CANdata *) &canData, 12);
    UDP.endPacket();
    ipPort = ipPort->next;
  }
}


/******************************************************************************
 * setCheckForNewClient()
 *
 * Arguments: None
 * 
 * Returns: None
 *
 * Side-Effects: Changes value of global variable checkClients
 *
 * Description: Changes value of global variable checkClients
 *
 *****************************************************************************/
void setCheckForNewClient() {
  checkClients = true;
}


/******************************************************************************
 * countTime()
 *
 * Arguments: boolean -> start_stop : if true starts timer and if false stops timer
 *            float* -> elapsedTimeReturn : adrress to save timer value
 * 
 * Returns: None
 *
 * Side-Effects: Changes value on adress passed to function and alocates memory for a 
 *               static variable
 *
 * Description: Starts and stops timer
 *
 *****************************************************************************/
/*void countTime(boolean start_stop, float* elapsedTimeReturn) {
  static unsigned long timeCounter = 0;

  if (start_stop) timeCounter = millis(); //Gets current time
  if (!start_stop) {
    timeCounter = millis() - timeCounter; //Gets elapsed time
    *elapsedTimeReturn = (float)timeCounter / 1000; //Converts time to seconds
  }
}*/
