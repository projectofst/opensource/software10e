/* 
 * Lap Timer is a software with the purpose of signaling that an object passed 
 * by a ultrasonic sensor signaling a lap for instance.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/******************************************************************************
 *
 * File Name:   constants.h
 * 
 * Author:      Andre Santigago Mestre e Costa
 *
 * Description: Definition of constants that are used across the code. This is 
 *              where most configuration and adjustments to the functionality 
 *              of the board can be made
 *
 *****************************************************************************/
#define echoPin 5
#define triggerPin 2
#define sensorLedPin 4
#define wifiLedPin 17
#define thresholdDistance 50
#define LedHoldTime 1
#define ledErrorFrequency 0.25
#define wifiConnectingBlinkPeriod 0.5
#define waitingForClientBlinkPeriod 1
#define validityCheckInterval 3
#define acceptableReadsPerCheckInterval 10
#define checkForNewClientTimeInterval 5
#define maximumClients 10
#define maxPacketSize 255
#define WIFI_SSID "[wifiNetworkName]"
#define WIFI_PASS "[wifiNetworkPassword]"
#define UDP_PORT 4210
#define DEV_ID 7
#define MSG_ID 32
