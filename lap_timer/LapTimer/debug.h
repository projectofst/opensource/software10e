/* 
 * Lap Timer is a software with the purpose of signaling that an object passed 
 * by a ultrasonic sensor signaling a lap for instance.
 * Copyright © 2021 Projecto FST.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.

 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/******************************************************************************
 *
 * File Name:   debug.h
 * 
 * Author:      Andre Santigago Mestre e Costa
 *
 * Description: Definition of debug serial prints that can be activated by
 *              uncommenting the first line
 *
 *****************************************************************************/
//#define DEBUG //Uncomment if debugging
#ifdef DEBUG
  #define Debug_begin(x) Serial.begin(x)
  #define Debug_print(x) Serial.print(x)
  #define Debug_println(x) Serial.println(x)
#else
  #define Debug_begin(x)
  #define Debug_print(x)
  #define Debug_println(x)
#endif
