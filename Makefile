TOPTARGETS := all clean test linux clang-format clang-format-check

SUBDIRS := pic30f_template pic33e_template sniffer steering_wheel te pa bms-master dash dcu iib hw fst-can-interface

-include local.mk

$(TOPTARGETS): $(SUBDIRS)
$(SUBDIRS):
	make -C $@ $(MAKECMDGOALS)

interface-doc:
	cd fst-can-interface; make doc

docs: interface-doc
	fcp docs can-ids-spec/fst10e.json html

init:
	git config core.hooksPath hooks


.PHONY: $(TOPTARGETS) $(SUBDIRS) test
