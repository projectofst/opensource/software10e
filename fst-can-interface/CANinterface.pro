QT += core gui
QT += concurrent
QT += serialport
QT += network
QT += charts
QT += quickwidgets
QT += quickcontrols2
QT += qml
QT += serialbus

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport
TARGET = CANinterface
TEMPLATE = app

macos {
    ICON = ass/FST_avatar.icns
}

win32 {
    RC_ICONS = ass/FST_avatar.ico
}


# Force C++ mode for C files - crucial to compile can-ids.
# Use clang++ compiler from XCode Toolchain (macOS)

macos {
    QMAKE_CC = clang++
    QMAKE_CXX = clang++
} else {
    QMAKE_CC = g++
}

CONFIG += silent
CONFIG+=C++17
#CONFIG+=ccache
QMAKE_CC = ccache $$QMAKE_CC
QMAKE_CXX = ccache $$QMAKE_CXX

#####################################################################
# COMPILER / LINKER  FLAGS

DEFINES = GIT_CURRENT_SHA1="\\\"$(shell git rev-parse HEAD)\\\""
DEFINES += GIT_CURRENT_BRANCH="\\\"$(shell git rev-parse --abbrev-ref HEAD)\\\""
DEFINES += QT_MESSAGELOGCONTEXT

# Force C++17 (macOS)
macos {
    QMAKE_CXXFLAGS_GNUCXX11 =
    QMAKE_CXXFLAGS_CXX11 = -std=c++17
}
else {
    QMAKE_CXXFLAGS += -std=c++17
}

QMAKE_CXXFLAGS += -Wall
QMAKE_CXXFLAGS += $$STRICT
QMAKE_CXXFLAGS += -Wextra
#QMAKE_CXXFLAGS += -Wfatal-errors
QMAKE_CXXFLAGS += -Wno-deprecated-declarations
QMAKE_CXXFLAGS += -Wunused

QMAKE_CXXFLAGS_RELEASE *= -O3


#####################################################################
# SOURCES
LIBS+= -L/usr/local/lib -lyaml-cpp
INCLUDEPATH+= /usr/local/include #for andre

INCLUDEPATH +=\
    ass/\
    can-ids/\
    can-ids/Devices/\
    can-ids-spec/\
    qt-can-com/\
    lib/\
    lib/fcp-cpp/src/\
    src/\
    src/Resources/\
    src/fcp-qt/\
    src/fcp-qt/Exceptions/\
    src/Battery/\
    can-ids-spec/interface-parser/\
    sql/\


SOURCES +=\
    can-ids-spec/interface-parser/signal_parser.c \
    can-ids/Devices/BMS_MASTER_CAN.c \
    can-ids/Devices/BMS_VERBOSE_CAN.c \
    can-ids/Devices/COMMON_CAN.c \
    can-ids/Devices/DASH_CAN.c \
    can-ids/Devices/DCU_CAN.c \
    can-ids/Devices/IIB_CAN.c \
    can-ids/Devices/INTERFACE_CAN.c \
    can-ids/Devices/TE_CAN.c \
    \
    qt-can-com/COM_PlotJuggler.cpp \
    qt-can-com/log/com_log.cpp \
    qt-can-com/loopback/COM_Loopback.cpp \
    qt-can-com/socket-can/com_socketcan.cpp \
    qt-can-com/UDPSocket.cpp \
    qt-can-com/COM.cpp \
    qt-can-com/COM_LogConvert.cpp \
    qt-can-com/COM_LogGen.cpp \
    qt-can-com/COM_LogPlay.cpp \
    qt-can-com/COM_LogRec.cpp \
    qt-can-com/COM_SerialPort.cpp \
    qt-can-com/COM_UdpSendSock.cpp \
    \
    src/ConfigManager/configitem.cpp \
    src/ConfigManager/configmanager.cpp \
    src/ConfigManager/configmanagerdialog.cpp \
    src/ETAS/etas.cpp \
    src/Console/FcpDisplay.cpp \
    src/Exceptions/logutils.cpp \
    src/GeneralWidget/GeneralWidget.cpp \
    src/Resources/configslist.cpp \
    src/fcp-qt/Exceptions/NoSuchConfigException.cpp \
    src/fcp-qt/Exceptions/badjsonfileformatexception.cpp \
    src/fcp-qt/Exceptions/encodinglowerboundexception.cpp \
    src/fcp-qt/Exceptions/encodingmethodnotsuportedexception.cpp \
    src/fcp-qt/Exceptions/encodingmissingsignalexception.cpp \
    src/fcp-qt/Exceptions/encodingoverbitesizeexception.cpp \
    src/fcp-qt/Exceptions/encodingtoolittlesignalsgivenexception.cpp \
    src/fcp-qt/Exceptions/encodingtoomanysignalsgivenexception.cpp \
    src/fcp-qt/Exceptions/encodinguperboundexception.cpp \
    src/fcp-qt/Exceptions/jsoncommandnoargsexception.cpp \
    src/fcp-qt/Exceptions/jsoncommandnoretsexception.cpp \
    src/fcp-qt/Exceptions/jsonexception.cpp \
    src/fcp-qt/Exceptions/jsonfiledoesnotexistexception.cpp \
    src/fcp-qt/Exceptions/nofileselectedexception.cpp \
    src/fcp-qt/Exceptions/nojsondevicesfoundexception.cpp \
    src/fcp-qt/Exceptions/nojsonlogsfoundexception.cpp \
    src/fcp-qt/Exceptions/nojsonmessagesavailableexception.cpp \
    src/fcp-qt/Exceptions/nojsonparsingdoneyetexception.cpp \
    src/fcp-qt/Exceptions/nosuchcommandexception.cpp \
    src/fcp-qt/Exceptions/nosuchjsoncansignalexception.cpp \
    src/fcp-qt/Exceptions/nosuchjsondeviceexception.cpp \
    src/fcp-qt/Exceptions/nosuchjsonlogexception.cpp \
    src/fcp-qt/Exceptions/nosuchmessageexception.cpp \
    src/fcp-qt/cansignal.cpp \
    src/fcp-qt/command.cpp \
    src/fcp-qt/config.cpp \
    src/fcp-qt/device.cpp \
    src/fcp-qt/fcpcom.cpp \
    src/fcp-qt/jsoncommandarg.cpp \
    src/fcp-qt/jsoncommon.cpp \
    src/fcp-qt/jsoncomponent.cpp \
    src/fcp-qt/jsonlog.cpp \
    src/fcp-qt/jsonparser.cpp \
    src/fcp-qt/message.cpp \
    \
    src/Exceptions/helper.cpp \
    \
    src/Resources/DataAlarm.cpp \
    src/Resources/DataAlarmManager.cpp \
    src/Resources/FCPSignalDisplayValue.cpp \
    src/Resources/InputAndNameTemplateWidget.cpp \
    src/Resources/MultipleInputTemplateWidget.cpp \
    src/Resources/Stopwatch.cpp \
    src/Resources/adcwidget.cpp \
    src/Resources/ledandtemperaturewidget.cpp \
    src/Resources/nameValueInputTemplate.cpp \
    src/Resources/filterwidget.cpp \
    src/Resources/ledform.cpp \
    src/Resources/LEDwidget.cpp \
    src/Resources/plot_filter_widget.cpp \
    \
    src/Battery/AMS.cpp \
    src/Battery/AMS_Battery.cpp \
    src/Battery/AMS_Cell.cpp \
    src/Battery/AMS_Cell_Line.cpp \
    src/Battery/AMS_Details.cpp \
    src/Battery/AMS_Stack.cpp \
    src/Battery/AMS_Stack_Details.cpp \
    src/Battery/AMS_Stack_Summary.cpp \
    src/Battery/BMS.cpp \
    src/Battery/BMS_Stack.cpp \
    \
    src/LogReplay/Exceptions/LogException.cpp \
    src/LogReplay/Exceptions/LogFileNotOpenException.cpp \
    src/LogReplay/Exceptions/LogFileNotSupportedException.cpp \
    src/LogReplay/LogKeeper.cpp \
    src/LogReplay/LogReplay.cpp \
    src/LogReplay/SampleBasedLogKeeper.cpp \
    src/LogReplay/csvparser.cpp \
    src/LogReplay/logparser.cpp \
    \
    src/ConfigWindow/cmdedit.cpp \
    src/ConfigWindow/cmdline.cpp \
    src/ConfigWindow/configwindow.cpp \
    src/ConfigWindow/setedit.cpp \
    src/ConfigWindow/setline.cpp \
    src/ConfigWindow/msgedit.cpp \
    src/ConfigWindow/msgline.cpp \
    src/ConfigWindow/msgargs.cpp \
    \
    src/fcp-widgets/fcpdocs.cpp \
    src/ASWidget/ASWidget.cpp \
    src/CCU/individualccu.cpp \
    src/SignalGenerator/ExprtkCompilationError.cpp \
    src/SignalGenerator/SignalGenerator.cpp \
    src/CanSignalGenerator.cpp \
    src/ADCManagerWidget.cpp \
    src/SingleInverter.cpp \
    src/Tail/Report.cpp \
    src/WindowMain.cpp \
    src/lap_time.cpp \
    src/Custom_Lap.cpp \
    src/COM_Manager.cpp \
    src/COM_Select.cpp \
    src/Console.cpp \
    src/Console_makeid.cpp \
    src/DevStatus.cpp \
    src/General.cpp \
    src/General_Details.cpp \
    src/General_Details_PotProgress.cpp \
    src/Inverters.cpp \
    src/ccu.cpp \
    src/ccu_indiv.cpp \
    src/main.cpp \
    src/Picasso.cpp \
    src/Isabelle.cpp \
    src/UiWindow.cpp \
    src/Main_Toolbar.cpp \
    src/COM_Interface.cpp \
    src/comselectline.cpp \
    src/run_laps.cpp \
    src/single_lap.cpp \
    src/testwidget.cpp \
    src/timeline/timeline.cpp \
    src/timestamp.cpp \
    src/time.cpp \
    src/isa_config.cpp \
    src/pcb_version.cpp \
    src/iib_motor_control.cpp \
    src/report_filters.cpp \
    src/report_indiv_filter.cpp \
    src/versionline.cpp \
    src/versions.cpp \
    lib/fcp-cpp/src/fcp_signal.cpp\
    lib/fcp-cpp/src/fcp_config.cpp\
    lib/fcp-cpp/src/fcp.cpp\
    lib/fcp-cpp/src/fcp_message.cpp\
    lib/fcp-cpp/src/fcp_log.cpp\
    lib/fcp-cpp/src/fcp_device.cpp\
    lib/fcp-cpp/src/fcp_command.cpp\
    lib/fcp-cpp/src/fcp_argument.cpp\


HEADERS +=\
    qt-can-com/COM_PlotJuggler.hpp \
    qt-can-com/log/com_log.h \
    qt-can-com/loopback/COM_Loopback.h \
    qt-can-com/socket-can/com_socketcan.h \
    src/ASWidget/ASWidget.hpp \
    src/Battery/AMS.hpp \
    src/Battery/AMS_Battery.hpp \
    src/Battery/AMS_Cell.hpp \
    src/Battery/AMS_Cell_Line.hpp \
    src/Battery/AMS_Details.hpp \
    src/Battery/AMS_Stack.hpp \
    src/Battery/AMS_Stack_Details.hpp \
    src/Battery/AMS_Stack_Summary.hpp \
    src/Battery/BMS.hpp \
    src/Battery/BMS_Stack.hpp \
    src/CanSignalGenerator.hpp \
    can-ids-spec/interface-parser/signal_parser.h \
    can-ids/Devices/BMS_MASTER_CAN.h \
    can-ids/Devices/BMS_VERBOSE_CAN.h \
    can-ids/Devices/COMMON_CAN.h \
    can-ids/Devices/DASH_CAN.h \
    can-ids/Devices/DCU_CAN.h \
    can-ids/Devices/IIB_CAN.h \
    can-ids/Devices/INTERFACE_CAN.h \
    can-ids/Devices/TE_CAN.h \
    qt-can-com/UDPSocket.hpp \
    src/ADCManagerWidget.h \
    src/ConfigManager/configitem.hpp \
    src/ConfigManager/configmanager.hpp \
    src/ConfigManager/configmanagerdialog.hpp \
    src/ConfigWindow/cmdedit.h \
    src/ConfigWindow/cmdline.h \
    src/ConfigWindow/configwindow.h \
    src/ConfigWindow/fcprunnable.h \
    src/ConfigWindow/msgargs.h \
    src/ConfigWindow/setedit.h \
    src/ConfigWindow/setline.h \
    src/ConfigWindow/msgedit.h \
    src/ConfigWindow/msgline.h \
    src/ETAS/etas.hpp \
    src/Console/FcpDisplay.hpp \
    src/Exceptions/logutils.hpp \
    src/GeneralWidget/GeneralWidget.hpp \
    src/Factory.hpp \
    src/LogReplay/csvparser.hpp \
    src/LogReplay/logparser.hpp \
    src/CCU/individualccu.hpp \
    src/LogReplay/Exceptions/LogException.hpp \
    src/LogReplay/Exceptions/LogFileNotOpenException.hpp \
    src/LogReplay/Exceptions/LogFileNotSupportedException.hpp \
    src/LogReplay/LogKeeper.hpp \
    src/LogReplay/LogReplay.hpp \
    src/LogReplay/SampleBasedLogKeeper.hpp \
    src/Resources/DataAlarm.h \
    src/Resources/DataAlarmManager.hpp \
    src/Resources/FCPSignalDisplayValue.hpp \
    src/Resources/InputAndNameTemplateWidget.hpp \
    src/Resources/MultipleInputTemplateWidget.hpp \
    src/Resources/Stopwatch.hpp \
    src/Resources/adcwidget.hpp \
    src/Resources/configslist.h \
    src/Resources/ledandtemperaturewidget.hpp \
    src/Resources/nameValueInputTemplate.hpp \
    src/Exceptions/helper.hpp \
    src/SignalGenerator/ExprtkCompilationError.hpp \
    src/SignalGenerator/SignalGenerator.hpp \
    src/SingleInverter.hpp \
    src/Tail/Report.hpp \
    src/WindowMain.hpp \
    src/fcp-qt/Exceptions/NoSuchConfigException.hpp \
    src/fcp-qt/Exceptions/badjsonfileformatexception.hpp \
    src/fcp-qt/Exceptions/encodinglowerboundexception.hpp \
    src/fcp-qt/Exceptions/encodingmethodnotsuportedexception.hpp \
    src/fcp-qt/Exceptions/encodingmissingsignalexception.hpp \
    src/fcp-qt/Exceptions/encodingoverbitesizeexception.hpp \
    src/fcp-qt/Exceptions/encodingtoolittlesignalsgivenexception.hpp \
    src/fcp-qt/Exceptions/encodingtoomanysignalsgivenexception.hpp \
    src/fcp-qt/Exceptions/encodinguperboundexception.hpp \
    src/fcp-qt/Exceptions/jsoncommandnoargsexception.hpp \
    src/fcp-qt/Exceptions/jsoncommandnoretsexception.hpp \
    src/fcp-qt/Exceptions/jsonexception.hpp \
    src/fcp-qt/Exceptions/jsonfiledoesnotexistexception.hpp \
    src/fcp-qt/Exceptions/nofileselectedexception.hpp \
    src/fcp-qt/Exceptions/nojsondevicesfoundexception.hpp \
    src/fcp-qt/Exceptions/nojsonlogsfoundexception.hpp \
    src/fcp-qt/Exceptions/nojsonmessagesavailableexception.hpp \
    src/fcp-qt/Exceptions/nojsonparsingdoneyetexception.hpp \
    src/fcp-qt/Exceptions/nosuchcommandexception.hpp \
    src/fcp-qt/Exceptions/nosuchjsoncansignalexception.hpp \
    src/fcp-qt/Exceptions/nosuchjsondeviceexception.hpp \
    src/fcp-qt/Exceptions/nosuchjsonlogexception.hpp \
    src/fcp-qt/Exceptions/nosuchmessageexception.hpp \
    src/fcp-qt/cansignal.hpp \
    src/fcp-qt/command.hpp \
    src/fcp-qt/config.hpp \
    src/fcp-qt/device.hpp \
    src/fcp-qt/fcpcom.hpp \
    src/fcp-qt/json.hpp \
    src/fcp-qt/jsoncommandarg.hpp \
    src/fcp-qt/jsoncommon.hpp \
    src/fcp-qt/jsoncomponent.hpp \
    src/fcp-qt/jsonlog.hpp \
    src/fcp-qt/jsonparser.hpp \
    src/fcp-qt/message.hpp \
    src/fcp-widgets/fcpdocs.h \
    src/lap_time.hpp \
    src/Custom_Lap.hpp \
    src/Resources/LEDwidget.hpp \
    src/Resources/ledform.hpp \
    src/Resources/filterwidget.hpp \
    src/Resources/plot_filter_widget.hpp \
    qt-can-com/COM.hpp \
    qt-can-com/COM_LogConvert.hpp \
    qt-can-com/COM_LogGen.hpp \
    qt-can-com/COM_LogPlay.hpp \
    qt-can-com/COM_LogRec.hpp \
    src/COM_Manager.hpp \
    src/COM_Select.hpp \
    qt-can-com/COM_SerialPort.hpp \
    src/Console.hpp \
    src/Console_makeid.hpp \
    src/DevStatus.hpp \
    src/General.hpp \
    src/General_Details.hpp \
    src/General_Details_PotProgress.hpp \
    src/Inverters.hpp \
    src/Picasso.hpp \
    src/Isabelle.hpp \
    src/UiWindow.hpp \
    qt-can-com/COM_UdpSendSock.hpp \
    src/Main_Toolbar.hpp \
    src/COM_Interface.hpp \
    src/ccu.hpp \
    src/ccu_indiv.hpp \
    src/comselectline.hpp \
    src/run_laps.hpp \
    src/single_lap.hpp \
    src/state_manager.hpp \
    src/testwidget.hpp \
    src/timeline/timeline.h \
    src/timestamp.hpp \
    src/time.hpp \
    src/isa_config.hpp \
    src/pcb_version.hpp \
    src/iib_motor_control.hpp \
    src/report_filters.hpp \
    src/report_indiv_filter.hpp \
    src/versionline.hpp\
    src/versions.hpp \

FORMS +=\
    src/ADCManagerWidget.ui \
    src/ASWidget/ASWidget.ui \
    src/Battery/AMS.ui \
    src/Battery/AMS_Cell_Line.ui \
    src/Battery/AMS_Details.ui \
    src/Battery/AMS_Stack_Details.ui \
    src/Battery/AMS_Stack_Summary.ui \
    src/Battery/BMS.ui \
    src/Battery/BMS_Stack.ui \
    src/CCU/individualccu.ui \
    src/ConfigManager/configitem.ui \
    src/ConfigManager/configmanagerdialog.ui \
    src/ConfigWindow/cmdedit.ui \
    src/ConfigWindow/cmdline.ui \
    src/ConfigWindow/configwindow.ui \
    src/ConfigWindow/setedit.ui \
    src/ConfigWindow/setline.ui \
    src/ConfigWindow/msgedit.ui \
    src/ConfigWindow/msgline.ui \
    src/ConfigWindow/msgargs.ui \
    src/ETAS/etas.ui \
    src/Console/FcpDisplay.ui \
    src/GeneralWidget/GeneralWidget.ui \
    src/LogReplay/LogReplay.ui \
    src/Resources/InputAndNameTemplateWidget.ui \
    src/Resources/MultipleInputTemplateWidget.ui \
    src/Resources/Stopwatch.ui \
    src/Resources/adcwidget.ui \
    src/Resources/configslist.ui \
    src/Resources/fcpsignaldisplayvalue.ui \
    src/Resources/ledandtemperaturewidget.ui \
    src/Resources/nameValueInputTemplate.ui \
    src/SingleInverter.ui \
    src/Tail/Report.ui \
    src/WindowMain.ui \
    src/aboutdialog.ui \
    src/fcp-widgets/fcpdocs.ui \
    src/lap_time.ui \
    src/Custom_Lap.ui \
    src/Resources/filterwidget.ui \
    src/Resources/ledform.ui \
    src/Resources/plot_filter_widget.ui \
    src/COM_LogConvert.ui \
    src/COM_Select.ui \
    src/Console.ui \
    src/Console_makeid.ui \
    src/DevStatus.ui \
    src/General.ui \
    src/General_Details.ui \
    src/General_Details_PotProgress.ui \
    src/Inverters.ui \
    src/Isabelle.ui \
    src/ccu.ui \
    src/ccu_indiv.ui \
    src/comselectline.ui \
    src/isa_config.ui \
    src/run_laps.ui \
    src/single_lap.ui \
    src/testwidget.ui \
    src/pcb_version.ui \
    src/iib_motor_control.ui \
    src/report_filters.ui \
    src/report_indiv_filter.ui \
    src/timeline/timeline.ui \
    src/versionline.ui \
    src/versions.ui

RESOURCES += \
    ass/Assets.qrc \

DISTFILES += \
    Makefile \
    ass/Backgrounds/Captura de ecrã 2018-12-06, às 05.03.09.png \
    ass/Backgrounds/FSTelemetryBackground.png \
    ass/Backgrounds/FSTelemetryBackground.psd \
    ass/Backgrounds/car_top.png \
    ass/CustomIcons/FSTLogo.png \
    ass/CustomIcons/Isabelle.png \
    ass/CustomIcons/aero.png \
    ass/CustomIcons/aero_blue.png \
    ass/CustomIcons/aero_resized.png \
    ass/CustomIcons/aero_transparent.png \
    ass/CustomIcons/lap.png \
    ass/CustomIcons/battery.png \
    ass/CustomIcons/car.png \
    ass/CustomIcons/checklist.png \
    ass/CustomIcons/cooling.png \
    ass/CustomIcons/database.png \
    ass/CustomIcons/engine.png \
    ass/CustomIcons/garbage.png \
    ass/CustomIcons/gitlab.png \
    ass/CustomIcons/gitlab.webp \
    ass/CustomIcons/gitlab_blue.png \
    ass/CustomIcons/gitlab_old.png \
    ass/CustomIcons/helmet.png \
    ass/CustomIcons/laboratory-bench.png \
    ass/CustomIcons/layout.png \
    ass/CustomIcons/line-graphic.png \
    ass/CustomIcons/link.png \
    ass/CustomIcons/log.png \
    ass/CustomIcons/log.svg \
    ass/CustomIcons/log_resized.png \
    ass/CustomIcons/logging.png \
    ass/CustomIcons/logging_blue.png \
    ass/CustomIcons/logging_resized.png \
    ass/CustomIcons/logging_transparent.png \
    ass/CustomIcons/mixer.png \
    ass/CustomIcons/s2gstqhycxdrk0o2zft6.webp \
    ass/CustomIcons/save-file-option.png \
    ass/CustomIcons/suspension.png \
    ass/CustomIcons/switch.png \
    ass/CustomIcons/terminal.png \
    ass/CustomIcons/thermometer.png \
    ass/CustomIcons/usb.png \
    ass/CustomIcons/versions.txt \
    ass/CustomIcons/voltmeter.png \
    ass/CustomIcons/wifi.png \
    ass/animated_radio.gif \
    ass/stylesheet.qss \
    ass/table.txt \
    ass/CustomIcons/mjf.jpeg \
    ass/CustomIcons/mjf.png \
    can-ids/table.txt \
    src/GeneralWidget/GeneralWidget.qml

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/Qt-Advanced-Docking-System/installed/lib/ -lqtadvanceddocking
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/Qt-Advanced-Docking-System/installed/lib/ -lqtadvanceddocking
else:unix: LIBS += -L$$PWD/Qt-Advanced-Docking-System/installed/lib/ -lqtadvanceddocking

INCLUDEPATH += $$PWD/Qt-Advanced-Docking-System/installed/include
DEPENDPATH += $$PWD/Qt-Advanced-Docking-System/installed/include
