#!/bin/bash

cp /c/msys64/mingw64/bin/libbrotlicommon.dll .
cp /c/msys64/mingw64/bin/libbrotlidec.dll .
cp /c/msys64/mingw64/bin/libbz2-1.dll .
cp /c/msys64/mingw64/bin/libdouble-conversion.dll .
cp /c/msys64/mingw64/bin/libfreetype-6.dll .
cp /c/msys64/mingw64/bin/libglib-2.0-0.dll .
cp /c/msys64/mingw64/bin/libgraphite2.dll .
cp /c/msys64/mingw64/bin/libharfbuzz-0.dll .
cp /c/msys64/mingw64/bin/libiconv-2.dll .
cp /c/msys64/mingw64/bin/libicudt68.dll .
cp /c/msys64/mingw64/bin/libicuin68.dll .
cp /c/msys64/mingw64/bin/libicuuc68.dll .
cp /c/msys64/mingw64/bin/libintl-8.dll .
cp /c/msys64/mingw64/bin/libpcre-1.dll .
cp /c/msys64/mingw64/bin/libpcre2-16-0.dll .
cp /c/msys64/mingw64/bin/libpng16-16.dll .
cp /c/msys64/mingw64/bin/libprotobuf.dll .
cp /c/msys64/mingw64/bin/libzstd.dll .
cp /c/msys64/mingw64/bin/yaml-cpp.dll .
cp /c/msys64/mingw64/bin/zlib1.dll .
cp /c/msys64/mingw64/bin/libgcc_s_seh-1.dll .
cp /c/msys64/mingw64/bin/libstdc++-6.dll .
cp /c/msys64/mingw64/bin/libwinpthread-1.dll .
cp /c/msys64/usr/lib/qtadvanceddocking.dll .
