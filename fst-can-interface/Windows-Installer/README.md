# CANinterface Windows Installer

This is the instructions on how to maintain the installer and release new builds. 

**THIS IS NOT A GUIDE TO INSTALL THE CANinterface!!!!** For instructions on how to use the installer to install or update check the documentation here: [/docs/install-and-update-Guide.pdf](/docs/install-and-update-Guide.pdf)

The installer is built using the QtInstallerFramework. See the [official documentation](https://doc.qt.io/qtinstallerframework/index.html) for additional questions. More specifically we use the online installer.



There are two components to the installer:

1. **The installer** itself ('.exe' file you download and use to install on a computer)
2. **The repository** (http server with all version files)

Each of these components have a separate version number and don't need to be updated together.



## Online Installer

The installer is just the `.exe` executable file you run on your computer. This executable fetches the files to be installed from the repository online and installs them on the local computer.

**You don't need to create a new installer when you build a new version of the application** since the installer works just the same fetching from the repository the latest version. The installer also creates an updater for updating your application on the local computer.

The settings for the installer are described in `config/config.xml` . For complete documentation on all the option you can change see the [official documentation](https://doc.qt.io/qtinstallerframework/ifw-globalconfig.html).

To generate the installer we could run the following command on the terminal:

```
C:\Qt\Tools\QtInstallerFramework\4.0\bin\binarycreator.exe --online-only -c config\config.xml -p packages installer.exe
```

Notice that it is advised that the package folder also be present.



## Generating New versions

When you want to generate a new version of the CANinterface for distribution you should follow these instruction:

### Compile the CANinterface on windows

1. Make sure your windows has everything needed to compile the interface. If you need help follow these:

   1. You propably want QtCreator installed... just follow instructions on the internet for that

   2. Install [MSYS2](https://www.msys2.org/)

   3. Open the app `MSYS2 MSYS` and install the following packages: 

      ```
      pacman -S mingw-w64-x86_64-yaml-cpp base-devel git mercurial cvs wget p7zip perl ruby python2 mingw-w64-x86_64-cmake mingw-w64-x86_64-ccache
      
      pacman --needed -S mingw-w64-x86_64-toolchain mingw-w64-x86_64-qwt-qt5
      ```

   4. If you desire you can set up QtCreator to use MSYS2 mingw. Instructions [here](https://gist.github.com/Bluexin/1c58d9b6d707f755b66a94314e3dfd32).

   5. You are the able to compile either from QtCreator or/and from the `MSYS2 MinGw 64`

2. Compile the Qt-Advanced-Docking-System and copy dlls to user libraries folder

   1. Navigate to the folder containing the dlls 

      ```
      cd fst-can-interface/Qt-Advanced-Docking-System
      ```

   2. Compile 

      ```
      qmake
      make install
      ```

   3. Copy files to user libs 

      ```
      cp installed/lib/* /usr/lib
      ```

3. Go back and compile CANinterface 

   ```
   cd ..
   qmake
   make strict
   ```

### Create new build and add them to the remote repository

4. Copy the executable file (`.exe`) to the `packages/CANinterface/data` folder

5. Make sure you rename the executable to `CANinterface.exe` if the executable has any other name

6. Run windeployqt to fetch most of the needed libraries. 

    ```
      cd Windows-Installer/packages/CANinterface/data
      windeployqt CANinterface.exe
    ```

7. Get the last libraries missing. A script was created for this called `missing_dll.sh`. Copy it to the folder containing the executable and double click it to execute. There might be some dlls missing. Keep trying to run the CANinterface and reading the error saying the the names of the missing dlls. Just serach for them in the MSYS2 folder and copy them to the data folder.

8. If it is a new version make sure you increment the version number in `packages/CANinterface/meta/package.xml` file

9. Now you need to transfer this packages folder to the computer with the repository. At the time of writing this is the new white windows workstation in covil. Navigate to the Documents folder and you'll see a folder containing a similar packages folder.

10. Replace the packages folder with your new one or add a new one with a different name

11. Open the terminal in that directory and run the following command to update the repository 

     ```
     ..\..\bin\repogen.exe --update-new-components -p packages repository
     ```

     If the folder added has a different name replace the word 'packages' with the folder name!

12. The permissions for the files under repository might have changed specially for the update file. Make sure to change them back following this tutorial: [https://www.youtube.com/watch?v=RHMp2Z7y9sQ](https://www.youtube.com/watch?v=RHMp2Z7y9sQ)

 All done. Now anyone can update it

   
