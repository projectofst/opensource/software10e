# Final Task - FCP Wrapper


This Qt GUI is intended for interfacing/debugging any CAN network through the CAN sniffer or the FST Telemetry System.

Also it should provide means of interfacing with specific modules, currently built around FST10e.

This started out as an interface for the batteries in FST05e in FSG2013, running in a terminal with ncurses (v1.0) This interface was flawed for many reasons and was thrown away in favor of something more modular, feature complete and mostly, something that will last longer than the competition.

Current version is v2.1.0 -- Update.
Modules implemented:

* Console
* Battery (FSAE compliant except for the override function)
* Low Voltage modules of the CAR
* --Need Update

v2.x.x doesn't indicate a final product, but the fact that it's a completely new version in face of v1.0 (thus 2nd gen).

Clarification on version numbers vX.Y.Z

* X: different technology (e.g. inclusion of telemetry systems or offline data analysis)
* Y: new functionality (e.g. new module dedicated to a car subsystem like the battery)
* Z: small updates / bug fixes


## Using the software

Developed for and tested on Arch Linux 64bit.

User should have access rights on serial port being opened. A simple solution in most distributions is to have the user joining the dialout group.

Use latest version of translator.

## Code structure

Code is all under src/ and forms for widgets are under ui/.

Provided makefile allows compilation without QtCreator, but the project should work well with the Creator provided the paths for the builds are set correctly.


## Developing

Any development should be made in a separate branch (rooted in develop) and submitted as a pull request.
Ask for partial access or fork the repository.

Code is tested against Qt 5.8.0 (OpenSource) and gcc 4.8.3.
Code is C++11 compliant.


## Contributing

This program is distributed under GNU GPLv2.

For in team developers the master branch is protected and functions only by merge request.
Currently this patch can only be approved by:

* Miguel Crespo
* João Revés
* João Ruas

Any patches or improvements are welcomed.
The proper way of submitting them should be negotiated by email with current maintainers case by case, including request for direct write access to repository.

## Configuring

To configure the FSTCAN --interface, you will need to start off by cloning the repository at: git@gitlab.com:projectofst/fst-can-interface.git into your computer.

You will also need to install the following packages: qt5-charts, qt5-serialport, qt5-base, qtcreator. If you are using an arch linux destribution, write the following command on your terminal:

```sh
sudo pacman -S qt5-charts qt5-serialport qt5-base qtcreator
```

After you have obtained all the necessary files, you will have to compile by running make on the installation folder.

Open the interface with QTCreator (Open QTCreator > File > Open File or Project > Browse to installation folder > CANinterface.pro).

With the project now open, go to Projects > Build and disable "Shadow build".
Check if you have a debugger configured. If not check FAQ nº2.

If any erros occur during the configuration process please conctact someone from the team.

### Google Protocol Buffers

The FST CAN Interface depends on the google protocol buffers. To install it you will need to install the *protobuf* package, by typing the following command on an arch based OS:

```sh
sudo pacman -S protobuf
```

### yaml-cpp

The FST CAN Interface depends on the yaml-cpp library. To install:

```sh
cd lib/yaml-cpp
rm -rf build
mkdir build
cd build
make ..
make install
```
or
```sh
cd lib && sudo ./install_yaml.sh
```
o

## FAQ

1 - How do i solve "GPGME error: No data" error while installing qt packages?

Run the following commands on your terminal (if you are using arch linux):
```sh
sudo pacman -Syy
```

```sh
sudo pacman -Sy archlinux-keyring man
```
```sh
sudo pacman-key --populate archlinux
```
```sh
sudo pacman-key --refresh-keys
```
```sh
sudo pacman -Sc
```
```sh
sudo pacman -Syyu
```


2 - My debugger isn't configured, what do i do?

* Go to "Manage Kits" > "Debuggers",
* Select "add",
* Browse to /user/bin,
* Select file named "gdb",
* Go to "Kits",
* Select newly added debugger.
