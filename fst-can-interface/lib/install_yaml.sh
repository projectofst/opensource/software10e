#!/bin/bash

cd yaml-cpp
rm -rf build
mkdir build
cd build
cmake ..
make install
cd ..
rm -rf build
