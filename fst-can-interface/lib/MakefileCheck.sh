#!/bin/bash

touch diff.txt

git diff HEAD origin/dev -- ../Makefile > diff.txt

if [ -s diff.txt ]
then
	echo Makefile was changed. If this is intentional, remove this step from CI.
	cat diff.txt
	rm -rf diff.txt
	exit 1
else
	rm -rf diff.txt
	exit 0
fi
