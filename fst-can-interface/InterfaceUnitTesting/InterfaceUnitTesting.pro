QT += testlib
CONFIG += qt warn_on depend_includepath testcase
CONFIG+=  silent

QMAKE_CXXFLAGS+= -Werror

DEFINES = GIT_CURRENT_SHA1="\\\"$(shell git rev-parse HEAD)\\\""
DEFINES += GIT_CURRENT_BRANCH="\\\"$(shell git rev-parse --abbrev-ref HEAD)\\\""
DEFINES += QT_MESSAGELOGCONTEXT

LIBS+= -L/usr/local/lib -lyaml-cpp
TEMPLATE = app

INCLUDEPATH +=\
    can-ids-spec/\
    can-ids-spec/interface-parser/\
    ../lib/\
    ../lib/fcp-cpp/src/\
    ../src/fcp-qt/Exceptions/\
    src/\
    src/fcp-qt/\
    src/fcp-qt/Exceptions/\

QMAKE_CXXFLAGS += -std=c++17

SOURCES +=  tst_fcptest.cpp \
    ../can-ids-spec/interface-parser/signal_parser.c \
    ../src/fcp-qt/Exceptions/NoSuchConfigException.cpp \
    ../src/fcp-qt/Exceptions/badjsonfileformatexception.cpp \
    ../src/fcp-qt/Exceptions/encodinglowerboundexception.cpp \
    ../src/fcp-qt/Exceptions/encodingmethodnotsuportedexception.cpp \
    ../src/fcp-qt/Exceptions/encodingmissingsignalexception.cpp \
    ../src/fcp-qt/Exceptions/encodingoverbitesizeexception.cpp \
    ../src/fcp-qt/Exceptions/encodingtoolittlesignalsgivenexception.cpp \
    ../src/fcp-qt/Exceptions/encodingtoomanysignalsgivenexception.cpp \
    ../src/fcp-qt/Exceptions/encodinguperboundexception.cpp \
    ../src/fcp-qt/Exceptions/jsoncommandnoargsexception.cpp \
    ../src/fcp-qt/Exceptions/jsoncommandnoretsexception.cpp \
    ../src/fcp-qt/Exceptions/jsonexception.cpp \
    ../src/fcp-qt/Exceptions/jsonfiledoesnotexistexception.cpp \
    ../src/fcp-qt/Exceptions/nofileselectedexception.cpp \
    ../src/fcp-qt/Exceptions/nojsondevicesfoundexception.cpp \
    ../src/fcp-qt/Exceptions/nojsonlogsfoundexception.cpp \
    ../src/fcp-qt/Exceptions/nojsonmessagesavailableexception.cpp \
    ../src/fcp-qt/Exceptions/nojsonparsingdoneyetexception.cpp \
    ../src/fcp-qt/Exceptions/nosuchcommandexception.cpp \
    ../src/fcp-qt/Exceptions/nosuchjsoncansignalexception.cpp \
    ../src/fcp-qt/Exceptions/nosuchjsondeviceexception.cpp \
    ../src/fcp-qt/Exceptions/nosuchjsonlogexception.cpp \
    ../src/fcp-qt/Exceptions/nosuchmessageexception.cpp \
    ../src/fcp-qt/cansignal.cpp \
    ../src/fcp-qt/command.cpp \
    ../src/fcp-qt/config.cpp \
    ../src/fcp-qt/device.cpp \
    ../src/fcp-qt/fcpcom.cpp \
    ../src/fcp-qt/jsoncommandarg.cpp \
    ../src/fcp-qt/jsoncommon.cpp \
    ../src/fcp-qt/jsoncomponent.cpp \
    ../src/fcp-qt/jsonlog.cpp \
    ../src/fcp-qt/jsonparser.cpp \
    ../src/fcp-qt/message.cpp \
    ../src/Exceptions/logutils.cpp \
    ../src/Exceptions/helper.cpp \
    ../lib/fcp-cpp/src/fcp_signal.cpp\
    ../lib/fcp-cpp/src/fcp_config.cpp\
    ../lib/fcp-cpp/src/fcp.cpp\
    ../lib/fcp-cpp/src/fcp_message.cpp\
    ../lib/fcp-cpp/src/fcp_log.cpp\
    ../lib/fcp-cpp/src/fcp_device.cpp\
    ../lib/fcp-cpp/src/fcp_command.cpp\
    ../lib/fcp-cpp/src/fcp_argument.cpp


HEADERS += \
    ../can-ids-spec/interface-parser/signal_parser.h \
    ../src/fcp-qt/Exceptions/NoSuchConfigException.hpp \
    ../src/fcp-qt/Exceptions/badjsonfileformatexception.hpp \
    ../src/fcp-qt/Exceptions/encodinglowerboundexception.hpp \
    ../src/fcp-qt/Exceptions/encodingmethodnotsuportedexception.hpp \
    ../src/fcp-qt/Exceptions/encodingmissingsignalexception.hpp \
    ../src/fcp-qt/Exceptions/encodingoverbitesizeexception.hpp \
    ../src/fcp-qt/Exceptions/encodingtoolittlesignalsgivenexception.hpp \
    ../src/fcp-qt/Exceptions/encodingtoomanysignalsgivenexception.hpp \
    ../src/fcp-qt/Exceptions/encodinguperboundexception.hpp \
    ../src/fcp-qt/Exceptions/jsoncommandnoargsexception.hpp \
    ../src/fcp-qt/Exceptions/jsoncommandnoretsexception.hpp \
    ../src/fcp-qt/Exceptions/jsonexception.hpp \
    ../src/fcp-qt/Exceptions/jsonfiledoesnotexistexception.hpp \
    ../src/fcp-qt/Exceptions/nofileselectedexception.hpp \
    ../src/fcp-qt/Exceptions/nojsondevicesfoundexception.hpp \
    ../src/fcp-qt/Exceptions/nojsonlogsfoundexception.hpp \
    ../src/fcp-qt/Exceptions/nojsonmessagesavailableexception.hpp \
    ../src/fcp-qt/Exceptions/nojsonparsingdoneyetexception.hpp \
    ../src/fcp-qt/Exceptions/nosuchcommandexception.hpp \
    ../src/fcp-qt/Exceptions/nosuchjsoncansignalexception.hpp \
    ../src/fcp-qt/Exceptions/nosuchjsondeviceexception.hpp \
    ../src/fcp-qt/Exceptions/nosuchjsonlogexception.hpp \
    ../src/fcp-qt/Exceptions/nosuchmessageexception.hpp \
    ../src/fcp-qt/cansignal.hpp \
    ../src/fcp-qt/command.hpp \
    ../src/fcp-qt/config.hpp \
    ../src/fcp-qt/device.hpp \
    ../src/fcp-qt/fcpcom.hpp \
    ../src/fcp-qt/jsoncommandarg.hpp \
    ../src/fcp-qt/jsoncommon.hpp \
    ../src/fcp-qt/jsoncomponent.hpp \
    ../src/fcp-qt/jsonlog.hpp \
    ../src/fcp-qt/jsonparser.hpp \
    ../src/fcp-qt/message.hpp

DISTFILES += \
    random.json
