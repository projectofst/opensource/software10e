#include <QtTest>
#include <QCoreApplication>
#include "../src/fcp-qt/fcpcom.hpp"
#include <QDebug>

// add necessary includes here

class FCPTest : public QObject
{
    Q_OBJECT

public:
    FCPTest();
    ~FCPTest();

private slots:
    void init();
    void cleanup();
    void initTestCase();
    void cleanupTestCase();
    void badJsonFileFormatTest();
    void noFileSelectedtest();
    void fileDoesNotExistTest();
    void verifySetJsonFile();
    void verifyValidJsonParseTest();
    void getDeviceByNameTest_01();
    void getDeviceByNameTest_02();
    void getDeviceByIdTest();
    void getNonExistentDeviceTest();
    void getLogsTest();
    void getLogTest_01();
    void getLogTest_02();
    void getNonExistentLogTest();
    void getMessageByNameTest();
    void getMessageByIdTest();
    void getNonExistentMessageTest();
    void getDevicesTest();
    void getSignalByNameTest();
    void decodeSignalTest();
    void decodeNonExistantSignalTest();
    void getConfigTest();
    void encodeMessageTest();
    void encondeRequestConfigTest();
    void encondeSetConfigTest();
    void decodeMessageTest();

private:
    FCPCom* _fcp;
    QString _mainPath = QDir::currentPath() + "/../InterfaceUnitTesting/";
    QString _validJsonFilePath = _mainPath + "test.json";
};

FCPTest::FCPTest()
{

}

FCPTest::~FCPTest()
{

}
void FCPTest::init(){
    this->_fcp = new FCPCom(_validJsonFilePath);
}

void FCPTest::initTestCase()
{
}

void FCPTest::cleanup(){
    delete this->_fcp;
}

void FCPTest::cleanupTestCase()
{

}

void FCPTest::verifySetJsonFile(){

    this->_fcp->setFile("abc.json");

    QVERIFY(this->_fcp->getCurrentJsonFile() == "abc.json");
}

void FCPTest::fileDoesNotExistTest(){

    this->_fcp->setFile("abc.json");
    QVERIFY_EXCEPTION_THROWN(this->_fcp->parse(), jsonFileDoesNotExistException);
}

void FCPTest::badJsonFileFormatTest(){

    this->_fcp->setFile(QDir::currentPath() + "/../InterfaceUnitTesting/random.json");
    QVERIFY_EXCEPTION_THROWN(this->_fcp->parse(), badJsonFileFormatException);
}

void FCPTest::noFileSelectedtest(){
    this->_fcp->setFile("");
    QVERIFY_EXCEPTION_THROWN(this->_fcp->parse(), noFileSelectedException);
}

void FCPTest::verifyValidJsonParseTest(){
    this->_fcp->parse();
    QList<Device*> devices = this->_fcp->getDevices();
    QVERIFY(devices.size() > 0);
    QVERIFY(this->_fcp->getLogs().size() > 0);
    QVERIFY(this->_fcp->getCommon()->getName() == "common");
}

void FCPTest::getDeviceByNameTest_01(){
    this->_fcp->parse();

    Device* dev = this->_fcp->getDevice("te");

    QVERIFY(dev->getName() == "te");
    QVERIFY(dev->getId() == 9);
    /* Check if it has these, they will be checked in other tests*/
    QVERIFY(dev->getMessages().size() > 0);
    QVERIFY(dev->getConfigs().size() > 0);
    QVERIFY(dev->getCommands().size() > 0);
}

void FCPTest::getDeviceByNameTest_02(){
    this->_fcp->parse();
    Device* dev = this->_fcp->getDevice("iib");
    QVERIFY(dev->getName() == "iib");
    QVERIFY(dev->getId() == 16);
    /* Check if it has these, they will be checked in other tests*/
    QVERIFY(dev->getMessages().size() > 0);
    QVERIFY(dev->getConfigs().size() > 0);
    QVERIFY(dev->getCommands().size() > 0);
}

void FCPTest::getDeviceByIdTest(){
    this->_fcp->parse();
    Device* dev = this->_fcp->getDevice(9);
    QVERIFY(dev->getName() == "te");
    QVERIFY(dev->getId() == 9);
    /* Check if it has these, they will be checked in other tests*/
    QVERIFY(dev->getMessages().size() > 0);
    QVERIFY(dev->getConfigs().size() > 0);
    QVERIFY(dev->getCommands().size() > 0);
}

void FCPTest::getNonExistentDeviceTest(){
    this->_fcp->parse();
    QVERIFY_EXCEPTION_THROWN(this->_fcp->getDevice("NonExistingDevice"), noSuchJsonDeviceException);
    QVERIFY_EXCEPTION_THROWN(this->_fcp->getDevice(32), noSuchJsonDeviceException);
}

void FCPTest::getLogsTest(){
    this->_fcp->parse();
    QList<JsonLog*> logs = this->_fcp->getLogs();
    QVERIFY(logs.size() > 0);
    QVERIFY(logs.first() != nullptr);
}

void FCPTest::getLogTest_01(){
    this->_fcp->parse();
    JsonLog* log = this->_fcp->getLog("wrong_cfg_id");

    QVERIFY(log != nullptr);
    QVERIFY(log->getId() == 1);
    QVERIFY(log->getName() == "wrong_cfg_id");
    QVERIFY(log->getNArgs() == 0);
    QVERIFY(log->getComment() == "");
    QVERIFY(log->getString() == "Cfg code was not found");
}

void FCPTest::getLogTest_02(){
    this->_fcp->parse();
    JsonLog* log = this->_fcp->getLog("rtd_off");

    QVERIFY(log != nullptr);
    QVERIFY(log->getId() == 10);
    QVERIFY(log->getName() == "rtd_off");
    QVERIFY(log->getNArgs() == 1);
    QVERIFY(log->getComment() == "rtd off");
    QVERIFY(log->getString() == "RTD Off");
}

void FCPTest::getNonExistentLogTest(){
    this->_fcp->parse();
    QVERIFY_EXCEPTION_THROWN(this->_fcp->getLog("NonExistingLog"), noSuchJsonLogException);
}

void FCPTest::getMessageByNameTest(){
    this->_fcp->parse();

    Message* msg = this->_fcp->getMessage("tach_right");
    QVERIFY(msg != nullptr);
    QVERIFY(msg->getId() == 35);
    QVERIFY(msg->getDlc() == 6);
    QVERIFY(msg->getName() == "tach_right");
    QVERIFY(msg->getSignals().size() > 0);
    QVERIFY(msg->getFrequency() == 0);
}

void FCPTest::getMessageByIdTest(){
    this->_fcp->parse();

    Message* msg = this->_fcp->getMessage(16, 37);

    QVERIFY(msg != nullptr);
    QVERIFY(msg->getId() == 37);
    QVERIFY(msg->getDlc() == 8);
    QVERIFY(msg->getName() == "iib_info");
    QVERIFY(msg->getSignals().size() > 0);
    QVERIFY(msg->getFrequency() == 500);
}

void FCPTest::getNonExistentMessageTest(){
    this->_fcp->parse();

    QVERIFY_EXCEPTION_THROWN(this->_fcp->getMessage("NonExistentMessage"), noSuchMessageException);
}

void FCPTest::getDevicesTest(){
    this->_fcp->parse();
    QVERIFY(this->_fcp->getMessages(16).size() != 0);
}

void FCPTest::getSignalByNameTest(){
    this->_fcp->parse();
    CanSignal* sig = this->_fcp->getSignal("ntc5_left");

    QVERIFY(sig != nullptr);
    QVERIFY(sig->getName() == "ntc5_left");
    QVERIFY(sig->getStart() == 0);
    QVERIFY(sig->getLength() == 16);
    QVERIFY(sig->getScale() == 0.1);
    QVERIFY(sig->getOffset() == 0.0);
    QVERIFY(sig->getUnit() == "");
    QVERIFY(sig->getMinValue() == 0.0);
    QVERIFY(sig->getMaxValue() == 0.0);
    QVERIFY(sig->getType() == FCP_UNSIGNED);
    QVERIFY(sig->getByteOrder() == FCP_LITTLE);
    QVERIFY(sig->getMux() == "");
    QVERIFY(sig->getMuxCount() == 1);
}

void FCPTest::decodeSignalTest(){
    this->_fcp->parse();
    CANmessage msg;
    msg.candata.dev_id = 9;
    msg.candata.msg_id = 37;
    msg.candata.data[0] = 10;
    msg.candata.data[1] = 20;
    msg.candata.data[2] = 30;
    msg.candata.data[3] = 40;
    QVERIFY(this->_fcp->decodeUint64FromSignal(msg, "te_accel_t") == 10);
    QVERIFY(this->_fcp->decodeUint64FromSignal(msg, "te_brk_press") == 20);
    QVERIFY(this->_fcp->decodeUint64FromSignal(msg, "te_brk_ele") == 30);

}

void FCPTest::decodeNonExistantSignalTest(){
    this->_fcp->parse();
    CANmessage msg;
    msg.candata.dev_id = 9;
    msg.candata.msg_id = 37;
    msg.candata.data[0] = 10;
    msg.candata.data[1] = 20;
    msg.candata.data[2] = 30;
    msg.candata.data[3] = 40;
    QVERIFY_EXCEPTION_THROWN(this->_fcp->decodeUint64FromSignal(msg, "NonExistantSignal"), noSuchJsonCanSignalException);
}

void FCPTest::getConfigTest(){
    this->_fcp->parse();
    Config* cfg = this->_fcp->getConfig("te", "apps0_vcc");

    QVERIFY(cfg->getName() == "apps0_vcc");
    QVERIFY(cfg->getId() == 3);
    QVERIFY(cfg->getComment() == "APPS0 VCC limit");
}

void FCPTest::encodeMessageTest(){
    this->_fcp->parse();
    QMap<QString, double> map;
    map.insert("ntc1_right", 10);
    map.insert("ntc2_right", 20);
    map.insert("ntc3_right", 30);
    map.insert("ntc4_right", 40);
    CANmessage msg = this->_fcp->encodeMessage("ccu_right", "temp1_right", map);

    QVERIFY(msg.candata.dev_id == 25);
    QVERIFY(msg.candata.msg_id == 33);
    QVERIFY(msg.candata.dlc == 8);
    QVERIFY(msg.candata.data[0] == 100);
    QVERIFY(msg.candata.data[1] == 200);
    QVERIFY(msg.candata.data[2] == 300);
    QVERIFY(msg.candata.data[3] == 400);
}

void FCPTest::encondeRequestConfigTest(){
    this->_fcp->parse();
    CANmessage msg = this->_fcp->encodeGetConfig("interface", "te", "hb_treshold");
    QVERIFY(msg.candata.dev_id == 31);
    QVERIFY(msg.candata.msg_id == 3);
    QVERIFY(msg.candata.dlc == 8);
    QVERIFY(msg.candata.data[0] == (9 + (1 << 8)));
}
void FCPTest::encondeSetConfigTest(){
    this->_fcp->parse();
    CANmessage msg = this->_fcp->encodeSetConfig("interface", "te", "hb_treshold", 100);
    QVERIFY(msg.candata.dev_id == 31);
    QVERIFY(msg.candata.msg_id == 5);
    QVERIFY(msg.candata.dlc == 8);
    QVERIFY(msg.candata.data[0] == (9 + (1 << 8)));
    QVERIFY(msg.candata.data[1] == 100);
}

void FCPTest::decodeMessageTest(){
    this->_fcp->parse();
    CANmessage msg;
    msg.candata.dev_id = 9;
    msg.candata.msg_id = 37;
    msg.candata.dlc = 8;
    msg.candata.data[0] = 10;
    msg.candata.data[1] = 20;
    msg.candata.data[2] = 30;
    msg.candata.data[3] = 40;

    QPair<QString, QMap<QString, double>> ret;

    ret = this->_fcp->decodeMessage(msg);

    QVERIFY(ret.first == "te_forces");
    QVERIFY(ret.second.value("te_accel_t") == 10);
    QVERIFY(ret.second.value("te_brk_ele") == 30);
    QVERIFY(ret.second.value("te_brk_press") == 20);
}


QTEST_APPLESS_MAIN(FCPTest)

#include "tst_fcptest.moc"
