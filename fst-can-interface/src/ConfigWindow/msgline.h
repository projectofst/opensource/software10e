#ifndef MSGLINE_H
#define MSGLINE_H

#include "ConfigWindow/fcprunnable.h"
#include "ConfigWindow/msgedit.h"
#include <QMessageBox>
#include <QTimer>
#include <QWidget>

namespace Ui {
class MsgLine;
}

class MsgLine : public FcpRunnable {
    Q_OBJECT

public:
    explicit MsgLine(QWidget* parent, FCPCom* fcp);
    ~MsgLine();
    QMap<QString, QString>* get_configs() override;
    QList<QVariant> getParameterValue() override;
    MsgEdit* get_edit();
    QString getDevice();
    QString getMessage();
    QString getValue();
    int getPeriod();
    bool getSignalType();
    void loadParameterValue(QList<QVariant> list) override;
    void setEditable(bool) override;
    void setPeriod(int period);
    void setMessage(QString);
    void run(void) override;
    void setDevice(QString);
    void setValue(double);
    void hide_device();

public slots:
    void update();

private slots:
    void on_deleteButton_clicked();
    void on_details_Button_clicked();
    void on_run_Button_clicked();
    void on_state_clicked();
    void on_value_line_textEdited(const QString& arg1);

private:
    Ui::MsgLine* ui;
    MsgEdit* msgEdit = NULL;
    FCPCom* fcp = NULL;
    QTimer* timer;
    QMap<QString, double> argsMap;
    void callback(void);
    bool repeat = false;
    unsigned period;
};

#endif // MSGLINE_H
