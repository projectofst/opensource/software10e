#include "msgargs.h"
#include "ui_msgargs.h"

/**
 * File explanation:
 * When sending a message with the RUN tab you have 3 classes that are being used, MsgLine, MsgEdit and MsgArgs
 * This one is the latter.
 *
 * This has a User interface that consists of one label and one spin box to choose the value of one CANsignal, one MsgEdit can contain more than one
 * MsgArgs depending on the number of signals that a certain message contains
 *
 * The function that exists in this Class are straight forward being mostly setter and getters, or function that belongs to slots
 */

MsgArgs::MsgArgs(QWidget* parent, double minValue, double maxValue, QString name)
    : QWidget(parent)
    , ui(new Ui::MsgArgs)
{
    ui->setupUi(this);

    this->maxValue = maxValue;

    this->ui->signalBox->setMinimum(minValue);
    this->ui->signalBox->setMaximum(maxValue);

    this->name = name;
    return;
}

MsgArgs::~MsgArgs()
{
    delete ui;
    return;
}

QString MsgArgs::getName()
{
    return this->name;
}

double MsgArgs::getSignalValue()
{
    return this->signalValue;
}

QPair<QString, double> MsgArgs::getValues()
{
    return qMakePair(this->name, this->signalValue);
}

void MsgArgs::on_signalBox_valueChanged(double arg1)
{
    this->signalValue = arg1;
    return;
}

void MsgArgs::setValue(double signalValue)
{
    this->signalValue = signalValue;
    this->ui->signalBox->setValue(signalValue);
    return;
}
