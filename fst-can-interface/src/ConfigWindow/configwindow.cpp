#include "configwindow.h"
#include "ui_configwindow.h"

#include <QDir>
#include <QFileDialog>
#include <QtConcurrent>
#include <QtWidgets>

#include <fstream>
#include <iostream>

#include "COM.hpp"
#include "ConfigWindow/cmdline.h"
#include "qtyaml.h"

using namespace QtConcurrent;

ConfigWindow::ConfigWindow(QWidget* parent, FCPCom* fcp, Helper*, ConfigManager* config_manager)
    : UiWindow(parent, fcp)
    , ui(new Ui::ConfigWindow)
{
    ui->setupUi(this);

    this->config_manager = config_manager;

    this->config_dialog = new ConfigManagerDialog(nullptr, this, this->config_manager);

    this->fcp = fcp;

    this->setupButton(ui->add_button, QString("Cmd"), QIcon(":/CustomIcons/CustomIcons/add.png"));
    this->setupButton(ui->set_button, QString("Set"), QIcon(":/CustomIcons/CustomIcons/add.png"));
    this->setupButton(ui->msg_button, QString("msg"), QIcon(":/CustomIcons/CustomIcons/add.png"));
    this->setupButton(ui->config_button, QString("Config Manager"), QIcon(":/Assets/CustomIcons/settings.png"));

    try {
        this->ans_get = this->fcp->getCommon()->getMessage("ans_get")->getId();
    } catch (...) {
        QMessageBox::warning(this, tr("FST CAN Interface"), tr("Failed to load ans_get"));
    }

    try {
        this->ans_set = this->fcp->getCommon()->getMessage("ans_set")->getId();
    } catch (...) {
        QMessageBox::warning(this, tr("FST CAN Interface"), tr("Failed to load ans_set"));
    }

    try {
        this->ret_cmd = this->fcp->getCommon()->getMessage("return_cmd")->getId();
    } catch (...) {
        QMessageBox::warning(this, tr("FST CAN Interface"), tr("Failed to load ret_cmd"));
    }
}

void ConfigWindow::setupButton(QToolButton* button, QString text, QIcon icon)
{
    button->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    button->setIcon(icon);
    button->setText(text);
    button->setIconSize(QSize(15, 15));
}

ConfigWindow::~ConfigWindow()
{
    delete ui;
}

void ConfigWindow::add_cmd(QString* dev, QString* cmd, QList<QVariant>* parameters)
{
    auto cmd_line = new CmdLine(this, this->fcp);
    this->ui->lines_layout->addWidget(cmd_line);
    lines.append(cmd_line);

    connect(cmd_line, &CmdLine::send_can, this, &ConfigWindow::send_can);
    connect(cmd_line, &CmdLine::deleted, this, &ConfigWindow::remove_line);
    connect(this, &ConfigWindow::dist_can, cmd_line, &CmdLine::receive_can);

    auto cmd_configs = cmd_line->get_configs();

    if (dev != nullptr && cmd != nullptr && parameters != nullptr) {
        cmd_configs->insert("cmd_name", *cmd);
        cmd_configs->insert("dst_name", *dev);
        cmd_line->loadParameterValue(*parameters);
        cmd_line->update();
    }

    cmd_line->setEditable(true);
}

void ConfigWindow::add_set(QString* dev, QString* cfg, QList<QVariant>* parameters)
{
    auto set_line = new SetLine(this, this->fcp);
    this->ui->lines_layout->addWidget(set_line);
    lines.append(set_line);

    connect(set_line, &FcpRunnable::send_can, this, &ConfigWindow::send_can);
    connect(set_line, &FcpRunnable::deleted, this, &ConfigWindow::remove_line);

    auto set_configs = set_line->get_configs();

    if (dev != nullptr && cfg != nullptr && parameters != nullptr) {
        set_configs->insert("cfg_name", *cfg);
        set_configs->insert("dst_name", *dev);
        set_line->loadParameterValue(*parameters);
        set_line->update();
    }

    set_line->setEditable(true);

    return;
}

void ConfigWindow::add_msg(QString* dev, QString* msg, QString period, QList<QVariant>* parameters)
{
    auto msg_line = new MsgLine(this, this->fcp);
    this->ui->lines_layout->addWidget(msg_line);
    lines.append(msg_line);

    connect(msg_line, &FcpRunnable::send_can, this, &ConfigWindow::send_can);
    connect(msg_line, &FcpRunnable::deleted, this, &ConfigWindow::remove_line);

    auto msg_configs = msg_line->get_configs();

    if (dev != nullptr && msg != nullptr && parameters != nullptr) {
        msg_configs->insert("dev_name", *dev);
        msg_configs->insert("msg_name", *msg);
        msg_configs->insert("period", period);

        msg_line->setDevice(*dev);
        msg_line->setMessage(*msg);
        msg_line->setPeriod(period.toUInt());
        msg_line->loadParameterValue(*parameters);
        msg_line->update();
    }

    msg_line->setEditable(true);

    return;
}

void ConfigWindow::process_CAN_message(CANmessage msg)
{
    if (msg.candata.msg_id == ans_get || msg.candata.msg_id == ans_set || msg.candata.msg_id == ret_cmd) {

        try {
            auto decoded = fcp->decodeMessage(msg);
            emit dist_can(decoded, msg.candata.dev_id, msg.candata.msg_id);

        } catch (...) {
            qDebug() << "Failed to decode msg";
        }
    }

    return;
}

void ConfigWindow::load_state(QMap<QString, QVariant> config)
{
    QList<QVariant> cmd_list = config["commands"].toList();

    // Clear list

    foreach (auto line, lines) {
        remove_line(line);
    }

    if (config.contains("load_default_ui")) {
        return;
    }

    foreach (auto cmd, cmd_list) {
        QMap<QString, QVariant> command = cmd.toMap();

        auto parameters_list = command["parameters_values"].toList();

        if (command["type"].toString() == "Cmd") {
            QString cmd_name = command["cmd_name"].toString();
            QString dst_name = command["dst_name"].toString();
            this->add_cmd(&dst_name, &cmd_name, &parameters_list);
        }

        if (command["type"].toString() == "Set") {
            QString dst_name = command["dst_name"].toString();
            QString cfg_name = command["cfg_name"].toString();
            this->add_set(&dst_name, &cfg_name, &parameters_list);
        }

        if (command["type"].toString() == "Msg") {
            QString dev_name = command["dev_name"].toString();
            QString msg_name = command["msg_name"].toString();
            QString period = command["period"].toString();
            this->add_msg(&dev_name, &msg_name, period, &parameters_list);
        }
    }
}

void ConfigWindow::export_state()
{
    QMap<QString, QVariant> export_config;
    QList<QVariant> cmd_list;

    foreach (auto line, lines) {
        QMap<QString, QVariant> cmd_map;

        auto config = *line->get_configs();

        cmd_map["parameters_values"] = line->getParameterValue();

        foreach (auto key, config.keys()) {
            cmd_map[key] = config[key];
        }

        cmd_list.append(cmd_map);
    }

    export_config["commands"] = cmd_list;

    config_dialog->registerConfig(this->objectName(), export_config);
}

void ConfigWindow::on_add_button_clicked()
{
    this->add_cmd();
}

void ConfigWindow::on_set_button_clicked()
{
    this->add_set();
}

void ConfigWindow::on_msg_button_clicked()
{
    this->add_msg();
}
void ConfigWindow::send_can(CANmessage msg)
{
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void ConfigWindow::on_edit_button_clicked(bool checked)
{
    if (!checked) {
        this->ui->edit_button->setText("↑");
    } else {
        this->ui->edit_button->setText("↓");
    }

    this->ui->toolbar->setVisible(!checked);

    for (auto line : lines) {
        line->setEditable(!checked);
    }
}

void ConfigWindow::remove_line(FcpRunnable* line)
{
    this->lines.removeAll(line);
    delete line;
}

void ConfigWindow::on_run_all_button_clicked()
{
    QProgressDialog dialog;
    dialog.setLabelText(QString("Running...").arg(QThread::idealThreadCount()));

    auto future = QtConcurrent::run([](QList<FcpRunnable*> lines, QProgressDialog* dialog) {
        int i = 0;
        for (auto line : lines) {
            line->run();
            dialog->setValue(i++);
            QThread::msleep(100);
        }
    },
        this->lines, &dialog);

    QFutureWatcher<void> futureWatcher;
    futureWatcher.setFuture(future);

    QObject::connect(&futureWatcher, &QFutureWatcher<void>::finished, &dialog, &QProgressDialog::reset);
    QObject::connect(&dialog, &QProgressDialog::canceled, &futureWatcher, &QFutureWatcher<void>::cancel);

    dialog.setRange(0, this->lines.count());
    dialog.setValue(0);
    dialog.exec();
    futureWatcher.waitForFinished();
}

void ConfigWindow::on_config_button_clicked()
{
    this->config_dialog->show();
}
