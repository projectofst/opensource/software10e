#ifndef CONFIGLINE_H
#define CONFIGLINE_H

#include <QWidget>

namespace Ui {
class ConfigLine;
}

class ConfigLine : public QWidget {
    Q_OBJECT

public:
    explicit ConfigLine(QWidget* parent = nullptr);
    ~ConfigLine();

private:
    Ui::ConfigLine* ui;
};

#endif // CONFIGLINE_H
