﻿#ifndef SETLINE_H
#define SETLINE_H

#include <QTimer>
#include <QWidget>

#include "ConfigWindow/fcprunnable.h"
#include "ConfigWindow/setedit.h"

namespace Ui {
class SetLine;
}

class SetLine : public FcpRunnable {
    Q_OBJECT

public:
    explicit SetLine(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~SetLine();
    void run(void) override;
    void setEditable(bool) override;
    QMap<QString, QString>* get_configs() override;
    QList<QVariant> getParameterValue() override;
    void loadParameterValue(QList<QVariant> list) override;
    SetEdit* get_edit();
    void hide_device();
    void setDevice(QString);
    void setConfig(QString);
    QString getDevice();
    QString getConfig();
    void setValue(double);
    QString getValue();
    int getPeriod();
    void setPeriod(int period);

public slots:
    void update();

private slots:
    void on_delete_button_clicked();
    void on_details_button_clicked();
    void on_run_button_clicked();

    void on_period_SpinBox_valueChanged(int arg1);

    void on_reload_Button_clicked();

private:
    Ui::SetLine* ui;
    FCPCom* fcp;
    SetEdit* set_edit;
    QTimer* timer;
    int period;
};

#endif // SETLINE_H
