#ifndef CMDEDIT_H
#define CMDEDIT_H

#include <QMap>
#include <QWidget>

#include "fcpcom.hpp"

namespace Ui {
class CmdEdit;
}

class CmdEdit : public QWidget {
    Q_OBJECT

public:
    explicit CmdEdit(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~CmdEdit();
    QMap<QString, QString>* get_configs();

private:
    Ui::CmdEdit* ui;
    FCPCom* fcp;
    QMap<QString, QString> configs;

signals:
    void edited();

private slots:
    void on_cmd_edit_currentTextChanged(const QString& arg1);
    void on_dst_edit_currentTextChanged(const QString& arg1);
    void on_arg1_period_edit_textChanged(const QString& arg1);
    void on_arg2_period_edit_textChanged(const QString& arg1);
    void on_arg3_period_edit_textChanged(const QString& arg1);
    void on_arg1_wave_edit_currentTextChanged(const QString& arg1);
    void on_arg2_wave_edit_currentTextChanged(const QString& arg1);
    void on_arg3_wave_edit_currentTextChanged(const QString& arg1);
};

#endif // CMDEDIT_H
