#include "setedit.h"
#include "ui_setedit.h"

SetEdit::SetEdit(QWidget* parent, FCPCom* fcp)
    : QWidget(parent)
    , ui(new Ui::SetEdit)
{
    ui->setupUi(this);

    this->fcp = fcp;

    for (auto dev : fcp->getDevices()) {
        this->ui->dev_edit->addItem(dev->getName());
    }

    this->configs["type"] = "Set";
}

SetEdit::~SetEdit()
{
    delete ui;
}

void SetEdit::set_device(QString device)
{
    this->on_dev_edit_currentTextChanged(device);
}

void SetEdit::set_config(QString config)
{
    this->on_cfg_edit_currentTextChanged(config);
}

QString SetEdit::getDevice()
{
    return this->configs["dst_name"];
}

QString SetEdit::getConfig()
{
    return this->configs["cfg_name"];
}

QMap<QString, QString>* SetEdit::get_configs()
{
    return &this->configs;
}

void SetEdit::on_dev_edit_currentTextChanged(const QString& arg1)
{
    Device* dev;
    try {
        dev = this->fcp->getDevice(arg1);
    } catch (...) {
        return;
    }

    this->configs["dst_name"] = arg1;

    this->ui->cfg_edit->clear();

    for (auto cfg : dev->getConfigs()) {
        this->ui->cfg_edit->addItem(cfg->getName());
    }

    emit edited();
}

void SetEdit::on_cfg_edit_currentTextChanged(const QString& arg1)
{
    this->configs["cfg_name"] = arg1;
    emit edited();
}
