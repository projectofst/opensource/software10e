#include "setline.h"
#include "ui_setline.h"

#include <QMessageBox>

SetLine::SetLine(QWidget*, FCPCom* fcp)
    : FcpRunnable()
    , ui(new Ui::SetLine)
{
    ui->setupUi(this);

    this->fcp = fcp;
    timer = new QTimer();

    this->setPeriod(1);
    this->ui->period_SpinBox->setMinimum(0);
    this->ui->period_SpinBox->setMaximum(INT32_MAX);

    set_edit = new SetEdit(nullptr, fcp);
    connect(set_edit, &SetEdit::edited, this, &SetLine::update);
    connect(timer, &QTimer::timeout, this, &SetLine::run);
}

QList<QVariant> SetLine::getParameterValue()
{
    QList<QVariant> args_list;
    args_list.append(ui->value_edit->text());
    return args_list;
}

void SetLine::loadParameterValue(QList<QVariant> list)
{
    ui->value_edit->setText(list[0].toString());
}

SetLine::~SetLine()
{
    delete ui;
}

SetEdit* SetLine::get_edit()
{
    return this->set_edit;
}

void SetLine::hide_device()
{
    this->ui->dst_label->hide();
}

void SetLine::run()
{
    auto configs = *this->get_configs();
    try {
        qDebug() << configs["dst_name"];
        qDebug() << configs["cfg_name"];
        CANmessage msg = this->fcp->encodeSetConfig(
            "interface",
            configs["dst_name"],
            configs["cfg_name"],
            this->ui->value_edit->text().toUInt());

        emit send_can(msg);

    } catch (...) {
        QMessageBox::warning(
            this,
            tr("FST CAN Interface"),
            tr(QString("Failed to request %1 from %2").arg(configs["cfg_name"], configs["dst_name"]).toLocal8Bit().data()));
    }
    return;
}

void SetLine::setEditable(bool checked)
{
    this->ui->details_button->setVisible(checked);
    this->ui->delete_button->setVisible(checked);
    this->ui->value_label->setVisible(!checked);
    this->ui->run_button->setVisible(!checked);
    this->ui->period_SpinBox->setVisible(!checked);
    this->ui->period_label->setVisible(!checked);
    this->ui->reload_Button->setVisible(!checked);

    return;
}

QMap<QString, QString>* SetLine::get_configs()
{
    return this->set_edit->get_configs();
}

void SetLine::on_details_button_clicked()
{
    this->set_edit->show();
    this->set_edit->raise();
}

void SetLine::update()
{
    auto configs = *this->get_configs();
    this->ui->cfg_label->setText(configs["cfg_name"]);
    this->ui->dst_label->setText(configs["dst_name"]);
}

void SetLine::setDevice(QString device)
{
    this->set_edit->set_device(device);
}

void SetLine::setConfig(QString config)
{
    this->set_edit->set_config(config);
}

QString SetLine::getDevice()
{
    return this->set_edit->getDevice();
}

QString SetLine::getConfig()
{
    return this->set_edit->getConfig();
}

void SetLine::setValue(double value)
{
    this->ui->value_label->setText(QString::number((uint32_t)(int32_t)value));
}

QString SetLine::getValue()
{
    return this->ui->value_edit->text();
}

void SetLine::on_run_button_clicked()
{
    this->run();
}

void SetLine::on_delete_button_clicked()
{
    emit deleted(this);
}

int SetLine::getPeriod()
{
    return this->period;
}

void SetLine::setPeriod(int period)
{
    this->period = period;
    this->ui->period_SpinBox->setValue(period);
    return;
}

void SetLine::on_period_SpinBox_valueChanged(int arg1)
{
    this->period = arg1;
    return;
}

void SetLine::on_reload_Button_clicked()
{
    if (this->timer->isActive())
        this->timer->stop();
    else
        this->timer->start(this->getPeriod());
    return;
}
