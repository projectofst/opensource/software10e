﻿#include "msgedit.h"
#include "ui_msgedit.h"

/**
 * File explanation:
 * When sending a message with the RUN tab you have 3 classes that are being used, MsgLine, MsgEdit and MsgArgs
 * This one is the MsgEdit class.
 *
 * This class is a UI, you can see this class is used when you are Changing the device or the message that you want to send,
 * it knows the number of signals a certain message contains and for it contains a MsgArgs object to every single one.
 *
 * The functions that exist in this Class are straight forward being mostly setter and getters, or functions that belong to slots
 */

MsgEdit::MsgEdit(QWidget* parent, FCPCom* fcp)
    : QWidget(parent)
    , ui(new Ui::MsgEdit)
{
    ui->setupUi(this);

    this->fcp = fcp;

    for (auto dev : fcp->getDevices())
        this->ui->deviceBox->addItem(dev->getName());

    this->messages["type"] = "Msg";
}

MsgEdit::~MsgEdit()
{
    delete ui;
}

QMap<QString, QString>* MsgEdit::get_messages()
{
    return &this->messages;
}

void MsgEdit::set_device(QString name)
{
    this->on_deviceBox_currentTextChanged(name);
    this->ui->deviceBox->setCurrentText(name);
}

void MsgEdit::set_message(QString name)
{
    this->on_messageBox_currentTextChanged(name);
    this->ui->messageBox->setCurrentText(name);
}

QString MsgEdit::getDevice()
{
    return this->messages["dev_name"];
}

QString MsgEdit::getMessage()
{
    return this->messages["msg_name"];
}

QMap<QString, double> MsgEdit::getArgs()
{
    QMap<QString, double> argsVector;

    for (auto const& arg : this->argslist) {
        argsVector.insert(arg->getName(), arg->getSignalValue());
    }

    return argsVector;
}

void MsgEdit::set_value(QString name, double value)
{
    MsgArgs* arg = this->getArg(name);

    if (arg == NULL)
        return;

    arg->setValue(value);

    return;
}

MsgArgs* MsgEdit::getArg(QString name)
{
    foreach (MsgArgs* arg, this->argslist) {
        if (arg->getName() == name)
            return arg;
    }
    return NULL;
}

void MsgEdit::on_deviceBox_currentTextChanged(const QString& arg1)
{
    Device* dev;
    try {
        dev = this->fcp->getDevice(arg1);
    } catch (...) {
        return;
    }

    this->messages["dev_name"] = arg1;

    this->ui->messageBox->clear();

    for (auto cfg : dev->getMessages()) {
        this->ui->messageBox->addItem(cfg->getName());
    }

    emit edited();
}

void MsgEdit::on_messageBox_currentTextChanged(const QString& arg1)
{
    this->messages["msg_name"] = arg1;
    Device* dev = NULL;
    Message* msg = NULL;
    MsgArgs* args = NULL;

    if (!this->argslist.empty())
        this->argslist.clear();

    while (this->ui->argsTab->count()) {
        this->ui->argsTab->removeTab(this->ui->argsTab->count() - 1);
    }

    this->args = 0;

    try {
        dev = this->fcp->getDevice(this->messages["dev_name"]);
        msg = dev->getMessage(this->messages["msg_name"]);

        this->canList = msg->getSignals();

        for (auto const& i : this->canList) {
            this->args += 1;
            args = new MsgArgs(this, i->getMinValue(), i->getMaxValue(), i->getName());
            this->ui->argsTab->insertTab(this->args, args, i->getName());
            this->argslist.append(args);
        }
    } catch (...) {
        return;
    }
    emit edited();
}
