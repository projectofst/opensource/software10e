#include "msgline.h"
#include "ui_msgline.h"

#include <QMessageBox>

typedef QPair<QString, double> MyType;
Q_DECLARE_METATYPE(MyType);

MsgLine::MsgLine(QWidget*, FCPCom* fcp)
    : FcpRunnable()
    , ui(new Ui::MsgLine)
{
    ui->setupUi(this);

    this->fcp = fcp;
    this->msgEdit = new MsgEdit(nullptr, fcp);
    connect(this->msgEdit, &MsgEdit::edited, this, &MsgLine::update);

    this->timer = new QTimer();
    connect(this->timer, &QTimer::timeout, this, &MsgLine::on_run_Button_clicked);

    this->show();
}

MsgLine::~MsgLine()
{
    delete ui;
    delete this->timer;
}

QList<QVariant> MsgLine::getParameterValue()
{
    QList<QVariant> args_list;

    foreach (QString key, this->msgEdit->getArgs().keys()) {
        args_list.append(key);
        args_list.append(this->msgEdit->getArgs()[key]);
    }
    return args_list;
}

void MsgLine::loadParameterValue(QList<QVariant> list)
{
    double signalValue;
    QString name;
    QString teste;
    int i;

    for (i = 0; i < list.size(); i += 2) {

        name = list[i].toString();
        teste = list[i + 1].toString();
        signalValue = list[i + 1].toDouble();

        this->msgEdit->set_value(name, signalValue);
    }

    return;
}

void MsgLine::setPeriod(int period)
{
    this->period = period;
    this->on_value_line_textEdited(QString::number(period));
    this->ui->value_line->setText(QString::number(period));
}

MsgEdit* MsgLine::get_edit()
{
    return this->msgEdit;
}

void MsgLine::hide_device()
{
    this->ui->device_label->hide();
}

void MsgLine::run()
{
    QMap<QString, double> args;
    auto configs = *this->get_configs();
    try {
        args = this->msgEdit->getArgs();
        CANmessage msg = this->fcp->encodeMessage(this->msgEdit->getDevice(), this->msgEdit->getMessage(), args);
        emit send_can(msg);

    } catch (...) {
        QMessageBox::warning(
            this,
            tr("FST CAN Interface"),
            tr(QString("Failed to request %1 from %2").arg(configs["dev_name"], configs["msg_name"]).toLocal8Bit().data()));
    }
    return;
}

void MsgLine::setEditable(bool checked)
{
    this->ui->details_Button->setVisible(checked);
    this->ui->deleteButton->setVisible(checked);
    this->ui->value_line->setVisible(!checked);
    this->ui->run_Button->setVisible(!checked);
    this->ui->state->setVisible(!checked);
    this->ui->label->setVisible(!checked);
    return;
}

QMap<QString, QString>* MsgLine::get_configs()
{
    auto configs = this->msgEdit->get_messages();
    configs->insert("period", QString::number(this->period));
    return this->msgEdit->get_messages();
}

void MsgLine::on_details_Button_clicked()
{
    this->msgEdit->show();
    this->msgEdit->raise();
}

void MsgLine::update()
{
    auto configs = *this->get_configs();

    this->ui->device_label->setText(configs["dev_name"]);
    this->ui->messageLabel->setText(configs["msg_name"]);

    if (this->getDevice() != configs["dev_name"])
        this->setDevice(configs["dev_name"]);

    if (this->getMessage() != configs["msg_name"]) {
        this->msgEdit->set_message(configs["msg_name"]);
    }
    this->argsMap = this->msgEdit->getArgs();
}

void MsgLine::setDevice(QString device)
{
    this->msgEdit->set_device(device);
}

void MsgLine::setMessage(QString message)
{
    this->msgEdit->set_message(message);
}

QString MsgLine::getDevice()
{
    return this->msgEdit->getDevice();
}

QString MsgLine::getMessage()
{
    return this->msgEdit->getMessage();
}

void MsgLine::setValue(double value)
{
    this->ui->value_line->setText(QString::number((uint32_t)(int32_t)value));
}

QString MsgLine::getValue()
{
    return this->ui->value_line->text();
}

void MsgLine::on_run_Button_clicked()
{
    this->run();
}

void MsgLine::on_deleteButton_clicked()
{
    emit deleted(this);
}

int MsgLine::getPeriod()
{
    return this->period;
}

void MsgLine::callback()
{
    this->run();
}

void MsgLine::on_state_clicked()
{
    this->repeat = !this->repeat;

    if (this->repeat) {
        this->timer->start(this->period);
    } else {
        this->timer->stop();
    }
}

void MsgLine::on_value_line_textEdited(const QString& arg1)
{
    this->period = arg1.toUInt();
}
