#include "cmdedit.h"
#include "ui_cmdedit.h"

CmdEdit::CmdEdit(QWidget* parent, FCPCom* fcp)
    : QWidget(parent)
    , ui(new Ui::CmdEdit)
{
    ui->setupUi(this);

    this->fcp = fcp;

    for (auto dev : fcp->getDevices()) {
        this->ui->dst_edit->addItem(dev->getName());
    }

    this->configs["type"] = "Cmd";

    this->ui->arg1_wave_edit->addItem("constant");
    this->ui->arg1_wave_edit->addItem("pulse");

    this->ui->arg2_wave_edit->addItem("constant");
    this->ui->arg2_wave_edit->addItem("pulse");

    this->ui->arg3_wave_edit->addItem("constant");
    this->ui->arg3_wave_edit->addItem("pulse");
}

CmdEdit::~CmdEdit()
{
    delete ui;
}

QMap<QString, QString>* CmdEdit::get_configs()
{
    return &this->configs;
}

void CmdEdit::on_cmd_edit_currentTextChanged(const QString& arg1)
{
    this->configs["cmd_name"] = arg1;
    emit edited();
}

void CmdEdit::on_dst_edit_currentTextChanged(const QString& arg1)
{
    Device* dev;
    try {
        dev = this->fcp->getDevice(arg1);
    } catch (...) {
        return;
    }

    this->configs["dst_name"] = arg1;

    this->ui->cmd_edit->clear();

    for (auto cmd : dev->getCommands()) {
        this->ui->cmd_edit->addItem(cmd->getName());
    }

    emit edited();
}

void CmdEdit::on_arg1_period_edit_textChanged(const QString& arg1)
{
    this->configs["arg1_period"] = arg1;
    emit edited();
}

void CmdEdit::on_arg2_period_edit_textChanged(const QString& arg1)
{
    this->configs["arg2_period"] = arg1;
    emit edited();
}

void CmdEdit::on_arg3_period_edit_textChanged(const QString& arg1)
{
    this->configs["arg3_period"] = arg1;
    emit edited();
}

void CmdEdit::on_arg1_wave_edit_currentTextChanged(const QString& arg1)
{
    this->configs["arg1_wave"] = arg1;
    emit edited();
}

void CmdEdit::on_arg2_wave_edit_currentTextChanged(const QString& arg1)
{
    this->configs["arg2_wave"] = arg1;
    emit edited();
}

void CmdEdit::on_arg3_wave_edit_currentTextChanged(const QString& arg1)
{
    this->configs["arg3_wave"] = arg1;
    emit edited();
}
