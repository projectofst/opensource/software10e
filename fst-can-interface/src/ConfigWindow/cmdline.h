#ifndef CMDLINE_H
#define CMDLINE_H

#include <QSpinBox>
#include <QTimer>
#include <QWidget>
#include <qspinbox.h>

#include "ConfigWindow/cmdedit.h"
#include "ConfigWindow/fcprunnable.h"

namespace Ui {
class CmdLine;
}

class CmdLine : public FcpRunnable {
    Q_OBJECT

public:
    explicit CmdLine(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~CmdLine();
    void setEditable(bool) override;
    void run() override;
    QMap<QString, QString>* get_configs() override;
    int getPeriod();
    void setPeriod(int period);
    QList<QVariant> getParameterValue() override;
    void loadParameterValue(QList<QVariant> list) override;

private:
    Ui::CmdLine* ui;
    FCPCom* fcp;
    CmdEdit* cmd_edit;
    QTimer* timer;
    int period = 1;
    void callback(void);
    void _run(unsigned arg1 = 0, unsigned arg2 = 0, unsigned arg3 = 0);
    unsigned dev_id = 0;

public slots:
    void update();
    void receive_can(QPair<QString, QMap<QString, double>>, unsigned, unsigned);

private slots:
    void on_details_button_clicked();
    void on_send_button_clicked();
    void on_delete_button_clicked();
    void on_reload_button_clicked(bool checked);
    void on_period_SpinBox_valueChanged(int arg1);
};

#endif // CMDLINE_H
