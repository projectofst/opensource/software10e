﻿#include "cmdline.h"
#include "ui_cmdline.h"

#include <QMessageBox>

CmdLine::CmdLine(QWidget*, FCPCom* fcp)
    : FcpRunnable()
    , ui(new Ui::CmdLine)
{
    ui->setupUi(this);

    this->fcp = fcp;

    cmd_edit = new CmdEdit(nullptr, fcp);
    connect(cmd_edit, &CmdEdit::edited, this, &CmdLine::update);

    this->show();

    this->ui->period_SpinBox->setMinimum(0);
    this->ui->period_SpinBox->setMaximum(INT32_MAX);
    this->setPeriod(period);

    this->timer = new QTimer();
    connect(timer, &QTimer::timeout, this, &CmdLine::callback);
}

QList<QVariant> CmdLine::getParameterValue()
{
    QList<QVariant> args_list;
    args_list.append(ui->arg1_edit->text());
    args_list.append(ui->arg2_edit->text());
    args_list.append(ui->arg3_edit->text());
    return args_list;
}

void CmdLine::loadParameterValue(QList<QVariant> list)
{
    ui->arg1_edit->setText(list[0].toString());
    ui->arg2_edit->setText(list[1].toString());
    ui->arg3_edit->setText(list[2].toString());
}

CmdLine::~CmdLine()
{
    delete ui;
}

void CmdLine::setEditable(bool checked)
{
    this->ui->delete_button->setVisible(checked);
    this->ui->details_button->setVisible(checked);
    this->ui->arg1_edit->setVisible(!checked);
    this->ui->arg2_edit->setVisible(!checked);
    this->ui->arg3_edit->setVisible(!checked);
    this->ui->ret1_label->setVisible(!checked);
    this->ui->ret2_label->setVisible(!checked);
    this->ui->ret3_label->setVisible(!checked);
    this->ui->send_button->setVisible(!checked);
    this->ui->reload_button->setVisible(!checked);
    this->ui->period_Label->setVisible(!checked);
    this->ui->period_SpinBox->setVisible(!checked);
}

void CmdLine::_run(unsigned arg1, unsigned arg2, unsigned arg3)
{
    auto configs = *this->get_configs();

    try {
        CANmessage msg = this->fcp->encodeSendCommand("interface",
            configs["dst_name"],
            configs["cmd_name"],
            arg1,
            arg2,
            arg3);
        emit send_can(msg);

    } catch (...) {
        QMessageBox::warning(
            this,
            tr("FST CAN Interface"),
            tr(QString("Failed to send %1 to %2").arg(configs["cmd_name"], configs["dst_name"]).toLocal8Bit().data()));
    }
}

void CmdLine::run()
{
    _run(this->ui->arg1_edit->text().toUInt(),
        this->ui->arg2_edit->text().toUInt(),
        this->ui->arg3_edit->text().toUInt());
}

QMap<QString, QString>* CmdLine::get_configs()
{
    return this->cmd_edit->get_configs();
}

unsigned calc_wave(QString wave, unsigned amplitude, unsigned, unsigned time)
{
    if (wave == "constant") {
        return amplitude;
    } else if (wave == "pulse") {
        return time * amplitude;
    }

    return 0;
}

void CmdLine::callback()
{
    static unsigned time = 0;

    auto configs = *this->get_configs();

    auto arg1 = calc_wave(
        configs.value("arg1_wave"),
        this->ui->arg1_edit->text().toUInt(),
        configs.value("arg1_period").toUInt(),
        time);

    auto arg2 = calc_wave(
        configs.value("arg2_wave"),
        this->ui->arg2_edit->text().toUInt(),
        configs.value("arg2_period").toUInt(),
        time);

    auto arg3 = calc_wave(
        configs.value("arg3_wave"),
        this->ui->arg3_edit->text().toUInt(),
        configs.value("arg3_period").toUInt(),
        time);

    this->_run(arg1, arg2, arg3);

    time = time ? 0 : 1;
}

void CmdLine::on_details_button_clicked()
{
    this->cmd_edit->show();
    this->cmd_edit->raise();
}

void CmdLine::update()
{
    auto configs = *this->get_configs();
    this->ui->cmd_name_label->setText(configs["cmd_name"]);
    this->ui->dst_label->setText(configs["dst_name"]);

    QList<JsonCommandArg*> args;

    try {
        auto dev = this->fcp->getDevice(configs["dst_name"]);
        args = dev->getCommand(configs["cmd_name"])->getArgs();
        this->dev_id = dev->getId();
    } catch (...) {
        return;
    }

    try {
        auto arg = args.value(0);
        if (arg != nullptr)
            this->ui->arg1_edit->setPlaceholderText(arg->getName());
    } catch (...) {
    }
    try {
        auto arg = args.value(1);
        if (arg != nullptr)
            this->ui->arg2_edit->setPlaceholderText(arg->getName());
    } catch (...) {
    }
    try {
        auto arg = args.value(2);
        if (arg != nullptr)
            this->ui->arg3_edit->setPlaceholderText(arg->getName());
    } catch (...) {
    }
}

void CmdLine::receive_can(QPair<QString, QMap<QString, double>> msg, unsigned dev_id, unsigned)
{
    auto msg_name = msg.first;

    try {
        if (msg_name == "return_cmd" && this->dev_id == dev_id) {
            auto sigs = msg.second;
            this->ui->ret1_label->setText(QString::number(sigs.value("ret1")));
            this->ui->ret2_label->setText(QString::number(sigs.value("ret2")));
            this->ui->ret3_label->setText(QString::number(sigs.value("ret3")));
        }
    } catch (...) {
        return;
    }
}

void CmdLine::on_send_button_clicked()
{
    this->run();
}

void CmdLine::on_delete_button_clicked()
{
    emit deleted(this);
}

void CmdLine::on_reload_button_clicked(bool checked)
{
    if (checked) {
        this->timer->start(this->getPeriod());
    } else {
        this->timer->stop();
    }
}

int CmdLine::getPeriod()
{
    return this->period;
}

void CmdLine::on_period_SpinBox_valueChanged(int arg1)
{
    this->period = arg1;
}

void CmdLine::setPeriod(int period)
{
    this->on_period_SpinBox_valueChanged(period);
    this->ui->period_SpinBox->setValue(period);
}
