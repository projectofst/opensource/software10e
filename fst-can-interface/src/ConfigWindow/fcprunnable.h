#ifndef FCPRUNNABLE_H
#define FCPRUNNABLE_H

#include <QWidget>

#include "fcpcom.hpp"

#include "can-ids/CAN_IDs.h"

Q_DECLARE_METATYPE(CANmessage);

class FcpRunnable : public QWidget {
    Q_OBJECT

public:
    FcpRunnable()
    {
        qRegisterMetaType<CANmessage>("CANMessage");
    }
    virtual void run(void) { return; };
    virtual void setEditable(bool) { return; };
    virtual QMap<QString, QString>* get_configs() { return nullptr; };
    virtual QList<QVariant> getParameterValue() { return QList<QVariant>(); };
    virtual void loadParameterValue(QList<QVariant>) {};

public slots:
    void receive_can(QPair<QString, QMap<QString, double>>, unsigned, unsigned) { return; };

signals:
    void deleted(FcpRunnable*);
    void send_can(CANmessage);
};

#endif // FCPRUNNABLE_H
