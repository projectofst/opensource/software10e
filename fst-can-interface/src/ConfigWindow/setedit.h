#ifndef SETEDIT_H
#define SETEDIT_H

#include <QMap>
#include <QWidget>

#include "fcpcom.hpp"

namespace Ui {
class SetEdit;
}

class SetEdit : public QWidget {
    Q_OBJECT

public:
    explicit SetEdit(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~SetEdit();
    QMap<QString, QString>* get_configs();
    void set_device(QString name);
    void set_config(QString name);
    QString getDevice();
    QString getConfig();

private:
    Ui::SetEdit* ui;
    FCPCom* fcp;
    QMap<QString, QString> configs;

signals:
    void edited();

private slots:
    void on_dev_edit_currentTextChanged(const QString& arg1);
    void on_cfg_edit_currentTextChanged(const QString& arg1);
};

#endif // SETEDIT_H
