#ifndef CONFIGWINDOW_H
#define CONFIGWINDOW_H

#include <QIcon>
#include <QToolButton>
#include <QVBoxLayout>
#include <QWidget>

#include "ConfigManager/configmanagerdialog.hpp"
#include "ConfigWindow/cmdline.h"
#include "ConfigWindow/msgedit.h"
#include "ConfigWindow/msgline.h"
#include "ConfigWindow/setline.h"
#include "Exceptions/helper.hpp"
#include "UiWindow.hpp"
#include "fcpcom.hpp"

#include <iostream>

namespace Ui {
class ConfigWindow;
}

class ConfigWindow : public UiWindow {
    Q_OBJECT

public:
    explicit ConfigWindow(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    void add_cmd(QString* dev = nullptr, QString* cmd = nullptr, QList<QVariant>* parameters = nullptr);
    void add_set(QString* dev = nullptr, QString* cfg = nullptr, QList<QVariant>* parameters = nullptr);
    void add_msg(QString* dev = nullptr, QString* msg = nullptr, QString period = "1", QList<QVariant>* parameters = nullptr);
    void export_state() override;
    void load_state(QMap<QString, QVariant> config) override;
    QString getTitle() override { return "FCP Config"; };
    ~ConfigWindow();

private:
    Ui::ConfigWindow* ui;
    ConfigManager* config_manager;
    ConfigManagerDialog* config_dialog;
    void setupButton(QToolButton*, QString, QIcon);
    FCPCom* fcp;
    QList<FcpRunnable*> lines;
    int ans_get;
    int ans_set;
    int ret_cmd;

private slots:
    void process_CAN_message(CANmessage) override;
    void send_can(CANmessage);
    void remove_line(FcpRunnable*);
    void on_add_button_clicked();
    void on_edit_button_clicked(bool checked);
    void on_run_all_button_clicked();
    void on_set_button_clicked();
    void on_msg_button_clicked();
    void on_config_button_clicked();

signals:
    void dist_can(const QPair<QString, QMap<QString, double>>, unsigned, unsigned);
};

#endif // CONFIGWINDOW_H
