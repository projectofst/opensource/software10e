#ifndef MSGARGS_H
#define MSGARGS_H

#include <QPair>
#include <QTimer>
#include <QWidget>
#include <iostream>

namespace Ui {
class MsgArgs;
}

class MsgArgs : public QWidget {
    Q_OBJECT

public:
    explicit MsgArgs(QWidget* parent = nullptr, double minValue = 0, double maxValue = 0, QString name = "");
    double getSignalValue();
    QString getName();
    void setValue(double signalValue);
    QPair<QString, double> getValues();
    ~MsgArgs();

private:
    Ui::MsgArgs* ui;
    QString name;
    int maxValue;
    double signalValue = 0;

private slots:
    void on_signalBox_valueChanged(double arg1);
};

#endif // MSGARGS_H
