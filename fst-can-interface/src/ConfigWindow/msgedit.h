#ifndef MSGEDIT_H
#define MSGEDIT_H

#include "fcpcom.hpp"
#include "msgargs.h"
#include <QMap>
#include <QPair>
#include <QVector>
#include <QWidget>

namespace Ui {
class MsgEdit;
}

class MsgEdit : public QWidget {
    Q_OBJECT

public:
    explicit MsgEdit(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~MsgEdit();
    void set_device(QString name);
    void set_message(QString name);
    void set_value(QString name, double value);
    QMap<QString, QString>* get_messages();
    QString getDevice();
    QString getMessage();
    QString getDeviceName();
    QString getMessageName();
    MsgArgs* getArg(QString name);
    QMap<QString, double> getArgs();

private:
    QMap<QString, QString> messages;
    QVector<MsgArgs*> argslist;
    QList<CanSignal*> canList;
    Ui::MsgEdit* ui;
    int args = 0;
    FCPCom* fcp;

signals:
    void edited();

private slots:
    void on_deviceBox_currentTextChanged(const QString& arg1);
    void on_messageBox_currentTextChanged(const QString& arg1);
};

#endif // MSGEDIT_H
