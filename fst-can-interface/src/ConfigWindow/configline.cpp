#include "configline.h"
#include "ui_configline.h"

ConfigLine::ConfigLine(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::ConfigLine)
{
    ui->setupUi(this);
}

ConfigLine::~ConfigLine()
{
    delete ui;
}
