#ifndef TIMESTAMP_HPP
#define TIMESTAMP_HPP

#include <stdint.h>

#include "time.hpp"

class Timestamp {
public:
    Timestamp(uint64_t max);
    void new_timestamp(Time cur);
    /*void increment(Seconds s);
    void increment(Seconds s, uSeconds us);*/
private:
    uint64_t max;
    Time old = Time(0, 0);
    Time acc = Time(0, 0);
};

#endif // TIMESTAMP_HPP
