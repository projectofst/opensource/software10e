#ifndef TIMELINE_HPP
#define TIMELINE_HPP

#include <QWidget>

namespace Ui {
class TimeLine;
}

class TimeLine : public QWidget {
    Q_OBJECT

public:
    explicit TimeLine(QWidget* parent = nullptr);
    ~TimeLine();

private:
    Ui::TimeLine* ui;
};

#endif // TIMELINE_HPP
