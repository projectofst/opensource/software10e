#ifndef PCB_VERSION_HPP
#define PCB_VERSION_HPP

#include <QWidget>

namespace Ui {
class pcb_version;
}

class pcb_version : public QWidget {
    Q_OBJECT

public:
    explicit pcb_version(QWidget* parent = nullptr);
    ~pcb_version();
    Ui::pcb_version* ui;
    QString get_name();
    void set_name(QString new_name);
    void set_current_version(QString version);
    void destroy();

private:
    QString name;
};

#endif // PCB_VERSION_HPP
