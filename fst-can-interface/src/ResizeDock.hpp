#ifndef RESIZEDOCK_HPP
#define RESIZEDOCK_HPP

#include "DevStatus.hpp"
#include <QKeyEvent>
#include <QObject>

class ResizeDock : public QObject {
    Q_OBJECT

protected:
    bool eventFilter(DevStatus*, QEvent*);
};

#endif // RESIZEDOCK_HPP
