/* TODO
 * replace table with unmutable list so we can remove lines correctly
 * Implement Log Play
 */

#include <QComboBox>
#include <QPushButton>
#include <QScrollArea>
#include <QTableWidget>
#include <QVBoxLayout>

#include "COM_Manager.hpp"
#include "COM_Select.hpp"

COMSelect::COMSelect(QWidget* parent, Comsguy* parentcom, FCPCom* fcp, ConfigManager* config_manger)
    : UiWindow(parent, fcp)
    , ui(new Ui::COMSelect)
{
    ui->setupUi(this);

    auto icon = QIcon(":/CustomIcons/CustomIcons/add.png");
    ui->Add->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->Add->setIcon(icon);
    ui->Add->setText("Add");

    auto icon1 = QIcon(":/Assets/CustomIcons/settings.png");
    ui->profiles->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->profiles->setIcon(icon1);
    ui->profiles->setText("Load state");

    this->config_manger = config_manger;

    this->count = 0;
    /*connect(ui->Add,&QPushButton::clicked,
            this,&COMSelect::generateTableLines);

    */
    connect(ui->Add, &QPushButton::clicked,
        this, &COMSelect::add_line);

    ComsManager = parentcom;

    this->add_line();
}

QVector<COM*>* COMSelect::get_coms()
{
    qDebug() << this->com_lines;
    QVector<COM*>* vector = new QVector<COM*>;

    for (auto com_line : this->com_lines) {
        vector->append(com_line->get_com1());
    }
    return vector;
    // return list;
}

COMSelect::~COMSelect()
{
    delete ui;
}

void COMSelect::add_line(void)
{
    COMSelectLine* new_line = new COMSelectLine(NULL, ComsManager);
    this->count++;
    qDebug() << "Creating COM: " << this->count;
    new_line->setComId(this->count);
    connect(new_line, &COMSelectLine::remove_com, this, &COMSelect::removeCOM);
    coms.append(new_line);
    ui->connection_list->addWidget(new_line);
}

void COMSelect::removeCOM(int id)
{
    for (int i = 0; i < coms.count(); i++) {
        if (coms[i]->getComId() == id) {
            qDebug() << "Removing COM: " << id;
            coms.removeAt(i);
            break;
        }
    }
}

void COMSelect::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}

void COMSelect::process_CAN_message(CANmessage) { }

void COMSelect::on_saveStateButton_clicked()
{
    int i = 0;
    /* We need to iterate connection_list */
    QMap<QString, QVariant> current_state;

    foreach (auto com, coms) {
        current_state["COM_" + QString::number(i)] = com->export_state();
        i++;
    }

    this->config_manger->registerConfig(this->objectName(), current_state);
}

void COMSelect::loadState(QMap<QString, QVariant> config)
{

    // First delete everything

    foreach (auto com, coms) {
        com->delete_com();
    }

    if (config.contains("load_default_ui")) {
        add_line();
        return;
    }

    foreach (auto com, config.values()) {
        add_line();
        QMap<QString, QVariant> current_com_map = com.toMap();
        coms.last()->auto_set(current_com_map["COM1"].toInt(),
            current_com_map["COM2"].toInt(),
            current_com_map["address1"].toString(),
            current_com_map["address2"].toString(),
            current_com_map["log"].toBool());
    }
}

void COMSelect::closeEvent(QCloseEvent* event)
{
    event->accept();
}

void COMSelect::on_profiles_clicked()
{
    loadState(this->config_manger->readConfig(this->objectName()));
}
