#include "ccu.hpp"
#include "ui_ccu.h"
#include <QDebug>

CCU::CCU(QWidget* parent, FCPCom* fcp, Helper* log, ConfigManager*)
    : UiWindow(parent, fcp)
    , ui(new Ui::CCU)
{
    ui->setupUi(this);
    this->logger = log;

    this->_rightCCU = this->ui->rightCCU;
    this->_rightCCU->setFcp(this->_fcp);
    connect(this->_rightCCU, &IndividualCCU::sendCan, this, &CCU::retransmit_can);
    this->_rightCCU->setSide("right");

    this->_leftCCU = this->ui->leftCCU;
    this->_leftCCU->setFcp(this->_fcp);
    connect(this->_leftCCU, &IndividualCCU::sendCan, this, &CCU::retransmit_can);
    this->_leftCCU->setSide("left");
}

CCU::~CCU()
{
    delete ui;
}

void CCU::process_CAN_message(CANmessage msg)
{
    updateTemperatures(msg);
    updateFlowValues(msg);
    updateCCUStatus(msg);
}

void CCU::retransmit_can(CANmessage msg)
{
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void CCU::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
    this->_rightCCU->setFcp(fcp);
    this->_leftCCU->setFcp(fcp);
}

void CCU::updateFlowValues(CANmessage message)
{
    try {
        IndividualCCU* ccu = getCCU(message);

        Message* msg = this->_fcp->getMessage(message.candata.dev_id, message.candata.msg_id);

        if (msg->getName().contains("tach")) {
            updateCCUFlowValues(message, ccu);
        } else
            return;
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        qDebug() << e.toString();
        return;
    }
}

void CCU::updateTemperatures(CANmessage message)
{
    try {
        Device* dev = this->_fcp->getDevice(message.candata.dev_id);
        Message* msg = this->_fcp->getMessage(message.candata.dev_id, message.candata.msg_id);
        IndividualCCU* ccu = NULL;
        int ntcStart = 0;
        QString side;

        if (dev->getName() == "ccu_right") {
            if (msg->getName() == "temp1_right") {
                ntcStart = 0;
            } else if (msg->getName() == "temp2_right") {
                ntcStart = 4;
            } else {
                /*not a temp message*/
                return;
            }
            ccu = this->_rightCCU;
        } else if (dev->getName() == "ccu_left") {

            if (msg->getName() == "temp1_left") {
                ntcStart = 0;
            } else if (msg->getName() == "temp2_left") {
                ntcStart = 4;
            } else {
                return;
            }
            ccu = this->_leftCCU;
        } else {
            /*Not a CCU message*/
            return;
        }

        updateCCUTemperatures(message, ccu, ntcStart);

    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        if (message.candata.dev_id == 25 || message.candata.dev_id == 24)
            qDebug() << e.toString();
    }

    return;
}

void CCU::updateCCUTemperatures(CANmessage message, IndividualCCU* ccu, int start)
{
    double value;
    try {
        for (int i = start; i < ccu->getNtcNumber(); i++) {
            QString sigName = "ntc" + QString::number(i + 1) + "_" + ccu->getSide();

            value = this->_fcp->decodeDoubleFromSignal(message, sigName);
            ccu->updateNtcTemperature(i, value);
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        if (message.candata.dev_id == 25 || message.candata.dev_id == 24)
            qDebug() << e.toString();
        return;
    }
}

void CCU::updateCCUFlowValues(CANmessage message, IndividualCCU* ccu)
{
    try {
        double value;
        QString frequencySigName = "frequency_" + ccu->getSide();
        QString flowSigName = "flow_" + ccu->getSide();
        QString periodSigName = "period_" + ccu->getSide();

        value = this->_fcp->decodeDoubleFromSignal(message, frequencySigName);
        ccu->setFrequency(value);

        value = this->_fcp->decodeDoubleFromSignal(message, flowSigName);
        ccu->setFlow(value);

        value = this->_fcp->decodeDoubleFromSignal(message, periodSigName);
        ccu->setPeriod(value);

    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        qDebug() << e.toString();
    }
}

void CCU::updateCCUStatus(CANmessage message)
{
    try {
        int value;
        IndividualCCU* ccu = getCCU(message);

        Message* msg = this->_fcp->getMessage(message.candata.dev_id, message.candata.msg_id);

        /**/
        if (msg->getName().contains("ccu_status")) {
            value = this->_fcp->decodeUint64FromSignal(message, "motor_status_" + ccu->getSide());

            ccu->updateFansAndPumpsStatus(value);

            value = this->_fcp->decodeUint64FromSignal(message, "flow_status_" + ccu->getSide());

            ccu->updateSensorFlowStatus(value);
        } else {
            return;
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        qDebug() << e.toString();
        return;
    }
}

IndividualCCU* CCU::getCCU(CANmessage message)
{
    IndividualCCU* ccu = nullptr;
    try {
        Device* dev = this->_fcp->getDevice(message.candata.dev_id);
        if (dev->getName() == "ccu_left") {
            return ccu = this->_leftCCU;
        } else if (dev->getName() == "ccu_right") {
            return ccu = this->_rightCCU;
        } else {
            return nullptr;
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        return nullptr;
    }
}
