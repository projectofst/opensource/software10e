#include "Custom_Lap.hpp"
#include "ui_Custom_Lap.h"

Custom_Lap::Custom_Lap(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::Custom_Lap)
{
    ui->setupUi(this);
    DM = OC = cones = time = 0;
}

Custom_Lap::~Custom_Lap()
{
    delete ui;
}

void Custom_Lap::on_close_button_clicked()
{
    this->hide();
}

void Custom_Lap::on_OC_check_stateChanged(int arg1)
{
    OC = arg1;
    return;
}

void Custom_Lap::on_DM_check_stateChanged(int arg1)
{
    DM = arg1;
}

void Custom_Lap::on_minutes_textChanged(const QString& arg1)
{
    time = arg1.toInt() * 60 * 1000 + ui->seconds->text().toInt() * 1000 + ui->mseconds->text().toInt();
}

void Custom_Lap::on_seconds_textChanged(const QString& arg1)
{
    time = ui->minutes->text().toInt() * 60 * 1000 + arg1.toInt() * 1000 + ui->mseconds->text().toInt();
}

void Custom_Lap::on_mseconds_textChanged(const QString& arg1)
{
    time = ui->minutes->text().toInt() * 60 * 1000 + ui->seconds->text().toInt() * 1000 + arg1.toInt();
}

void Custom_Lap::on_add_clicked()
{
    cones = ui->cones_input->text().toInt();
    emit lap_finished(time, cones, OC, DM);
    this->hide();
}
