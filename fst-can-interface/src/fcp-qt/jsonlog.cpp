#include "jsonlog.hpp"

JsonLog::JsonLog(int id, QString name, int n_args,
    QString comment, QString string)
    : JsonComponent(name, id)
{
    this->_nArgs = n_args;
    this->_comment = comment;
    this->_string = string;
}

int JsonLog::getNArgs()
{
    return this->_nArgs;
}

QString JsonLog::getComment()
{
    return this->_comment;
}

QString JsonLog::getString()
{
    return this->_string;
}
