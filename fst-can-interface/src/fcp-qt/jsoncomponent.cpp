#include "jsoncomponent.hpp"

JsonComponent::JsonComponent(QString name, uint16_t id)
{
    this->_id = id;
    this->_name = name;
}

JsonComponent::~JsonComponent() { }

uint16_t JsonComponent::getId()
{
    return this->_id;
}

QString JsonComponent::getName()
{
    return this->_name;
}
