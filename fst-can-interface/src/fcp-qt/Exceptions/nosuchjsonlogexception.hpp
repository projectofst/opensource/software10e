#ifndef NOSUCHJSONLOGEXCEPTION_HPP
#define NOSUCHJSONLOGEXCEPTION_HPP
#include "QString"
#include "jsonexception.hpp"

class noSuchJsonLogException : public JsonException {
private:
    int _id;

public:
    noSuchJsonLogException(int id);

    QString toString();
};

#endif // NOSUCHJSONLOGEXCEPTION_HPP
