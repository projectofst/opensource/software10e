#ifndef EncodingMethodNotSuportEdexception_HPP
#define EncodingMethodNotSuportEdexception_HPP

#include "QString"
#include "jsonexception.hpp"

class EncodingMethodNotSuportedException : public JsonException {
public:
    virtual QString toString();
    EncodingMethodNotSuportedException();
};

#endif // EncodingMethodNotSuportEdexception_HPP
