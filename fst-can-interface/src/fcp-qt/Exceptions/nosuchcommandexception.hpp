#ifndef NOSUCHCOMMANDEXCEPTION_HPP
#define NOSUCHCOMMANDEXCEPTION_HPP

#include "jsonexception.hpp"

class noSuchCommandException : public JsonException {
private:
    QString _commandName;

public:
    noSuchCommandException(QString commandName);

    QString toString();
};

#endif // NOSUCHCOMMANDEXCEPTION_HPP
