#ifndef JSONEXCEPTION_HPP
#define JSONEXCEPTION_HPP

#include "QString"
#include <exception>

class JsonException : public std::exception {
public:
    JsonException();

    virtual QString toString() = 0;
};

#endif // JSONEXCEPTION_HPP
