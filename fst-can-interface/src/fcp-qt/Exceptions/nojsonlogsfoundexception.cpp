#include "nojsonlogsfoundexception.hpp"

noJsonLogsFoundException::noJsonLogsFoundException(QString path)
{
    this->_fileName = path;
}

QString noJsonLogsFoundException::toString()
{
    return "noJsonLogsFoundException: No logs found in Json file " + this->_fileName + ".";
}
