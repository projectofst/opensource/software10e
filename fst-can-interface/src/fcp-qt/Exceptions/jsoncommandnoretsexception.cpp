#include "jsoncommandnoretsexception.hpp"

JsonCommandNoRetsException::JsonCommandNoRetsException(QString commandName)
{
    this->_commandName = commandName;
}

QString JsonCommandNoRetsException::toString()
{
    return "JsonCommandNoRetsException: There are no ret values for command " + this->_commandName;
}
