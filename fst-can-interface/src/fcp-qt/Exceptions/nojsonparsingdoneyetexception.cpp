#include "nojsonparsingdoneyetexception.hpp"

noJsonParsingDoneYetException::noJsonParsingDoneYetException(QString filePath)
{
    this->_fileName = filePath;
}

QString noJsonParsingDoneYetException::toString()
{
    return "noJsonParsingDoneYetException: No parsing has been done with the Json file " + this->_fileName + ".";
}
