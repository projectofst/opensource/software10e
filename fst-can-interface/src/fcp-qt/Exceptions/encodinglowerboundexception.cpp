#include "encodinglowerboundexception.hpp"

EncodingLowerBoundException::EncodingLowerBoundException(uint64_t minValue, uint64_t actualValue)
{
    this->_minValue = minValue;
    this->_actualValue = actualValue;
}

QString EncodingLowerBoundException::toString()
{
    return "EncodingLowerBoundException: signal under it's min size of: " + QString::number(this->_minValue) + "and actual value is: " + QString::number(this->_actualValue) + ".";
}
