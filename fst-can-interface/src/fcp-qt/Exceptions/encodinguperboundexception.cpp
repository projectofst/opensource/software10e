#include "encodinguperboundexception.hpp"

EncodingUpperBoundException::EncodingUpperBoundException(uint64_t maxValue, uint64_t actualValue)
{
    this->_maxValue = maxValue;
    this->_actualValue = actualValue;
}

QString EncodingUpperBoundException::toString()
{
    return "EncodingUpperBoundException: signal over it's max size of: " + QString::number(this->_maxValue) + "and inserted value is: " + QString::number(this->_actualValue) + ".";
}
