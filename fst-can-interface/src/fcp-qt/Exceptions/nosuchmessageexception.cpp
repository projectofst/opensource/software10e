#include "nosuchmessageexception.hpp"

noSuchMessageException::noSuchMessageException(int id)
{
    this->_id = id;
}
noSuchMessageException::noSuchMessageException(QString name)
{
    this->_name = name;
}

QString noSuchMessageException::toString()
{
    if (this->_name == "") {
        return "noSuchMessageException: No such Message with the name " + QString::number(this->_id) + ".";
    }
    return "noSuchMessageException: No such Message with the name " + this->_name + ".";
}
