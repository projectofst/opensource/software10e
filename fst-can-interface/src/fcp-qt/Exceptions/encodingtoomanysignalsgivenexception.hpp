#ifndef EncodingTooManySignalsGivenException_HPP
#define EncodingTooManySignalsGivenException_HPP

#include "QString"
#include "jsonexception.hpp"
#include <QMap>

class EncodingTooManySignalsGivenException : public JsonException {
private:
    QMap<QString, uint64_t> _parameters;

public:
    virtual QString toString();
    EncodingTooManySignalsGivenException(QMap<QString, uint64_t> parameters);
};
#endif // EncodingTooManySignalsGivenException_HPP
