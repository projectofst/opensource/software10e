#ifndef EncodingLowerBoundException_HPP
#define EncodingLowerBoundException_HPP

#include "QString"
#include "jsonexception.hpp"

class EncodingLowerBoundException : public JsonException {
private:
    uint64_t _minValue;
    uint64_t _actualValue;

public:
    virtual QString toString();
    EncodingLowerBoundException(uint64_t minValue, uint64_t actualvalue);
};
#endif // EncodingLowerBoundException_HPP
