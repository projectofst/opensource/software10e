#ifndef JSONCOMMANDNOARGSEXCEPTION_HPP
#define JSONCOMMANDNOARGSEXCEPTION_HPP
#include "QString"
#include "jsonexception.hpp"

class JsonCommandNoArgsException : public JsonException {
private:
    QString _commandName;

public:
    JsonCommandNoArgsException(QString commandName);
    QString toString() override;
};

#endif // JSONCOMMANDNOARGSEXCEPTION_HPP
