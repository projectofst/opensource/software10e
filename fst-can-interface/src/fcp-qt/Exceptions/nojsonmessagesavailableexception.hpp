#ifndef NOJSONMESSAGESAVAILABLEEXCEPTION_HPP
#define NOJSONMESSAGESAVAILABLEEXCEPTION_HPP
#include "QString"
#include "jsonexception.hpp"

class noJsonMessagesAvailableException : public JsonException {
private:
    QString _deviceName;

public:
    noJsonMessagesAvailableException(QString deviceName);
    QString toString() override;
};

#endif // NOJSONMESSAGESAVAILABLEEXCEPTION_HPP
