#ifndef EncodingOverBiteSizeException_HPP
#define EncodingOverBiteSizeException_HPP

#include "QString"
#include "jsonexception.hpp"

class EncodingOverBiteSizeException : public JsonException {
private:
    uint64_t _nBits;
    uint64_t _value;

public:
    virtual QString toString();
    EncodingOverBiteSizeException(uint64_t nBits, uint64_t value);
};

#endif // EncodingOverBiteSizeException_HPP
