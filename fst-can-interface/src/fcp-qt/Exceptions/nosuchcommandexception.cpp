#include "nosuchcommandexception.hpp"

noSuchCommandException::noSuchCommandException(QString commandName)
{
    this->_commandName = commandName;
}

QString noSuchCommandException::toString()
{
    return "noSuchCommandException: There is no command named: " + this->_commandName;
}
