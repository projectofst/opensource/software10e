#include "jsonfiledoesnotexistexception.hpp"

jsonFileDoesNotExistException::jsonFileDoesNotExistException(QString filePath)
{
    this->_filePath = filePath;
}

QString jsonFileDoesNotExistException::toString()
{

    return "jsonFileDoesNotExistException: Json file with path " + this->_filePath + " does not exist.";
}
