#include "badjsonfileformatexception.hpp"

badJsonFileFormatException::badJsonFileFormatException(QString jsonLine)
{
    this->_string = jsonLine;
}

QString badJsonFileFormatException::toString()
{
    return "badJsonFileFormatException: Unsupported Json File Format. Error occured at line: " + this->_string + ".";
}
