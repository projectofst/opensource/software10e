#ifndef NOJSONPARSINGDONEYETEXCEPTION_HPP
#define NOJSONPARSINGDONEYETEXCEPTION_HPP

#include "jsonexception.hpp"

class noJsonParsingDoneYetException : public JsonException {
private:
    QString _fileName;

public:
    noJsonParsingDoneYetException(QString file);
    virtual QString toString();
};

#endif // NOJSONPARSINGDONEYETEXCEPTION_HPP
