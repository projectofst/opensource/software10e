#ifndef NOFILESELECTEDEXCEPTION_HPP
#define NOFILESELECTEDEXCEPTION_HPP

#include "QString"
#include "jsonexception.hpp"

class noFileSelectedException : public JsonException {
public:
    noFileSelectedException();
    virtual QString toString();
};

#endif // NOFILESELECTEDEXCEPTION_HPP
