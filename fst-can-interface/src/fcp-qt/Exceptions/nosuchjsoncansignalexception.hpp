#ifndef NOSUCHJSONCANSIGNALEXCEPTION_HPP
#define NOSUCHJSONCANSIGNALEXCEPTION_HPP

#include "jsonexception.hpp"

class noSuchJsonCanSignalException : public JsonException {
private:
    int _deviceID = -1;
    int _messageID = -1;
    QString _sigName;

public:
    noSuchJsonCanSignalException(int devId, int msgId, QString sigName);
    noSuchJsonCanSignalException(QString sigName);
    virtual QString toString();
};

#endif // NOSUCHJSONCANSIGNALEXCEPTION_HPP
