#include "nosuchjsonlogexception.hpp"

noSuchJsonLogException::noSuchJsonLogException(int id)
{
    this->_id = id;
}

QString noSuchJsonLogException::toString()
{
    return "noSuchJsonLogException: Log with id " + QString::number(this->_id) + " not found.";
}
