#include "jsoncommandnoargsexception.hpp"

JsonCommandNoArgsException::JsonCommandNoArgsException(QString commandName)
{
    this->_commandName = commandName;
}

QString JsonCommandNoArgsException::toString()
{
    return "JsonCommandNoArgsException: The command " + this->_commandName + " does not have any arguments.";
}
