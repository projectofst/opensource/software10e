#ifndef NOSUCHCONFIGEXCEPTION_HPP
#define NOSUCHCONFIGEXCEPTION_HPP

#include "jsonexception.hpp"

class NoSuchConfigException : public JsonException {
public:
    NoSuchConfigException(QString deviceName, QString configName);
    virtual QString toString() override;

private:
    QString _configName;
    QString _deviceName;
};

#endif // NOSUCHCONFIGEXCEPTION_HPP
