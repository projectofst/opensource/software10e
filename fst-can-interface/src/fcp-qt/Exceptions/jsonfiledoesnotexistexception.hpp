#ifndef JSONFILEDOESNOTEXISTEXCEPTION_HPP
#define JSONFILEDOESNOTEXISTEXCEPTION_HPP
#include "QString"
#include "jsonexception.hpp"

class jsonFileDoesNotExistException : public JsonException {

private:
    QString _filePath;

public:
    jsonFileDoesNotExistException(QString filePath);
    virtual QString toString();
};

#endif // JSONFILEDOESNOTEXISTEXCEPTION_HPP
