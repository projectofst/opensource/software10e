#include "NoSuchConfigException.hpp"

NoSuchConfigException::NoSuchConfigException(QString deviceName, QString configName)
{
    this->_deviceName = deviceName;
    this->_configName = configName;
}

QString NoSuchConfigException::toString()
{
    return "NoSuchConfigException: There is no config with named " + this->_configName + " in device " + this->_deviceName;
}
