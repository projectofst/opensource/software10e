#include "encodingtoomanysignalsgivenexception.hpp"

EncodingTooManySignalsGivenException::EncodingTooManySignalsGivenException(QMap<QString, uint64_t> parameters)
{
    this->_parameters = parameters;
}

QString EncodingTooManySignalsGivenException::toString()
{
    QString outstring = "EncodingTooManySignalsGivenException: too many signals given in Qmap.";
    foreach (QString name, _parameters.keys()) {
        outstring.append("; ");
        outstring.append(name);
    }
    return outstring;
}
