#ifndef NOSUCHMESSAGEEXCEPTION_HPP
#define NOSUCHMESSAGEEXCEPTION_HPP

#include "QString"
#include "jsonexception.hpp"

class noSuchMessageException : public JsonException {
private:
    int _id;
    QString _name = "";

public:
    noSuchMessageException(int id);
    noSuchMessageException(QString name);

    virtual QString toString();
};

#endif // NOSUCHMESSAGEEXCEPTION_HPP
