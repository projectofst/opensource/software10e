#include "encodingoverbitesizeexception.hpp"

EncodingOverBiteSizeException::EncodingOverBiteSizeException(uint64_t nBits, uint64_t value)
{
    this->_nBits = nBits;
    this->_value = value;
}

QString EncodingOverBiteSizeException::toString()
{
    return "EncodingOverBitSizeException: signal: " + QString::number(this->_value) + " is over the max bit value of: " + QString::number(2 ^ this->_nBits) + " (" + _nBits + ").";
}
