#include "nosuchjsondeviceexception.hpp"

noSuchJsonDeviceException::noSuchJsonDeviceException(int id)
{
    this->_id = id;
}
noSuchJsonDeviceException::noSuchJsonDeviceException(QString name)
{
    this->_name = name;
}

QString noSuchJsonDeviceException::toString()
{
    if (this->_name == "") {
        return "noSuchJsonDeviceException: No device with id " + QString::number(this->_id) + ".";
    } else {
        return "noSuchJsonDeviceException: No device with name " + this->_name + ".";
    }
}
