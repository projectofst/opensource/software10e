#ifndef BADJSONFILEFORMATEXCEPTION_HPP
#define BADJSONFILEFORMATEXCEPTION_HPP

#include "QString"
#include "jsonexception.hpp"

class badJsonFileFormatException : public JsonException {
private:
    QString _string;

public:
    badJsonFileFormatException(QString jsonLine);
    virtual QString toString();
};

#endif // BADJSONFILEFORMATEXCEPTION_HPP
