#include "nosuchjsoncansignalexception.hpp"

noSuchJsonCanSignalException::noSuchJsonCanSignalException(int devId, int msgId, QString sigName)
{
    this->_deviceID = devId;
    this->_messageID = msgId;
    this->_sigName = sigName;
}

noSuchJsonCanSignalException::noSuchJsonCanSignalException(QString sigName)
{
    this->_sigName = sigName;
}

QString noSuchJsonCanSignalException::toString()
{
    if (this->_deviceID == -1 && this->_messageID == -1) {
        return "noSuchJsonCanSignalException: No signal found with name: " + this->_sigName + ".";
    }
    return "noSuchJsonCanSignalException: No signal found where device id: " + QString::number(this->_deviceID)
        + ", message id: " + QString::number(this->_messageID)
        + " and name: " + this->_sigName;
}
