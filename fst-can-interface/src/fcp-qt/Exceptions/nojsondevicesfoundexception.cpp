#include "nojsondevicesfoundexception.hpp"

noJsonDevicesFoundException::noJsonDevicesFoundException(QString filePath)
{
    this->_filePath = filePath;
}

QString noJsonDevicesFoundException::toString()
{
    return "noJsonDevicesFoundException: No devices found in Json file " + this->_filePath + ".";
}
