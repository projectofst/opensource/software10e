#ifndef NOJSONDEVICESFOUNDEXCEPTION_HPP
#define NOJSONDEVICESFOUNDEXCEPTION_HPP

#include "QString"
#include "jsonexception.hpp"

class noJsonDevicesFoundException : public JsonException {
private:
    QString _filePath;

public:
    noJsonDevicesFoundException(QString filePath);

    virtual QString toString();
};

#endif // NOJSONDEVICESFOUNDEXCEPTION_HPP
