#ifndef NOSUCHJSONDEVICEEXCEPTION_HPP
#define NOSUCHJSONDEVICEEXCEPTION_HPP

#include "QString"
#include "jsonexception.hpp"

class noSuchJsonDeviceException : public JsonException {
private:
    int _id;
    QString _name = "";

public:
    noSuchJsonDeviceException(int id);
    noSuchJsonDeviceException(QString name);
    virtual QString toString();
};

#endif // NOSUCHJSONDEVICEEXCEPTION_HPP
