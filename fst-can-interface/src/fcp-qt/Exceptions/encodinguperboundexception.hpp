#ifndef EncodingUpperBoundException_HPP
#define EncodingUpperBoundException_HPP

#include "QString"
#include "jsonexception.hpp"

class EncodingUpperBoundException : public JsonException {
private:
    uint64_t _maxValue;
    uint64_t _actualValue;

public:
    virtual QString toString();
    EncodingUpperBoundException(uint64_t maxValue, uint64_t actualvalue);
};

#endif // EncodingUpperBoundException_HPP
