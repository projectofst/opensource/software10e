#ifndef EncodingTooLittleSignalsGivenException_HPP
#define EncodingTooLittleSignalsGivenException_HPP

#include "QString"
#include "jsonexception.hpp"
#include <QMap>

class EncodingTooLittleSignalsGivenException : public JsonException {
private:
    QMap<QString, uint64_t> _parameters;

public:
    virtual QString toString();
    EncodingTooLittleSignalsGivenException(QMap<QString, uint64_t> parameters);
};

#endif // EncodingTooLittleSignalsGivenException_HPP
