#include "encodingmissingsignalexception.hpp"

EncodingMissingSignalException::EncodingMissingSignalException(QMap<QString, uint64_t> parameters)
{
    this->_parameters = parameters;
}

QString EncodingMissingSignalException::toString()
{
    QString outstring = "EncodingMissingSignalException: missing signal given in Qmap.";
    foreach (QString name, _parameters.keys()) {
        outstring.append("; ");
        outstring.append(name);
    }
    return outstring;
}
