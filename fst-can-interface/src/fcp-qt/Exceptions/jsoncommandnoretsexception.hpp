#ifndef JSONCOMMANDNORETSEXCEPTION_HPP
#define JSONCOMMANDNORETSEXCEPTION_HPP

#include "QString"
#include "jsonexception.hpp"

class JsonCommandNoRetsException : public JsonException {
private:
    QString _commandName;

public:
    JsonCommandNoRetsException(QString commandName);
    QString toString() override;
};

#endif // JSONCOMMANDNORETSEXCEPTION_HPP
