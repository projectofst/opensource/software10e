#include "nojsonmessagesavailableexception.hpp"

noJsonMessagesAvailableException::noJsonMessagesAvailableException(QString deviceName)
{
    this->_deviceName = deviceName;
}

QString noJsonMessagesAvailableException::toString()
{
    return "noJsonMessagesAvailableException: There are no messages available for the device: " + this->_deviceName + ".";
}
