#ifndef EncodingMissingSignalException_HPP
#define EncodingMissingSignalException_HPP

#include "QString"
#include "jsonexception.hpp"
#include <QMap>

class EncodingMissingSignalException : public JsonException {
private:
    QMap<QString, uint64_t> _parameters;

public:
    virtual QString toString();
    EncodingMissingSignalException(QMap<QString, uint64_t> parameters);
};

#endif // EncodingMissingSignalException_HPP
