#ifndef NOJSONLOGSFOUNDEXCEPTION_HPP
#define NOJSONLOGSFOUNDEXCEPTION_HPP

#include "QString"
#include "jsonexception.hpp"

class noJsonLogsFoundException : public JsonException {
private:
    QString _fileName;

public:
    noJsonLogsFoundException(QString _fileName);

    virtual QString toString();
};

#endif // NOJSONLOGSFOUNDEXCEPTION_HPP
