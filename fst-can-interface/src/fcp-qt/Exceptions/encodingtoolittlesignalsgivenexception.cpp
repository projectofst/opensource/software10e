#include "encodingtoolittlesignalsgivenexception.hpp"

EncodingTooLittleSignalsGivenException::EncodingTooLittleSignalsGivenException(QMap<QString, uint64_t> parameters)
{
    this->_parameters = parameters;
}

QString EncodingTooLittleSignalsGivenException::toString()
{
    QString outstring = "EncodingTooLittleSignalsGivenException: not enought signals given in Qmap.";
    foreach (QString name, _parameters.keys()) {
        outstring.append("; ");
        outstring.append(name);
    }
    return outstring;
}
