#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include "QDebug"
#include "QList"
#include "QMap"
#include "QString"
#include "can-ids/CAN_IDs.h"
#include "cansignal.hpp"
#include "fcp_message.hpp"
#include "fcp_signal.hpp"
#include "jsoncomponent.hpp"

class Message : public JsonComponent {
public:
    /**
     * @brief Message constructor.
     * @param id - Message Id.
     * @param name - Message Name.
     * @param dlc - Message DLC.
     * @param frequency - Message frequency.
     */
    Message(int id, QString name, int dlc, int frequency);

    ~Message();

    FcpMessage* getFCPMessage();

    /**
     * @brief Returns the DLC of the Message.
     * @return Returns the DLC of the Message.
     */
    int getDlc();

    /**
     * @brief Returns the Frequency of the Message.
     * @return Returns the Frequency of the Message.
     */
    int getFrequency();

    /**
     * @brief Returns a QList of pointers to CanSignal associated with a Message.
     * @return Returns a QList of pointers to CanSignal associated with a Message.
     */
    QList<CanSignal*> getSignals();

    /**
     * @brief Adds a CanSignal to the Message.
     * @param sig - CanSignal to be added.
     */
    void addSignal(CanSignal* sig);

    /**
     * @brief Changes the Frequency of the Message.
     * @param frequency - Value of the new frequency.
     */
    void setFrequency(int frequency);

    /**
     * @brief Decodes a CANmessage in a dictionary of key:value for each signal.
     * @param msg - The CANmessage
     * @return QMap containing containing the decoded value for each signal in the message.
     */
    QMap<QString, double> decodeMessage(CANmessage msg);

    void updateList();

    void encodeToYaml(YAML::Node& node, QString deviceName, QMap<QString, uint64_t> values);

    void setSignals(std::map<std::string, FcpSignal> signal_map);

private:
    QList<CanSignal*>* _signals;
    QList<std::string> _signals_name;
    int _dlc;
    int _frequency;
    bool _updated = false;
    FcpMessage* message;
};

#endif // MESSAGE_HPP
