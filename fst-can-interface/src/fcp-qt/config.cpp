#include "config.hpp"

Config::Config(int id, QString name, QString comment)
    : JsonComponent(name, id)
{
    config = new FcpConfig();
    config->id = id;
    config->name = name.toStdString();
    config->comment = comment.toStdString();
}

Config::~Config()
{
    delete config;
}

int Config::getId()
{
    return config->id;
}

QString Config::getName()
{
    return QString::fromUtf8(config->name.c_str());
}

QString Config::getComment()
{
    return QString::fromUtf8(config->comment.c_str());
}
void Config::encodeToYaml(YAML::Node& node, QString device, double value)
{
    YAML::Node infoNode;
    node["Device"] = device.toStdString();
    node["Type"] = "set";

    infoNode["name"] = this->getName().toStdString();
    infoNode["value"] = value;
    node["Information"] = infoNode;
}

FcpConfig* Config::getFcpConfig()
{
    return this->config;
}

void Config::setComment(QString new_comment)
{
    _comment = new_comment;
}
