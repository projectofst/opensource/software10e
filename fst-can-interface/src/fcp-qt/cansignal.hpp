#ifndef CANSIGNAL_HPP
#define CANSIGNAL_HPP

#include "Exceptions/encodinglowerboundexception.hpp"
#include "Exceptions/encodingmethodnotsuportedexception.hpp"
#include "Exceptions/encodingoverbitesizeexception.hpp"
#include "Exceptions/encodinguperboundexception.hpp"
#include "can-ids/CAN_IDs.h"
#include "jsoncomponent.hpp"

#include "fcp_signal.hpp"
#include <yaml-cpp/yaml.h>

class CanSignal : public JsonComponent {
public:
    CanSignal(QString name, int start, int length, double scale, double offset, QString unit, int minValue, int maxValue, fcp_type_t type, fcp_endianess_t byteOrder, QString mux, int muxCount);
    ~CanSignal();
    int getStart();
    int getLength();
    double getScale();
    double getOffset();
    QString getUnit();
    double getMinValue();
    double getMaxValue();
    fcp_type_t getType();
    fcp_endianess_t getByteOrder();
    fcp_signal_t getCSignalStruct();
    QString getMux();
    FcpSignal* getSignal();
    int getMuxCount();
    void encode(uint64_t& data, uint64_t new_data);
    void encodeToYaml(YAML::Node& node, double value);
    double decode(CANmessage msg);

private:
    int _start;
    int _length;
    double _scale;
    double _offset;
    QString _unit;
    double _minValue;
    double _maxValue;
    fcp_type_t _type;
    fcp_endianess_t _byteOrder;
    QString _mux;
    int _muxCount;
    FcpSignal* signal;

private:
    void checkBounderies(uint64_t value);
};

#endif // CANSIGNAL_HPP
