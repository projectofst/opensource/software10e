#ifndef JSONCOMMON_HPP
#define JSONCOMMON_HPP
#include "jsoncomponent.hpp"
#include "message.hpp"
class JsonCommon : public JsonComponent {

private:
    QList<Message*> _messages;
    int _id;
    QString _name;

public:
    int getId();
    QString getName();
    QList<Message*> getMessages();
    JsonCommon(int id, QString name);
    void appendMessage(Message* newMessage);
};

#endif // JSONCOMMON_HPP
