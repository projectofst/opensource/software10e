#ifndef JSONLOG_HPP
#define JSONLOG_HPP

#include "jsoncomponent.hpp"

class JsonLog : public JsonComponent {

private:
    QString _comment;
    QString _string;
    int _nArgs;

public:
    JsonLog(int id, QString name, int n_args, QString comment, QString string);
    // Getter:
    int getNArgs();
    QString getComment();
    QString getString();
};

#endif // JSONLOG_HPP
