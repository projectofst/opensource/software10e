#include "message.hpp"

Message::Message(int id, QString name, int dlc, int frequency)
    : JsonComponent(name, id)
{
    _signals = new QList<CanSignal*>();
    message = new FcpMessage;
    message->id = id;
    message->name = name.toStdString();
    message->dlc = dlc;
    message->frequency = frequency;
}
Message::~Message()
{
    delete message;
}
int Message::getDlc()
{
    return message->dlc;
}
int Message::getFrequency()
{
    return message->frequency;
}
QList<CanSignal*> Message::getSignals()
{
    updateList();
    return *this->_signals;
}
void Message::addSignal(CanSignal* sig)
{
    message->add_signal(*(sig->getSignal()));
    _updated = false;
}
void Message::setFrequency(int frequency)
{
    message->frequency = frequency;
}
QMap<QString, double> Message::decodeMessage(CANmessage msg)
{
    QMap<QString, double> aux;

    for (auto const& [key, val] : message->decode_msg(msg.candata).second) {
        aux[QString::fromStdString(key)] = val;
    }

    return aux;
}
void Message::encodeToYaml(YAML::Node& node, QString deviceName, QMap<QString, uint64_t> values)
{
    QList<CanSignal*> sigs = *this->_signals;
    YAML::Node allSigs;
    node["Device Name"] = deviceName.toStdString();
    node["type"] = "message";
    node["name"] = this->getName().toStdString();

    for (int i = 0; i < sigs.size(); i++) {
        YAML::Node sigNode;
        CanSignal* currentNode = sigs.at(i);
        currentNode->encodeToYaml(sigNode, values.value(currentNode->getName()));
        allSigs[currentNode->getName().toStdString()] = sigNode;
    }
    node["signals"] = allSigs;
}

FcpMessage* Message::getFCPMessage()
{
    return this->message;
}

void Message::updateList()
{
    FcpSignal fcpsignal;
    if (!_updated) {
        for (auto const& [key, val] : message->signals_list) {
            if (!_signals_name.contains(val.name)) {
                _signals_name.append(val.name);
                fcpsignal = val;
                _signals->append(new CanSignal(QString::fromStdString(fcpsignal.name),
                    fcpsignal.get_Start(),
                    fcpsignal.get_Lenght(),
                    fcpsignal.get_scale(),
                    fcpsignal.get_offset(),
                    QString::fromStdString(fcpsignal.unit),
                    fcpsignal.MinValue,
                    fcpsignal.MaxValue,
                    fcpsignal.get_type(),
                    fcpsignal.get_endianess(),
                    QString::fromStdString(fcpsignal.mux),
                    fcpsignal.mux_count));
            }
        }
    }
    _updated = true;
    return;
}

void Message::setSignals(std::map<std::string, FcpSignal> signal_map)
{
    CanSignal* new_signal;

    FcpSignal fcpsignal;

    for (auto const& [key, val] : signal_map) {
        if (!_signals_name.contains(val.name)) {
            fcpsignal = val;
            new_signal = new CanSignal(QString::fromStdString(fcpsignal.name),
                fcpsignal.get_Start(),
                fcpsignal.get_Lenght(),
                fcpsignal.get_scale(),
                fcpsignal.get_offset(),
                QString::fromStdString(fcpsignal.unit),
                fcpsignal.MinValue,
                fcpsignal.MaxValue,
                fcpsignal.get_type(),
                fcpsignal.get_endianess(),
                QString::fromStdString(fcpsignal.mux),
                fcpsignal.mux_count);
            addSignal(new_signal);
            updateList();
        }
    }
}
