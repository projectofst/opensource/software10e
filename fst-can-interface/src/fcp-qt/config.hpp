#ifndef CONFIG_HPP
#define CONFIG_HPP

#include "fcp_config.hpp"
#include "jsoncomponent.hpp"
#include <yaml-cpp/yaml.h>

class Config : public JsonComponent {
public:
    Config(int id, QString name, QString comment);
    ~Config();

private:
    QString _comment;
    FcpConfig* config;

public:
    int getId();
    FcpConfig* getFcpConfig();
    QString getName() override;
    QString getComment();

    void encodeToYaml(YAML::Node& node, QString device, double value);

    void setComment(QString new_comment);
};

#endif // CONFIG_HPP
