
#include "device.hpp"
#include "QDebug"
Device::Device(int id, QString name)
    : JsonComponent(name, id)
{
    this->_configs = new QMap<int, Config*>();
    this->_commands = new QMap<int, Command*>();
    this->_messages = new QMap<int, Message*>();
    device = new FcpDevice;
}

Device::~Device()
{
    qDeleteAll(*this->_configs);
    qDeleteAll(*this->_commands);
    qDeleteAll(*this->_messages);

    delete this->_configs;
    delete this->_commands;
    delete this->_messages;

    message_name.clear();
    commands_name.clear();
    configs_name.clear();

    delete device;
}

QList<Message*> Device::getMessages()
{
    QList<Message*> messages;
    updateMessages();

    messages = this->_messages->values();
    if (messages.isEmpty()) {
        throw noJsonMessagesAvailableException(this->getName());
    }
    return messages;
}

QList<Command*> Device::getCommands()
{
    QList<Command*> commands;
    updateCommands();
    commands = this->_commands->values();
    return commands;
}

QList<Config*> Device::getConfigs()
{
    QList<Config*> configs;
    updateConfigs();
    configs = this->_configs->values();
    return configs;
}

Config* Device::getConfig(int configID)
{
    QMap<int, Config*>::const_iterator it = this->_configs->begin();

    updateConfigs();

    while (it != this->_configs->end()) {
        if (it.value()->getId() == configID) {
            return it.value();
        }
        ++it;
    }
    throw NoSuchConfigException(this->getName(), "");
}

Config* Device::getConfig(QString configName)
{
    QMap<int, Config*>::const_iterator it = this->_configs->begin();

    updateConfigs();

    while (it != this->_configs->end()) {
        if (it.value()->getName() == configName) {
            return it.value();
        }
        ++it;
    }
    throw NoSuchConfigException(this->getName(), configName);
}

Command* Device::getCommand(QString commandName)
{
    QMap<int, Command*>::const_iterator it = this->_commands->begin();

    updateCommands();

    while (it != this->_commands->end()) {
        if (it.value()->getName() == commandName) {
            return it.value();
        }
        ++it;
    }
    throw noSuchCommandException(commandName);
}

Command* Device::getCommand(int commandId)
{
    QMap<int, Command*>::const_iterator it = this->_commands->begin();

    updateCommands();

    while (it != this->_commands->end()) {
        if (it.value()->getId() == commandId) {
            return it.value();
        }
        ++it;
    }
    throw noSuchCommandException(QString::number(commandId));
}

void Device::addMessage(Message* msg)
{
    if (!msg)
        return;
    this->_messages->insert(msg->getId(), msg);

    update_message = false;
    return;
}

void Device::addConfig(Config* cfg)
{
    if (!cfg)
        return;
    this->_configs->insert(cfg->getId(), cfg);

    update_config = false;
    return;
}

void Device::addCommand(Command* cmd)
{
    if (!cmd)
        return;
    this->_commands->insert(cmd->getId(), cmd);

    update_command = false;
    return;
}

Message* Device::getMessage(int id)
{
    updateMessages();

    Message* msg = this->_messages->value(id);

    if (msg == NULL) {
        throw noSuchMessageException(id);
    }
    return msg;
}

Message* Device::getMessage(QString name)
{
    QMap<int, Message*>::const_iterator it = this->_messages->begin();

    updateMessages();

    while (it != this->_messages->end()) {
        if (it.value()->getName() == name) {
            return it.value();
        }
        ++it;
    }

    throw noSuchMessageException(name);
}

void Device::updateCommands()
{
    if (!update_command)
        for (auto const& [key, val] : device->cmds)
            if (!commands_name.contains(val.name)) {
                commands_name.append(val.name);
                _commands->insert(val.id, new Command(val.id, QString::fromStdString(val.name), val.n_args, QString::fromStdString(val.comment)));
            }
    update_command = true;
    return;
}

void Device::updateConfigs()
{
    Config* new_cfg;
    if (!update_config)
        for (auto const& [key, val] : device->cfgs)
            if (!commands_name.contains(val.name)) {
                configs_name.append(val.name);
                new_cfg = new Config(val.id, QString::fromStdString(val.name), QString::fromStdString(val.comment));
                _configs->insert(val.id, new_cfg);
            }
    update_config = true;
    return;
}

void Device::updateMessages()
{
    Message* new_message;
    if (!update_message)
        for (auto const& [key, val] : device->msgs) {
            if (!message_name.contains(val->name)) {
                message_name.append(val->name);
                new_message = new Message(val->id, QString::fromStdString(val->name), val->dlc, val->frequency);
                new_message->setSignals(val->signals_list);
                _messages->insert(val->id, new_message);
            }
        }
    update_message = true;
    return;
}

void Device::setMessage(std::map<std::string, FcpMessage*> msgs)
{
    Message* message;
    for (auto const& [key, val] : msgs) {
        if (!message_name.contains(key)) {
            message = new Message(val->id, QString::fromStdString(val->name), val->dlc, val->frequency);
            message->setSignals(val->signals_list);
            addMessage(message);
            updateMessages();
        }
    }
}

void Device::setCommand(std::map<std::string, FcpCommand> cmds)
{
    Command* command;
    for (auto const& [key, val] : cmds) {
        if (!commands_name.contains(key)) {
            command = new Command(val.id, QString::fromStdString(val.name), val.n_args, QString::fromStdString(val.comment));
            addCommand(command);
            updateCommands();
        }
    }
}

void Device::setConfig(std::map<std::string, FcpConfig> cfgs)
{
    Config* config;
    for (auto const& [key, val] : cfgs) {
        if (!configs_name.contains(key)) {
            config = new Config(val.id, QString::fromStdString(val.name), QString::fromStdString(val.comment));
            addConfig(config);
            updateConfigs();
        }
    }
}

QMap<int, Message*>* Device::getMapMessages()
{
    return this->_messages;
}
