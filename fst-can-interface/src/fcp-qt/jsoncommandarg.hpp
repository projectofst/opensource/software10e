#ifndef JSONCOMMANDARG_HPP
#define JSONCOMMANDARG_HPP

#include "QString"
#include "jsoncomponent.hpp"

class JsonCommandArg : public JsonComponent {
private:
    QString _comment;

public:
    JsonCommandArg(int id, QString name, QString comment);
    QString getComment();
};

#endif // JSONCOMMANDARG_HPP
