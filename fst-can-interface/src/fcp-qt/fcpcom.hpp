#ifndef FCPCOM_HPP
#define FCPCOM_HPP

#include <QDateTime>
#include <QPair>
#include <QString>

#include "Exceptions/NoSuchConfigException.hpp"
#include "Exceptions/badjsonfileformatexception.hpp"
#include "Exceptions/encodingmissingsignalexception.hpp"
#include "Exceptions/encodingtoolittlesignalsgivenexception.hpp"
#include "Exceptions/encodingtoomanysignalsgivenexception.hpp"
#include "Exceptions/nofileselectedexception.hpp"
#include "Exceptions/nojsondevicesfoundexception.hpp"
#include "Exceptions/nojsonlogsfoundexception.hpp"
#include "Exceptions/nojsonparsingdoneyetexception.hpp"
#include "Exceptions/nosuchcommandexception.hpp"
#include "Exceptions/nosuchjsoncansignalexception.hpp"
#include "Exceptions/nosuchjsondeviceexception.hpp"
#include "Exceptions/nosuchjsonlogexception.hpp"
#include "can-ids-spec/interface-parser/signal_parser.h"
#include "can-ids/CAN_IDs.h"
#include "fcp.hpp"
#include "jsonparser.hpp"
#include <optional>
#include <qdebug.h>
#include <yaml-cpp/yaml.h>

/**
 * @brief The FCPCom class. Used to establish the interaction with FCP Json file, where the whole
 * communication protocol is specified for the FST10e Formula Student Car from FSTLisboa.
 * @author Miguel Crespo
 * @author Henrique Brum
 * @date 18/03/2020
 */

/*
 * NOTE: This class is only a QT wrapper for the fcp-cpp library which can be found at lib/fcp-cpp
 */

class FCPCom {
private:
    QList<CanSignal*> signalList; // Keeps all signals that are dinamiclay allocated, to be deleted in deconstructor;
    QList<Message*> messageList;
    QMap<int, Device*> deviceList;
    QMap<int, JsonLog*> logMap;
    JsonParser* _parser; // JsonParser -> Classe
    Fcp* fcp_obj; // Fcp -> Classe já estudada no fcp.cpp
    QString _filePath; // Vai guardar o nome e local do ficherio
    bool _hasParsed = false; // Guarda se já ocorreu algum parse ao ficheiro ou não, default = NÃO, pois o ficheiro começa sem qualquer parse

private slots:
    void handleMissingElementException();

    uint16_t getDeviceID(QString messageName);

    void verifySignalPresence(QMap<QString, uint64_t>& parameters, Message& msg);

public:
    /**
     * @brief Constructor
     */
    FCPCom();

    /**
     * @brief Contructor.
     * @param jsonFileName - path to json file.
     */
    FCPCom(QString jsonFileName);

    QString getCurrentJsonFile();

    ~FCPCom();

    /**
     * @brief Returns a <a href=https://doc.qt.io/qt-5/qlist.html target="_blank"> QList</a> of pointers to JsonLog objects.
     * @return Returns a <a href=https://doc.qt.io/qt-5/qlist.html target="_blank"> QList</a> list of pointers all the JsonLog.
     */
    QList<JsonLog*> getLogs();

    /**
     * @brief From a given <a href="https://doc.qt.io/qt-5/qstring.html" target="_blank"> QString </a>"name" returns the pointer JsonLog object with the corresponding name.
     * @param name - Name of the JsonLog to return.
     * @return Reference to requested JsonLog.
     */
    JsonLog* getLog(QString name);

    /**
     * @brief From a given integer "id" returns the pointer to the JsonLog object with the corresponding id.
     * @param id - Identifier of the requested JsonLog.
     * @throws noJsonLogsFoundException is thrown if no JsonLog with requested "id" was not found.
     * @return Reference to requested JsonLog.
     */
    JsonLog* getLog(int id);

    /**
     * @brief From given Device name and Command name returns the reference to the JsonLog object with the corresponding name.
     * @param deviceName - Name of the device to which the requested command belongs.
     * @param commandName - Name of the requested command.
     * @throws noJsonLogsFoundException is thrown if no Command with the given name was found.
     * @throws noSuchJsonDeviceException is thrown if no Device with name ""
     * @return Reference to the requested Command.
     */
    Command* getCommand(QString deviceName, QString commandName);

    /**
     * @brief Returns a <a href=https://doc.qt.io/qt-5/qlist.html target="_blank"> QList</a> of pointers to Device objects. These devices are descriminated
     * on the FCP Json File.
     * @throws noJsonDevicesFoundException is thrown if there are references to Device.
     * @return Returns a list of all Devices.
     */
    QList<Device*> getDevices();

    /**
     * @brief From a given  String name returns the reference to the Device object with the corresponding name.
     * @param name - Name of the requested Device.
     * @throws noSuchJsonDeviceException is thrown if there is no Device with the given name.
     * @return Returns a reference to the requested Device.
     */
    Device* getDevice(QString name);
    std::optional<Device*> getDeviceSafe(QString deviceName);

    /**
     * @brief From a given integer id returns the reference to the Device object with the corresponding identifier.
     * @param id - Identifier of the requested Device.
     * @throws noSuchJsonDeviceException is thrown if there is no Device with the given id.
     * @return Returns a reference to the requested Device.
     */
    Device* getDevice(int id);

    /**
     * @brief From a given integer deviceIdentifier returns the QList of pointers to Message associated with the
     * the Device with the given deviceIdentifier.
     * @param deviceIdentifier
     * @throws noSuchJsonDeviceException is thrown if there is no Device with the given id.
     * @return Returns a list of pointers to Message associated with the requested Device.
     */
    QList<Message*> getMessages(int deviceIdentifier);

    /**
     * @brief Returns a reference to a Message with the specified messageIdentifier, assuming the latter belongs
     * to the Device with the given identifier deviceIdentifier.
     * @param deviceIdentifier - Identifier of the Device to which the requested Message belongs.
     * @param messageIdentifier - Identifier of the requested Message.
     * @throws noSuchJsonDeviceException is thrown if the given deviceIdentifier doesn't corresponde to any known Device.
     * @throws noSuchMessageException is thrown if the given messageIdentifier doesnt't corresponde to any Message belonging
     * to the Device designated by the given deviceIdentifier.
     * @return Returns a reference to the requested Message.
     */
    Message* getMessage(int deviceIdentifier, int messageIdentifier);

    /**
     * @brief Returns a reference to a Message with the specified messageIdentifier, assuming the latter belongs
     * to the Device with the given device.
     * @param device - Reference to the Device to which the requested Message belongs.
     * @param messageName - name of the requested Message.
     * @throws noSuchMessageException is thrown if the given messageIdentifier doesnt't corresponde to any Message belonging
     * to the Device designated by the given device.
     * @return Returns a reference to the requested message
     */
    Message* getMessage(Device* device, QString messageName);

    /**
     * @brief Returns a reference to a Message with the specified messageName. Looks through all of the available Devices.
     * @param messageName - Name of the requested message.
     * @throws noSuchMessageException if there is no Message with given name.
     * @return Returns a reference to the requested message.
     */
    Message* getMessage(QString messageName);

    /**
     * @brief Returns a reference to a CanSignal from the given parameters.
     * @param deviceIdentifier - Device identifier to which the requested signal belongs.
     * @param messageIdentifier - Message identifier that belongs to the Device with the given deviceIdentifier
     * and to which the requested CanSignal belongs.
     * @param sigName - Name of the requested CanSignal.
     * @throws noSuchJsonDeviceException if there is no Device with the given deviceIdentifier.
     * @throws noSuchMessageException if there is no Message with the given messageIdentifier.
     * @throws noSuchJsonCanSignalException if there is no CanSignal that matches with the given sigName
     * @return Returns a reference to the requested signal.
     */
    CanSignal* getSignal(int deviceIdentifier, int messageIdentifier, QString sigName);

    /**
     * @brief Returns a reference to a CanSignal that has the requested name.
     * @param name - Name of the requested CanSignal.
     * @throws noSuchJsonCanSignalException if there is no CanSignal with the given name.
     * @return Returns a reference to the requested CanSignal.
     */
    CanSignal* getSignal(QString name);

    /**
     * @brief Returns a reference to the CanSignal with the name: sigName and that belongs to the Message referenced by
     * msg,
     * @param msg - Reference to the Message to which the requested CanSignal belongs.
     * @param sigName - Name of the requested CanSignal.
     * @throws noSuchJsonCanSignalException if there is no CanSignal with the given sigName.
     * @return Returns a reference to the requested CanSignal.
     */
    CanSignal* getSignal(Message* msg, QString sigName);

    /**
     * @brief Returns a list of all signals.
     * @return Returns a list of Signal pointers
     */
    QList<CanSignal*> getSignals();

    QMap<QString, std::tuple<CanSignal*, Device*, Message*>> getSignalsAndDevicesAndMessages();

    /**
     * @brief Returns a reference to the common Device.
     * @return Reference to the Common Device.
     */
    Device* getCommon();

    /**
     * @brief sets the path of the new json file.
     * @param file - Path to the new json File.
     */
    void setFile(QString file);

    /**
     * @brief Parses a json file and creates all of the Device, Message and CanSignal references nessecary to
     * utilize the FCP communication protocol.
     */
    void parse();

    /**
     * @brief From a given CANmessage and a QString returns a 64 bit unsigned integer that resulted from the decoding
     * specified by the CanSignal with the given sigName.
     * @param msg - CANmessage with the information to decode.
     * @param sigName - Name of the CanSignal.
     * @throws noSuchJsonCanSignalException if there is no CanSignal with the given sigName.
     * @return Returns the value that resulted from the decoding.
     */
    uint64_t decodeUint64FromSignal(CANmessage msg, QString sigName);

    /**
     * @brief From a given CANmessage and a QString returns a double that resulted from the decoding
     * specified by the CanSignal with the given sigName.
     * @param msg - CANmessage with the information to decode.
     * @param sigName - Name of the CanSignal.
     * @throws noSuchJsonCanSignalException if there is no CanSignal with the given sigName.
     * @return Returns the value that resulted from the decoding.
     */
    double decodeDoubleFromSignal(CANmessage msg, QString sigName);

    /**
     * @brief Recieves a device name and a message name in order to identify the signals to be encoded. Recieves a Map that maps
     * QString to 64 bit unsigned long values. Maps the name of the CanSignal to its respective Value.
     * @param deviceName - Name of the Device to which the Message belongs to.
     * @param messageName - Name of the Message to be sent.
     * @param parameters - Map of the CanSignal Names to their values.
     * @throws noSuchJsonDeviceException if there is no Device with the given deviceIdentifier.
     * @throws noSuchMessageException if there is no Message with the given messageIdentifier.
     * @throws EncodingUpperBoundException if the value of any parameter is greater that the allowed by the specified CanSignal's max value.
     * @throws EncodingLowerBoundException if the value of any parameter is lower that the allowed by the specified CanSignal's min value.
     * @throws EncodingMissingSignalException if there is at least one signal missing in parameters from the total associated with the requested Message to encode.
     * @throws EncodingTooManySignalsGivenException if there is at least one signal more in parameters than the total associated with the requested Message to encode.
     * @throws EncodingTooLittleSignalGivenException if there are less signals provided that what is specified in the requested Message to encode.
     * @return Returns a CANMessage with the characteristics specified by the input arguments.
     */
    CANmessage encodeMessage(QString deviceName, QString messageName, QMap<QString, double> parameters);

    /**
     * @brief Encondes a command Message.
     * @param originName - Name of the device that is sending the Message.
     * @param destinationName - Name of the destination Device.
     * @param commandName - Name of the command to be sent
     * @param arg1 - 1st argument of the Message.
     * @param arg2 - 2nd argument of the Message.
     * @param arg3 - 3rd argument of the Message.
     * @return Returns a Send Command CANmessage.
     */
    CANmessage encodeSendCommand(QString originName,
        QString destinationName,
        QString commandName,
        uint64_t arg1, uint64_t arg2, uint64_t arg3);

    /**
     * @brief getComponentsByType
     * @param componentType
     * @return
     */
    QList<JsonComponent*>* getComponentsByType(QString deviceName, QString componentName, QString componentType);

    CANmessage encodeGetConfig(QString originName, QString destinationName, QString configName);
    Config* getConfig(QString deviceName, QString configName);
    CANmessage encodeSetConfig(QString originName, QString destinationName, QString configName, uint64_t data);

    /**
     * @brief Decodes a CANmessage in a dictionary of key:value for each signal.
     * @param msg - The CANmessage
     * @return QMap containing containing the decoded value for each signal in the message.
     */
    QPair<QString, QMap<QString, double>> decodeMessage(CANmessage msg);

    /**
     * @brief Decodes a CANmessage in a dictionary of keymux:value for each signal.
     * @param msg - The CANmessage
     * @return QMap containing containing the decoded value for each signal in the message.
     */
    QPair<QString, QMap<QString, double>> decodeMessageMuxed(CANmessage msg);

    /**
     * @brief getConfig
     * @param deviceName
     * @param configNumber
     * @return
     */
    Config* getConfig(QString deviceName, int configNumber);

    /**
     * @brief isCorrectReturnCommand - Checks if given message is a return_cmd of device with deviceName and
     * command with commandName.
     * @param message
     * @param deviceName
     * @param commandName
     * @return
     */
    bool isCorrectReturnCommand(CANmessage message, QString deviceName, QString commandName);

    void encodeCommandToYaml(YAML::Node& node, QString deviceName, QString commandName, double arg1, double arg2, double arg3);
    void encodeReqSetToYaml(YAML::Node& node, QString deviceName, QString configName, double value);
    void encodeMessageToYaml(YAML::Node& node, QString deviceName, QString messageName, QMap<QString, uint64_t> value);
    Command* getCommand(QString deviceName, int commandId);
    int getLogId(QString logName);
    CANmessage encodeSetMessage(QString originName, QString destinationName, QString messageName, uint64_t data);
};

#endif // FCPCOM_HPP
