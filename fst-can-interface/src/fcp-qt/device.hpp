#ifndef DEVICE_HPP
#define DEVICE_HPP

#include "Exceptions/NoSuchConfigException.hpp"
#include "Exceptions/nojsonmessagesavailableexception.hpp"
#include "Exceptions/nosuchcommandexception.hpp"
#include "Exceptions/nosuchmessageexception.hpp"
#include "command.hpp"
#include "config.hpp"
#include "fcp_device.hpp"
#include "jsoncomponent.hpp"
#include "message.hpp"
#include <QList>
#include <QMap>

class Device : public JsonComponent {
public:
    /**
     * @brief Device constructor.
     * @param id - Identifier of the Device.
     * @param name - Name of the Device.
     */
    Device(int id, QString name);

    ~Device();
    /**
     * @brief Returns a QList of references to the messages ( Message ) associated with this Device.
     * @return Reference to list of pointers to Message.
     */
    QList<Message*> getMessages();

    /**
     * @brief Returns a QList of references to the commands ( Command ) associated with this Device.
     * @return Reference to list of pointers to Command.
     */
    QList<Command*> getCommands();

    /**
     * @brief Returns a QList of references to the configs ( Config ) associated with this Device.
     * @return Reference to list of pointers to Config.
     */
    QList<Config*> getConfigs();

    /**
     * @brief Adds a Message to Device.
     * @param msg - Message to append.
     */
    void addMessage(Message* msg);

    /**
     * @brief Adds a Config to Device.
     * @param cfg - Config to append.
     */
    void addConfig(Config* cfg);

    /**
     * @brief Adds a Command to Device.
     * @param cmd - Command to append.
     */
    void addCommand(Command* cmd);

    /**
     * @brief Returns the Message with the given id.
     * @param id - Identifier of the requested Message.
     * @throws noSuchMessageException if there is no Message with identifier id.
     * @return Returns Message with the given id.
     */
    Message* getMessage(int id);

    /**
     * @brief Returns the Message with the given name.
     * @param name - Name of the requested Message.
     * @throws noSuchMessageException if there is no Message with name name.
     * @return Returns the Message with the given name.
     */
    Message* getMessage(QString name);

    /**
     * @brief getConfig
     * @param configName
     * @return
     */
    Config* getConfig(QString configName);

    /**
     * @brief Device::getCommand
     * @param commandName
     * @return
     */
    Command* getCommand(QString commandName);

    /**
     * @brief getConfig - Returns a pointer to the Config object that has a id ConfigID
     * @param configID - ID of wanted config
     * @return
     */
    Config* getConfig(int configID);

    /**
     * @brief Device::getCommand
     * @param commandId
     * @return
     */
    Command* getCommand(int commandId);

    void updateMessages();

    void updateCommands();

    void updateConfigs();

    void setMessage(std::map<std::string, FcpMessage*> msgs);
    void setCommand(std::map<std::string, FcpCommand> cmds);
    void setConfig(std::map<std::string, FcpConfig> cfgs);

    QMap<int, Message*>* getMapMessages();

private:
    QMap<int, Message*>* _messages;
    QMap<int, Command*>* _commands;
    QMap<int, Config*>* _configs;

    QList<std::string> signals_name;
    QList<std::string> message_name;
    QList<std::string> commands_name;
    QList<std::string> configs_name;

    bool update_signals = false;
    bool update_message = false;
    bool update_command = false;
    bool update_config = false;

    FcpDevice* device;
};

#endif // DEVICE_HPP
