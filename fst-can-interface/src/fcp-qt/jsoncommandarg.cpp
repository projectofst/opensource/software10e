#include "jsoncommandarg.hpp"

JsonCommandArg::JsonCommandArg(int id, QString name, QString comment)
    : JsonComponent(name, id)
{
    this->_comment = comment;
}

QString JsonCommandArg::getComment()
{
    return this->_comment;
}
