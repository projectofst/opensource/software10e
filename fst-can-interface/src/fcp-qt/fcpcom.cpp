
#include "fcpcom.hpp"

/**
 * @brief Constructor
 */
FCPCom::FCPCom()
{
    this->_parser = new JsonParser();
    this->fcp_obj = new Fcp;
    this->_filePath = "";
}

FCPCom::FCPCom(QString jsonFileName)
{
    this->_parser = new JsonParser();
    this->fcp_obj = new Fcp;
    this->_filePath = jsonFileName;
}

FCPCom::~FCPCom()
{
    signalList.clear();
    messageList.clear();
    deviceList.clear();
    logMap.clear();
    delete this->_parser;
    delete this->fcp_obj;
}

QList<Device*> FCPCom::getDevices()
{
    Device* aux_device;

    for (auto const& [key, val] : fcp_obj->devices) {
        if (!this->deviceList.contains(val.id)) {
            aux_device = new Device(val.id, QString::fromStdString(key));
            aux_device->setMessage(val.msgs);
            aux_device->setCommand(val.cmds);
            aux_device->setConfig(val.cfgs);
            this->deviceList.insert(aux_device->getId(), aux_device);
        }
    }

    if (this->deviceList.isEmpty()) {
        handleMissingElementException();
        throw noJsonDevicesFoundException(this->_filePath);
    } else {
        return this->deviceList.values();
    }
}

Device* FCPCom::getDevice(int deviceIdentifier)
{

    Device* device;
    if (deviceIdentifier == 0)
        return this->getCommon();

    if (this->deviceList.contains(deviceIdentifier))
        return this->deviceList[deviceIdentifier];

    for (auto const& [key, val] : fcp_obj->devices) {
        if (val.id == deviceIdentifier) {
            device = new Device(val.id, QString::fromStdString(key));
            device->setMessage(val.msgs);
            device->setCommand(val.cmds);
            device->setConfig(val.cfgs);
            this->deviceList.insert(val.id, device);
            return device;
        }
    }

    handleMissingElementException();
    throw noSuchJsonDeviceException(deviceIdentifier);
}

Device* FCPCom::getDevice(QString deviceName)
{
    Device* device;
    int deviceId = getDeviceID(deviceName);

    if (this->deviceList.contains(deviceId))
        return this->deviceList[deviceId];

    if (fcp_obj->devices.count(deviceName.toStdString())) {
        FcpDevice fcp_device = fcp_obj->devices[deviceName.toStdString()];
        device = new Device(fcp_device.id, deviceName);
        device->setMessage(fcp_device.msgs);
        device->setCommand(fcp_device.cmds);
        device->setConfig(fcp_device.cfgs);
        this->deviceList.insert(deviceId, device);
        return device;
    }

    handleMissingElementException();
    throw noSuchJsonDeviceException(deviceName);
}

std::optional<Device*> FCPCom::getDeviceSafe(QString deviceName)
{

    Device* device;
    int deviceId = getDeviceID(deviceName);
    if (this->deviceList.contains(deviceId))
        return this->deviceList[deviceId];

    for (auto const& [key, val] : fcp_obj->devices) {
        if (val.name == deviceName.toStdString()) {
            device = new Device(val.id, QString::fromStdString(key));
            this->deviceList.insert(val.id, device);
            return device;
        }
    }
    return {};
}

QList<JsonLog*> FCPCom::getLogs()
{
    std::map<std::string, FcpLog> logs = fcp_obj->logs;
    for (auto const& [key, val] : logs) {
        this->logMap[val.id] = new JsonLog(val.id, QString::fromStdString(val.name), val.n_args, QString::fromStdString(val.comment), QString::fromStdString(val.string));
    }

    if (this->logMap.isEmpty()) {
        handleMissingElementException();
        throw noJsonLogsFoundException(this->_filePath);
    } else {
        return this->logMap.values();
    }
}

JsonLog* FCPCom::getLog(QString logName)
{
    std::map<std::string, FcpLog> logs = fcp_obj->logs;
    FcpLog fcp_log;
    int logId = 0;

    if (fcp_obj->logs.count(logName.toStdString())) {
        logId = fcp_obj->logs[logName.toStdString()].id;

        if (this->logMap.contains(logId))
            return this->logMap[logId];

        fcp_log = logs[logName.toStdString()];
        logMap[fcp_log.id] = new JsonLog(fcp_log.id, QString::fromStdString(fcp_log.name), fcp_log.n_args, QString::fromStdString(fcp_log.comment), QString::fromStdString(fcp_log.string));
        return logMap[fcp_log.id];
    }

    handleMissingElementException();
    throw noSuchJsonLogException(-1);
}

JsonLog* FCPCom::getLog(int id)
{

    if (this->logMap.contains(id))
        return this->logMap[id];

    for (auto const& [key, val] : this->fcp_obj->logs) {
        if (val.id == id) {
            this->logMap[id] = new JsonLog(val.id, QString::fromStdString(val.name), val.n_args, QString::fromStdString(val.comment), QString::fromStdString(val.string));
            return logMap[id];
        }
    }

    handleMissingElementException();
    throw noSuchJsonLogException(id);
}

Device* FCPCom::getCommon()
{
    if (this->deviceList.contains(fcp_obj->common.id))
        return this->deviceList[fcp_obj->common.id];

    Device* common_device = new Device(fcp_obj->common.id, QString::fromStdString(fcp_obj->common.name));
    common_device->setCommand(fcp_obj->common.cmds);
    common_device->setMessage(fcp_obj->common.msgs);
    common_device->setConfig(fcp_obj->common.cfgs);
    this->deviceList.insert(fcp_obj->common.id, common_device);
    return common_device;
}

void FCPCom::setFile(QString filePath)
{
    this->_filePath = filePath;
    this->_hasParsed = false;
    return;
}

QList<Message*> FCPCom::getMessages(int deviceIdentifier)
{
    FcpDevice* fcpdev;
    Device* device = getDevice(deviceIdentifier);
    QMap<int, Message*>* _messages = device->getMapMessages();

    if (deviceIdentifier == 0) {
        fcpdev = &fcp_obj->common;
    } else {
        fcpdev = fcp_obj->get_device(deviceIdentifier);
    }

    for (auto const& [key, val] : fcpdev->msgs) {
        if (!_messages->contains(val->id)) {
            _messages->insert(val->id, new Message(val->id, QString::fromStdString(val->name), val->dlc, val->frequency));
        }
    }

    return device->getMessages();
}

Message* FCPCom::getMessage(int deviceIdentifier, int messageIdentifier)
{
    Device* device = getDevice(deviceIdentifier);
    QList<Message*> messages = device->getMessages();
    foreach (Message* msg, messages) {
        if (msg->getId() == messageIdentifier) {
            return msg;
        }
    }

    // If message is not in the device we will try to get the messages from common device,
    // since all the messages that are common to all devices are defined in a separeted device
    device = getDevice(0);

    foreach (Message* msg, device->getMessages())
        if (msg->getId() == messageIdentifier)
            return msg;

    handleMissingElementException();
    throw noSuchMessageException(messageIdentifier);
}

Message* FCPCom::getMessage(Device* device, QString messageName)
{
    QList<Message*> list = device->getMessages();
    foreach (Message* msg, list) {
        if (msg->getName() == messageName) {
            msg = getMessage(device->getId(), msg->getId());
            return msg;
        }
    }
    handleMissingElementException();
    throw noSuchMessageException(messageName);
}

Message* FCPCom::getMessage(QString messageName)
{
    QList<Device*> devices = getDevices();
    foreach (Device* dev, devices) {
        QList<Message*> messages = dev->getMessages();
        foreach (Message* msg, messages) {
            if (msg->getName() == messageName) {
                return msg;
            }
        }
    }
    handleMissingElementException();
    throw noSuchMessageException(messageName);
}

CanSignal* FCPCom::getSignal(int deviceIdentifier,
    int messageIdentifier, QString sigName)
{
    Message* msg = getMessage(deviceIdentifier, messageIdentifier);

    QList<CanSignal*> sigs = msg->getSignals();
    foreach (CanSignal* sig, sigs) {
        if (sig->getName() == sigName) {
            return sig;
        }
    }
    handleMissingElementException();
    throw noSuchJsonCanSignalException(deviceIdentifier, messageIdentifier, sigName);
}

CanSignal* FCPCom::getSignal(Message* msg, QString sigName)
{
    QList<CanSignal*> sigs = msg->getSignals();
    foreach (CanSignal* sig, sigs) {
        if (sig->getName() == sigName) {
            return sig;
        }
    }

    handleMissingElementException();
    throw noSuchJsonCanSignalException(sigName);
}

CanSignal* FCPCom::getSignal(QString sigName)
{

    FcpSignal currentSignal;
    CanSignal* signalToReturn = NULL;

    for (auto const& [key, val] : fcp_obj->devices) {
        for (auto const& [key1, val1] : val.msgs) {
            for (auto const& [key2, val2] : val1->signals_list) {
                currentSignal = val2;
                if (QString::fromStdString(currentSignal.name) == sigName) {
                    signalToReturn = new CanSignal(QString::fromStdString(currentSignal.name),
                        currentSignal.get_Start(),
                        currentSignal.get_Lenght(),
                        currentSignal.get_scale(),
                        currentSignal.get_offset(),
                        QString::fromStdString(currentSignal.unit),
                        currentSignal.MinValue,
                        currentSignal.MaxValue,
                        currentSignal.get_type(),
                        currentSignal.get_endianess(),
                        QString::fromStdString(currentSignal.mux),
                        currentSignal.mux_count);
                    signalList.append(signalToReturn);
                    return signalToReturn;
                }
            }
        }
    }
    handleMissingElementException();
    throw noSuchJsonCanSignalException(sigName);
}

QList<CanSignal*> FCPCom::getSignals()
{
    signalList.clear();
    FcpSignal currentSignal;
    CanSignal* signalToReturn = NULL;

    for (auto const& [key, val] : fcp_obj->devices) {
        for (auto const& [key1, val1] : val.msgs) {
            for (auto const& [key2, val2] : val1->signals_list) {
                currentSignal = val2;
                signalToReturn = new CanSignal(QString::fromStdString(currentSignal.name),
                    currentSignal.get_Start(),
                    currentSignal.get_Lenght(),
                    currentSignal.get_scale(),
                    currentSignal.get_offset(),
                    QString::fromStdString(currentSignal.unit),
                    currentSignal.MinValue,
                    currentSignal.MaxValue,
                    currentSignal.get_type(),
                    currentSignal.get_endianess(),
                    QString::fromStdString(currentSignal.mux),
                    currentSignal.mux_count);
                signalList.append(signalToReturn);
            }
        }
    }
    return signalList;
}

QMap<QString, std::tuple<CanSignal*, Device*, Message*>> FCPCom::getSignalsAndDevicesAndMessages()
{
    QMap<QString, std::tuple<CanSignal*, Device*, Message*>> signalsDevicesMessages;
    foreach (auto device, this->getDevices()) {
        foreach (auto msg, device->getMessages()) {
            foreach (auto signal, msg->getSignals()) {
                signalsDevicesMessages.insert(signal->getName(), std::make_tuple(signal, device, msg));
            }
        }
    }
    return signalsDevicesMessages;
}

void FCPCom::parse()
{
    if (this->_filePath == "") {
        throw noFileSelectedException();
    }

    try {
        fcp_obj->decompile_file(this->_filePath.toStdString());
    } catch (std::exception& e) {
        if (!std::strcmp(e.what(), "error_reading")) {
            throw jsonFileDoesNotExistException(this->_filePath);
        }

        throw badJsonFileFormatException(QString::fromUtf8(e.what()));
    }
    this->_hasParsed = true;

    return;
}

void FCPCom::handleMissingElementException()
{
    if (this->_filePath == "") {
        throw noFileSelectedException();
    } else if (!this->_hasParsed) {
        throw noJsonParsingDoneYetException(this->_filePath);
    }
    return;
}

uint64_t FCPCom::decodeUint64FromSignal(CANmessage msg, QString sigName)
{
    CanSignal* sig = getSignal(msg.candata.dev_id, msg.candata.msg_id, sigName);
    return fcp_decode_signal_uint64_t(msg.candata, (fcp_signal_t) { .start = (uint16_t)sig->getStart(), .length = (uint16_t)sig->getLength(), .scale = sig->getScale(), .offset = sig->getOffset(), .type = sig->getType(), .endianess = sig->getByteOrder() });
}

double FCPCom::decodeDoubleFromSignal(CANmessage msg, QString sigName)
{
    CanSignal* sig = getSignal(msg.candata.dev_id, msg.candata.msg_id, sigName);
    return fcp_decode_signal_double(msg.candata, (fcp_signal_t) { .start = (uint16_t)sig->getStart(), .length = (uint16_t)sig->getLength(), .scale = sig->getScale(), .offset = sig->getOffset(), .type = sig->getType(), .endianess = sig->getByteOrder() });
}

uint16_t FCPCom::getDeviceID(QString messageName)
{
    QList<Device*> list = getDevices();
    foreach (Device* dev, list) {
        if (dev->getName() == messageName)
            return dev->getId();
    }

    handleMissingElementException();
    throw noSuchJsonDeviceException(messageName);
}

CANmessage FCPCom::encodeMessage(QString deviceName, QString messageName, QMap<QString, double> parameters)
{
    std::map<std::string, double> args;

    foreach (QString key, parameters.keys())
        args[key.toStdString()] = parameters[key];

    CANdata canData = this->fcp_obj->encode_msg(deviceName.toStdString(), messageName.toStdString(), args);

    CANmessage CANMessage;

    CANMessage.candata = canData;
    CANMessage.timestamp = QDateTime::currentMSecsSinceEpoch();

    return CANMessage;
}

Command* FCPCom::getCommand(QString deviceName, QString commandName)
{
    Device* device = this->getDevice(deviceName);

    return device->getCommand(commandName);
}

Command* FCPCom::getCommand(QString deviceName, int commandId)
{
    Device* device = this->getDevice(deviceName);

    return device->getCommand(commandId);
}

CANmessage FCPCom::encodeSendCommand(QString originName,
    QString destinationName,
    QString commandName,
    uint64_t arg1,
    uint64_t arg2,
    uint64_t arg3)
{

    Device* common = this->getCommon();

    Device* destination = this->getDevice(destinationName);
    Command* command = this->getCommand(destinationName, commandName);

    QMap<QString, double> arguments;

    arguments.insert("id", command->getId());
    arguments.insert("dst", destination->getId());
    arguments.insert("arg1", arg1);
    arguments.insert("arg2", arg2);
    arguments.insert("arg3", arg3);

    CANmessage message = encodeMessage(common->getName(), "send_cmd", arguments);

    message.candata.dev_id = this->getDevice(originName)->getId();
    return message;
}

Config* FCPCom::getConfig(QString deviceName, QString configName)
{
    return this->getDevice(deviceName)->getConfig(configName);
}

Config* FCPCom::getConfig(QString deviceName, int configNumber)
{
    return this->getDevice(deviceName)->getConfig(configNumber);
}

CANmessage FCPCom::encodeGetConfig(QString originName,
    QString destinationName,
    QString configName)
{

    Device* common = this->getCommon();

    Device* destination = this->getDevice(destinationName);
    Config* Config = this->getConfig(destinationName, configName);
    QMap<QString, double> arguments;

    arguments.insert("id", Config->getId());
    arguments.insert("dst", destination->getId());

    CANmessage message = encodeMessage(common->getName(), "req_get", arguments);

    message.candata.dev_id = this->getDevice(originName)->getId();
    return message;
}

CANmessage FCPCom::encodeSetConfig(QString originName, QString destinationName, QString configName, uint64_t data)
{
    Device* common = this->getCommon();

    Device* destination = this->getDevice(destinationName);
    Config* Config = this->getConfig(destinationName, configName);

    QMap<QString, double> arguments;

    arguments.insert("id", Config->getId());
    arguments.insert("dst", destination->getId());
    arguments.insert("data", data);
    CANmessage message = encodeMessage(common->getName(), "req_set", arguments);

    message.candata.dev_id = this->getDevice(originName)->getId();
    return message;
}

CANmessage FCPCom::encodeSetMessage(QString originName, QString destinationName, QString messageName, uint64_t data)
{
    Device* common = this->getCommon();

    Device* destination = this->getDevice(destinationName);
    Device* origin = this->getDevice(FCPCom::getDeviceID(originName));

    Message* message = this->getMessage(origin, messageName);

    QMap<QString, double> arguments;

    arguments.insert("id", message->getId());
    arguments.insert("dst", destination->getId());
    arguments.insert("data", data);
    CANmessage Canmessage = encodeMessage(common->getName(), "req_set", arguments);

    Canmessage.candata.dev_id = this->getDevice(originName)->getId();
    return Canmessage;
}

QList<JsonComponent*>* FCPCom::getComponentsByType(QString deviceName, QString componentName, QString componentType)
{
    QList<JsonComponent*>* jsonComponents = new QList<JsonComponent*>();
    Device* device;

    if (deviceName == "common") {
        device = this->getCommon();
    } else {
        device = this->getDevice(deviceName);
    }

    if (componentType == "Config") {
        QList<Config*> deviceConfigs = device->getConfigs();
        foreach (Config* conf, deviceConfigs) {
            jsonComponents->append(conf);
        }
    } else if (componentType == "Message") {
        Message* msg = device->getMessage(componentName);
        QList<CanSignal*> sigs = msg->getSignals();
        foreach (CanSignal* sig, sigs) {
            jsonComponents->append(sig);
        }
    } else if (componentType == "Command") {
        QList<Command*> deviceCommands = device->getCommands();
        foreach (Command* cmd, deviceCommands) {
            jsonComponents->append(cmd);
        }
    }

    return jsonComponents;
}

QPair<QString, QMap<QString, double>> FCPCom::decodeMessage(CANmessage msg)
{
    Message* fcp_msg = getMessage(msg.candata.dev_id, msg.candata.msg_id);
    QPair<QString, QMap<QString, double>>
        p(fcp_msg->getName(), fcp_msg->decodeMessage(msg));
    return p;
}

QPair<QString, QMap<QString, double>> FCPCom::decodeMessageMuxed(CANmessage msg)
{
    QMap<QString, double> new_signals;
    QPair<QString, QMap<QString, double>> regular_message = decodeMessage(msg);
    QString new_signal_name;
    foreach (auto key, regular_message.second.keys()) {
        auto signal = getSignal(key);
        if (signal->getMuxCount() > 1) {
            new_signal_name = key + QString::number(regular_message.second[signal->getMux()]);
        } else {
            new_signal_name = key;
        }
        new_signals[new_signal_name] = regular_message.second[key];
    }
    QPair<QString, QMap<QString, double>> p(regular_message.first, new_signals);
    return p;
}

QString FCPCom::getCurrentJsonFile()
{
    return this->_filePath;
}

bool FCPCom::isCorrectReturnCommand(CANmessage message, QString deviceName, QString commandName)
{
    QPair<QString, QMap<QString, double>> pair = this->decodeMessage(message);

    QString messageName = pair.first;
    QMap<QString, double> data = pair.second;
    if (messageName == "return_cmd") {
        uint64_t commandId = this->getCommand(deviceName, commandName)->getId();
        if (commandId == data.value("id")) {
            return true;
        }
    }
    return false;
}

void FCPCom::encodeCommandToYaml(YAML::Node& node, QString deviceName, QString commandName, double arg1, double arg2, double arg3)
{
    node.reset();
    Command* cmd = this->getCommand(deviceName, commandName);
    cmd->encodeToYaml(node, deviceName, arg1, arg2, arg3);
}

void FCPCom::encodeReqSetToYaml(YAML::Node& node, QString deviceName, QString configName, double value)
{
    node.reset();
    Config* cfg = this->getConfig(deviceName, configName);
    cfg->encodeToYaml(node, deviceName, value);
}

void FCPCom::encodeMessageToYaml(YAML::Node& node, QString deviceName, QString messageName, QMap<QString, uint64_t> values)
{
    node.reset();
    Message* msg = this->getMessage(messageName);
    verifySignalPresence(values, *msg);

    msg->encodeToYaml(node, deviceName, values);
}

void FCPCom::verifySignalPresence(QMap<QString, uint64_t>& parameters, Message& msg)
{
    QList<CanSignal*> sigs = msg.getSignals();
    QMap<QString, uint64_t> paramAux = parameters;
    if (parameters.size() < sigs.size()) {
        throw EncodingTooLittleSignalsGivenException(parameters);
    }

    foreach (CanSignal* signal, sigs) {
        if (paramAux.remove(signal->getName()) == 0) {
            throw EncodingMissingSignalException(parameters);
        }
    }

    if (paramAux.size() > 0) {
        throw EncodingTooManySignalsGivenException(parameters);
    }
}
