#include "cansignal.hpp"
#include "QDebug"

CanSignal::CanSignal(QString name,
    int start,
    int length,
    double scale,
    double offset,
    QString unit,
    int minValue,
    int maxValue,
    fcp_type_t type,
    fcp_endianess_t byteOrder,
    QString mux,
    int muxCount)
    : JsonComponent(name, -1)
{

    signal = new FcpSignal();
    signal->name = name.toStdString();
    signal->set_start(start);
    signal->set_lenght(length);
    signal->set_scale(scale);
    signal->set_offset(offset);
    signal->set_type(type);
    signal->set_endianess(byteOrder);
    signal->MinValue = minValue;
    signal->MaxValue = maxValue;
    signal->unit = unit.toStdString();
    signal->mux = mux.toStdString();
    signal->mux_count = muxCount;
}

CanSignal::~CanSignal()
{
    delete signal;
}

int CanSignal::getStart()
{
    return signal->get_signal().start;
}
int CanSignal::getLength()
{
    return signal->get_signal().length;
}
double CanSignal::getScale()
{
    return signal->get_signal().scale;
}
double CanSignal::getOffset()
{
    return signal->get_signal().offset;
}
QString CanSignal::getUnit()
{
    return QString::fromUtf8(signal->unit.c_str());
}
double CanSignal::getMinValue()
{
    return signal->MinValue;
}
double CanSignal::getMaxValue()
{
    return signal->MaxValue;
}
fcp_type_t CanSignal::getType()
{
    return signal->get_signal().type;
}
fcp_endianess_t CanSignal::getByteOrder()
{
    return signal->get_signal().endianess;
}
QString CanSignal::getMux()
{
    return QString::fromUtf8(signal->mux.c_str());
}
int CanSignal::getMuxCount()
{
    return signal->mux_count;
}

fcp_signal_t CanSignal::getCSignalStruct()
{
    return (fcp_signal_t) {
        .start = (uint16_t)this->_start,
        .length = (uint16_t)this->_length,
        .scale = this->_scale,
        .offset = this->_offset,
        .type = this->_type,
        .endianess = this->_byteOrder
    };
}

void CanSignal::encode(uint64_t& data, uint64_t newData)
{
    data |= signal->encode_signal(double(newData));
    return;
}

void CanSignal::checkBounderies(uint64_t value)
{
    if (value > this->_maxValue) {
        throw EncodingUpperBoundException(this->_maxValue, value);
    }
    if (value < this->_minValue) {
        throw EncodingLowerBoundException(this->_minValue, value);
    }
}

double CanSignal::decode(CANmessage msg)
{
    return signal->decode_signal(msg.candata);
}

void CanSignal::encodeToYaml(YAML::Node& node, double value)
{
    node["value"] = value;
}

FcpSignal* CanSignal::getSignal()
{
    return signal;
}
