#include "jsonparser.hpp"

/* The strategy used to parse the FCP Json File was to keep track of
the number of curly braces( { ) open a and closed so that we know when to
stop reading stuff from certain data types */

/*
 * Some Updates (09/2020):
 * This Class was refactored in order to use
 * Yaml-cpp lib without changing its external behaviour.
 * The external syntax was not modified
 */

JsonParser::JsonParser()
{
    this->_devices = *new QMap<int, Device*>();
    this->_logs = *new QMap<int, JsonLog*>();
    this->_common = nullptr;
}

JsonParser::~JsonParser()
{
    qDeleteAll(this->_devices);
    qDeleteAll(this->_logs);
    delete this->_common;
}

QMap<int, Device*> JsonParser::getDevices()
{
    return this->_devices;
}

QMap<int, JsonLog*> JsonParser::getLogs()
{
    return this->_logs;
}

Device* JsonParser::getCommon()
{
    return this->_common;
}

std::ifstream QFileToifstream(QFile& file)
{
    Q_ASSERT(file.isReadable());
    return std::ifstream();
}

void JsonParser::parse(QString fileName)
{
    clear();

    std::ifstream file;
    YAML::Node doc;

    if (!QFileInfo::exists(fileName)) {
        throw jsonFileDoesNotExistException(fileName);
    }

    try {
        file.open(fileName.toStdString());
    } catch (std::ifstream::failure& e) {
        throw jsonFileDoesNotExistException(fileName);
    }

    try {
        doc = YAML::Load(file);
    } catch (std::exception& e) {
        throw badJsonFileFormatException(QString::fromUtf8(e.what()));
    }

    /*Parse Devices*/
    YAML::Node devices = doc["devices"];
    int ndev = 0;
    for (YAML::const_iterator it = devices.begin(); it != devices.end(); ++it) {
        ndev++;
        Device* newDevice = parseDevice(it->second);
        _devices.insert(newDevice->getId(), newDevice);
    }

    /*Parse the Logs*/
    YAML::Node logs = doc["logs"];
    for (YAML::const_iterator it = logs.begin(); it != logs.end(); ++it) {
        JsonLog* newLog = parseLog(it->second);
        _logs.insert(newLog->getId(), newLog);
    }

    this->_common = parseDevice(doc["common"]);

    version = getStringFromNode(doc["version"]);

    qDebug() << "JSON Version: " << version;
    qDebug() << "Parsed " << ndev << " Devices";

    return;
}

JsonLog* JsonParser::parseLog(YAML::Node log)
{
    JsonLog* newLog;

    int id = log["id"].as<int>();

    QString name = getStringFromNode(log["name"]);

    int nArgs = log["n_args"].as<int>();

    QString comment = getStringFromNode(log["comment"]);

    QString string = getStringFromNode(log["string"]);

    newLog = new JsonLog(id, name, nArgs, comment, string);

    return newLog;
}

Device* JsonParser::parseDevice(YAML::Node device)
{
    Device* newDevice;
    Message* newMessage;
    Command* newCommand;
    Config* newConfig;

    QString name = getStringFromNode(device["name"]);
    int id = device["id"].as<int>();
    newDevice = new Device(id, name);
    YAML::Node msgs = device["msgs"];
    YAML::Node cmds = device["cmds"];
    YAML::Node cfgs = device["cfgs"];

    for (YAML::const_iterator it = msgs.begin(); it != msgs.end(); ++it) {
        newMessage = parseMessage(it->second);
        newDevice->addMessage(newMessage);
    }

    for (YAML::const_iterator it = cmds.begin(); it != cmds.end(); ++it) {
        newCommand = parseCommand(it->second);
        newDevice->addCommand(newCommand);
    }

    for (YAML::const_iterator it = cfgs.begin(); it != cfgs.end(); ++it) {
        newConfig = parseConfig(it->second);
        newDevice->addConfig(newConfig);
    }

    return newDevice;
}

Config* JsonParser::parseConfig(YAML::Node cfg)
{
    Config* newConfig;

    QString name = getStringFromNode(cfg["name"]);

    int id = cfg["id"].as<int>();

    QString comment = getStringFromNode(cfg["comment"]);

    newConfig = new Config(id, name, comment);

    return newConfig;
}

JsonCommandArg* JsonParser::parseCommandArg(YAML::Node arg)
{

    QString name = getStringFromNode(arg["name"]);

    int id = arg["id"].as<int>();

    QString comment = getStringFromNode(arg["comment"]);

    return new JsonCommandArg(id, name, comment);
}

Command* JsonParser::parseCommand(YAML::Node cmd)
{
    Command* newCommand;
    JsonCommandArg* newArg;

    QString name = getStringFromNode(cmd["name"]);

    int n_args = cmd["n_args"].as<int>();

    QString comment = getStringFromNode(cmd["comment"]);

    int id = cmd["id"].as<int>();

    newCommand = new Command(id, name, n_args, comment);

    if (n_args > 0) {
        YAML::Node args = cmd["args"];
        for (YAML::const_iterator it = args.begin(); it != args.end(); ++it) {
            newArg = parseCommandArg(it->second);
            newCommand->insertArg(newArg);
        }
    }

    /*args and rets are the same shit

      thank you! :) */

    if (n_args > 0) {
        YAML::Node rets = cmd["rets"];
        for (YAML::const_iterator it = rets.begin(); it != rets.end(); ++it) {
            newArg = parseCommandArg(it->second);
            newCommand->insertRet(newArg);
        }
    }
    /*Rets and args are not fully defined so we just use empty lists*/

    return newCommand;
}

Message* JsonParser::parseMessage(YAML::Node msg)
{
    Message* newMessage;
    CanSignal* newSignal;
    YAML::Node signalMap = msg["signals"];

    QString name = getStringFromNode(msg["name"]);
    int id = msg["id"].as<int>();
    int dlc = msg["dlc"].as<int>();

    newMessage = new Message(id, name, dlc, 0);

    for (YAML::const_iterator it = signalMap.begin(); it != signalMap.end(); ++it) {
        newSignal = parseSignal(it->second);
        newMessage->addSignal(newSignal);
    }

    int frequency = msg["frequency"].as<int>();

    newMessage->setFrequency(frequency);
    return newMessage;
}

CanSignal* JsonParser::parseSignal(YAML::Node signal)
{
    CanSignal* newSignal;

    QString name = getStringFromNode(signal["name"]);

    int start = signal["start"].as<int>();

    int length = signal["length"].as<int>();

    double scale = signal["scale"].as<double>();

    double offset = signal["offset"].as<double>();

    QString unit = getStringFromNode(signal["unit"]);

    QString comment = getStringFromNode(signal["comment"]);

    double minValue = signal["min_value"].as<double>();

    double maxValue = signal["max_value"].as<double>();

    QString type_str = getStringFromNode(signal["type"]);

    fcp_type_t type;

    if (type_str == "signed") {
        type = FCP_SIGNED;
    } else if (type_str == "double") {
        type = FCP_DOUBLE;
    } else if (type_str == "float") {
        type = FCP_FLOAT;
    } else {
        type = FCP_UNSIGNED;
    }

    QString byteOrder_str = getStringFromNode(signal["byte_order"]);

    fcp_endianess_t byteOrder;

    if (byteOrder_str == "big_endian") {
        byteOrder = FCP_BIG;
    } else {
        byteOrder = FCP_LITTLE;
    }

    QString mux = getStringFromNode(signal["mux"]);

    int muxCount = signal["mux_count"].as<int>();

    newSignal = new CanSignal(name,
        start,
        length,
        scale,
        offset,
        unit,
        minValue,
        maxValue,
        type,
        byteOrder,
        mux,
        muxCount);

    return newSignal;
}

QString JsonParser::getStringFromNode(YAML::Node node)
{
    return QString::fromUtf8(node.as<std::string>().c_str());
}

void JsonParser::clear()
{
    this->_devices.clear();
    this->_logs.clear();
    this->_common = nullptr;
}
