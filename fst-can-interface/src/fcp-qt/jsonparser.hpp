#ifndef JSONPARSER_HPP
#define JSONPARSER_HPP
#include "Exceptions/badjsonfileformatexception.hpp"
#include "Exceptions/jsonfiledoesnotexistexception.hpp"
#include "cansignal.hpp"
#include "device.hpp"
#include "jsonlog.hpp"
#include <QChar>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QString>
#include <fstream>

class JsonParser {
public:
    JsonParser();

private:
    QString version;
    QMap<int, Device*> _devices;
    QMap<int, JsonLog*> _logs;
    Device* _common;

    Device* parseDevice(YAML::Node device);
    Message* parseMessage(YAML::Node msg);
    CanSignal* parseSignal(YAML::Node signal);
    Command* parseCommand(YAML::Node cmd);
    JsonCommandArg* parseCommandArg(YAML::Node arg);
    Config* parseConfig(YAML::Node cfg);
    JsonLog* parseLog(YAML::Node log);

    QString getStringFromNode(YAML::Node node);

    void clear();

public:
    void parse(QString filename);
    QMap<int, Device*> getDevices();
    QMap<int, JsonLog*> getLogs();
    Device* getCommon();

    ~JsonParser();
};

#endif // JSONPARSER_HPP
