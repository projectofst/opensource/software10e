#ifndef JSONCOMPONENT_HPP
#define JSONCOMPONENT_HPP

#include <QString>

class JsonComponent {
public:
    /**
     * @brief JsonComponent Constructor.
     * @param name - Name of the JsonComponent.
     * @param id - Identifier of the JsonComponent.
     */
    JsonComponent(QString name, uint16_t id);

    /**
     * @brief JsonComponent Destructor.
     */
    virtual ~JsonComponent();

private:
    QString _name;
    uint16_t _id;

public:
    /**
     * @brief Returns the name of the JsonComponent.
     * @return Returns a QString with the name of the JsonComponent.
     */
    virtual QString getName();

    /**
     * @brief Returns the id of the JsonComponent.
     * @return Returns a unsigned 16 bit integer with the id of the JsonComponent.
     */
    uint16_t getId();
};

#endif // JSONCOMPONENT_HPP
