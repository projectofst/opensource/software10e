#ifndef COMMAND_HPP
#define COMMAND_HPP
#include "../../lib/fcp-cpp/src/fcp_command.hpp"
#include "Exceptions/jsoncommandnoargsexception.hpp"
#include "Exceptions/jsoncommandnoretsexception.hpp"
#include "Exceptions/jsonexception.hpp"
#include "QMap"
#include "jsoncommandarg.hpp"
#include "jsoncomponent.hpp"
#include <QString>
#include <QStringList>
#include <yaml-cpp/yaml.h>

class Command : public JsonComponent {
public:
    Command(int id, QString name, int nArgs, QString comment);
    Command(int nArgs, QString comment);
    ~Command();

private:
    int _nArgs;
    QString _comment;
    QMap<int, JsonCommandArg*> _args;
    QMap<int, JsonCommandArg*> _rets;
    FcpCommand* command;

public:
    int getNArgs();
    QString getComment();
    FcpCommand* getFcpCommad();
    QList<JsonCommandArg*> getArgs();
    QList<JsonCommandArg*> getRets();

    void encodeToYaml(YAML::Node& node, QString deviceName, double value1, double value2, double value3);

    void insertArg(JsonCommandArg* arg);
    void insertRet(JsonCommandArg* ret);
    void updateListArgs();
    void updateListRets();
};

#endif // COMMAND_HPP
