#include "command.hpp"

Command::Command(int id, QString name, int nArgs, QString comment)
    : JsonComponent(name, id)
{
    command = new FcpCommand();
    command->n_args = nArgs;
    command->comment = comment.toStdString();
    this->_args = *new QMap<int, JsonCommandArg*>();
    this->_rets = *new QMap<int, JsonCommandArg*>();
}

Command::~Command()
{
    _args.clear();
    _rets.clear();
    delete command;
}

int Command::getNArgs()
{
    return command->n_args;
}
QString Command::getComment()
{
    return QString::fromStdString(command->comment);
}
QList<JsonCommandArg*> Command::getArgs()
{
    updateListArgs();
    return this->_args.values();
}

QList<JsonCommandArg*> Command::getRets()
{
    updateListRets();
    QList<JsonCommandArg*> list;
    list = this->_rets.values();
    if (list.isEmpty()) {
        throw JsonCommandNoRetsException(this->getName());
    }

    return list;
}

void Command::insertArg(JsonCommandArg* json_arg)
{
    FcpArgument arg;
    arg.id = json_arg->getId();
    arg.name = json_arg->getName().toStdString();
    arg.comment = json_arg->getComment().toStdString();
    command->args[json_arg->getName().toStdString()] = arg;
    return;
}

void Command::insertRet(JsonCommandArg* arg)
{
    FcpArgument new_ret;
    new_ret.id = arg->getId();
    new_ret.name = arg->getName().toStdString();
    new_ret.comment = arg->getComment().toStdString();
    command->rets[arg->getName().toStdString()] = new_ret;
    return;
}

void Command::encodeToYaml(YAML::Node& node, QString deviceName, double value1, double value2, double value3)
{
    YAML::Node infoNode, argNode;
    node["Device"] = deviceName.toStdString();
    node["Type"] = "cmd";

    infoNode["Name"] = command->name;
    node["Information"] = infoNode;

    argNode["arg1"] = value1;
    argNode["arg2"] = value2;
    argNode["arg3"] = value3;

    infoNode["args"] = argNode;
}

void Command::updateListArgs()
{

    for (auto const& [key, val] : command->args) {
        if (!_args.contains(val.id)) {
            _args[val.id] = new JsonCommandArg(val.id, QString::fromStdString(val.name), QString::fromStdString(val.comment));
        }
    }
}

void Command::updateListRets()
{
    for (auto const& [key, val] : command->rets) {
        if (!_args.contains(val.id)) {
            _args[val.id] = new JsonCommandArg(val.id, QString::fromStdString(val.name), QString::fromStdString(val.comment));
        }
    }
}

FcpCommand* Command::getFcpCommad()
{
    return this->command;
}
