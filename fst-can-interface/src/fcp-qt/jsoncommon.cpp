#include "jsoncommon.hpp"

JsonCommon::JsonCommon(int id, QString name)
    : JsonComponent(name, id)
{
    this->_messages = *new QList<Message*>();
}

int JsonCommon::getId()
{
    return this->_id;
}

QString JsonCommon::getName()
{

    return this->_name;
}

QList<Message*> JsonCommon::getMessages()
{
    return this->_messages;
}

void JsonCommon::appendMessage(Message* newMessage)
{
    this->_messages.append(newMessage);
}
