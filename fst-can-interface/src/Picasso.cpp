#include "Picasso.hpp"

Picasso::Picasso(MainToolbar* toolbar, MainWindow* parent, Comsguy*)
{
    mainwindow = parent;
    this->log = new Helper();

    MainToolBar = toolbar;
    connect(MainToolBar, &MainToolbar::openUiWindow,
        this, &Picasso::launchWidget);

    this->config_manager = parent->getConfigManager();
    /*connect(MainToolBar, &MainToolbar::saveLayout,
            this, &Picasso::saveLayout);
    connect(MainToolBar, &MainToolbar::deleteLayout,
            this, &Picasso::closeLayout);
    connect(MainToolBar->LayoutCombo, QOverload<const QString &>::of(&QComboBox::activated),
            this, &Picasso::loadLayout);*/
    // updateToolbar();

    /* Widget Factory
     * Add you widget here to have it available through the
     * Picasso::launchWidget interface
     */
    factory.registerType<Console>("Console");
    factory.registerType<DevStatus>("Device Status");
    factory.registerType<versions>("Versions");
    factory.registerType<Log>("Log");
    factory.registerType<ConfigWindow>("Config");
    factory.registerType<Lap_time>("Lap");
    factory.registerType<General>("General");
    factory.registerType<ASWidget>("as");
    factory.registerType<General>("dcu");
    factory.registerType<General>("dash");
    factory.registerType<General>("te");
    factory.registerType<CCU>("ccu_left");
    factory.registerType<CCU>("ccu_right");
    factory.registerType<DynamicUnit>("general_widget");
    factory.registerType<ISA>("isabel");
    factory.registerType<AMS>("master");
    factory.registerType<Inverters>("iib");
    factory.registerType<Timeline>("Timeline");
    factory.registerType<Etas>("etas");

    connect(mainwindow, &MainWindow::willClose, this, &Picasso::deleteLogger);

    // Initialize checker

    dock_checker = new QTimer(this);
    connect(dock_checker, &QTimer::timeout, this, &Picasso::checkDockWidgets);
    dock_checker->start(500);
}

void Picasso::launchWidget(QString type, bool detached)
{
    auto parent = mainwindow;

    FCPCom* fcp = mainwindow->getFCPCom();
    UiWindow* widget;

    if (type.contains("du")) {
        widget = factory.create("general_widget", parent, fcp, this->log, this->config_manager);
        widget->setPrivateValue("du_signals");
    } else {
        widget = factory.create(type, parent, fcp, this->log, this->config_manager);
    }
    if (widget == nullptr) {
        QMessageBox::warning(mainwindow, "Error", QString("Couldn't launch widget %1").arg(type));
        return;
    }

    connect(widget, &UiWindow::openWidget, this, &Picasso::launchWidget);

    ads::CDockWidget* ads_dock_widget = new ads::CDockWidget(type);

    ads_dock_widget->setStyleSheet("");

    ads_dock_widget->setWidget(widget);

    mainwindow->m_DockManager->addDockWidget(ads::LeftDockWidgetArea, ads_dock_widget);
    if (detached) {
        ads_dock_widget->setFloating();
    }

    connect(mainwindow->interfaceCom, &Interface::broadcast_in_message,
        widget, &UiWindow::process_CAN_message);
    connect(widget, &UiWindow::send_to_CAN,
        mainwindow->interfaceCom, &Interface::new_messages);

    // mainwindow->attachJsonObserver(widget);
    // connect(widget, &UiWindow::removeFromObservers, mainwindow, &MainWindow::detachJsonObserver);
    // connect(widget, &UiWindow::removeFromActiveWidgets, this, &Picasso::removeActiveWidget);

    /*This next bit of code connects the console rewind time signal to all the other widgets*/
    /*
    this->_activeWindows.append(widget);
    if(EnumValue == ConDock){
        this->_activeConsoles.append(widget);
        foreach(UiWindow* win, this->_activeWindows){
            connect(widget, &UiWindow::changeLogTime, win, &UiWindow::handleTimeChange);
            connect(widget, &UiWindow::newLogMessage, win, &UiWindow::process_CAN_message);
        }
    }
    else{
        foreach(UiWindow* con, this->_activeConsoles){
            connect(con, &UiWindow::changeLogTime, widget, &UiWindow::handleTimeChange);
            connect(con, &UiWindow::newLogMessage, widget, &UiWindow::process_CAN_message);
        }
    }
    */
}

void Picasso::checkDockWidgets()
{
    foreach (auto widget, mainwindow->m_DockManager->dockWidgetsMap().values()) {
        if (!widget->widget()->isVisible() && !widget->widget()->isMinimized()) {
            widget->deleteDockWidget();
            mainwindow->m_DockManager->removeDockWidget(widget);
            break;
        }
    }
}

Picasso::~Picasso(void)
{
}

void Picasso::update(int)
{
    qDebug() << "tab double clicked";
}

/*
void Picasso::saveLayout(void)
{
    // Layout file creation
    QStringList LayoutList = LayoutDir.entryList(QStringList("*.ini"), QDir::NoFilter, QDir::NoSort);
    LayoutList.replaceInStrings(".ini","");
    int i = 1;
    QString init = "Layout";
    foreach (QString LayoutName,LayoutList) {
        QString is = QString::number(i);
        QString TempName = init;
        TempName.append(is);
        if (0==LayoutName.compare(TempName))
            i = i + 1;
    }
    QString is = QString::number(i);
    LayoutName = init.append(is.append(".ini"));
    QSettings layout(LayoutName.prepend("set/Layouts/"),QSettings::IniFormat);

    // Add DockWidget Data
    ListOfChildren = mainwindow->findChildren<QDockWidget *>();
    foreach (QDockWidget* dock, ListOfChildren) {
        layout.beginGroup(dock->objectName());
        layout.setValue("Geometry",dock->geometry());
        layout.endGroup();
    }

    // Add TabbedDocks Data
    while (!ListOfChildren.isEmpty()) {
        foreach (QDockWidget* dock, ListOfChildren) {
            QList<QDockWidget*> TabbedDocks = mainwindow->tabifiedDockWidgets(dock);
            if (!TabbedDocks.isEmpty()) {
                foreach (QDockWidget *tabdock, TabbedDocks) {
                    layout.beginGroup(tabdock->objectName());
                    layout.setValue("ParentDock",dock->objectName());
                    layout.endGroup();
                    ListOfChildren.removeOne(tabdock);
                }
            }
            layout.beginGroup(dock->objectName());
            layout.setValue("ParentDock",QVariant());
            layout.endGroup();
            ListOfChildren.removeOne(dock);
            break;
        }
    }

    // Save MainWindowState
    layout.beginGroup("MainWindowState");
    layout.setValue("geometry", mainwindow->saveGeometry());
    layout.setValue("windowState", mainwindow->saveState());
    layout.endGroup();

    // Save
    layout.sync();
    updateToolbar();

}
*/

/*void Picasso::loadLayout(QString Layout)
{
    closeLayout();

    // Add Docks
    QSettings newlayout(Layout.prepend("set/Layouts/").append(".ini"),QSettings::IniFormat,nullptr);
    foreach (QString group, newlayout.childGroups()) {
        if (group!="MainWindowState") {
            openDockWidget(group.mid(1,7));
        }
    }

    // Tabbify respective
    ListOfChildren = mainwindow->findChildren<QDockWidget *>();
    ListOfDocks.clear();
    foreach (QDockWidget *dock, ListOfChildren)
        ListOfDocks.append(dock->objectName());
    foreach (QString group, newlayout.childGroups()) {
        newlayout.beginGroup(group);
        if (newlayout.value("ParentDock").isValid()) {
            mainwindow->tabifyDockWidget(ListOfChildren.at(ListOfDocks.indexOf(newlayout.value("ParentDock").toString(),0)),
                                         ListOfChildren.at(ListOfDocks.indexOf(group)));
        }
        newlayout.endGroup();
    }

    // Load MainWindowState
    newlayout.beginGroup("MainWindowState");
    mainwindow->restoreGeometry(newlayout.value("geometry").toByteArray());
    mainwindow->restoreState(newlayout.value("windowState").toByteArray());
    newlayout.endGroup();
}
*/

/*
void Picasso::closeLayout(void)
{
    ListOfChildren = mainwindow->findChildren<QDockWidget *>();
    foreach (QObject* object, ListOfChildren)
        delete object;
}
*/

/*
 * void Picasso::updateObjectName(QObject *TargetObj, QString ObjType)
{
    repeated = 0;
    QList<QObject*> ListOfChildren = mainwindow->children();
    foreach (QObject* object, ListOfChildren) {
        if (object->objectName().startsWith(ObjType))
            repeated = repeated + 1;
    }
    FinalName.asprintf("%s - %d",ObjType.toLocal8Bit().data(),repeated+1);
    TargetObj->setObjectName(FinalName);
}
*/

/*
void Picasso::updateToolbar(void)
{
    MainToolBar->LayoutCombo->clear();
    QStringList LayoutList = LayoutDir.entryList(QStringList("*.ini"), QDir::NoFilter, QDir::NoSort);
    LayoutList.replaceInStrings(".ini","");
    MainToolBar->LayoutCombo->addItems(LayoutList);
}

void Picasso::removeActiveWidget(UiWindow* win){
    this->_activeWindows.removeAll(win);
    this->_activeConsoles.removeAll(win);
}
*/

void Picasso::deleteLogger()
{
    qDebug() << "Saving log...";
    delete this->log;
}
