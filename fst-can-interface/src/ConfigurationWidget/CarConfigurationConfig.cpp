#include "CarConfigurationConfig.hpp"
#include "ui_CarConfigurationConfig.h"

CarConfigurationConfig::CarConfigurationConfig(QWidget* parent, FCPCom* fcp)
    : QWidget(parent)
    , ui(new Ui::CarConfigurationConfig)
{
    ui->setupUi(this);
    this->_fcp = fcp;
    QList<Device*> devices = this->_fcp->getDevices();
    if (fcp != nullptr) {
        foreach (Device* dev, devices) {
            if (!dev->getConfigs().isEmpty()) {
                this->ui->allDevices->addItem(dev->getName());
            }
        }
    }
    QIntValidator validator(0, (2 ^ 16) - 1, this);

    this->ui->ValueInput->setValidator(&validator);
}

CarConfigurationConfig::~CarConfigurationConfig()
{
    emit configDestroyed(this);
    delete ui;
}

QString CarConfigurationConfig::getDevice()
{
    return this->ui->allDevices->currentText();
}

QString CarConfigurationConfig::getConfig()
{

    return this->ui->allConfigs->currentText();
}

void CarConfigurationConfig::setDevice(QString device)
{
    Device* dev = this->_fcp->getDevice(device);

    int indexOf = this->ui->allDevices->findText(dev->getName());
    if (indexOf > 0)
        this->ui->allDevices->setCurrentIndex(indexOf);
}
void CarConfigurationConfig::setConfig(QString config)
{
    Device* dev = this->_fcp->getDevice(this->ui->allDevices->currentText());
    Config* cfg = this->_fcp->getConfig(dev->getName(), config);

    int indexOf = this->ui->allConfigs->findText(cfg->getName());
    if (indexOf > 0)
        this->ui->allConfigs->setCurrentIndex(indexOf);
}
void CarConfigurationConfig::setValue(int value)
{
    this->ui->ValueInput->setText(QString::number(value));
}

int CarConfigurationConfig::getValue()
{
    return this->ui->ValueInput->text().toInt();
}

void CarConfigurationConfig::on_allDevices_currentIndexChanged(const QString& arg1)
{
    Device* dev = this->_fcp->getDevice(arg1);

    QList<Config*> configs = dev->getConfigs();

    this->ui->allConfigs->clear();
    foreach (Config* cfg, configs) {
        this->ui->allConfigs->addItem(cfg->getName());
    }
}

void CarConfigurationConfig::on_deleteButton_clicked()
{
    this->~CarConfigurationConfig();
}
