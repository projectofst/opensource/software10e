#include "CarConfigCommand.hpp"
#include "ui_CarConfigCommand.h"

CarConfigCommand::CarConfigCommand(QWidget* parent, FCPCom* fcp)
    : QWidget(parent)
    , ui(new Ui::CarConfigCommand)
{
    ui->setupUi(this);
    this->_fcp = fcp;
    QList<Device*> devices = this->_fcp->getDevices();
    if (fcp != nullptr) {
        foreach (Device* dev, devices) {
            if (!dev->getCommands().isEmpty()) {
                this->ui->allDevices->addItem(dev->getName());
            }
        }
    }
    QIntValidator validator(0, (2 ^ 16) - 1, this);

    this->ui->agr1Input->setValidator(&validator);
    this->ui->agr2Input->setValidator(&validator);
    this->ui->arg3Input->setValidator(&validator);
}

CarConfigCommand::~CarConfigCommand()
{
    emit commandDestroyed(this);
    delete ui;
}

void CarConfigCommand::on_allDevices_currentIndexChanged(const QString& arg1)
{
    Device* dev = this->_fcp->getDevice(arg1);

    QList<Command*> commands = dev->getCommands();

    this->ui->allCommands->clear();
    foreach (Command* cmd, commands) {
        this->ui->allCommands->addItem(cmd->getName());
    }
}

QString CarConfigCommand::getDevice()
{
    return this->ui->allDevices->currentText();
}

QString CarConfigCommand::getCommand()
{

    return this->ui->allCommands->currentText();
}

void CarConfigCommand::setDevice(QString device)
{
    Device* dev = this->_fcp->getDevice(device);

    int indexOf = this->ui->allDevices->findText(dev->getName());
    if (indexOf > 0)
        this->ui->allDevices->setCurrentIndex(indexOf);
}
void CarConfigCommand::setCommand(QString command)
{
    Device* dev = this->_fcp->getDevice(this->ui->allDevices->currentText());
    Command* cmd = this->_fcp->getCommand(dev->getName(), command);

    int indexOf = this->ui->allCommands->findText(cmd->getName());
    if (indexOf > 0)
        this->ui->allCommands->setCurrentIndex(indexOf);
}
void CarConfigCommand::setArgs(int arg1, int arg2, int arg3)
{
    this->ui->agr1Input->setText(QString::number(arg1));
    this->ui->agr2Input->setText(QString::number(arg2));
    this->ui->arg3Input->setText(QString::number(arg3));
}

QList<int> CarConfigCommand::getArgs()
{
    QList<int> argList;

    argList.append(this->ui->agr1Input->text().toInt());
    argList.append(this->ui->agr2Input->text().toInt());
    argList.append(this->ui->arg3Input->text().toInt());
    return argList;
}

void CarConfigCommand::on_deleteButton_clicked()
{
    this->~CarConfigCommand();
}
