#ifndef CARCONFIGURATIONCONFIG_HPP
#define CARCONFIGURATIONCONFIG_HPP

#include "fcpcom.hpp"
#include <QWidget>

namespace Ui {
class CarConfigurationConfig;
}

class CarConfigurationConfig : public QWidget {
    Q_OBJECT

public:
    explicit CarConfigurationConfig(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~CarConfigurationConfig();

    QString getDevice();

    QString getConfig();

    void setDevice(QString device);
    void setConfig(QString command);
    void setValue(int value);

    int getValue();

private:
    Ui::CarConfigurationConfig* ui;
    FCPCom* _fcp;

signals:
    void configDestroyed(CarConfigurationConfig* wid);
private slots:
    void on_allDevices_currentIndexChanged(const QString& arg1);
    void on_deleteButton_clicked();
};

#endif // CARCONFIGURATIONCONFIG_HPP
