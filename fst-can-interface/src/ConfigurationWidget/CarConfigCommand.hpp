#ifndef CARCONFIGCOMMAND_HPP
#define CARCONFIGCOMMAND_HPP

#include <QWidget>
#include <fcpcom.hpp>

namespace Ui {
class CarConfigCommand;
}

class CarConfigCommand : public QWidget {
    Q_OBJECT

public:
    explicit CarConfigCommand(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~CarConfigCommand();

    QString getDevice();
    QString getCommand();
    QList<int> getArgs();

    void setDevice(QString device);
    void setCommand(QString command);
    void setArgs(int arg1, int arg2, int arg3);

private slots:
    void on_allDevices_currentIndexChanged(const QString& arg1);

    void on_deleteButton_clicked();

private:
    Ui::CarConfigCommand* ui;
    FCPCom* _fcp;

signals:
    void commandDestroyed(CarConfigCommand* command);
};

#endif // CARCONFIGCOMMAND_HPP
