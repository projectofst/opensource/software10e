#ifndef CONFIGURATIONWIDGET_HPP
#define CONFIGURATIONWIDGET_HPP

#include "CarConfigCommand.hpp"
#include "CarConfigurationConfig.hpp"
#include "can-ids/CAN_IDs.h"
#include "fcpcom.hpp"
#include <QDir>
#include <QFileDialog>
#include <QWidget>
#include <memory>
#include <yaml-cpp/yaml.h>

namespace Ui {
class ConfigurationWidget;
}

class ConfigurationWidget : public QWidget {
    Q_OBJECT

public:
    explicit ConfigurationWidget(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~ConfigurationWidget();

    void setFCP(FCPCom* fcp);

private slots:
    void on_toggleYamlViewButton_clicked(bool checked);

    void handleCommandDestruction(CarConfigCommand* destroyedWidget);
    void handleConfigDestruction(CarConfigurationConfig* destroyedWidget);

    void on_NewCommandButton_clicked();

    void on_ExportButton_clicked();

    void on_ImportButton_clicked();

    void clearAll();

    QString loadFile();

    void parseYaml();

    void on_refreshButton_clicked();

    void on_chooseFile_clicked();

    void copyToYamlEditor(QString fileName);

    void on_SendButton_clicked();

    void on_NewConfigButton_clicked();

    void clearWidgetLists();

private:
    Ui::ConfigurationWidget* ui;
    QList<CarConfigCommand*> _commands;
    QList<CarConfigurationConfig*> _configs;

    FCPCom* _fcp;

    QString _chosenFile;

signals:
    void newMessage(CANmessage msg);
};

#endif // CONFIGURATIONWIDGET_HPP
