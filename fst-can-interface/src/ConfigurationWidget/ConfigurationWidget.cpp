#include "ConfigurationWidget.hpp"
#include "can-ids/CAN_IDs.h"
#include "ui_ConfigurationWidget.h"
#include <QDebug>

ConfigurationWidget::ConfigurationWidget(QWidget* parent, FCPCom* fcp)
    : QWidget(parent)
    , ui(new Ui::ConfigurationWidget)
{
    ui->setupUi(this);
    this->ui->toggleYamlViewButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    this->_fcp = fcp;

    QPixmap pixmap(":/Assets/CustomIcons/upload.png");
    QIcon ButtonIcon(pixmap);

    this->ui->ExportButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    this->ui->ExportButton->setIcon(ButtonIcon);
    this->ui->ExportButton->setText("Export");
    QFont font;
    font.setPointSize(10);
    this->ui->ExportButton->setFont(font);

    QPixmap Import(":/Assets/CustomIcons/download.png");
    QIcon ImportButtonIcon(Import);
    this->ui->ImportButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    this->ui->ImportButton->setIcon(ImportButtonIcon);
    this->ui->ImportButton->setText("Import");
    this->ui->ImportButton->setFont(font);

    QPixmap Sendpixmap(":/Assets/CustomIcons/email.png");
    QIcon sendButtonIcon(Sendpixmap);

    this->ui->SendButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    this->ui->SendButton->setIcon(sendButtonIcon);
    this->ui->SendButton->setText("Send");
    this->ui->SendButton->setFont(font);

    QPixmap refresh(":/Assets/CustomIcons/refresh.png");
    QIcon refreshButtonIcon(refresh);
    this->ui->refreshButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    this->ui->refreshButton->setIcon(refreshButtonIcon);
    this->ui->refreshButton->setText("Refresh");
    this->ui->refreshButton->setFont(font);
    this->ui->yamlEditor->setVisible(false);
    this->ui->fileWidgets->setVisible(false);
}

ConfigurationWidget::~ConfigurationWidget()
{
    foreach (CarConfigCommand* cmd, this->_commands) {
        if (cmd != nullptr) {
            delete cmd;
        }
    }
    foreach (CarConfigurationConfig* cfg, this->_configs) {
        if (cfg != nullptr) {
            delete cfg;
        }
    }

    delete ui;
}

void ConfigurationWidget::on_toggleYamlViewButton_clicked(bool checked)
{
    this->ui->yamlEditor->setVisible(checked);
    this->ui->fileWidgets->setVisible(checked);
}

void ConfigurationWidget::on_NewCommandButton_clicked()
{
    CarConfigCommand* newCarCommand = new CarConfigCommand(this, this->_fcp);
    connect(newCarCommand, &CarConfigCommand::commandDestroyed, this, &ConfigurationWidget::handleCommandDestruction);

    this->ui->scrollLayout->addWidget(newCarCommand);
    this->_commands.append(newCarCommand);
}

void ConfigurationWidget::setFCP(FCPCom* fcp)
{
    this->_fcp = fcp;
}

void ConfigurationWidget::handleCommandDestruction(CarConfigCommand* destroyedWidget)
{
    this->_commands.removeAll(destroyedWidget);
}

void ConfigurationWidget::handleConfigDestruction(CarConfigurationConfig* destroyedWidget)
{
    this->_configs.removeAll(destroyedWidget);
}
void ConfigurationWidget::on_ExportButton_clicked()
{
    this->ui->yamlEditor->clear();
    YAML::Emitter out;
    out << YAML::BeginMap;
    YAML::Node commands;
    YAML::Node configs;
    int it = 1;
    try {
        foreach (CarConfigCommand* cmd, this->_commands) {

            YAML::Node commandNode;
            QString device = cmd->getDevice();
            QString commandName = cmd->getCommand();
            QList<int> args = cmd->getArgs();
            this->_fcp->encodeCommandToYaml(commandNode, device, commandName, args[0], args[1], args[2]);
            commands.push_back(commandNode);
            it++;
        }

        out << YAML::Key << "Commands";
        out << YAML::Value << commands;
        out << YAML::Key << "Configs";

        foreach (CarConfigurationConfig* cfg, this->_configs) {

            YAML::Node configNode;
            QString device = cfg->getDevice();
            QString configName = cfg->getConfig();
            int value = cfg->getValue();
            this->_fcp->encodeReqSetToYaml(configNode, device, configName, value);
            configs.push_back(configNode);
            it++;
        }
        out << YAML::Value << configs;
        out << YAML::EndMap;
    } catch (JsonException& e) {
        this->ui->errorOutput->setText(e.toString());
    } catch (std::exception& stdex) {
        this->ui->errorOutput->setText(stdex.what());
    }
    this->ui->yamlEditor->clear();
    this->ui->yamlEditor->appendPlainText(out.c_str());
}

QString ConfigurationWidget::loadFile()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Yaml File"), QDir::currentPath(), tr("Log Files (*.yml *.yaml)"));

    return fileName;
}

void ConfigurationWidget::copyToYamlEditor(QString fileName)
{
    QFile inputFile(fileName);

    if (inputFile.open(QIODevice::ReadOnly)) {
        QTextStream in(&inputFile);
        {
            QString text = in.readAll();
            this->ui->yamlEditor->clear();
            this->ui->yamlEditor->appendPlainText(text);
        }
    }
    inputFile.close();
}

void ConfigurationWidget::parseYaml()
{
    this->clearWidgetLists();
    qDebug() << "1";
    try {
        YAML::Node nodes = YAML::Load(this->ui->yamlEditor->toPlainText().toStdString());
        for (auto it = nodes.begin(); it != nodes.end(); it++) {
            qDebug() << QString::fromStdString(it->first.as<std::string>());
            YAML::Node n = nodes[it->first.as<std::string>()];
            for (auto i2t = n.begin(); i2t != n.end(); i2t++) {
                YAML::Node currentNode = (*i2t);
                if (QString::fromStdString(currentNode["Type"].as<std::string>()) == "cmd") {
                    YAML::Node info = currentNode["Information"];
                    QString deviceName = (QString::fromStdString(currentNode["Device"].as<std::string>()));
                    QString commandName = (QString::fromStdString(info["Name"].as<std::string>()));

                    YAML::Node args = info["args"];
                    int arg1 = args["arg1"].as<int>();
                    int arg2 = args["arg2"].as<int>();
                    int arg3 = args["arg3"].as<int>();

                    CarConfigCommand* newCarCommand = new CarConfigCommand(this, this->_fcp);

                    newCarCommand->setDevice(deviceName);
                    newCarCommand->setCommand(commandName);
                    newCarCommand->setArgs(arg1, arg2, arg3);

                    this->ui->scrollLayout->addWidget(newCarCommand);
                    this->_commands.append(newCarCommand);
                    connect(newCarCommand, &CarConfigCommand::commandDestroyed, this, &ConfigurationWidget::handleCommandDestruction);
                } else if (QString::fromStdString(currentNode["Type"].as<std::string>()) == "cfg") {
                    YAML::Node info = currentNode["Information"];
                    QString deviceName = (QString::fromStdString(currentNode["Device"].as<std::string>()));
                    QString configName = (QString::fromStdString(info["Name"].as<std::string>()));

                    int value = info["value"].as<int>();

                    CarConfigurationConfig* newCarConfig = new CarConfigurationConfig(this, this->_fcp);

                    newCarConfig->setDevice(deviceName);
                    newCarConfig->setConfig(configName);
                    newCarConfig->setValue(value);

                    this->ui->scrollLayout->addWidget(newCarConfig);
                    this->_configs.append(newCarConfig);
                    connect(newCarConfig, &CarConfigurationConfig::configDestroyed, this, &ConfigurationWidget::handleConfigDestruction);
                }
            }
        }
    } catch (JsonException& e) {
        this->ui->errorOutput->setText(e.toString());
    } catch (std::exception& e) {
        this->ui->errorOutput->setText(e.what());
    }
}

void ConfigurationWidget::on_ImportButton_clicked()
{
    QString fileName = loadFile();

    copyToYamlEditor(fileName);

    this->ui->filePath->setText(fileName);
    this->_chosenFile = fileName;
    parseYaml();
}

void ConfigurationWidget::clearAll()
{
    clearWidgetLists();

    this->ui->yamlEditor->clear();
}

void ConfigurationWidget::on_refreshButton_clicked()
{
    parseYaml();
}

void ConfigurationWidget::on_chooseFile_clicked()
{
    QString file = loadFile();
    this->ui->filePath->setText(file);
    copyToYamlEditor(file);
}

void ConfigurationWidget::on_SendButton_clicked()
{
    this->clearWidgetLists();
    try {
        foreach (CarConfigCommand* cmd, this->_commands) {
            QString deviceName = cmd->getDevice();
            QString commandName = cmd->getCommand();

            QList<int> values = cmd->getArgs();

            CANmessage msg = this->_fcp->encodeSendCommand("interface", deviceName, commandName, values[0], values[1], values[2]);

            emit newMessage(msg);
        }
        foreach (CarConfigurationConfig* cfg, this->_configs) {
            QString deviceName = cfg->getDevice();
            QString configName = cfg->getConfig();

            int value = cfg->getValue();

            CANmessage msg = this->_fcp->encodeSetConfig("interface", deviceName, configName, value);

            emit newMessage(msg);
        }
    } catch (JsonException& e) {
        this->ui->errorOutput->setText(e.toString());
    }
}

void ConfigurationWidget::on_NewConfigButton_clicked()
{
    CarConfigurationConfig* newCarConfig = new CarConfigurationConfig(this, this->_fcp);
    connect(newCarConfig, &CarConfigurationConfig::configDestroyed, this, &ConfigurationWidget::handleConfigDestruction);

    this->ui->scrollLayout->addWidget(newCarConfig);
    this->_configs.append(newCarConfig);
}

void ConfigurationWidget::clearWidgetLists()
{
    foreach (CarConfigCommand* cmd, this->_commands) {
        if (cmd != nullptr) {
            delete cmd;
        }
    }
    foreach (CarConfigurationConfig* cfg, this->_configs) {
        if (cfg != nullptr) {
            delete cfg;
        }
    }
    this->_configs.clear();
    this->_commands.clear();
}
