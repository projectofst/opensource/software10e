#pragma once
#ifndef RUN_LAPS_HPP
#define RUN_LAPS_HPP

#include "lap_time.hpp"
#include "single_lap.hpp"
#include <QWidget>
class Single_lap;

namespace Ui {

class run_laps;
}

class run_laps : public QWidget {
    Q_OBJECT

public:
    explicit run_laps(QWidget* parent = nullptr, int new_track_length = 0, QString new_track_type = "");
    ~run_laps();
    void add_lap(Single_lap* new_lap);
    Single_lap* lap_number(int i);
    int get_number_laps();
    void remove_lap(int index);
    void clear_gen_info();
    void set_new_fastest_lap(Single_lap* lap);
    Single_lap* get_fastest_lap();
    void set_track(QString new_track);
    void set_length(int new_length);
    void on_clear_clicked();
    int get_dnf_flag();
    void set_DNF_flag(int flag);

private slots:
    void update_general_info();
    QString transform_time(qint64 time);
    QList<qint64> format_time(qint64 time);

    void on_clear_all_clicked();

private:
    QVector<Single_lap*> laps;
    Single_lap* best_lap;
    Ui::run_laps* ui;
    QMap<QString, double> cones_map, OC_map;
    QString track_type;
    int track_length, DNF_flag;
    double total_time, total_laps, avg_speed, avg_time, total_time_with_penalties;
};
#endif // RUN_LAPS_HPP
