#ifndef PICASSO_H
#define PICASSO_H

// Qt Classes
#include <QDockWidget>
#include <QList>
#include <QMessageBox>
#include <QToolBar>
#include <QtWidgets>

// Widgets
#include "AMS.hpp"
#include "ASWidget/ASWidget.hpp"
#include "BMS.hpp"
#include "COM_LogGen.hpp"
#include "ConfigWindow/configwindow.h"
#include "Console.hpp"
#include "DevStatus.hpp"
#include "ETAS/etas.hpp"
#include "General.hpp"
#include "GeneralWidget/GeneralWidget.hpp"
#include "General_DriverInputs.hpp"
#include "Inverters.hpp"
#include "Isabelle.hpp"
#include "Main_Toolbar.hpp"
#include "Sensors.hpp"
#include "UiWindow.hpp"
#include "WindowMain.hpp"
#include "ccu.hpp"
#include "lap_time.hpp"
#include "timeline/timeline.h"
#include "versions.hpp"

#include "COM_Interface.hpp"
#include "COM_Manager.hpp"
#include "Exceptions/helper.hpp"
#include "Factory.hpp"
#include <QMessageBox>
//#include "SQL_CANID.hpp"
//#include "SQL_CANID_Select.hpp"

class Picasso : public QObject {
    Q_OBJECT

public:
    Picasso(MainToolbar*, MainWindow*, Comsguy*);
    void deleteLogger();
    MainToolbar* MainToolBar;
    void checkDockWidgets();
    ~Picasso();

public slots:
    void launchWidget(QString type, bool detached = true);

signals:
    void widgetWillClose();

private:
    MainWindow* mainwindow;
    Factory<UiWindow> factory;
    Helper* log;
    ConfigManager* config_manager;
    QTimer* dock_checker;

private slots:
    void update(int);
};
#endif
