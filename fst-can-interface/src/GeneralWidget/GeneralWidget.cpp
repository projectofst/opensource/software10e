#include "GeneralWidget.hpp"
#include "ui_GeneralWidget.h"

InterfaceBridge::InterfaceBridge()
{

    this->split_state = false;
    this->time_out = true;
    this->timing = new QTimer(this);
    connect(this->timing, &QTimer::timeout, this, &InterfaceBridge::checkTimeout);
    this->timing->start(1000);
    resetValues();
}

void InterfaceBridge::checkTimeout()
{
    this->watchdog++;
    if (this->watchdog > 1) {
        resetValues();
        this->time_out = true;
    }
}

bool InterfaceBridge::isTimedOut()
{
    return this->time_out;
}

double InterfaceBridge::getRange(int type)
{
    return this->ranges[type];
}

double InterfaceBridge::getValue(int motor_number)
{

    return values[motor_number];
}

QString InterfaceBridge::getSrc()
{
    return this->src;
}

bool InterfaceBridge::getSplitState()
{
    return this->split_state;
}

void InterfaceBridge::resetValues()
{
    for (int i = 0; i < 4; i++) {
        values[i] = 0;
    }
}

void InterfaceBridge::setValue(int motor_number, double value)
{
    values[motor_number] = value;
    this->time_out = false;
    this->watchdog = 0;
}

void InterfaceBridge::setRanges(int min, int max)
{
    ranges[0] = min;
    ranges[1] = max;
}

void InterfaceBridge::setSource(QString src)
{
    this->src = src;
    resetValues();
}

void InterfaceBridge::setSplitState(bool state)
{
    this->split_state = state;
}

InterfaceBridge::~InterfaceBridge()
{
}

DynamicUnit::DynamicUnit(QWidget* parent, FCPCom* fcp, Helper* log, ConfigManager* config_manager)
    : UiWindow(parent, fcp)
    , ui(new Ui::DynamicUnit)
{
    ui->setupUi(this);
    this->logger = log;
    ui_bridge = new InterfaceBridge();
    // Register context
    ui->quickWidget->rootContext()->setContextProperty("uiData", ui_bridge);
    ui->quickWidget->setSource(QUrl(QStringLiteral("qrc:/qmls/GeneralWidget.qml")));
    ui->quickWidget->show();
    this->fcp = fcp;
    this->setWindowTitle("GW");

    // WARNING: Custom signal name can't be an existing fcp signal name

    custom_signals["du_signals"].append("adc_front_left");
    custom_signals["du_signals"].append("adc_front_right");
    custom_signals["du_signals"].append("adc_rear_left");
    custom_signals["du_signals"].append("adc_rear_right");

    custom_signal_selected = false;
    registerSignals();

    this->config_manager = config_manager;

    loadState(config_manager->readConfig(this->objectName()));
}

void DynamicUnit::updateFCP(FCPCom* fcp)
{
    this->UiWindow::setFCP(fcp);
}

void DynamicUnit::registerSignals()
{
    ui->CB->addItem("--");

    // Custom signals

    foreach (auto custom_signal, custom_signals.keys()) {
        ui->CB->addItem(custom_signal);
    }

    try {
        foreach (auto signal, fcp->getSignals()) {

            if (signal->getMuxCount() == 4) {
                ui->CB->addItem(signal->getName());
            }
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        return;
    }
}

DynamicUnit::~DynamicUnit()
{
    qDebug() << "Closing bro";
    delete ui_bridge;
}

void DynamicUnit::on_CB_currentIndexChanged(int)
{
    QString signal_name;
    if (ui->CB->currentText() != "--") {
        currentSignal = ui->CB->currentText();
        this->setWindowTitle("GW - " + currentSignal);

        if (custom_signals.keys().contains(currentSignal)) {
            signal_name = custom_signals[currentSignal][0];
            custom_signal_selected = true;
        } else {
            signal_name = currentSignal;
            custom_signal_selected = false;
        }

        try {
            auto signal = fcp->getSignal(signal_name);
            ui_bridge->setRanges(signal->getMinValue(), signal->getMaxValue());
            ui->splitButton->setChecked(signal->getMinValue() < 0);
        } catch (JsonException& e) {
            logger->handle_exception(e.toString());
            return;
        }

    } else {
        this->setWindowTitle("Dynamic Unit - No signal selected");
        ui_bridge->setRanges(0, 100);
        ui_bridge->resetValues();
    }
    ui_bridge->setSource(ui->CB->currentText());
}

void DynamicUnit::on_resetButton_clicked()
{
    ui->CB->setCurrentIndex(0);
}

void DynamicUnit::process_CAN_message(CANmessage msg)
{
    QString signalName;
    try {
        auto signals_dict = fcp->decodeMessageMuxed(msg);
        for (int i = 0; i < 4; i++) {
            if (custom_signal_selected) {
                signalName = custom_signals[currentSignal][i];
            } else {
                signalName = currentSignal + QString::number(i);
            }
            if (signals_dict.second.keys().contains(signalName)) {
                ui_bridge->setValue(i, signals_dict.second[signalName]);
                if ((custom_signal_selected && (i + 1) % 2 == 0) || !custom_signal_selected) {
                    break;
                }
            }
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        return;
    }
}

void DynamicUnit::on_splitButton_toggled(bool checked)
{
    ui_bridge->setSplitState(checked);
}

void DynamicUnit::on_saveStateButton_clicked()
{
    QMap<QString, QVariant> my_settings;

    my_settings["signal"] = ui->CB->currentText();

    this->config_manager->registerConfig(this->objectName(), my_settings);
}

void DynamicUnit::loadState(QMap<QString, QVariant> config)
{
    if (config.contains("load_default_ui"))
        return;
    ui->CB->setCurrentText(config["signal"].toString());
    on_CB_currentIndexChanged(0);
}

void DynamicUnit::setPrivateValue(QVariant value)
{
    ui->CB->setCurrentText(value.toString());
    on_CB_currentIndexChanged(0);
}
