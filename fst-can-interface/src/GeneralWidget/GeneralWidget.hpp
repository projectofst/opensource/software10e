#ifndef DYNAMICUNIT_HPP
#define DYNAMICUNIT_HPP

#include "../UiWindow.hpp"
#include "Exceptions/helper.hpp"
#include <QCloseEvent>
#include <QQmlContext>
#include <QSettings>
#include <QTimer>
#include <QWidget>

#include "ConfigManager/configmanager.hpp"

namespace Ui {
class DynamicUnit;
}

class InterfaceBridge : public QObject {
    Q_OBJECT

public:
    explicit InterfaceBridge();
    Q_INVOKABLE double getRange(int type);
    Q_INVOKABLE double getValue(int motor_number);
    Q_INVOKABLE QString getSrc();
    Q_INVOKABLE bool getSplitState();
    Q_INVOKABLE bool isTimedOut();
    void setRanges(int min, int max);
    void setValue(int motor_number, double value);
    void setSource(QString src);
    void setSplitState(bool state);
    void resetValues();
    void checkTimeout();
    ~InterfaceBridge();

private:
    QMap<int, double> ranges;
    QMap<int, double> values;
    QString src;
    QTimer* timing;
    int watchdog;
    bool time_out;
    bool split_state;
};

class DynamicUnit : public UiWindow {
    Q_OBJECT

public:
    explicit DynamicUnit(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    void updateFCP(FCPCom* fcp) override;
    void process_CAN_message(CANmessage) override;
    void registerSignals();
    QString getTitle() override { return "Dynamic Unit"; };
    void loadState(QMap<QString, QVariant> config);
    void setPrivateValue(QVariant value) override;
    ~DynamicUnit();

private slots:
    void on_CB_currentIndexChanged(int);

    void on_resetButton_clicked();

    void on_splitButton_toggled(bool checked);

    void on_saveStateButton_clicked();

private:
    Ui::DynamicUnit* ui;
    InterfaceBridge* ui_bridge;
    FCPCom* fcp;
    QString currentSignal;
    Helper* logger;
    ConfigManager* config_manager;
    QMap<QString, QList<QString>> custom_signals;
    bool custom_signal_selected;
};

#endif // DYNAMICUNIT_HPP
