import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5




Rectangle {
    id: rectangle
    width: 670
    Layout.fillWidth: true
    Layout.fillHeight: true
    color: "#31363b"

    Image {
        id: image
        x: 130
        y: 8
        width: 400
        height: 400
        source: "../../ass/Backgrounds/car_top.png"
        sourceSize.height: 400
        sourceSize.width: 400
        scale: 1
        rotation: -90
        fillMode: Image.PreserveAspectFit
    }

    ProgressBar {
        id: motor0
        x: 130
        y: 125
        width: 150
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitHeight: 30
            implicitWidth: 200
        }
        rotation: -90
        padding: 2
        contentItem: Item {
            Rectangle {
                width: motor0.visualPosition * parent.width
                height: parent.height
                color: "#17a81a"
                radius: 0
                rotation: 0
            }
            implicitHeight: 4
            implicitWidth: 200
        }
        value: 0.5
    }

    Rectangle {
        id: box0
        x: 36
        y: 118
        width: 120
        height: 45
        Text {
            id: value0
            x: 0
            y: 6
            width: 120
            height: 37
            color: "#e4e3e3"
            text: qsTr("10000")
            font.pixelSize: 30
            horizontalAlignment: Text.AlignHCenter
            font.strikeout: false
            font.bold: true
            font.family: "Verdana"
            minimumPixelSize: 30
            font.italic: false
            font.underline: false
        }
        color: "#232629"
        radius: 4
    }

    ProgressBar {
        id: motor0_up
        x: 167
        y: 88
        width: 75
        height: 30
        rotation: -90
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        padding: 2
        contentItem: Item {
            implicitWidth: 200
            Rectangle {
                width: motor0_up.visualPosition * parent.width
                height: parent.height
                color: "#17a81a"
                radius: 0
                rotation: 0
            }
            implicitHeight: 4
        }
    }

    ProgressBar {
        id: motor0_down
        x: 167
        y: 163
        width: 75
        height: 30
        rotation: 90
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        padding: 2
        contentItem: Item {
            implicitWidth: 200
            Rectangle {
                width: motor0_down.visualPosition * parent.width
                height: parent.height
                color: "#dd2e00"
                radius: 0
                rotation: 0
            }
            implicitHeight: 4
        }
    }

    Timer {
        interval: 100; running: true; repeat: true
        onTriggered: function() {

            //Visibility
            var visibility = uiData.getSplitState()
            motor0.visible = !visibility
            motor1.visible = !visibility
            motor2.visible = !visibility
            motor3.visible = !visibility
            motor0_down.visible = visibility
            motor1_down.visible = visibility
            motor2_down.visible = visibility
            motor3_down.visible = visibility
            motor0_up.visible = visibility
            motor1_up.visible = visibility
            motor2_up.visible = visibility
            motor3_up.visible = visibility

            var motor0_value = uiData.getValue(0)
            var motor1_value = uiData.getValue(1)
            var motor2_value = uiData.getValue(2)
            var motor3_value = uiData.getValue(3)

            if(!visibility) {
                motor0.from = uiData.getRange(0)
                motor1.from = motor0.from
                motor2.from = motor0.from
                motor3.from = motor0.from
                motor0.to = uiData.getRange(1)
                motor1.to = motor0.to
                motor2.to = motor0.to
                motor3.to = motor0.to
                motor0.value = motor0_value
                motor1.value = motor1_value
                motor2.value = motor2_value
                motor3.value = motor3_value
            } else {

                motor0_up.from = 0
                motor1_up.from = 0
                motor2_up.from = 0
                motor3_up.from = 0

                motor0_down.from = 0
                motor1_down.from = 0
                motor2_down.from = 0
                motor3_down.from = 0

                motor0_up.to = uiData.getRange(1)
                motor1_up.to = motor0_up.to
                motor2_up.to = motor0_up.to
                motor3_up.to = motor0_up.to

                motor0_down.to = uiData.getRange(0) * -1
                motor1_down.to = motor0_down.to
                motor2_down.to = motor0_down.to
                motor3_down.to = motor0_down.to


                if (motor0_value >= 0) {
                    motor0_up.value = motor0_value
                    motor0_down.value = 0
                } else {
                    motor0_up.value = 0
                    motor0_down.value = motor0_value * -1
                }

                if (motor1_value >= 0) {
                    motor1_up.value = motor1_value
                    motor1_down.value = 0
                } else {
                    motor1_up.value = 0
                    motor1_down.value = motor1_value * -1
                }

                if (motor2_value >= 0) {
                    motor2_up.value = motor2_value
                    motor2_down.value = 0
                } else {
                    motor2_up.value = 0
                    motor2_down.value = motor2_value * -1
                }

                if (motor3_value >= 0) {
                    motor3_up.value = motor3_value
                    motor3_down.value = 0
                } else {
                    motor3_up.value = 0
                    motor3_down.value = motor3_value * -1
                }
            }

            if (uiData.isTimedOut()) {
                value0.text = "--"
                value1.text = "--"
                value2.text = "--"
                value3.text = "--"
            } else {
                value0.text = motor0_value
                value1.text = motor1_value
                value2.text = motor2_value
                value3.text = motor3_value
            }

            value4.text = uiData.getSrc()
        }
    }

    ProgressBar {
        id: motor1
        x: 385
        y: 126
        width: 150
        padding: 2
        contentItem: Item {
            Rectangle {
                width: motor1.visualPosition * parent.width
                height: parent.height
                color: "#17a81a"
                radius: 0
                rotation: 0
            }
            implicitWidth: 200
            implicitHeight: 4
        }
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        rotation: -90
    }

    ProgressBar {
        id: motor1_up
        x: 422
        y: 89
        width: 75
        height: 30
        padding: 2
        contentItem: Item {
            Rectangle {
                width: motor1_up.visualPosition * parent.width
                height: parent.height
                color: "#17a81a"
                radius: 0
                rotation: 0
            }
            implicitWidth: 200
            implicitHeight: 4
        }
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        rotation: -90
    }

    ProgressBar {
        id: motor1_down
        x: 422
        y: 164
        width: 75
        height: 30
        padding: 2
        contentItem: Item {
            Rectangle {
                width: motor1_down.visualPosition * parent.width
                height: parent.height
                color: "#dd2e00"
                radius: 0
                rotation: 0
            }
            implicitWidth: 200
            implicitHeight: 4
        }
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        rotation: 90
    }

    ProgressBar {
        id: motor2
        x: 130
        y: 303
        width: 150
        padding: 2
        contentItem: Item {
            Rectangle {
                width: motor2.visualPosition * parent.width
                height: parent.height
                color: "#17a81a"
                radius: 0
                rotation: 0
            }
            implicitWidth: 200
            implicitHeight: 4
        }
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        rotation: -90
    }

    ProgressBar {
        id: motor2_up
        x: 167
        y: 266
        width: 75
        height: 30
        padding: 2
        contentItem: Item {
            Rectangle {
                width: motor2_up.visualPosition * parent.width
                height: parent.height
                color: "#17a81a"
                radius: 0
                rotation: 0
            }
            implicitWidth: 200
            implicitHeight: 4
        }
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        rotation: -90
    }

    ProgressBar {
        id: motor2_down
        x: 167
        y: 341
        width: 75
        height: 30
        padding: 2
        contentItem: Item {
            Rectangle {
                width: motor2_down.visualPosition * parent.width
                height: parent.height
                color: "#dd2e00"
                radius: 0
                rotation: 0
            }
            implicitWidth: 200
            implicitHeight: 4
        }
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        rotation: 90
    }

    ProgressBar {
        id: motor3
        x: 386
        y: 303
        width: 150
        padding: 2
        contentItem: Item {
            Rectangle {
                width: motor3.visualPosition * parent.width
                height: parent.height
                color: "#17a81a"
                radius: 0
                rotation: 0
            }
            implicitWidth: 200
            implicitHeight: 4
        }
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        rotation: -90
    }

    ProgressBar {
        id: motor3_up
        x: 423
        y: 266
        width: 75
        height: 30
        padding: 2
        contentItem: Item {
            Rectangle {
                width: motor3_up.visualPosition * parent.width
                height: parent.height
                color: "#17a81a"
                radius: 0
                rotation: 0
            }
            implicitWidth: 200
            implicitHeight: 4
        }
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        rotation: -90
    }

    ProgressBar {
        id: motor3_down
        x: 423
        y: 341
        width: 75
        height: 30
        padding: 2
        contentItem: Item {
            Rectangle {
                width: motor3_down.visualPosition * parent.width
                height: parent.height
                color: "#dd2e00"
                radius: 0
                rotation: 0
            }
            implicitWidth: 200
            implicitHeight: 4
        }
        value: 0.5
        background: Rectangle {
            color: "#595959"
            radius: 0
            implicitWidth: 200
            implicitHeight: 30
        }
        rotation: 90
    }

    Rectangle {
        id: box1
        x: 505
        y: 118
        width: 120
        height: 45
        color: "#232629"
        radius: 4
        Text {
            id: value1
            x: 0
            y: 6
            width: 120
            height: 37
            color: "#e4e3e3"
            text: qsTr("10000")
            font.pixelSize: 30
            horizontalAlignment: Text.AlignHCenter
            font.family: "Verdana"
            minimumPixelSize: 30
            font.italic: false
            font.bold: true
            font.underline: false
            font.strikeout: false
        }
    }

    Rectangle {
        id: box2
        x: 36
        y: 296
        width: 120
        height: 45
        color: "#232629"
        radius: 4
        Text {
            id: value2
            x: 0
            y: 6
            width: 120
            height: 37
            color: "#e4e3e3"
            text: qsTr("10000")
            font.pixelSize: 30
            horizontalAlignment: Text.AlignHCenter
            font.family: "Verdana"
            minimumPixelSize: 30
            font.italic: false
            font.bold: true
            font.underline: false
            font.strikeout: false
        }
    }

    Rectangle {
        id: box3
        x: 505
        y: 296
        width: 120
        height: 45
        color: "#232629"
        radius: 4
        Text {
            id: value3
            x: 0
            y: 6
            width: 120
            height: 37
            color: "#e4e3e3"
            text: qsTr("10000")
            font.pixelSize: 30
            horizontalAlignment: Text.AlignHCenter
            font.family: "Verdana"
            minimumPixelSize: 30
            font.italic: false
            font.bold: true
            font.underline: false
            font.strikeout: false
        }
    }

    Rectangle {
        id: box4
        x: 130
        y: 414
        width: 420
        height: 45
        color: "#232629"
        radius: 4
        Text {
            id: value4
            x: 0
            y: 4
            width: 420
            height: 37
            color: "#e4e3e3"
            text: qsTr("src")
            font.pixelSize: 30
            horizontalAlignment: Text.AlignHCenter
            font.family: "Verdana"
            minimumPixelSize: 30
            font.italic: false
            font.bold: true
            font.underline: false
            font.strikeout: false
        }
    }

}








