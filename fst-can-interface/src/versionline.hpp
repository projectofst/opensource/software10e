#ifndef VERSIONLINE_HPP
#define VERSIONLINE_HPP

#include <QWidget>

#include "UiWindow.hpp"

namespace Ui {
class VersionLine;
}

class VersionLine : public UiWindow {
    Q_OBJECT

public:
    explicit VersionLine(QWidget* parent = nullptr);
    ~VersionLine();

private:
    Ui::VersionLine* ui;
};

#endif // VERSIONLINE_HPP
