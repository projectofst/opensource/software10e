#include "PlotUi.hpp"
#include "ui_PlotUi.h"
#include <QDesktopServices>
#include <QLayout>
#include <QListWidgetItem>
#include <QSharedPointer>
#include <QtCharts/QChartView>
#include <QtCharts>
#include <qdebug.h>

using namespace QtCharts;

PlotUI::PlotUI(QWidget* parent, FCPCom* fcp)
    : UiWindow(parent, fcp)
    , ui(new Ui::PlotUI)
{
    ui->setupUi(this);
    // Setup Sidebars
    setup_plot_active = false;
    setupRerun = false;
    ui->leftSidebar->hide();
    ui->plotProperties->hide();
    ui->plotSetup->hide();
    ui->exportTab->hide();
    ui->Console->hide();
    ui->advancedCANtab->hide();
    ui->frozenNotification->hide();
    ui->exportProgressBar->hide();
    ui->mathTab->hide();
    ui->rightSidebarButton->setEnabled(false);
    ui->scrollCheckbox->setEnabled(false);
    ui->exportButton->setEnabled(false);
    ui->mathButton->setEnabled(false);
    ui->graphicsView->setRubberBand(QChartView::NoRubberBand);
    enableZoomTools(false);
    currentSelectedPlot = 0;

    // Setup Line Series Colors

    colorSetupFinished = false;

    // This colors are from http://ksrowell.com/blog-visualizing-data/2012/02/02/optimal-colors-for-graphs/
    plotColors["matlab blue"] = QColor(57, 106, 177);
    plotColors["matlab orange"] = QColor(218, 124, 48);
    plotColors["matlab green"] = QColor(62, 150, 81);
    plotColors["matlab red"] = QColor(204, 37, 41);
    plotColors["matlab grey"] = QColor(83, 81, 41);
    plotColors["matlab purple"] = QColor(107, 76, 154);
    plotColors["matlab magenta"] = QColor(146, 36, 40);
    plotColors["matlab yellow"] = QColor(148, 139, 61);

    matlabColorOrder = { "blue", "orange", "green", "red", "grey", "purple", "magenta", "yellow" };

    // This colors were pulled from https://www.materialpalette.com/ with a Python3 webscraping script
    plotColors["red"] = QColor(244, 67, 54);
    plotColors["pink"] = QColor(233, 30, 99);
    plotColors["deep purple"] = QColor(103, 58, 183);
    plotColors["indigo"] = QColor(63, 81, 181);
    plotColors["blue"] = QColor(33, 150, 243);
    plotColors["light blue"] = QColor(3, 169, 244);
    plotColors["cyan"] = QColor(0, 188, 212);
    plotColors["teal"] = QColor(0, 150, 136);
    plotColors["green"] = QColor(76, 175, 80);
    plotColors["light green"] = QColor(139, 195, 74);
    plotColors["lime"] = QColor(205, 220, 57);
    plotColors["yellow"] = QColor(255, 235, 59);
    plotColors["amber"] = QColor(255, 193, 7);
    plotColors["orange"] = QColor(255, 152, 0);
    plotColors["deep orange"] = QColor(255, 87, 34);
    plotColors["brown"] = QColor(121, 85, 72);
    plotColors["grey"] = QColor(158, 158, 158);
    plotColors["blue grey"] = QColor(96, 125, 139);

    QMapIterator<QString, QColor> i(plotColors);
    while (i.hasNext()) {
        i.next();
        ui->plotColorInput->addItem(i.key());
        ui->changeColorInput->addItem(i.key());
    }

    currentColorIndex = 0;

    // Setup Chart Themes

    QMap<QString, QColor> theme;
    theme["chartBackground"] = QColor("#FFFFFF");
    theme["chartAxis"] = QColor("#D6D6D6");
    theme["chartGrid"] = QColor("#E2E2E2");
    theme["chartLabel"] = QColor("#4A4A50");
    chartThemes["Light"] = theme;
    theme["chartBackground"] = QColor("#181b1d");
    theme["chartAxis"] = QColor("#404142");
    theme["chartGrid"] = QColor("#24282c");
    theme["chartLabel"] = QColor("#D0D0D0");
    chartThemes["Dark"] = theme;

    QMapIterator<QString, QMap<QString, QColor>> themeIterator(chartThemes);
    while (themeIterator.hasNext()) {
        themeIterator.next();
        ui->themesPicker->addItem(themeIterator.key());
    }

    colorSetupFinished = true;

    // Setup Export
    ui->exportTypeInput->addItem("Plot List");
    ui->exportTypeInput->addItem("Plot Points");
    ui->exportTypeInput->addItem("Screenshot");

    // Setup Math

    simpleMathDB["Add"] = "x + ";
    simpleMathDB["Subtract"] = "x - ";
    simpleMathDB["Multiply"] = "x * ";
    simpleMathDB["Divide"] = "x / ";
    simpleMathDB["Custom Expression"] = "custom";

    ui->opTypeInput->addItem("Single Plot");
    // ui->opTypeInput->addItem("Two Plots");

    QMapIterator<QString, QString> simpleMathIterator(simpleMathDB);
    while (simpleMathIterator.hasNext()) {
        simpleMathIterator.next();
        ui->singleOpInput->addItem(simpleMathIterator.key());
    }

    // Connect UI
    connect(ui->devIdInput, &QComboBox::currentTextChanged, this, &PlotUI::refreshDEVID);
    connect(ui->msgIdInput, &QComboBox::currentTextChanged, this, &PlotUI::refreshMSGID);
    connect(ui->lengthInput, QOverload<int>::of(&QSpinBox::valueChanged), this, &PlotUI::setLengthStart);
    connect(ui->startInput, QOverload<int>::of(&QSpinBox::valueChanged), this, &PlotUI::setLengthStart);
    connect(ui->signalInput, &QComboBox::currentTextChanged, this, &PlotUI::setSignalParameters);
    connect(ui->createPlotButton, &QPushButton::clicked, this, &PlotUI::addNewPlot);
    connect(ui->advancedCANbtn, &QToolButton::clicked, this, &PlotUI::toggleAdvancedCANSettings);
    connect(ui->sampleRateInput, QOverload<int>::of(&QSpinBox::valueChanged), this, &PlotUI::setSampleRate);
    connect(ui->exportNameInput, &QLineEdit::textChanged, this, &PlotUI::checkExportFields);

    // Setup Sidebar Widget Color
    ui->plotColor->setFixedHeight(4);
    ui->plotColor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    ui->plotColor->setStyleSheet(QString("background-color: #0800f5;"));

    // Setup Sidebar Plot Setup Widget Color
    ui->plotColorSetup->setFixedHeight(4);
    ui->plotColorSetup->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    ui->plotColorSetup->setStyleSheet(QString("background-color: #0800f5;"));

    QPalette pal = ui->frozenNotification->palette();
    pal.setColor(QPalette::Background, "#404142");
    ui->frozenNotification->setAutoFillBackground(true);
    ui->frozenNotification->setPalette(pal);

    // Setup Taskbar
    QFont font;
    ui->OpenConsole->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->OpenConsole->setIcon(QIcon(":/CustomIcons/CustomIcons/terminal.png"));
    ui->OpenConsole->setText("Open console");
    font = ui->OpenConsole->font();
    font.setPointSize(10);
    ui->OpenConsole->setFont(font);

    ui->exportButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->exportButton->setIcon(QIcon(":/Assets/CustomIcons/upload.png"));
    ui->exportButton->setText("Export");
    ui->exportButton->setIconSize(QSize(30, 30));
    font = ui->exportButton->font();
    font.setPointSize(10);
    ui->exportButton->setFont(font);

    ui->importButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->importButton->setIcon(QIcon(":/Assets/CustomIcons/download.png"));
    ui->importButton->setText("Import");
    ui->importButton->setIconSize(QSize(30, 30));
    font = ui->importButton->font();
    font.setPointSize(10);
    ui->importButton->setFont(font);

    ui->addPlotButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->addPlotButton->setIcon(QIcon(":/CustomIcons/CustomIcons/add.png"));
    ui->addPlotButton->setIconSize(QSize(30, 30));
    ui->addPlotButton->setText("Add Plot");
    font = ui->addPlotButton->font();
    font.setPointSize(10);
    ui->addPlotButton->setFont(font);

    ui->zoomButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->zoomButton->setIcon(QIcon(":/CustomIcons/CustomIcons/zoom-selection.png"));
    ui->zoomButton->setIconSize(QSize(30, 30));
    ui->zoomButton->setText("Zoom by selection");
    font = ui->zoomButton->font();
    font.setPointSize(10);
    ui->zoomButton->setFont(font);

    ui->fixLimits->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->fixLimits->setIcon(QIcon(":/CustomIcons/CustomIcons/fix-point.png"));
    ui->fixLimits->setIconSize(QSize(35, 35));
    ui->fixLimits->setText("Fix limits");
    font = ui->fixLimits->font();
    font.setPointSize(10);
    ui->fixLimits->setFont(font);

    ui->mathButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->mathButton->setIcon(QIcon(":/CustomIcons/CustomIcons/keys.png"));
    ui->mathButton->setIconSize(QSize(35, 35));
    ui->mathButton->setText("Plots Math");
    font = ui->mathButton->font();
    font.setPointSize(10);
    ui->mathButton->setFont(font);

    ui->reportButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->reportButton->setIcon(QIcon(":/CustomIcons/CustomIcons/alert.png"));
    ui->reportButton->setIconSize(QSize(35, 35));
    ui->reportButton->setText("Report Problem");
    font = ui->reportButton->font();
    font.setPointSize(10);
    ui->reportButton->setFont(font);

    // Create chart, add data, hide legend, and add axis
    plot = new QChart();

    // Create axis
    axisX = new QValueAxis;
    axisY = new QValueAxis;
    axisY->setRange(0, 10);
    setSampleRate();

    // Used to display the chart
    ui->graphicsView->setChart(plot);
    // ui->graphicsView->setRenderHint(QPainter::Antialiasing);

    // Set Chart Default Theme
    ui->themesPicker->setCurrentIndex(0);
    on_themesPicker_currentTextChanged("Dark");

    // Enable scroll mode by default
    on_scrollCheckbox_stateChanged(2);

    // Start Refresh Timer
    fixedLimits = false;
    freezeState = false;
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, QOverload<>::of(&PlotUI::refreshPlots));
    timer->start(50);

    // Add signals
    SignalsAndDevicesAndMessages = _fcp->getSignalsAndDevicesAndMessages();

    ui->Console->insertPlainText("PlotUI: Setup Complete! \n");
    ui->Console->insertPlainText("Ready \n");
}

void PlotUI::parseSignals(FCPCom* fcp)
{
    foreach (auto signal, fcp->getSignals()) {
        ui->signalInput->addItem(signal->getName());
    }
}

PlotUI::~PlotUI()
{
    timer->stop();
    delete ui->plotList;
    plotObjects.erase(plotObjects.begin(), plotObjects.end());
    delete axisY;
    delete axisX;
    delete plot;
    delete ui;
}

void PlotUI::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}

void PlotUI::process_CAN_message(CANmessage msg)
{
    double maxY = 0, maxX = 0, minY = 0;
    static double minX = 0;
    double plotMinY, plotMaxY;
    if (!(0 < backupRangeX && msg.timestamp == 0)) {
        foreach (auto plotObj, plotObjects) {
            plotObj->receiveCANMsg(msg);
            plotMinY = plotObj->getSubRanges()["min"];
            plotMaxY = plotObj->getSubRanges()["max"];
            if (plotObj->getMaxX() > maxX) {
                maxX = plotObj->getMaxX();
            }
            if (plotMaxY > maxY) {
                maxY = plotMaxY;
            }
            if (plotMinY < minY) {
                minY = plotMinY;
            }
        }
    }

    if (!freezeState) {
        if (fixedLimits && fixedRangesSet && !scrollEnabled) {
            axisX->setRange(fixedRangeMinX, maxX);
            axisY->setRange(fixedRangeMinY, fixedRangeMaxY);
        } else {
            timestampQueue.enqueue(msg.timestamp);
            if (timestampQueue.length() >= sampleRate + 1) {
                while (timestampQueue.length() > sampleRate + 1) { // Resync
                    minX = timestampQueue.dequeue();
                }
            }
            axisX->setRange(minX, maxX);
            if (fixedLimits && fixedRangesSet && scrollEnabled) {
                axisY->setRange(fixedRangeMinY, fixedRangeMaxY);
                // qDebug() << "Forced set to: " << fixedRangeMinY;
            } else {
                // qDebug() << "Shoud set to: " << minY;
                axisY->setRange(minY, maxY);
            }
        }
        backupRangeX = maxX;
        backupRangeMaxY = maxY;
        backupRangeMinY = minY;
    }
}

void PlotUI::on_freezeBtn_clicked()
{
    if (!freezeState) {
        ui->frozenNotification->show();
        ui->freezeBtn->setText("Unfreeze");
        freezeState = true;
        timer->stop();
        ui->fixLimits->setEnabled(false);
        enableZoomTools(true);
        ui->scrollCheckbox->setEnabled(false);
    } else {
        ui->frozenNotification->hide();
        ui->freezeBtn->setText("Freeze");
        freezeState = false;
        timer->start(50);
        ui->fixLimits->setEnabled(true);
        if (fixedLimits) {
            ui->scrollCheckbox->setEnabled(true);
            if (scrollEnabled) {
                ui->gotoMinXInput->setEnabled(false);
            }
        }
        if (fixedLimits) {
            ui->zoomButton->setEnabled(false);
            ui->gotoMaxXInput->setEnabled(false);
        } else {
            enableZoomTools(false);
        }
    }
}

void PlotUI::refreshPlots()
{
    if (!plotObjects.isEmpty()) {
        foreach (auto plot, plotObjects) {
            plot->Render();
            refreshWidget(plot->getPlotCfg()->getPlotId(), plot->getRanges());
            if (ui->plotProperties->isVisible() && plot->getPlotCfg()->getPlotId() == currentSelectedPlot) {
                refreshPlotInfo(plot->getRanges());
            }
        }
    }
}

void PlotUI::refreshWidget(int Id, QList<double> values)
{
    PlotWidget* widget;
    for (int i = 0; i < ui->plotList->count(); i++) {
        widget = (PlotWidget*)(ui->plotList->itemWidget(ui->plotList->item(Id)));
        if (widget) {
            if (widget->getPlotId() == Id) {
                widget->refresh(values);
            }
        }
    }
}

// Show/Hide Plot View Left Sidebar Widget
void PlotUI::on_leftSidebarButton_clicked()
{
    if (ui->leftSidebar->isVisible()) {
        ui->leftSidebarButton->setArrowType(Qt::RightArrow);
        ui->leftSidebar->hide();
    } else {
        ui->leftSidebarButton->setArrowType(Qt::LeftArrow);
        ui->leftSidebar->show();
    }
}

// Show/Hide Plot View Right Sidebar Widget
void PlotUI::on_rightSidebarButton_clicked()
{
    if (ui->plotProperties->isVisible() || ui->plotSetup->isVisible() || ui->exportTab->isVisible() || ui->mathTab->isVisible()) {
        if (!setupRerun) {
            ui->rightSidebarButton->setArrowType(Qt::LeftArrow);
        }
        if (setup_plot_active) {
            if (!setupRerun) {
                ui->plotSetup->hide();
            }
        } else {
            ui->plotProperties->hide();
            ui->exportTab->hide();
            ui->mathTab->hide();
        }
    } else {
        ui->rightSidebarButton->setArrowType(Qt::RightArrow);
        if (setup_plot_active) {
            ui->plotSetup->show();
        } else {
            ui->plotProperties->show();
        }
    }
}

// Show/Hide Advanced CAN Settings on Plot Setup
void PlotUI::toggleAdvancedCANSettings()
{
    if (ui->advancedCANtab->isVisible()) {
        ui->advancedCANbtn->setText("Show Advanced");
        ui->advancedCANtab->hide();
    } else {
        ui->advancedCANbtn->setText("Hide Advanced");
        ui->advancedCANtab->show();
    }
}

// Initialize new plot setup
void PlotUI::on_addPlotButton_clicked()
{
    ui->addPlotButton->setEnabled(false);
    if (!ui->rightSidebarButton->isEnabled()) {
        ui->rightSidebarButton->setEnabled(true);
    }
    if (ui->plotProperties->isVisible()) { // If plot properties sidebar is visible, hide it to show the plot setup sidebar
        on_rightSidebarButton_clicked();
    }
    setup_plot_active = true;
    length = 0;
    start = 0;
    plotNameSize = 0;
    color = plotColors[QString("matlab %1").arg(matlabColorOrder[currentColorIndex])];
    ui->plotColorInput->setCurrentIndex(ui->plotColorInput->findText(QString("matlab %1").arg(matlabColorOrder[currentColorIndex])));
    ui->plotColorSetup->setStyleSheet(QString("background-color: %1;").arg(color.name()));
    parseSignals(_fcp);
    ui->MUX_box->setEnabled(false); // Hide Mux Option
    checkPlotCfg(devId, messageId, start, length, plotNameSize);
    on_rightSidebarButton_clicked();
    setupRerun = false;
}

// Cancel Plot Setup
void PlotUI::on_cancelPlotSetupButton_clicked()
{
    ui->plotSetup->hide();
    ui->rightSidebarButton->setArrowType(Qt::LeftArrow);
    setup_plot_active = false;
    ui->addPlotButton->setEnabled(true);
    ui->rightSidebarButton->setEnabled(false);
    endPlotSetup();
}

// End Plot Setup
void PlotUI::endPlotSetup()
{
    parseSignals(_fcp);
    ui->signalInput->clear();
    ui->msgIdInput->setCurrentIndex(0);
    ui->lengthInput->setValue(0);
    ui->offsetInput->setValue(0);
    ui->startInput->setValue(0);
    ui->devIdInput->setCurrentIndex(0);
}

// Check Repeated Name
QString PlotUI::checkRepeatedName(QString name, int n)
{
    QString newName;
    if (n != 0) {
        newName.append(name);
        newName.append(QString(" #%1").arg(QString::number(n)));
    } else {
        newName = name;
    }
    if (getPlotObjByName(newName) != nullptr) {
        qDebug() << "OOPS: " << newName << " is not available";
        return checkRepeatedName(name, n + 1);
    }
    return newName;
}

// Add Plot
void PlotUI::addNewPlot()
{
    QList<int> parentsId;
    if (muxSignal != "null") {
        plotName = plotName + "->" + QString::number(ui->muxInput->value());
    }
    std::shared_ptr<PlotCfg> plotSetupCfg = std::make_shared<PlotCfg>(
        devId,
        messageId,
        ui->offsetInput->value(),
        length, start, ui->scaleInput->value(),
        checkRepeatedName(plotName, 0),
        plotObjects.count(),
        color,
        parentsId,
        muxSignal,
        ui->muxInput->value());
    std::shared_ptr<PlotBaseObj> plotObj = std::make_shared<PlotParentObj>(plotSetupCfg, plot, axisX, axisY, this->_fcp);
    printToConsole(QString("Registering Plot with ID: %1 \n").arg(QString::number(plotObj->getPlotCfg()->getPlotId())));
    plotObjects.append(plotObj);
    addPlotToList(plotObj);
    printToConsole(plotObj->helloMsg());
    if (!ui->leftSidebar->isVisible()) {
        on_leftSidebarButton_clicked();
    }
    setupRerun = true;
    ui->exportButton->setEnabled(true);
    ui->mathButton->setEnabled(true);
    if (currentColorIndex == matlabColorOrder.length() - 1) {
        currentColorIndex = 0;
    } else {
        currentColorIndex++;
    }
    // DEBUG
    // std::shared_ptr<PlotChildObj> child, child2;
    // child = std::make_shared<PlotChildObj>(plotObj->getPlotCfg(), plot, axisX, axisY, "x+10000");
    // child2 = std::make_shared<PlotChildObj>(plotObj->getPlotCfg(), plot, axisX, axisY, "x+100");
    // child->addChild(child2);
    // plotObj->addChild(child);
    // child2->updateColor(plotColors["orange"]);
    // child->updateColor(plotColors["red"]);

    on_addPlotButton_clicked();
}

void PlotUI::addImportedPlot(std::shared_ptr<PlotBaseObj> importedPlot)
{
    printToConsole(QString("IMPORT: Registering Plot with ID: %1 \n").arg(QString::number(importedPlot->getPlotCfg()->getPlotId())));
    plotObjects.append(importedPlot);
    addPlotToList(importedPlot);
    printToConsole(importedPlot->helloMsg());
}

// Create Child Plot
std::shared_ptr<PlotBaseObj> PlotUI::createChildPlot(std::shared_ptr<PlotBaseObj> parentPlot, QString plotName, QString plotOp)
{
    QList<int> parentsId;
    parentsId.append(parentPlot->getPlotCfg()->getPlotId());
    std::shared_ptr<PlotCfg> childCfg = std::make_shared<PlotCfg>(
        parentPlot->getPlotCfg()->getDevId(),
        parentPlot->getPlotCfg()->getMessageId(),
        parentPlot->getPlotCfg()->getOffset(),
        parentPlot->getPlotCfg()->getLength(),
        parentPlot->getPlotCfg()->getStart(),
        parentPlot->getPlotCfg()->getScale(),
        plotName,
        plotObjects.count(),
        plotColors[QString("matlab %1").arg(matlabColorOrder[currentColorIndex])],
        parentsId,
        parentPlot->getPlotCfg()->getPlotMux(),
        parentPlot->getPlotCfg()->getPlotMuxValue());
    std::shared_ptr<PlotChildObj> child = std::make_shared<PlotChildObj>(childCfg, plot, axisX, axisY, this->_fcp, plotOp);
    if (currentColorIndex == matlabColorOrder.length() - 1) {
        currentColorIndex = 0;
    } else {
        currentColorIndex++;
    }
    return child;
}

// Get plotObj by name

std::shared_ptr<PlotBaseObj> PlotUI::getPlotObjByName(QString name)
{
    std::shared_ptr<PlotBaseObj> plot = nullptr;
    foreach (auto plotObj, plotObjects) {
        if (plotObj->getPlotCfg()->getName() == name) {
            plot = plotObj;
        }
    }
    return plot;
}

// Get plotObj with Id
std::shared_ptr<PlotBaseObj> PlotUI::getPlotObj(int Id)
{
    std::shared_ptr<PlotBaseObj> plot = nullptr;
    foreach (auto plotObj, plotObjects) {
        if (plotObj->getPlotCfg()->getPlotId() == Id) {
            plot = plotObj;
        }
    }
    return plot;
}

// Get plot index in array with id
int PlotUI::getPlotArrayId(int Id)
{
    int arrayId = 0;
    foreach (auto plotObj, plotObjects) {
        if (plotObj->getPlotCfg()->getPlotId() == Id) {
            return arrayId;
        }
        arrayId++;
    }
    return -1;
}

// Create List Item
void PlotUI::addPlotToList(std::shared_ptr<PlotBaseObj> plot)
{
    QListWidgetItem* listItem;
    QSize itemSize;
    printToConsole(QString("List: Adding plot with Id: %1 \n").arg(QString::number(plot->getPlotCfg()->getPlotId())));
    PlotWidget* widget = new PlotWidget(plot);
    listItem = new QListWidgetItem();
    itemSize.setHeight(widget->sizeHint().rheight());
    itemSize.setWidth(ui->plotList->sizeHint().rwidth());
    listItem->setSizeHint(itemSize);
    ui->plotList->addItem(listItem);
    ui->plotList->setItemWidget(listItem, widget);
}

// Open console
void PlotUI::on_OpenConsole_clicked()
{
    if (ui->Console->isVisible()) {
        ui->OpenConsole->setText("Open console");
        ui->Console->hide();
    } else {
        ui->OpenConsole->setText("Close console");
        ui->Console->show();
    }
}

// Console Output
void PlotUI::printToConsole(QString msg)
{
    ui->Console->insertPlainText(msg);
    ui->Console->verticalScrollBar()->setValue(ui->Console->verticalScrollBar()->maximum()); // Maintain focus on last line
}

// Set Msg ID and re-run verification
void PlotUI::refreshMSGID()
{
    messageId = ui->msgIdInput->currentText().toInt();
    checkPlotCfg(devId, messageId, start, length, plotNameSize);
}

// Set Dev ID and re-run verification
void PlotUI::refreshDEVID()
{
    devId = ui->devIdInput->currentText().toInt();
    checkPlotCfg(devId, messageId, start, length, plotNameSize);
}

void PlotUI::setSignalParameters(QString signalName)
{
    if (SignalsAndDevicesAndMessages.contains(signalName)) {
        std::tuple<CanSignal*, Device*, Message*> signalandthings = SignalsAndDevicesAndMessages.value(signalName);
        if (std::get<0>(signalandthings)->getMuxCount() != 1) {
            ui->muxInput->setMaximum(std::get<0>(signalandthings)->getMuxCount() - 1);
            ui->muxLabel->show();
            ui->muxInput->show();
            muxSignal = std::get<0>(signalandthings)->getMux();
        } else {
            ui->muxLabel->hide();
            ui->muxInput->hide();
            muxSignal = "null";
        }
        ui->devIdInput->setCurrentText(QString::number(std::get<1>(signalandthings)->getId()));
        ui->msgIdInput->setCurrentText(QString::number(std::get<2>(signalandthings)->getId()));
        ui->startInput->setValue(std::get<0>(signalandthings)->getStart());
        ui->offsetInput->setValue(std::get<0>(signalandthings)->getOffset());
        ui->lengthInput->setValue(std::get<0>(signalandthings)->getLength());
        ui->scaleInput->setValue(std::get<0>(signalandthings)->getScale());
        plotName = ui->signalInput->currentText();
        plotNameSize = plotName.length();
        checkPlotCfg(devId, messageId, start, length, plotNameSize);
    }
}

// Set length and start on lengthInput or startInput change
void PlotUI::setLengthStart(int)
{
    // Get Length and Offset Values
    length = ui->lengthInput->value();
    start = ui->startInput->value();
    checkPlotCfg(devId, messageId, start, length, plotNameSize);
}

// Check empty fields
bool PlotUI::emptyFields()
{
    bool response;
    response = false;
    if (ui->devIdInput->currentText().size() == 0) {
        ui->devIdLabel->setStyleSheet("QLabel { color : #ff3333; }");
        response = true;
    } else {
        ui->devIdLabel->setStyleSheet("QLabel { color : #d0d0d0; }");
    }
    if (ui->msgIdInput->currentText().size() == 0) {
        ui->msgIdLabel->setStyleSheet("QLabel { color : #ff3333; }");
        response = true;
    } else {
        ui->msgIdLabel->setStyleSheet("QLabel { color : #d0d0d0; }");
    }
    return response;
}

// Check Message ID and Dev ID data
bool PlotUI::checkSID(uint16_t dev_id, uint16_t msg_id)
{
    uint16_t sid = dev_id + (msg_id << 5);
    if (sid < (1 << 11)) {
        ui->msgIdLabel->setStyleSheet("QLabel { color : #d0d0d0; }");
        if (dev_id < 32) {
            ui->devIdLabel->setStyleSheet("QLabel { color : #d0d0d0; }");
            return true;
        } else {
            ui->devIdLabel->setStyleSheet("QLabel { color : #ff3333; }");
            return false;
        }
    }
    ui->devIdLabel->setStyleSheet("QLabel { color : #ff3333; }");
    ui->msgIdLabel->setStyleSheet("QLabel { color : #ff3333; }");
    return false;
}

// Check Message Length and Offset
bool PlotUI::checkLenghtOffset(int len, int ofst)
{
    if (len == 0) {
        ui->lengthLabel->setStyleSheet("QLabel { color : #ff3333; }");
        return false;
    } else if (len + ofst > 64) {
        ui->lengthLabel->setStyleSheet("QLabel { color : #ff3333; }");
        ui->startLabel->setStyleSheet("QLabel { color : #ff3333; }");
        return false;
    } else {
        ui->lengthLabel->setStyleSheet("QLabel { color : #d0d0d0; }");
        ui->startLabel->setStyleSheet("QLabel { color : #d0d0d0; }");
        return true;
    }
}

// Check if the config is valid
bool PlotUI::plotCfgIsValid(uint16_t dev_id, uint16_t msg_id, int ofst, int len)
{
    if (!emptyFields() && checkSID(dev_id, msg_id) && checkLenghtOffset(len, ofst)) {
        return true;
    }
    return false;
}

// Check plot configuration when fields are modified
void PlotUI::checkPlotCfg(uint16_t dev_id, uint16_t msg_id, int ofst, int len, int name_length)
{
    if (plotCfgIsValid(dev_id, msg_id, ofst, len) && name_length != 0) { // First check if everything is filed
        ui->createPlotButton->setEnabled(true);
    } else {
        ui->createPlotButton->setEnabled(false);
    }
}

void PlotUI::refreshPlotInfo(QList<double> values)
{
    ui->minValueInfo->setText(QString::number(values[0]));
    if (values[0] == 0 && values[2] == 0) {
        values[1] = 0;
    }
    ui->avgValueInfo->setText(QString::number(values[1]));
    ui->maxValueInfo->setText(QString::number(values[2]));
}

void PlotUI::on_plotList_itemClicked(QListWidgetItem* item)
{
    PlotWidget* widget;
    std::shared_ptr<PlotBaseObj> plotObj;
    if (setup_plot_active) {
        on_cancelPlotSetupButton_clicked();
    }
    if (!ui->rightSidebarButton->isEnabled()) {
        ui->rightSidebarButton->setEnabled(true);
    }
    if (!ui->plotProperties->isVisible()) {
        on_rightSidebarButton_clicked();
    }
    widget = (PlotWidget*)(ui->plotList->itemWidget(item));
    currentSelectedItem = item;
    currentSelectedPlot = widget->getPlotId();
    plotObj = getPlotObj(currentSelectedPlot);
    refreshPlotInfo(plotObj->getRanges());
    ui->plotName->setText(plotObj->getPlotCfg()->getName().replace(" ", "\n"));
    ui->devIdInfo->setText(QString::number(plotObj->getPlotCfg()->getDevId()));
    ui->msgIdInfo->setText(QString::number(plotObj->getPlotCfg()->getMessageId()));
    ui->plotColor->setStyleSheet(QString("background-color: %1;").arg(plotObj->getPlotCfg()->getPlotColor().name()));
    ui->changeColorInput->setCurrentText(getCurrentColorOption(plotObj->getPlotCfg()->getPlotColor()));
}

QString PlotUI::getCurrentColorOption(QColor color)
{
    QMapIterator<QString, QColor> i(plotColors);
    while (i.hasNext()) {
        i.next();
        if (i.value() == color) {
            return i.key();
        }
    }
    return "undefined";
}

void PlotUI::on_changeColorInput_currentIndexChanged(const QString& arg1)
{
    if (colorSetupFinished) {
        getPlotObj(currentSelectedPlot)->updateColor(plotColors[arg1]);
        ui->plotColor->setStyleSheet(QString("background-color: %1;").arg(plotColors[arg1].name()));
    }
}

void PlotUI::on_plotColorInput_currentIndexChanged(const QString& arg1)
{
    color = plotColors[arg1];
    ui->plotColorSetup->setStyleSheet(QString("background-color: %1;").arg(color.name()));
}

// Remove selected plot
void PlotUI::on_removePlotButton_clicked()
{
    killChildren(getPlotObj(currentSelectedPlot));
    getPlotObj(currentSelectedPlot)->kill();
    delete ui->plotList->takeItem(ui->plotList->row(currentSelectedItem)); // Remove item from plot list

    on_rightSidebarButton_clicked(); // Hide Plot Tab
    if (plotObjects.length() == 0) {
        ui->exportButton->setEnabled(false);
        ui->mathButton->setEnabled(false);
    }
    ui->rightSidebarButton->setEnabled(false);
}

void PlotUI::killChildren(std::shared_ptr<PlotBaseObj> plot)
{
    PlotWidget* widget;
    foreach (auto childObj, plot->getChildren()) {
        if (!childObj->isDead()) {
            for (int i = 0; i < plotObjects.count(); i++) {
                widget = (PlotWidget*)(ui->plotList->itemWidget(ui->plotList->item(i)));
                if (widget->getPlotId() == childObj->getPlotCfg()->getPlotId()) {
                    if (!childObj->getChildren().isEmpty()) {
                        killChildren(childObj);
                    }
                    ui->plotList->takeItem(i);
                    break;
                }
            }
        }
    }
}

// Activate Zoom by selection
void PlotUI::on_zoomButton_clicked()
{
    if (ui->zoomButton->isChecked()) {
        ui->graphicsView->setRubberBand(QChartView::RectangleRubberBand);
        ui->zoomButton->setText("Return to Default");
    } else {
        resetPlotZoom();
    }
}

// Reset Selection Zoom
void PlotUI::resetPlotZoom()
{
    ui->graphicsView->setRubberBand(QChartView::NoRubberBand);
    ui->zoomButton->setText("Zoom by selection");
    plot->zoomReset();
}

// Enable Zoom Tools on the UI
void PlotUI::enableZoomTools(bool choice)
{
    ui->zoomButton->setEnabled(choice);
    ui->gotoMinXInput->setEnabled(choice);
    ui->gotoMaxXInput->setEnabled(choice);
    ui->gotoMinYInput->setEnabled(choice);
    ui->gotoMaxYInput->setEnabled(choice);
    ui->resetZoomBtn->setEnabled(choice);
    ui->gotoBtn->setEnabled(choice);
}

// Zoom to a specific section
void PlotUI::on_gotoBtn_clicked()
{
    if ((!fixedLimits || freezeState) || (fixedLimits && freezeState)) {
        axisX->setRange(ui->gotoMinXInput->value(), ui->gotoMaxXInput->value());
        axisY->setRange(ui->gotoMinYInput->value(), ui->gotoMaxYInput->value());
    } else {
        CANmessage fakeMsg;
        fixedRangeMinX = ui->gotoMinXInput->value();
        fixedRangeMinY = ui->gotoMinYInput->value();
        fixedRangeMaxY = ui->gotoMaxYInput->value();
        fixedRangesSet = true;
        fakeMsg.timestamp = 0;
        process_CAN_message(fakeMsg);
    }
}

// Reset General Zoom
void PlotUI::on_resetZoomBtn_clicked()
{
    axisX->setRange(0, backupRangeX);
    axisY->setRange(backupRangeMinY, backupRangeMaxY);
    fixedRangesSet = false;
    ui->scrollCheckbox->setCheckState(Qt::CheckState(2));
    resetPlotZoom();
}

// Fix limits
void PlotUI::on_fixLimits_clicked()
{
    if (!fixedLimits) {
        fixedLimits = true;
        fixedRangesSet = false;
        enableZoomTools(true);
        ui->scrollCheckbox->setEnabled(true);
        ui->gotoMaxXInput->setEnabled(false);
        ui->zoomButton->setEnabled(false);
        if (scrollEnabled) {
            ui->gotoMinXInput->setEnabled(false);
        }
    } else {
        fixedLimits = false;
        ui->scrollCheckbox->setEnabled(false);
        enableZoomTools(false);
    }
}

void PlotUI::setSampleRate()
{
    sampleRate = ui->sampleRateInput->value();
    qDebug() << "Sample rate set to: " << QString::number(sampleRate);
}

void PlotUI::on_scrollCheckbox_stateChanged(int arg1)
{
    if (arg1 == 2) {
        scrollEnabled = true;
        ui->gotoMinXInput->setEnabled(false);
    } else {
        scrollEnabled = false;
        ui->gotoMinXInput->setEnabled(true);
    }
}

// Show Plot View Export Widget
void PlotUI::on_exportButton_clicked()
{
    if (!ui->exportTab->isVisible()) {
        if (ui->plotProperties->isVisible() || ui->plotSetup->isVisible()) {
            on_rightSidebarButton_clicked();
        }
        ui->exportTab->show();
        ui->exportButton->setEnabled(false);
        ui->exportPath->setText("No path selected");
        exportPath = "";
        ui->rightSidebarButton->setArrowType(Qt::RightArrow);
        checkExportFields();
    }
}

// Cancel Plot View Export Widget
void PlotUI::on_cancelExportBtn_clicked()
{
    on_rightSidebarButton_clicked();
    ui->exportButton->setEnabled(true);
}

void PlotUI::on_exportPathBtn_clicked()
{
    if (!freezeState) {
        on_freezeBtn_clicked();
    }
    exportPath = QFileDialog::getExistingDirectory(this, tr("Open Directory"), QDir::currentPath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    ui->exportPath->setText(exportPath);
    checkExportFields();
    if (freezeState) {
        on_freezeBtn_clicked();
    }
}

void PlotUI::on_exportTypeInput_currentTextChanged(const QString& arg1)
{
    if (arg1 == "Plot List") {
        exportFileType = "yaml";
    } else if (arg1 == "Plot Points") {
        exportFileType = "csv";
    } else {
        exportFileType = "png";
    }
    ui->exportFileType->setText("." + exportFileType);
}

void PlotUI::checkExportFields()
{
    if (ui->exportNameInput->text().length() == 0 || exportPath.length() == 0) {
        ui->exportActionBtn->setEnabled(false);
    } else {
        ui->exportActionBtn->setEnabled(true);
    }
}

// Export Data
void PlotUI::on_exportActionBtn_clicked()
{
    ui->cancelExportBtn->setEnabled(false);
    ui->exportActionBtn->setEnabled(false);
    ioEngine* out = new ioEngine(this->_fcp);
    out->setExport(ui->exportPath->text(), ui->exportNameInput->text(), ui->exportProgressBar);
    if (ui->exportTypeInput->currentText() == "Plot List") {
        out->exportPlotList(plotObjects);
    } else if (ui->exportTypeInput->currentText() == "Plot Points") {
        if (!freezeState) {
            on_freezeBtn_clicked();
            out->exportPoints(plotObjects);
            on_freezeBtn_clicked();
        } else {
            out->exportPoints(plotObjects);
        }
    } else {
        out->exportScreenshot(ui->graphicsView);
    }
    ui->cancelExportBtn->setEnabled(true);
    ui->exportActionBtn->setEnabled(true);
}

// Import Data
void PlotUI::on_importButton_clicked()
{
    ui->importButton->setEnabled(false);
    QString path = QFileDialog::getOpenFileName(this, tr("Load Plot List"), QDir::currentPath(), tr("Plot List YAML File (*.yaml)"));
    if (!path.isEmpty()) {
        ioEngine* in = new ioEngine(this->_fcp);
        in->setImport(path, plotObjects.length(), plot, axisX, axisY);
        foreach (auto plot, in->importExec()) {
            addImportedPlot(plot);
        }
        if (!ui->leftSidebar->isVisible()) {
            on_leftSidebarButton_clicked();
        }
        ui->exportButton->setEnabled(true);
        ui->mathButton->setEnabled(true);
    }
    ui->importButton->setEnabled(true);
}

// Chart themes system
void PlotUI::on_themesPicker_currentTextChanged(const QString& arg1)
{
    if (colorSetupFinished) {
        plot->setBackgroundBrush(chartThemes[arg1]["chartBackground"]);
        // Change Axis Pens
        axisX->setLinePenColor(chartThemes[arg1]["chartAxis"]);
        axisY->setLinePenColor(chartThemes[arg1]["chartAxis"]);
        axisX->setGridLinePen(chartThemes[arg1]["chartGrid"]);
        axisY->setGridLinePen(chartThemes[arg1]["chartGrid"]);
        axisX->setLabelsColor(chartThemes[arg1]["chartLabel"]);
        axisY->setLabelsColor(chartThemes[arg1]["chartLabel"]);
    }
}

// Show Math Tab
void PlotUI::on_mathButton_clicked()
{
    if (!ui->mathTab->isVisible()) {
        if (ui->plotProperties->isVisible() || ui->plotSetup->isVisible()) {
            on_rightSidebarButton_clicked();
        }
        on_cancelPlotSetupButton_clicked();
        // Setup Combobox
        ui->singlePlotX->clear();
        ui->plotX->clear();
        ui->plotY->clear();
        foreach (auto plot, plotObjects) {
            auto name = plot->getPlotCfg()->getName();
            ui->singlePlotX->addItem(name);
            ui->plotX->addItem(name);
            ui->plotY->addItem(name);
        }
        ui->singleOpInput->setCurrentIndex(0);
        ui->doubleOpInput->setCurrentIndex(0);
        on_singleOpInput_currentTextChanged("value");
        ui->mathButton->setEnabled(false);
        ui->singleOP->hide();
        ui->doubleOP->hide();
        on_opTypeInput_currentTextChanged("Single Plot");
        ui->mathTab->show();
        ui->rightSidebarButton->setArrowType(Qt::RightArrow);
    }
}

// Hide Math Tab
void PlotUI::on_cancelMathButton_clicked()
{
    on_rightSidebarButton_clicked();
    ui->mathButton->setEnabled(true);
}

void PlotUI::on_opTypeInput_currentTextChanged(const QString& arg1)
{
    if (arg1 == "Single Plot") {
        if (ui->doubleOP->isVisible()) {
            ui->doubleOP->hide();
        }
        ui->singleOP->show();
    } else {
        if (ui->singleOP->isVisible()) {
            ui->singleOP->hide();
        }
        ui->doubleOP->show();
    }
}

void PlotUI::on_singleOpInput_currentTextChanged(const QString& arg1)
{
    if (arg1 == "Custom Expression") {
        ui->opSingleValueInput->hide();
        ui->singleExpLabel->setText("Custom Expression:");
        ui->singleOpExpression->show();
    } else {
        ui->opSingleValueInput->show();
        ui->singleExpLabel->setText("Value:");
        ui->singleOpExpression->hide();
    }
}

void PlotUI::on_createMathPlotBtn_clicked()
{
    QString type = ui->opTypeInput->currentText();
    QString opTemplate, plotName;
    if (type == "Single Plot") {
        opTemplate = simpleMathDB[ui->singleOpInput->currentText()];
        plotName = ui->singlePlotX->currentText();
        plotName.append(QString(" [%1]").arg(ui->singleOpInput->currentText()));
    } else {
        opTemplate = complexMathDB[ui->doubleOpInput->currentText()];
        plotName = ui->plotX->currentText() + "/" + ui->plotY->currentText();
        plotName.append(QString(" [%1]").arg(ui->doubleOpInput->currentText()));
    }
    if (opTemplate == "custom") {
        if (type == "Single Plot") {
            opTemplate = ui->singleOpExpression->text();
        } else {
            opTemplate = ui->doubleOpExpression->text();
        }
    } else {
        if (type == "Single Plot") {
            opTemplate.append(QString::number(ui->opSingleValueInput->value()));
            plotName.append(QString("[%1]").arg(ui->opSingleValueInput->value()));
        }
    }
    // qDebug() << "Expression Generated: " << opTemplate;
    // qDebug() << "Name: " << plotName;

    // Get ParentX
    std::shared_ptr<PlotBaseObj> parentX = getPlotObjByName(ui->singlePlotX->currentText());

    if (type == "Single Plot") {
        std::shared_ptr<PlotBaseObj> child = createChildPlot(parentX, plotName, opTemplate);
        parentX->addChild(child);
        addImportedPlot(child);
    }

    on_rightSidebarButton_clicked();
    ui->mathButton->setEnabled(true);
    ui->rightSidebarButton->setEnabled(false);
}

void PlotUI::on_reportButton_clicked()
{
    QDesktopServices::openUrl(QUrl("https://gitlab.com/projectofst/software10e/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=", QUrl::TolerantMode));
}
