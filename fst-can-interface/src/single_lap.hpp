﻿#pragma once
#ifndef SINGLE_LAP_HPP
#define SINGLE_LAP_HPP

#include "lap_time.hpp"
#include <QWidget>
namespace Ui {
class Single_lap;
}

class Single_lap : public QWidget {
    Q_OBJECT

public:
    explicit Single_lap(QWidget* parent = nullptr, int new_lap_number = 0, QString lap_id = "", qint64 new_time = 0, QString new_string_time = "ERROR", int new_cones = 0);
    ~Single_lap();

    int get_lap_number();

    int get_cones();

    qint64 get_time();

    QString get_time_string();

    void set_new_lap_number(int new_lap_number);

    Ui::Single_lap* ui;

    QString get_transform_time();
    QList<qint64> format_time(qint64 time);
    void set_new_lap_time(qint64 new_time);

    void press_del();

    void set_DNF_lap();

    int get_DNF_flag();

private slots:
    void on_delete_button_clicked();

    void on_cones_textChanged(const QString& arg1);

private:
    int lap_number, DNF_flag;
    int cones;
    qint64 time;
    QString string_time;
};

#endif // SINGLE_LAP_HPP
