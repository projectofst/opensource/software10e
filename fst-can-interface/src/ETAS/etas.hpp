#ifndef ETAS_HPP
#define ETAS_HPP

#include "Console/FcpDisplay.hpp"
#include "Exceptions/helper.hpp"
#include "Resources/configslist.h"
#include "UiWindow.hpp"
#include "fcpcom.hpp"
#include <QWidget>

namespace Ui {
class Etas;
}

class Etas : public UiWindow {
    Q_OBJECT

public:
    explicit Etas(
        QWidget* parent = nullptr,
        FCPCom* fcp = nullptr,
        Helper* log = nullptr,
        ConfigManager* config_manager = nullptr);
    void updateFCP(FCPCom* fcp) override;
    void load_state(QMap<QString, QVariant> config) override;
    QString getTitle() override { return "ETAS"; };
    void export_state() override;
    ~Etas();

private slots:
    void process_CAN_message(CANmessage msg) override;

private:
    Ui::Etas* ui;
    Helper* logger;
    ConfigManager* config_manager;
    ConfigsList* configs_list;
    FcpDisplay* yrc_list;
    FcpDisplay* pl_list;
    FcpDisplay* em_list;
    FCPCom* _fcp;
};

#endif // ETAS_HPP
