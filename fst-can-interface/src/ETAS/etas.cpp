#include "etas.hpp"
#include "ui_etas.h"

Etas::Etas(QWidget* parent,
    FCPCom* fcp,
    Helper* log,
    ConfigManager* config_manager)
    : UiWindow(parent, fcp)
    , ui(new Ui::Etas)
{
    ui->setupUi(this);
    this->_fcp = fcp;
    this->config_manager = config_manager;
    this->logger = log;

    configs_list = ConfigsList::Builder(parent, fcp).withDevice("etas").Build();
    yrc_list = FcpDisplay::Builder(this, fcp)
                   .withShowBar(false)
                   .withShowDelete(false)
                   .withShowDeviceName(false)
                   .withSignal("etas", "YRC_max_trq_F", 0)
                   .withSignal("etas", "YRC_max_trq_R", 0)
                   .withSignal("etas", "YRC_min_trq_F", 0)
                   .withSignal("etas", "YRC_min_trq_R", 0)
                   .withSignal("etas", "etas_max_pwr", 0)
                   .withSignal("etas", "etas_max_regen_pwr", 0)
                   .withSignal("etas", "YRC_kp", 0)
                   .withSignal("etas", "YRC_ku", 0)
                   .withSignal("etas", "YRC_steer_factor", 0)
                   .withSignal("etas", "YRC_grip_factor", 0)
                   .Build();

    pl_list = FcpDisplay::Builder(this, fcp)
                  .withShowBar(false)
                  .withShowDelete(false)
                  .withShowDeviceName(false)
                  .withSignal("etas", "PL_kp_gain", 0)
                  .withSignal("etas", "PL_ki_gain", 0)
                  .withSignal("etas", "PL_max_pwr", 0)
                  .withSignal("etas", "EM_init_pwr_lim", 0)
                  .withSignal("etas", "EM_curr_lap", 0)
                  .Build();

    em_list = FcpDisplay::Builder(this, fcp)
                  .withShowBar(false)
                  .withShowDelete(false)
                  .withShowDeviceName(false)
                  .withSignal("etas", "EM_enable", 0)
                  .withShowValue(false)
                  .withLed(
                      LEDwidget::Builder(this).Build())
                  .withSignal("etas", "EM_total_laps", 0)
                  .withSignal("etas", "EM_init_pwr_lim", 0)
                  .withSignal("etas", "EM_curr_lap", 0)
                  .withSignal("etas", "YRC_EM_pwr_lim", 0)
                  .Build();

    this->ui->yrc_display->addWidget(yrc_list);
    this->ui->pl_display->addWidget(pl_list);
    this->ui->em_display->addWidget(em_list);
    this->ui->config_layout->addWidget(configs_list);
    connect(configs_list, &ConfigsList::send_to_CAN, this, [this](QByteArray& msg) { emit send_to_CAN(msg); });
}

void Etas::updateFCP(FCPCom* fcp)
{
    this->_fcp = fcp;
}

void Etas::load_state(QMap<QString, QVariant>)
{
    qDebug() << "NEPIA";
}

void Etas::export_state()
{
    qDebug() << "chapeu";
}

void Etas::process_CAN_message(CANmessage msg)
{
    yrc_list->updateFCPDisplay(msg);
    pl_list->updateFCPDisplay(msg);
    em_list->updateFCPDisplay(msg);
}

Etas::~Etas()
{
    delete ui;
    delete configs_list;
    delete yrc_list;
    delete pl_list;
}
