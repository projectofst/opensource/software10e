#ifndef COMSGUY_HPP
#define COMSGUY_HPP

#include "WindowMain.hpp"
#include <QDialog>
#include <QFile>
#include <QFileDialog>
#include <QMainWindow>
#include <QMap>
#include <QObject>
#include <QString>
#include <QTextStream>

#include "COM.hpp"
#include "COM_Interface.hpp"
#include "COM_LogGen.hpp"
#include "COM_LogPlay.hpp"
#include "COM_LogRec.hpp"
#include "COM_SerialPort.hpp"
#include "Console.hpp"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/INTERFACE_CAN.h"
#include "fcpcom.hpp"
#include "log/com_log.h"
#include "loopback/COM_Loopback.h"
#include "qt-can-com/COM_PlotJuggler.hpp"
#include "qt-can-com/UDPSocket.hpp"
#include "socket-can/com_socketcan.h"

#include <qmessagebox.h>

/*typedef enum _PortType {
    Close = 0,
    Serial = 1,
    WifiUdp = 2,
    LogRecorder = 3,
    LogGenera = 4,
    LogPlayer = 5,
    Kvaser = 6,
} PortType;
*/

class Comsguy : public QObject {
    Q_OBJECT

public:
    explicit Comsguy(QObject* parent = nullptr, FCPCom* fcp = nullptr);
    ~Comsguy(void);
    QMap<QString, COM*> Templates;

public slots:
    void connectCOM(QString, QString, QString, QString);
    void disconnectCOM(QString, QString, QString, QString);
    void connectMainWin(MainWindow*);
    QList<COM*> newCOM(QString protocol);
    bool InCOMList(QString com_id);
    QList<COM*> getCOM(QString com_id);

private:
    int MsgCounter;
    int currSock;
    COM* SourceCOM;
    COM* DestinCOM;
    LogRec* LoggerCOM;
    LogPlay* LogPlayCOM;
    LogGenerator* LogGenCOM;
    SerialPort* SerialCOM;
    QList<COM*> sockList;
    FCPCom* fcp;
    QList<QList<COM*>> ComList;
    CANmessage socketPresenceMessage;
};

#endif // COMSGUY_HPP
