#ifndef FCPDOCS_H
#define FCPDOCS_H

#include <QWidget>

namespace Ui {
class FcpDocs;
}

class FcpDocs : public QWidget {
    Q_OBJECT

public:
    explicit FcpDocs(QWidget* parent = nullptr);
    ~FcpDocs();

private slots:
    void on_generateButtons_clicked();

private:
    Ui::FcpDocs* ui;
    void open_text_browser();
};

#endif // FCPDOCS_H
