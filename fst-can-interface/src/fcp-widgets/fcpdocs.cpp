#include "fcpdocs.h"
#include "ui_fcpdocs.h"

#include "QVBoxLayout"
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QProcess>
#include <QTextBrowser>
#include <QUrl>

FcpDocs::FcpDocs(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::FcpDocs)
{
    ui->setupUi(this);

    if (!QDir("fcp-docs").exists()) {
        this->ui->message->setText("Could not find documentation sources.\n Press generate!");
        return;
    } else {
        this->ui->message->setText("");
    }

    open_text_browser();
}

FcpDocs::~FcpDocs()
{
    delete ui;
}

void FcpDocs::open_text_browser()
{
    this->ui->message->setText("");
    QTextBrowser* browser = new QTextBrowser();
    browser->setOpenExternalLinks(true);
    browser->setSource(QUrl("fcp-docs/index.html"));
    browser->setSearchPaths({ "fcp-docs" });
    ui->verticalLayoutBrowser->addWidget(browser);
}

void FcpDocs::on_generateButtons_clicked()
{

    QString filename = QFileDialog::getOpenFileName(this, tr("Open FCP JSON"),
        QDir::currentPath(),
        tr("JSON (*.json)"));

    QProcess process;
    process.start("fcp", QStringList() << "docs" << filename << "fcp-docs");
    process.waitForFinished();
    open_text_browser();
}
