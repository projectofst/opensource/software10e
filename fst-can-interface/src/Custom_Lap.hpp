#ifndef CUSTOM_LAP_HPP
#define CUSTOM_LAP_HPP

#include <QDialog>

namespace Ui {
class Custom_Lap;
}

class Custom_Lap : public QDialog {
    Q_OBJECT

public:
    explicit Custom_Lap(QWidget* parent = nullptr);
    ~Custom_Lap();

private slots:
    void on_close_button_clicked();

    void on_OC_check_stateChanged(int arg1);

    void on_DM_check_stateChanged(int arg1);

    void on_minutes_textChanged(const QString& arg1);

    void on_seconds_textChanged(const QString& arg1);

    void on_mseconds_textChanged(const QString& arg1);

    void on_add_clicked();

private:
    Ui::Custom_Lap* ui;
    qint64 time;
    int cones, OC, DM;

signals:
    void lap_finished(qint64 time, int cones, int OC, int DM);
};

#endif // CUSTOM_LAP_HPP
