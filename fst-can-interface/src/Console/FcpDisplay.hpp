#ifndef FCPDISPLAY_HPP
#define FCPDISPLAY_HPP

#include "Exceptions/helper.hpp"
#include "Resources/FCPSignalDisplayValue.hpp"
#include "fcpcom.hpp"
#include <QComboBox>
#include <QFileDialog>
#include <QWidget>

namespace Ui {
class FcpDisplay;
}

class FcpDisplay : public QWidget {
    Q_OBJECT

public:
    // Builder system
    class Builder {
    public:
        FcpDisplay* fcpdisplay_object = nullptr;

        Builder(QWidget* parent, FCPCom* fcp)
        {
            fcpdisplay_object = new FcpDisplay(parent, fcp);
        }

        Builder& withShowDelete(bool show_delete)
        {
            fcpdisplay_object->setShowDeletes(show_delete);
            return *this;
        }

        Builder& withShowBar(bool show_bar)
        {
            fcpdisplay_object->setShowLowerBar(show_bar);
            fcpdisplay_object->setSignalCheck(show_bar);
            return *this;
        }

        Builder& withShowDeviceName(bool show_devicename)
        {
            fcpdisplay_object->setShowDeviceName(show_devicename);
            return *this;
        }

        Builder& withSignal(QString deviceName, QString signalName, int muxCount)
        {
            qDebug() << "Registering: " << signalName;
            fcpdisplay_object->createNewFcpDisplayWidget(deviceName, signalName,
                muxCount);
            return *this;
        }

        Builder& withShowValue(bool show_value)
        {
            fcpdisplay_object->setCurrentWidgetShowValue(show_value);
            return *this;
        }

        Builder& withLed(LEDwidget* led)
        {
            fcpdisplay_object->setCurrentWidgetLed(led);
            return *this;
        }

        FcpDisplay* Build() { return fcpdisplay_object; }
    };

    explicit FcpDisplay(QWidget* parent, FCPCom* fcp);

    void addDevices();
    QList<QVariant> exportConfig();
    void loadConfig(QList<QVariant> config);
    void updateMessageBox(QString arg1, QComboBox* box, QString type);
    QString clearComboBoxInput(QString toClean);
    void updateFCPDisplay(CANmessage msg);
    void createNewFcpDisplayWidget(QString deviceName, QString signalName,
        int muxCount);
    bool muxSelected(FCPSignalDisplayValue* filter, int mux);
    void updateFCP(FCPCom* fcp);
    void checkIfExists();
    int reverse_list(int index);

    void setShowDeletes(bool show_deletes);
    void setShowLowerBar(bool show_bar);
    void setShowDeviceName(bool show_devicename);
    void setSignalCheck(bool enable_signalcheck);
    void setCurrentWidgetShowValue(bool show_value);
    void setCurrentWidgetLed(LEDwidget* led);

    ~FcpDisplay();

private slots:
    void on_AddNewFCPDisplayFilterButton_clicked();

    void on_FCPDisplaySignalBox_currentIndexChanged(const QString& arg1);

    void on_FCPDisplayDeviceBox_currentIndexChanged(const QString& arg1);

    void on_FCPDisplayMessageBox_currentIndexChanged(const QString& arg1);

    void on_FCPDisplayMuxSelector_textChanged(const QString& arg1);

private:
    bool showDeletes = true;
    bool showLowerBar = true;
    bool showDeviceName = true;
    bool enableSignalCheck = true;
    Ui::FcpDisplay* ui;
    FCPCom* _fcp;
    QMap<QString, Device*> _deviceMap;
    FCPSignalDisplayValue* current_widget = nullptr;
    QMap<QString, FCPSignalDisplayValue*> _fcpDisplayMap;
};

#endif // FCPDISPLAY_HPP
