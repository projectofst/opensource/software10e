#include "FcpDisplay.hpp"
#include "ui_FcpDisplay.h"

FcpDisplay::FcpDisplay(QWidget* parent, FCPCom* fcp)
    : QWidget(parent)
    , ui(new Ui::FcpDisplay)
{
    ui->setupUi(this);

    this->_fcp = fcp;

    try {
        addDevices();
    } catch (JsonException& e) {
        qDebug() << e.toString();
    }

    ui->FCPDisplayMuxSelector->setDisabled(true);
}

void FcpDisplay::setShowLowerBar(bool show_bar)
{
    ui->lowerBar->setVisible(show_bar);
}

void FcpDisplay::setShowDeletes(bool show_deletes)
{
    this->showDeletes = show_deletes;
}

void FcpDisplay::setShowDeviceName(bool show_devicename)
{
    this->showDeviceName = show_devicename;
}

void FcpDisplay::setCurrentWidgetShowValue(bool show_value)
{
    if (current_widget != nullptr) {
        current_widget->showValue(show_value);
    } else {
        qDebug() << "[FCPDisplay][Illegal Builder Call]: Could not find any widget in scope to apply this property";
    }
}

void FcpDisplay::setCurrentWidgetLed(LEDwidget* led)
{
    if (current_widget != nullptr) {
        current_widget->setLed(led);
    } else {
        qDebug() << "[FCPDisplay][Illegal Builder Call]: Could not find any widget in scope to apply this property";
    }
}

void FcpDisplay::setSignalCheck(bool enable_signalcheck)
{
    this->enableSignalCheck = enable_signalcheck;
}

void FcpDisplay::addDevices()
{

    QList<Device*> devices = _fcp->getDevices();
    devices.append(_fcp->getCommon());

    ui->FCPDisplayDeviceBox->clear();

    foreach (Device* currentDevice, devices) {
        QString currentName = currentDevice->getName();

        this->_deviceMap.insert(currentName, currentDevice);
        ui->FCPDisplayDeviceBox->addItem(
            currentName + " (" + QString::number(currentDevice->getId()) + ")");
    }
}

void FcpDisplay::updateFCPDisplay(CANmessage msg)
{
    QList<CanSignal*> sigs;
    try {
        sigs = this->_fcp->getMessage(msg.candata.dev_id, msg.candata.msg_id)
                   ->getSignals();
    } catch (...) {
        qDebug("Failed to retrieve message");
        return;
    }

    QMap<QString, double> signalValues;
    try {
        signalValues = this->_fcp->decodeMessage(msg).second;
    } catch (...) {
        qDebug("Failed to decode message");
        return;
    }

    foreach (CanSignal* sig, sigs) {
        auto mux = signalValues.value(sig->getMux(), -1);

        QString muxString = sig->getName();
        if (mux != -1) {
            muxString += QString::number(mux);
        }
        if (this->_fcpDisplayMap.contains(muxString)) {
            double value = this->_fcp->decodeDoubleFromSignal(msg, sig->getName());
            if (sig->getMuxCount() <= 1 || muxSelected(_fcpDisplayMap.value(muxString), mux)) {
                this->_fcpDisplayMap.value(muxString)->updateValue(value);
            }
        }
    }
}

bool FcpDisplay::muxSelected(FCPSignalDisplayValue* filter, int mux)
{
    return filter->getMuxCount() == mux;
}

void FcpDisplay::updateMessageBox(QString arg1, QComboBox* box, QString type)
{
    box->clear();
    if (arg1 == "") {
        return;
    }
    try {
        Device* currentDevice = this->_deviceMap.value(clearComboBoxInput(arg1));
        if (currentDevice == NULL) {
            return;
        }

        /*dirty and there is a better way to do this, by using the superclass
         * JsonComponent, but i have no time*/
        if (type == "Message") {
            QList<Message*> messages = this->_fcp->getMessages(currentDevice->getId());

            try {
                messages.append(this->_fcp->getCommon()->getMessages());
            } catch (...) {
                qDebug() << "Failed to load common messages";
            }

            box->addItem("Any");

            foreach (Message* currentMessage, messages) {
                box->addItem(currentMessage->getName() + " (" + QString::number(currentMessage->getId()) + ")");
            }

        } else if (type == "Get" || type == "Set") {
            QList<Config*> configs = this->_fcp->getDevice(currentDevice->getId())->getConfigs();
            foreach (Config* config, configs) {
                box->addItem(config->getName() + " (" + QString::number(config->getId()) + ")");
            }
        } else if (type == "Command") {
            QList<Command*> commands = this->_fcp->getDevice(currentDevice->getId())->getCommands();
            foreach (Command* command, commands) {
                box->addItem(command->getName() + " (" + QString::number(command->getId()) + ")");
            }
        }
    } catch (JsonException& e) {
        qDebug() << e.toString();
        return;
    }
}

void FcpDisplay::createNewFcpDisplayWidget(QString deviceName,
    QString signalName, int muxCount)
{
    QString muxName;
    try {
        muxName = this->_fcp->getSignal(signalName)->getMux();
    } catch (JsonException& e) {
        qDebug() << e.toString();
        return;
    }

    FCPSignalDisplayValue* newDisplay = new FCPSignalDisplayValue(
        this, deviceName, signalName, muxCount, muxName);
    newDisplay->showDeleteButton(this->showDeletes);
    newDisplay->showDeviceName(this->showDeviceName);

    current_widget = newDisplay; // Track current widget

    if (muxName != "") {
        signalName += QString::number(muxCount);
    }

    this->ui->FCPDisplayWidegts->addWidget(newDisplay);
    this->_fcpDisplayMap.insert(signalName, newDisplay);

    connect(newDisplay, &FCPSignalDisplayValue::destroyed, [this, newDisplay]() {
        this->_fcpDisplayMap.remove(newDisplay->getSignalName());
        checkIfExists();
    });
}

QString FcpDisplay::clearComboBoxInput(QString toClean)
{
    /*toClean is like: <String> <(Id)>*/
    QList<QString> stringList = toClean.split(" ");
    return stringList[0];
}

QList<QVariant> FcpDisplay::exportConfig()
{
    QList<QVariant> configList;
    foreach (auto signalPair, this->_fcpDisplayMap) {
        QMap<QString, QVariant> config;
        config["deviceName"] = signalPair->getDeviceName();
        config["signalName"] = signalPair->getSignalName();
        config["muxCount"] = signalPair->getMuxCount();
        configList.append(config);
    }
    return configList;
}

void FcpDisplay::loadConfig(QList<QVariant> signal_config)
{
    foreach (auto signal, signal_config) {
        QMap<QString, QVariant> config = signal.toMap();
        createNewFcpDisplayWidget(config["deviceName"].toString(),
            config["signalName"].toString(),
            config["muxCount"].toInt());
    }
}

void FcpDisplay::checkIfExists()
{
    QString muxName;
    QString signalName;

    if (!enableSignalCheck) {
        return;
    }

    try {
        muxName = this->_fcp->getSignal(this->ui->FCPDisplaySignalBox->currentText())->getMux();
        signalName = this->ui->FCPDisplaySignalBox->currentText();
    } catch (JsonException& e) {
        qDebug() << "[checkIfExists]: " << e.toString();
        ui->AddNewFCPDisplayFilterButton->setDisabled(true);
        ui->AddNewFCPDisplayFilterButton->setText("Error");
        return;
    }

    if (muxName != "") {
        signalName += QString::number(this->ui->FCPDisplayMuxSelector->value());
    }

    if (this->_fcpDisplayMap.contains(signalName)) {
        ui->AddNewFCPDisplayFilterButton->setDisabled(true);
        ui->AddNewFCPDisplayFilterButton->setText("Already added");
    } else {
        ui->AddNewFCPDisplayFilterButton->setDisabled(false);
        ui->AddNewFCPDisplayFilterButton->setText("Add");
    }
}

FcpDisplay::~FcpDisplay()
{
    this->_fcpDisplayMap.clear();

    delete ui;
}

void FcpDisplay::updateFCP(FCPCom* fcp)
{
    if (fcp == NULL) {
        return;
    }
    this->_fcp = fcp;
    addDevices();
    return;
}

void FcpDisplay::on_AddNewFCPDisplayFilterButton_clicked()
{
    this->createNewFcpDisplayWidget(
        this->ui->FCPDisplayDeviceBox->currentText().split(" ").at(0),
        this->ui->FCPDisplaySignalBox->currentText(),
        this->ui->FCPDisplayMuxSelector->value());
    checkIfExists();
}

void FcpDisplay::on_FCPDisplaySignalBox_currentIndexChanged(
    const QString& arg1)
{
    try {
        CanSignal* current_message = this->_fcp->getSignal(arg1);

        int mux_count = current_message->getMuxCount();

        if (mux_count > 1) {
            ui->FCPDisplayMuxSelector->setDisabled(false);
            ui->FCPDisplayMuxSelector->setRange(0, mux_count - 1);
        } else {
            ui->FCPDisplayMuxSelector->setDisabled(true);
        }
        checkIfExists();
    } catch (JsonException& e) {
        ui->FCPDisplayMuxSelector->setDisabled(true);
        return;
    }
}

void FcpDisplay::on_FCPDisplayDeviceBox_currentIndexChanged(
    const QString& arg1)
{
    updateMessageBox(arg1, this->ui->FCPDisplayMessageBox, "Message");
    return;
}

void FcpDisplay::on_FCPDisplayMessageBox_currentIndexChanged(
    const QString& arg1)
{
    QList<CanSignal*> sigs;
    try {
        Message* msg = this->_fcp->getMessage(arg1.split(" ").at(0));
        sigs = msg->getSignals();
    } catch (JsonException& e) {
        qDebug() << e.toString();
        return;
    }

    this->ui->FCPDisplaySignalBox->clear();
    foreach (CanSignal* sig, sigs) {
        this->ui->FCPDisplaySignalBox->addItem(sig->getName());
    }
}

void FcpDisplay::on_FCPDisplayMuxSelector_textChanged(const QString&)
{
    checkIfExists();
}
