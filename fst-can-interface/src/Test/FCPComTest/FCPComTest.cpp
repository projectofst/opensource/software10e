#include "FCPComTest.hpp"

void FCPComTest::verifySetJsonFile()
{
    FCPCom* fcp = new FCPCom();

    fcp->setFile("abc.json");

    QVERIFY(fcp->getCurrentJsonFile() == "abc.json");

    delete fcp;
}

void FCPComTest::fileDoesNotExistTest()
{
    FCPCom* fcp = new FCPCom();

    fcp->setFile("abc.json");
    QVERIFY_EXCEPTION_THROWN(fcp->parse(), jsonFileDoesNotExistException);
    delete fcp;
}

void FCPComTest::badJsonFileFormatTest()
{
    FCPCom* fcp = new FCPCom();

    fcp->setFile("random.json");
    QVERIFY_EXCEPTION_THROWN(fcp->parse(), badJsonFileFormatException);
    delete fcp;
}

void FCPComTest::noFileSelectedtest()
{
    FCPCom* fcp = new FCPCom();

    QVERIFY_EXCEPTION_THROWN(fcp->parse(), noFileSelectedException);
}
QTEST_MAIN(FCPComTest);
