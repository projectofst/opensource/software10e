#include "../../fcpcom.hpp"
#include "QtTest/QtTest"

class FCPComTest : public QObject {
    Q_OBJECT
private slots:
    void verifySetJsonFile();
    void fileDoesNotExistTest();
    void badJsonFileFormatTest();
};
