#!/bin/bash


for d in */ ; do
	cd $d; make;
	EXECUTABLE="${d///}"
	ls
	echo $EXECUTABLE
	./$EXECUTABLE
	if [ $? -eq 1 ]
	then
		exit 1
	fi
	cd ..
done
