#include "WindowMain.hpp"
#include "ui_WindowMain.h"

#include "ui_aboutdialog.h"

#include "QDebug"
#include "QDir"
#include "QFileInfo"

#include "QJsonDocument"

MainWindow::MainWindow(QWidget* parent, ConfigManager* config_manager)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    // this->ui->statusBar = new QStatusBar(parent);
    QLabel* label = new QLabel(this->ui->statusBar);
    label->setText("FST Can Interface v11.2 - Competition Version");

    this->ui->statusBar->insertPermanentWidget(1, label);

    this->_fcpObservers = *new QList<UiWindow*>();
    m_DockManager = new ads::CDockManager(this);

    QFile StyleSheetFile(":/custom_stylesheet.css");
    StyleSheetFile.open(QIODevice::ReadOnly);
    QTextStream StyleSheetStream(&StyleSheetFile);
    m_DockManager->setStyleSheet(StyleSheetStream.readAll());
    StyleSheetFile.close();

    this->config_manager = config_manager;
}

void MainWindow::setFCP(FCPCom* fcp)
{
    this->_fcp = fcp;
}

MainWindow::~MainWindow(void)
{
    delete this->_fcp;
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent*)
{
    QPainter* pPainter = new QPainter(this);
    pPainter->drawPixmap(rect(), QPixmap(":/Assets/Backgrounds/FSTelemetryBackground2.png"));
    delete pPainter;
}

#if 0
bool MainWindow::eventFilter(QObject *obj, QEvent *event) {
    /*
    if (event->type() == QEvent::Resize) {
        QResizeEvent *resizeEvent = static_cast<QResizeEvent*>(event);
        if (obj->objectName().startsWith("[DevDock]"))
            emit resizeDev(resizeEvent->size().width());
        if (obj->objectName().startsWith("[BatDock]"))
            emit resizeBat(resizeEvent->size().width());
        return false;
    } else {
        return QWidget::eventFilter(obj, event);
    }
    */
    return QWidget::eventFilter(obj, event);

}
#endif

void MainWindow::updateStatusBar()
{
    QPixmap* pixmap = new QPixmap;

    // Note that here I don't specify the format to make it try to autodetect it,
    // but you can specify this if you want to.
    pixmap->load(":/Instatroll/IconWallet/png/verified.png");

    QLabel* iconLbl = new QLabel;
    iconLbl->setPixmap(*pixmap);
    ui->statusBar->addWidget(iconLbl);
}

void MainWindow::on_actionAbout_triggered()
{
    QDialog* dialog = new QDialog(this);
    QString branch = GIT_CURRENT_BRANCH;
    QString branch_desc = " (Collected from: " + branch + " ) </b>";
    auto ui = new Ui::AboutDialog();
    ui->setupUi(dialog);

    ui->version->setText(QString("<b>version: <b>") + GIT_CURRENT_SHA1);
    ui->stability->setText(QString("<b>Is Stable: ") + QString(branch.contains("release/") ? "<b style=\"color:#2da160;\">YES" : "<b style=\"color:#ed1b20;\">NO") + branch_desc);
    ui->version->setAlignment(Qt::AlignCenter);
    ui->stability->setAlignment(Qt::AlignCenter);
    dialog->setAttribute(Qt::WA_DeleteOnClose);
    dialog->setWindowTitle("About");

    dialog->exec();
}

void MainWindow::on_actionOpen_Log_triggered()
{
    //    LogConverter *log_convert = new LogConverter();
    //    log_convert->show();
    //    log_convert->activateWindow();
    //    log_convert->raise();
}

void MainWindow::on_actionNew_triggered()
{
}

ConfigManager* MainWindow::getConfigManager()
{
    return this->config_manager;
}

/*
void MainWindow::actionToggleCOM_triggered()
{
    //    QDockWidget *Coms = new QDockWidget("Coms",this);
    //    Coms->setAllowedAreas(Qt::AllDockWidgetAreas);
    //    this->addDockWidget(Qt::TopDockWidgetArea, Coms);
    //    QQuickWidget *temp = new QQuickWidget(QUrl("file:///Users/diogopereira/Documents/fst-can-interface/src/COM_Select.qml"),Coms);
    //    temp->setResizeMode(QQuickWidget::SizeRootObjectToView);
    //    Coms->setWidget(static_cast<QWidget*>(temp));

    //    comselect->USBList();

}
*/

/*
void MainWindow::attachJsonObserver(UiWindow* newObserver){
    this->_fcpObservers.append(newObserver);
    return;
}
*/

/*
int MainWindow::detachJsonObserver(UiWindow* observer){
    return this->_fcpObservers.removeAll(observer);
}
*/

void MainWindow::on_actionFCPJson_triggered()
{
    QString _jsonFileName = "";
    QString path = QFileDialog::getOpenFileName(this, "Load Json File", QDir::currentPath(), "Load a json file (*.json)");
    if (!path.isEmpty()) {
        _jsonFileName = path;

        QMessageBox sure;
        sure.setText("The can interface needs to restart in order to load the json file");
        sure.setInformativeText("Do you wanna restart now?");
        sure.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        sure.setDefaultButton(QMessageBox::Yes);

        QMap<QString, QVariant> current_cfg;
        current_cfg["json_path"] = _jsonFileName;
        this->config_manager->registerGlobalConfig(current_cfg);

        int ans = sure.exec();
        switch (ans) {
        case QMessageBox::Yes:
            qApp->quit();
            QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
            break;

        case QMessageBox::No:
            break;
        }
    }
}

void MainWindow::notifyObservers()
{
    foreach (UiWindow* win, this->_fcpObservers) {
        win->updateFCP(this->_fcp);
    }
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    event->accept();
    emit willClose();
}

FCPCom* MainWindow::getFCPCom()
{
    return this->_fcp;
}
