#ifndef STATE_MANAGER_HPP
#define STATE_MANAGER_HPP

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QObject>
#include <QPair>
#include <QSettings>
#include <QVector>

template <typename storedValueType>
class state_manager {
public:
    explicit state_manager()
    {
    }

signals:

public slots:
    void save(QString name, QString specific)
    {
        QPair<QString, storedValueType> pair;
        QString loc = QDir::currentPath() + "/set/states/" + name.toUpper() + "_" + specific.toUpper() + "_settings.ini";
        QSettings settings(loc, QSettings::NativeFormat);
        foreach (pair, added) {
            settings.setValue(pair.first, pair.second);
        }
    }

    QVector<QPair<QString, QString>> load(QString filePath)
    {
        QVector<QPair<QString, QString>> states;
        QPair<QString, QString> pair;
        QString loc = filePath;
        QSettings settings(loc, QSettings::NativeFormat);

        foreach (QString key, settings.allKeys()) {
            pair.first = key.split("_").first();
            pair.second = settings.value(key, "").toString();
            states.append(pair);
        }
        return states;
    }

    QString find_ini(QString name, QString specific = "NO_INFO")
    {
        QDir directory(QDir::currentPath() + "/set/states/");
        QStringList inis = directory.entryList(QStringList() << "*.ini", QDir::Files);
        foreach (QString filename, inis) {
            if ((filename.contains(name.toUpper())) && (specific == "")) {
                return filename;
            } else if ((filename.contains(name.toUpper())) && (name.contains(specific))) {
                return filename;
            }
        }
        return "";
    }

    void add(QString name, storedValueType value)
    {
        QPair<QString, storedValueType> pair;
        pair.first = name + "_" + QString::number(itemCounter);
        pair.second = value;
        added.append(pair);
        itemCounter++;
    }

private:
    QVector<QPair<QString, storedValueType>> added;
    int itemCounter = 1;
};

#endif // STATE_MANAGER_HPP
