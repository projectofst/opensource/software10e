#ifndef SampleBasedLogKeeper_HPP
#define SampleBasedLogKeeper_HPP

#include "LogKeeper.hpp"

class SampleBasedLogKeeper : public LogKeeper {
private:
    ulong _sampleNumber;

public:
    /**
     * @brief SampleBasedLogKeeper
     * @param logFileName - Name of the log File.
     */
    SampleBasedLogKeeper(QString logFileName);

    /**
     * @brief SampleBasedLogKeeper - Constructor.
     */
    SampleBasedLogKeeper();

    /**
     * @brief setNewProgress - Sets a new progress to the log processing. Use this to got back and forth in the log.
     * @param percentage - New percentage value.
     */
    void setNewProgress(float percentage) override;

    /**
     * @brief parseLog - Parses the log File.
     */
    void parseLog() override;

private slots:
    void setSampleNumber(int sampleNumber);
    float calculatePercentage() override;
    void recalculateProgress() override;
    void recalculateIndex(float percentage) override;
};

#endif // SampleBasedLogKeeper_HPP
