#ifndef LOGREPLAY_HPP
#define LOGREPLAY_HPP

#include "LogKeeper.hpp"
#include "LogReplay/Exceptions/LogFileNotSupportedException.hpp"
#include "SampleBasedLogKeeper.hpp"
#include <QFileDialog>
#include <QKeyEvent>
#include <QWidget>

namespace Ui {
class LogReplay;
}

class LogReplay : public QWidget {
    Q_OBJECT

public:
    /**
     * @brief LogReplay - Contructor.
     * @param parent - Parent Widget.
     */
    explicit LogReplay(QWidget* parent = nullptr);
    ~LogReplay();

    /**
     * @brief setLogFile - Sets a new log file and parses it.
     * @param fileName - Path to log file
     */
    void setLogFile(QString fileName);

private slots:
    void on_StartButton_clicked();

    void on_stopButton_clicked();

    void on_timeSlider_sliderMoved(int position);

    void on_chooseLogButton_clicked();

    void processLogKeeperMessage(float percentage, CANmessage msg);
    void on_timeSlider_sliderReleased();

    void on_timeSlider_sliderPressed();

    void on_speedInput_editingFinished();
    void keyPressEvent(QKeyEvent* event);

    void on_StartButton_clicked(bool checked);

private:
    Ui::LogReplay* ui;
    LogKeeper* _logKeeper;
    QString _fileName;
signals:
    /**
     * @brief newMessage - Signal that has the current progress percentage of the log processing and the latest CAN Message.
     *                     This signal is emmited when a new CAN Message is ready to be sent out.
     * @param percentage - Progress percentage.
     * @param msg - Latest CAN Message.
     */
    void newMessage(float percentage, CANmessage msg);

    /**
     * @brief timeChanged - Time changed signal. It is emmited when the user manually changes the current time of the log, for example
     * by dragging the time slider.
     */
    void timeChanged();
};

#endif // LOGREPLAY_HPP
