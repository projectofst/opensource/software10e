#include "logparser.hpp"

LogParser::LogParser(QString filename)
{
    this->logFilename = filename;
    this->_times = new QVector<ulong>();
    this->_messages = new QVector<CANmessage>();
    qDebug() << "File to parse: " << filename;
}

QVector<ulong>* LogParser::getTimestamps()
{
    return this->_times;
}

QVector<CANmessage>* LogParser::getMessages()
{
    return this->_messages;
}

QString LogParser::getLogFilename()
{
    return this->logFilename;
}

void LogParser::appendMessage(CANmessage msg)
{
    _messages->append(msg);
}

void LogParser::appendTimestamp(ulong timestamp)
{
    _times->append(timestamp);
}

ulong LogParser::convertTimestamp(QString data)
{
    int conversionFactor = 10 * 10 * 10 * 10 * 10;
    ulong result = data.toFloat() * conversionFactor;
    return result;
}

void LogParser::parse()
{
    QFile file(this->logFilename);
    QTextStream in(&file);
    int lines = 0;
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        throw LogFileNotOpenException(this->logFilename);
        return;
    }
    while (!in.atEnd()) {
        QStringList list = in.readLine().split(",");
        if (list.size() == 8) {
            CANmessage currentMessage; // Default Parsing
            currentMessage.candata.dev_id = list[0].toInt();
            currentMessage.candata.msg_id = list[1].toInt();
            currentMessage.candata.dlc = list[2].toInt();
            currentMessage.candata.data[0] = list[3].toInt();
            currentMessage.candata.data[1] = list[4].toInt();
            currentMessage.candata.data[2] = list[5].toInt();
            currentMessage.candata.data[3] = list[6].toInt();
            currentMessage.timestamp = list[7].toFloat();
            this->_messages->append(currentMessage);
            this->_times->append(list[7].toULong());
        } else {
            throw LogFileNotSupportedException(this->logFilename);
        }
        lines++;
    }
    qDebug() << "Parsed: " << lines << " lines";
}
