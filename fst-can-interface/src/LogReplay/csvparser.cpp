#include "csvparser.hpp"

CSVParser::CSVParser(
    QString filename)
    : LogParser(filename) {

    };

void CSVParser::parse()
{
    CANmessage currentMessage;
    QString logFilename = this->getLogFilename();
    QFile file(logFilename);
    QTextStream in(&file);
    int lines = 0;
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        throw LogFileNotOpenException(logFilename);
        return;
    }
    while (!in.atEnd()) {
        QStringList list = in.readLine().split(",");
        if (list.size() == 14) {
            int SID;
            SID = list[2].toInt();
            currentMessage.candata.dev_id = SID & 0b11111;
            currentMessage.candata.msg_id = SID >> 5;
            currentMessage.candata.dlc = list[4].toInt();
            currentMessage.candata.data[0] = list[5].toInt() + list[6].toInt() * 256;
            currentMessage.candata.data[1] = list[7].toInt() + list[8].toInt() * 256;
            currentMessage.candata.data[2] = list[9].toInt() + list[10].toInt() * 256;
            currentMessage.candata.data[3] = list[11].toInt() + list[12].toInt() * 256;
            currentMessage.timestamp = convertTimestamp(list[0]);
            this->appendMessage(currentMessage);
            this->appendTimestamp(this->convertTimestamp(list[0]));
        } else {
            throw LogFileNotSupportedException(logFilename);
        }
        lines++;
    }
    qDebug() << "Parsed: " << lines << " lines";
}
