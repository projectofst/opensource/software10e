#ifndef CSVPARSER_HPP
#define CSVPARSER_HPP

#include "logparser.hpp"

class CSVParser : public LogParser {
public:
    CSVParser(QString filename);

    virtual void parse() override;
};

#endif // CSVPARSER_HPP
