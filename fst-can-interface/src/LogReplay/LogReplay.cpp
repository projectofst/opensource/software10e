#include "LogReplay.hpp"
#include "ui_LogReplay.h"
#include <QDebug>
#include <iostream>

LogReplay::LogReplay(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::LogReplay)
{
    ui->setupUi(this);
    this->_logKeeper = new SampleBasedLogKeeper();
    connect(this->_logKeeper, &LogKeeper::newMessage, this, &LogReplay::processLogKeeperMessage);
    this->ui->StartButton->setCheckable(true);
    this->ui->StartButton->setText("▶️");
}

LogReplay::~LogReplay()
{
    delete ui;
}

void LogReplay::setLogFile(QString fileName)
{
    if (this->_fileName == fileName) {
        return;
    }
    this->_fileName = fileName;
    try {
        this->_logKeeper->setFileName(fileName);
        this->_logKeeper->parseLog();
        this->ui->StartButton->setEnabled(true);
        this->ui->messageLabel->setText("Log File Parsed Successfully.");

    } catch (LogException& exception) {
        this->ui->messageLabel->setText(exception.toString());
        this->ui->StartButton->setEnabled(false);
    }
}

void LogReplay::on_StartButton_clicked()
{
}

void LogReplay::on_stopButton_clicked()
{
}

void LogReplay::on_timeSlider_sliderMoved(int position)
{

    this->_logKeeper->setNewProgress(((float)position / this->ui->timeSlider->maximum()) * 100);
}

void LogReplay::on_chooseLogButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open JsonFile"), QDir::currentPath(), tr("Log Files (*.log *.txt *.csv)"));
    this->setLogFile(fileName);
    this->ui->logFileName->setText(fileName);
}

void LogReplay::processLogKeeperMessage(float percentage, CANmessage msg)
{
    this->ui->timeSlider->setSliderPosition(percentage * this->ui->timeSlider->maximum());

    emit newMessage(percentage, msg);
}

void LogReplay::on_timeSlider_sliderReleased()
{
    this->_logKeeper->startLogReplay();
    emit timeChanged();
}

void LogReplay::on_timeSlider_sliderPressed()
{
    this->_logKeeper->stopLogReplay();
}

void LogReplay::on_speedInput_editingFinished()
{
    QString speedStr = this->ui->speedInput->text();
    float speed;
    if (speedStr == "0" || speedStr == "") {
        speed = 1.0;
    } else {
        speed = speedStr.toFloat();
    }
    this->_logKeeper->setSpeed(speed);
}

void LogReplay::keyPressEvent(QKeyEvent* event)
{
    switch (event->key()) {
    case Qt::UpArrow:
        this->_logKeeper->increaseSpeed();
        this->ui->speedInput->setText(QString::number(this->_logKeeper->getSpeed()));
        break;
    case Qt::DownArrow:
        this->_logKeeper->decreaseSpeed();
        this->ui->speedInput->setText(QString::number(this->_logKeeper->getSpeed()));
        break;
    case Qt::LeftArrow:
        this->ui->timeSlider->setSliderPosition(this->ui->timeSlider->value() - 1);
        break;
    case Qt::RightArrow:
        this->ui->timeSlider->setSliderPosition(this->ui->timeSlider->value() + 1);
        break;
    default:
        return;
    }
}

void LogReplay::on_StartButton_clicked(bool checked)
{
    qDebug() << checked;
    if (checked) {
        if (this->_logKeeper->getFileName() == "") {
            return;
        }
        this->_logKeeper->startLogReplay();
        this->ui->StartButton->setText("▋▋");
    } else {
        this->_logKeeper->stopLogReplay();
        this->ui->StartButton->setText("▶️");
    }
}
