#include "SampleBasedLogKeeper.hpp"
#include <QDebug>

SampleBasedLogKeeper::SampleBasedLogKeeper(QString logFileName)
    : LogKeeper(logFileName)
{
    this->_sampleNumber = this->_messages->size();
}
SampleBasedLogKeeper::SampleBasedLogKeeper()
{
    this->_sampleNumber = 0;
}

void SampleBasedLogKeeper::recalculateIndex(float percentage)
{
    LogKeeper::setNewCurrentIndex((percentage / 100) * (int(this->_sampleNumber)));
}
void SampleBasedLogKeeper::recalculateProgress()
{
    if (this->_sampleNumber != 0) {
        this->setNewProgress(this->getCurrentIndex() / this->_sampleNumber);
    }
}

float SampleBasedLogKeeper::calculatePercentage()
{
    if (this->_sampleNumber != 0) {
        return this->getCurrentIndex() / float(this->_sampleNumber);
    }
    return 0;
}

void SampleBasedLogKeeper::setNewProgress(float percentage)
{
    this->setProgress(percentage);
    this->recalculateIndex(percentage);
}

void SampleBasedLogKeeper::setSampleNumber(int sampleNumber)
{
    this->_sampleNumber = sampleNumber;
}

void SampleBasedLogKeeper::parseLog()
{
    LogKeeper::parseLog();
    setSampleNumber(this->_messages->size());
}
