#include "LogFileNotSupportedException.hpp"

LogFileNotSupportedException::LogFileNotSupportedException(QString logFileName)
{
    this->_logFileName = logFileName;
}

QString LogFileNotSupportedException::toString()
{
    return "LogFileNotSupportedException: " + this->_logFileName;
}
