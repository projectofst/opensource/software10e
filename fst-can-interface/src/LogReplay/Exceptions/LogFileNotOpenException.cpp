#include "LogFileNotOpenException.hpp"

LogFileNotOpenException::LogFileNotOpenException(QString fileName)
{
    this->_fileName = fileName;
}
QString LogFileNotOpenException::toString()
{
    return "LogFileNotOpenException: Could not open " + this->_fileName + ".";
}
