#ifndef LOGFILENOTOPENEXCEPTION_HPP
#define LOGFILENOTOPENEXCEPTION_HPP

#include "LogReplay/Exceptions/LogException.hpp"

class LogFileNotOpenException : public LogException {
    QString _fileName;

public:
    LogFileNotOpenException(QString fileName);
    virtual QString toString();
};

#endif // LOGFILENOTOPENEXCEPTION_HPP
