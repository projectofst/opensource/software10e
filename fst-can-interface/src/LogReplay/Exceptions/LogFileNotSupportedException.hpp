#ifndef LOGFILENOTSUPPORTEDEXCEPTION_HPP
#define LOGFILENOTSUPPORTEDEXCEPTION_HPP

#include "LogReplay/Exceptions/LogException.hpp"

class LogFileNotSupportedException : public LogException {
private:
    QString _logFileName;

public:
    LogFileNotSupportedException(QString logFileName);

    virtual QString toString();
};

#endif // LOGFILENOTSUPPORTEDEXCEPTION_HPP
