#ifndef LOGEXCEPTION_HPP
#define LOGEXCEPTION_HPP

#include <QString>

class LogException {
public:
    LogException();

    virtual QString toString() = 0;
};

#endif // LOGEXCEPTION_HPP
