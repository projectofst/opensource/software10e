#include "LogKeeper.hpp"

#include <QDebug>
LogKeeper::LogKeeper(QString logFileName)
{
    this->_logFileName = logFileName;
    this->init();
}

LogKeeper::LogKeeper()
{
    this->_logFileName = "";
    this->init();
}

LogKeeper::~LogKeeper()
{
    delete this->_times;
    delete this->_timer;
    delete this->_messages;
}

void LogKeeper::init()
{
    this->_times = new QVector<ulong>();
    this->_messages = new QVector<CANmessage>();
    this->_currentDataIndex = 0;
    this->_totalTime = 0;
    this->_progress = 0;
    this->_timer = new QTimer();
    connect(this->_timer, &QTimer::timeout, this, &LogKeeper::sendNewMessage);
    this->_speed = 1;
}

void LogKeeper::startLogReplay()
{
    sendNewMessage();
}

void LogKeeper::stopLogReplay()
{
    this->_timer->stop();
}

void LogKeeper::parseLog()
{
    LogParser* parser;
    if (_logFileName.contains(".csv")) {
        parser = new CSVParser(this->_logFileName);
    } else {
        parser = new LogParser(this->_logFileName);
    }
    parser->parse();
    this->_messages = parser->getMessages();
    this->_times = parser->getTimestamps();
    // Avoid leaks

    this->_totalTime = this->_times->last() - this->_times->first();
}

void LogKeeper::sendNewMessage()
{
    if (this->_currentDataIndex >= (ulong)this->_messages->size()) {
        this->_timer->stop();
        return;
    }
    CANmessage msg = this->_messages->at(this->_currentDataIndex);
    ulong waitTime = this->calculateNextWaitingTime();
    this->_currentDataIndex++;
    emit newMessage(this->calculatePercentage(), msg);
    this->wait(waitTime);
}
ulong LogKeeper::calculateNextWaitingTime()
{

    if (this->_currentDataIndex >= (ulong)this->_messages->size() - 1) {
        this->_timer->stop();
        return 0;
    }
    ulong currentTime = this->_times->at(this->_currentDataIndex);
    ulong nextTime = this->_times->at(this->_currentDataIndex + 1);
    return nextTime - currentTime;
}
void LogKeeper::wait(ulong waitTime)
{
    this->_timer->start(ulong(waitTime / this->_speed));
}

ulong LogKeeper::getTotalTime()
{
    return this->_totalTime;
}

void LogKeeper::setProgress(float percentage)
{
    this->_progress = percentage;
}

ulong LogKeeper::getCurrentIndex()
{
    return this->_currentDataIndex;
}

void LogKeeper::setFileName(QString fileName)
{
    this->_logFileName = fileName;
}

QString LogKeeper::getFileName()
{
    return this->_logFileName;
}

void LogKeeper::setNewCurrentIndex(ulong newIndex)
{
    this->_currentDataIndex = newIndex;
}

void LogKeeper::setSpeed(float speedValue)
{
    this->_speed = speedValue;
}

float LogKeeper::getSpeed()
{
    return this->_speed;
}

void LogKeeper::increaseSpeed()
{
    this->_speed = this->_speed * 2;
}

void LogKeeper::decreaseSpeed()
{
    this->_speed = this->_speed / 2;
}
