#include "WindowMain.hpp"
#include "ui_MainWindow.h"

#include "about.h"
#include "amsdetails.h"
#include "logconverter.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Register SerialPort -> Widgets
    connect(ui->ConsWidget, SIGNAL(broadcast_arrival_of_CAN_message(CANdata)), ui->LogWidget, SLOT(process_CAN_message(CANdata)));
    connect(ui->ConsWidget, SIGNAL(broadcast_arrival_of_CAN_message(CANdata)), ui->GenWidget, SLOT(process_CAN_message(CANdata)));
    connect(ui->ConsWidget, SIGNAL(broadcast_arrival_of_CAN_message(CANdata)), ui->AMSWidget, SLOT(process_CAN_message(CANdata)));
    connect(ui->ConsWidget, SIGNAL(broadcast_arrival_of_CAN_message(CANdata)), ui->InvertersWidget, SLOT(process_CAN_message(CANdata)));
    connect(ui->ConsWidget, SIGNAL(broadcast_arrival_of_CAN_message(CANdata)), ui->SensorsWidget, SLOT(process_CAN_message(CANdata)));

    // Register Widgets -> SerialPort
    connect(ui->GenWidget, SIGNAL(send_to_CAN(CANdata)), ui->ConsWidget, SLOT(send_to_CAN(CANdata)));
    connect(ui->AMSWidget, SIGNAL(send_to_CAN(CANdata)), ui->ConsWidget, SLOT(send_to_CAN(CANdata)));
    connect(ui->InvertersWidget, SIGNAL(send_to_CAN(CANdata)), ui->ConsWidget, SLOT(send_to_CAN(CANdata)));
    connect(ui->TestBenchWidget, SIGNAL(send_to_CAN(CANdata)), ui->ConsWidget, SLOT(send_to_CAN(CANdata)));
    // connect(ui->BatWidget, SIGNAL(send_to_CAN(CANdata)), ui->ConsWidget, SLOT(send_to_CAN(CANdata)));
    // connect(ui->AMKWidget, SIGNAL(send_to_CAN(CANdata)), ui->ConsWidget, SLOT(send_to_CAN(CANdata)));
}

MainWindow::~MainWindow(void)
{
    delete ui;
}

void MainWindow::on_actionAbout_triggered()
{
    About* about = new About();
    about->show();
    about->activateWindow();
    about->raise();
}

void MainWindow::on_actionOpen_Log_triggered()
{
    LogConverter* log_convert = new LogConverter();
    log_convert->show();
    log_convert->activateWindow();
    log_convert->raise();
}
