#ifndef PLOTUI_HPP
#define PLOTUI_HPP

#include "UiWindow.hpp"
#include <PlotUi/PlotLib.hpp>
#include <PlotUi/PlotObjTypes.hpp>
#include <PlotUi/ioengine.hpp>
#include <QList>
#include <QString>
#include <QWidget>
#include <QtCharts>
#include <memory>
#include <stdint.h>

namespace Ui {
class PlotUI;
}

class PlotUI : public UiWindow {
    Q_OBJECT

public:
    explicit PlotUI(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~PlotUI();

    void process_CAN_message(CANmessage msg) override;
    void updateFCP(FCPCom* fcp) override;
    void printToConsole(QString msg);
    bool signalExists(CANmessage msg);
    void registerSignal(CANmessage msg);
    void refreshMSGID();
    void refreshDEVID();
    void setLengthStart(int);
    void parseSignals(FCPCom* fcp);
    void checkPlotCfg(uint16_t dev_id, uint16_t msg_id, int ofst, int len, int name_length);
    bool plotCfgIsValid(uint16_t dev_id, uint16_t msg_id, int ofst, int len);
    bool emptyFields();
    bool checkSID(uint16_t dev_id, uint16_t msg_id);
    bool checkLenghtOffset(int len, int ofst);
    void addNewPlot();
    void addImportedPlot(std::shared_ptr<PlotBaseObj> importedPlot);
    std::shared_ptr<PlotBaseObj> createChildPlot(std::shared_ptr<PlotBaseObj> parentPlot,
        QString plotName,
        QString plotOp);
    void addPlotToList(std::shared_ptr<PlotBaseObj> plot);
    QString checkRepeatedName(QString name, int n);
    void endPlotSetup();
    void refreshPlots();
    void removePlots();
    void killChildren(std::shared_ptr<PlotBaseObj> plot);
    void refreshWidget(int Id, QList<double> values);
    void toggleAdvancedCANSettings();
    void setSignalParameters(QString signalName);
    std::shared_ptr<PlotBaseObj> getPlotObjByName(QString name);
    std::shared_ptr<PlotBaseObj> getPlotObj(int Id);
    int getPlotArrayId(int Id);
    int getPlotWidgetListId(int Id);
    void refreshPlotInfo(QList<double> values);
    QString getCurrentColorOption(QColor color);
    void resetPlotZoom();
    void enableZoomTools(bool choice);
    void setSampleRate();
    void checkExportFields();
private slots:

    void on_OpenConsole_clicked();

    void on_leftSidebarButton_clicked();

    void on_rightSidebarButton_clicked();

    void on_addPlotButton_clicked();

    void on_cancelPlotSetupButton_clicked();
    void on_plotList_itemClicked(QListWidgetItem* item);

    void on_plotColorInput_currentIndexChanged(const QString& arg1);

    void on_removePlotButton_clicked();

    void on_freezeBtn_clicked();

    void on_zoomButton_clicked();

    void on_gotoBtn_clicked();

    void on_resetZoomBtn_clicked();

    void on_fixLimits_clicked();

    void on_scrollCheckbox_stateChanged(int arg1);

    void on_exportButton_clicked();

    void on_cancelExportBtn_clicked();

    void on_exportPathBtn_clicked();

    void on_exportTypeInput_currentTextChanged(const QString& arg1);

    void on_exportActionBtn_clicked();

    void on_importButton_clicked();

    void on_changeColorInput_currentIndexChanged(const QString& arg1);

    void on_mathButton_clicked();

    void on_cancelMathButton_clicked();

    void on_opTypeInput_currentTextChanged(const QString& arg1);

    void on_singleOpInput_currentTextChanged(const QString& arg1);

    void on_themesPicker_currentTextChanged(const QString& arg1);

    void on_createMathPlotBtn_clicked();

    void on_reportButton_clicked();

private:
    Ui::PlotUI* ui;
    QLineSeries* series;
    QLineSeries* new_line_series;
    QChart* plot;
    QValueAxis* axisX;
    QValueAxis* axisY;
    int sampleRate;
    QQueue<uint32_t> timestampQueue;
    double backupRangeX;
    double backupRangeMaxY;
    double backupRangeMinY;
    double fixedRangeMaxY;
    double fixedRangeMinX;
    double fixedRangeMinY;
    bool fixedRangesSet;
    bool setup_plot_active;
    bool setupRerun;
    bool signalRegistered;
    uint16_t devId;
    uint16_t messageId;
    int length;
    int start;
    int plotNameSize;
    QString plotName;
    QString muxSignal;
    QColor color;
    bool colorSetupFinished;
    QList<std::shared_ptr<PlotBaseObj>> plotObjects;
    QList<std::shared_ptr<PlotCfg>> PlotConfigurations;
    QTimer* timer;
    bool freezeState;
    bool fixedLimits;
    bool scrollEnabled;
    int currentSelectedPlot;
    int currentColorIndex;
    QListWidgetItem* currentSelectedItem;
    QMap<QString, QColor> plotColors;
    QList<QString> matlabColorOrder;
    QMap<QString, std::tuple<CanSignal*, Device*, Message*>> SignalsAndDevicesAndMessages;
    QString exportPath;
    QString exportFileType;
    QString exportName;
    // Theme System
    QMap<QString, QMap<QString, QColor>> chartThemes;
    // Operations DB
    QMap<QString, QString> simpleMathDB; // One Plot
    QMap<QString, QString> complexMathDB; // Two Plots
};

#endif // PLOTUI_HPP
