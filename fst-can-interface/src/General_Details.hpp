#ifndef GENERALDETAILS_H
#define GENERALDETAILS_H

#include <QDialog>
#include <QTimer>

#include "General_Details_PotProgress.hpp"

#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/Devices/DASH_CAN.h"
#include "can-ids/Devices/DCU_CAN.h"
#include "can-ids/Devices/INTERFACE_CAN.h"
#include "can-ids/Devices/STEER_CAN.h"
#include "can-ids/Devices/TE_CAN.h"
#include "fcpcom.hpp"
#include "message.hpp"
#include <stdint.h>

namespace Ui {
class GeneralDetails;
}

class GeneralDetails : public QDialog {
    Q_OBJECT

public:
    explicit GeneralDetails(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~GeneralDetails();
    void updateTorqueEncoderDetails(TE_CAN_Data* data);
    void UpdateTE_STATUS(unsigned int TE_Message);
    void UpdateSupplies(CANmessage msg);
    void UpdateDash(unsigned int dash);
    void updateDashLEDs(DASH_CAN_Data data);
    void updateRTDSequence(COMMON_MSG_RTD_ON data);
    void clear();

    void clear_rtd_steps();

private slots:
    void on_update_limits_clicked();
    void te_request_async();
    void on_send_hard_braking_lim_clicked();

private:
    Ui::GeneralDetails* ui;
    uint16_t** limits;
    void update_te_limits(TE_CAN_Data* data);
    void te_request_limits();
    uint16_t limit;
    uint16_t sensor;
    QTimer te_request_limit_timer;
    void update_progress_bars(TE_CAN_Data*);
    void update_progress_bar(int, uint16_t);
    QString check_te_limits(int, int);
    QVector<PotProgress*> bars;

    FCPCom* _fcp;

signals:
    void send_CAN(CANmessage);
};

#endif // GENERALDETAILS_H
