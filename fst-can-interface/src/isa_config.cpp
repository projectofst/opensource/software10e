#include "isa_config.hpp"
#include "COM_SerialPort.hpp"
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"
#include "ui_isa_config.h"
#include <QWidget>

isa_config::isa_config(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::isa_config)
{
    ui->setupUi(this);
}

isa_config::~isa_config()
{
    delete ui;
}

void isa_config::on_close_clicked()
{
    this->hide();
    return;
}

void isa_config::send_to_isa(int message_sid, int DB0, int DB1, int DB2, int DB3, int DB4, int DB5, int DB6, int DB7)
{
    CANmessage msg;
    msg.candata.sid = message_sid;
    msg.candata.dlc = 8;
    msg.candata.data[0] = (DB1 << 8) + DB0;
    msg.candata.data[1] = (DB3 << 8) + DB2;
    msg.candata.data[2] = (DB5 << 8) + DB4;
    msg.candata.data[3] = (DB7 << 8) + DB6;
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void isa_config::on_current_enable_clicked()
{
    int value = ui->current_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x20, 0x72, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_current_disable_clicked()
{
    int value = ui->current_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x20, 0x70, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_V1_enable_clicked()
{
    int value = ui->V1_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x21, 0x72, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_V1_disable_clicked()
{
    int value = ui->V1_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x21, 0x70, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_V2_enable_clicked()
{
    int value = ui->V2_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x22, 0x72, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_V2_disable_clicked()
{
    int value = ui->V2_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x22, 0x70, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_V3_enable_clicked()
{
    int value = ui->V3_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x23, 0x72, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_V3_disable_clicked()
{
    int value = ui->V3_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x23, 0x70, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_temp_enable_clicked()
{
    int value = ui->temp_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x24, 0x72, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_temp_disable_clicked()
{
    int value = ui->temp_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x24, 0x70, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_power_enable_clicked()
{
    int value = ui->power_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x25, 0x72, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_power_disable_clicked()
{
    int value = ui->power_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x25, 0x70, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_current_counter_enable_clicked()
{
    int value = ui->current_counter_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x26, 0x72, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_current_counter_disable_clicked()
{
    int value = ui->current_counter_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x26, 0x70, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_energy_counte_enable_clicked()
{
    int value = ui->energy_counter_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x27, 0x72, DB2, DB3, 0, 0, 0, 0);
}

void isa_config::on_energy_counter_disable_clicked()
{
    int value = ui->current_counter_line_edit->text().toInt();
    int DB2 = (value >> 8);
    int DB3 = (value & 0x00FF);
    send_to_isa(0x3E, 0x27, 0x70, DB2, DB3, 0, 0, 0, 0);
}
