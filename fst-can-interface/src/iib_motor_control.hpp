#ifndef IIB_MOTOR_CONTROL_HPP
#define IIB_MOTOR_CONTROL_HPP

#include "SingleInverter.hpp"
#include <QMap>
#include <QWidget>

#ifndef MOTORS
#define MOTORS
#define FRONTLEFT 0
#define FRONTRIGHT 1
#define REARLEFT 2
#define REARRIGHT 3
#endif
namespace Ui {
class iib_motor_control;
}

class iib_motor_control : public QWidget {
    Q_OBJECT

public:
    explicit iib_motor_control(QWidget* parent = nullptr);
    ~iib_motor_control();
    void updateInformation(QPair<QString, QMap<QString, double>> information);

    void clear();

private:
    QMap<int, SingleInverter*> _singleInverters;
    Ui::iib_motor_control* ui;
};

#endif // IIB_MOTOR_CONTROL_HPP
