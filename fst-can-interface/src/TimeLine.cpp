#include "TimeLine.hpp"
#include "ui_TimeLine.h"

TimeLine::TimeLine(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::TimeLine)
{
    ui->setupUi(this);
}

TimeLine::~TimeLine()
{
    delete ui;
}
