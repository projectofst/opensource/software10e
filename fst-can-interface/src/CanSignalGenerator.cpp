#include "CanSignalGenerator.hpp"

CanSignalGenerator::CanSignalGenerator(QObject* parent,
    QString expression,
    int start,
    int end,
    double step,
    int delay,
    FCPCom* fcp,
    QString deviceName,
    QString messageName,
    QString sigName)
    : SignalGenerator(parent, expression, start, end, step, delay)
{
    this->_fcp = fcp;
    this->_sigName = sigName;
    this->_deviceName = deviceName;
    this->_messageName = messageName;
    connect(this, &CanSignalGenerator::newValue, this, &CanSignalGenerator::processNewValue);
    try {
        if (this->_fcp != nullptr) {
            this->checkFcpInfo();
        }
    } catch (JsonException& e) {
        return;
    }
}

void CanSignalGenerator::processNewValue(double x, double y)
{
    try {
        checkFcpInfo();
        this->map.insert(this->_sigName, y);
        CANmessage msg = this->_fcp->encodeMessage(this->_deviceName, this->_messageName, this->map);
        msg.timestamp = x;
        emit newSample(msg);
    } catch (JsonException& e) {
        qDebug() << e.toString();
    }
}

void CanSignalGenerator::setDeviceName(QString newDeviceName)
{
    this->_deviceName = newDeviceName;
}

void CanSignalGenerator::setSignalName(QString newSigName)
{
    this->_sigName = newSigName;
}

void CanSignalGenerator::setMessageName(QString newDevName)
{
    this->_messageName = newDevName;
}

void CanSignalGenerator::checkFcpInfo()
{
    this->map.clear();
    Device* dev = this->_fcp->getDevice(this->_deviceName);
    Message* msg = dev->getMessage(this->_messageName);
    QList<CanSignal*> sigs = msg->getSignals();

    foreach (CanSignal* sig, sigs) {
        this->map.insert(sig->getName(), 0);
    }
}
