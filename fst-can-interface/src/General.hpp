#ifndef GENERAL_H
#define GENERAL_H

#include <QMap>
#include <QWidget>

#include "General_Details.hpp"
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"
#include "ledform.hpp"
//#include "SQL_CANID.hpp"
#include "Exceptions/helper.hpp"
#include "WindowMain.hpp"
#include "fcpcom.hpp"

namespace Ui {
class General;
}

class General : public UiWindow {
    Q_OBJECT

public:
    explicit General(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    ~General() override;
    void updateDCUGeneralUI();
    void updateTEGeneralUI();
    void updateDashGeneralUI();
    void updateIIBUI();
    void updateBMSUI();
    void dcu_toggle(DCU_TOGGLE code);
    void debug_toggle(DEBUG_TOGGLE code);
    void updateFCP(FCPCom* fcp) override;
    QString getTitle() override { return "General"; };

private slots:
    void process_CAN_message(CANmessage message) override;
    void on_dcu_toggle_dcdc_drs_clicked();
    void on_debug_mode_te_clicked();
    void on_debug_mode_dcu_clicked();
    void on_debug_mode_dash_clicked();
    void set_pedal_thresholds(INTERFACE_MSG_PEDAL_THRESHOLDS select);
    void on_clearButton_clicked();
    void disable_brake_thresholds();
    void set_steer_threshold(/*INTERFACE_MSG_STEER_THRESHOLDS select*/);
    void on_steer_set_left_clicked();
    void on_steer_set_middle_clicked();
    void on_steer_set_right_clicked();
    void on_start_log_button_clicked();
    void on_details_clicked();
    void on_dcu_toggle_pumps_clicked();
    void retransmit_can(CANmessage msg);
    void on_brake_upper_clicked();
    void on_brake_lower_clicked();
    void on_accel_upper_clicked();
    void on_accel_lower_clicked();
    void updateSteer(CANmessage message);
    void updateFuses(CANmessage message);
    void clear_all();

private:
    Ui::General* ui;
    // SQL_CANID *teste;
    GeneralDetails* general_details;
    QMap<unsigned int, LEDForm*> forms;
    QMap<unsigned int, bool> active_modules;
    QTimer* clear_timer;
    int clear_flag;
    FCPCom* _fcp;
    Helper* logger;

private slots:
    void update();
};

#endif // GENERAL_H
