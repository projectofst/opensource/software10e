#include "single_lap.hpp"
#include "ui_single_lap.h"

Single_lap::Single_lap(QWidget* parent, int new_lap_number, QString lap_id, qint64 new_time, QString new_string_time, int new_cones)
    : QWidget(parent)
    , ui(new Ui::Single_lap)
{
    ui->setupUi(this);
    ui->time->setText(new_string_time);
    ui->cones->setText(QString::number(new_cones));
    ui->lap_number->setText(lap_id);

    DNF_flag = 0;
    string_time = new_string_time;
    lap_number = new_lap_number;
    time = new_time;
    cones = new_cones;
}

Single_lap::~Single_lap()
{
    delete ui;
}

void Single_lap::on_delete_button_clicked()
{
    return;
}

int Single_lap::get_lap_number()
{
    return lap_number;
}

int Single_lap::get_cones()
{
    return cones;
}

qint64 Single_lap::get_time()
{
    return time;
}

QString Single_lap::get_time_string()
{
    return string_time;
}

void Single_lap::on_cones_textChanged(const QString& arg1)
{
    cones = arg1.toInt();
}

void Single_lap::set_new_lap_number(int new_lap_number)
{
    lap_number = new_lap_number;
    ui->lap_number->setText("Lap #" + QString::number(new_lap_number));
}

QString Single_lap::get_transform_time()
{
    char pretty_time[32];
    QList<qint64> times;

    times = format_time(time);
    sprintf(pretty_time, "%02lld:%02lld.%03lld", times[0], times[1], times[2]);
    QString x = pretty_time; /*macacada*/
    return x;
}

QList<qint64> Single_lap::format_time(qint64 time)
{

    QList<qint64> times;
    qint64 miliseconds, seconds, minutes;

    miliseconds = time % 1000;
    time = qint64(time / 1000);
    seconds = time % 60;
    time = qint64(time / 60);
    minutes = time;

    times.append(minutes);
    times.append(seconds);
    times.append(miliseconds);

    return times;
}

void Single_lap::set_new_lap_time(qint64 new_time)
{
    time = new_time;
    return;
}

void Single_lap::press_del()
{
    emit ui->delete_button->clicked();
}

void Single_lap::set_DNF_lap()
{
    DNF_flag = 1;
    ui->label->deleteLater();
    ui->label_3->deleteLater();
    ui->time->deleteLater();
    ui->cones->deleteLater();
    ui->horizontalLayout_2->deleteLater();
    ui->horizontalLayout_3->deleteLater();
    ui->verticalLayout->deleteLater();
    ui->lap_number->setText("DNF");
}

int Single_lap::get_DNF_flag()
{
    return DNF_flag;
}
