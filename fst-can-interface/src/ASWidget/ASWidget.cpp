#include "ASWidget.hpp"
#include "ui_ASWidget.h"

ASWidget::ASWidget(QWidget* parent, FCPCom* fcp, Helper* log, ConfigManager*)
    : UiWindow(parent, fcp)
    , ui(new Ui::ASWidget)
{
    ui->setupUi(this);

    this->logger = log;

    QList<QString> values = { "te_press_f", "te_press_r" };
    MultipleInputTemplateWidget* TEPressWidget = new MultipleInputTemplateWidget(this, "te_press", "TE Press", values);
    TEPressWidget->setButtonText("Send");
    this->ui->DebugInfoLayout->addWidget(TEPressWidget); /*It is now the layout's responsability to free this memory, which is done  in
                                                           delelte ui on the deconstructor*/
    connect(TEPressWidget, &MultipleInputTemplateWidget::emitInputValues, this, &ASWidget::handleWidgetMessages);
    values.clear();

    values = { "acc", "brake" };
    MultipleInputTemplateWidget* pedalSetpoint = new MultipleInputTemplateWidget(this, "pedal_setpoint", "Pedal Setpoint", values);
    pedalSetpoint->setButtonText("Send");
    this->ui->DebugInfoLayout->addWidget(pedalSetpoint);
    connect(pedalSetpoint, &MultipleInputTemplateWidget::emitInputValues, this, &ASWidget::handleWidgetMessages);

    values.clear();

    values = { "rpm" };
    MultipleInputTemplateWidget* velocitySetpoint = new MultipleInputTemplateWidget(this, "velocity_setpoint", "Velocity Setpoint", values);
    this->ui->DebugInfoLayout->addWidget(velocitySetpoint);
    velocitySetpoint->setButtonText("Send");
    connect(velocitySetpoint, &MultipleInputTemplateWidget::emitInputValues, this, &ASWidget::handleWidgetMessages);

    values.clear();
    values = { "status" };
    MultipleInputTemplateWidget* staStatus = new MultipleInputTemplateWidget(this, "sta_status", "Steering Actuator Status", values);
    this->ui->DebugInfoLayout->addWidget(staStatus);
    staStatus->setButtonText("Send");
    connect(staStatus, &MultipleInputTemplateWidget::emitInputValues, this, &ASWidget::handleWidgetMessages);

    this->ui->steeringSetpointSlider->setMaximum(10000);
    this->ui->steeringSetpointSlider->setMinimum(-10000);
    this->ui->steeringSetpointSlider->setSliderPosition(0);
    connect(this->ui->steeringSetpointSlider, &QSlider::valueChanged, this, &ASWidget::updateSliderValue);

    // ENUM
    ebsStates.insert(0, "EBS Unavailable");
    ebsStates.insert(1, "EBS Armed");
    ebsStates.insert(2, "EBS Activated");

    asStates.insert(0, "AS Off");
    asStates.insert(1, "AS Ready");
    asStates.insert(2, "AS Driving");
    asStates.insert(3, "AS Finished");
    asStates.insert(4, "AS Emergency");
    asStates.insert(5, "AS Manual");

    asMissions.insert(0, "Acceleration");
    asMissions.insert(1, "Skidpad");
    asMissions.insert(2, "Autocross");
    asMissions.insert(3, "Trackdrive");
    asMissions.insert(4, "EBS Test");
    asMissions.insert(5, "Inspection");
    asMissions.insert(6, "Manual Driving");

    this->ui->FCPDisplayWidget->setVisible(false);
    this->setupEBSFcpDisplay();

    // this->ui->brakeProgressBar->setStyleSheet("QProgressBar::chunk {background-color: red;}");

    // this->ui->accProgressBar->setStyleSheet("QProgressBar::chunk {background-color: green;}");

    this->ui->brakeProgressBar->setStyleSheet("QProgressBar{border: 2px solid grey;border-radius: 5px;text-align: center} QProgressBar::chunk {background-color: red;}");
    this->ui->accProgressBar->setStyleSheet("QProgressBar{border: 2px solid grey;border-radius: 5px;text-align: center} QProgressBar::chunk {background-color: green;}");
}

ASWidget::~ASWidget()
{
    this->clearFCPDisplay();
    delete ui;
}

void ASWidget::on_sendASStateButton_clicked()
{
    int asStateInputIndex = this->ui->asStateInput->currentIndex();

    /*Missions are described in enum in "../shared/as_lib/common.h". They are ordered in the combo box, with the first place
    being empty, thus the  -1 */
    if (asStateInputIndex > 0) {
        try {
            CANmessage msg = this->_fcp->encodeSendCommand("interface", "ebs", "ebs_as_state", asStateInputIndex - 1, 0, 0);
            emit send_to_CAN(*COM::encodeToByteArray<CANmessage>(msg));
        } catch (JsonException& e) {
            logger->handle_exception(e.toString());
            this->ui->exceptionOutput->setText(e.toString());
        }
    }
}

void ASWidget::on_sendAsMissionButton_clicked()
{
    int asMissionInputIndex = this->ui->asMissionInput->currentIndex();

    if (asMissionInputIndex > 0) {
        try {
            CANmessage msg = this->_fcp->encodeSendCommand("interface", "ebs", "ebs_as_mission", asMissionInputIndex - 1, 0, 0);
            emit send_to_CAN(*COM::encodeToByteArray<CANmessage>(msg));
        } catch (JsonException& e) {
            logger->handle_exception(e.toString());
            this->ui->exceptionOutput->setText(e.toString());
        }
    }
}

void ASWidget::process_CAN_message(CANmessage message)
{
    try {
        auto messageMap = this->_fcp->decodeMessage(message);

        QString messageName = messageMap.first;
        if (messageName == "send_cmd") {

            QString destName = this->_fcp->getDevice(messageMap.second["dst"])->getName();

            QString commandName = this->_fcp->getCommand(destName, messageMap.second["id"])->getName();

            if (destName == "iib" && commandName == "pedal_setpoint") {
                uint64_t acceleratorValue = messageMap.second["arg1"];
                uint64_t brakeValue = messageMap.second["arg2"];
                this->updatePedalProgressBars(acceleratorValue, brakeValue);
            }
        } else if (messageName == "ebs_status") {
            uint64_t ebsStatus = this->_fcp->decodeUint64FromSignal(message, "ebs_ebs_state");
            this->ui->currentEBSState->setText(this->ebsStates.value(ebsStatus));

            uint64_t asState = this->_fcp->decodeUint64FromSignal(message, "ebs_as_state");
            this->ui->currentASState->setText(this->asStates.value(asState));

            uint64_t asMission = this->_fcp->decodeUint64FromSignal(message, "ebs_as_mission");
            this->ui->currentASMission->setText(this->asMissions.value(asMission));

            parseEBSFcpDisplayMessage(message);
        } else if (messageName == "DV_driving_dynamics_1") {
            auto values = this->_fcp->decodeMessage(message);
            double steeringInput = values.second.value("steering_angle_target");
            if (steeringInput > 0) {
                this->ui->SteeringInputNeg->setValue(0);
                this->ui->steeringInputPos->setValue(steeringInput);
            } else {
                this->ui->steeringInputPos->setValue(0);
                this->ui->SteeringInputNeg->setValue(-steeringInput);
            }
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        this->ui->exceptionOutput->setText(e.toString());
    }
}

void ASWidget::updateFCP(FCPCom* fcp)
{
    this->_fcp = fcp;
    this->setupEBSFcpDisplay();
}

void ASWidget::handleWidgetMessages(QString name, QMap<QString, QString> values)
{
    try {
        QString senderDeviceName;
        CANmessage message;

        /*messages*/
        if (name == "te_press") {
            senderDeviceName = "te";
        } else if (name == "sta_status") {
            senderDeviceName = "sta";
        }
        /*commands*/
        else {
            QString destinationName = "";
            QString commandName = "";
            uint64_t arg1 = 0;
            uint64_t arg2 = 0;
            uint64_t arg3 = 0;

            if (name == "pedal_setpoint") {
                commandName = name;
                arg1 = values["acc"].toULong();
                arg2 = values["brake"].toULong();
                destinationName = "iib";
            } else if (name == "velocity_setpoint") {
                commandName = name;
                arg1 = values["rpm"].toULong();
                destinationName = "iib";
            }
            message = this->_fcp->encodeSendCommand("interface", destinationName, commandName, arg1, arg2, arg3);

            emit send_to_CAN(*COM::encodeToByteArray<CANmessage>(message));
            return;
        }

        QMap<QString, double> numericalValues;
        for (auto it = values.begin(); it != values.end(); it++) {
            numericalValues.insert(it.key(), it.value().toDouble());
        }

        message = this->_fcp->encodeMessage(senderDeviceName, name, numericalValues);
        this->send_to_CAN(*COM::encodeToByteArray<CANmessage>(message));
        return;

    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        this->ui->exceptionOutput->setText(e.toString());
    }
}

void ASWidget::updateSliderValue(int value)
{
    this->ui->steeringSetpointValue->setText(QString::number(((float)value) / 10000));
}

void ASWidget::on_steeringSetpointSlider_sliderMoved(int)
{
    if (this->ui->sendOnBarSlide->isChecked()) {
        /*TODO: Encode steering setpoint message. Message not yet created*/
    }
}

void ASWidget::on_resGoSendButton_clicked()
{
    try {
        QMap<QString, double> values;
        values.insert("res_go", 1);
        values.insert("radio_quality", 0);
        values.insert("res_emergency", 0);
        CANmessage resGoMessage = this->_fcp->encodeMessage("res", "status", values);
        emit send_to_CAN(*COM::encodeToByteArray<CANmessage>(resGoMessage));
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        this->ui->exceptionOutput->setText(e.toString());
    }
}

void ASWidget::updatePedalProgressBars(uint64_t acceleratorValue, uint64_t brakeValue)
{
    this->ui->accProgressBar->setValue(((float)acceleratorValue));
    this->ui->brakeProgressBar->setValue(((float)brakeValue));
}

void ASWidget::on_sendEmergencyBuzzerButton_clicked()
{
    try {
        CANmessage message = this->_fcp->encodeSendCommand("interface", "ebs", "as_emergency_buzzer", 1, 0, 0);
        emit send_to_CAN(*COM::encodeToByteArray<CANmessage>(message));
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
    }
}

void ASWidget::on_EBSFCPDisplayButton_toggled(bool checked)
{
    this->ui->FCPDisplayWidget->setVisible(checked);
}

void ASWidget::setupEBSFcpDisplay()
{
    this->clearFCPDisplay();
    try {
        Message* ebsStatus = this->_fcp->getMessage(this->_fcp->getDevice("ebs"), "ebs_status");
        QList<CanSignal*> sigs = ebsStatus->getSignals();
        foreach (CanSignal* sig, sigs) {
            FCPSignalDisplayValue* newFCPDisplay = new FCPSignalDisplayValue(this, "ebs", sig->getName());

            this->EBSFcpMap.insert(sig->getName(), newFCPDisplay);

            this->ui->FCPDisplayLayout->addWidget(newFCPDisplay);

            connect(newFCPDisplay, &FCPSignalDisplayValue::destroyed, [this, sig]() { this->EBSFcpMap.remove(sig->getName()); });
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        this->ui->exceptionOutput->setText(e.toString());
        return;
    }
}

void ASWidget::parseEBSFcpDisplayMessage(CANmessage msg)
{
    try {
        auto messageInfo = this->_fcp->decodeMessage(msg);

        QMap<QString, double> sigValues = messageInfo.second;

        for (auto it = sigValues.begin(); it != sigValues.end(); it++) {
            QString signalName = it.key();
            double signalValue = it.value();
            FCPSignalDisplayValue* display = EBSFcpMap.value(signalName);
            display->updateValue(signalValue);
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        this->ui->exceptionOutput->setText(e.toString());
    }
}

void ASWidget::clearFCPDisplay()
{
    QList<FCPSignalDisplayValue*> displays = EBSFcpMap.values();
    foreach (FCPSignalDisplayValue* display, displays) {
        delete display;
    }
    EBSFcpMap.clear();
    return;
}

void ASWidget::on_restoreFCPDisplayButton_clicked()
{
    qDebug() << EBSFcpMap.size();
    this->clearFCPDisplay();
    setupEBSFcpDisplay();
}
