#ifndef ASWIDGET_HPP
#define ASWIDGET_HPP

#include "../Resources/MultipleInputTemplateWidget.hpp"
#include "COM.hpp"
#include "Exceptions/helper.hpp"
#include "FCPSignalDisplayValue.hpp"
#include "Resources/nameValueInputTemplate.hpp"
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"
#include "fcpcom.hpp"
#include <QWidget>
#include <memory>

namespace Ui {
class ASWidget;
}

class ASWidget : public UiWindow {
    Q_OBJECT

public:
    explicit ASWidget(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    QString getTitle() override { return "Autonomous Systems"; };
    ~ASWidget();

private slots:
    void on_sendASStateButton_clicked();

    void on_sendAsMissionButton_clicked();

    virtual void process_CAN_message(CANmessage) override;

    virtual void updateFCP(FCPCom*) override;

    void handleWidgetMessages(QString name, QMap<QString, QString> values);

    void updateSliderValue(int value);

    void on_steeringSetpointSlider_sliderMoved(int position);

    void on_resGoSendButton_clicked();

    void updatePedalProgressBars(uint64_t acceleratorValue, uint64_t brakeValue);

    void on_sendEmergencyBuzzerButton_clicked();

    void on_EBSFCPDisplayButton_toggled(bool checked);

    void setupEBSFcpDisplay();

    void parseEBSFcpDisplayMessage(CANmessage msg);

    void clearFCPDisplay();

    void on_restoreFCPDisplayButton_clicked();

private:
    Ui::ASWidget* ui;
    QMap<int, QString> ebsStates;
    QMap<int, QString> asStates;
    QMap<int, QString> asMissions;
    QMap<QString, FCPSignalDisplayValue*> EBSFcpMap;
    Helper* logger;
};

#endif // ASWIDGET_HPP
