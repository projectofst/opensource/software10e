#include <QApplication>
#include <QObject>

#include "COM_Interface.hpp"
#include "COM_Manager.hpp"
#include "COM_Select.hpp"
#include "ConfigManager/configmanager.hpp"
#include "Exceptions/logutils.hpp"
#include "Main_Toolbar.hpp"
#include "Picasso.hpp"
#include "WindowMain.hpp"

QString setupJson(MainWindow* window, ConfigManager* cfg_manager)
{
    QString _jsonFileName = "";

    // Check if json is saved

    QMap<QString, QVariant> global_cfg = cfg_manager->readGlobalConfig();

    if (global_cfg.contains("json_path")) { // Try to load from our config file
        if (QFileInfo::exists(global_cfg["json_path"].toString())) {
            return global_cfg["json_path"].toString();
        }
    }

    if (QFileInfo::exists("can-ids-spec/fst10e.json")) { // Some hardcoded default paths
        _jsonFileName = "can-ids-spec/fst10e.json";
    } else if (QFileInfo::exists(QDir::currentPath() + "/../software10e/can-ids-spec/fst10e.json")) {
        _jsonFileName = QDir::currentPath() + "/../software10e/can-ids-spec/fst10e.json";
    } else {
        // Default paths failed, ask file path
        QString path = QFileDialog::getOpenFileName(window, "Load Json File", QDir::currentPath(), "Load a json file (*.json)");
        if (!path.isEmpty()) {
            _jsonFileName = path;
        } else {
            return "null";
        }
    }

    QMap<QString, QVariant> current_cfg;
    current_cfg["json_path"] = _jsonFileName;
    cfg_manager->registerGlobalConfig(current_cfg); // Save json file path

    return _jsonFileName;
}

/**
 * @section The FST CAN Interface Documentation
 * This is the introduction
 */
int main(int argc, char* argv[])
{
    LogUtils::initLogging(); // Init logging system

    FCPCom* _fcp = nullptr;
    Comsguy* comsguy;
    QApplication GUI(argc, argv);
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);

    ConfigManager* config_manger = new ConfigManager;
    MainWindow* window = new MainWindow(nullptr, config_manger);

    // window->setStyleSheet(style);
    MainToolbar* maintoolbar = new MainToolbar(window);
    // maintoolbar->setStyleSheet(style);

    QString jsonFile = setupJson(window, config_manger);

    if (jsonFile == "null") {
        qDebug() << "The interface needs a JSON File to proceed";
        QMessageBox msgBox;
        msgBox.setText("The interface needs a JSON File to proceed!");
        msgBox.exec();
        return 0;
    }

    try {
        _fcp = new FCPCom();
        _fcp->setFile(jsonFile);
        _fcp->parse();
        window->setFCP(_fcp);
    } catch (JsonException& e) {
        qDebug() << e.toString();
    }

    comsguy = new Comsguy(window, _fcp);
    comsguy->connectMainWin(window);

    new Picasso(maintoolbar, window, comsguy);

    window->show();

    COMSelect* comselect = new COMSelect(nullptr, comsguy, _fcp, config_manger);
    comselect->show();
    comselect->activateWindow();
    comselect->raise();

    window->comselect = comselect;

    return GUI.exec();
}
