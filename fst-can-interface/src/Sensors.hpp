#ifndef SENSORS_H
#define SENSORS_H

#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/WATER_TEMP_CAN.h"
#include <QWidget>

#include "UiWindow.hpp"

namespace Ui {
class Sensors;
}

class Sensors : public UiWindow {
    Q_OBJECT

public:
    explicit Sensors(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~Sensors() override;
    void updateUI(void);
    // void updateSupotsUI(SUPOT_MSG_SIG supots_values, WT_Data water_temp);

private slots:

    void process_CAN_message(CANmessage msg) override;

    void updateFCP(FCPCom* fcp) override;

private:
    Ui::Sensors* ui;
};

#endif // SENSORS_H
