#include "SingleInverter.hpp"
#include "ui_SingleInverter.h"
#include <QDebug>
SingleInverter::SingleInverter(QWidget* parent, QString motorName)
    : QWidget(parent)
    , ui(new Ui::SingleInverter)
{
    ui->setupUi(this);
    this->_motorName = motorName;
    this->ui->boundingBox->setTitle(motorName);
}

SingleInverter::~SingleInverter()
{
    delete ui;
}

void SingleInverter::updateMotorInformation(QMap<QString, double> info)
{
    QMap<QString, double> signalValues = info;

    double motorTemp = signalValues.value("temp_motor");
    double motorSpeed = signalValues.value("motor_speed");
    double motorCurrent = signalValues.value("motor_current");
    double motorTorque = signalValues.value("motor_torque");

    ui->MotorTemp->setText(QString::number(motorTemp, 'f', 1));
    ui->Torque->setText(QString::number(motorTorque, 'f', 1));
    ui->Speed->setText(QString::number(motorSpeed, 'f', 1));
    ui->Current->setText(QString::number(motorCurrent, 'f', 1));
}

void SingleInverter::updateInverterInformation(QMap<QString, double> info)
{

    bool invMaxPassed = (bool)info.value("inv_max_passed", 0.0);
    double inverterTemperature = info.value("inv_temp", 0.0);
    double IGBTTemp = info.value("inv_temp_igbt");
    double errorValue = info.value("inv_error");
    bool igbtMaxPassed = (bool)info.value("igbt_max_passed", 0.0);
    bool motorMaxPassed = (bool)info.value("motor_max_passed", 0.0);

    ui->InvTemp->setText(QString::number(inverterTemperature / 10.0, 'f', 1));
    ui->IGBT->setText(QString::number(IGBTTemp / 10.0, 'f', 1));
    ui->Error->setText(QString::number(errorValue));

    if (invMaxPassed) {
        ui->INV_TEMP_LED->turnRed();
    } else {
        ui->INV_TEMP_LED->turnGreen();
    }

    if (igbtMaxPassed) {
        ui->IGBT_TEMP_LED->turnRed();
    } else {
        ui->IGBT_TEMP_LED->turnGreen();
    }

    if (motorMaxPassed) {
        ui->MT_LED->turnRed();
    } else {
        ui->MT_LED->turnGreen();
    }
    return;
}

void SingleInverter::updateInverterLimits(QMap<QString, double> info)
{
    double maxOpPower = info.value("max_op_power", 0.0);
    double maxOpTorque = info.value("max_op_torque", 0.0);
    double maxSafetyTorque = info.value("max_sfty_torque", 0.0);
    double maxSafetyPower = info.value("max_sfty_power", 0.0);

    ui->MAX_OP_POWER->setText(QString::number(maxOpPower, 'f', 1));
    ui->MAX_OP_TORQUE->setText(QString::number(maxOpTorque, 'f', 1));
    ui->MAX_SAF_TORQUE->setText(QString::number(maxSafetyTorque, 'f', 1));
    ui->MAX_SAF_POWER->setText(QString::number(maxSafetyPower, 'f', 1));
}
void SingleInverter::clear()
{
}

void SingleInverter::setName(QString name)
{
    this->_motorName = name;
    this->ui->boundingBox->setTitle(name);
}
