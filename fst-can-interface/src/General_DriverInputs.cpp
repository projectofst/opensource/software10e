#include "General_DriverInputs.hpp"
#include "ui_General_DriverInputs.h"

#include <QQuickItem>
#include <QQuickWidget>

#define PEDAL_MAX_VALUE 10000
#define STEER_MAX_VALUE 10000
#define STEER_MAX_ANGLE 360

DriverInputs::DriverInputs(QWidget* parent, FCPCom* fcp)
    : UiWindow(parent, fcp)
    , ui(new Ui::DriverInputs)
{
    ui->setupUi(this);

    // Set bars to maximum ADC value (12bits => 4096)
    // TODO: APPS max was 1000 before
    ui->APPS->setMaximum(PEDAL_MAX_VALUE);
    ui->APPS->setMinimum(0);

    ui->BPS->setMaximum(PEDAL_MAX_VALUE);
    ui->BPS->setMinimum(0);

    ui->BPEL->setMaximum(PEDAL_MAX_VALUE);
    ui->BPEL->setMinimum(0);

    updateTimer = new QTimer;
    connect(updateTimer, &QTimer::timeout,
        this, &DriverInputs::updateUI);
    updateTimer->start(70);

    // ui->SteeringWheel->setMaximum(STEER_MAX_VALUE);
    // ui->SteeringWheel->setMinimum(-STEER_MAX_VALUE);
}

DriverInputs::~DriverInputs()
{
    delete ui;
}

void DriverInputs::process_CAN_message(CANmessage message)
{
    CANdata msg = message.candata;
    QString module_name = nomes_placas[msg.dev_id];

    switch (msg.dev_id) {
    case DEVICE_ID_TE:
        parse_can_te(msg, &ggen_TE_CAN_Data);
        break;
    case DEVICE_ID_STEER:
        // parse_can_steer(msg, &ggen_steer);
        break;
    }
}

void DriverInputs::updateUI()
{

    ui->APPS->setValue(ggen_TE_CAN_Data.main_message.APPS);
    ui->BPS->setValue(ggen_TE_CAN_Data.main_message.BPS_pressure);
    ui->BPEL->setValue(ggen_TE_CAN_Data.main_message.BPS_electric);
}

void DriverInputs::on_horizontalSlider_valueChanged(int value)
{
    QString temps = QString::number(value / 100.0);
    temps.append(" %");

    value = value * STEER_MAX_ANGLE / 10000;
    QQuickItem* temp = ui->quickWidget->rootObject();
    temp->childItems().at(0)->setProperty("rotation", value);
    temp->childItems().at(1)->childItems().at(0)->setProperty("text", temps);
}

void DriverInputs::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}
