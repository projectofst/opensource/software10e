#include "Isabelle.hpp"
#include "can-ids/CAN_IDs.h"
#include "isa_config.hpp"
#include "ui_Isabelle.h"
#include "ui_isa_config.h"

ISA::ISA(QWidget* parent, FCPCom* fcp, Helper* log, ConfigManager*)
    : UiWindow(parent, fcp)
    , ui(new Ui::ISA)
{
    ui->setupUi(this);
    this->logger = log;
    // send_to_isa(0x3E, 0x74,0,0,0,0,0,0,0);
    //  Create chart, add data, hide legend;
    // chartView->addScrollBarWidget(ui->Plot, Qt::AlignLeft);
    num_current = 0;
    stop_mode = 0;
    cont = 1;
    error_number = 0;
    isa_config_dialog = new isa_config(this);
}

void ISA::process_CAN_message(CANmessage message)
{
    auto DB1 = (message.candata.data[0] >> 8); // Data Byte configuration easier reading
    auto DB0 = (message.candata.data[0] & 0x00FF);
    auto DB3 = (message.candata.data[1] >> 8);
    auto DB2 = (message.candata.data[1] & 0x00FF);
    if (message.candata.sid == 0x03D) {
        current_current = (message.candata.data[2] << 16) + message.candata.data[1];
        ui->current_current->setText(QString::number(current_current, 10));

        send_to_isa(0x03E, 0x43, 0x21, 0, 0, 0, 0, 0, 0);
        send_to_isa(0x03E, 0x43, 0x22, 0, 0, 0, 0, 0, 0);
    } else if (message.candata.sid == 0x5D) {
        v1_current = (message.candata.data[2] << 16) + message.candata.data[1];
        ui->v1_current->setText(QString::number(v1_current, 10));
        send_to_isa(0x03E, 0x43, 0x23, 0, 0, 0, 0, 0, 0);
        send_to_isa(0x03E, 0x43, 0x24, 0, 0, 0, 0, 0, 0);
    } else if (message.candata.sid == 0x07D) {
        v2_current = (message.candata.data[2] << 16) + message.candata.data[1];
        ui->v2_current->setText(QString::number(v2_current, 10));
        send_to_isa(0x03E, 0x43, 0x25, 0, 0, 0, 0, 0, 0);
        send_to_isa(0x03E, 0x43, 0x26, 0, 0, 0, 0, 0, 0);

    } else if (message.candata.sid == 0x09D) {
        v3_current = (message.candata.data[2] << 16) + message.candata.data[1];
        ui->v3_current->setText(QString::number(v3_current, 10));
        send_to_isa(0x03E, 0x43, 0x28, 0, 0, 0, 0, 0, 0);
        send_to_isa(0x03E, 0x43, 0x27, 0, 0, 0, 0, 0, 0);
    } else if (message.candata.sid == 0x0BD) {
        temp_current = ((message.candata.data[2] << 16) + message.candata.data[1]) / 10;
        ui->temp_current->setText(QString::number(temp_current, 10));
        send_to_isa(0x03E, 0x43, 0x29, 0, 0, 0, 0, 0, 0);
        send_to_isa(0x03E, 0x43, 0x2A, 0, 0, 0, 0, 0, 0);
    } else if (message.candata.sid == 0x0DD) {
        power_current = (message.candata.data[2] << 16) + message.candata.data[1];
        ui->power_current->setText(QString::number(power_current, 10));
    }

    else if (message.candata.sid == 0x0FD) {
        curr_count_current = (message.candata.data[2] << 16) + message.candata.data[1];
        ui->curr_count_current->setText(QString::number(curr_count_current, 10));
        send_to_isa(0x3E, 0x43, 0x01, 0, 0, 0, 0, 0, 0);
        send_to_isa(0x3E, 0x43, 0x02, 0, 0, 0, 0, 0, 0);
        send_to_isa(0x3E, 0x43, 0x03, 0, 0, 0, 0, 0, 0);

    }

    else if (message.candata.sid == 0x011D) {
        energy_count_current = (message.candata.data[2] << 16) + message.candata.data[1];
        ui->energy_count_current->setText(QString::number(energy_count_current, 10));
        send_to_isa(0x3E, 0x43, 0x04, 0, 0, 0, 0, 0, 0);
        send_to_isa(0x3E, 0x43, 0x05, 0, 0, 0, 0, 0, 0);
        send_to_isa(0x3E, 0x43, 0x06, 0, 0, 0, 0, 0, 0);
    }

    else if (message.candata.sid == 0x05E) { // Responses (0x05E)
        if (DB0 == 0x83) { // Response on overall LogData
            if (DB1 == 0x21) {
                int64_t current_max = calculate_maxormin(message.candata);
                ui->current_max->setText(QString::number(current_max));
            } else if (DB1 == 0x22) {
                int64_t current_min = calculate_maxormin(message.candata);
                ui->current_min->setText(QString::number(current_min));
            } else if (DB1 == 0x23) {
                int64_t v1_max = (int64_t)calculate_maxormin(message.candata);
                ui->v1_max->setText(QString::number(v1_max));
            } else if (DB1 == 0x24) {
                int64_t v1_min = calculate_maxormin(message.candata);
                ui->v1_min->setText(QString::number(v1_min));
            } else if (DB1 == 0x25) {
                int64_t v2_max = calculate_maxormin(message.candata);
                ui->v2_max->setText(QString::number(v2_max));
            } else if (DB1 == 0x26) {
                int64_t v2_min = calculate_maxormin(message.candata);
                ui->v2_min->setText(QString::number(v2_min));
            } else if (DB1 == 0x27) {
                int64_t v3_max = calculate_maxormin(message.candata);
                ui->v3_max->setText(QString::number(v3_max));
            } else if (DB1 == 0x28) {
                int64_t v3_min = calculate_maxormin(message.candata);
                ui->v3_min->setText(QString::number(v3_min));
            } else if (DB1 == 0x29) {
                int64_t temp_max = calculate_maxormin(message.candata) / 10;
                ui->temp_max->setText(QString::number(temp_max));
            } else if (DB1 == 0x2A) {
                int64_t temp_min = calculate_maxormin(message.candata) / 10;
                ui->temp_min->setText(QString::number(temp_min));
            } else if (DB1 == 0x01) {
                // Current counter overall
            } else if (DB1 == 0x02) {
                int64_t current_counter_charging = calculate_maxormin(message.candata);
                ui->curr_count_charging->setText(QString::number(current_counter_charging));
            } else if (DB1 == 0x03) {
                int64_t current_counter_discharging = calculate_maxormin(message.candata);
                ui->curr_count_discharging->setText(QString::number(current_counter_discharging));
            } else if (DB1 == 0x04) {
                // ENergy overall
            } else if (DB1 == 0x05) {
                int64_t energy_charging = calculate_maxormin(message.candata);
                ui->energy_count_charging->setText(QString::number(energy_charging));
            } else if (DB1 == 0x05) {
                int64_t energy_discharging = calculate_maxormin(message.candata);
                ui->energy_count_discharging->setText(QString::number(energy_discharging));
            }
        } else if (DB0 == 0x80) { // Measurment Erros
            if (DB1 == 0x00) {
                char string[512];
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error ADC Interrupt");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error Overflow ADC channel 1");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    sprintf(string, "Error Underflow ADC channel 1");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error Overflow ADC channel 2");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error Underflow ADC channel 2");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error Vref");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error current measurment implausible l1-l2");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error thermal EMF correction");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error current measurment l1 open circuit");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error current measurment U1 open circuit");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error current measurment U2 open circuit");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error current measurment U3 open circuit");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error ntc-h open circuit");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error ntc-l open circuit");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error calibration data offset-, gain error to high");
                    ui->error_output->appendPlainText(string);
                }
            }
            ui->error_number->setText(QString::number(error_number));
        } else if (DB0 == 0x81) { // System Erros
            if (DB1 == 0x00) {
                char string[512];
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error Code CRC");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error Parameter CRC");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    sprintf(string, "Error CAN bus recieve Data");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error CAN bus transmit Data CRC");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error overtemp");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error undertemp");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error power failure");
                    ui->error_output->appendPlainText(string);
                }
                DB2 = binary_check(DB2);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error system clock");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error system init");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error configuration");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error overcurrent detection");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error eeprom r/w");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error ADC Clock");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error Reset Illegal opcode");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error Reset Watchdog");
                    ui->error_output->appendPlainText(string);
                }
                DB3 = binary_check(DB3);
                if (resto == 1) {
                    error_number++;
                    sprintf(string, "Error Reset EMC");
                    ui->error_output->appendPlainText(string);
                }
            }

            ui->error_number->setText(QString::number(error_number));
        } else if (DB0 == 0xB4) {
            error_number++;
            char string[64] = "";
            ui->error_output->appendPlainText(string);
        }
        ui->error_number->setText(QString::number(error_number));
    }
}

int ISA::binary_check(int binary)
{
    resto = binary % 2;
    return binary = binary / 2;
}

int64_t ISA::calculate_maxormin(CANdata message)
{
    int64_t num = (((message.data[3] & 0xFF00) >> 8) + ((message.data[3] & 0x00FF) << 8) + ((message.data[2] & 0xFF00) << 8) + ((uint64_t)(message.data[2] & 0x00FF) << 24) + ((uint64_t)(message.data[1] & 0xFF00) << 24) + ((uint64_t)(message.data[1] & 0x00FF) << 40));
    int64_t ver = num & 0x800000000000;
    if (ver != 0) {
        num = num + (0xFFFF000000000000);
    }
    return num;
}

void ISA::on_set_threshold_button_clicked()
{

    positive_threshold = ui->positive_threshold_edit->text().toInt();
    negative_threshold = ui->negative_threshold_edit->text().toInt();
    ui->current_positive_threshold->setText(ui->positive_threshold_edit->text());
    ui->current_negative_threshold->setText(ui->negative_threshold_edit->text());
    ui->positive_threshold_edit->setText(" ");
    ui->negative_threshold_edit->setText(" ");
}

void ISA::send_to_isa(int message_sid, int DB0, int DB1, int DB2, int DB3, int DB4, int DB5, int DB6, int DB7)
{
    CANmessage msg;
    msg.candata.sid = uint16_t(message_sid);
    msg.candata.dlc = 8;
    msg.candata.data[0] = uint16_t((DB1 << 8) + DB0);
    msg.candata.data[1] = uint16_t((DB3 << 8) + DB2);
    msg.candata.data[2] = uint16_t((DB5 << 8) + DB4);
    msg.candata.data[3] = uint16_t((DB7 << 8) + DB6);
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

ISA::~ISA()
{
    delete ui;
}

void ISA::on_run_button_clicked()
{
    send_to_isa(0x03E, 0x34, 0x01, 0x01, 0, 0, 0, 0, 0);
}

void ISA::on_stop_button_clicked()
{

    send_to_isa(0x03E, 0x34, 0, 0x01, 0, 0, 0, 0, 0);
    if (stop_mode == 1)
        stop_mode = 0;
    else
        stop_mode = 1;
}

void ISA::on_reset_button_clicked()
{
    // TODO: apagar tudo o que esta na interface
    send_to_isa(0x03E, 0x030, 0x02, 0, 0, 0, 0x15, 0xF7, 0);
}

void ISA::on_config_button_clicked()
{
    isa_config_dialog->show();
}

void ISA::test()
{
}

void ISA::on_store_button_clicked()
{
    return;
}

void ISA::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}
