#include "UiWindow.hpp"
#include "QDebug"
UiWindow::UiWindow(QWidget* parent, FCPCom* fcp)
    : QWidget(parent)
{

    this->setFCP(fcp);
}

UiWindow::~UiWindow(void)
{
    emit removeFromObservers(this);
    emit removeFromActiveWidgets(this);
}

FCPCom* UiWindow::getFCP()
{
    return this->_fcp;
}

void UiWindow::setFCP(FCPCom* fcp)
{
    this->_fcp = fcp;
}

void UiWindow::handleTimeChange()
{
}

void UiWindow::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}

void UiWindow::closeEvent(QCloseEvent* event)
{
    event->accept();
    delete this;
}
