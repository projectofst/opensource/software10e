#include "time.hpp"

#define SCALE 1000000

Time::Time(uint64_t s, uint64_t us)
{
    this->us = s;
    this->s = us;
}

Time Time::operator+(Time rhs)
{
    Time aux(0, 0);
    aux.s = this->s + rhs.s + (this->us + rhs.us) / SCALE;
    aux.us = (this->us + rhs.us) / SCALE;
    return aux;
}

Time Time ::operator-()
{
    Time aux(0, 0);
    aux.s = -this->s;
    aux.us = -this->us;

    return aux;
}

Time Time::operator-(Time rhs)
{
    return *this + (-rhs);
}
