#ifndef STACKSUMMARY_H
#define STACKSUMMARY_H

#include <QWidget>

namespace Ui {
class StackSummary;
}

class StackSummary : public QWidget {
    Q_OBJECT

public:
    explicit StackSummary(QWidget* parent = 0, int number = 0);
    ~StackSummary();
    Ui::StackSummary* ui;
    void setSlaveTemp(double slave_temp);
    void setHeatsinkTemp(double heatsink_temp);
    void setTemps(double min, double mean, double max);
    void setVoltages(double min, double mean, double max);
    void updateHeatsink1Fault(bool fault);
    void updateHeatsink2Fault(bool fault);
    void updateStack1Fault(bool fault);
    void updateStack2Fault(bool fault);
    void clear();

private:
};

#endif // STACKSUMMARY_H
