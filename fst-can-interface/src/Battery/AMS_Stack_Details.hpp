#ifndef STACKDETAILS_H
#define STACKDETAILS_H

#include <QVector>
#include <QWidget>

#include "AMS_Cell_Line.hpp"

namespace Ui {
class StackDetails;
}

class StackDetails : public QWidget {
    Q_OBJECT

public:
    explicit StackDetails(QWidget* parent = 0, QString title = "", int numberOfCells = 12);
    ~StackDetails();
    QVector<CellLine*> cells;
    void clear(void);

private:
    Ui::StackDetails* ui;
    int numberOfCells;
};

#endif // STACKDETAILS_H
