#include "AMS_Battery.hpp"
#include "AMS_Cell.hpp"
#include "AMS_Stack.hpp"

Battery::Battery(int stack_number, int cells_per_stack)
{
    _number_of_cells = cells_per_stack * stack_number;
    _number_of_stacks = stack_number;
    _number_of_cells_per_stack = cells_per_stack;

    for (int i = 0; i < stack_number; i++) {
        Stack* stack = new Stack(cells_per_stack);
        stacks.append(stack);
    }
}

Stack* Battery::getStack(int stack)
{
    return stacks[stack];
}

Cell* Battery::getCell(int stack, int cell)
{
    return stacks[stack]->cells[cell];
}

int Battery::number_of_cells_per_stack()
{
    return _number_of_cells_per_stack;
}

int Battery::number_of_stacks()
{
    return _number_of_stacks;
}

void Battery::setVoltages(int min, int mean, int max)
{
    min_voltage = min / 10 / 1000.0;
    mean_voltage = mean / 10 / 1000.0;
    max_voltage = max / 10 / 1000.0;
}

void Battery::setTemps(int min, int mean, int max)
{
    min_temp = min / 10.0;
    mean_temp = mean / 10.0;
    max_temp = max / 10.0;
}

void Battery::setPower(int voltage, int current, int soc, int power)
{
    this->voltage = voltage / 100.0;
    this->current = current;
    this->soc = soc;
    this->power = power;
}

void Battery::setHottestCell(int cell, int stack)
{
    hottest_cell = cell;
    hottest_stack = stack;
}

void Battery::setBalanceTarget(int cell, int stack)
{
    balance_target_cell = cell;
    balance_target_stack = stack;
}
