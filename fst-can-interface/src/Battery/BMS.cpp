#include "BMS.hpp"
#include "ui_BMS.h"

BatteryT::BatteryT(QWidget* parent, FCPCom* fcp)
    : UiWindow(parent, fcp)
    , ui(new Ui::BatteryT)
{
    ui->setupUi(this);
    currentK = 1;
    lastK = -1;
    createDevs();
}

BatteryT::~BatteryT()
{
    delete ui;
}

void BatteryT::process_CAN_message(CANmessage msg)
{
    emit rebroadcast_CAN_message(msg);
}

void BatteryT::createDevs()
{
    for (uint i = 0; i < 12; i++) {
        Stacks[i] = new StackT(this, i + 1);
        connect(this, SIGNAL(rebroadcast_CAN_message(CANmessage)),
            Stacks[i], SLOT(process_CAN_message(CANmessage)));
    }
}

void BatteryT::resize(int Width)
{
    layoutDevs(((Width) / 320) - 1);
}

void BatteryT::layoutDevs(int k)
{
    if (lastK != k) {
        QGridLayout* StacksGrid = new QGridLayout;
        int c = 0;
        int l = 0;
        foreach (QWidget* stack, Stacks) {
            StacksGrid->addWidget(stack, l, c);
            c++;
            if (c > k) {
                c = 0;
                l++;
            }
        }
        foreach (QObject* Button, ui->Stacks->layout()->children()) {
            ui->Stacks->layout()->removeWidget(static_cast<QWidget*>(Button));
        }
        delete ui->Stacks->layout();
        ui->Stacks->setLayout(StacksGrid);
        lastK = k;
    }
}

void BatteryT::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}
