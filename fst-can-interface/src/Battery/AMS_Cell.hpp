#ifndef CELL_H
#define CELL_H

class Cell {
public:
    Cell();
    void setVoltageState(int voltage_state);
    void setTemperatureState(int temperature_state);
    void setVoltage(double voltage);
    void setTemperature(double temperature);
    void setSOC(int soc);
    float getVoltage();
    float getTemp();
    void clear();

private:
    int voltage_state;
    int temp_state;
    double voltage;
    double temperature;
    int soc;
};

#endif // CELL_H
