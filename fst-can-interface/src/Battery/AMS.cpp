#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QProgressBar>
#include <QTimer>

#include "AMS.hpp"
#include "AMS_Details.hpp"
#include "ui_AMS.h"
#include "ui_AMS_Stack_Summary.h"
#include <qdebug.h>

#include "AMS_Battery.hpp"
#include "AMS_Stack.hpp"
#include "AMS_Stack_Summary.hpp"
#include "COM.hpp"
#include "Resources/LEDwidget.hpp"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/BMS_MASTER_CAN.h"
#include "can-ids/Devices/BMS_VERBOSE_CAN.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/Devices/INTERFACE_CAN.h"
#define SET_SIZE 11

const char* bms_sets[SET_SIZE] = {
    "",
    "",
    "",
    "",
    "MAX_T",
    "MIN_T",
    "MAX_V",
    "MIN_V",
    "BAT_MAX_V",
    "CHARGING_DELTA",
    "MAX_HEATSINK"

};

AMS::AMS(QWidget* parent, FCPCom* fcp, Helper* log, ConfigManager*)
    : UiWindow(parent, fcp)
    , ui(new Ui::AMS)
{
    ui->setupUi(this);

    this->logger = log;

    // TODO Send message, receive message, parse message
    int numberOfStacks = 12;
    int numberOfCellsPerStack = 12;

    this->ams_details = new AMSDetails(nullptr, numberOfStacks, numberOfCellsPerStack);

    for (int i = 0; i < numberOfStacks; i++) {
        StackSummary* ss = new StackSummary(this, i);
        stacks.append(ss);
        ui->StackSummaryArea->addWidget(ss);
    }

    /* Initializing battery with 12 stacks of 12 cells */
    this->battery = new Battery(numberOfStacks, numberOfCellsPerStack);

    this->received_messages = 0;

    QTimer* battery_timer = new QTimer(this);
    connect(battery_timer, &QTimer::timeout, this, &AMS::update_battery);
    battery_timer->start(250);

    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &AMS::timer_clear_ams);
    timer->start(2000);

    QTimer* details_timer = new QTimer(this);
    connect(details_timer, &QTimer::timeout, this, &AMS::update_details);
    details_timer->start(250);

    ui->voltage->setText(QString::number(0, 'f', 2) + " V");
    ui->soc->setText(QString::number(0, 'f', 2) + " %");
    ui->power->setText(QString::number(0, 'f', 2) + " kW");
    ui->current->setText(QString::number(0, 'f', 2) + " A");

    ui->hottest_stack->setText(QString::number(0, 'f', 2));

    battery->setBalanceTarget(0, 0);

    for (int i = 4; i < SET_SIZE; i++) {
        ui->set_select->addItem(bms_sets[i]);
    }
}

AMS::~AMS()
{
    delete ui;
}

void AMS::process_CAN_message(CANmessage message)
{
    CANdata msg = message.candata;
    this->received_messages = 1;

    if (msg.dev_id == DEVICE_ID_BMS_MASTER && msg.msg_id == CMD_ID_COMMON_GET && msg.data[0] == DEVICE_ID_BMS_MASTER) {
        QString value = QString::number(msg.data[2], 10);
        switch (msg.data[1]) {
        case P_BMS_CELL_LIMIT_MAX_T:
            ui->max_t->setText(value);
            break;
        case P_BMS_CELL_LIMIT_MAX_V:
            ui->max_v->setText(value);
            break;
        case P_BMS_CELL_LIMIT_MIN_T:
            ui->min_t->setText(value);
            break;
        case P_BMS_CELL_LIMIT_MIN_V:
            ui->min_v->setText(value);
            break;
        case P_BMS_BAT_LIMIT_MAX_V:
            ui->bat_v->setText(value);
            break;
        case P_BMS_BAT_CHARGING_DELTA:
            ui->charging_delta->setText(value);
            break;
            /*case P_BMS_SLAVE_MAX_HEATSINK_TEMP:
                ui->heatsink_temp->setText(value);
                break;*/
        }
    }

    MASTER_MSG_Data data;
    MASTER_VERBOSE_MSG_Data verbose_data;
    if (msg.dev_id == DEVICE_ID_BMS_MASTER && msg.msg_id == MSG_ID_MASTER_STATUS) {
        parse_can_message_master(msg, &data);
        if (data.status.AMS_OK)
            ui->ams_ok->turnGreen();
        else
            ui->ams_ok->turnOff();
        if (data.status.IMD_OK)
            ui->imd_ok->turnGreen();
        else
            ui->imd_ok->turnOff();
        if (data.status.IMD_LATCH)
            ui->imd_latch->turnGreen();
        else
            ui->imd_latch->turnOff();
        if (data.status.AMS_LATCH)
            ui->ams_latch->turnGreen();
        else
            ui->ams_latch->turnOff();
        if (data.status.AIR_positive)
            ui->air_positive->turnGreen();
        else
            ui->air_positive->turnOff();
        if (data.status.AIR_negative)
            ui->air_negative->turnGreen();
        else
            ui->air_negative->turnOff();
        if (data.status.PreChK)
            ui->precharge->turnGreen();
        else
            ui->precharge->turnOff();
        if (data.status.DisChK)
            ui->discharge->turnGreen();
        else
            ui->discharge->turnOff();
        if (data.status.SC_DCU_IMD)
            ui->shutdown_dcu_imd->turnGreen();
        else
            ui->shutdown_dcu_imd->turnOff();
        if (data.status.SC_IMD_AMS)
            ui->shutdown_imd_ams->turnGreen();
        else
            ui->shutdown_imd_ams->turnOff();
        if (data.status.SC_AMS_DCU)
            ui->shutdown_ams_dcu->turnGreen();
        else
            ui->shutdown_ams_dcu->turnOff();
        if (data.status.SC_TSMS_Relays)
            ui->shutdown_tsms_relays->turnGreen();
        else
            ui->shutdown_tsms_relays->turnOff();
        if (data.status.shutdown_above_minimum)
            ui->shutdown_above_minimum->turnGreen();
        else
            ui->shutdown_above_minimum->turnOff();
        if (data.status.shutdown_open)
            ui->shutdown_open->turnGreen();
        else
            ui->shutdown_open->turnOff();
        if (data.status.verbose) {
            ui->verbose->turnGreen();
        } else {
            ui->verbose->turnOff();
        }
        if (data.status.ts_on)
            ui->ts_on->turnGreen();
        else
            ui->ts_on->turnOff();
    }
    if (msg.dev_id == DEVICE_ID_BMS_MASTER && msg.msg_id == MSG_ID_MASTER_CELL_TEMPERATURE_INFO) {
        parse_can_message_master(msg, &data);
        battery->setTemps(data.cell_temp_info.min_temperature,
            data.cell_temp_info.mean_temperature,
            data.cell_temp_info.max_temperature);
        battery->setHottestCell(data.cell_temp_info.hottest_cell[1],
            data.cell_temp_info.hottest_cell[0]);
    }
    if (msg.dev_id == DEVICE_ID_BMS_MASTER && msg.msg_id == MSG_ID_MASTER_CELL_VOLTAGE_INFO) {
        parse_can_message_master(msg, &data);
        battery->setVoltages(data.cell_voltage_info.min_voltage,
            data.cell_voltage_info.mean_voltage,
            data.cell_voltage_info.max_voltage);
        battery->setBalanceTarget(data.cell_voltage_info.balance_target[1],
            data.cell_voltage_info.balance_target[0]);
    }
    if (msg.dev_id == DEVICE_ID_BMS_MASTER && msg.msg_id == MSG_ID_MASTER_ENERGY_METER) {
        parse_can_message_master(msg, &data);
        battery->setPower(_fcp->decodeDoubleFromSignal(message, "master_em_voltage"), _fcp->decodeDoubleFromSignal(message, "master_em_current"),
            _fcp->decodeDoubleFromSignal(message, "master_em_soc"), _fcp->decodeDoubleFromSignal(message, "master_em_power"));
    }
    if (msg.dev_id == DEVICE_ID_BMS_MASTER && msg.msg_id == MSG_ID_MASTER_SC_OPEN) {
    }
    if (msg.dev_id == DEVICE_ID_BMS_VERBOSE && msg.msg_id == MSG_ID_MASTER_CELL_INFO) {
        parse_can_message_master_verbose(msg, &verbose_data);
        MASTER_MSG_Cell_Info cell_info = verbose_data.cell;

        if (verbose_data.cell.cell_id > battery->number_of_stacks() * battery->number_of_cells_per_stack()) {
            return;
        }
        int stack_id = verbose_data.cell.cell_id / battery->number_of_stacks();
        int cell_id = verbose_data.cell.cell_id % battery->number_of_cells_per_stack();

        Cell* cell = battery->getCell(stack_id, cell_id);
        cell->setTemperature(cell_info.temperature);
        cell->setVoltage(cell_info.voltage);
        cell->setSOC(cell_info.SoC);
        cell->setTemperatureState(cell_info.temperature_state);
        cell->setVoltageState(cell_info.voltage_state);
    }
    if (msg.dev_id == DEVICE_ID_BMS_VERBOSE && msg.msg_id == MSG_ID_MASTER_SLAVE_INFO_1) {
        parse_can_message_master_verbose(msg, &verbose_data);
        MASTER_MSG_Slave_Info_1 slave_info_1 = verbose_data.slave.slave_info_1;
        int stack_id = slave_info_1.slave_id;

        if (stack_id > 12) {
            return;
        }
        Stack* st = battery->getStack(stack_id);

        st->setCellConnectionsFault(slave_info_1.cell_connection_fault_mask);
        st->setCellTempFault(slave_info_1.cell_temp_fault_mask);
        st->setDischargeChannelFault(slave_info_1.discharge_channel_fault_mask);
        st->stack_temp1_fault = slave_info_1.slave_temp1_fault;
        st->stack_temp2_fault = slave_info_1.slave_temp2_fault;
        st->heatsink_temp1_fault = slave_info_1.heatsink_temp1_fault;
        st->heatsink_temp2_fault = slave_info_1.heatsink_temp2_fault;
    }

    if (msg.dev_id == DEVICE_ID_BMS_VERBOSE && msg.msg_id == MSG_ID_MASTER_SLAVE_INFO_2) {
        parse_can_message_master_verbose(msg, &verbose_data);
        MASTER_MSG_Slave_Info_2 slave_info_2 = verbose_data.slave.slave_info_2;

        int stack_id = slave_info_2.slave_id;

        if (stack_id > battery->number_of_stacks()) {
            return;
        }
        Stack* st = battery->getStack(stack_id);

        st->setDischargeChannelState(slave_info_2.discharge_channel_state);
        st->setHeatsinkTemp1(slave_info_2.heatsink_temperature1);
        st->setHeatsinkTemp2(slave_info_2.heatsink_temperature2);
    }
    if (msg.dev_id == DEVICE_ID_BMS_VERBOSE && msg.msg_id == MSG_ID_MASTER_SLAVE_INFO_3) {
        parse_can_message_master_verbose(msg, &verbose_data);
        MASTER_MSG_Slave_Info_3 slave_info_3 = verbose_data.slave.slave_info_3;

        int stack_id = slave_info_3.slave_id;
        if (stack_id > battery->number_of_stacks()) {
            return;
        }
        Stack* st = battery->getStack(stack_id);

        st->setBMTemp(slave_info_3.BM_temperature);
        st->setStackTemp1(slave_info_3.slave_temperature1);
        st->setStackTemp2(slave_info_3.slave_temperature2);
    }
}

void AMS::update_battery()
{

    if (this->received_messages == 0) {
        return;
    }
    ui->voltage->setText(QString::number(battery->voltage, 'f', 2) + " V");
    ui->soc->setText(QString::number(battery->soc / 10, 'f', 2) + " %");

    ui->SOC_bar->setValue(static_cast<int>(battery->soc / 10));
    ui->power->setText(QString::number(battery->power / 1000, 'f', 2) + " kW");
    ui->current->setText(QString::number(battery->current / 10000.0, 'f', 2) + " A");

    ui->min_temp->setText(QString::number(battery->min_temp, 'f', 1) + " °C");
    ui->mean_temp->setText(QString::number(battery->mean_temp, 'f', 1) + " °C");
    ui->max_temp->setText(QString::number(battery->max_temp, 'f', 1) + " °C");

    ui->balance_target_cell->setText(QString::number(battery->balance_target_cell));
    ui->balance_target_stack->setText(QString::number(battery->balance_target_stack));

    ui->min_voltage->setText(QString::number(battery->min_voltage, 'f', 3) + " V");
    ui->mean_voltage->setText(QString::number(battery->mean_voltage, 'f', 3) + " V");
    ui->max_voltage->setText(QString::number(battery->max_voltage, 'f', 3) + " V");

    ui->hottest_cell->setText(QString::number(battery->hottest_cell));
    ui->hottest_stack->setText(QString::number(battery->hottest_stack));
}

void AMS::updateSumary(int stack)
{
    Stack* s = this->battery->getStack(stack);
    StackSummary* stack_summary = stacks[stack];

    QLabel* heatsink_temp = stacks[stack]->ui->heatsink_temp;
    double heatsink_temp1 = s->heatsink_temp1;
    double heatsink_temp2 = s->heatsink_temp1;
    heatsink_temp->setText(QString::number(heatsink_temp1, 'f', 1) + "°C / " + QString::number(heatsink_temp2, 'f', 1) + "°C");

    QLabel* slave_temp = stacks[stack]->ui->slave_temp;
    double slave_temp1 = s->slave_temp1;
    double slave_temp2 = s->slave_temp1;

    slave_temp->setText(QString::number(slave_temp1, 'f', 1) + "°C / " + QString::number(slave_temp2, 'f', 1) + "°C");

    stack_summary->setVoltages(s->getMinVoltage(), s->getMeanVoltage(), s->getMaxVoltage());
    stack_summary->setTemps(s->getMinTemp(), s->getMeanTemp(), s->getMaxTemp());

    stack_summary->updateHeatsink1Fault(s->heatsink_temp1_fault);
    stack_summary->updateHeatsink2Fault(s->heatsink_temp2_fault);
    stack_summary->updateStack1Fault(s->stack_temp1_fault);
    stack_summary->updateStack2Fault(s->stack_temp2_fault);
}
void AMS::update_details(void)
{
    if (this->received_messages == 0) {
        return;
    }
    Cell* aux_cell;
    Stack* aux_stack;
    for (int i = 0; i < battery->number_of_stacks(); i++) {
        this->updateSumary(i);
        aux_stack = battery->stacks[i];

        for (int j = 0; j < battery->number_of_cells_per_stack(); j++) {
            aux_cell = this->battery->getCell(i, j);
            ams_details->updateCell(i, j, aux_cell->getVoltage(), aux_cell->getTemp());
            ams_details->updateTempFault(i, j, aux_stack->cell_temp_fault[j]);
            ams_details->updateVoltageFault(i, j, aux_stack->cell_connections_fault[j]);
            // Must preserve order of function application
            ams_details->updateDischargeState(i, j, aux_stack->discharge_channel_state[j]);
            ams_details->updateDischargeFault(i, j, aux_stack->discharge_channel_fault[j]);
        }
    }
}

void AMS::on_pushButton_2_clicked()
{
    CANmessage msg;

    try {
        msg = _fcp->encodeSendCommand("interface", "master", "common_ts", TS_DASH_BUTTON, 0, 0);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        qDebug() << e.toString();
    }

    printf("%d/%d/%d/%d/%d/%d\n", msg.candata.sid, msg.candata.dlc, msg.candata.data[0], msg.candata.data[1], msg.candata.data[2], msg.candata.data[3]);
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_TS;
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void AMS::on_pushButton_5_clicked()
{
    CANmessage msg;
    try {
        msg = _fcp->encodeSendCommand("interface", "master", "fake_error", 0, 0, 0);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        qDebug() << e.toString();
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void AMS::on_pushButton_6_clicked()
{
    CANmessage msg;
    try {
        msg = _fcp->encodeSendCommand("interface", "master", "reset_error", 0, 0, 0);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        qDebug() << e.toString();
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void AMS::on_pushButton_4_clicked()
{
    CANmessage msg;
    try {
        msg = _fcp->encodeSendCommand("interface", "master", "toggle_verbose", 0, 0, 0);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        qDebug() << e.toString();
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

/*void AMS::on_verbose_customContextMenuRequested(const QPoint &pos)
{
    return;
}*/

void AMS::on_pushButton_7_clicked()
{
    clear_ams();
    this->ams_details->clear();
}

void AMS::clear_ams()
{
    ui->ams_ok->turnOff();
    ui->imd_ok->turnOff();
    ui->imd_latch->turnOff();
    ui->ams_latch->turnOff();
    ui->air_positive->turnOff();
    ui->air_negative->turnOff();
    ui->precharge->turnOff();
    ui->discharge->turnOff();
    ui->shutdown_dcu_imd->turnOff();
    ui->shutdown_imd_ams->turnOff();
    ui->shutdown_ams_dcu->turnOff();
    ui->shutdown_tsms_relays->turnOff();
    ui->shutdown_above_minimum->turnOff();
    ui->shutdown_open->turnOff();
    ui->verbose->turnOff();
    ui->ts_on->turnOff();
    ui->min_v->setText("");
    ui->max_v->setText("");
    ui->min_t->setText("");
    ui->max_v->setText("");
    ui->bat_v->setText("");
    ui->charging_delta->setText("");
    ui->heatsink_temp->setText("");
    ui->max_voltage->setText("");
    ui->min_voltage->setText("");
    ui->mean_voltage->setText("");
    ui->balance_target_cell->setText("");
    ui->balance_target_stack->setText("");
    ui->min_temp->setText("");
    ui->mean_temp->setText("");
    ui->max_temp->setText("");
    ui->hottest_stack->setText("");
    ui->hottest_cell->setText("");
    ui->voltage->setText("");
    ui->current->setText("");
    ui->power->setText("");
    ui->soc->setText("");
    ui->SOC_bar->setValue(0);
    for (int i = 0; i < 12; i++) {
        this->battery->getStack(i)->clear();
    }
}

void AMS::timer_clear_ams(void)
{
    if (this->received_messages) {
        this->received_messages--;
    } else {
        clear_ams();
        this->ams_details->clear();
    }
}

void AMS::on_pushButton_8_clicked()
{
    ams_details->show();
}

void AMS::on_pushButton_clicked()
{
    CANmessage msg;
    try {
        msg = _fcp->encodeSendCommand("interface", "master", "toggle_fans", 0, 0, 0);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        qDebug() << e.toString();
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void AMS::on_pushButton_3_clicked()
{
    CANmessage msg;
    try {
        msg = _fcp->encodeSendCommand("interface", "master", "toggle_charge", 0, 0, 0);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        qDebug() << e.toString();
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void AMS::on_pushButton_10_clicked()
{
    CANmessage message;
    CANdata* msg = &message.candata;

    msg->dev_id = DEVICE_ID_INTERFACE;
    msg->msg_id = CMD_ID_COMMON_GET;
    msg->dlc = 6;
    msg->data[0] = DEVICE_ID_BMS_MASTER;

    msg->data[1] = P_BMS_CELL_LIMIT_MIN_T;
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(message).get()));

    msg->data[1] = P_BMS_CELL_LIMIT_MAX_T;
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(message).get()));

    msg->data[1] = P_BMS_CELL_LIMIT_MIN_V;
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(message).get()));

    msg->data[1] = P_BMS_CELL_LIMIT_MAX_V;
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(message).get()));

    msg->data[1] = P_BMS_BAT_LIMIT_MAX_V;
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(message).get()));

    msg->data[1] = P_BMS_BAT_CHARGING_DELTA;
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(message).get()));
    // msg->data[1] = P_BMS_SLAVE_MAX_HEATSINK_TEMP;
    // emit send_to_CAN(message);
}

void AMS::on_send_set_clicked()
{
    QString parameter = ui->set_select->currentText();
    QString str_value = ui->set_value->text();
    int value = str_value.toInt();

    CANmessage message;
    CANdata* msg = &message.candata;

    msg->dev_id = DEVICE_ID_INTERFACE;
    msg->msg_id = CMD_ID_COMMON_SET;
    msg->dlc = 6;
    msg->data[0] = DEVICE_ID_BMS_MASTER;

    for (int i = 0; i < SET_SIZE; i++) {
        if (!strcmp(bms_sets[i], parameter.toLatin1())) {
            msg->data[1] = i;
            msg->data[2] = value;
        }
    }

    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(message).get()));
}

void AMS::updateFCP(FCPCom* fcp)
{
    this->UiWindow::setFCP(fcp);
}
