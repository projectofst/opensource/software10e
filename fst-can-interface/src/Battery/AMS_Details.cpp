#include <QGridLayout>
#include <QVector>

#include "AMS_Cell_Line.hpp"
#include "AMS_Details.hpp"
#include "AMS_Stack_Details.hpp"
#include "ui_AMS_Cell_Line.h"
#include "ui_AMS_Details.h"

#include <CAN_IDs.h>

AMSDetails::AMSDetails(QWidget* parent, int numberOfStacks, int numberOfCellsPerStack)
    : QWidget(parent)
    , ui(new Ui::AMSDetails)
{
    ui->setupUi(this);
    this->numberOfStacks = numberOfStacks;
    this->numberOfCellsPerStack = numberOfCellsPerStack;

    for (int i = 0; i < this->numberOfStacks; i++) {
        StackDetails* sd = new StackDetails(this, "Stack " + QString::number(i), this->numberOfCellsPerStack);
        stacks.append(sd);
        ui->StackDetailsArea->addWidget(sd, i / 3, i % 3);
    }

    for (int i = 0; i < this->numberOfStacks; i++) {
        for (int j = 0; j < this->numberOfCellsPerStack; j++) {
            updateCell(i, j, 0, 0);
        }
    }
}

AMSDetails::~AMSDetails()
{
    delete ui;
}

void AMSDetails::updateCell(int stack, int cell, double voltage, double temperature)
{
    stacks[stack]->cells[cell]->updateVoltage(voltage);
    stacks[stack]->cells[cell]->updateTemperature(temperature);
}

void AMSDetails::clear()
{
    for (int i = 0; i < numberOfStacks; i++) {
        stacks[i]->clear();
    }
}
void AMSDetails::updateTempFault(int stack, int cell, bool fault)
{
    stacks[stack]->cells[cell]->updateTempFault(fault);
}
void AMSDetails::updateVoltageFault(int stack, int cell, bool fault)
{
    stacks[stack]->cells[cell]->updateVoltageFault(fault);
}
void AMSDetails::updateDischargeFault(int stack, int cell, bool fault)
{
    stacks[stack]->cells[cell]->updateDischargeFault(fault);
}
void AMSDetails::updateDischargeState(int stack, int cell, bool fault)
{
    stacks[stack]->cells[cell]->updateDischargeState(fault);
}
