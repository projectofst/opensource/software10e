#ifndef CELLLINE_H
#define CELLLINE_H

#include <QWidget>

namespace Ui {
class CellLine;
}

class CellLine : public QWidget {
    Q_OBJECT

public:
    explicit CellLine(QWidget* parent = 0, QString cell = "");
    ~CellLine();
    void clear(void);
    void updateVoltage(double voltage);
    void updateTemperature(double temperature);
    void updateVoltageState(int voltage_state);
    void updateTempState(int voltage_state);
    void updateTempFault(bool fault);
    void updateVoltageFault(bool fault);
    void updateDischargeFault(bool fault);
    void updateDischargeState(bool fault);

private:
    Ui::CellLine* ui;
};

#endif // CELLLINE_H
