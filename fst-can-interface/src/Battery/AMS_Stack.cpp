#include "AMS_Stack.hpp"
#include "AMS_Cell.hpp"

#include <QVector>

Stack::Stack(int cells_per_stack)
{
    number_cells = cells_per_stack;

    for (int i = 0; i < number_cells; i++) {
        discharge_channel_fault.append(false);
        cell_connections_fault.append(false);
        discharge_channel_state.append(false);
        cell_temp_fault.append(false);

        Cell* cell = new Cell();
        cells.append(cell);
    }

    heatsink_temp1 = 0;
    heatsink_temp2 = 0;
    slave_temp1 = 0;
    slave_temp2 = 0;
    BM_temp = 0;
}

void Stack::setDischargeChannelFault(int fault_bitfield)
{
    for (int i = 0; i < number_cells; i++) {
        this->discharge_channel_fault[i] = (fault_bitfield >> i) & 1;
    }
}

void Stack::setCellConnectionsFault(int fault_bitfield)
{
    for (int i = 0; i < number_cells; i++) {
        this->cell_connections_fault[i] = (fault_bitfield >> i) & 1;
    }
}

void Stack::setCellTempFault(int fault_bitfield)
{
    for (int i = 0; i < number_cells; i++) {
        this->cell_temp_fault[i] = (fault_bitfield >> i) & 1;
    }
}

void Stack::setDischargeChannelState(int state_bitfield)
{
    for (int i = 0; i < number_cells; i++) {
        this->discharge_channel_state[i] = (state_bitfield >> i) & 1;
    }
}

void Stack::setHeatsinkTemp1(int temp)
{
    heatsink_temp1 = temp / 10.0;
    if (heatsink_temp1 > 120) {
        heatsink_temp1 = 120.0;
    }
}

void Stack::setHeatsinkTemp2(int temp)
{
    heatsink_temp2 = temp / 10.0;
    if (heatsink_temp2 > 120) {
        heatsink_temp2 = 120.0;
    }
}

void Stack::setStackTemp1(int temp)
{
    slave_temp1 = temp / 10.0;
    if (slave_temp1 > 120) {
        slave_temp1 = 120.0;
    }
}

void Stack::setStackTemp2(int temp)
{
    slave_temp2 = temp / 10.0;
    if (slave_temp2 > 120) {
        slave_temp2 = 120.0;
    }
}

void Stack::setBMTemp(int temp)
{
    BM_temp = temp / 10.0;
    if (BM_temp > 120) {
        BM_temp = 120.0;
    }
}

double Stack::getMeanVoltage()
{
    double mean = 0;
    int valids = 0;
    for (int i = 0; i < number_cells; i++) {
        if (!this->cell_connections_fault[i]) {
            mean += cells[i]->getVoltage();
            valids++;
        }
    }
    return mean /= valids;
}

double Stack::getMaxVoltage()
{
    double max = 0;
    for (int i = 0; i < number_cells; i++) {
        if (max < cells[i]->getVoltage() && !this->cell_connections_fault[i]) {
            max = cells[i]->getVoltage();
        }
    }
    return max;
}

double Stack::getMinVoltage()
{
    double min = 7;
    for (int i = 0; i < number_cells; i++) {
        if (min > cells[i]->getVoltage() && !this->cell_connections_fault[i]) {
            min = cells[i]->getVoltage();
        }
    }
    return min;
}

double Stack::getMeanTemp()
{
    double mean = 0;
    int valids = 0;
    for (int i = 0; i < number_cells; i++) {
        if (!this->cell_temp_fault[i]) {
            mean += cells[i]->getTemp();
            valids++;
        }
    }
    return mean /= valids;
}

double Stack::getMaxTemp()
{
    double max = 0;
    for (int i = 0; i < number_cells; i++) {
        if (max < cells[i]->getTemp() && !this->cell_temp_fault[i]) {
            max = cells[i]->getTemp();
        }
    }
    return max;
}

double Stack::getMinTemp()
{
    double min = 120;
    for (int i = 0; i < number_cells; i++) {
        if (min > cells[i]->getTemp() && !this->cell_temp_fault[i]) {
            min = cells[i]->getTemp();
        }
    }
    return min;
}

void Stack::clear()
{
    heatsink_temp1 = 120;
    heatsink_temp2 = 120;
    slave_temp1 = 120;
    slave_temp2 = 120;
    stack_temp1_fault = 0;
    stack_temp2_fault = 0;
    heatsink_temp1_fault = 0;
    heatsink_temp2_fault = 0;
    for (int i = 0; i < number_cells; i++) {
        cells[i]->clear();
    }
}
