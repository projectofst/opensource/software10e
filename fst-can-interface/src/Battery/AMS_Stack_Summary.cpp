#include "AMS_Stack_Summary.hpp"
#include "ui_AMS_Stack_Summary.h"
#include <QGroupBox>
#include <QLabel>

StackSummary::StackSummary(QWidget* parent, int number)
    : QWidget(parent)
    , ui(new Ui::StackSummary)
{
    ui->setupUi(this);
    ui->groupBox->setTitle("Slave " + QString::number(number));
}

StackSummary::~StackSummary()
{
    delete ui;
}

void StackSummary::setSlaveTemp(double slave_temp)
{
    ui->slave_temp->setText(QString::number(slave_temp, 'f', 1) + "°C");
}

void StackSummary::setHeatsinkTemp(double heatsink_temp)
{
    ui->heatsink_temp->setText(QString::number(heatsink_temp, 'f', 1) + "°C");
}

void StackSummary::setTemps(double min, double mean, double max)
{
    ui->min_temp->setText(QString::number(min, 'f', 1) + "°C");
    ui->mean_temp->setText(QString::number(mean, 'f', 1) + "°C");
    ui->max_temp->setText(QString::number(max, 'f', 1) + "°C");
}

void StackSummary::setVoltages(double min, double mean, double max)
{
    ui->min_voltage->setText(QString::number(min, 'f', 3) + "V");
    ui->mean_voltage->setText(QString::number(mean, 'f', 3) + "V");
    ui->max_voltage->setText(QString::number(max, 'f', 3) + "V");
}

void StackSummary::updateHeatsink1Fault(bool fault)
{
    QLabel* label = ui->heatsink_temp;
    if (fault) {
        label->setStyleSheet("QLabel {color : red}");
    } else {
        label->setStyleSheet("QLabel {color : black}");
    }
}
void StackSummary::updateHeatsink2Fault(bool fault)
{
    QLabel* label = ui->heatsink_temp;
    if (fault) {
        label->setStyleSheet("QLabel {color : red}");
    } else {
        label->setStyleSheet("QLabel {color : black}");
    }
}

void StackSummary::updateStack1Fault(bool fault)
{
    QLabel* label = ui->slave_temp;
    if (fault) {
        label->setStyleSheet("QLabel {color : red}");
    } else {
        label->setStyleSheet("QLabel {color : black}");
    }
}
void StackSummary::updateStack2Fault(bool fault)
{
    QLabel* label = ui->slave_temp;
    if (fault) {
        label->setStyleSheet("QLabel {color : red}");
    } else {
        label->setStyleSheet("QLabel {color : black}");
    }
}

void StackSummary::clear()
{
    setSlaveTemp(120.0);
    setHeatsinkTemp(120.0);
    setTemps(120, 120, 120);
    setVoltages(7, 7, 7);
    updateHeatsink1Fault(false);
    updateHeatsink2Fault(false);
    updateStack1Fault(false);
    updateStack2Fault(false);
}
