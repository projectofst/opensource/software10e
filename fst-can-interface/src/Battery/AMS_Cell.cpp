#include "AMS_Cell.hpp"

Cell::Cell()
{
    this->voltage_state = 0;
    this->temp_state = 0;
    this->voltage = 0;
    this->temperature = 0;
    this->soc = 0;
}

void Cell::setVoltageState(int voltage_state)
{
    this->voltage_state = voltage_state;
}

void Cell::setTemperatureState(int temperature_state)
{
    this->temp_state = temperature_state;
}
void Cell::setVoltage(double voltage)
{
    this->voltage = voltage / 10 / 1000.0;
}

void Cell::setTemperature(double temperature)
{
    this->temperature = temperature / 10.0;
    if (this->temperature > 120) {
        this->temperature = 120.0;
    }
}

void Cell::setSOC(int soc)
{
    this->soc = soc;
}

float Cell::getVoltage()
{
    return this->voltage;
}

float Cell::getTemp()
{
    return this->temperature;
}

void Cell::clear()
{
    voltage_state = 0;
    temp_state = 0;
    voltage = 7;
    temperature = 120;
    soc = 0;
}
