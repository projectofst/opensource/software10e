#ifndef STACK_H
#define STACK_H

#include <QVector>

#include "AMS_Cell.hpp"

class Stack {
public:
    Stack(int cells_per_stack = 12);
    QVector<Cell*> cells;
    int number_cells;

    void setDischargeChannelFault(int fault_bitfield);
    void setCellConnectionsFault(int fault_bitfield);
    void setCellTempFault(int fault_bitfield);
    void setDischargeChannelState(int state_bitfield);
    void setHeatsinkTemp1(int temp);
    void setHeatsinkTemp2(int temp);
    void setStackTemp1(int temp);
    void setStackTemp2(int temp);
    void setBMTemp(int temp);

    double getMeanVoltage();
    double getMaxVoltage();
    double getMinVoltage();

    double getMeanTemp();
    double getMaxTemp();
    double getMinTemp();

    void clear();

    QVector<bool> discharge_channel_fault;
    QVector<bool> cell_connections_fault;
    QVector<bool> cell_temp_fault;
    QVector<bool> discharge_channel_state;

    double heatsink_temp1;
    double heatsink_temp2;
    double slave_temp1;
    double slave_temp2;
    double BM_temp;

    bool stack_temp1_fault;
    bool stack_temp2_fault;
    bool heatsink_temp1_fault;
    bool heatsink_temp2_fault;

private:
};

#endif // STACK_H
