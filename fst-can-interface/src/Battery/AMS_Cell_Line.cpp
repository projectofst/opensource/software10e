#include "AMS_Cell_Line.hpp"
#include "LEDwidget.hpp"
#include "ui_AMS_Cell_Line.h"
#include <QLabel>

CellLine::CellLine(QWidget* parent, QString cell)
    : QWidget(parent)
    , ui(new Ui::CellLine)
{
    ui->setupUi(this);
    ui->id->setText(cell);
}

CellLine::~CellLine()
{
    delete ui;
}

void CellLine::clear(void)
{
    updateVoltage(0);
    updateTemperature(0);
    updateVoltageState(0);
    updateTempState(0);
}

void CellLine::updateVoltage(double voltage)
{
    if (voltage == 0.0) {
        ui->voltage->setText("-.- V");
        ui->voltage_state->turnOff();
    } else {
        if (voltage < 5.0) {
            ui->voltage->setText(QString::number(voltage, 'f', 3) + "V");
        } else {
            clear();
        }
    }
}

void CellLine::updateTemperature(double temperature)
{
    if (temperature == 0.0 || temperature > 100.0) {
        ui->temp->setText("-.- ºC");
        ui->temp_state->turnOff();
    } else {
        ui->temp->setText(QString::number(temperature, 'f', 1) + "°C");
    }
}

void CellLine::updateVoltageState(int)
{
    return;
}

void CellLine::updateTempState(int)
{
    return;
}

void CellLine::updateTempFault(bool fault)
{
    if (fault) {
        ui->temp_state->turnOff();
    } else {
        ui->temp_state->turnGreen();
    }
    return;
}
void CellLine::updateVoltageFault(bool fault)
{
    if (fault) {
        ui->voltage_state->turnOff();
    } else {
        ui->voltage_state->turnGreen();
    }
    return;
}
void CellLine::updateDischargeFault(bool fault)
{
    if (fault) {
        ui->dis_chann_state->turnOff();
    }
    return;
}
void CellLine::updateDischargeState(bool fault)
{
    if (fault) {
        ui->dis_chann_state->turnGreen();
    } else {
        ui->dis_chann_state->turnOff();
    }
    return;
}
