#ifndef BMS_HPP
#define BMS_HPP

#include <QLabel>
#include <QLineEdit>
#include <QMap>
#include <QSpinBox>
#include <QWidget>

#include "BMS_Stack.hpp"
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"

namespace Ui {
class BatteryT;
}

class BatteryT : public UiWindow {
    Q_OBJECT

public:
    explicit BatteryT(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~BatteryT() override;

public slots:
    void process_CAN_message(CANmessage) override;
    void resize(int);
    void updateFCP(FCPCom* fcp) override;

private:
    Ui::BatteryT* ui;
    void layoutDevs(int);
    void createDevs();
    int currentK;
    int lastK;

    QMap<uint, QWidget*> Stacks;
    QLineEdit* templine;
    QSpinBox* tempspin;

signals:
    void rebroadcast_CAN_message(CANmessage);
};

#endif // BMS_HPP
