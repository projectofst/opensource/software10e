#ifndef BMS_STACK_HPP
#define BMS_STACK_HPP

#include <QWidget>

#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"

namespace Ui {
class StackT;
}

class StackT : public UiWindow {
    Q_OBJECT

public:
    explicit StackT(QWidget* parent = nullptr, uint Id = 1, FCPCom* fcp = nullptr);
    ~StackT() override;

public slots:
    void process_CAN_message(CANmessage) override;
    void updateFCP(FCPCom* fcp) override;

private:
    Ui::StackT* ui;
    int MinVolt;
    int MaxVolt;
    int MinTemp;
    int MaxTemp;
};

#endif // BMS_STACK_HPP
