#include <QVBoxLayout>
#include <QVector>

#include "AMS_Cell_Line.hpp"
#include "AMS_Stack_Details.hpp"
#include "ui_AMS_Stack_Details.h"

StackDetails::StackDetails(QWidget* parent, QString title, int numberOfCells)
    : QWidget(parent)
    , ui(new Ui::StackDetails)
{
    ui->setupUi(this);
    ui->groupBox->setTitle(title);
    this->numberOfCells = numberOfCells;

    for (int i = 0; i < this->numberOfCells; i++) {
        CellLine* cl = new CellLine(this, "Cell " + QString::number(i));
        cells.append(cl);
        ui->cellLines->addWidget(cl);
    }
}

StackDetails::~StackDetails()
{
    delete ui;
}

void StackDetails::clear(void)
{
    for (int i = 0; i < this->numberOfCells; i++) {
        cells[i]->clear();
    }
}
