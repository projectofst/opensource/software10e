#ifndef BATTERY_H
#define BATTERY_H

#include "AMS_Stack.hpp"
#include <QVector>

class Battery {
public:
    Battery(int stack_number = 12, int cells_per_stack = 12);
    QVector<Stack*> stacks;
    Stack* getStack(int stack);
    Cell* getCell(int stack, int cell);
    int number_of_stacks();
    int number_of_cells_per_stack();

    void setVoltages(int min, int mean, int max);
    void setTemps(int min, int mean, int max);
    void setHottestCell(int cell, int stack);
    void setBalanceTarget(int cell, int stack);
    void setPower(int voltage, int current, int soc, int power);

    double min_temp;
    double mean_temp;
    double max_temp;
    double hottest_cell;
    double hottest_stack;
    double min_voltage;
    double mean_voltage;
    double max_voltage;
    double balance_target_cell;
    double balance_target_stack;
    double voltage;
    double current;
    double power;
    double soc;

private:
    int _number_of_stacks;
    int _number_of_cells;
    int _number_of_cells_per_stack;
};

#endif // BATTERY_H
