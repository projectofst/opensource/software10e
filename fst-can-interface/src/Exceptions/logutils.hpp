#ifndef LOGUTILS_HPP
#define LOGUTILS_HPP

#define LOGSIZE 1024 * 10000 // log size in bytes
#define LOGFILES 1

#include <QCache>
#include <QDate>
#include <QDateTime>
#include <QDebug>
#include <QObject>
#include <QString>
#include <QTime>

namespace LogUtils {
const QString logFolderName = "logs";

bool initLogging();
void myMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg);
void AddLineofLog(QString log_message);
}

#endif // LOGUTILS_HPP
