#ifndef HELPER_HPP
#define HELPER_HPP

#include "jsonexception.hpp"
#include <QDateTime>
#include <QFile>
#include <QQueue>
#include <QTextStream>
#include <exception>

class Helper {
public:
    Helper();
    int handle_exception(QString exception,
        const char* caller_file = __builtin_FILE(),
        const char* caller_fn = __builtin_FUNCTION(),
        const int caller_line = __builtin_LINE());
    ~Helper();

private:
    QQueue<QString> exceptions_cache;
};

#endif // HELPER_HPP
