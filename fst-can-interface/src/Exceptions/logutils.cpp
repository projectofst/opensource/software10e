#include "logutils.hpp"

#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QFileInfoList>
#include <QTime>
#include <iostream>

namespace LogUtils {
QCache<QString, QString>* error_cache;
static QString logFileName;

void initLogFileName()
{
    /*
    logFileName = QString(logFolderName + "/Log_%1__%2.txt")
                  .arg(QDate::currentDate().toString("yyyy_MM_dd"))
                  .arg(QTime::currentTime().toString("hh_mm_ss_zzz"));
    */

    logFileName = QString(logFolderName + "/log.txt");
}

/**
 * @brief deletes old log files, only the last ones are kept
 */
void deleteOldLogs()
{
    QDir dir;
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    dir.setSorting(QDir::Time | QDir::Reversed);
    dir.setPath(logFolderName);

    QFileInfoList list = dir.entryInfoList();
    if (list.size() <= LOGFILES) {
        return; // no files to delete
    } else {
        for (int i = 0; i < (list.size() - LOGFILES); i++) {
            QString path = list.at(i).absoluteFilePath();
            QFile file(path);
            file.remove();
        }
    }
}

bool initLogging()
{
    error_cache = new QCache<QString, QString>(10);

    // Create folder for logfiles if not exists
    if (!QDir(logFolderName).exists()) {
        QDir().mkdir(logFolderName);
    }

    QString qstring = QDateTime::currentDateTime().toString(Qt::ISODate);

    deleteOldLogs(); // delete old log files
    initLogFileName(); // create the logfile name

    QFile outFile(logFileName);
    if (outFile.open(QIODevice::WriteOnly | QIODevice::Append)) {
        qInstallMessageHandler(LogUtils::myMessageHandler);
        outFile.write("------------------------------------------- New Log -------------------------------------------\nTimeStamp:");
        outFile.write(qstring.toStdString().c_str());
        outFile.write("\nVersion: ");
        outFile.write(GIT_CURRENT_SHA1);
        outFile.write("\n");
        return true;
    } else {
        return false;
    }
}
void AddLineofLog(QString log_message)
{
    QFile outFile(logFileName);
    if (outFile.open(QIODevice::WriteOnly | QIODevice::Append)) {
        qInstallMessageHandler(LogUtils::myMessageHandler);
        outFile.write(log_message.toStdString().c_str());
    }
    return;
}

void myMessageHandler(QtMsgType, const QMessageLogContext&,
    const QString& txt)
{
    // check file size and if needed create new log!
    {
        QFile outFileCheck(logFileName);
        int size = outFileCheck.size();

        if (size > LOGSIZE) // check current log size
        {
            deleteOldLogs();
            initLogFileName();
        }
    }

    QFile outFile(logFileName);
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << txt << "\n";
}
}
