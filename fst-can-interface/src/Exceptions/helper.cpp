#include "helper.hpp"
#include <qdebug.h>

Helper::Helper()
{
}

int Helper::handle_exception(QString exception, const char* caller_file, const char* caller_fn, const int caller_line)
{
    QString log_msg = "[" + QDateTime::currentDateTime().toString("dd.MM.yyyy-hh:mm:ss") + "]";
    if (!exceptions_cache.contains(exception)) {
        qDebug() << "[CRITICAL]" << log_msg << "[" << caller_fn << " (" << caller_file << ":" << caller_line << ")"
                 << "]: " + exception + "\n";
        exceptions_cache.enqueue(exception);
        if (exceptions_cache.count() > 10) {
            exceptions_cache.dequeue();
        }
    }
    return 0;
}

Helper::~Helper()
{
}
