#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "AMS.hpp"
#include "AMS_Details.hpp"
#include "COM_Interface.hpp"
#include "General.hpp"
#include "Inverters.hpp"
#include "Resources/ledform.hpp"
#include "Sensors.hpp"
#include "Tail/Report.hpp"
//#include "COM_Select.hpp"
//#include "Picasso.hpp"

#include "ConfigManager/configmanager.hpp"
#include "DockManager.h"
#include <QApplication>
#include <QCloseEvent>
#include <QDockWidget>
#include <QMainWindow>
#include <QPainter>
#include <QProcess>
#include <QResizeEvent>
#include <QSettings>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr, ConfigManager* config_manager = nullptr);
    void setFCP(FCPCom* fcp);
    void closeEvent(QCloseEvent* event);
    ~MainWindow();
    Interface* interfaceCom;
    QWidget* comselect;
    ads::CDockManager* m_DockManager;
    ConfigManager* getConfigManager();

signals:
    void jsonChanged(QString jsonFile);

public slots:
    void updateStatusBar();
    // void actionToggleCOM_triggered();
    // void attachJsonObserver(UiWindow* newObserver);
    // int detachJsonObserver(UiWindow* observer);
    void notifyObservers();
    FCPCom* getFCPCom();

private:
    Ui::MainWindow* ui;
    bool jsonLoaded;
    QList<UiWindow*> _fcpObservers;
    FCPCom* _fcp;
    ConfigManager* config_manager;
    void loadStyleSheet(QString file_path);

protected:
    // bool eventFilter(QObject *, QEvent *);
    /*#ifndef QT_NO_CONTEXTMENU
    void contextMenuEvent(QContextMenuEvent *event) override;
#endif // QT_NO_CONTEXTMENU*/

private slots:
    void paintEvent(QPaintEvent*);
    void on_actionAbout_triggered();
    void on_actionOpen_Log_triggered();
    void on_actionNew_triggered();
    void on_actionFCPJson_triggered();

signals:
    void resizeDev(int);
    void resizeBat(int);
    void broadcast(CANmessage);
    void send(CANmessage);
    void willClose();
};

#endif
