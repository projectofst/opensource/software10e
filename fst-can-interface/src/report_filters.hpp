#ifndef REPORT_FILTERS_HPP
#define REPORT_FILTERS_HPP

#include "can-ids/CAN_IDs.h"
#include <QDialog>
//#include "Console.hpp"
#include "COM_SerialPort.hpp"
#include "UiWindow.hpp"
#include "report_indiv_filter.hpp"

namespace Ui {
class Report_filters;
}

class Report_filters : public QDialog {
    Q_OBJECT

public:
    explicit Report_filters(QWidget* parent = nullptr);
    ~Report_filters();
    void set_new_filter(Report_indiv_filter* filter);
    Report_indiv_filter* at(int k);
    int size();
    void add_device(QString device);
    int get_master_error_state();
    int get_master_get_state();
    int get_master_set_state();
    int get_master_reset_state();

private slots:
    void on_add_button_clicked();
    void on_master_error_stateChanged(int arg1);
    void on_master_get_stateChanged(int arg1);
    void on_master_set_stateChanged(int arg1);
    void on_master_reset_stateChanged(int arg1);
    int find_in_filters(QString name);
    void create_new_filter(QString name);
    void on_add_all_clicked();

private:
    Ui::Report_filters* ui;
    QVector<Report_indiv_filter*> all_filters;
    int master_error_flag;
    int master_set_flag;
    int master_get_flag;
    int master_reset_flag;
};

#endif // REPORT_FILTERS_HPP
