#include "report_filters.hpp"
#include "COM_SerialPort.hpp"
#include "Console.hpp"
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"
#include "ui_report_filters.h"
#include "ui_report_indiv_filter.h"
#include <QWidget>

Report_filters::Report_filters(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::Report_filters)
{
    ui->setupUi(this);
    master_set_flag = 1;
    master_get_flag = 1;
    master_error_flag = 1;
    master_reset_flag = 1;
}

Report_filters::~Report_filters()
{
    delete ui;
}

void Report_filters::set_new_filter(Report_indiv_filter*)
{
    return;
}

Report_indiv_filter* Report_filters::at(int k)
{
    return all_filters[k];
}

int Report_filters::size()
{
    return all_filters.size();
}

void Report_filters::add_device(QString device)
{
    ui->devices_dropdown->addItem(device);
    return;
}

void Report_filters::on_add_button_clicked()
{
    for (int k = 0; k < all_filters.size(); k++) {
        if (all_filters[k]->get_device_name() == ui->devices_dropdown->currentText())
            return;
    }
    create_new_filter(ui->devices_dropdown->currentText());
    return;
}

void Report_filters::on_master_error_stateChanged(int arg1)
{
    master_error_flag ^= 1;
    for (int k = 0; k < all_filters.size(); k++) {
        all_filters[k]->ui->error_check_box->setCheckState(Qt::CheckState(arg1));
    }
}

void Report_filters::on_master_get_stateChanged(int arg1)
{
    master_get_flag ^= 1;
    for (int k = 0; k < all_filters.size(); k++) {
        all_filters[k]->ui->get_check_box->setCheckState(Qt::CheckState(arg1));
    }
}

void Report_filters::on_master_set_stateChanged(int arg1)
{
    master_set_flag ^= 1;
    for (int k = 0; k < all_filters.size(); k++) {
        all_filters[k]->ui->set_check_box->setCheckState(Qt::CheckState(arg1));
    }
}

void Report_filters::on_master_reset_stateChanged(int arg1)
{
    master_reset_flag ^= 1;
    for (int k = 0; k < all_filters.size(); k++) {
        all_filters[k]->ui->reset_check_box->setCheckState(Qt::CheckState(arg1));
    }
}

int Report_filters::get_master_error_state()
{
    return master_error_flag;
}
int Report_filters::get_master_get_state()
{
    return master_get_flag;
}
int Report_filters::get_master_set_state()
{
    return master_set_flag;
}
int Report_filters::get_master_reset_state()
{
    return master_reset_flag;
}

void Report_filters::on_add_all_clicked()
{
    int size = ui->devices_dropdown->count();
    for (int i = 0; i < size; i++) {
        if (!find_in_filters(ui->devices_dropdown->itemText(i))) {
            create_new_filter(ui->devices_dropdown->itemText(i));
        }
    }
    return;
}

int Report_filters::find_in_filters(QString name)
{
    for (int i = 0; i < all_filters.size(); i++) {
        if (all_filters[i]->get_device_name() == name) {
            return 1;
        }
    }
    return 0;
}

void Report_filters::create_new_filter(QString name)
{
    Report_indiv_filter* new_indiv_filter = new Report_indiv_filter;
    new_indiv_filter->set_device_name(name);
    ui->all_filters->addWidget(new_indiv_filter);
    all_filters.append(new_indiv_filter);
    connect(new_indiv_filter->ui->del_but, &QPushButton::clicked, this, [this, new_indiv_filter]() {this->all_filters.remove(this->all_filters.indexOf(new_indiv_filter));delete new_indiv_filter; });
}
