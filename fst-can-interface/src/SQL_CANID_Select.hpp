#ifndef SQL_CANID_SELECT_HPP
#define SQL_CANID_SELECT_HPP

#include <QObject>
#include <QSqlDatabase>
#include <QWidget>

#include "SQL_CANID.hpp"
#include "UiWindow.hpp"

namespace Ui {
class SQL_CANID_Select;
}

class SQL_CANID_Select : public UiWindow {
    Q_OBJECT

public:
    explicit SQL_CANID_Select(QWidget* parent = nullptr,
        SQL_CANID* parentsql = nullptr);
    ~SQL_CANID_Select();
    SQL_CANID* DataBase;
    void updateViewList();
    void updateDbPath();

public slots:
    void updateTable(QString);

private:
    Ui::SQL_CANID_Select* ui;
};

#endif // SQL_CANID_SELECT_HPP
