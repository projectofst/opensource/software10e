#ifndef COMSELECTLINE_HPP
#define COMSELECTLINE_HPP

#include "COM_Manager.hpp"

#include <QStringList>
#include <QWidget>

namespace Ui {
class COMSelectLine;
}

class COMSelectLine : public QWidget {
    Q_OBJECT

public:
    explicit COMSelectLine(QWidget* parent = 0, Comsguy* comsguy = NULL);
    void connect_api(QString COM1,
        QString COM2,
        QString address1,
        QString address2,
        bool btn_is_checked);
    void auto_set(int COM1, int COM2, QString address1, QString address2, bool);
    QMap<QString, QVariant> export_state();
    void delete_com();
    void setComId(int id);
    int getComId();
    COM* get_com1();
    ~COMSelectLine();

private slots:
    void on_close_clicked();
    void on_address1_currentTextChanged(const QString& arg1);
    void on_COM1_currentTextChanged(const QString& arg1);
    void on_COM2_currentTextChanged(const QString& arg1);
    void on_address2_currentTextChanged(const QString& arg1);
    void on_connectButton_toggled(bool checked);
    void up();
    void down();

private:
    Ui::COMSelectLine* ui;
    Comsguy* comsguy;
    COM* targetcom1;
    COM* targetcom2;
    int com_id;
    QMovie* streaming_movie;
    bool conn_state = false;

signals:
    void close_com1(void);
    void close_com2(void);
    void remove_com(int);
};

#endif // COMSELECTLINE_HPP
