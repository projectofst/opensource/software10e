#ifndef ARM_HPP
#define ARM_HPP

#include "UiWindow.hpp"
#include "arms_torque_set.hpp"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/ARM_CAN.h"
#include "state_manager.hpp"
#include "ui_arms_torque_set.h"
#include <QDir>
#include <QQuickWidget>
#include <QSettings>
#include <QTimer>
#include <QWidget>

#define FL 0
#define FR 1
#define RL 2
#define RR 3

extern int PARAMETERS[];
typedef QPair<QString, int> ids;
namespace Ui {
class arm;
}

class arm : public UiWindow {
    Q_OBJECT

public:
    explicit arm(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~arm();

    void updateFCP(FCPCom* fcp) override;

private slots:
    void process_CAN_message(CANmessage) override;
    void init_sets();
    void retransmit_can(CANmessage);
    void do_gets();

    void on_send_all_clicked();

    void refresh_values();

    void clear_values();

    void on_set_torque_mode_clicked();

    void get_traction_mode();
    void on_set_regen_clicked();
    void get_regen_mode();

    void on_set_lateral_clicked();

    void on_set_event_clicked();

    void save_settings();

    void load_settings();

    void on_save_clicked();

    void on_load_clicked();

    virtual void keyPressEvent(QKeyEvent* event);

    void on_comboBox_currentTextChanged(const QString& arg1);

private:
    Ui::arm* ui;
    QString m_sSettingsFile;
    QQuickWidget* m_quickWidget;
    QList<int> parameters, states;
    QList<QString> names, state_names;
    QList<ids> name;
    QVector<arms_torque_set*> sets;
    QMap<int, QString> torque_map, regen_map, yaw_map, track_map;
    state_manager<int>* manager;
    int first_time;
    QTimer *refresh_timer, *clear_timer;
    QString specific;

    QString _name = "ARM";
};

#endif // ARM_HPP
