#include "ccu_indiv.hpp"
#include "CAN_IDs.h"
#include "can-ids/Devices/CCU_CAN.h"
#include "ui_ccu_indiv.h"
#include <QDebug>

ccu_indiv::ccu_indiv(QWidget* parent, QString new_name, uint16_t new_dev_id,
    uint16_t new_set_parameter, uint16_t new_get_parameter)
    : QWidget(parent)
    , ui(new Ui::ccu_indiv)
{
    ui->setupUi(this);
    connect(ui->set_but, &QPushButton::clicked, this, &ccu_indiv::on_set_but_clicked);
    name = new_name;
    dev_id = new_dev_id;
    set_parameter = new_set_parameter;
    get_parameter = new_get_parameter;
    set_name();
}

ccu_indiv::~ccu_indiv()
{
    delete ui;
}

void ccu_indiv::set_car_segment(QString new_car_segments)
{
    car_segment = new_car_segments;
    return;
}

void ccu_indiv::set_device(QString new_device)
{
    device = new_device;
    return;
}

void ccu_indiv::set_name()
{
    ui->name->setText(name);
    return;
}
void ccu_indiv::set_new_curr_value(int value)
{
    current_value = value;
    ui->current_value->setText(QString::number(value));
    return;
}
QString ccu_indiv::get_name()
{
    return name;
}

void ccu_indiv::on_set_but_clicked()
{
    uint16_t new_value = ui->value->text().toInt();
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 6;
    msg.candata.data[0] = dev_id;
    msg.candata.data[1] = set_parameter;
    msg.candata.data[2] = new_value;
    emit send_CAN(msg);

    return;
}

void ccu_indiv::get_value()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_GET;
    msg.candata.dlc = 4;
    msg.candata.data[0] = dev_id;
    msg.candata.data[1] = get_parameter;
    emit send_CAN(msg);
}

void ccu_indiv::clear()
{
    ui->current_value->setText("");
    return;
}

void ccu_indiv::send_custom_set_msg(int value)
{

    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 6;
    msg.candata.data[0] = dev_id;
    msg.candata.data[1] = set_parameter;
    msg.candata.data[2] = value;
    emit send_CAN(msg);
}
