#include "timeline.h"
#include "ui_timeline.h"

Timeline::Timeline(QWidget* parent, FCPCom* fcp, Helper*, ConfigManager*)
    : UiWindow(parent, fcp)
    , ui(new Ui::Timeline)
{
    ui->setupUi(this);
    this->parent = (MainWindow*)parent;
    this->com_select = (COMSelect*)this->parent->comselect;
    for (auto com : *com_select->get_coms()) {
        this->ui->comm_selector->addItem(com->protocol);
    }
}

void Timeline::process_CAN_message(CANmessage)
{
    return;
}

void Timeline::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}

Timeline::~Timeline()
{
    delete ui;
}

void Timeline::on_comm_selector_currentIndexChanged(const QString& com_selected)
{
    for (auto com : *this->com_select->get_coms()) {
        if (com->protocol == com_selected) {
            this->com = com;
            return;
        }
    }
}
