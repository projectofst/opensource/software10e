#ifndef TIMELINE_H
#define TIMELINE_H

#include <QWidget>

#include "COM_Select.hpp"
#include "UiWindow.hpp"
#include "WindowMain.hpp"
#include "fcpcom.hpp"

namespace Ui {
class Timeline;
}

class Timeline : public UiWindow {
    Q_OBJECT

public:
    explicit Timeline(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    QString getTitle() override { return "Timeline"; };
    ~Timeline();

public slots:
    void process_CAN_message(CANmessage) override;
    void updateFCP(FCPCom*) override;

private slots:
    void on_comm_selector_currentIndexChanged(const QString& arg1);

private:
    FCPCom* fcp;
    MainWindow* parent;
    Ui::Timeline* ui;
    COMSelect* com_select;
    COM* com;
};

#endif // TIMELINE_H
