#ifndef CANSIGNALGENERATOR_HPP
#define CANSIGNALGENERATOR_HPP

#include "SignalGenerator/SignalGenerator.hpp"
#include "cansignal.hpp"
#include "fcpcom.hpp"
#include "message.hpp"
#include <QMap>
#include <QObject>

class CanSignalGenerator : public SignalGenerator {
    Q_OBJECT
public:
    explicit CanSignalGenerator(QObject* parent = nullptr, QString expression = "x", int start = 0, int end = 0, double step = 1.0, int delay = 0, FCPCom* fcp = nullptr, QString deviceName = "", QString messageName = "", QString sigName = "");

private:
    FCPCom* _fcp;
    QString _sigName;
    QString _messageName;
    QString _deviceName;
    QMap<QString, double> map;

public slots:
    void setDeviceName(QString newDeviceName);
    void setSignalName(QString newSigName);
    void setMessageName(QString newDevName);
private slots:

    void processNewValue(double x, double y);
    void checkFcpInfo();
signals:
    void newSample(CANmessage msg);
};

#endif // CANSIGNALGENERATOR_HPP
