#ifndef COMSELECT_H
#define COMSELECT_H

#include "COM_Manager.hpp"
#include "UiWindow.hpp"
#include "comselectline.hpp"
#include "ui_COM_Select.h"
#include <QFileDialog>
#include <QListWidgetItem>
#include <QString>
#include <QWidget>

//#include "Sitrep.hpp"

class COMSelect : public UiWindow {
    Q_OBJECT

public:
    explicit COMSelect(QWidget* parent = nullptr,
        Comsguy* parentcom = nullptr,
        FCPCom* fcp = nullptr,
        ConfigManager* config_manger = nullptr);
    void removeCOM(int id);
    void loadState(QMap<QString, QVariant> config);
    explicit COMSelect(QWidget* parent = nullptr, Comsguy* parentcom = nullptr, FCPCom* fcp = nullptr);
    QVector<COM*>* get_coms();
    Ui::COMSelect* ui;
    ~COMSelect();

public slots:
    void updateFCP(FCPCom* fcp) override;
    void process_CAN_message(CANmessage msg) override;

private:
    Comsguy* ComsManager;
    ConfigManager* config_manger;
    QList<COMSelectLine*> coms;
    int count;
    QVector<COMSelectLine*> com_lines;
    // int TableCurrentRowCount;
    // QTableWidgetItem *TempItem;

private slots:
    void add_line(void);

    void on_profiles_clicked();

private:
    void closeEvent(QCloseEvent*) override;

    void on_saveStateButton_clicked();
};

#endif // COMSELECT_H
