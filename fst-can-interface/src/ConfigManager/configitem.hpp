#ifndef CONFIGITEM_HPP
#define CONFIGITEM_HPP

#include <QWidget>

namespace Ui {
class ConfigItem;
}

class ConfigItem : public QWidget {
    Q_OBJECT

public:
    explicit ConfigItem(QWidget* parent = nullptr);
    void setName(QString name);
    ~ConfigItem();

public slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::ConfigItem* ui;

signals:
    void on_pushed(QString config_name);
    void on_delete(QString config_name);
};

#endif // CONFIGITEM_HPP
