#include "configitem.hpp"
#include "ui_configitem.h"
#include <QDebug>

ConfigItem::ConfigItem(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::ConfigItem)
{
    ui->setupUi(this);
}

void ConfigItem::setName(QString name)
{
    ui->configName->setText(name);
}

ConfigItem::~ConfigItem()
{
    delete ui;
}

void ConfigItem::on_pushButton_clicked()
{
    emit on_pushed(ui->configName->text());
}

void ConfigItem::on_pushButton_2_clicked()
{
    emit on_delete(ui->configName->text());
}
