#ifndef CONFIGMANAGERDIALOG_HPP
#define CONFIGMANAGERDIALOG_HPP

#include <QWidget>
#include <UiWindow.hpp>

namespace Ui {
class ConfigManagerDialog;
}

class ConfigManagerDialog : public QWidget {
    Q_OBJECT

public:
    explicit ConfigManagerDialog(QWidget* parent = nullptr, UiWindow* current_widget = nullptr, ConfigManager* current_config = nullptr);
    void registerConfig(QString widget_name, QMap<QString, QVariant> config);
    void listConfigs();
    void loadConfigDialog(QString config_name);
    void deleteConfigDialog(QString config_name);
    void deleteListConfigs();
    ~ConfigManagerDialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::ConfigManagerDialog* ui;
    UiWindow* current_widget;
    ConfigManager* current_config;
};

#endif // CONFIGMANAGERDIALOG_HPP
