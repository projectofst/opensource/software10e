#ifndef CONFIGMANAGER_HPP
#define CONFIGMANAGER_HPP

#include <QDir>
#include <QInputDialog>
#include <QObject>
#include <QSettings>
#include <qdebug.h>

class ConfigManager : public QObject {

    Q_OBJECT

public:
    ConfigManager();
    void registerConfig(QString widget_name, QMap<QString, QVariant> config);
    void registerGlobalConfig(QMap<QString, QVariant> config);
    QList<QString> listConfigs(QString widget_name);
    QMap<QString, QVariant> readConfig(QString widget_name);
    QMap<QString, QVariant> readGlobalConfig();
    QMap<QString, QVariant> loadConfig(QString widget_name, QString config_name);
    void deleteConfig(QString widget_name, QString config_name);

private:
    QSettings* config_obj;
};

#endif // CONFIGMANAGER_HPP
