#include "configmanagerdialog.hpp"
#include "configitem.hpp"
#include "ui_configmanagerdialog.h"

ConfigManagerDialog::ConfigManagerDialog(QWidget* parent, UiWindow* current_widget, ConfigManager* current_config)
    : QWidget(parent)
    , ui(new Ui::ConfigManagerDialog)
{
    ui->setupUi(this);
    this->current_widget = current_widget;
    this->current_config = current_config;
    this->setWindowTitle("Configuration Manager - " + this->current_widget->objectName());
    this->move(current_widget->width() / 2, current_widget->height() / 2);
    listConfigs();
}

void ConfigManagerDialog::listConfigs()
{
    foreach (auto config_name, this->current_config->listConfigs(this->current_widget->objectName())) {
        ConfigItem* new_item = new ConfigItem();
        new_item->setName(config_name);
        ui->configList->addWidget(new_item);
        connect(new_item, &ConfigItem::on_pushed, this, &ConfigManagerDialog::loadConfigDialog);
        connect(new_item, &ConfigItem::on_delete, this, &ConfigManagerDialog::deleteConfigDialog);
    }
}

void ConfigManagerDialog::deleteListConfigs()
{
    QLayoutItem* item;
    while ((item = ui->configList->layout()->takeAt(0)) != nullptr) {
        delete item->widget();
        delete item;
    }
}

void ConfigManagerDialog::deleteConfigDialog(QString config_name)
{
    current_config->deleteConfig(this->current_widget->objectName(), config_name);
    deleteListConfigs();
    listConfigs();
}

void ConfigManagerDialog::loadConfigDialog(QString config_name)
{
    QMap<QString, QVariant> loaded_config = this->current_config->loadConfig(this->current_widget->objectName(), config_name);
    this->hide();
    this->current_widget->load_state(loaded_config);
}

void ConfigManagerDialog::registerConfig(QString widget_name, QMap<QString, QVariant> config)
{
    this->current_config->registerConfig(widget_name, config);
    deleteListConfigs();
    listConfigs();
}

ConfigManagerDialog::~ConfigManagerDialog()
{
    delete ui;
}

void ConfigManagerDialog::on_pushButton_clicked()
{
    this->current_widget->export_state();
}
