#include "configmanager.hpp"
#include <QMessageBox>

ConfigManager::ConfigManager()
{
    config_obj = new QSettings();
}

void ConfigManager::registerConfig(QString widget_name, QMap<QString, QVariant> config)
{
    bool ok;
    config_obj->beginGroup(widget_name);
    QString config_name = QInputDialog::getText(nullptr,
        tr("New Profile:"),
        tr("Profile Name:"), QLineEdit::Normal,
        "config" + QString::number(config_obj->allKeys().count()),
        &ok);
    if (!ok || config_name.isEmpty()) {
        config_obj->endGroup();
        return;
    }

    if (config_obj->childGroups().contains(config_name)) {
        QMessageBox sure;
        sure.setText("There is already a save with that name.");
        sure.setInformativeText("Do you wanna overwrite the last save?");
        sure.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        sure.setDefaultButton(QMessageBox::Yes);
        int ans = sure.exec();

        switch (ans) {
        case QMessageBox::Yes:
            break;

        case QMessageBox::No:
            config_obj->endGroup();
            return;
        }
    }

    config_obj->beginGroup(config_name);
    QMapIterator<QString, QVariant> i(config);
    while (i.hasNext()) {
        i.next();
        config_obj->setValue(i.key(), i.value());
    }
    config_obj->endGroup();
    config_obj->endGroup();
}

void ConfigManager::registerGlobalConfig(QMap<QString, QVariant> config)
{
    config_obj->beginGroup("GLOBAL_CONFIG");
    QMapIterator<QString, QVariant> i(config);
    while (i.hasNext()) {
        i.next();
        config_obj->setValue(i.key(), i.value());
    }
    config_obj->endGroup();
}

QList<QString> ConfigManager::listConfigs(QString widget_name)
{
    QList<QString> cfg_list;

    config_obj->beginGroup(widget_name);

    cfg_list.append("DEFAULT");

    cfg_list.append(config_obj->childGroups());

    config_obj->endGroup();

    return cfg_list;
}

QMap<QString, QVariant> ConfigManager::loadConfig(QString widget_name, QString config_name)
{
    QMap<QString, QVariant> config;
    config_obj->beginGroup(widget_name);

    if (config_name == "DEFAULT") {
        config_obj->endGroup();
        config["load_default_ui"] = true;
        qDebug() << "CONFIG_LOAD: load_default_ui == " << config["load_default_ui"].toBool();
        return config;
    }

    config_obj->beginGroup(config_name);
    foreach (auto key, config_obj->allKeys()) {
        qDebug() << "CONFIG_LOAD: " << key << " == " << config_obj->value(key);
        config[key] = config_obj->value(key);
    }
    config_obj->endGroup();
    config_obj->endGroup();
    return config;
}

void ConfigManager::deleteConfig(QString widget_name, QString config_name)
{
    config_obj->beginGroup(widget_name);
    config_obj->beginGroup(config_name);

    config_obj->remove("");

    config_obj->endGroup();
    config_obj->endGroup();
    return;
}

QMap<QString, QVariant> ConfigManager::readConfig(QString widget_name)
{
    QMap<QString, QVariant> config;
    bool ok;
    config_obj->beginGroup(widget_name);

    QList<QString> cfg_list;

    cfg_list.append("DEFAULT");

    cfg_list.append(config_obj->childGroups());

    QString config_name = QInputDialog::getItem(nullptr,
        tr("Choose profile:"),
        tr("Profile:"),
        cfg_list, 0, false, &ok);

    if (!ok || config_name.isEmpty() || config_name == "DEFAULT") {
        config_obj->endGroup();
        config["load_default_ui"] = true;
        qDebug() << "CONFIG_LOAD: load_default_ui == " << config["load_default_ui"].toBool();
        return config;
    }

    config_obj->beginGroup(config_name);
    foreach (auto key, config_obj->allKeys()) {
        qDebug() << "CONFIG_LOAD: " << key << " == " << config_obj->value(key);
        config[key] = config_obj->value(key);
    }
    config_obj->endGroup();
    config_obj->endGroup();
    return config;
}

QMap<QString, QVariant> ConfigManager::readGlobalConfig()
{
    QMap<QString, QVariant> config;

    config_obj->beginGroup("GLOBAL_CONFIG");

    foreach (auto key, config_obj->allKeys()) {
        qDebug() << "CONFIG_LOAD: " << key << " == " << config_obj->value(key);
        config[key] = config_obj->value(key);
    }

    config_obj->endGroup();
    return config;
}
