#include "General_Details.hpp"
#include "ui_General_Details.h"

#include "General_Details_PotProgress.hpp"

#include <QDebug>
#include <QLabel>
#include <QProgressBar>
#include <QSpinBox>
#include <QTimer>

#include <chrono> // std::chrono::seconds
#include <iostream> // std::cout, std::endl
#include <thread> // std::this_thread::sleep_for

#include "can-ids/Devices/INTERFACE_CAN.h"

QString danger = "QProgressBar::chunk {background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0,stop: 0 #FF0350,stop: 0.4999 #FF0020,stop: 0.5 #FF0019,stop: 1 #FF0000 );border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;border: .px solid black;}";
QString safe = "QProgressBar::chunk {background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0,stop: 0 #78d,stop: 0.4999 #46a,stop: 0.5 #45a,stop: 1 #238 );border-bottom-right-radius: 7px;border-bottom-left-radius: 7px;border: 1px solid black;}";

const char* titles[5] = { "apps 0", "apps 1", "pressure 0", "pressure 1", "eletric" };

#define PEDAL_MAX_VALUE 10000
GeneralDetails::GeneralDetails(QWidget* parent, FCPCom* fcp)
    : QDialog(parent)
    , ui(new Ui::GeneralDetails)
{
    ui->setupUi(this);

    this->setWindowTitle("General Details");

    limits = (uint16_t**)malloc(5 * sizeof(uint16_t*));
    for (int i = 0; i < 5; i++) {
        limits[i] = (uint16_t*)malloc(6 * sizeof(uint16_t));
        for (int j = 0; j < 6; j++) {
            limits[i][j] = 0;
        }
    }

    for (int i = 0; i < 5; i++) {
        PotProgress* p = new PotProgress(nullptr, QString(titles[i]));
        bars.append(p);
        ui->te_limit_layout->addWidget(p);
    }

    connect(&te_request_limit_timer, &QTimer::timeout, this, &GeneralDetails::te_request_async);

    this->_fcp = fcp;
}

void GeneralDetails::clear()
{

    ui->RTD_seq_dash->turnOff();
    ui->RTD_seq_dcu->turnOff();
    ui->RTD_seq_inv_1->turnOff();
    ui->RTD_seq_inv_2->turnOff();
    ui->RTD_seq_te->turnOff();

    ui->AIRm->turnOff();
    ui->AIRp->turnOff();
    ui->AMS->turnOff();
    ui->IMD->turnOff();
    ui->IMDL->turnOff();
}

GeneralDetails::~GeneralDetails()
{
    delete ui;
}

void GeneralDetails::updateRTDSequence(COMMON_MSG_RTD_ON rtd_msg)
{

    switch (rtd_msg.step) {
    case RTD_STEP_DASH_BUTTON:
        ui->RTD_seq_dash->turnGreen();
        break;
    case RTD_STEP_TE_OK:
        ui->RTD_seq_te->turnGreen();
        break;
    case RTD_STEP_INV_OK:
        ui->RTD_seq_inv_1->turnGreen();
        break;
    case RTD_STEP_DCU:
        ui->RTD_seq_dcu->turnGreen();
        break;
    case RTD_STEP_INV_GO:
        ui->RTD_seq_inv_2->turnGreen();
        break;
    case RTD_STEP_TE_NOK:
        ui->RTD_seq_te->turnRed();
        break;
    case RTD_STEP_INV_NOK:
        ui->RTD_seq_inv_1->turnRed();
        break;
    case RTD_STEP_DASH_LED:
        break;
    }
}
void GeneralDetails::updateDashLEDs(DASH_CAN_Data)
{
    /*
    if(data.status.AIR_plus)
        ui->AIRp->turnRed();

    else ui->AIRp->turnOff();

    if(data.status.AIR_minus)
        ui->AIRm->turnRed();

    else ui->AIRm->turnOff();

    if(data.status.AMS)
        ui->AMS->turnRed();

    else ui->AMS->turnOff();

    if(data.status.IMD)
        ui->IMD->turnRed();

    else ui->IMD->turnOff();

    if(data.status.IMD_latch)
        ui->IMDL->turnRed();

    else ui->IMDL->turnOff();

    if(data.status.RTD_mode)
      ui->RTD->turnRed();

    else ui->RTD->turnOff();
    */
}

void GeneralDetails::update_te_limits(TE_CAN_Data* data)
{
    uint16_t sensor = (data->limit_message.header >> 3) & 0x7;
    uint16_t limit = (data->limit_message.header) & 0x7;
    if (sensor >= 5) {
        return;
    }
    bars[sensor]->update_limits(static_cast<TE_THRESHOLD_LIMIT>(limit), data->limit_message.data_limit);
    return;
}

void GeneralDetails::updateTorqueEncoderDetails(TE_CAN_Data* data)
{

    double brake_bias = 0;
    /*Pedal Short Circuit*/
    if (data->main_message.status.CC_APPS_0)
        ui->CC_TPS1->turnRed();
    else
        ui->CC_TPS1->turnOff();

    if (data->main_message.status.CC_APPS_1)
        ui->CC_TPS2->turnRed();
    else
        ui->CC_TPS2->turnOff();

    if (data->main_message.status.CC_BPS_pressure_0)
        ui->CC_BPS1->turnRed();
    else
        ui->CC_BPS1->turnOff();

    if (data->main_message.status.CC_BPS_pressure_1)
        ui->CC_BPS2->turnRed();
    else
        ui->CC_BPS2->turnOff();

    if (data->main_message.status.CC_BPS_electric_0)
        ui->CC_BPEL->turnRed();
    else
        ui->CC_BPEL->turnOff();

    if (data->main_message.status.Overshoot_APPS_0)
        ui->apps0_over->turnRed();
    else
        ui->apps0_over->turnOff();

    if (data->main_message.status.Overshoot_APPS_1)
        ui->apps1_over->turnRed();
    else
        ui->apps1_over->turnOff();

    if (data->main_message.status.Overshoot_BPS_electric)
        ui->bps_electric_over->turnRed();
    else
        ui->bps_electric_over->turnOff();
    if (data->main_message.status.Implausibility_APPS_BPS_Timer_Exceeded) {
        ui->IMP_APPS_BPS->turnRed();
    } else
        ui->IMP_APPS_BPS->turnOff();

    if (data->main_message.status.Hard_braking) {
        ui->hard_braking_led->turnRed();
    } else {
        ui->hard_braking_led->turnOff();
    }
    ui->bp_bar_0->setText(QString::number(data->pressure_bar.pressure_F_bar / 10.0, 'f', 1));
    ui->bp_bar_1->setText(QString::number(data->pressure_bar.pressure_R_bar / 10.0, 'f', 1));
    if (2 * data->pressure_bar.pressure_F_bar + data->pressure_bar.pressure_F_bar != 0)
        brake_bias = ((2.0 * data->pressure_bar.pressure_F_bar) / (2.0 * data->pressure_bar.pressure_F_bar + data->pressure_bar.pressure_R_bar));
    ui->brake_bias->setText(QString::number(brake_bias));

    /*Pedal Implausibility*/
    if (data->main_message.status.Implausibility_APPS_Timer_Exceeded)
        ui->IMP_APPS_TIMER->turnRed();
    else
        ui->IMP_APPS_TIMER->turnOff();

    update_te_limits(data);

    update_progress_bars(data);
}

void GeneralDetails::update_progress_bars(TE_CAN_Data* data)
{
    bars[0]->update_value(data->verbose_message.apps_0);
    bars[1]->update_value(data->verbose_message.apps_1);
    bars[2]->update_value(data->verbose_message.pressure_0);
    bars[3]->update_value(data->verbose_message.pressure_1);
    bars[4]->update_value(data->verbose_message.electric);
}

void GeneralDetails::UpdateTE_STATUS(unsigned int TE_Message)
{
    QString str;

    if ((TE_Message & 0x2000) == 0) {
        ui->BrakeLight_LED->turnOff();
    } else {
        ui->BrakeLight_LED->turnGreen();
    }
}

void GeneralDetails::UpdateSupplies(CANmessage msg)
{
    try {
        if (this->_fcp->decodeUint64FromSignal(msg, "Detection_VCC_TSAL")) {
            ui->VCC_TSAL->turnGreen();
        } else {
            ui->VCC_TSAL->turnRed();
        }
        if (this->_fcp->decodeUint64FromSignal(msg, "Detection_VCC_FANS_AMS")) {
            ui->VCC_FANS_AMS->turnGreen();
        } else {
            ui->VCC_FANS_AMS->turnRed();
        }
        if (this->_fcp->decodeUint64FromSignal(msg, "Detection_VCC_CUA")) {
            ui->VCC_CUA->turnGreen();
        } else {
            ui->VCC_CUA->turnRed();
        }
        if (this->_fcp->decodeUint64FromSignal(msg, "Detection_VCC_DRS")) {
            ui->VCC_DRS->turnGreen();
        } else {
            ui->VCC_DRS->turnRed();
        }
        if (this->_fcp->decodeUint64FromSignal(msg, "Detection_VCC_PUMPS")) {
            ui->VCC_PUMP->turnGreen();
        } else {
            ui->VCC_PUMP->turnRed();
        }

        if (this->_fcp->decodeUint64FromSignal(msg, "Detecion_VCC_BL")) {
            ui->VCC_BL->turnGreen();
        } else {
            ui->VCC_BL->turnRed();
        }

        if (this->_fcp->decodeUint64FromSignal(msg, "Detection_VCC_AMS")) {
            ui->VCC_AMS->turnGreen();
        } else {
            ui->VCC_AMS->turnRed();
        }
        if (this->_fcp->decodeUint64FromSignal(msg, "Detection_VCC_EM")) {
            ui->VCC_EM->turnGreen();
        } else {
            ui->VCC_EM->turnRed();
        }
        if (this->_fcp->decodeUint64FromSignal(msg, "Detection_VCC_AMS")) {
            ui->VCC_FANS->turnGreen();
        } else {
            ui->VCC_FANS->turnRed();
        }
        if (this->_fcp->decodeUint64FromSignal(msg, "Detection_VCC_CUB")) {
            ui->VCC_CUB->turnGreen();
        } else {
            ui->VCC_CUB->turnRed();
        }

        if (this->_fcp->decodeUint64FromSignal(msg, "Detection_VCC_DATA")) {
            ui->VCC_DATA->turnGreen();
        } else {
            ui->VCC_DATA->turnRed();
        }
    } catch (JsonException& e) {
        qDebug() << e.toString();
    }
    return;
}

void GeneralDetails::clear_rtd_steps()
{

    ui->RTD_seq_dash->turnOff();

    ui->RTD_seq_te->turnOff();

    ui->RTD_seq_inv_1->turnOff();

    ui->RTD_seq_dcu->turnOff();

    ui->RTD_seq_inv_2->turnOff();

    ui->RTD_seq_te->turnOff();

    ui->RTD_seq_inv_1->turnOff();

    return;
}

void GeneralDetails::te_request_limits()
{
    te_request_limit_timer.start(10);
    return;
}

void GeneralDetails::te_request_async()
{

    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = MSG_ID_INTERFACE_TE_LIMITS_REQUEST;
    msg.candata.dlc = 4;
    for (int i = 0; i < 4; i++) {
        msg.candata.data[i] = 0;
    }

    msg.candata.data[0] = static_cast<uint16_t>(sensor << 3) + limit;

    emit send_CAN(msg);

    if (limit == 5 && sensor == 4) {
        te_request_limit_timer.stop();
    }

    if (limit == 4) {
        sensor = (sensor + 1) % 5;
    }
    limit = (limit + 1) % 6;
}

void GeneralDetails::on_update_limits_clicked()
{
    te_request_limits();
    return;
}

void GeneralDetails::on_send_hard_braking_lim_clicked()
{
    CANdata message;
    message.dev_id = DEVICE_ID_INTERFACE;
    message.msg_id = CMD_ID_COMMON_SET;
    message.dlc = 8;
    message.data[0] = DEVICE_ID_TE;
    message.data[1] = P_TE_HARD_BRAKING_PRESS_THRS; // parameter number
    message.data[2] = uint16_t(ui->hard_braking_input->text().toDouble() * 10);
    CANmessage msg;
    msg.candata = message;
    emit send_CAN(msg);
}
