#ifndef INVERTERS_H
#define INVERTERS_H

#include "COM.hpp"
#include "Exceptions/helper.hpp"
#include "Resources/configslist.h"
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"
#include "fcpcom.hpp"
#include "iib_motor_control.hpp"
#include "state_manager.hpp"
#include <QChartView>
#include <QLineSeries>
#include <QTableView>
#include <QVBoxLayout>
#include <QWidget>
#include <QtCharts>

#ifndef MOTORS
#define MOTORS
#define FRONTLEFT 0
#define FRONTRIGHT 1
#define REARLEFT 2
#define REARRIGHT 3
#endif
extern const char* MODES[];
extern int PARAMETERS[];

namespace Ui {
class Inverters;
}

class Inverters : public UiWindow {
    Q_OBJECT

public:
    explicit Inverters(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    void setup_fcp_params();
    void updateFCP(FCPCom* fcp) override;
    QString getTitle() override { return "IIB"; };
    void loadState(QMap<QString, QVariant> config);
    ~Inverters() override;

private slots:
    void process_CAN_message(CANmessage message) override;
    void on_pushButton_5_clicked();
    void timer_clear_inverters();
    void clear();
    void on_rtd_button_clicked();
    void on_set_mode_clicked();
    void on_specific_val_textEdited(const QString& arg1);
    void on_parameter_currentIndexChanged(int index);
    void check_spec_val(double val);
    void on_set_new_pow_tor_clicked();
    void on_Set_motor_speed_clicked();
    void on_motor_speed_textChanged(const QString& arg1);
    void on_set_regen_clicked();
    void on_debug_but_toggled(bool checked);
    void on_set_all_iib_clicked();
    void on_set_torque_front_clicked();
    void on_set_torque_rear_clicked();
    void on_save_button_clicked();
    void save_settings();
    void load_settings();
    void on_save_clicked();
    void on_load_clicked();
    void updateMotorInfo(CANmessage msg);
    void updateInverterInfo(CANmessage message);
    void updateGeneralInverterInfo(CANmessage message);
    void updateInverterLimits(CANmessage message);
    void updateGeneralLimitInfo(CANmessage message);
    void updateCarInfo(CANmessage message);
    void updateGeneralIIBinfo(CANmessage message);
    void verifyAnswers(CANmessage message);
    void updateAnswers(QString id, int data);
    void updateException(JsonException& e);
    void on_saveState_clicked();
    void handle_get(CANmessage);
    void handle_set(CANmessage);

private:
    Ui::Inverters* ui;
    int clear_inv_flag;
    int Inv_cleared;
    int regen_on;
    state_manager<int>* manager;
    QString m_sSettingsFile, specific;
    QString _jsonFileName;
    Helper* logger;
    ConfigManager* config_manager;
    FCPCom* fcp;
    ConfigsList* configs_list;
    Message* set = nullptr;
    Message* get = nullptr;
};

#endif // INVERTERS_H
