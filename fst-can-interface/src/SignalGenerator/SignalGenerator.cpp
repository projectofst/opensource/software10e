#include "SignalGenerator.hpp"

SignalGenerator::SignalGenerator(QObject* parent, QString functionExpression, int start, int end, double step, int delay)
    : QObject(parent)
{
    this->_functionExpression = functionExpression.toStdString();
    this->_workingVariable = this->checkForWorkingVariable();
    this->_start = start;
    this->_end = end;
    this->_step = step;
    this->_delay = delay;
    this->_currentSample = (double)start;
    this->_timer.setInterval(delay);

    connect(&this->_timer, &QTimer::timeout, this, &SignalGenerator::calculateNextPoint);
}

QList<std::pair<int, double>> SignalGenerator::calculateAllPointsAsList()
{
    QList<std::pair<int, double>> returnList;
    double current = this->_start;
    while (current <= this->_end) {
        /* returnList.append(std::pair<int, double>(current, calculateValue())); */
        current += this->_step;
    }
    this->_currentSample = this->_start;
    return returnList;
}

void SignalGenerator::start()
{
    this->_timer.start();
}

void SignalGenerator::stop()
{
    this->_timer.stop();
}
void SignalGenerator::reset()
{
    this->_timer.stop();
    this->_currentSample = this->_start;
}

void SignalGenerator::setStart(int start)
{
    this->_start = start;
}

void SignalGenerator::setEnd(int end)
{
    this->_end = end;
}

void SignalGenerator::setStep(int step)
{
    this->_step = step;
}

void SignalGenerator::setDelay(int delay)
{
    this->_delay = delay;
}

double SignalGenerator::calculateNextPoint()
{
    /*
    double nextValue = this->calculateValue();
    emit this->newValue(this->_currentSample, nextValue);
    this->_currentSample += this->_step;
    if(this->_currentSample >= this->_end){
        this->_timer.stop();
        this->_currentSample = this->_start;
    }

    return nextValue;
    */
    return -1;
}

void SignalGenerator::changeFunction(QString newExpression)
{
    this->_functionExpression = newExpression.toStdString();
}

/*
double SignalGenerator::calculateValue(){
    exprtk::symbol_table<double> symbolTable;

    symbolTable.add_variable("x", this->_currentSample);
    exprtk::expression<double> expression;
    expression.register_symbol_table(symbolTable);

    exprtk::parser<double> parser;

    if(!parser.compile(this->_functionExpression, expression)){
        throw ExprtkCompilationErrorException(QString::fromStdString(this->_functionExpression));
        return 0.0;
    }

    return expression.value();
}
*/

std::string SignalGenerator::checkForWorkingVariable()
{
    for (auto it = this->_functionExpression.begin(); it != this->_functionExpression.end(); it++) {
        if ((int)(*it) >= 'A' && (int)(*it) < 'z') {
            std::string str(&*it);
            return str;
        }
    }
    return "x";
}
