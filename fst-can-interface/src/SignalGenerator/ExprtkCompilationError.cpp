#include "ExprtkCompilationError.hpp"

ExprtkCompilationErrorException::ExprtkCompilationErrorException(QString expression)
{
    this->expression = expression;
}

QString ExprtkCompilationErrorException::toString()
{
    return "ExprtkCompilationError:: Custom Expression Compilation Error: Expression not valid: " + this->expression + ".";
}
