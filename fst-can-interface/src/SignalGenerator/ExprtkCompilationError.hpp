#ifndef EXPRTKCOMPILATIONERROR_HPP
#define EXPRTKCOMPILATIONERROR_HPP
#include <QString>
#include <exception>

class ExprtkCompilationErrorException : std::exception {
public:
    ExprtkCompilationErrorException(QString expression);

    QString toString();

private:
    QString expression;
};

#endif // EXPRTKCOMPILATIONERROR_HPP
