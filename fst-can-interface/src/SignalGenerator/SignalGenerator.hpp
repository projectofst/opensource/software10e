#ifndef SIGNALGENERATOR_HPP
#define SIGNALGENERATOR_HPP

#include "SignalGenerator/ExprtkCompilationError.hpp"
#include <QDebug>
#include <QObject>
#include <QTimer>
#include <functional>
#include <utility>
class SignalGenerator : public QObject {
    Q_OBJECT
public:
    explicit SignalGenerator(QObject* parent = nullptr, QString functioExpression = "x", int start = 0, int end = 0, double step = 1.0, int delay = 0);

    QList<std::pair<int, double>> calculateAllPointsAsList();

    void start();
    void stop();
    void reset();

    void setStart(int start);
    void setEnd(int start);
    void setStep(int step);
    void setDelay(int delay);
    void changeFunction(QString newExpression);

private:
    int _start;
    int _end;
    double _step;
    int _delay;
    double _currentSample;
    std::string _functionExpression;
    std::string _workingVariable;
    QTimer _timer;

private slots:

    double calculateNextPoint();

    std::string checkForWorkingVariable();

signals:
    void newValue(int x, double y);
};

#endif // SIGNALGENERATOR_HPP
