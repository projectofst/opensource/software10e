#ifndef TIME_HPP
#define TIME_HPP

#include "stdint.h"

class Time {
public:
    Time(uint64_t s, uint64_t us);
    Time operator-();
    Time operator-(Time rhs);
    Time operator+(Time rhs);

private:
    uint64_t us;
    uint64_t s;
};

#endif // TIME_HPP
