#include "Main_Toolbar.hpp"

MainToolbar::MainToolbar(MainWindow* parent)
    : MainWindow(parent)
{
    toolbar = new QToolBar;
    toolbar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    this->mainwindow = parent;

    // COM
    COM = new QAction(this);
    COM->setObjectName("actionToggleCOM");
    COM->setIcon(QIcon(":/CustomIcons/CustomIcons/link.png"));
    COM->setToolTip("Toggle Comunication Selector (Alt+C)");
    COM->setText("Coms");
    COM->setShortcut(QKeySequence(Qt::ALT + Qt::Key_C));
    // COM->setCheckable(true);
    toolbar->addAction(COM);
    connect(COM, &QAction::triggered, this, &MainToolbar::launch_coms);

    // Separator
    toolbar->addSeparator();

    // Console
    temp = new QAction(this);
    temp->setObjectName("Console");
    temp->setIcon(QIcon(":/CustomIcons/CustomIcons/terminal.png"));
    temp->setToolTip("Create Console Widget (Alt+T)");
    temp->setText("Console");
    temp->setShortcut(QKeySequence(Qt::ALT + Qt::Key_T));
    toolbar->addAction(temp);
    connect(temp, SIGNAL(triggered()), this, SLOT(widgetAction()));

    // DevStatus
    QIcon* tempicon = new QIcon(":/CustomIcons/CustomIcons/devices.png");
    temp = new QAction(this);
    temp->setObjectName("Device Status");
    temp->setIcon(*tempicon);
    temp->setText("Devices");
    temp->setToolTip("Create Device Status Widget (Alt+D)");
    temp->setShortcut(QKeySequence(Qt::ALT + Qt::Key_D));
    toolbar->addAction(temp);
    connect(temp, SIGNAL(triggered()), this, SLOT(widgetAction()));

    // PCB Versions
    temp = new QAction(this);
    temp->setObjectName("Versions");
    QPixmap bgPixmap(":/CustomIcons/CustomIcons/versions.png");
    QPixmap scaled = bgPixmap.scaled(QSize(800, 800), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    temp->setIcon(QIcon(scaled));
    temp->setToolTip("Create PCB Versions Widget (Alt+T)");
    temp->setText("Versions");
    temp->setShortcut(QKeySequence(Qt::ALT + Qt::Key_V));
    toolbar->addAction(temp);
    connect(temp, SIGNAL(triggered()), this, SLOT(widgetAction()));

    // PlotUi
    tempicon = new QIcon(":/CustomIcons/CustomIcons/line-graphic.png");
    temp = new QAction(this);
    temp->setObjectName("PlotUi");
    temp->setIcon(*tempicon);
    temp->setText("Plot");
    temp->setToolTip("Open Plots (Alt+P");
    temp->setShortcut(QKeySequence(Qt::ALT + Qt::Key_P));
    toolbar->addAction(temp);
    connect(temp, &QAction::triggered, this, &MainToolbar::launch_plotjuggler);

    // General Widget
    QPixmap bgPixmap1(":/CustomIcons/CustomIcons/car.png");
    QPixmap scaled1 = bgPixmap1.scaled(QSize(700, 700), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    temp = new QAction(this);
    temp->setObjectName("general_widget");
    temp->setIcon(QIcon(scaled1));
    temp->setText("GW");
    temp->setToolTip("Open GW (Alt+G");
    temp->setShortcut(QKeySequence(Qt::ALT + Qt::Key_G));
    toolbar->addAction(temp);
    connect(temp, SIGNAL(triggered()), this, SLOT(widgetAction()));

    // Log Console
    tempicon = new QIcon(":/CustomIcons/CustomIcons/log.png");
    temp = new QAction(this);
    temp->setObjectName("Log");
    temp->setIcon(*tempicon);
    temp->setText("Log");
    temp->setToolTip("Log (Alt+L)");
    temp->setShortcut(QKeySequence(Qt::ALT + Qt::Key_L));
    toolbar->addAction(temp);
    connect(temp, SIGNAL(triggered()), this, SLOT(widgetAction()));

    tempicon = new QIcon(":/CustomIcons/CustomIcons/fcp.png");
    temp = new QAction(this);
    temp->setObjectName("Config");
    temp->setIcon(*tempicon);
    temp->setText("Run cmd/cfg");
    temp->setToolTip("Run cmd/cfg (Alt+F)");
    temp->setShortcut(QKeySequence(Qt::ALT + Qt::Key_F));
    toolbar->addAction(temp);
    connect(temp, SIGNAL(triggered()), this, SLOT(widgetAction()));

    // Separator
    toolbar->addSeparator();

    // Lap Widget
    tempicon = new QIcon(":/CustomIcons/CustomIcons/stopwatch_blue.png");
    temp = new QAction(this);
    temp->setObjectName("Lap");
    temp->setIcon(*tempicon);
    temp->setText("Lap");
    temp->setToolTip("Create Lap Widget (Alt+S)");
    temp->setShortcut(QKeySequence(Qt::ALT + Qt::Key_S));
    toolbar->addAction(temp);
    connect(temp, SIGNAL(triggered()), this, SLOT(widgetAction()));

    /* TimeLine
    tempicon = new QIcon(":/CustomIcons/CustomIcons/stopwatch_blue.png");
    temp = new QAction(this);
    temp->setObjectName("Timeline");
    temp->setIcon(*tempicon);
    temp->setText("Timeline");
    temp->setToolTip("Create Timeline Widget (Alt+M)");
    temp->setShortcut(QKeySequence(Qt::ALT + Qt::Key_M ));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));
    */

    // Separator
    toolbar->addSeparator();

    // Save Layout
    tempicon = new QIcon(":/CustomIcons/CustomIcons/layout.png");
    temp = new QAction(this);
    temp->setObjectName("saveLayout");
    temp->setIcon(*tempicon);
    temp->setText("Save");
    temp->setToolTip("Save");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S));
    toolbar->addAction(temp);
    connect(temp, SIGNAL(triggered()), this, SLOT(widgetAction()));
    temp->setDisabled(true);

    // Delete Layout
    tempicon = new QIcon(":/CustomIcons/CustomIcons/garbage.png");
    temp = new QAction(this);
    temp->setObjectName("deleteLayout");
    temp->setIcon(*tempicon);
    temp->setText("Delete");
    temp->setToolTip("Delete");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_D));
    toolbar->addAction(temp);
    connect(temp, SIGNAL(triggered()), this, SLOT(widgetAction()));
    temp->setDisabled(true);

    // LayoutCombobox
    LayoutCombo = new QComboBox;
    toolbar->addWidget(LayoutCombo);
    LayoutCombo->setMinimumWidth(100);

    LayoutCombo->setDisabled(true);

    toolbar->setObjectName("MainToolBar");
    parent->addToolBar(toolbar);
    if (QSysInfo::productType() == "osx")
        parent->setUnifiedTitleAndToolBarOnMac(true);
}

void MainToolbar::widgetAction()
{
    QString type = QObject::sender()->objectName();
    if (type == "saveLayout") {
        emit saveLayout();
    } else if (type == "deleteLayout") {
        emit deleteLayout();
    } else {
        emit openUiWindow(type, false);
    }
}

void MainToolbar::launch_plotjuggler()
{
#if defined(Q_OS_WIN)
    QString file = "";
    return;
#else
    QString file = "/usr/local/bin/plotjuggler";
#endif
    if (QFileInfo::exists(file)) {
        QProcess* process = new QProcess(this);
        process->start(file);
    } else {
        QMessageBox::critical(nullptr, tr("Plot Juggler Missing"), tr("Could not find PlotJuggler\nInstall it here: https://github.com/facontidavide/PlotJuggler\nIf this error persists, ensure that plotjuggler is at:\n/usr/local/bin/plotjuggler"));
    }
}

void MainToolbar::launch_coms()
{
    mainwindow->comselect->show();
    mainwindow->comselect->raise();
    /*
    static bool launched = false;
    if (!launched) {
        mainwindow->comselect->show();
        mainwindow->comselect->raise();
    }
    else {
        mainwindow->comselect->close();
    }

    launched = !launched;
    */
}
