#include "run_laps.hpp"
#include "ui_run_laps.h"

run_laps::run_laps(QWidget* parent, int new_track_length, QString new_track_type)
    : QWidget(parent)
    , ui(new Ui::run_laps)
{
    ui->setupUi(this);
    track_length = new_track_length;

    cones_map["Endurance"] = 2.0 * 1000;
    cones_map["Skidpad"] = 0.2 * 1000;
    cones_map["Autocross"] = 2.0 * 1000;

    OC_map["Endurance"] = 10.0 * 1000;
    OC_map["Autocross"] = 10.0 * 1000;

    track_type = new_track_type;
    clear_gen_info();
    best_lap = nullptr;
    DNF_flag = 0;
}

void run_laps::add_lap(Single_lap* new_lap)
{

    if (!DNF_flag) {
        laps.append(new_lap);
        ui->place_to_add_lap->addWidget(new_lap);
    }

    if (new_lap->get_DNF_flag() || DNF_flag) {
        DNF_flag = 1;
    } else {
        update_general_info();
    }
    return;
}

Single_lap* run_laps::lap_number(int i)
{
    if (i < laps.size()) {
        return laps[i];
    } else {
        return nullptr;
    }
}
run_laps::~run_laps()
{
    delete ui;
}

void run_laps::update_general_info()
{
    int total_laps, total_distance;
    double avg_speed, avg_time, total_time, total_time_w_pen;
    QString avg_speed_string, avg_time_string;
    total_laps = total_distance = avg_speed = avg_time = total_time = total_time_w_pen = 0;

    for (int i = 0; i < laps.size(); i++) {
        total_laps++;
        total_distance += track_length;
        total_time += laps[i]->get_time();
        total_time_w_pen += laps[i]->get_cones() * cones_map[track_type];
    }
    total_time_w_pen += total_time;
    avg_speed = (total_distance / 1000.0) / ((total_time / 1000.0) / 3600.0);
    avg_time = total_time / total_laps;
    ui->total_distance->setText(QString::number(total_distance) + " m");
    ui->avg_speed->setText(QString::number(avg_speed) + " km/h");
    ui->total_laps->setText(QString::number(total_laps));
    ui->avg_time->setText(transform_time(avg_time) + "s");
    ui->total_time->setText(transform_time(total_time) + "s");
    ui->total_time_w_pen->setText(transform_time(total_time_w_pen) + "s");
}

QList<qint64> run_laps::format_time(qint64 time)
{

    QList<qint64> times;
    qint64 miliseconds, seconds, minutes;

    miliseconds = time % 1000;
    time = qint64(time / 1000);
    seconds = time % 60;
    time = qint64(time / 60);
    minutes = time;

    times.append(minutes);
    times.append(seconds);
    times.append(miliseconds);

    return times;
}

QString run_laps::transform_time(qint64 time)
{
    char pretty_time[32];
    QList<qint64> times;

    times = format_time(time);
    sprintf(pretty_time, "%02lld:%02lld.%03lld", times[0], times[1], times[2]);
    QString x = pretty_time; /*macacada*/
    return x;
}

int run_laps::get_number_laps()
{
    return laps.size();
}

void run_laps::remove_lap(int index)
{
    laps.remove(index);
    if (!laps.size()) {
        clear_gen_info();
        return;
    }
    update_general_info();
}

void run_laps::clear_gen_info()
{
    ui->total_distance->setText("0 m");
    ui->avg_speed->setText("0 km/h");
    ui->total_laps->setText("0");
    ui->avg_time->setText("0");
    ui->total_time->setText("0 s");
    ui->total_time_w_pen->setText("0 s");
    return;
}

void run_laps::on_clear_clicked()
{
}

void run_laps::set_new_fastest_lap(Single_lap* lap)
{
    best_lap = lap;
    return;
}

Single_lap* run_laps::get_fastest_lap()
{
    return best_lap;
}

void run_laps::set_track(QString new_track)
{
    track_type = new_track;
    update_general_info();
    return;
}
void run_laps::set_length(int new_length)
{
    track_length = new_length;
    update_general_info();
    return;
}

void run_laps::on_clear_all_clicked()
{
    while (laps.size()) {
        for (int var = 0; var < laps.size(); var++) {
            emit laps[var]->ui->delete_button->clicked();
        }
    }
    clear_gen_info();
    return;
}

int run_laps::get_dnf_flag()
{
    return DNF_flag;
}

void run_laps::set_DNF_flag(int flag)
{
    DNF_flag = flag;
    return;
}
