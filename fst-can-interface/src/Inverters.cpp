#include "Inverters.hpp"
#include "ui_Inverters.h"
#include <stdio.h>

#include <QChartView>
#include <QLineSeries>
#include <QTableView>
#include <QThread>
#include <QVBoxLayout>
#include <QtCharts>

#include "Resources/LEDwidget.hpp"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/ARM_CAN.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/Devices/IIB_CAN.h"
#include "can-ids/Devices/INTERFACE_CAN.h"

#include "COM.hpp"
#include "ConfigWindow/setline.h"

#include "iib_motor_control.hpp"
#include "ui_iib_motor_control.h"

using namespace QtCharts;

// static int current_mode = MODE_TRACTION_LIMIT;

const char* MODES[] = {
    "ARM",
    "DEFAULT",
    "TEST BENCH",
    "ERROR"
};

Inverters::Inverters(QWidget* parent, FCPCom* fcp, Helper* log, ConfigManager* config_manager)
    : UiWindow(parent, fcp)
    , ui(new Ui::Inverters)
{
    this->fcp = fcp;
    ui->setupUi(this);
    this->logger = log;
    specific = "";
    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Inverters::timer_clear_inverters);
    timer->start(2000);

    clear_inv_flag = 0;
    Inv_cleared = 0;
    regen_on = 0;

    manager = new state_manager<int>();

    m_sSettingsFile = QDir::currentPath() + "/set/states/" + "IIB" + "_" + specific.toUpper() + "_settings.ini";

    this->config_manager = config_manager;

    loadState(config_manager->readConfig(this->objectName()));

    this->configs_list = new ConfigsList(this, this->fcp);
    this->configs_list->list_configs("iib");
    this->ui->params_area->addWidget(this->configs_list);
    connect(this->configs_list, &ConfigsList::send_to_CAN, this, [this](QByteArray& msg) { emit send_to_CAN(msg); });

    auto common = this->fcp->getCommon();
    this->get = common->getMessage("ans_get");
    this->set = common->getMessage("ans_set");
}

Inverters::~Inverters()
{
    delete ui;
}

void Inverters::updateGeneralInverterInfo(CANmessage message)
{
    QPair<QString, QMap<QString, double>> info;
    try {
        info = this->_fcp->decodeMessage(message);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
    QString messageName = info.first;
    QMap<QString, double> signalValues = info.second;

    if (messageName == "iib_inv") {
        int inverterNumber = signalValues.value("n_inv_info", 4);
        bool BE_1 = signalValues.value("BE_1", 0);
        bool BE_2 = signalValues.value("BE_2", 0);
        bool inverterActive = signalValues.value("inv_active", 0);
        switch (inverterNumber) {
        case FRONTLEFT:
            if (BE_1)
                ui->IN_1_BE_1->turnGreen();
            else
                ui->IN_1_BE_1->turnOff();
            if (BE_2)
                ui->IN_1_BE_2->turnGreen();
            else
                ui->IN_1_BE_2->turnOff();
            if (inverterActive)
                ui->iib_inv_1_led->turnGreen();
            else
                ui->iib_inv_1_led->turnOff();
            return;
        case FRONTRIGHT:
            if (BE_1)
                ui->IN_2_BE_1->turnGreen();
            else
                ui->IN_2_BE_1->turnOff();
            if (BE_2)
                ui->IN_2_BE_2->turnGreen();
            else
                ui->IN_2_BE_2->turnOff();
            if (inverterActive)
                ui->iib_inv_2_led->turnGreen();
            else
                ui->iib_inv_2_led->turnOff();
            return;
        case REARLEFT:
            if (BE_1)
                ui->IN_3_BE_1->turnGreen();
            else
                ui->IN_3_BE_1->turnOff();
            if (BE_2)
                ui->IN_3_BE_2->turnGreen();
            else
                ui->IN_3_BE_2->turnOff();
            if (inverterActive)
                ui->iib_inv_3_led->turnGreen();
            else
                ui->iib_inv_3_led->turnOff();
            return;
        case REARRIGHT:
            if (BE_1)
                ui->IN_4_BE_1->turnGreen();
            else
                ui->IN_4_BE_1->turnOff();
            if (BE_2)
                ui->IN_4_BE_2->turnGreen();
            else
                ui->IN_4_BE_2->turnOff();
            if (inverterActive)
                ui->iib_inv_4_led->turnGreen();
            else
                ui->iib_inv_4_led->turnOff();
            return;
        }
    }
}

void Inverters::updateGeneralLimitInfo(CANmessage message)
{
    QPair<QString, QMap<QString, double>> info;
    try {
        info = this->_fcp->decodeMessage(message);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        return;
    }
    QString messageName = info.first;
    QMap<QString, double> signalValues = info.second;

    if (messageName == "iib_limits") {
        int inverterNumber = signalValues.value("n_inv_limits", 4);
        double maxOpPower = signalValues.value("max_op_power", 0.0);
        double maxOpTorque = signalValues.value("max_op_torque", 0.0);
        switch (inverterNumber) {
        case FRONTLEFT:
            ui->FL_info_power->setText(QString::number(maxOpPower, 'f', 1));
            ui->FL_info_torque->setText(QString::number(maxOpTorque, 'f', 1));
            return;
        case FRONTRIGHT:
            ui->FR_info_power->setText(QString::number(maxOpPower, 'f', 1));
            ui->FR_info_torque->setText(QString::number(maxOpTorque, 'f', 1));
            return;
        case REARLEFT:
            ui->RL_info_power->setText(QString::number(maxOpPower, 'f', 1));
            ui->RL_info_torque->setText(QString::number(maxOpTorque, 'f', 1));
            return;
        case REARRIGHT:
            ui->RR_info_power->setText(QString::number(maxOpPower, 'f', 1));
            ui->RR_info_torque->setText(QString::number(maxOpTorque, 'f', 1));
            return;
        }
    }
}

void Inverters::updateMotorInfo(CANmessage message)
{
    try {
        this->ui->inverters_widget->updateInformation(this->_fcp->decodeMessage(message));
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
}

void Inverters::updateInverterInfo(CANmessage message)
{
    updateGeneralInverterInfo(message);
    try {
        this->ui->inverters_widget->updateInformation(this->_fcp->decodeMessage(message));
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
}

void Inverters::updateInverterLimits(CANmessage message)
{
    updateGeneralLimitInfo(message);
    try {
        this->ui->inverters_widget->updateInformation(this->_fcp->decodeMessage(message));
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }

    try {
        auto msg = this->_fcp->decodeMessage(message);

        if (msg.first == "iib_limits") {
            this->ui->iib_pos_torque_limit_led->turnGreenIf(msg.second.value("pos_torque_limiter", 0));
            this->ui->iib_neg_torque_limit_led->turnGreenIf(msg.second.value("neg_torque_limiter", 0));
            this->ui->iib_pos_power_limit_led->turnGreenIf(msg.second.value("pos_power_limiter", 0));
            this->ui->iib_neg_power_limit_led->turnGreenIf(msg.second.value("neg_power_limiter", 0));
            this->ui->iib_electric_power_limit_led->turnGreenIf(msg.second.value("electric_power_limiter", 0));
            this->ui->iib_max_rpm_limit_led->turnGreenIf(msg.second.value("max_rpm_limiter", 0));
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        qDebug() << "Error catching torque limiters message";
    }
}

void Inverters::updateGeneralIIBinfo(CANmessage message)
{
    QPair<QString, QMap<QString, double>> info;
    try {
        info = this->_fcp->decodeMessage(message);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }

    QString messageName = info.first;
    QMap<QString, double> signalValues = info.second;

    if (messageName == "iib_info") {

        double maxSafetyRpm = signalValues.value("max_sfty_rpm", 0.0);
        double airTemp = signalValues.value("iib_air_tmp", 0.0);
        double dischTemp = signalValues.value("iib_disch_temp", 0.0);
        bool isFanOn = int(signalValues.value("iib_fan_on", 0.0));
        bool invRTD = int(signalValues.value("inv_rtd", 0.0));
        bool invON = int(signalValues.value("inv_on", 0.0));
        bool cuaPower = int(signalValues.value("cua_power", 0.0));
        bool cubPower = int(signalValues.value("cub_power", 0.0));
        bool regenOn = int(signalValues.value("regen_on", 0.0));

        ui->air_temp->setText(QString::number(airTemp));
        ui->fan_speed->setText(QString::number(maxSafetyRpm));
        ui->rpm_info->setText(QString::number(maxSafetyRpm));
        ui->iib_temp->setText(QString::number(dischTemp));
        if (isFanOn) {
            ui->fan_on_LED->turnGreen();
        } else {
            ui->fan_on_LED->turnOff();
        }
        if (invRTD) {
            ui->iib_inv_rtd_led->turnGreen();
        } else {
            ui->iib_inv_rtd_led->turnOff();
        }
        if (invON) {
            ui->iib_inv_on_led->turnGreen();
        } else {
            ui->iib_inv_on_led->turnOff();
        }
        if (cuaPower) {
            ui->EFA_LED->turnGreen();
        } else {
            ui->EFA_LED->turnOff();
        }
        if (cubPower) {
            ui->EFB_LED->turnGreen();
        } else {
            ui->EFB_LED->turnOff();
        }
        if (regenOn) {
            ui->REGEN_LED->turnGreen();
            regen_on = 1;
        } else {
            ui->REGEN_LED->turnOff();
            regen_on = 0;
        }
        return;
    }
}

void Inverters::updateCarInfo(CANmessage message)
{
    QPair<QString, QMap<QString, double>> info;
    try {
        info = this->_fcp->decodeMessage(message);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }

    QString messageName = info.first;
    QMap<QString, double> signalValues = info.second;

    if (messageName == "iib_car") {

        bool teOk = int(signalValues.value("car_te_ok"));
        bool armOk = int(signalValues.value("car_arm_ok"));
        bool sdc1 = int(signalValues.value("car_sdc1"));
        bool sdc2 = int(signalValues.value("car_sdc2"));
        bool carRTD = int(signalValues.value("car_rtd"));
        bool HV = int(signalValues.value("inv_hv"));
        int inputMode = signalValues.value("iib_input_mode");

        if (inputMode < 4) {
            ui->mode->setText(MODES[inputMode]);
            ui->mode_info->setText(MODES[inputMode]);
        } else {
            char mode[64];
            sprintf(mode, "NOT A VALID MODE %d", inputMode);
            ui->mode->setText(mode);
            ui->mode_info->setText(mode);
        }
        if (teOk) {
            ui->TE_OK_LED->turnGreen();
        } else {
            ui->TE_OK_LED->turnOff();
        }
        if (armOk) {
            ui->ARM_OK_LED->turnGreen();
        } else {
            ui->ARM_OK_LED->turnOff();
        }
        if (sdc1) {
            ui->SDC_1_LED->turnGreen();
        } else {
            ui->SDC_1_LED->turnOff();
        }
        if (sdc2) {
            ui->SDC_2_LED->turnGreen();
        } else {
            ui->SDC_2_LED->turnOff();
        }
        if (carRTD) {
            ui->CAR_RTD_LED->turnGreen();
        } else {
            ui->CAR_RTD_LED->turnOff();
        }
        if (HV) {
            ui->INV_HV_LED->turnGreen();
        } else {
            ui->INV_HV_LED->turnOff();
        }
    }
    return;
}

void updateLabel(QLabel* label, double data)
{
    label->setText(QString::number(data));
    return;
}

void Inverters::updateAnswers(QString id, int data)
{
    if (id == "debug_mode") {
        if (data)
            this->ui->debug_info->setText("ON");
        else
            this->ui->debug_info->setText("OFF");
    } else if (id == "regen_on") {
        if (data)
            this->ui->REGEN_LED->turnGreen();
        else
            this->ui->REGEN_LED->turnRed();
    } else if (id == "torque_fl") {
        updateLabel(ui->FL_info_torque, data);
    } else if (id == "torque_fr") {
        updateLabel(ui->FR_info_torque, data);
    } else if (id == "torque_rr") {
        updateLabel(ui->RR_info_torque, data);
    } else if (id == "torque_rl") {
        updateLabel(ui->RL_info_torque, data);
    } else if (id == "pwr_fl") {
        updateLabel(ui->FL_info_power, data);
    } else if (id == "pwr_fr") {
        updateLabel(ui->FR_info_power, data);
    } else if (id == "pwr_rr") {
        updateLabel(ui->RR_info_power, data);
    } else if (id == "pwr_rl") {
        updateLabel(ui->RL_info_power, data);
    } else if (id == "rpm") {
        updateLabel(ui->rpm_info, data);
    }
}

void Inverters::verifyAnswers(CANmessage message)
{
    QPair<QString, QMap<QString, double>> info;
    try {
        info = this->_fcp->decodeMessage(message);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        return;
    }
    QString messageName = info.first;
    QMap<QString, double> signalValues = info.second;

    if (messageName == "ans_set") {
        try {
            updateAnswers(this->_fcp->getConfig("iib", signalValues.value("id"))->getName(), signalValues.value("id"));
        } catch (JsonException& e) {
            logger->handle_exception(e.toString());
            updateException(e);
            return;
        }
    }
}

void Inverters::handle_get(CANmessage message)
{
    auto r = this->fcp->decodeMessage(message);
    auto sigs = r.second;
    auto data = sigs.value("data");
    auto id = sigs.value("id");
    this->configs_list->setValue(id, data);
}

void Inverters::handle_set(CANmessage message)
{
    auto r = this->fcp->decodeMessage(message);
    auto sigs = r.second;
    auto data = sigs.value("data");
    auto id = sigs.value("id");
    this->configs_list->setValue(id, data);
}

void Inverters::process_CAN_message(CANmessage message)
{
    try {
        if (this->_fcp->getDevice(message.candata.dev_id)->getName() != "iib") {
            return;
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }

    if (message.candata.msg_id == this->get->getId()) {
        handle_get(message);
    } else if (message.candata.msg_id == this->set->getId()) {
        handle_set(message);
    }

    clear_inv_flag = 0;
    Inv_cleared = 0;
    updateMotorInfo(message);
    updateInverterInfo(message);
    updateInverterLimits(message);
    updateCarInfo(message);
    updateGeneralIIBinfo(message);

    verifyAnswers(message);
}

void Inverters::on_pushButton_5_clicked()
{
}

void Inverters::timer_clear_inverters()
{
    if (clear_inv_flag && !Inv_cleared) {
        clear();
        clear_inv_flag = 0;
        Inv_cleared = 1;
    } else
        clear_inv_flag = 1;
    return;
}

void Inverters::clear()
{
    ui->inverters_widget->clear();
    ui->iib_inv_1_led->turnOff();
    ui->iib_inv_2_led->turnOff();
    ui->iib_inv_3_led->turnOff();
    ui->iib_inv_4_led->turnOff();
    ui->EFA_LED->turnOff();
    ui->EFB_LED->turnOff();
    ui->IN_1_BE_1->turnOff();
    ui->IN_1_BE_2->turnOff();
    ui->IN_2_BE_1->turnOff();
    ui->IN_2_BE_2->turnOff();
    ui->IN_3_BE_1->turnOff();
    ui->IN_3_BE_2->turnOff();
    ui->IN_4_BE_1->turnOff();
    ui->IN_4_BE_2->turnOff();
    ui->iib_temp->clear();
    ui->REGEN_LED->turnOff();
    ui->SDC_1_LED->turnOff();
    ui->SDC_2_LED->turnOff();
    ui->TE_OK_LED->turnOff();
    ui->fan_speed->clear();
    ui->ARM_OK_LED->turnOff();
    ui->INV_HV_LED->turnOff();
    ui->fan_on_LED->turnOff();
    ui->BMS_voltage->clear();
    ui->CAR_RTD_LED->turnOff();
    ui->TE_PERCENTAGE->clear();
    ui->air_temp->clear();
    ui->iib_inv_rtd_led->turnOff();
}

void Inverters::on_rtd_button_clicked()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = DEVICE_ID_IIB;
    msg.candata.dlc = 8;
    msg.candata.data[0] = 2;
    msg.candata.data[1] = msg.candata.data[2] = msg.candata.data[3] = 0;
    send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    return;
}

void Inverters::on_set_mode_clicked()
{
    if (ui->modes_dropdown->currentIndex() != 0) {
        CANmessage msg;
        msg.candata.dev_id = DEVICE_ID_INTERFACE;
        msg.candata.msg_id = 27;
        msg.candata.dlc = 8;
        msg.candata.data[0] = DEVICE_ID_IIB;
        msg.candata.data[1] = 15;
        msg.candata.data[2] = uint16_t(ui->modes_dropdown->currentIndex() - 1);
        msg.candata.data[3] = 0;
        send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    }
    return;
}

void Inverters::on_specific_val_textEdited(const QString& arg1)
{
    double current_number = arg1.toDouble();
    check_spec_val(current_number);
    return;
}

void Inverters::on_parameter_currentIndexChanged(int)
{
    check_spec_val(ui->specific_val->text().toDouble());
}

void Inverters::check_spec_val(double val)
{
    double current_number = val;
    if (ui->parameter->currentText() == "POWER") {
        if (current_number > 35000) {
            ui->specific_val->setText("35000");
        }
    } else if (ui->parameter->currentText() == "TORQUE") {
        if (current_number > 21000) {
            ui->specific_val->setText("21000");
        }
    }

    return;
}

void Inverters::updateException(JsonException& e)
{
    this->ui->exceptions->clear();
    this->ui->exceptions->setText(e.toString());
    return;
}

void Inverters::on_set_new_pow_tor_clicked()
{
    QString component = "";
    QString configName = "'";
    QString parameter = "";
    uint16_t valueToSend = uint16_t(ui->specific_val->text().toInt());
    CANmessage msg;
    if (ui->parameter->currentIndex() == 0) {
        return;
    }
    try {
        if ((ui->component->currentIndex() > 0) && (ui->component->currentIndex() < 5)) {
            component = this->ui->component->currentText().toLower();
            parameter = this->ui->parameter->currentText().toLower();
            if (parameter == "power")
                parameter = "pwr";
            configName = parameter + "_" + component;

            msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);

            emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
            return;
        } else {
            if (ui->component->currentText() == "REAR") {
                if (ui->parameter->currentText() == "TORQUE") {
                    configName = "torque_rl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "torque_rr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                } else if (ui->parameter->currentText() == "PWR") {
                    configName = "pwr_rl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "pwr_rr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                } else if (ui->parameter->currentText() == "MAX_REGEN_PWR") {
                    configName = "max_regen_pwr_rl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "max_regen_pwr_rr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                } else if (ui->parameter->currentText() == "REGEN_TORQUE") {
                    configName = "max_regen_torque_rl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "max_regen_torque_rr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                }

            } else if (ui->component->currentText() == "FRONT") {
                if (ui->parameter->currentText() == "TORQUE") {
                    configName = "torque_fl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "torque_fr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                } else if (ui->parameter->currentText() == "PWR") {
                    configName = "pwr_fl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "pwr_fr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                }

                else if (ui->parameter->currentText() == "MAX_REGEN_PWR") {
                    configName = "max_regen_pwr_fl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "max_regen_pwr_fr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                } else if (ui->parameter->currentText() == "REGEN_TORQUE") {
                    configName = "max_regen_pwr_fl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "max_regen_pwr_fr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                }
            } else if (ui->component->currentText() == "ALL") {
                if (ui->parameter->currentText() == "TORQUE") {
                    configName = "torque_fl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "torque_fr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "torque_rl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "torque_rr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                } else if (ui->parameter->currentText() == "PWR") {
                    configName = "pwr_fl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "pwr_fr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "pwr_rl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "pwr_rr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                } else if (ui->parameter->currentText() == "MAX_REGEN_PWR") {
                    configName = "max_regen_pwr_fl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "max_regen_pwr_fr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "max_regen_pwr_rr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "max_regen_pwr_rl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                } else if (ui->parameter->currentText() == "REGEN_TORQUE") {
                    configName = "max_regen_torque_rl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "max_regen_torque_rr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "max_regen_torque_fl";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

                    configName = "max_regen_torque_fr";
                    msg = this->_fcp->encodeSetConfig("interface", "iib", configName, valueToSend);
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
                }
            }
        }
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
    return;
}

void Inverters::on_Set_motor_speed_clicked()
{
    CANmessage msg;
    try {
        msg = this->_fcp->encodeSetConfig("interface", "iib", "rpm", uint16_t(ui->motor_speed->text().toInt()));
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void Inverters::on_motor_speed_textChanged(const QString& arg1)
{
    int current_number = arg1.toInt();
    if (current_number > 20000) {
        ui->motor_speed->setText("20000");
    } else if (current_number < 0) {
        ui->motor_speed->setText("0");
    }
    return;
}

void Inverters::on_set_regen_clicked()
{
    CANmessage msg;
    try {
        msg = this->_fcp->encodeSetConfig("interface", "iib", "regen_on", regen_on);

    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void Inverters::on_debug_but_toggled(bool checked)
{
    CANmessage msg;
    try {
        msg = this->_fcp->encodeSetConfig("interface", "iib", "torque_fl", checked);
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    return;
}

void Inverters::on_set_all_iib_clicked()
{
    if (ui->motor_speed->text() != "") {
        on_Set_motor_speed_clicked();
    }
    if (ui->specific_val->text() != "") {
        on_set_new_pow_tor_clicked();
    }
    if (ui->modes_dropdown->currentText() != "") {
        on_set_mode_clicked();
    }
    if (ui->torque_front_input->text() != "") {
        on_set_torque_front_clicked();
    }
    if (ui->torque_rear_input->text() != "") {
        on_set_torque_rear_clicked();
    }
    return;
}

void Inverters::on_set_torque_front_clicked()
{
    CANmessage msg;
    try {
        msg = this->_fcp->encodeSetConfig("interface", "iib", "torque_fl", uint16_t(ui->torque_front_input->text().toDouble()));
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    try {
        msg = this->_fcp->encodeSetConfig("interface", "iib", "torque_fr", uint16_t(ui->torque_front_input->text().toDouble()));
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void Inverters::on_set_torque_rear_clicked()
{
    CANmessage msg;
    try {
        msg = this->_fcp->encodeSetConfig("interface", "iib", "torque_rl", uint16_t(ui->torque_rear_input->text().toDouble()));
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    try {
        msg = this->_fcp->encodeSetConfig("interface", "iib", "torque_rr", uint16_t(ui->torque_rear_input->text().toDouble()));
    } catch (JsonException& e) {
        logger->handle_exception(e.toString());
        updateException(e);
        return;
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void Inverters::on_save_button_clicked()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 2;
    msg.candata.data[0] = DEVICE_ID_IIB;
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void Inverters::save_settings()
{
    manager->add("Component", ui->component->currentIndex());
    manager->add("Modes", ui->modes_dropdown->currentIndex());
    manager->add("Parameter", ui->parameter->currentIndex());
    manager->add("Motor Speed", ui->motor_speed->text().toInt());
    manager->add("Torque Rear", ui->torque_rear_input->text().toInt());
    manager->add("Torque Front", ui->torque_front_input->text().toInt());
    manager->add("Power_Torque", ui->set_new_pow_tor->text().toInt());
    manager->save("IIB", specific);
}

void Inverters::load_settings()
{
    QSettings settings(m_sSettingsFile, QSettings::NativeFormat);
    ui->component->setCurrentIndex(settings.value("Component", "").toInt());
    ui->modes_dropdown->setCurrentIndex(settings.value("Modes", "").toInt());
    ui->parameter->setCurrentIndex(settings.value("Parameter", "").toInt());

    ui->specific_val->setText((settings.value("Power_Torque", "")).toString());
    ui->torque_front_input->setText((settings.value("Torque Front", "")).toString());
    ui->torque_rear_input->setText((settings.value("Torque Rear", "")).toString());
    ui->motor_speed->setText((settings.value("Motor Speed", "")).toString());
}

void Inverters::on_save_clicked()
{
    save_settings();
}

void Inverters::on_load_clicked()
{
    load_settings();
}

void Inverters::updateFCP(FCPCom* fcp)
{
    this->_fcp = fcp;
}

void Inverters::loadState(QMap<QString, QVariant> config)
{
    if (config.contains("load_default_ui"))
        return;

    ui->component->setCurrentIndex(config["component"].toInt());
    ui->parameter->setCurrentIndex(config["parameter"].toInt());
    ui->specific_val->setText(config["specific_val"].toString());
    ui->torque_front_input->setText(config["torque_front_input"].toString());
    ui->torque_rear_input->setText(config["torque_rear_input"].toString());
    ui->motor_speed->setText(config["motor_speed"].toString());
    ui->modes_dropdown->setCurrentIndex(config["modes_dropdown"].toInt());
}

void Inverters::on_saveState_clicked()
{
    QMap<QString, QVariant> config;

    config["component"] = ui->component->currentIndex();
    config["parameter"] = ui->parameter->currentIndex();
    config["specific_val"] = ui->specific_val->text();
    config["torque_front_input"] = ui->torque_front_input->text();
    config["torque_rear_input"] = ui->torque_rear_input->text();
    config["motor_speed"] = ui->motor_speed->text();
    config["modes_dropdown"] = ui->modes_dropdown->currentIndex();

    this->config_manager->registerConfig(this->objectName(), config);
}
