#include "arm.hpp"
#include "ui_arm.h"

#include "arms_torque_set.hpp"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/ARM_CAN.h"
#include "ui_arms_torque_set.h"
#include <QGroupBox>
#include <QProgressBar>

arm::arm(QWidget* parent, FCPCom* fcp)
    : UiWindow(parent, fcp)
    , ui(new Ui::arm)
{
    ui->setupUi(this);
    manager = new state_manager<int>();
    specific = "";

    parameters << P_ARM_OPERATIONAL_TORQUE_FRONT << P_ARM_OPERATIONAL_TORQUE_REAR << P_ARM_OPERATIONAL_REGEN_TORQUE_FRONT << P_ARM_OPERATIONAL_REGEN_TORQUE_REAR << P_ARM_OPERATIONAL_POWER
               << P_ARM_OPERATIONAL_POWER_REGEN << P_ARM_TL_GAIN << P_ARM_YRC_DELTA_GAIN << P_ARM_OPERATIONAL_MOTOR_SPEED;
    names << "Torque Front [Nmm]"
          << "Torque Rear[Nmm]"
          << "Torque Regen Front[|Nmm|]"
          << "Torque Regen Rear[|Nmm|]"
          << "Power[kW]"
          << "Power Regen[|kW|]"
          << "TL GAIN"
          << "Steering Differential Gain"
          << "Motor Speed [RPM]";
    states << P_ARM_CONTROL_STATE << P_ARM_TL_STATE << P_ARM_LAUNCH_CONTROL_STATE << P_ARM_LATERAL_CONTROL_STATE << P_ARM_POWER_LIMITER_STATE;
    state_names << "Control State(0/1)"
                << "TL STATE"
                << "LAUNCH CONTROL STATE"
                << "LATERAL CONTROL STATE"
                << "POWER LIMITER STATE";

    init_sets();
    ui->FL_neg_torque->setStyleSheet("QProgressBar::chunk {background-color: green;}");
    ui->FR_neg_torque->setStyleSheet("QProgressBar::chunk {background-color: green;}");
    ui->RL_neg_torque->setStyleSheet("QProgressBar::chunk {background-color: green;}");
    ui->RR_neg_torque->setStyleSheet("QProgressBar::chunk {background-color: green;}");

    first_time = 1;
    refresh_values();

    refresh_timer = new QTimer(this);
    connect(refresh_timer, &QTimer::timeout, this, &arm::refresh_values);
    refresh_timer->start(1000);

    clear_timer = new QTimer(this);
    torque_map[FWD] = "FWD";
    torque_map[RWD] = "RWD";
    torque_map[AWD] = "AWD";

    regen_map[NO_REGEN] = "No Regen";
    regen_map[MAX_REGEN] = "Max";
    regen_map[MAX_BRAKING] = "Max Braking";
    regen_map[BASIC_REGEN] = "Basic Regen";

    yaw_map[FEEDBACK] = "Feedback";
    yaw_map[STEERING_PROPORTIONAL] = "Steering Proportional";

    track_map[SKIDPAD] = "Skidpad";
    track_map[AUTOCROSS] = "Autocross";
    track_map[ACCELERATION] = "Acceleration";
    track_map[ENDURANCE] = "Endurance";
    connect(clear_timer, &QTimer::timeout, this, &arm::clear_values);
    clear_timer->start(3000);

    m_sSettingsFile = QDir::currentPath() + "/set/states/" + this->_name + "_" + specific.toUpper() + "_settings.ini";
}

arm::~arm()
{
    save_settings();
    delete ui;
}

void arm::process_CAN_message(CANmessage msg)
{
    CANdata message = msg.candata;
    clear_timer->start(3000);

    double val, torque_pos, torque_val, torque_neg = 0.0;
    int16_t data_2 = 0;
    switch (msg.candata.dev_id) {
    case (DEVICE_ID_ARM):
        switch (message.msg_id) {
        case MSG_ID_ARM_TORQUE:
            ARM_Torque_Setpoint set_points;

            parse_ARM_torque_setpoint_message(message.data, &set_points);

            torque_val = int16_t(set_points.torque);
            switch (set_points.motor_num) {
            case FL:
                if (torque_val > 0) {
                    ui->FL_pos_torque->update_value(torque_val);
                    ui->FL_neg_torque->update_value(0);
                } else if (torque_val < 0) {
                    ui->FL_pos_torque->update_value(0);
                    ui->FL_neg_torque->update_value(torque_val);
                } else {
                    ui->FL_pos_torque->update_value(torque_val);
                    ui->FL_neg_torque->update_value(torque_val);
                }
                break;
            case FR:
                if (torque_val > 0)
                    ui->FR_pos_torque->update_value(torque_val);
                else if (torque_val < 0)
                    ui->FR_neg_torque->update_value(torque_val);
                else {
                    ui->FR_pos_torque->update_value(torque_val);
                    ui->FR_neg_torque->update_value(torque_val);
                }
                break;
            case RL:
                if (torque_val > 0)
                    ui->RL_pos_torque->update_value(torque_val);
                else if (torque_val < 0)
                    ui->RL_neg_torque->update_value(torque_val);
                else {
                    ui->RL_pos_torque->update_value(torque_val);
                    ui->RL_neg_torque->update_value(torque_val);
                }
                break;
            case RR:
                if (torque_val > 0)
                    ui->RR_pos_torque->update_value(torque_val);
                else if (torque_val < 0)
                    ui->RR_neg_torque->update_value(torque_val);
                else {
                    ui->RR_pos_torque->update_value(torque_val);
                    ui->RR_neg_torque->update_value(torque_val);
                }
                break;
            }
            break;
        case MSG_ID_ARM_SPEED:

            ARM_Speed_Setpoint speed;
            parse_ARM_speed_setpoint_message(message.data, &speed);

            torque_pos = speed.torque_pos / 1000;
            torque_neg = int16_t(speed.torque_neg) / 1000;

            switch (speed.motor_num) {
            case FL:
                ui->FL_pos_torque->update_value(torque_pos);
                ui->FL_neg_torque->update_value(torque_neg);
                break;
            case FR:
                ui->FR_pos_torque->update_value(torque_pos);
                ui->FR_neg_torque->update_value(torque_neg);
                break;
            case RL:
                ui->RL_pos_torque->update_value(torque_pos);
                ui->RL_neg_torque->update_value(torque_neg);
                break;
            case RR:
                ui->RR_pos_torque->update_value(torque_pos);
                ui->RR_neg_torque->update_value(torque_neg);
                break;
            }
            break;
        case MSG_ID_ARM_DRS:

            ARM_DRS_Angle drs_state;
            parse_ARM_DRS_state(message.data, &drs_state);

            if (drs_state.state) {

                QPalette pal = palette();
                pal.setColor(QPalette::Window, Qt::green);
                ui->DRS_Widget->setAutoFillBackground(true);
                ui->DRS_Widget->setPalette(pal);

            } else {
                QPalette pal = palette();
                pal.setColor(QPalette::Window, Qt::red);
                ui->DRS_Widget->setAutoFillBackground(true);
                ui->DRS_Widget->setPalette(pal);
            }
            break;
        case CMD_ID_COMMON_SET:
            data_2 = int16_t(msg.candata.data[2]);
            val = data_2 / 1000.0;

            for (int i = 0; i < sets.size(); i++) {
                if (sets[i]->get_parameter() == msg.candata.data[1]) {

                    sets[i]->set_new_current_value(val * 1000);

                    if (msg.candata.data[1] == P_ARM_OPERATIONAL_TORQUE_FRONT) {
                        ui->FR_pos_torque->set_new_maximum(val);
                        ui->FL_pos_torque->set_new_maximum(val);
                    } else if (msg.candata.data[1] == P_ARM_OPERATIONAL_TORQUE_REAR) {
                        ui->RR_pos_torque->set_new_maximum(val);
                        ui->RL_pos_torque->set_new_maximum(val);
                    } else if (msg.candata.data[1] == P_ARM_OPERATIONAL_REGEN_TORQUE_REAR) {
                        ui->RR_neg_torque->set_new_maximum(val);
                        ui->RL_neg_torque->set_new_maximum(val);
                    } else if (msg.candata.data[1] == P_ARM_OPERATIONAL_REGEN_TORQUE_FRONT) {
                        ui->FL_neg_torque->set_new_maximum(val);
                        ui->FR_neg_torque->set_new_maximum(val);
                    }
                    return;
                }
            }
            break;
        case CMD_ID_COMMON_GET:
            val = int16_t(msg.candata.data[2]);

            for (int i = 0; i < sets.size(); i++) {
                if (sets[i]->get_parameter() == msg.candata.data[1]) {
                    sets[i]->set_new_current_value(val);
                    return;
                }
            }
            if (msg.candata.data[1] == P_ARM_TRACTION_MODE) {
                ui->current_torque_mode->setText(torque_map[val]);
            } else if (msg.candata.data[1] == P_ARM_REGEN_MODE) {
                ui->current_regen_mode->setText(regen_map[val]);
            } else if (msg.candata.data[1] == P_ARM_LATERAL_CONTROL_MODE) {
                ui->current_yaw->setText(yaw_map[val]);
            } else if (msg.candata.data[1] == P_ARM_DYNAMIC_EVENT) {
                ui->currentevent->setText(track_map[val]);
            }
            break;
        }
    }
}

void arm::init_sets()
{
    arms_torque_set* new_line;

    for (int i = 0; i < parameters.size(); i++) {
        new_line = new arms_torque_set(this, names[i], parameters[i]);
        ui->this_is_scroll_area->addWidget(new_line);
        connect(new_line, &arms_torque_set::send_CAN, this, &arm::retransmit_can);
        sets.append(new_line);
    }

    for (int i = 0; i < states.size(); i++) {
        new_line = new arms_torque_set(this, state_names[i], states[i]);
        ui->states->addWidget(new_line);
        connect(new_line, &arms_torque_set::send_CAN, this, &arm::retransmit_can);
        sets.append(new_line);
    }
    return;
}
void arm::retransmit_can(CANmessage msg)
{
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    return;
}

void arm::do_gets()
{
    for (int i = 0; i < sets.size(); i++) {
        sets[i]->get_values();
        get_traction_mode();
        get_regen_mode();
        if (sets[i]->get_parameter() == P_ARM_OPERATIONAL_TORQUE_FRONT) {
            if (sets[i]->get_current_value() != -1) {
                ui->FL_pos_torque->set_new_maximum(sets[i]->get_current_value());
                ui->FR_pos_torque->set_new_maximum(sets[i]->get_current_value());
            }
        } else if (sets[i]->get_parameter() == P_ARM_OPERATIONAL_TORQUE_REAR) {
            if (sets[i]->get_current_value() != -1) {
                ui->RL_pos_torque->set_new_maximum(sets[i]->get_current_value());
                ui->RR_pos_torque->set_new_maximum(sets[i]->get_current_value());
            }
        } else if (sets[i]->get_parameter() == P_ARM_OPERATIONAL_REGEN_TORQUE_FRONT) {
            if (sets[i]->get_current_value() != -1) {
                ui->FL_neg_torque->set_new_maximum(sets[i]->get_current_value());
                ui->FR_neg_torque->set_new_maximum(sets[i]->get_current_value());
            }
        } else if (sets[i]->get_parameter() == P_ARM_OPERATIONAL_REGEN_TORQUE_REAR) {
            if (sets[i]->get_current_value() != -1) {
                ui->RL_neg_torque->set_new_maximum(sets[i]->get_current_value());
                ui->RR_neg_torque->set_new_maximum(sets[i]->get_current_value());
            }
        }
    }
    return;
}

void arm::on_send_all_clicked()
{
    for (int i = 0; i < sets.size(); i++) {
        sets[i]->send_set_message();
    }
}

void arm::refresh_values()
{
    do_gets();
    return;
}

void arm::clear_values()
{
    for (int i = 0; i < sets.size(); i++) {
        sets[i]->clear_value();
    }
    return;
}

void arm::on_set_torque_mode_clicked()
{
    if (!ui->torque_mode_dropdown->currentIndex()) {
        CANmessage msg;
        msg.candata.dev_id = DEVICE_ID_INTERFACE;
        msg.candata.msg_id = CMD_ID_COMMON_SET;
        msg.candata.dlc = 6;
        msg.candata.data[0] = DEVICE_ID_ARM;
        msg.candata.data[1] = P_ARM_TRACTION_MODE;
        msg.candata.data[2] = uint16_t(torque_map.key(ui->torque_mode_dropdown->currentText()));
        send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    }
    return;
}

void arm::get_traction_mode()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_GET;
    msg.candata.dlc = 4;
    msg.candata.data[0] = DEVICE_ID_ARM;
    msg.candata.data[1] = P_ARM_TRACTION_MODE;
    send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    return;
}

void arm::get_regen_mode()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_GET;
    msg.candata.dlc = 4;
    msg.candata.data[0] = DEVICE_ID_ARM;
    msg.candata.data[1] = P_ARM_REGEN_MODE;
    send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    return;
}
void arm::on_set_regen_clicked()
{
    if (ui->regen_mode_drop->currentIndex()) {
        CANmessage msg;
        msg.candata.dev_id = DEVICE_ID_INTERFACE;
        msg.candata.msg_id = CMD_ID_COMMON_SET;
        msg.candata.dlc = 6;
        msg.candata.data[0] = DEVICE_ID_ARM;
        msg.candata.data[1] = P_ARM_REGEN_MODE;
        msg.candata.data[2] = uint16_t(regen_map.key(ui->regen_mode_drop->currentText()));
        send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    }
    return;
}

void arm::on_set_lateral_clicked()
{
    if (ui->lateral_drop->currentIndex()) {
        CANmessage msg;
        msg.candata.dev_id = DEVICE_ID_INTERFACE;
        msg.candata.msg_id = CMD_ID_COMMON_SET;
        msg.candata.dlc = 6;
        msg.candata.data[0] = DEVICE_ID_ARM;
        msg.candata.data[1] = P_ARM_LATERAL_CONTROL_MODE;
        msg.candata.data[2] = uint16_t(yaw_map.key(ui->lateral_drop->currentText()));
        send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    }
    return;
}

void arm::on_set_event_clicked()
{
    if (ui->event_drop->currentIndex()) {
        CANmessage msg;
        msg.candata.dev_id = DEVICE_ID_INTERFACE;
        msg.candata.msg_id = CMD_ID_COMMON_SET;
        msg.candata.dlc = 6;
        msg.candata.data[0] = DEVICE_ID_ARM;
        msg.candata.data[1] = P_ARM_DYNAMIC_EVENT;
        msg.candata.data[2] = uint16_t(track_map.key(ui->event_drop->currentText()));
        send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    }
    return;
}

void arm::save_settings()
{
    manager->add(ui->yaw->text(), ui->lateral_drop->currentIndex());
    manager->add(ui->event->text(), ui->event_drop->currentIndex());
    manager->add(ui->regen->text(), ui->regen_mode_drop->currentIndex());
    manager->add(ui->traction->text(), ui->torque_mode_dropdown->currentIndex());
    for (int i = 0; i < sets.size(); i++) {
        manager->add(sets[i]->get_name(), sets[i]->return_current_text().toInt());
    }
    manager->save(this->_name, specific);
}

void arm::load_settings()
{
    QSettings settings(m_sSettingsFile, QSettings::NativeFormat);
    ui->lateral_drop->setCurrentIndex(settings.value(ui->yaw->text(), "").toInt());
    ui->event_drop->setCurrentIndex(settings.value(ui->event->text(), "").toInt());
    ui->regen_mode_drop->setCurrentIndex(settings.value(ui->regen->text(), "").toInt());
    ui->torque_mode_dropdown->setCurrentIndex(settings.value(ui->traction->text(), "").toInt());
    for (int i = 0; i < sets.size(); i++) {
        QString sText = settings.value(sets[i]->get_name(), "").toString();
        sets[i]->write_label(sText);
    }
}

void arm::on_save_clicked()
{
    save_settings();
}

void arm::on_load_clicked()
{
    load_settings();
}

void arm::keyPressEvent(QKeyEvent* event)
{
    switch (event->key()) {
    case Qt::Key_L: {
        load_settings();
        break;
    }
    case Qt::Key_S: {
        save_settings();
        break;
    }
    }
}

void arm::on_comboBox_currentTextChanged(const QString& arg1)
{
    specific = arg1;
    m_sSettingsFile = QDir::currentPath() + "/set/states/" + this->_name + "_" + specific.toUpper() + "_settings.ini";
    return;
}

void arm::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}
