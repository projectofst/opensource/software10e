#include "COM_Interface.hpp"

Interface::Interface()
{
    StatusInt = 0;

    addOption("(no options)");
    protocol = "Interface";
}

bool Interface::open(QString)
{
    StatusInt = 1;
    return true;
}

void Interface::read(void)
{
    qDebug() << "READ INTERFACE";
}

int Interface::close(void)
{
    return 0;
}

int Interface::send(QByteArray& array)
{
    // if (StatusInt == 1)
    emit broadcast_in_message((COM::decodeFromByteArray<CANmessage>(array)));

    return 1;
}

void Interface::setId(QString address)
{
    this->COMId = this->protocol + "@" + address;
}
