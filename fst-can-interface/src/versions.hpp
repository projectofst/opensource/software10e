#ifndef VERSIONS_HPP
#define VERSIONS_HPP

#include "COM.hpp"
#include "Exceptions/helper.hpp"
#include "UiWindow.hpp"
#include "pcb_version.hpp"
#include <QWidget>
namespace Ui {
class versions;
}

class versions : public UiWindow {
    Q_OBJECT

public:
    explicit versions(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    void updateFCP(FCPCom* fcp) override;
    QString getTitle() override { return "Versions"; };
    ~versions();

private slots:
    void process_CAN_message(CANmessage message) override;
    void request_version();
    void init_devices();
    void on_refresh_button_clicked();
    void process_new_version(QString pcb_name, QString version);

private:
    Ui::versions* ui;
    QVector<pcb_version*> all_pcbs;
    QTimer* timer1;
    QList<Device*> devices;
    int timer_status;
    FCPCom* fcp;
};

#endif // VERSIONS_HPP
