#include "iib_motor_control.hpp"
#include "ui_iib_motor_control.h"

iib_motor_control::iib_motor_control(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::iib_motor_control)
{
    ui->setupUi(this);

    this->ui->FrontLeft->setName("FL");
    this->_singleInverters.insert(FRONTLEFT, this->ui->FrontLeft);

    this->ui->FrontRight->setName("FR");
    this->_singleInverters.insert(FRONTRIGHT, this->ui->FrontRight);

    this->ui->RearLeft->setName("RL");
    this->_singleInverters.insert(REARLEFT, this->ui->RearLeft);

    this->ui->RearRight->setName("RR");
    this->_singleInverters.insert(REARRIGHT, this->ui->RearRight);
}

void iib_motor_control::updateInformation(QPair<QString, QMap<QString, double>> information)
{
    QString messageName = information.first;
    QMap<QString, double> signalValues = information.second;

    if (messageName == "iib_motor") {
        uint motorNumber = signalValues.value("n_motor_info", 4);
        if (motorNumber < 4) {
            SingleInverter* inverter = this->_singleInverters.value(motorNumber);
            inverter->updateMotorInformation(signalValues);
        }
    } else if (messageName == "iib_inv") {
        uint inverterNumber = signalValues.value("n_inv_info", 4);
        if (inverterNumber < 4) {
            SingleInverter* inverter = this->_singleInverters.value(inverterNumber);
            inverter->updateInverterInformation(signalValues);
        }
    } else if (messageName == "iib_limits") {
        uint inverterNumber = signalValues.value("n_inv_info", 4);
        if (inverterNumber < 4) {
            SingleInverter* inverter = this->_singleInverters.value(inverterNumber);
            inverter->updateInverterLimits(signalValues);
        }
    }
}

void iib_motor_control::clear()
{
    for (auto it = this->_singleInverters.begin(); it != this->_singleInverters.end(); it++) {
        it.value()->clear();
    }
    return;
}

iib_motor_control::~iib_motor_control()
{
    qDeleteAll(this->_singleInverters);
    delete ui;
}
