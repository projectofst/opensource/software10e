#ifndef ADCMANAGERWIDGET_H
#define ADCMANAGERWIDGET_H

#include "Resources/adcwidget.hpp"
#include <QUdpSocket>
#include <QWidget>

namespace Ui {
class ADCManagerWidget;
}

class ADCManagerWidget : public QWidget {
    Q_OBJECT

public:
    explicit ADCManagerWidget(QWidget* parent = nullptr);
    ~ADCManagerWidget();

private slots:
    void on_addADC_clicked();
    void removeADCWidget(ADCWidget* const widget);

    void on_reconnect_clicked();
    void sendADC(const std::string str);

private:
    Ui::ADCManagerWidget* ui;
    QList<ADCWidget*>* _adcWidgets;
    QUdpSocket* _socket;
};

#endif // ADCMANAGERWIDGET_H
