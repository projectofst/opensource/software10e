#include "SQL_CANID.hpp"

SQL_CANID::SQL_CANID()
{
    CANId = QSqlDatabase::addDatabase("QSQLITE");
    CANId.setDatabaseName("/Users/diogopereira/Documents/fst-can-interface/sql/MainCAN.db");
    CANId.open();
}

int SQL_CANID::giveId(QString Device)
{
    QSqlQuery test;
    test.exec(QString("SELECT Id FROM Devices WHERE Designation = '").append(Device.append("'")));
    test.first();
    return test.value(0).toInt();
}

QMap<QString, QVector<QVariant>> SQL_CANID::giveView(QString)
{

    QMap<QString, QVector<QVariant>> View;

    return View;
}

bool SQL_CANID::translateMsg(CANdata Input, QString Field, int*)
{
    QSqlQuery TranslateQuery;

    QString Pattern = QString::number((Input.msg_id << 5) + Input.dev_id);
    TranslateQuery.exec(QString("SELECT FIELDNAME, STARTbit, ENDbit FROM Struct WHERE CANID = %d AND FIELDNAME = '%s'").arg((Input.msg_id << 5) + Input.dev_id).arg(Field));
    TranslateQuery.first();

    if (TranslateQuery.isValid() && TranslateQuery.value(0).toString() == Field) {
        uint64_t Data = (uint64_t(Input.data[0]) << 48) + (uint64_t(Input.data[1]) << 32) + (Input.data[2] << 16) + Input.data[3];
        bitwiseThis(Data, TranslateQuery.value(1).toInt(), TranslateQuery.value(2).toInt());
        return true;
    } else {
        return false;
    }
}

int SQL_CANID::bitwiseThis(int64_t Input, int Start, int End)
{
    int Output;

    Output = Input << Start & End;

    return Output;
}

void SQL_CANID::setDatabase(QString AbsPath)
{
    CANId.close();
    CANId = QSqlDatabase::addDatabase("QSQLITE");
    CANId.setDatabaseName(AbsPath);
    CANId.open();
}
