#ifndef CCU_HPP
#define CCU_HPP

#include "CAN_IDs.h"
#include "CCU/individualccu.hpp"
#include "CCU_CAN.h"
#include "COM.hpp"
#include "Exceptions/helper.hpp"
#include "LEDwidget.hpp"
#include "UiWindow.hpp"
#include "ccu_indiv.hpp"
#include <QTimer>
#include <QWidget>

/*Not proud of this*/
#define LEFT 0
#define RIGHT 1

namespace Ui {
class CCU;
}

class CCU : public UiWindow {
    Q_OBJECT

public:
    explicit CCU(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    void retransmit_can(CANmessage msg);
    void process_CAN_message(CANmessage msg) override;
    void updateFCP(FCPCom* fcp) override;
    QString getTitle() override { return "CCU"; };
    ~CCU();

private:
    Ui::CCU* ui;
    IndividualCCU* _rightCCU;
    IndividualCCU* _leftCCU;

    void updateCCUTemperatures(CANmessage message, IndividualCCU* ccu, int start);
    void updateCCUFlowValues(CANmessage message, IndividualCCU* ccu);

    void updateTemperatures(CANmessage message);
    void updateFlowValues(CANmessage message);
    void updateCCUStatus(CANmessage msg);

    IndividualCCU* getCCU(CANmessage msg);
    Helper* logger;
};

#endif // CCU_HPP
