/**********************************************************************
 *	 FST CAN tools --- interface
 *
 *	 Console widget
 *	 _______________________________________________________________
 *
 *	 Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *	 This program is free software; you can redistribute it and/or
 *	 modify it under the terms of the GNU General Public License
 *	 as published by the Free Software Foundation; version 2 of the
 *	 License only.
 *
 *	 This program is distributed in the hope that it will be useful,
 *	 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	 GNU General Public License for more details.
 *
 *	 You should have received a copy of the GNU General Public License
 *	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/
#include <qmessagebox.h>

#include "Console.hpp"
#include "ui_Console.h"
#include <QCheckBox>
#include <QComboBox>
#include <QDateTime>
#include <QFile>
#include <QLineEdit>
#include <QMutex>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QThreadPool>
#include <QVector>
#include <iostream>

#include "COM_SerialPort.hpp"
#include "Resources/filterwidget.hpp"
#include "can-ids-spec/skel/signal_parser.h"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/INTERFACE_CAN.h"
#include "fcp-widgets/fcpdocs.h"
#include "ui_filterwidget.h"

/**********************************************************************
 * Name:	Console
 * Args:	-
 * Return:	-
 * Desc:	Console class constructor.
 **********************************************************************/
Console::Console(QWidget* parent, FCPCom* fcp, Helper*, ConfigManager* config_manager)
    : UiWindow(parent, fcp)
    , ui(new Ui::Console)
{
    ui->setupUi(this);
    ui->Display->setMaximumBlockCount(MAX_LOG_LINES_CONSOLE);

    ui->var_type->addItem("Signed");
    ui->var_type->addItem("Unsigned");
    ui->var_type->addItem("Hex");

    connect(&make_id, &MakeID::id_value, this, &Console::set_id_value);

    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Console::update);
    timer->start(50);

    LED_timer = new QTimer(this);
    connect(LED_timer, &QTimer::timeout, this, &Console::update_LED);
    LED_timer->start(3000);

    telemetry_timer = new QTimer(this);
    connect(telemetry_timer, &QTimer::timeout, this, &Console::updateTelemetryLed);
    telemetry_timer->start(1000);

    this->_fcpInputList = new QList<nameValueInputTemplate*>();
    connect(this->ui->logReplayWidget, &LogReplay::newMessage, this, &Console::processLogMessage);
    connect(this->ui->logReplayWidget, &LogReplay::timeChanged, this, &UiWindow::changeLogTime);
    ui->warningLabel->setStyleSheet("QLabel {color: red;}");

    try {
        addDevices();
    } catch (JsonException& e) {
        qDebug() << e.toString();

        ui->DeviceBox->clear();
        ui->DeviceChoice->clear();
        ui->DeviceBox->addItem(e.toString());
        ui->DeviceBox->addItem(e.toString());
        ui->MessageBox->addItem(e.toString());
        ui->JsonComponentNameBox->addItem(e.toString());
        ui->MessageBox->setEditable(false);
        ui->DeviceBox->setEditable(false);
    }

    ui->TabWidget->setCurrentIndex(0);

    this->ui->ADCWidget->setVisible(false);
    // connect(this->ui->CarConfigWidget, &ConfigurationWidget::newMessage, this, &Console::redirectCanMessage);
    // this->ui->CarConfigWidget->setFCP(this->_fcp);
    this->_canSigGen = new CanSignalGenerator(this, "x", 0, 0, 1.0, 0, this->_fcp, "", "", "");
    connect(this->_canSigGen, &CanSignalGenerator::newSample, this, &Console::handleNewCanSigGeneratorSample);

    this->config_manager = config_manager;

    config_dialog = new ConfigManagerDialog(nullptr, this, config_manager);
    // config_dialog->show();

    selected_tab = 0;
    // loadState(this->config_manager->readConfig(this->objectName()));

    fcpdisplay_widget = FcpDisplay::Builder(parent, fcp).Build();
    ui->fcpdisplay_layout->addWidget(fcpdisplay_widget);

    // Inicialize button value
    this->no_output = true;
    ui->pushButton_5->setText("Continue");
}

/**********************************************************************
 * Name:	~Console
 * Args:	-
 * Return:	-
 * Desc:	Console class destructor.
 **********************************************************************/
Console::~Console(void)
{
    this->_fcpDisplayMap.clear();
    qDeleteAll(*(this->_fcpInputList));
    delete this->_fcpInputList;

    for (int i = 0; i < filter_vector.size(); i++) {
        this->removeFilter(filter_vector[i]);
    }

    delete this->ui->ADCWidget;
    delete this->_canSigGen;
    delete config_dialog;
    delete ui;
}

void Console::addDevices()
{

    QList<Device*> devices = _fcp->getDevices();
    devices.append(_fcp->getCommon());

    ui->DeviceBox->clear();
    ui->DeviceChoice->clear();
    ui->SigGeneratorDeviceBox->clear();

    foreach (Device* currentDevice, devices) {
        QString currentName = currentDevice->getName();

        this->_deviceMap.insert(currentName, currentDevice);
        ui->DeviceBox->addItem(currentName + " (" + QString::number(currentDevice->getId()) + ")");
        ui->DeviceChoice->addItem(currentName + " (" + QString::number(currentDevice->getId()) + ")");
        ui->SigGeneratorDeviceBox->addItem(currentName + " (" + QString::number(currentDevice->getId()) + ")");
    }
}

void Console::update()
{
    if (messages == "\n" || messages == "") {

        return;
    }
    ui->Display->insertPlainText(messages);
    ui->Display->moveCursor(QTextCursor::End);
    messages.clear();
}

/**********************************************************************
 * Name:	process_print_messages
 * Args:	-
 * Return:	-
 * Desc:	Prints all the messages in the console screen from Comsguy
 *          signal.
 **********************************************************************/
void Console::process_CAN_message(CANmessage msg)
{
    ui->CAN_LED->turnGreen();
    LED_timer->start(3000);
    fcpdisplay_widget->updateFCPDisplay(msg);
    parseTelemetry(msg);
    write_to_console(msg);
    log(msg);
}

void Console::send_start_log()
{
    int year = QDate::currentDate().year();
    int month = QDate::currentDate().month();
    int day = QDate::currentDate().day();

    int hour = QTime::currentTime().hour();
    int minute = QTime::currentTime().minute();
    int second = QTime::currentTime().second();

    CANmessage msg;

    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_INTERFACE_START_LOG;

    msg.candata.dlc = 6;

    msg.candata.data[0] = static_cast<uint16_t>(year | day << 11);
    msg.candata.data[1] = static_cast<uint16_t>(month | hour << 4);
    msg.candata.data[2] = static_cast<uint16_t>(minute | second << 6);

    INTERFACE_CAN_Data interface_data;

    parse_can_interface(msg.candata, &interface_data);

    year = interface_data.start_log.year;
    month = interface_data.start_log.month;
    day = interface_data.start_log.day;
    hour = interface_data.start_log.hour;
    minute = interface_data.start_log.minute;
    second = interface_data.start_log.second;

    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

bool Console::filter_message(CANmessage msg)
{
    FilterWidget* filter_aux;
    bool device_match = false, message_match = false;

    if (this->filter_vector.size() == 0) {
        device_match = true;
        message_match = true;
    }

    for (int i = 0; i < this->filter_vector.size(); i++) {
        filter_aux = this->filter_vector.at(i);

        if (filter_aux->isActive() && (filter_aux->getDevId() == -1 || msg.candata.dev_id == filter_aux->getDevId())) {
            device_match = true;
            ui->warningLabel->setText("");
        }

        if (filter_aux->isActive() && (filter_aux->getMessageId() == -1 || msg.candata.msg_id == filter_aux->getMessageId())) {
            message_match = true;
        }
    }

    return device_match && message_match;
}

void Console::write_to_console(CANmessage msg)
{
    if (!filter_message(msg) || no_output) {
        return;
    }
    char string[512];

    unsigned dev_id = msg.candata.sid & 0x1F;
    unsigned msg_id = (msg.candata.sid >> 5) & 0x3F;
    if (var_type == "Unsigned") {
        sprintf(string, "> DEV_ID: %6d\tMSG_ID: %6d\tDLC: %d\tData: %05d  %05d  %05d  %05d\tTIME: %lu\n",
            dev_id, msg_id, msg.candata.dlc, msg.candata.data[0],
            msg.candata.data[1], msg.candata.data[2], msg.candata.data[3], (uint64_t)msg.timestamp);
    } else if (var_type == "Signed") {
        sprintf(string, "> DEV_ID: %6d\tMSG_ID: %6d\tDLC: %d\tData: %06d  %06d  %06d  %06d\tTIME: %ld\n",
            dev_id, msg_id, msg.candata.dlc, (int16_t)msg.candata.data[0],
            (int16_t)msg.candata.data[1], (int16_t)msg.candata.data[2], (int16_t)msg.candata.data[3], (int64_t)msg.timestamp);
    } else {
        sprintf(string, "> DEV_ID: %6d\tMSG_ID: %6d\tDLC: %d\tData: %04x  %04x  %04x  %04x\tTIME: %ld\n",
            dev_id, msg_id, msg.candata.dlc, msg.candata.data[0],
            msg.candata.data[1], msg.candata.data[2], msg.candata.data[3], msg.timestamp);
    }
    messages.append(string);
}

void Console::log(CANmessage msg)
{
    if (!logging) {
        return;
    }

    QDateTime now = QDateTime::currentDateTime();
    qDebug() << start.msecsTo(now);

    unsigned data[8] = { 0 };
    for (int i = 0; i < 8; i++) {
        unsigned x = msg.candata.data[i / 2];
        data[i] = (x >> (8 * (i % 2))) & 0xFF;
    }

    *log_out << QString("%1,1,%2,2,%3,%4,%5,%6,%7,%8,%9,%10,%11,%12\n").arg(QString("%1").arg(start.msecsTo(now) / 1000.0), QString::number(msg.candata.dev_id + msg.candata.msg_id * 32), QString::number(msg.candata.dlc), QString::number(data[0]), QString::number(data[1]), QString::number(data[2]), QString::number(data[3]), QString::number(data[4]), QString::number(data[5]), QString::number(data[6]), QString::number(data[7]), QString::number(log_count));
    log_out->flush();

    log_count++;
}

/**********************************************************************
 * Name:	on_consoleclear_clicked
 * Args:	-
 * Return:	-
 * Desc:	Clear the console output.
 **********************************************************************/
void Console::on_consoleclear_clicked(void)
{
    ui->Display->clear();
}

/**********************************************************************
 * Name:	on_sendbutton_clicked
 * Args:	-
 * Return:	-
 * Desc:	Sends message to CAN.
 **********************************************************************/
void Console::on_sendbutton_clicked(void)
{
    CANmessage msg;

    msg.candata.sid = static_cast<uint16_t>(ui->ID->value());
    msg.candata.dlc = static_cast<uint16_t>(ui->DLC->value());

    msg.candata.data[0] = static_cast<uint16_t>(ui->word1_2->value());
    msg.candata.data[1] = static_cast<uint16_t>(ui->word2_2->value());
    msg.candata.data[2] = static_cast<uint16_t>(ui->word3_2->value());
    msg.candata.data[3] = static_cast<uint16_t>(ui->word4_2->value());

    qint64 log_diff_ms = time.msecsTo(time.currentDateTime());
    msg.timestamp = static_cast<uint32_t>(log_diff_ms);

    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));

    return;
}

void Console::on_pushButton_5_clicked()
{
    this->no_output = !this->no_output;
    if (this->no_output) {
        ui->pushButton_5->setText("Continue");
    } else {
        ui->pushButton_5->setText("Stop");
    }
}

void Console::on_show_ids_clicked()
{
    qDebug() << "hello";

    FcpDocs* docs = new FcpDocs();
    docs->show();
    docs->activateWindow();
    docs->raise();
    docs->update();

    qDebug() << "hmm";
}

void Console::set_id_value(int id)
{
    ui->ID->setValue(id);
}

void Console::addFilter(FilterWidget* filter)
{
    ui->filterList_4->addWidget(filter);
    filter_vector.append(filter);
    connect(filter, &FilterWidget::destroyed, [this, filter]() { this->removeFilter(filter); });
}

void Console::on_AddMessageFilterButton_clicked()
{
    FilterWidget* filter = new FilterWidget();
    addFilter(filter);
}
void Console::on_make_id_clicked()
{
    make_id.show();
    make_id.activateWindow();
    make_id.raise();
}

void Console::on_var_type_currentIndexChanged(const QString& arg1)
{
    var_type = arg1;
}

void Console::update_LED()
{
    ui->CAN_LED->turnRed();
    return;
}

void Console::updateTelemetryLed()
{
    ui->Telemetry_Widget->turnRed();
    return;
}

void Console::parseTelemetry(CANmessage message)
{
    CanSignal* telemetryStatus;
    try {
        telemetryStatus = this->_fcp->getSignal(message.candata.dev_id,
            message.candata.msg_id,
            "presence");
    } catch (JsonException& e) {
        return;
        qDebug() << e.toString();
    }

    int statusValue = fcp_decode_signal_int64_t(message.candata, (fcp_signal_t) { .start = (uint16_t)telemetryStatus->getStart(), .length = (uint16_t)telemetryStatus->getLength(), .scale = telemetryStatus->getScale(), .offset = telemetryStatus->getOffset(), .type = telemetryStatus->getType(), .endianess = telemetryStatus->getByteOrder() });

    if (statusValue < 0) {
        ui->Telemetry_Widget->turnYellow();
        telemetry_timer->start(TELEMETRY_TIMOUT);
    } else if (statusValue == 1) {
        ui->Telemetry_Widget->turnGreen();
        telemetry_timer->start(TELEMETRY_TIMOUT);
    } else {
        ui->Telemetry_Widget->turnRed();
        telemetry_timer->start(TELEMETRY_TIMOUT);
    }
}

void Console::on_DeviceBox_currentTextChanged(const QString& arg1)
{
    updateMessageBox(arg1, this->ui->MessageBox, "Message");

    return;
}

void Console::on_addFcpButton_clicked()
{
    try {
        int dev_id = 0;
        int msg_id = 0;
        bool actv = true;
        auto dev_id_string = ui->DeviceBox->currentText();
        auto msg_id_string = ui->MessageBox->currentText();

        Device* dev = nullptr;
        if (dev_id_string.contains("(")) {
            dev = this->_fcp->getDevice(dev_id_string.split(" ")[0]);
            dev_id = dev->getId();
        } else if (dev_id_string == "") {
            dev_id = -1;
        } else {
            dev_id = dev_id_string.toUInt();
        }

        if (msg_id_string.contains("(")) {
            Message* msg = this->_fcp->getMessage(dev, msg_id_string.split(" ")[0]);
            msg_id = msg->getId();
        } else if (msg_id_string == "Any" || msg_id_string == "") {
            msg_id = -1;
        } else {
            msg_id = msg_id_string.toUInt();
        }

        FilterWidget* newFilter = new FilterWidget(dev_id, msg_id, actv);
        addFilter(newFilter);

    } catch (JsonException& e) {
        qDebug() << e.toString();
    }
}

void Console::updateFCP(FCPCom* fcp)
{
    if (fcp == NULL) {
        return;
    }
    this->_fcp = fcp;
    addDevices();
    fcpdisplay_widget->updateFCP(fcp);
    return;
}

void Console::removeFilter(FilterWidget* filter)
{
    filter_vector.removeAll(filter);
    return;
}

void Console::updateMessageBox(QString arg1, QComboBox* box, QString type)
{
    box->clear();
    if (arg1 == "") {
        return;
    }
    try {
        Device* currentDevice = this->_deviceMap.value(clearComboBoxInput(arg1));
        if (currentDevice == NULL) {
            return;
        }

        /*dirty and there is a better way to do this, by using the superclass JsonComponent, but i have no time*/
        if (type == "Message") {
            QList<Message*> messages = this->_fcp->getMessages(currentDevice->getId());

            try {
                messages.append(this->_fcp->getCommon()->getMessages());
            } catch (...) {
                qDebug() << "Failed to load common messages";
            }

            box->addItem("Any");

            foreach (Message* currentMessage, messages) {
                box->addItem(currentMessage->getName() + " (" + QString::number(currentMessage->getId()) + ")");
            }

        } else if (type == "Get" || type == "Set") {
            QList<Config*> configs = this->_fcp->getDevice(currentDevice->getId())->getConfigs();
            foreach (Config* config, configs) {
                box->addItem(config->getName() + " (" + QString::number(config->getId()) + ")");
            }
        } else if (type == "Command") {
            QList<Command*> commands = this->_fcp->getDevice(currentDevice->getId())->getCommands();
            foreach (Command* command, commands) {
                box->addItem(command->getName() + " (" + QString::number(command->getId()) + ")");
            }
        }
    } catch (JsonException& e) {
        qDebug() << e.toString();
        return;
    }
}

void Console::on_DeviceChoice_currentTextChanged(const QString& arg1)
{
    updateMessageBox(arg1, this->ui->JsonComponentNameBox, this->ui->MessageTypeBox->currentText());
    updateFCPInfo(arg1, this->ui->JsonComponentNameBox->currentText(), this->ui->MessageTypeBox->currentText());
    return;
}

void Console::updateFCPInfo(QString deviceName, QString componentName, QString type)
{
    clearFcpInfo();
    if (deviceName == "" || componentName == "" || type == "") {
        return;
    }
    QList<JsonComponent*>* componentList = nullptr;
    try {
        if (type == "Command") {
            componentList = this->_fcp->getComponentsByType("common", "send_cmd", "Message");
        } else if (type == "Get") {
            componentList = this->_fcp->getComponentsByType("common", "req_get", "Message");
        } else if (type == "Set") {
            componentList = this->_fcp->getComponentsByType("common", "req_set", "Message");
        } else {
            componentList = this->_fcp->getComponentsByType(clearComboBoxInput(deviceName), clearComboBoxInput(componentName), type);
        }
    } catch (JsonException& e) {
        qDebug() << e.toString();
        return;
    }

    fcp_input_boxes.clear();

    foreach (JsonComponent* component, *componentList) {
        nameValueInputTemplate* newFcpInfo = new nameValueInputTemplate(this, component->getName());

        this->_fcpInputList->append(newFcpInfo);
        this->ui->FCPScroll->addWidget(newFcpInfo);

        fcp_input_boxes.append(newFcpInfo);
    }

    if (type == "Command" || type == "Set" || type == "Get") {
        foreach (nameValueInputTemplate* input, *(this->_fcpInputList)) {
            try {
                Device* destDevice = this->_fcp->getDevice(clearComboBoxInput(deviceName));
                JsonComponent* component = nullptr;

                if (type == "Command")
                    component = this->_fcp->getCommand(clearComboBoxInput(deviceName), clearComboBoxInput(componentName));
                else if (type == "Set" || type == "Get") {
                    component = this->_fcp->getConfig(clearComboBoxInput(deviceName), clearComboBoxInput(componentName));
                }

                if (input->getName() == "dst") {
                    input->setValue(destDevice->getId());
                } else if (input->getName() == "id") {
                    input->setValue(component->getId());
                }
            } catch (JsonException& e) {
                qDebug() << e.toString();
            }
        }
    }
}

void Console::on_JsonComponentNameBox_currentTextChanged(const QString& arg1)
{
    updateFCPInfo(this->ui->DeviceChoice->currentText(), arg1, this->ui->MessageTypeBox->currentText());
}

void Console::on_MessageTypeBox_currentTextChanged()
{
    on_DeviceChoice_currentTextChanged(this->ui->DeviceChoice->currentText());
}

void Console::clearFcpInfo()
{
    foreach (QWidget* widget, *(_fcpInputList)) {
        this->ui->FCPScroll->removeWidget(widget);
        delete widget;
    }
    this->_fcpInputList->clear();
}

QString Console::clearComboBoxInput(QString toClean)
{
    /*toClean is like: <String> <(Id)>*/
    QList<QString> stringList = toClean.split(" ");
    return stringList[0];
}

void Console::on_sendFCPMessage_clicked()
{
    this->ui->exceptions->clear();
    QString messageType = this->ui->MessageTypeBox->currentText();
    CANmessage msg;
    try {
        if (messageType == "Command") {
            msg = this->_fcp->encodeSendCommand(clearComboBoxInput(ui->DeviceChoice->currentText()),
                this->_fcp->getDevice(findFCPInfo("dst"))->getName(),
                clearComboBoxInput(ui->JsonComponentNameBox->currentText()),
                findFCPInfo("arg1"),
                findFCPInfo("arg2"),
                findFCPInfo("arg3"));

        } else if (messageType == "Get") {
            msg = this->_fcp->encodeGetConfig(clearComboBoxInput(ui->DeviceChoice->currentText()),
                this->_fcp->getDevice(findFCPInfo("dst"))->getName(),
                clearComboBoxInput(ui->JsonComponentNameBox->currentText()));
        } else if (messageType == "Set") {
            msg = this->_fcp->encodeSetConfig(clearComboBoxInput(ui->DeviceChoice->currentText()),
                this->_fcp->getDevice(findFCPInfo("dst"))->getName(),
                clearComboBoxInput(ui->JsonComponentNameBox->currentText()),
                findFCPInfo("data"));
        } else {
            QMap<QString, double> encodingValues;
            foreach (nameValueInputTemplate* temp, *(this->_fcpInputList)) {
                encodingValues.insert(temp->getName(), (double)temp->getValue());
            }

            msg = this->_fcp->encodeMessage(clearComboBoxInput(ui->DeviceChoice->currentText()),
                clearComboBoxInput(ui->JsonComponentNameBox->currentText()),
                encodingValues);
        }
        if (this->ui->InterfaceCheckBox->isChecked()) {
            msg.candata.dev_id = this->_fcp->getDevice("interface")->getId();
        }
    } catch (JsonException& e) {
        qDebug() << e.toString();
        this->ui->exceptions->setText(e.toString());
        return;
    }
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

uint64_t Console::findFCPInfo(QString info)
{
    foreach (nameValueInputTemplate* temp, *(this->_fcpInputList)) {
        if (temp->getName() == info) {
            return temp->getValue();
        }
    }
    return 0;
}

void Console::on_adcToggleButton_toggled(bool checked)
{
    this->ui->ADCWidget->setVisible(checked);
}

void Console::processLogMessage(float, CANmessage msg)
{
    emit newLogMessage(msg);
    send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void Console::redirectCanMessage(CANmessage msg)
{
    send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void Console::on_sigGeneratorStartInput_editingFinished()
{
    this->_canSigGen->setStart(this->ui->sigGeneratorStartInput->text().toDouble());
}

void Console::on_signalGeneratorEndInput_editingFinished()
{
    this->_canSigGen->setEnd(this->ui->signalGeneratorEndInput->text().toDouble());
}

void Console::on_signalGeneratorStepInput_editingFinished()
{
    this->_canSigGen->setStep(this->ui->signalGeneratorStepInput->text().toDouble());
}

void Console::on_signalGeneratorDelayInput_editingFinished()
{
    this->_canSigGen->setDelay(this->ui->signalGeneratorDelayInput->text().toDouble());
}

void Console::on_SigGeneratorDeviceBox_currentIndexChanged(const QString& arg1)
{
    updateMessageBox(arg1, this->ui->SigGeneratorMessageBox, "Message");
}

void Console::on_SigGeneratorMessageBox_currentIndexChanged(const QString& arg1)
{
    try {
        Message* msg = this->_fcp->getMessage(arg1.split(" ").at(0));
        QList<CanSignal*> sigs = msg->getSignals();
        this->ui->SigGeneratorSignalBox->clear();
        foreach (CanSignal* sig, sigs) {
            this->ui->SigGeneratorSignalBox->addItem(sig->getName());
        }
    } catch (JsonException& e) {
        this->ui->exceptions->setText(e.toString());
    }
}

void Console::on_startSigGen_clicked()
{
    this->_canSigGen->setDeviceName(this->ui->SigGeneratorDeviceBox->currentText().split(" ").first());
    this->_canSigGen->setMessageName(this->ui->SigGeneratorMessageBox->currentText().split(" ").first());
    this->_canSigGen->setSignalName(this->ui->SigGeneratorSignalBox->currentText());
    this->_canSigGen->start();
}

void Console::on_stopSigGen_clicked()
{
    this->_canSigGen->stop();
}

void Console::on_resetSigGen_clicked()
{
    this->_canSigGen->reset();
}

void Console::handleNewCanSigGeneratorSample(CANmessage message)
{
    emit send_to_CAN(*COM::encodeToByteArray<CANmessage>(message));
}

void Console::on_signalExpressionInput_editingFinished()
{
    this->_canSigGen->changeFunction(this->ui->signalExpressionInput->text());
}

void Console::on_log_button_clicked(bool checked)
{
    if (checked) {
        auto dir = QDir("log");
        dir.mkpath(".");

        QDateTime current = QDateTime::currentDateTime();

        log_file = new QFile(dir.filePath(current.toString()));
        log_file->open(QIODevice::ReadWrite | QIODevice::Text);

        log_out = new QTextStream(log_file);
        start = current;
        log_count = 0;
    } else {
        log_file->close();
    }

    logging = checked;
}

void Console::load_state(QMap<QString, QVariant> config)
{

    QLayoutItem* item;
    while ((item = ui->filterList_4->layout()->takeAt(0)) != nullptr) {
        delete item->widget();
        delete item;
    }

    // Defaults

    if (config.contains("load_default_ui")) {
        ui->ID->setValue(0);
        ui->DLC->setValue(0);
        ui->word1_2->setValue(0);
        ui->word2_2->setValue(0);
        ui->word3_2->setValue(0);
        ui->word4_2->setValue(0);
        ui->DeviceChoice->setCurrentIndex(0);
        ui->MessageTypeBox->setCurrentIndex(0);
        ui->JsonComponentNameBox->setCurrentIndex(0);
        ui->InterfaceCheckBox->setChecked(0);
        return;
    }

    ui->ID->setValue(config["ID"].toInt());
    ui->DLC->setValue(config["DLC"].toInt());
    ui->word1_2->setValue(config["word1_2"].toInt());
    ui->word2_2->setValue(config["word2_2"].toInt());
    ui->word3_2->setValue(config["word3_2"].toInt());
    ui->word4_2->setValue(config["word4_2"].toInt());

    if (config["fcp_filters"].toList().count() != 0) {
        foreach (auto filter, config["fcp_filters"].toList()) {
            FilterWidget* newFilter = new FilterWidget(filter.toMap()["dev_id"].toInt(), filter.toMap()["msg_id"].toInt(), filter.toMap()["active"].toBool());
            addFilter(newFilter);
        }
    }
    // The UI Load has to be in this specific order

    ui->DeviceChoice->setCurrentIndex(config["FCPCom_device"].toInt());
    ui->MessageTypeBox->setCurrentIndex(config["FCPCom_type"].toInt());
    ui->JsonComponentNameBox->setCurrentIndex(config["FCPCom_signal"].toInt());
    ui->InterfaceCheckBox->setChecked(config["FCPCom_interface"].toBool());

    QMap<QString, QVariant> fcp_input_list = config["FCPCom_values"].toMap();

    foreach (auto fcp_input_box, fcp_input_boxes) {
        fcp_input_box->setValue(fcp_input_list[fcp_input_box->getName()].value<uint64_t>());
    }

    // Load FCP Display

    fcpdisplay_widget->loadConfig(config["fcp_display"].toList());
}

void Console::export_state()
{
    QMap<QString, QVariant> config;
    QList<QVariant> filter_list;
    QMap<QString, QVariant> fcp_input_list;

    config["ID"] = ui->ID->value();
    config["DLC"] = ui->DLC->value();
    config["word1_2"] = ui->word1_2->value();
    config["word2_2"] = ui->word2_2->value();
    config["word3_2"] = ui->word3_2->value();
    config["word4_2"] = ui->word4_2->value();

    foreach (auto filter, filter_vector) {
        QMap<QString, QVariant> filter_pair;
        filter_pair["dev_id"] = filter->getDevId();
        filter_pair["msg_id"] = filter->getMessageId();
        filter_pair["active"] = filter->isActive();

        filter_list.append(filter_pair);
    }

    foreach (auto fcp_input_box, fcp_input_boxes) {
        fcp_input_list[fcp_input_box->getName()] = QVariant::fromValue(fcp_input_box->getValue());
    }

    config["FCPCom_device"] = ui->DeviceChoice->currentIndex();
    config["FCPCom_type"] = ui->MessageTypeBox->currentIndex();
    config["FCPCom_signal"] = ui->JsonComponentNameBox->currentIndex();
    config["FCPCom_interface"] = ui->InterfaceCheckBox->isChecked();

    config["FCPCom_values"] = fcp_input_list;

    config["fcp_filters"] = filter_list;

    config["fcp_display"] = fcpdisplay_widget->exportConfig();

    config_dialog->registerConfig(this->objectName(), config);

    ui->TabWidget->setCurrentIndex(0);
}

void Console::on_loadState_clicked()
{
    config_dialog->show();
}

void Console::on_TabWidget_currentChanged(int index)
{
    if (index == 5) {
        ui->TabWidget->setCurrentIndex(selected_tab);
        config_dialog->show();
    } else {
        selected_tab = index;
    }
}
