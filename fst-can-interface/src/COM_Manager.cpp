#include "COM_Manager.hpp"

Comsguy::Comsguy(QObject* parent, FCPCom* fcp)
    : QObject(parent)
{
    this->currSock = 0;
    this->fcp = fcp;

    // Generate templates

    this->socketPresenceMessage.candata.dev_id = 31;
    this->socketPresenceMessage.candata.msg_id = 32;
    this->socketPresenceMessage.candata.dlc = 2;
    this->socketPresenceMessage.candata.data[0] = 1;

    Templates["Serial"] = new SerialPort;
    Templates.value("Serial")->setObjectName("Serial");
    Templates["Player"] = new LogPlay;
    Templates.value("Player")->setObjectName("Play");
    Templates["Generator"] = new LogGenerator;
    Templates.value("Generator")->setObjectName("Gen");
    Templates["Network"] = new UDPSocket(*(COM::encodeToByteArray<CANmessage>(this->socketPresenceMessage)));
    Templates.value("Network")->setObjectName("UDPSocket");
    Templates["PlotJuggler"] = new PlotJuggler(sizeof(CANmessage), *(COM::encodeToByteArray<CANmessage>(this->socketPresenceMessage)), this->fcp);
    Templates.value("PlotJuggler")->setObjectName("PlotJuggler");
    Templates["SocketCAN"] = new COM_SocketCAN;
    Templates.value("SocketCAN")->setObjectName("SocketCAN");
    Templates["Log"] = new COM_Log;
    Templates.value("Log")->setObjectName("Log");
    Templates["Loopback"] = new COM_Loopback;
    Templates.value("Loopback")->setObjectName("Loopback");
}

QList<COM*> Comsguy::newCOM(QString protocol)
{
    QList<COM*> comPair;
    COM* new_com;

    // new_com_log = new LogRec;

    if (protocol == "Serial") {
        new_com = new SerialPort;
    } else if (protocol == "Player") {
        new_com = new LogPlay;
    } else if (protocol == "Generator") {
        new_com = new LogGenerator;
    } else if (protocol == "Network") {
        new_com = new UDPSocket(*(COM::encodeToByteArray<CANmessage>(this->socketPresenceMessage)));
    } else if (protocol == "PlotJuggler") {
        new_com = new PlotJuggler(sizeof(CANmessage), *(COM::encodeToByteArray<CANmessage>(this->socketPresenceMessage)), this->fcp);
    } else if (protocol == "Interface") {
        new_com = new Interface;
    } else if (protocol == "SocketCAN") {
        new_com = new COM_SocketCAN;
    } else if (protocol == "Log") {
        new_com = new COM_Log;
    } else if (protocol == "socketcan") {
        new_com = new COM_SocketCAN;
    } else {
        qDebug() << "protocol" << protocol;
        Q_ASSERT(1 == 0);
    }

    comPair.append(new_com);
    // comPair.append(new_com_log);
    return comPair;
}

Comsguy::~Comsguy()
{
    return;
}

bool Comsguy::InCOMList(QString com_id)
{
    foreach (auto COM, this->ComList) {
        if (COM[0]->getId() == com_id) {
            return true;
        }
    }
    return false;
}

QList<COM*> Comsguy::getCOM(QString com_id)
{
    QList<COM*> com;
    com.append(nullptr);

    foreach (auto COM, this->ComList) {
        if (COM[0]->getId() == com_id) {
            com = COM;
            break;
        }
    }
    return com;
}

void Comsguy::connectCOM(QString targetone, QString argone,
    QString targettwo, QString argtwo)
{
    QList<COM*> One, Two;

    // First get COMs unique ids
    Templates.value(targetone)->setId(argone);
    Templates.value(targettwo)->setId(argtwo);

    QString targetone_id = Templates.value(targetone)->getId();
    QString targettwo_id = Templates.value(targettwo)->getId();

    // If the COM already exists then use it, otherwise create a new one

    if (InCOMList(targetone_id)) {
        One = getCOM(targetone_id);
    } else {
        One = newCOM(targetone);
        One[0]->setId(argone);
        ComList.append(One);
    }

    if (InCOMList(targettwo_id)) {
        Two = getCOM(targettwo_id);
    } else {
        Two = newCOM(targettwo);
        Two[0]->setId(argtwo);
        ComList.append(Two);
    }

    // Open COM Socket unless if it's already open

    if (One[0]->status() <= 0) {
        One[0]->open(argone);
    }

    if (Two[0]->status() <= 0) {
        Two[0]->open(argtwo);
    }

    // Connect both COMs

    connect(One[0], SIGNAL(new_messages(QByteArray&)),
        Two[0], SLOT(send(QByteArray&)));

    connect(Two[0], SIGNAL(new_messages(QByteArray&)),
        One[0], SLOT(send(QByteArray&)));

    // Increase instances

    One[0]->createInstance();
    Two[0]->createInstance();

    // Idk whats happening here... Some log things I guess

    /*
    if (One[1]->status() <= 0) {
        One[1]->open("");
        connect(One[0],SIGNAL(new_messages(QByteArray&)),
                One[1],SLOT(send(QByteArray&)));
    }
    */
}

void Comsguy::disconnectCOM(QString targetone, QString argone,
    QString targettwo, QString argtwo)
{
    QList<COM*> One, Two;

    // Get COMs unique ids
    Templates.value(targetone)->setId(argone);
    Templates.value(targettwo)->setId(argtwo);

    QString targetone_id = Templates.value(targetone)->getId();
    QString targettwo_id = Templates.value(targettwo)->getId();

    // Check if they exist

    if (!(InCOMList(targetone_id) && InCOMList(targettwo_id))) {
        return;
    }

    One = getCOM(targetone_id);
    Two = getCOM(targettwo_id);

    /*Yet another band aid fix. This COM stuff has 0 expandability and is not developer friendly
    and these 2 lines of code will cause problems down the line*/

    disconnect(One[0], SIGNAL(new_messages(QByteArray&)),
        Two[0], SLOT(send(QByteArray&)));
    disconnect(Two[0], SIGNAL(new_messages(QByteArray&)),
        One[0], SLOT(send(QByteArray&)));

    if (One[0]->status() == 1) {
        One[0]->close();
        disconnect(One[0], SIGNAL(new_messages(QByteArray&)),
            One[0], SLOT(send(QByteArray&)));
    }

    One[0]->deleteInstance();
    Two[0]->deleteInstance();

    if (One[0]->getInstances() == 0) {
        One[0]->close();
    }

    if (Two[0]->getInstances() == 0) {
        Two[0]->close();
    }
}

void Comsguy::connectMainWin(MainWindow* MainWindow)
{
    int index = 0;
    foreach (QString interFace, Templates.keys()) {
        if (interFace.section("@", 1, 1).toInt() > index)
            index = interFace.section("@", 1, 1).toInt();
    }

    QList<COM*> interface_template;
    Interface* temp = new Interface;
    temp->setId(QString::number(index));
    Templates[temp->getId()] = temp;
    interface_template.append(temp);
    interface_template.append(new LogRec);
    ComList.append(interface_template);
    MainWindow->interfaceCom = temp;
}
