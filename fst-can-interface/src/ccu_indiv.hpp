#ifndef CCU_INDIV_HPP
#define CCU_INDIV_HPP

#include "CAN_IDs.h"
#include "ui_ccu_indiv.h"
#include <QWidget>

namespace Ui {
class ccu_indiv;
}

class ccu_indiv : public QWidget {
    Q_OBJECT

public:
    explicit ccu_indiv(QWidget* parent, QString new_name, uint16_t new_dev_id,
        uint16_t set_parameter, uint16_t get_parameter);
    ~ccu_indiv();
    void set_device(QString new_device);
    void set_car_segment(QString new_car_segment);
    void set_name();
    void on_set_but_clicked();
    void get_value();
    void set_new_curr_value(int value);
    void clear();
    int dev_id;
    int get_parameter;
    int set_parameter;
    Ui::ccu_indiv* ui;
    void send_custom_set_msg(int value);

signals:
    void send_CAN(CANmessage);

private:
    QString car_segment, device, name;
    int current_value;
    QString get_name();
};

#endif // CCU_INDIV_HPP
