#ifndef REPORT_HPP
#define REPORT_HPP

#define N_MASTER 2

#define REPORT_RED 1
#define REPORT_GREEN 2
#define REPORT_BLUE 3
#define REPORT_BLACK 0
#define REPORT_ERROR 10

#include "UiWindow.hpp"
#include <QLabel>
#include <QTimer>
#include <QWidget>

#include "Exceptions/helper.hpp"
#include "can-ids/CAN_IDs.h"
#include "can-ids/erros.h"
#include "report_filters.hpp"

namespace Ui {
class Log;
}

class Log : public UiWindow {
    Q_OBJECT

public:
    explicit Log(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    QString getTitle() override { return "Logs"; };
    Report_filters* filters;
    void updateFCP(FCPCom* fcp) override;
    ~Log(void);

private slots:
    void process_CAN_message(CANmessage message) override;
    void on_clearlog_clicked(void);
    void UpdateTimer(void);

    void on_savebutton_clicked();

    void on_StatusLog_clicked();

    void on_filters_button_clicked();
    void init_filters();
    int get_check_status(int msg_id);

    void on_toggleButton_toggled(bool checked);

private:
    Ui::Log* ui;
    int _op_mode_master[N_MASTER];
    bool _refreshBit = true;
    unsigned int _status_flags_mask_master[N_MASTER];
    unsigned int _ts_on;
    unsigned int _ams_error;
    unsigned int _air_feed;
    unsigned int IMD_status;
    unsigned int IMD_statusreset;
    int send_message;
    // void initflags();
    void WriteString(QString, unsigned int);
    void WriteLog(QString, int, int args[3], QString);

    // Status _CarStatus;

signals:
    // void send_to_CAN(CANdata msg);
    /*void detail_voltage(unsigned int cell_n, float voltage);
    void detail_temperature(unsigned int cell_n, float temperature);
    void change_mode(int mode);
    void wire_fault(unsigned int wire_n, bool ok, int type);
    void SetDash(unsigned int AIRm, unsigned int AIRp, unsigned int IMDmem, unsigned int IMDnok, unsigned int AMS);
    void SetDCU(unsigned int OFF1, unsigned int VCC_Siemens, unsigned int VCC_CAN_C, unsigned int temp);
    void SetTE(unsigned int override, unsigned int TPS_plaus, unsigned int TPS_BP_plaus, unsigned int temp);*/
};

#endif // REPORT_HPP
