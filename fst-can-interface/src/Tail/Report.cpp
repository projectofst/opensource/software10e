#include <QCloseEvent>
#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMap>
#include <QString>
#include <QTextStream>

#include "Report.hpp"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/BMS_MASTER_CAN.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/table.h"
#include "report_indiv_filter.hpp"
#include "ui_Report.h"

enum trap_types_enum {
    HARD_TRAP_OSCILATOR_FAIL,
    HARD_TRAP_ADDRESS_ERROR,
    HARD_TRAP_STACK_ERROR,
    HARD_TRAP_MATH_ERROR,
    CUSTOM_TRAP_PARSE_ERROR,
};

const char* const log_level[] = {
    "E",
    "W",
    "D",
    "I"
};

Log::Log(QWidget* parent, FCPCom* fcp, Helper*, ConfigManager*)
    : UiWindow(parent, fcp)
    , ui(new Ui::Log)
{
    ui->setupUi(this);
    send_message = 1;
    WriteString("Welcome to FST CAN Interface log\n", REPORT_BLACK);
    filters = new Report_filters;
    init_filters();
}

Log::~Log()
{
    delete ui;
}

void Log::UpdateTimer()
{
    // timestamp
    QDateTime time = time.currentDateTime();

    if (IMD_status == 0 && IMD_statusreset == 0) {
        ui->LogConsole->setTextColor(Qt::darkGreen);
        ui->LogConsole->append(QString("%1 -- DCU -- IMD NOK and MEM DETECTED").arg(time.toString(QString("yyyy-MM-dd hh:mm:ss.zzz"))));

        IMD_statusreset = 1;
    } else {
        IMD_status = 0;
    }
}

/**********************************************************************
 * Name:    initflags
 * Args:    -
 * Return:  -
 * Desc:    Flag init default state.
 **********************************************************************/
/*void Log::initflags(void){
    _status_flags_mask_master[0]	= 0;
    _status_flags_mask_master[1]	= 0;
    _op_mode_master[0]				= 0;
    _op_mode_master[1]				= 0;
    _ts_on 							= 0;
    _ams_error						= 0;
    _air_feed						= 0;
}*/

void Log::WriteString(QString string, unsigned int colour)
{

    // timestamp
    QDateTime time = time.currentDateTime();

    switch (colour) {
    case REPORT_RED:
        ui->LogConsole->setTextColor(Qt::red);
        break;

    case REPORT_GREEN:
        ui->LogConsole->setTextColor(Qt::darkGreen);
        break;
    case REPORT_BLUE:
        ui->LogConsole->setTextColor(Qt::blue);
        break;
    default:
        ui->LogConsole->setTextColor(Qt::black);
        break;
    }
    if (send_message && _refreshBit)
        ui->LogConsole->append(QString("%1 -- %2").arg(time.toString(QString("hh:mm:ss.zzz"))).arg(string));
    send_message = 1;
    return;
}

void Log::WriteLog(QString log, int level, int args[3], QString dev)
{
    QDateTime time = time.currentDateTime();
    QString time_string = time.toString("hh:mm:ss.zzz");
    ui->LogConsole->setTextColor(Qt::black);
    ui->LogConsole->insertPlainText(time_string);
    auto color = Qt::black;
    switch (level) {
    case 0:
        color = Qt::red;
        break;
    case 1:
        color = Qt::yellow;
        break;
    case 2:
        color = Qt::black;
        break;
    case 3:
        color = Qt::blue;
        break;
    }
    ui->LogConsole->setTextColor(color);
    ui->LogConsole->insertPlainText(QString(" ") + log_level[level] + " ");
    ui->LogConsole->setTextColor(Qt::black);
    ui->LogConsole->insertPlainText(dev + " - " + QString::asprintf(log.toLatin1(), args[0], args[1], args[2]) + "\n");
}

/**********************************************************************
 * Name:    process_CAN_message
 * Args:    CANmessage msg
 * Return:  -
 * Desc:    Process incoming CAN message.
 **********************************************************************/
void Log::process_CAN_message(CANmessage message)
{
    CANdata msg = message.candata;
    JsonLog* log;
    QMap<QString, double> sigs;

    QPair<QString, QMap<QString, double>> r;
    try {
        r = this->_fcp->decodeMessage(message);
        auto name = r.first;
        if (name != "log") {
            return;
        }
        sigs = r.second;
        log = this->_fcp->getLog(sigs["err_code"]);
    } catch (JsonException& e) {
        return;
    }

    int aux_args[3] = { (int)sigs["arg1"], (int)sigs["arg2"], (int)sigs["arg3"] };
    this->WriteLog(log->getString(), sigs["level"], aux_args, this->_fcp->getDevice(msg.dev_id)->getName());
    return;
}

/**********************************************************************
 * Name:    on_clearlog_clicked
 * Args:    -
 * Return:  -
 * Desc:    Clear the console output.
 **********************************************************************/
void Log::on_clearlog_clicked(void)
{
    ui->LogConsole->clear();
}

void Log::on_savebutton_clicked()
{
    QFile outfile;
    outfile.setFileName("log.txt");
    outfile.open(QIODevice::Append | QIODevice::Text);
    QTextStream out(&outfile);
    out << ui->LogConsole->toPlainText() << endl;

    return;
}

void Log::on_StatusLog_clicked()
{
    /*_CarStatus.show();
    _CarStatus.raise();
    _CarStatus.activateWindow();*/

    return;
}

void Log::init_filters()
{
    /*
    for(int k = 0; k < 32; k++){
        if((nomes_placas[k][0] < '0') || (nomes_placas[k][0] > '9')){
            filters->add_device(nomes_placas[k]);
        }
    }
    */
    return;
}

void Log::on_filters_button_clicked()
{
    filters->show();
}

int Log::get_check_status(int msg_id)
{
    return 0;
    Report_indiv_filter* filter = nullptr;
    int not_found = 1;

    if (!filters->size())
        return not_found; /*If there are no filters applied,send message by default*/
#if 0
    for (int k = 0; k < filters->size(); k++){ /*This is not very well done*/
        if(dev_id < 32){
            if(filters->at(k)->get_device_name() == nomes_placas[dev_id]){
                filter = filters->at(k);
                not_found = 0;
            }
        }
    }
#endif

    if (not_found) /*If there are filters applied, and it isnt found, we dont send the message*/
        return 0;

    if (filter != nullptr) { /*Check the status of the boxes*/
        if (filter->get_master_check_box_status()) {
            if (msg_id == CMD_ID_COMMON_GET) {
                return filter->get_get_status();
            } else if (msg_id == CMD_ID_COMMON_SET) {
                return filter->get_set_status();
            } else if (msg_id == MSG_ID_COMMON_ERROR) {
                return filter->get_error_status();
            } else if (msg_id == MSG_ID_COMMON_RESET) {
                return filter->get_reset_status();
            }
        } else {
            return 0;
        }
    }
    return 1;
}

void Log::on_toggleButton_toggled(bool checked)
{
    _refreshBit = !checked;
}

void Log::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}
