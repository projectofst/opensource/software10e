#include "ADCManagerWidget.h"
#include "ui_ADCManagerWidget.h"

ADCManagerWidget::ADCManagerWidget(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::ADCManagerWidget)
{
    ui->setupUi(this);
    this->_adcWidgets = new QList<ADCWidget*>();
    this->_socket = new QUdpSocket();
}

ADCManagerWidget::~ADCManagerWidget()
{
    qDeleteAll(*this->_adcWidgets);
    delete this->_adcWidgets;
    delete this->_socket;
    delete ui;
}

void ADCManagerWidget::on_addADC_clicked()
{
    ADCWidget* newADCWidget = new ADCWidget(this);
    this->_adcWidgets->append(newADCWidget);
    this->ui->insertADCHere->addWidget(newADCWidget);
    connect(newADCWidget, &ADCWidget::deleted, this, &ADCManagerWidget::removeADCWidget);
    // connect(newADCWidget, &ADCWidget::newAdcMessage, this, &ADCManagerWidget::handleNewAdcMessage);
}

void ADCManagerWidget::removeADCWidget(ADCWidget* const widget)
{
    this->_adcWidgets->removeAll(widget);
}

/*
void ADCManagerWidget::handleNewAdcMessage(QString id, uint16_t value){
    ADC* newADC = ProtoBufFactory::createNewADC(id.toStdString(), value);
    std::string str = ProtoBufFactory::createNewADCString(*newADC);
    sendADC(str);
    delete newADC;
}
*/

void ADCManagerWidget::sendADC(const std::string str)
{
    char* buffer = (char*)&str;
    /*get the message into an array of bytes*/
    QByteArray* array = new QByteArray(buffer, str.length());
    this->_socket->write(*array);
}

void ADCManagerWidget::on_reconnect_clicked()
{
    this->_socket->disconnectFromHost();
    quint16 port = 8888;
    this->_socket->connectToHost("127.0.0.1", port);
}
