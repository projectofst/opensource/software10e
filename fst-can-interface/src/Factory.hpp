#ifndef FACTORY_HPP
#define FACTORY_HPP

#include "ConfigManager/configmanager.hpp"
#include "Exceptions/helper.hpp"
#include "fcpcom.hpp"
#include <QMap>
#include <QString>
#include <QWidget>

template <typename T>
class Factory {
public:
    template <typename TDerived>
    void registerType(QString name)
    {
        static_assert(std::is_base_of<T, TDerived>::value, "Factory::registerType doesn't accept this type because doesn't derive from base class");
        _createFuncs[name] = &createFunc<TDerived>;
    }

    T* create(QString name, QWidget* parent, FCPCom* fcp, Helper* log, ConfigManager* config_manager)
    {
        typename QMap<QString, PCreateFunc>::const_iterator it = _createFuncs.find(name);
        if (it != _createFuncs.end()) {
            return it.value()(parent, fcp, log, config_manager);
        }
        return nullptr;
    }

private:
    template <typename TDerived>
    static T* createFunc(QWidget* parent, FCPCom* fcp, Helper* log, ConfigManager* config_manager)
    {
        return new TDerived(parent, fcp, log, config_manager);
    }

    typedef T* (*PCreateFunc)(QWidget*, FCPCom*, Helper*, ConfigManager*);
    QMap<QString, PCreateFunc> _createFuncs;
};

#endif // FACTORY_HPP
