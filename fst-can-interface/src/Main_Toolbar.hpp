#ifndef MAIN_TOOLBAR_HPP
#define MAIN_TOOLBAR_HPP

#include <QAction>
#include <QApplication>
#include <QIcon>
#include <QMainWindow>
#include <QToolBar>
#include <QtWidgets>

#include "WindowMain.hpp"

class MainToolbar : public MainWindow {

    Q_OBJECT

public:
    MainToolbar(MainWindow* parent = nullptr);
    QToolBar* toolbar;
    QComboBox* LayoutCombo;
    QAction* COM;
    QAction* temp;
    MainWindow* mainwindow;

public slots:
    void widgetAction();

signals:
    void openUiWindow(QString, bool);
    void saveLayout(void);
    void deleteLayout(void);
    void loadLayout(QString);
private slots:
    void launch_plotjuggler();
    void launch_coms();
};

#endif // MAIN_TOOLBAR_HPP
