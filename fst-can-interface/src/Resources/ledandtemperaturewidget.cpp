#include "ledandtemperaturewidget.hpp"
#include "ui_ledandtemperaturewidget.h"

ledAndTemperatureWidget::ledAndTemperatureWidget(QWidget* parent, QString text)
    : QWidget(parent)
    , ui(new Ui::ledAndTemperatureWidget)
{
    ui->setupUi(this);

    this->_displayText = text;

    /*default values*/
    this->ui->text->setText(this->_displayText);
    this->ui->value->setText("--,- °C");
}

ledAndTemperatureWidget::ledAndTemperatureWidget(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::ledAndTemperatureWidget)
{
    ui->setupUi(this);

    this->ui->text->setText("NO TEXT");
    this->ui->value->setText("--/-- °C");
}

ledAndTemperatureWidget::~ledAndTemperatureWidget()
{
    delete ui;
}

double ledAndTemperatureWidget::getTemperature()
{
    return this->_temperature;
}

void ledAndTemperatureWidget::updateTemperature(double temperature)
{
    this->_temperature = temperature;
    updateUiTemperature(temperature);
}

void ledAndTemperatureWidget::updateUiTemperature(double temperature)
{
    QString tempStr;

    tempStr = QString::number(this->_temperature) + " °C";

    this->ui->value->setText(tempStr);

    if (temperature == 500) {
        this->ui->LED->turnYellow();
        this->ui->value->setText("N/A");
    } else if (temperature == 600) {
        this->ui->LED->turnOff();
        this->ui->value->setText("--/-- °C");
    } else if (temperature > 85.0) {
        this->ui->LED->turnRed();
    } else {
        this->ui->LED->turnGreen();
    }
}
