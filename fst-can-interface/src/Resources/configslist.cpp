#include "configslist.h"
#include "COM.hpp"
#include "ui_configslist.h"

ConfigsList::ConfigsList(QWidget* parent, FCPCom* fcp)
    : QWidget(parent)
    , ui(new Ui::ConfigsList)
{
    this->fcp = fcp;
    this->sets = new QVector<SetLine*>();

    auto timer = new QTimer();
    connect(timer, &QTimer::timeout, this, &ConfigsList::request_all_configs);
    timer->start(1000);

    ui->setupUi(this);
}

bool ConfigsList::list_configs(QString device_name)
{
    this->device_name = device_name;
    auto _device = this->fcp->getDeviceSafe(this->device_name);
    if (!_device.has_value()) {
        return true;
    }

    auto device = _device.value();
    auto configs = device->getConfigs();

    for (auto config : configs) {
        if ((!configs_filter.isEmpty() && configs_filter.contains(config->getName())) || configs_filter.isEmpty()) {
            auto set = new SetLine(this, fcp);
            set->setEditable(false);
            set->hide_device();
            set->setDevice(this->device_name);
            set->setConfig(config->getName());
            this->sets->append(set);
            ui->params_list->addWidget(set);
            connect(set, &FcpRunnable::send_can, this, [this](CANmessage msg) { sendCAN(msg); });
        }
    }

    return false;
}

void ConfigsList::request_all_configs()
{
    for (auto config : *(this->sets)) {
        auto msg = fcp->encodeGetConfig("interface", device_name, config->getConfig());
        emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    }
}

void ConfigsList::sendCAN(CANmessage msg)
{
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}

void ConfigsList::setValue(int id, double value)
{
    for (auto set : *(this->sets)) {
        auto config = this->fcp->getConfig(set->getDevice(), set->getConfig());
        if (config->getId() == id) {
            set->setValue(value);
        }
    }
}

ConfigsList::~ConfigsList()
{
    delete ui;
}

void ConfigsList::on_send_all_button_clicked()
{
    for (auto config : *(this->sets)) {
        auto value = config->getValue();
        qDebug() << "value" << value;
        if (value == "") {
            continue;
        }

        qDebug() << config->getConfig();

        auto msg = this->fcp->encodeSetConfig("interface", "iib", config->getConfig(), value.toInt());
        emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
    }
}

void ConfigsList::on_save_flash_button_clicked()
{
    auto msg = this->fcp->encodeSendCommand("interface", "iib", "save_flash", 1, 0, 0);
    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(msg).get()));
}
