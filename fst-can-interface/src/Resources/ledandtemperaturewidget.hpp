#ifndef LEDANDTEMPERATUREWIDGET_HPP
#define LEDANDTEMPERATUREWIDGET_HPP

#include "LEDwidget.hpp"
#include <QWidget>

namespace Ui {
class ledAndTemperatureWidget;
}

class ledAndTemperatureWidget : public QWidget {
    Q_OBJECT

public:
    explicit ledAndTemperatureWidget(QWidget* parent = nullptr, QString text = "NO TEXT");
    ledAndTemperatureWidget(QWidget* parent);

    ~ledAndTemperatureWidget();

    double getTemperature();
    void updateTemperature(double temperature);

private slots:
    void updateUiTemperature(double temperature);

private:
    Ui::ledAndTemperatureWidget* ui;
    double _temperature;
    QString _displayText;
};

#endif // ledAndTemperatureWidget_HPP
