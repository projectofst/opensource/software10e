#include "DataAlarmManager.hpp"
#include <QDebug>
DataAlarmManager::DataAlarmManager(QObject* parent)
    : QObject(parent)
{
    this->_dataAlarms = std::unique_ptr<QMap<QByteArray, std::shared_ptr<DataAlarm>>>(new QMap<QByteArray, std::shared_ptr<DataAlarm>>());
}

void DataAlarmManager::stopAll()
{
    for (auto it = this->_dataAlarms->begin(); it != this->_dataAlarms->end(); ++it) {
        it->get()->stop();
    }
    return;
}
void DataAlarmManager::startAll()
{
    for (auto it = this->_dataAlarms->begin(); it != this->_dataAlarms->end(); ++it) {
        it->get()->start();
    }
    return;
}

void DataAlarmManager::addNewAlarm(QByteArray& data, int frequency)
{

    this->_dataAlarms->insert(data, std::unique_ptr<DataAlarm>(new DataAlarm(this, data, frequency)));
    connect(this->_dataAlarms->value(data).get(), &DataAlarm::dataReady, this, &DataAlarmManager::handleTimeout);
    connect(this->_dataAlarms->value(data).get(), &DataAlarm::durationTimeout, this, &DataAlarmManager::handleDurationTimeout);
}

void DataAlarmManager::addNewTimedAlarm(QByteArray& data, int frequency, int duration)
{

    this->_dataAlarms->insert(data, std::unique_ptr<DataAlarm>(new DataAlarm(this, data, frequency, duration)));
    connect(this->_dataAlarms->value(data).get(), &DataAlarm::dataReady, this, &DataAlarmManager::handleTimeout);
    connect(this->_dataAlarms->value(data).get(), &DataAlarm::durationTimeout, this, &DataAlarmManager::handleDurationTimeout);
}

void DataAlarmManager::stopDataTransmission(QByteArray& data)
{
    this->_dataAlarms->value(data).get()->stop();
}

void DataAlarmManager::stopAllDataTransmission()
{
    for (auto it = this->_dataAlarms->begin(); it != this->_dataAlarms->end(); it++) {
        it->get()->stop();
    }
    return;
}

void DataAlarmManager::clear()
{
    this->_dataAlarms->clear();
}

void DataAlarmManager::startDataTransmission(QByteArray& data)
{
    this->_dataAlarms->value(data).get()->start();
}

void DataAlarmManager::handleTimeout(std::shared_ptr<QByteArray> data)
{
    emit newData(data);
}

void DataAlarmManager::handleDurationTimeout()
{
    emit durationTimeout();
}
