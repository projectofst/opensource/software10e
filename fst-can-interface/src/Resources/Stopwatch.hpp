#ifndef STOPWATCH_HPP
#define STOPWATCH_HPP

#include <QElapsedTimer>
#include <QTimer>
#include <QWidget>
#include <memory>

#define REFRESH_RATE 10
namespace Ui {
class Stopwatch;
}

class Stopwatch : public QWidget {
    Q_OBJECT

public:
    explicit Stopwatch(QWidget* parent = nullptr, int time = 0);
    ~Stopwatch();

    void setTime(int time);

    void start();
    void stop();
    void reset();
    void resume();

private:
    Ui::Stopwatch* ui;
    int _time = 0;
    std::unique_ptr<QElapsedTimer> _timer;
    std::unique_ptr<QTimer> _controlTimer;

private slots:
    void updateStopWatchUi(int time);
    void calculateElapsedTime();

signals:
    void timeout();
};

#endif // STOPWATCH_HPP
