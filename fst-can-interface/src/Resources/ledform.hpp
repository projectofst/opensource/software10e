#ifndef LEDFORM_H
#define LEDFORM_H

#include <QWidget>

namespace Ui {
class LEDForm;
}

class LEDForm : public QWidget {
    Q_OBJECT

public:
    explicit LEDForm(QWidget* parent = 0, QString label = "Label");
    ~LEDForm();
    void turnGreen();
    void turnRed();
    void turnBlue();
    void turnYellow();
    void turnCyan();
    void turnOff();

private slots:

private:
    Ui::LEDForm* ui;
};

#endif // LEDFORM_H
