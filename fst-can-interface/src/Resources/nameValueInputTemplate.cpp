#include "nameValueInputTemplate.hpp"
#include "ui_nameValueInputTemplate.h"

nameValueInputTemplate::nameValueInputTemplate(QWidget* parent, QString name)
    : QWidget(parent)
    , ui(new Ui::nameValueInputTemplate)
{
    ui->setupUi(this);
    this->_name = name;
    this->_value = 0;
    this->ui->name->setText(name);
}

nameValueInputTemplate::~nameValueInputTemplate()
{
    delete ui;
}

void nameValueInputTemplate::on_valueInpit_textChanged(const QString& arg1)
{
    this->_value = arg1.toLong();
}

void nameValueInputTemplate::setValue(uint64_t value)
{
    this->_value = value;
    this->ui->valueInpit->setText(QString::number(value));
}
