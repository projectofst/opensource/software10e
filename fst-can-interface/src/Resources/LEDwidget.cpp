#include "LEDwidget.hpp"

LEDwidget::LEDwidget(QWidget* parent)
    : QWidget(parent)
{
    turnOff();
    return;
}

LEDwidget::~LEDwidget(void)
{
    return;
}

void LEDwidget::setColor(QColor new_color)
{
    if (color != new_color) {
        color = new_color;
        update();
        repaint();
    }
}

void LEDwidget::setLambda(std::function<bool(double)> new_lambda)
{
    this->lambda = new_lambda;
}

void LEDwidget::updateValue(double value)
{
    turnGreenIf(lambda(value));
}

void LEDwidget::paintEvent(QPaintEvent*)
{
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBrush(color);
    painter.drawEllipse(0, 0, width(), height());
    painter.end();
    return;
}

void LEDwidget::turnGreenIf(bool check)
{
    if (check) {
        turnGreen();
    } else {
        turnOff();
    }
}
