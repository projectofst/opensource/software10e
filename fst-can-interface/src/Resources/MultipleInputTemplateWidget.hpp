#ifndef MULTIPLEINPUTTEMPLATEWIDGET_HPP
#define MULTIPLEINPUTTEMPLATEWIDGET_HPP

#include "Resources/InputAndNameTemplateWidget.hpp"
#include <QMap>
#include <QWidget>
#include <memory>

namespace Ui {
class MultipleInputTemplateWidget;
}

class MultipleInputTemplateWidget : public QWidget {
    Q_OBJECT

public:
    explicit MultipleInputTemplateWidget(QWidget* parent, QString name, QString displayName, QList<QString> inputWidgetNames);
    ~MultipleInputTemplateWidget();

    void setButtonText(QString newButtonName);
    QString getButtonText();

    QString getInputCurrentText(QString inputName);

    double getInputCurrentTextAsDouble(QString inputName);

    int getInputCurrentTextAsInt(QString inputName);

    void setButtonVisibility(bool value);

    bool isButtonVisible();

private slots:
    void addInputWidget(QString inputName);
    void on_button_clicked();

private:
    Ui::MultipleInputTemplateWidget* ui;
    std::unique_ptr<QMap<QString, InputAndNameTemplateWidget*>> _inputWidgetMap;
    QString _idName;

signals:
    void emitInputValues(QString inputName, QMap<QString, QString> values);
};

#endif // MULTIPLEINPUTTEMPLATEWIDGET_HPP
