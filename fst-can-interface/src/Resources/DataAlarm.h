#ifndef DATAALARM_H
#define DATAALARM_H

#include <QMap>
#include <QObject>
#include <QTimer>
#include <memory>

class DataAlarm : public QObject {
    Q_OBJECT
public:
    DataAlarm();

    /**
     * @brief DataAlarm
     * @param parent - Parent QObject
     * @param dataDa
     * @param frequency
     */
    DataAlarm(QObject* parent, QByteArray& data, int frequency);
    DataAlarm(QObject* parent, QByteArray& data, int frequency, int duration);

    /**
     * @brief stop - stops the timer.
     */
    void stop();

    /**
     * @brief start - starts the timer;
     */
    void start();

    /**
     * @brief setFrequency
     * @param frequency - New Frequency.
     */
    void setFrequency(int frequency);

private:
    std::shared_ptr<QTimer> _timer;
    std::shared_ptr<QTimer> _durationTimer;
    std::shared_ptr<QByteArray> _data;
    int _frequency, _duration = 0;

private slots:
    void handleTimeout();
    void handleDurationTimeout();

signals:
    void durationTimeout();
    void dataReady(std::shared_ptr<QByteArray> data);
};

#endif // DATAALARM_H
