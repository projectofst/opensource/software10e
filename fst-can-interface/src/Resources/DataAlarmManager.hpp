#ifndef DATAALARMMANAGER_HPP
#define DATAALARMMANAGER_HPP

#include <QObject>
#include <Resources/DataAlarm.h>
#include <memory>

class DataAlarmManager : public QObject {
    Q_OBJECT
public:
    explicit DataAlarmManager(QObject* parent = nullptr);

private slots:

    void handleTimeout(std::shared_ptr<QByteArray> data);
    void handleDurationTimeout();

public:
    void stopAll();
    void startAll();
    void stopDataTransmission(QByteArray& data);
    void startDataTransmission(QByteArray& data);
    void addNewAlarm(QByteArray& data, int frequency);
    void addNewTimedAlarm(QByteArray& data, int frequency, int duration);
    void stopAllDataTransmission();
    void clear();

private:
    std::unique_ptr<QMap<QByteArray, std::shared_ptr<DataAlarm>>> _dataAlarms;
signals:
    void durationTimeout();
    void newData(std::shared_ptr<QByteArray> data);
};

#endif // DATAALARMMANAGER_HPP
