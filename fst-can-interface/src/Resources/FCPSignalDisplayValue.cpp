#include "FCPSignalDisplayValue.hpp"
#include "ui_fcpsignaldisplayvalue.h"

FCPSignalDisplayValue::FCPSignalDisplayValue(QWidget* parent, QString deviceName, QString signalName, int mux_count, QString muxName)
    : QWidget(parent)
    , ui(new Ui::FCPSignalDisplayValue)
{
    ui->setupUi(this);
    this->_deviceName = deviceName;
    this->_signalName = signalName;
    this->_muxName = muxName;
    this->_muxCount = mux_count;
    this->ui->Signal->setText(signalName);
    this->ui->Device->setText(deviceName);
    this->ui->Mux->setText(muxName + QString("[%1]").arg(this->_muxCount));

    ui->textSpacer->setVisible(false);
}

FCPSignalDisplayValue::~FCPSignalDisplayValue()
{
    delete ui;
}

void FCPSignalDisplayValue::updateValue(double newValue)
{
    this->_latestValue = newValue;
    QString value = QString::number(newValue);
    this->ui->latestValue->setText(value);
    if (led != nullptr) {
        this->led->updateValue(newValue);
    }
}

void FCPSignalDisplayValue::showDeleteButton(bool show_button)
{
    ui->deleteButton->setVisible(show_button);
}

void FCPSignalDisplayValue::showDeviceName(bool show_devicename)
{
    ui->Device->setVisible(show_devicename);
}

void FCPSignalDisplayValue::setLed(LEDwidget* led)
{
    this->led = led;
    this->led->setMaximumHeight(50);
    this->led->setMaximumWidth(18);
    this->led->setMinimumWidth(18);
    ui->horizontalLayout_3->addWidget(this->led);
}

void FCPSignalDisplayValue::showValue(bool show_value)
{
    ui->latestValue->setVisible(show_value);
    ui->label_4->setVisible(show_value);
    if (!show_value) {
        ui->textSpacer->setVisible(true);
    }
}

QString FCPSignalDisplayValue::getDeviceName()
{
    return this->_deviceName;
}

QString FCPSignalDisplayValue::getSignalName()
{
    return this->_signalName;
}

QString FCPSignalDisplayValue::getFilter()
{
    return this->_signalName + QString::number(_muxCount);
}

int FCPSignalDisplayValue::getMuxCount()
{
    return this->_muxCount;
}

void FCPSignalDisplayValue::on_deleteButton_clicked()
{
    this->~FCPSignalDisplayValue();
}
