#include "Stopwatch.hpp"
#include "ui_Stopwatch.h"
#include <QDebug>

Stopwatch::Stopwatch(QWidget* parent, int time)
    : QWidget(parent)
    , ui(new Ui::Stopwatch)
{
    ui->setupUi(this);

    this->_time = time;
    this->updateStopWatchUi(time);

    this->_timer = std::unique_ptr<QElapsedTimer>(new QElapsedTimer());

    this->_controlTimer = std::unique_ptr<QTimer>(new QTimer());
    this->_controlTimer->setInterval(REFRESH_RATE);
    connect(this->_controlTimer.get(), &QTimer::timeout, this, &Stopwatch::calculateElapsedTime);
}

Stopwatch::~Stopwatch()
{
    delete ui;
}

void Stopwatch::setTime(int time)
{
    this->_time = time;
}

void Stopwatch::start()
{
    this->_timer->start();
    this->_controlTimer->start();
}

void Stopwatch::stop()
{
    this->_timer->invalidate();
    this->_controlTimer->stop();
}

void Stopwatch::updateStopWatchUi(int time)
{
    if (time < 0) {
        time = 0;
    }
    qint64 miliseconds, seconds, minutes;

    miliseconds = time % 1000;
    time = qint64(time / 1000);
    seconds = time % 60;
    time = qint64(time / 60);
    minutes = time;
    char pretty_time[32];
    sprintf(pretty_time, "%02lld:%02lld.%03lld", minutes, seconds, miliseconds);
    QString x = pretty_time; /*macacada*/
    this->ui->time->setText(x);
}

void Stopwatch::calculateElapsedTime()
{
    if (this->_timer->isValid()) {
        int elapsedTime = this->_timer->elapsed();
        int remainingTime = this->_time - elapsedTime;
        if (remainingTime < 0) {
            this->stop();
        }
        updateStopWatchUi(remainingTime);
    }
}
void Stopwatch::reset()
{
    this->stop();
    this->ui->time->setText("--.--,---");
}
