#ifndef CONFIGSLIST_H
#define CONFIGSLIST_H

#include <QWidget>

#include "ConfigWindow/setline.h"
#include "fcpcom.hpp"

namespace Ui {
class ConfigsList;
}

class ConfigsList : public QWidget {
    Q_OBJECT

public:
    // Builder system
    class Builder {
    public:
        ConfigsList* configlist_object = nullptr;

        Builder(QWidget* parent, FCPCom* fcp)
        {
            configlist_object = new ConfigsList(parent, fcp);
        }

        Builder& withDevice(QString device)
        {
            configlist_object->setDevice(device);
            return *this;
        }

        Builder& withConfig(QString config)
        {
            configlist_object->addConfig(config);
            return *this;
        }

        ConfigsList* Build() { return configlist_object; }
    };

    explicit ConfigsList(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    bool list_configs(QString);
    void setValue(int id, double value);
    void addConfig(QString name) { configs_filter.append(name); };
    void setDevice(QString device) { list_configs(device); };
    ~ConfigsList();

private:
    QList<QString> configs_filter;
    Ui::ConfigsList* ui;
    Ui::ConfigsList* config;
    FCPCom* fcp;
    QString device_name;
    QVector<SetLine*>* sets;
    void sendCAN(CANmessage msg);
    void request_all_configs();

signals:
    void send_to_CAN(QByteArray&);
private slots:
    void on_send_all_button_clicked();
    void on_save_flash_button_clicked();
};

#endif // CONFIGSLIST_H
