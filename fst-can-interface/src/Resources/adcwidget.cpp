#include "adcwidget.hpp"
#include "ui_adcwidget.h"

ADCWidget::ADCWidget(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::ADCWidget)
{
    ui->setupUi(this);
    this->_id = "";
    this->_frequency = 0;
    this->_value = 0;
    this->_timer = new QTimer();
    connect(this->_timer, &QTimer::timeout, this, &ADCWidget::sendADCMessage);
}

ADCWidget::~ADCWidget()
{
    emit deleted(this);
    delete this->_timer;
    delete ui;
}

void ADCWidget::on_idInput_textChanged(const QString& arg1)
{
    this->_id = arg1;
}

void ADCWidget::on_valueInput_textChanged(const QString& arg1)
{
    this->_value = arg1.toInt();
}

void ADCWidget::on_toggleButton_clicked()
{
    bool sendState = this->ui->toggleButton->isChecked();

    if (sendState == false) {
        this->_timer->stop();
    } else {
        if (this->_frequency != 0) {
            this->_timer->start(this->_frequency);
        } else {
            this->ui->toggleButton->setChecked(false);
            emit newAdcMessage(this->_id, this->_value);
        }
    }
}

void ADCWidget::sendADCMessage()
{
    emit newAdcMessage(this->_id, this->_value);
}

void ADCWidget::on_deleteButton_clicked()
{
    delete this;
}

void ADCWidget::on_frequencyInput_textChanged(const QString& arg1)
{
    if (arg1 == "") {
        this->_frequency = 0;
    } else {
        this->_frequency = arg1.toInt();
    }
}
