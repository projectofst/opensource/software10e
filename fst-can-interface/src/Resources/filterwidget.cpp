#include "filterwidget.hpp"
#include "ui_filterwidget.h"
#include <QCheckBox>
#include <QDebug>

FilterWidget::FilterWidget()
    : ui(new Ui::FilterWidget)
{
    ui->setupUi(this);
    ui->enabled->setCheckState(Qt::Checked);
    this->_devId = -1;
    this->_messageId = -1;
    this->_active = true;
}

FilterWidget::FilterWidget(int devId, int messageId, bool actv)
    : ui(new Ui::FilterWidget)
{
    ui->setupUi(this);
    ui->enabled->setCheckState(Qt::Checked);
    this->_devId = devId;
    this->_messageId = messageId;
    this->_active = actv;
    this->ui->dev_id->setText(QString::number(this->_devId));
    this->ui->msg_id->setText(QString::number(this->_messageId));
    this->ui->enabled->setChecked(this->_active);
}

FilterWidget::FilterWidget(Device* dev, Message* msg)
    : ui(new Ui::FilterWidget)
{
    ui->setupUi(this);
    ui->enabled->setCheckState(Qt::Checked);
    this->_devId = dev->getId();
    this->_messageId = msg->getId();
    this->_active = this->isActive();
    this->_isReadOnly = true;
    this->ui->dev_id->setText(dev->getName() + " (" + QString::number(dev->getId()) + ")");
    this->ui->msg_id->setText(msg->getName() + " (" + QString::number(msg->getId()) + ")");
    this->ui->enabled->setChecked(this->_active);
    this->ui->dev_id->setReadOnly(true);
    this->ui->msg_id->setReadOnly(true);
}

FilterWidget::~FilterWidget()
{
    delete ui;
}

void FilterWidget::on_delete_2_clicked()
{
    emit destroyed();
    this->~FilterWidget();
}

void FilterWidget::on_enabled_stateChanged(int active)
{
    this->_active = active;
}

int FilterWidget::getDevId()
{
    return this->_devId;
}

int FilterWidget::getMessageId()
{
    return this->_messageId;
}

bool FilterWidget::isActive()
{
    return this->_active;
}

void FilterWidget::on_dev_id_textChanged(const QString& arg1)
{
    if (this->_isReadOnly) {
        return;
    }

    if (arg1 == "") {
        this->_devId = -1;
        return;
    }
    this->_devId = arg1.toInt();
}

void FilterWidget::on_msg_id_textChanged(const QString& arg1)
{
    if (this->_isReadOnly) {
        return;
    }
    if (arg1 == "") {
        this->_messageId = -1;
        return;
    }
    this->_messageId = arg1.toInt();
}
