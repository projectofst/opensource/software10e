#ifndef FCPSIGNALDISPLAYVALUE_HPP
#define FCPSIGNALDISPLAYVALUE_HPP

#include "Resources/LEDwidget.hpp"
#include <QWidget>
#include <qdebug.h>

namespace Ui {
class FCPSignalDisplayValue;
}

class FCPSignalDisplayValue : public QWidget {
    Q_OBJECT

public:
    explicit FCPSignalDisplayValue(QWidget* parent = nullptr, QString deviceName = "", QString signalName = "", int mux_count = 0, QString muxName = "");
    ~FCPSignalDisplayValue();

    void updateValue(double newValue);
    void showDeleteButton(bool show_button);
    void showDeviceName(bool show_devicename);
    void showValue(bool show_value);
    void setLed(LEDwidget* led);
    QString getDeviceName();
    QString getSignalName();
    QString getFilter();
    int getMuxCount();

private slots:
    void on_deleteButton_clicked();

private:
    Ui::FCPSignalDisplayValue* ui;
    LEDwidget* led = nullptr;
    QString _deviceName, _signalName, _muxName;
    int _muxCount, _listId;
    double _latestValue;
};

class Builder {
public:
    FCPSignalDisplayValue* fcpsignalvalue_object = nullptr;

    Builder(QWidget* parent, QString deviceName, QString signalName, int mux_count, QString muxName)
    {
        fcpsignalvalue_object = new FCPSignalDisplayValue(parent, deviceName, signalName, mux_count, muxName);
    }

    Builder& withShowValue(bool show_value)
    {
        fcpsignalvalue_object->showValue(show_value);
        return *this;
    }

    Builder& withLed(LEDwidget* led)
    {
        fcpsignalvalue_object->setLed(led);
        return *this;
    }

    FCPSignalDisplayValue* Build() { return fcpsignalvalue_object; }
};

#endif // FCPSIGNALDISPLAYVALUE_HPP
