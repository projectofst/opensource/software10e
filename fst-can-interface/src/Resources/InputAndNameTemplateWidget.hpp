#ifndef INPUTANDNAMETEMPLATEWIDGET_HPP
#define INPUTANDNAMETEMPLATEWIDGET_HPP

#include <QWidget>

namespace Ui {
class InputAndNameTemplateWidget;
}

class InputAndNameTemplateWidget : public QWidget {
    Q_OBJECT

public:
    explicit InputAndNameTemplateWidget(QWidget* parent = nullptr, QString name = "NO NAME");
    ~InputAndNameTemplateWidget();

    void setName(QString newName);
    QString getName();

    QString getCurrentValueAsString();
    double getCurrentValueAsDouble();
    int getCurrentValueAsInt();

private:
    Ui::InputAndNameTemplateWidget* ui;
};

#endif // INPUTANDNAMETEMPLATEWIDGET_HPP
