#include "InputAndNameTemplateWidget.hpp"
#include "ui_InputAndNameTemplateWidget.h"

#include <QDebug>
InputAndNameTemplateWidget::InputAndNameTemplateWidget(QWidget* parent, QString name)
    : QWidget(parent)
    , ui(new Ui::InputAndNameTemplateWidget)
{
    ui->setupUi(this);
    this->ui->name->setText(name);
    this->ui->name->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    this->ui->input->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
}

InputAndNameTemplateWidget::~InputAndNameTemplateWidget()
{
    delete ui;
}

void InputAndNameTemplateWidget::setName(QString newName)
{
    this->ui->name->setText(newName);
}
QString InputAndNameTemplateWidget::getName()
{
    return this->ui->name->text();
}

QString InputAndNameTemplateWidget::getCurrentValueAsString()
{
    return this->ui->input->text();
}
double InputAndNameTemplateWidget::getCurrentValueAsDouble()
{
    return this->getCurrentValueAsString().toDouble();
}
int InputAndNameTemplateWidget::getCurrentValueAsInt()
{
    return this->getCurrentValueAsString().toInt();
}
