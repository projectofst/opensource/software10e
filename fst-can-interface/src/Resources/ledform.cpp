#include <QLabel>

#include "ledform.hpp"
#include "ui_ledform.h"

LEDForm::LEDForm(QWidget* parent, QString label)
    : QWidget(parent)
    , ui(new Ui::LEDForm)
{
    ui->setupUi(this);
    ui->label->setText(label);
    ui->led->turnGreen();
}

LEDForm::~LEDForm()
{
    delete ui;
}

void LEDForm::turnGreen()
{
    this->ui->led->turnGreen();
}
void LEDForm::turnRed()
{
    this->ui->led->turnRed();
}
void LEDForm::turnBlue()
{
    this->ui->led->turnBlue();
}
void LEDForm::turnYellow()
{
    this->ui->led->turnYellow();
}
void LEDForm::turnCyan()
{
    this->ui->led->turnCyan();
}
void LEDForm::turnOff()
{
    this->ui->led->turnOff();
}
