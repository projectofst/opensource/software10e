#ifndef ADCWIDGET_HPP
#define ADCWIDGET_HPP

#include <QTimer>
#include <QWidget>

namespace Ui {
class ADCWidget;
}

class ADCWidget : public QWidget {
    Q_OBJECT

public:
    explicit ADCWidget(QWidget* parent = nullptr);
    ~ADCWidget();

private slots:

    void on_idInput_textChanged(const QString& arg1);

    void on_valueInput_textChanged(const QString& arg1);

    void on_toggleButton_clicked();

    void sendADCMessage();

    void on_deleteButton_clicked();

    void on_frequencyInput_textChanged(const QString& arg1);

private:
    Ui::ADCWidget* ui;
    QString _id;
    int _value;
    uint16_t _frequency;
    QTimer* _timer;

signals:
    void newAdcMessage(QString id, uint16_t value);
    void deleted(ADCWidget* widget);
};

#endif // ADCWIDGET_HPP
