#ifndef PLOT_FILTER_WIDGET_H
#define PLOT_FILTER_WIDGET_H

#include <QWidget>

namespace Ui {
class plot_filter_widget;
}

class plot_filter_widget : public QWidget {
    Q_OBJECT

public:
    explicit plot_filter_widget(QWidget* parent = nullptr);
    ~plot_filter_widget();
    Ui::plot_filter_widget* ui;
    bool active;
private slots:

    void on_delete_but_clicked();

private:
};

#endif // PLOT_FILTER_WIDGET_H
