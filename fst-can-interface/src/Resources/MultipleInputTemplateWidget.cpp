#include "MultipleInputTemplateWidget.hpp"
#include "ui_MultipleInputTemplateWidget.h"
#include <QDebug>
MultipleInputTemplateWidget::MultipleInputTemplateWidget(QWidget* parent, QString idName, QString displayName, QList<QString> inputWidgetNames)
    : QWidget(parent)
    , ui(new Ui::MultipleInputTemplateWidget)
{
    ui->setupUi(this);
    this->_idName = idName;
    this->_inputWidgetMap = std::unique_ptr<QMap<QString, InputAndNameTemplateWidget*>>(new QMap<QString, InputAndNameTemplateWidget*>());
    foreach (QString name, inputWidgetNames) {
        this->addInputWidget(name);
    }
    this->ui->nameLabel->setText(displayName);
}

MultipleInputTemplateWidget::~MultipleInputTemplateWidget()
{
    this->_inputWidgetMap->clear();
    delete ui;
}

void MultipleInputTemplateWidget::setButtonText(QString newButtonText)
{
    this->ui->button->setText(newButtonText);
}

QString MultipleInputTemplateWidget::getButtonText()
{
    return this->ui->button->text();
}

void MultipleInputTemplateWidget::addInputWidget(QString inputName)
{
    if (!this->_inputWidgetMap->contains(inputName)) {
        InputAndNameTemplateWidget* newWidget = new InputAndNameTemplateWidget(this, inputName);
        this->_inputWidgetMap->insert(newWidget->getName(), newWidget);
        this->ui->inputLayout->addWidget(newWidget);
    }
}

QString MultipleInputTemplateWidget::getInputCurrentText(QString inputName)
{
    return this->_inputWidgetMap->value(inputName)->getCurrentValueAsString();
}

double MultipleInputTemplateWidget::getInputCurrentTextAsDouble(QString inputName)
{
    return this->_inputWidgetMap->value(inputName)->getCurrentValueAsDouble();
}

int MultipleInputTemplateWidget::getInputCurrentTextAsInt(QString inputName)
{
    return this->_inputWidgetMap->value(inputName)->getCurrentValueAsInt();
}

void MultipleInputTemplateWidget::on_button_clicked()
{
    QMap<QString, QString> values;
    for (auto it = this->_inputWidgetMap->begin(); it != this->_inputWidgetMap->end(); it++) {
        values.insert(it.value()->getName(), it.value()->getCurrentValueAsString());
    }
    emit emitInputValues(this->_idName, values);
}

void MultipleInputTemplateWidget::setButtonVisibility(bool value)
{
    this->ui->button->setVisible(value);
}

bool MultipleInputTemplateWidget::isButtonVisible()
{
    return this->ui->button->isVisible();
}
