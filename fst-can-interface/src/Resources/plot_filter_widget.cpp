#include "plot_filter_widget.hpp"
#include "ui_plot_filter_widget.h"

plot_filter_widget::plot_filter_widget(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::plot_filter_widget)
{
    ui->setupUi(this);
    ui->show_plot_check_box->setCheckState(Qt::Checked);
    this->active = true;
}

plot_filter_widget::~plot_filter_widget()
{
    delete ui;
}

void plot_filter_widget::on_delete_but_clicked()
{
    return;
}
