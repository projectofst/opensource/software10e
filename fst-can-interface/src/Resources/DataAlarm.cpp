#include "DataAlarm.h"

#include <QDebug>
DataAlarm::DataAlarm()
{
    this->_frequency = 0;
    this->_data = std::make_shared<QByteArray>();
    this->_timer = std::make_shared<QTimer>();
    this->_durationTimer = std::make_shared<QTimer>();
    this->_duration = 0;
}

DataAlarm::DataAlarm(QObject* parent, QByteArray& data, int frequency)
    : QObject(parent)
{
    this->_data = std::make_shared<QByteArray>(data);
    this->_frequency = frequency;
    this->_timer = std::make_shared<QTimer>();
    this->_timer->setInterval(this->_frequency);
    this->_durationTimer = std::make_shared<QTimer>();
    this->_duration = 0;
    connect(this->_timer.get(), &QTimer::timeout, this, &DataAlarm::handleTimeout);
    this->_timer->start();
}

DataAlarm::DataAlarm(QObject* parent, QByteArray& data, int frequency, int duration)
    : QObject(parent)
{
    this->_data = std::make_shared<QByteArray>(data);
    this->_frequency = frequency;
    this->_timer = std::make_shared<QTimer>();
    this->_timer->setInterval(this->_frequency);
    this->_durationTimer = std::make_shared<QTimer>();
    this->_durationTimer->setInterval(duration);
    connect(this->_durationTimer.get(), &QTimer::timeout, this, &DataAlarm::handleDurationTimeout);
    connect(this->_timer.get(), &QTimer::timeout, this, &DataAlarm::handleTimeout);
    this->_duration = duration;

    this->_timer->start();
    this->_durationTimer->start();
}

void DataAlarm::stop()
{
    this->_timer->stop();
    this->_durationTimer->stop();
}

void DataAlarm::start()
{
    this->_timer->start();
    if (_duration != 0)
        this->_durationTimer->start();
}

void DataAlarm::setFrequency(int frequency)
{
    this->_frequency = frequency;
}

void DataAlarm::handleTimeout()
{
    emit dataReady(this->_data);
}

void DataAlarm::handleDurationTimeout()
{
    this->_timer->stop();
    this->_durationTimer->stop();
    emit durationTimeout();
}
