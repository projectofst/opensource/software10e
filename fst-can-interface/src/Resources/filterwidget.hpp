#ifndef FILTERWIDGET_H
#define FILTERWIDGET_H

#include "device.hpp"
#include "message.hpp"
#include <QWidget>

namespace Ui {
class FilterWidget;
}

class FilterWidget : public QWidget {
    Q_OBJECT

private:
    Ui::FilterWidget* ui;
    bool _active;
    int _devId;
    int _messageId;
    bool _isReadOnly = false;

public:
    FilterWidget();
    explicit FilterWidget(int devId, int messageId, bool actv);
    FilterWidget(Device* dev, Message* msg);
    ~FilterWidget();
    int getDevId();
    int getMessageId();
    bool isActive();

private slots:
    void on_delete_2_clicked();
    void on_enabled_stateChanged(int);
    void on_dev_id_textChanged(const QString& arg1);
    void on_msg_id_textChanged(const QString& arg1);

private:
signals:
    void destroyed();
};

#endif // FILTERWIDGET_H
