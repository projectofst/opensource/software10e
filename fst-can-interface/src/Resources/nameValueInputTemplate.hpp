#ifndef NAMEVALUEINPUTTEMPLATE_HPP
#define NAMEVALUEINPUTTEMPLATE_HPP

#include <QDebug>
#include <QWidget>

namespace Ui {
class nameValueInputTemplate;
}

class nameValueInputTemplate : public QWidget {
    Q_OBJECT

public:
    explicit nameValueInputTemplate(QWidget* parent = nullptr, QString name = "DEFAULT NAME");
    ~nameValueInputTemplate();

    uint64_t getValue()
    {
        qDebug() << "getValue(): " << this->_value;
        return this->_value;
    };
    QString getName() { return this->_name; };
    void setValue(uint64_t value);

private slots:
    void on_valueInpit_textChanged(const QString& arg1);

private:
    Ui::nameValueInputTemplate* ui;
    QString _name;
    int64_t _value;
};

#endif // NAMEVALUEINPUTTEMPLATE_HPP
