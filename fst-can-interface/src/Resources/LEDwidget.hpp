#ifndef LEDWIDGET_HPP
#define LEDWIDGET_HPP

#include <QColor>
#include <QPainter>
#include <QWidget>
#include <qdebug.h>

class LEDwidget : public QWidget {
    Q_OBJECT

public:
    LEDwidget(QWidget* parent = 0);
    ~LEDwidget(void);

    class Builder {
    public:
        LEDwidget* led_object = nullptr;

        Builder(QWidget* parent)
        {
            led_object = new LEDwidget(parent);
        }

        Builder& withCustomExpression(std::function<bool(double)> expression)
        {
            led_object->setLambda(expression);
            return *this;
        }

        LEDwidget* Build() { return led_object; }
    };

    void setColor(QColor new_color);

    QColor getColor() { return color; }

    void turnGreen() { setColor(Qt::green); }
    void turnRed() { setColor(Qt::red); }
    void turnBlue() { setColor(Qt::blue); }
    void turnYellow() { setColor(Qt::yellow); }
    void turnCyan() { setColor(Qt::cyan); }
    void turnOff() { setColor(Qt::lightGray); }
    void turnGreenIf(bool);
    void updateValue(double value);
    void setLambda(std::function<bool(double)> new_lambda);

private:
    QColor color;
    QPainter painter;
    std::function<bool(double)> lambda = [](double a) { return (a >= 1); };
    void paintEvent(QPaintEvent*);
};
#endif // LEDWIDGET_HPP
