import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.2
import QtQuick.Controls.Imagine 2.3
import QtQuick.Controls.Material 2.0
import QtQuick.Controls.Universal 2.0

Rectangle {
    id: shutdown
    width: 720
    height: 480
    color: "#383838"
    antialiasing: true

    ScrollView {
        id: scrollView
        anchors.fill: parent

        MouseArea {
            id: zoom
            x: 0
            y: 0
            width: 56
            height: 30
            hoverEnabled: true
            onEntered: {zoomin.enabled = true;
                zoomin.visible = true;
                zoomout.enabled = true;
                zoomout.visible = true}
            onExited:  {zoomin.enabled = false;
                zoomin.visible = false;
                zoomout.enabled = false;
                zoomout.visible = false}

            RoundButton {
                id: zoomin
                x: 0
                y: 0
                width: 27
                height: 25
                text: "+"
                visible: false
                enabled: false
                font.pointSize: 24
                display: AbstractButton.TextOnly
            }

            RoundButton {
                id: zoomout
                x: 27
                y: 0
                width: 27
                height: 25
                text: "-"
                visible: false
                enabled: false
                font.pointSize: 24
                display: AbstractButton.TextOnly
            }

        }
    }
}



































/*##^## Designer {
    D{i:0;width:640}
}
 ##^##*/
