#include "PlotObjTypes.hpp"

//
//  PLOT CHILD OBJECT
//

PlotChildObj::PlotChildObj(
    std::shared_ptr<PlotCfg> plotSetupCfg,
    QChart* chartViewSetup,
    QValueAxis* axisX,
    QValueAxis* axisY,
    FCPCom* fcp,
    QString custom_expression)
    : PlotBaseObj(plotSetupCfg, chartViewSetup, axisX, axisY, fcp)
{
    symbol_table_t symbol_table;
    parser_t parser;
    // Compile Expression
    customExpression = custom_expression.toUtf8().constData();
    symbol_table.add_variable("x", currentPoint);
    compiledExpression.register_symbol_table(symbol_table);
    parser.compile(customExpression, compiledExpression);
}

void PlotChildObj::receiveParentPoint(QPointF point)
{
    // qDebug() << "CHILD: I received: " << point.x() << " / " << point.y();
    if (!this->isDead()) {
        calcSimplePoint(point);
        updateRanges(point.x(), point.y());
        addToBuffer(point);
        sendPointToChildren(point);
    }
}

void PlotChildObj::calcSimplePoint(QPointF& point)
{
    currentPoint = point.y();
    point.setY(compiledExpression.value());
}

YAML::Node PlotChildObj::exportToYAML()
{
    YAML::Node plotCfg;
    YAML::Node filter;
    YAML::Node color;
    // YAML::Emitter parents;
    color["R"] = this->getPlotCfg()->getPlotColor().red();
    color["G"] = this->getPlotCfg()->getPlotColor().green();
    color["B"] = this->getPlotCfg()->getPlotColor().blue();
    color["A"] = this->getPlotCfg()->getPlotColor().alpha();
    filter["length"] = this->getPlotCfg()->getLength();
    filter["start"] = this->getPlotCfg()->getStart();
    filter["scale"] = this->getPlotCfg()->getScale();
    filter["offset"] = this->getPlotCfg()->getOffset();
    plotCfg["wasId"] = this->getPlotCfg()->getPlotId();
    plotCfg["devId"] = this->getPlotCfg()->getDevId();
    plotCfg["messageId"] = this->getPlotCfg()->getMessageId();
    plotCfg["filter"] = filter;
    plotCfg["color"] = color;
    plotCfg["parentsId"] = this->getPlotCfg()->getParentsId();
    plotCfg["op"] = customExpression;
    return plotCfg;
}

//
//  PLOT PARENT OBJECT
//

PlotParentObj::PlotParentObj(
    std::shared_ptr<PlotCfg> plotSetupCfg,
    QChart* chartViewSetup,
    QValueAxis* axisX,
    QValueAxis* axisY,
    FCPCom* fcp)
    : PlotBaseObj(plotSetupCfg, chartViewSetup, axisX, axisY, fcp) {};
