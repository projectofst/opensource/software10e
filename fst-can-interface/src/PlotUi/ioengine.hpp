#ifndef IOENGINE_HPP
#define IOENGINE_HPP
#include <PlotUi/PlotLib.hpp>
#include <PlotUi/PlotObjTypes.hpp>
#include <fstream>

class ioEngine {
public:
    ioEngine(FCPCom* fcp);
    void setupProgressBar(QString format, int maxvalue);
    void setExport(QString savePath, QString fileName, QProgressBar* progress);
    void exportPlotList(QList<std::shared_ptr<PlotBaseObj>> plotObjects);
    void exportPoints(QList<std::shared_ptr<PlotBaseObj>> plotObjects);
    void exportScreenshot(QChartView* chart);
    void setImport(QString path, int crtId, QChart* chart, QValueAxis* X, QValueAxis* Y);
    QList<std::shared_ptr<PlotBaseObj>> importExec();
    std::shared_ptr<PlotBaseObj> getImportedPlotById(QList<std::shared_ptr<PlotBaseObj>> imported_plotObjects, int id);
    void connectChildren(QList<std::shared_ptr<PlotBaseObj>> plots);
    void rebuildIdStructure(QList<std::shared_ptr<PlotBaseObj>> plots);

private:
    int currentId;
    int ioType; // 2 = Not Defined  1 = Import  0 = Export , Just a failsafe
    FCPCom* _fcp;
    QChart* tempChart;
    QValueAxis *tempAxisX, *tempAxisY;
    QString importPath;
    QString exportPath;
    QProgressBar* pgb;
};

#endif // IOENGINE_HPP
