#ifndef PLOTCHILDOBJ_H
#define PLOTCHILDOBJ_H
#include "PlotLib.hpp"
#ifndef Q_OS_WIN
#include <exptrk.hpp>
#endif

#ifndef Q_OS_WIN
typedef exprtk::symbol_table<double> symbol_table_t;
typedef exprtk::expression<double> expression_t;
typedef exprtk::parser<double> parser_t;
#endif

class PlotChildObj : public PlotBaseObj {
public:
    PlotChildObj(
        std::shared_ptr<PlotCfg> plotSetupCfg,
        QChart* chartViewSetup,
        QValueAxis* axisX,
        QValueAxis* axisY,
        FCPCom* fcp,
        QString custom_expression);
    virtual void receiveParentPoint(QPointF point) override;
    virtual void calcSimplePoint(QPointF& point) override;
    virtual void receiveCANMsg(CANmessage) override {};
    virtual YAML::Node exportToYAML() override;

private:
    double currentPoint;
    std::string customExpression;
    expression_t compiledExpression;
};

class PlotParentObj : public PlotBaseObj {
public:
    PlotParentObj(
        std::shared_ptr<PlotCfg> plotSetupCfg,
        QChart* chartViewSetup,
        QValueAxis* axisX,
        QValueAxis* axisY,
        FCPCom* fcp);
};

#endif // PLOTCHILDOBJ_H
