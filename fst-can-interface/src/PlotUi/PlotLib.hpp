#ifndef PLOTOBJ_HPP
#define PLOTOBJ_HPP
#include "can-ids/CAN_IDs.h"
#include "fcpcom.hpp"
#include "qtyaml.h"
#include <QList>
#include <QPlainTextEdit>
#include <QValueAxis>
#include <QtCharts>
#include <memory>
#include <stdint.h>

// Plot Configuration Object
class PlotCfg {

public:
    PlotCfg(uint16_t dev_id, uint16_t msg_id, double ofst, int len, int strt, double scl, QString name, int plot_id, QColor color, QList<int> parents_id, QString mux_name, int mux_value);
    uint16_t getDevId();
    uint16_t getMessageId();
    QString getName();
    void updateName(QString name);
    int getPlotId();
    void updatePlotId(int value);
    QColor getPlotColor();
    void updatePlotColor(QColor newColor);
    int getLength();
    double getOffset();
    double getScale();
    int getStart();
    QList<int> getParentsId();
    QString getPlotMux();
    int getPlotMuxValue();
    YAML::Node exportToYAML();

private:
    QString plotName;
    QColor plotColor;
    QString plotMux;
    int plotMuxValue;
    uint16_t devId;
    uint16_t messageId;
    int length;
    int start;
    double scale;
    double offset;
    int plotId;
    QList<int> parentsId;
};

// Plot Object
class PlotBaseObj {
public:
    PlotBaseObj(std::shared_ptr<PlotCfg> plotSetupCfg, QChart* chartView, QValueAxis* axisX, QValueAxis* axisY, FCPCom* fcp);
    std::shared_ptr<PlotCfg> getPlotCfg();
    QString helloMsg();
    virtual void receiveCANMsg(CANmessage msg);
    bool isSelectedMux(CANmessage msg);
    bool isThisForMe(CANmessage msg);
    u_int32_t getMaxY();
    double getMinY();
    u_int32_t getMaxX();
    QLineSeries* getPlotData();
    QList<double> getRanges();
    void updateRanges(uint32_t timestamp, double value);
    double decodeValue(CANmessage msg);
    void addToBuffer(QPointF point);
    void Render();
    QList<QPointF> getPoints();
    void updateColor(QColor newColor);
    virtual YAML::Node exportToYAML();
    // PARENT and CHILD
    void sendPointToChildren(QPointF point);
    void renderChildrenPoints();
    void addChild(std::shared_ptr<PlotBaseObj> child);
    void removeChild(int childId);
    void kill();
    QList<std::shared_ptr<PlotBaseObj>> getChildren();
    QMap<QString, double> getSubRanges();
    bool isDead();
    // CHILD
    virtual void receiveParentPoint(QPointF) {};
    virtual void calcSimplePoint(QPointF&) {};
    // Destructor
    ~PlotBaseObj();

private:
    FCPCom* _fcp;
    std::shared_ptr<PlotCfg> plotConfiguration;
    QList<std::shared_ptr<PlotBaseObj>> plotChildren;
    CanSignal* muxSignal;
    QChart* chartView;
    QList<QPointF> dataBuffer;
    QLineSeries* plotData;
    QChart* plotWidget;
    uint32_t maxX;
    double minY;
    uint32_t avgY;
    uint32_t maxY;
    bool minYDefined;
    double totalY;
    double samplesNumber;
    bool dead;
};

// Plot List Widget
class PlotWidget : public QWidget {
    Q_OBJECT
public:
    PlotWidget(std::shared_ptr<PlotBaseObj> plot);
    QLineEdit* CreateValuesLayout(QString title, QHBoxLayout* layout);
    int getPlotId();
    void refresh(QList<double> values);
    void changePlotVisibility(int state);
    ~PlotWidget()
    {
        qDebug() << "Removing from UI...";
        qDebug() << "Used by " << plotObj.use_count() << " instances";
    };

private:
    std::shared_ptr<PlotBaseObj> plotObj;
    QLineEdit* minValue;
    QLineEdit* avgValue;
    QLineEdit* maxValue;
};
#endif // PLOTOBJ_HPP
