#include "ioengine.hpp"

ioEngine::ioEngine(FCPCom* fcp)
{
    ioType = 2;
    _fcp = fcp;
}

void ioEngine::setupProgressBar(QString format, int maxvalue)
{
    pgb->setMaximum(maxvalue);
    pgb->setValue(0);
    pgb->setFormat(format);
    pgb->show();
}

/*
 *
 *   Export Module
 *
 */

void ioEngine::setExport(QString savePath, QString fileName, QProgressBar* progress)
{
    exportPath = savePath + "/" + fileName;
    pgb = progress;
    ioType = 0;
}

void ioEngine::exportPlotList(QList<std::shared_ptr<PlotBaseObj>> plotObjects)
{
    if (ioType == 0) {
        QFile file(exportPath + ".yaml");
        YAML::Emitter fileOut;
        YAML::Node yamlOut;
        YAML::Node plotList;
        setupProgressBar("Exporting %p%", plotObjects.length());
        foreach (auto plot, plotObjects) {
            plotList[plot->getPlotCfg()->getName().toStdString()] = plot->exportToYAML();
            pgb->setValue(pgb->value() + 1);
        }
        yamlOut["plots"] = plotList;
        fileOut << yamlOut;
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;
        QTextStream out(&file);
        out << fileOut.c_str();
        pgb->hide();
    }
}

void ioEngine::exportPoints(QList<std::shared_ptr<PlotBaseObj>> plotObjects)
{
    if (ioType == 0) {
        QFile file(exportPath + ".csv");
        if (file.open(QFile::WriteOnly | QFile::Truncate)) {
            QTextStream stream(&file);
            setupProgressBar("Exporting %p%", plotObjects.length());
            foreach (auto plot, plotObjects) {
                stream << plot->getPlotCfg()->getName() << "\n";
                foreach (QPointF point, plot->getPoints()) {
                    stream << point.x() << "\t" << point.y() << "\n";
                }
                pgb->setValue(pgb->value() + 1);
            }
            file.close();
        }
        pgb->hide();
    }
}

void ioEngine::exportScreenshot(QChartView* chart)
{
    if (ioType == 0) {
        QPixmap image = chart->grab();
        setupProgressBar("Exporting PNG", 0);
        image.save(exportPath + ".png", "PNG");
        pgb->hide();
    }
}

/*
 *
 *   Import Module
 *
 */

void ioEngine::setImport(QString path, int crtId, QChart* chart, QValueAxis* X, QValueAxis* Y)
{
    importPath = path;
    currentId = crtId;
    tempChart = chart;
    tempAxisX = X;
    tempAxisY = Y;
    ioType = 1;
}

std::shared_ptr<PlotBaseObj> ioEngine::getImportedPlotById(QList<std::shared_ptr<PlotBaseObj>> imported_plotObjects, int id)
{
    std::shared_ptr<PlotBaseObj> plot = nullptr;
    foreach (auto plotObj, imported_plotObjects) {
        if (plotObj->getPlotCfg()->getPlotId() == id) {
            plot = plotObj;
        }
    }
    return plot;
}

void ioEngine::connectChildren(QList<std::shared_ptr<PlotBaseObj>> plots)
{
    foreach (auto plot, plots) {
        if (!plot->getPlotCfg()->getParentsId().empty()) { // It's a child
            foreach (auto id, plot->getPlotCfg()->getParentsId()) { // Retrieve ID's
                getImportedPlotById(plots, id)->addChild(plot);
            }
        }
    }
}

void ioEngine::rebuildIdStructure(QList<std::shared_ptr<PlotBaseObj>> plots)
{
    foreach (auto plot, plots) {
        plot->getPlotCfg()->updatePlotId(currentId);
        currentId++;
    }
}

QList<std::shared_ptr<PlotBaseObj>> ioEngine::importExec()
{
    QList<std::shared_ptr<PlotBaseObj>> importedPlotObjects;
    if (ioType == 1) {
        uint16_t devId, messageId, wasId;
        int length, start, R, G, B, A, muxValue;
        double scale, offset;
        QColor plotColor;
        QString muxName;
        QList<int> parentsId;
        YAML::Node baseNode = YAML::LoadFile(importPath.toUtf8().constData())["plots"];
        for (YAML::const_iterator it = baseNode.begin(); it != baseNode.end(); ++it) {
            QString plotName = QString::fromUtf8(it->first.as<std::string>().c_str()); // <- key
            YAML::Node plotNode = baseNode[plotName.toUtf8().constData()];
            wasId = plotNode["wasId"].as<uint16_t>();
            devId = plotNode["devId"].as<uint16_t>();
            messageId = plotNode["messageId"].as<uint16_t>();
            length = plotNode["filter"]["length"].as<int>();
            start = plotNode["filter"]["start"].as<int>();
            scale = plotNode["filter"]["scale"].as<double>();
            offset = plotNode["filter"]["offset"].as<double>();
            muxName = QString::fromUtf8(plotNode["mux"]["signal_name"].as<std::string>().c_str());
            muxValue = plotNode["mux"]["mux_select"].as<int>();
            R = plotNode["color"]["R"].as<int>();
            G = plotNode["color"]["G"].as<int>();
            B = plotNode["color"]["B"].as<int>();
            A = plotNode["color"]["A"].as<int>();
            plotColor = QColor(R, G, B, A);
            parentsId = plotNode["parentsId"].as<QList<int>>();
            /* Create Old Structure */
            std::shared_ptr<PlotCfg> plotTempCfg = std::make_shared<PlotCfg>(devId, messageId, offset, length, start, scale, plotName, wasId, plotColor, parentsId, muxName, muxValue);
            if (parentsId.empty()) { // Is a parent
                importedPlotObjects.append(std::make_shared<PlotParentObj>(plotTempCfg, tempChart, tempAxisX, tempAxisY, _fcp));
            } else {
                importedPlotObjects.append(std::make_shared<PlotChildObj>(plotTempCfg, tempChart, tempAxisX, tempAxisY, _fcp, plotNode["op"].as<QString>()));
            }
        }
        connectChildren(importedPlotObjects);
        rebuildIdStructure(importedPlotObjects);
    }
    return importedPlotObjects;
}
