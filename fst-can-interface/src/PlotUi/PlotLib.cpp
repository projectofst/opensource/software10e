#include "PlotLib.hpp"

//
//  PLOT CFG
//

PlotCfg::PlotCfg(uint16_t dev_id, uint16_t msg_id, double ofst, int len, int strt, double scl, QString name, int plot_id, QColor color, QList<int> parents_id, QString mux_name, int mux_value)
{
    devId = dev_id;
    messageId = msg_id;
    offset = ofst;
    length = len;
    start = strt;
    scale = scl;
    plotName = name;
    plotId = plot_id;
    plotColor = color;
    parentsId = parents_id;
    plotMux = mux_name;
    plotMuxValue = mux_value;
}

uint16_t PlotCfg::getDevId()
{
    return this->devId;
}

uint16_t PlotCfg::getMessageId()
{
    return this->messageId;
}

QString PlotCfg::getName()
{
    return this->plotName;
}

void PlotCfg::updateName(QString name)
{
    plotName = name;
}

QColor PlotCfg::getPlotColor()
{
    return this->plotColor;
}

void PlotCfg::updatePlotColor(QColor newColor)
{
    plotColor = newColor;
}

int PlotCfg::getPlotId()
{
    return this->plotId;
}

void PlotCfg::updatePlotId(int value)
{
    plotId = value;
}

int PlotCfg::getLength()
{
    return this->length;
}

double PlotCfg::getOffset()
{
    return this->offset;
}

int PlotCfg::getStart()
{
    return this->start;
}

double PlotCfg::getScale()
{
    return this->scale;
}

QList<int> PlotCfg::getParentsId()
{
    return this->parentsId;
}

QString PlotCfg::getPlotMux()
{
    return this->plotMux;
}

int PlotCfg::getPlotMuxValue()
{
    return this->plotMuxValue;
}

YAML::Node PlotCfg::exportToYAML()
{
    YAML::Node plotCfg;
    YAML::Node filter;
    YAML::Node color;
    YAML::Node mux;
    // YAML::Emitter parents;
    color["R"] = plotColor.red();
    color["G"] = plotColor.green();
    color["B"] = plotColor.blue();
    color["A"] = plotColor.alpha();
    filter["length"] = length;
    filter["start"] = start;
    filter["scale"] = scale;
    filter["offset"] = offset;
    mux["signal_name"] = plotMux;
    mux["mux_select"] = plotMuxValue;
    plotCfg["wasId"] = plotId;
    plotCfg["devId"] = devId;
    plotCfg["messageId"] = messageId;
    plotCfg["filter"] = filter;
    plotCfg["mux"] = mux;
    plotCfg["color"] = color;
    /*
    parents << YAML::Flow;
    parents << YAML::BeginSeq;
    foreach(auto id, parentsId) {
        parents << id;
    }
    parents << YAML::EndSeq;
    */
    plotCfg["parentsId"] = parentsId;
    return plotCfg;
}

//
//  PLOT OBJ
//

PlotBaseObj::PlotBaseObj(std::shared_ptr<PlotCfg> plotSetupCfg, QChart* chartViewSetup, QValueAxis* axisX, QValueAxis* axisY, FCPCom* fcp)
{
    QPen pen;
    plotConfiguration = plotSetupCfg;
    _fcp = fcp;
    if (plotConfiguration->getPlotMux() != "null")
        muxSignal = _fcp->getSignal(plotConfiguration->getPlotMux());
    chartView = chartViewSetup;
    plotData = new QLineSeries();
    pen = plotData->pen();
    pen.setWidth(2);
    plotData->setPen(pen);
    plotData->setName(plotConfiguration->getName());
    plotData->setColor(plotConfiguration->getPlotColor());
    plotWidget = chartViewSetup;
    plotWidget->addSeries(plotData);
    plotWidget->setAxisX(axisX, plotData);
    plotWidget->setAxisY(axisY, plotData);
    totalY = 0;
    samplesNumber = 0;
    minYDefined = false;
    dead = false;
    maxX = 0;
    maxY = 0;
    minY = 0;
}

void PlotBaseObj::updateColor(QColor newColor)
{
    plotData->setColor(newColor);
    plotConfiguration->updatePlotColor(newColor);
}

std::shared_ptr<PlotCfg> PlotBaseObj::getPlotCfg()
{
    return this->plotConfiguration;
}

QList<QPointF> PlotBaseObj::getPoints()
{
    return this->plotData->points();
}

QLineSeries* PlotBaseObj::getPlotData()
{
    return this->plotData;
}

u_int32_t PlotBaseObj::getMaxX()
{
    return this->maxX;
}

u_int32_t PlotBaseObj::getMaxY()
{
    return this->maxY;
}

double PlotBaseObj::getMinY()
{
    return this->minY;
}

QString PlotBaseObj::helloMsg()
{
    QString himsg;
    himsg = QString("%1: Ready to receive data at DEV ID: %2 / MSG ID: %3 \n").arg(plotConfiguration->getName()).arg(QString::number(plotConfiguration->getDevId())).arg(QString::number(plotConfiguration->getMessageId()));
    return himsg;
}

double PlotBaseObj::decodeValue(CANmessage msg)
{
    fcp_signal_t signal = {
        .start = (uint16_t)getPlotCfg()->getStart(),
        .length = (uint16_t)getPlotCfg()->getLength(),
        .scale = getPlotCfg()->getScale(),
        .offset = getPlotCfg()->getOffset(),
        .type = DOUBLE,
        .endianess = LITTLE
    };
    return fcp_decode_signal_double(msg.candata, signal);
}

// Recieve CAN Message and buffer data
void PlotBaseObj::receiveCANMsg(CANmessage msg)
{
    if (isThisForMe(msg) && !isDead()) {
        // uncomment these lines to enable CAN Message debug
        //
        // qDebug() << "Start: " << getPlotCfg()->getStart() << " / Length: " << getPlotCfg()->getLength() << " / Scale: " << getPlotCfg()->getScale() << " / Offset: " << getPlotCfg()->getOffset();
        double value = decodeValue(msg);
        // qDebug() << value;
        updateRanges(msg.timestamp, value);
        // qDebug() << "PARENT: I received: " << msg.timestamp << " / " << value;
        sendPointToChildren(QPointF(msg.timestamp, value));
        addToBuffer(QPointF(msg.timestamp, value));
    }
}

QList<double> PlotBaseObj::getRanges()
{
    QList<double> ranges;
    ranges << minY;
    ranges << avgY;
    ranges << maxY;
    return ranges;
}

// Kill this plot and all its children
void PlotBaseObj::kill()
{
    delete plotData;
    this->dead = true;
    this->getPlotCfg()->updateName("deleted");
    foreach (auto child, plotChildren) {
        child->kill();
    }
}

// Check if this child is alive
bool PlotBaseObj::isDead()
{
    return this->dead;
}

// Calculate plot ranges
void PlotBaseObj::updateRanges(uint32_t timestamp, double value)
{
    if (timestamp > maxX) {
        maxX = timestamp;
    }
    if (value > maxY) {
        maxY = value;
        if (!minYDefined) {
            minY = maxY;
            minYDefined = true;
        }
    }
    if (value < minY) {
        minY = value;
    }
    samplesNumber++;
    totalY += value;
    avgY = totalY / samplesNumber;
}

void PlotBaseObj::addToBuffer(QPointF point)
{
    dataBuffer.append(point);
}

// Render all the data in the buffer
void PlotBaseObj::Render()
{
    renderChildrenPoints();
    if (!dataBuffer.isEmpty()) {
        plotData->append(dataBuffer);
        dataBuffer.clear();
    }
}

bool PlotBaseObj::isSelectedMux(CANmessage msg)
{
    if (plotConfiguration->getPlotMux() != "null") {
        if (_fcp->decodeDoubleFromSignal(msg, plotConfiguration->getPlotMux()) == plotConfiguration->getPlotMuxValue()) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

bool PlotBaseObj::isThisForMe(CANmessage msg)
{
    if (msg.candata.dev_id == plotConfiguration->getDevId() && msg.candata.msg_id == plotConfiguration->getMessageId() && isSelectedMux(msg)) {
        return true;
    }
    return false;
}

YAML::Node PlotBaseObj::exportToYAML()
{
    return plotConfiguration->exportToYAML();
}

// PARENT and CHILD

void PlotBaseObj::sendPointToChildren(QPointF point)
{
    if (plotChildren.count() > 0) {
        foreach (auto child, plotChildren) {
            child->receiveParentPoint(point);
        }
    }
}

void PlotBaseObj::renderChildrenPoints()
{
    if (plotChildren.count() > 0) {
        foreach (auto child, plotChildren) {
            child->Render();
        }
    }
}

void PlotBaseObj::removeChild(int childId)
{
    for (int i = 0; i < plotChildren.count(); i++) {
        if (plotChildren[i]->getPlotCfg()->getPlotId() == childId) {
            qDebug() << "Removig from Parent...";
            plotChildren.removeAt(i);
        }
    }
}

void PlotBaseObj::addChild(std::shared_ptr<PlotBaseObj> child)
{
    plotChildren.append(child);
}

QList<std::shared_ptr<PlotBaseObj>> PlotBaseObj::getChildren()
{
    return this->plotChildren;
}

QMap<QString, double> PlotBaseObj::getSubRanges()
{
    QMap<QString, double> ranges;
    double currentMaxY = getMaxY();
    double currentMinY = getMinY();
    double childMaxY, childMinY;
    if (plotChildren.count() > 0) {
        foreach (auto child, plotChildren) {
            childMaxY = child->getSubRanges()["max"];
            childMinY = child->getSubRanges()["min"];
            if (childMaxY > currentMaxY) {
                currentMaxY = childMaxY;
            }
            if (childMinY < currentMinY) {
                currentMinY = childMinY;
            }
        }
    }
    ranges["max"] = currentMaxY;
    ranges["min"] = currentMinY;
    return ranges;
}

PlotBaseObj::~PlotBaseObj()
{
    qDebug() << "BYE BYE: " << getPlotCfg()->getName();
    plotChildren.clear();
}

//
//  PLOT WIDGET
//

PlotWidget::PlotWidget(std::shared_ptr<PlotBaseObj> plot)
{
    QFont font;
    QVBoxLayout* verticalLayout = new QVBoxLayout(this);
    QHBoxLayout* horizontalLayout = new QHBoxLayout();
    QLabel* deviceName = new QLabel(plot->getPlotCfg()->getName().replace(" ", "\n"));
    QCheckBox* visibilityCheckbox = new QCheckBox("Enable", this);
    visibilityCheckbox->setCheckState(Qt::CheckState(2));
    QList<double> default_values = { 0, 0, 0 };
    font = deviceName->font();
    font.setPointSize(12);
    deviceName->setFont(font);
    QFrame* line = new QFrame;
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    verticalLayout->addWidget(deviceName);
    verticalLayout->addWidget(visibilityCheckbox);
    connect(visibilityCheckbox, &QCheckBox::stateChanged, this, &PlotWidget::changePlotVisibility);
    minValue = CreateValuesLayout("Min:", horizontalLayout);
    avgValue = CreateValuesLayout("Avg:", horizontalLayout);
    maxValue = CreateValuesLayout("Max:", horizontalLayout);
    verticalLayout->addLayout(horizontalLayout);
    verticalLayout->addWidget(line);
    verticalLayout->addStretch();
    plotObj = plot;
    refresh(default_values);
}

QLineEdit* PlotWidget::CreateValuesLayout(QString title, QHBoxLayout* layout)
{
    QVBoxLayout* verticalLayout = new QVBoxLayout();
    QLineEdit* valueWidget = new QLineEdit(this);
    QLabel* titleWidget = new QLabel(title);
    verticalLayout->addWidget(titleWidget);
    verticalLayout->addWidget(valueWidget);
    layout->addLayout(verticalLayout);
    return valueWidget;
}

int PlotWidget::getPlotId()
{
    return this->plotObj->getPlotCfg()->getPlotId();
}

void PlotWidget::changePlotVisibility(int state)
{
    if (state == 0) {
        plotObj->getPlotData()->hide();
    } else {
        plotObj->getPlotData()->show();
    }
}

void PlotWidget::refresh(QList<double> values)
{
    minValue->setText(QString::number(values[0]));
    if (values[0] == 0 && values[2] == 0) {
        values[1] = 0;
    }
    avgValue->setText(QString::number(values[1]));
    maxValue->setText(QString::number(values[2]));
}
