#ifndef VIOCONTROL_H
#define VIOCONTROL_H

#include <QWidget>

namespace Ui {
class VioControl;
}

class VioControl : public QWidget {
    Q_OBJECT

public:
    explicit VioControl(QWidget* parent = nullptr);
    ~VioControl();

private:
    Ui::VioControl* ui;
};

#endif // VIOCONTROL_H
