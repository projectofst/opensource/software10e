#ifndef DEVSTATUS_HPP
#define DEVSTATUS_HPP

#include <QCloseEvent>
#include <QCommandLinkButton>
#include <QDebug>
#include <QDockWidget>
#include <QMap>
#include <QMessageBox>
#include <QQuickWidget>
#include <QSettings>
#include <QString>
#include <QTextEdit>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>

#include "COM.hpp"
#include "Exceptions/helper.hpp"
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"
#include "fcpcom.hpp"
#include "table.h"
#include "ui_DevStatus.h"

namespace Ui {
class DevStatus;
}

class DevStatus : public UiWindow {
    Q_OBJECT

public:
    explicit DevStatus(QWidget* parent = nullptr, /*QDockWidget *dockwidget = nullptr,*/ FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    ~DevStatus() override;
    void resizeEvent(QResizeEvent*);
public slots:
    void process_CAN_message(CANmessage) override;
    void resize(int);
    void updateFCP(FCPCom* fcp) override;

private:
    Ui::DevStatus* ui;
    QList<int> DevOrder;
    QMap<QString, QCommandLinkButton*> DevButtons;
    QMap<QString, QTimer*> DevTimers;
    QMap<QString, int> DevStates;
    int Id;

    QLinearGradient red;
    QLinearGradient green;
    QLinearGradient yellow;

    void createDevs();
    void layoutDevs(int);
    int currentK;
    int lastK;
    FCPCom* fcp;

private slots:
    void silentWarning(void);
    void closeWindow(void);
};

#endif // DEVSTATUS_HPP
