#include "pcb_version.hpp"
#include "ui_pcb_version.h"
#include <QDebug>
#include <QPalette>

pcb_version::pcb_version(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::pcb_version)
{
    ui->setupUi(this);
    name = "UNDEFINED";

    QPalette palette;
    palette.setColor(ui->name->foregroundRole(), Qt::red);
    ui->name->setPalette(palette);
    ui->current_version->setText("NONE");
}

pcb_version::~pcb_version()
{
    delete ui;
}

QString pcb_version::get_name()
{
    return name;
}

void pcb_version::set_current_version(QString version)
{
    QPalette palette;
    palette.setColor(ui->name->foregroundRole(), Qt::green);

    ui->current_version->setText(version);

    ui->name->setPalette(palette);
    return;
}

void pcb_version::set_name(QString new_name)
{
    name = new_name;
    ui->name->setText(new_name);
    return;
}

void pcb_version::destroy()
{
    delete this;
}
