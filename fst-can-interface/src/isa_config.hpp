#ifndef ISA_CONFIG_HPP
#define ISA_CONFIG_HPP

#include "COM_SerialPort.hpp"
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"
#include <QDialog>

namespace Ui {
class isa_config;
}

class isa_config : public QDialog {
    Q_OBJECT

public:
    explicit isa_config(QWidget* parent = nullptr);
    ~isa_config();
    Ui::isa_config* ui;
signals:
    void send_to_CAN(QByteArray& msg);
private slots:
    void on_close_clicked();
    void send_to_isa(int message_sid, int DB0, int DB1, int DB2, int DB3, int DB4, int DB5, int DB6, int DB7);
    void on_current_enable_clicked();
    void on_current_disable_clicked();
    void on_V1_enable_clicked();
    void on_V1_disable_clicked();
    void on_V2_enable_clicked();
    void on_V2_disable_clicked();
    void on_V3_enable_clicked();
    void on_V3_disable_clicked();
    void on_temp_enable_clicked();
    void on_temp_disable_clicked();
    void on_power_enable_clicked();
    void on_power_disable_clicked();
    void on_current_counter_enable_clicked();
    void on_current_counter_disable_clicked();
    void on_energy_counte_enable_clicked();
    void on_energy_counter_disable_clicked();

private:
};

#endif // ISA_CONFIG_HPP
