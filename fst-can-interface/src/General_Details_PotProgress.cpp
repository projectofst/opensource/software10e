﻿#include "General_Details_PotProgress.hpp"
#include "ui_General_Details_PotProgress.h"

#include "can-ids/Devices/TE_CAN.h"

#include <QGroupBox>
#include <QProgressBar>

PotProgress::PotProgress(QWidget* parent, QString title)
    : QWidget(parent)
    , ui(new Ui::PotProgress)
{
    ui->setupUi(this);

    ui->title->setTitle(title);
}

PotProgress::~PotProgress()
{
    delete ui;
}

void PotProgress::update_limits(TE_THRESHOLD_LIMIT limit, int value)
{
    switch (limit) {
    case GND:
        limits[GND] = value;
        ui->gnd->setText(QString::number(value));
        break;
    case VCC:
        limits[VCC] = value;
        ui->vcc->setText(QString::number(value));
        break;
    case ZERO_FORCE:
        limits[ZERO_FORCE] = value;
        ui->zero_force->setText(QString::number(value));
        break;
    case MAX_FORCE:
        limits[MAX_FORCE] = value;
        ui->max_force->setText(QString::number(value));
        break;
    }
}

QString PotProgress::check_te_limit(int value)
{
    if (value < limits[GND]) {
        return QString("QProgressBar::chunk {background-color: red;}");
    } else if (value < limits[ZERO_FORCE]) {
        return QString("QProgressBar::chunk {background-color: yellow;}");
    } else if (value < limits[MAX_FORCE]) {
        return QString("QProgressBar::chunk {background-color: cyan;}");
    } else if (value < limits[VCC]) {
        return QString("QProgressBar::chunk {background-color: yellow;}");
    } else {
        return QString("QProgressBar::chunk {background-color: red;}");
    }
}

void PotProgress::update_value(int reading)
{
    this->reading = reading;
    ui->reading->setText(QString::number(reading));
    ui->bar->setValue(reading);
    ui->bar->setStyleSheet(check_te_limit(reading));
}
