#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include "COM.hpp"
#include "can-ids/CAN_IDs.h"
#include <QObject>

//#include <QSqlDatabase>
//#include <QSqlQuery>

namespace Ui {
class Interface;
}

class Interface : public COM {
    Q_OBJECT

public:
    Interface();
    virtual bool open(QString) override;
    virtual int close(void) override;
    virtual int status(void) override { return StatusInt; }
    virtual void setId(QString address) override;
    int StatusInt;
public slots:
    virtual void read(void) override;
    virtual int send(QByteArray&) override;

signals:
    void broadcast_in_message(CANmessage);
};

#endif // COM_INTERFACE_HPP
