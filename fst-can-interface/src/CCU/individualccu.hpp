#ifndef INDIVIDUALCCU_HPP
#define INDIVIDUALCCU_HPP

// Qt
#include <QDebug>
#include <QWidget>

// files
#include "LEDwidget.hpp"
#include "Resources/ledandtemperaturewidget.hpp"
#include "fcpcom.hpp"

namespace Ui {
class IndividualCCU;
}

class IndividualCCU : public QWidget {
    Q_OBJECT

public:
    explicit IndividualCCU(QWidget* parent = nullptr);
    ~IndividualCCU();

    double getflow();
    int getFrequency();
    int getPeriod();
    int getNtcNumber();
    bool getFlowSensorState();
    bool getFanState();

    void setFrequency(double frequency);
    void setFlow(double flow);
    void setPeriod(double period);

    void addNtcTemperature(ledAndTemperatureWidget* tempWidget);
    void updateNtcTemperature(int ntc, double temperature);
    void updateFansAndPumpsStatus(int status);
    void updateSensorFlowStatus(int status);

    void setFcp(FCPCom* fcp);

    void setSide(QString keyWord);

    QString getSide();

private slots:
    void on_turnOnFlowSensorButton_clicked();

    void on_turnOffFlowSensorButton_clicked();

    void on_ntcSpinBox_valueChanged(int arg1);

    void updateTemperatureScroll(int newNtcNumber);

    void on_TurnOnFans_clicked();

    void on_turnOffFans_clicked();

    void removeNtcTemperature(int index);

    bool eventFilter(QObject* obj, QEvent* event);

private:
    Ui::IndividualCCU* ui;

    QList<ledAndTemperatureWidget*> _temperatures;
    double _flow;
    int _frequency;
    int _period;
    int _ntcNumber;
    bool _flowSensorState;
    bool _fanState;
    FCPCom* _fcp;
    QString _side;

signals:
    void sendCan(CANmessage);
};

#endif // INDIVIDUALCCU_HPP
