#include "individualccu.hpp"
#include "ui_individualccu.h"

IndividualCCU::IndividualCCU(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::IndividualCCU)
{
    ui->setupUi(this);
    this->_temperatures = *new QList<ledAndTemperatureWidget*>();
    this->_ntcNumber = 8;

    for (int i = 0; i < this->_ntcNumber; i++) {
        addNtcTemperature(new ledAndTemperatureWidget(this, "Temperature " + QString::number(i)));
    }

    this->ui->ntcSpinBox->installEventFilter(this);
}

IndividualCCU::~IndividualCCU()
{
    delete ui;
}

double IndividualCCU::getflow()
{
    return this->_flow;
}

int IndividualCCU::getFrequency()
{
    return this->_frequency;
}

int IndividualCCU::getPeriod()
{
    return this->_period;
}

int IndividualCCU::getNtcNumber()
{
    return this->_ntcNumber;
}

bool IndividualCCU::getFlowSensorState()
{
    return this->_flowSensorState;
}

bool IndividualCCU::getFanState()
{
    return this->_fanState;
}

void IndividualCCU::setFrequency(double frequency)
{
    this->_frequency = frequency;
    this->ui->frequencyValue->setText(QString::number(this->_frequency) + " Hz");
    return;
}

void IndividualCCU::setFlow(double flow)
{
    this->_flow = flow;
    this->ui->flowValue->setText(QString::number(this->_flow) + " L/min");
    return;
}

void IndividualCCU::setPeriod(double period)
{
    this->_period = period;
    this->ui->periodValue->setText(QString::number(this->_period) + " ms");
    return;
}

void IndividualCCU::addNtcTemperature(ledAndTemperatureWidget* tempWidget)
{
    this->ui->temperatures->addWidget(tempWidget);
    this->_temperatures.append(tempWidget);
    return;
}

void IndividualCCU::updateNtcTemperature(int ntc, double temperature)
{
    if (ntc >= this->_temperatures.size()) {
        return;
    }

    ledAndTemperatureWidget* temp = this->_temperatures.at(ntc);

    if (temp == NULL) {
        /*Throw exception?*/
        return;
    }
    temp->updateTemperature(temperature);

    return;
}

void IndividualCCU::on_turnOnFlowSensorButton_clicked()
{
    try {
        CANmessage message = this->_fcp->encodeSendCommand("interface", "ccu_" + this->_side, "set_flow_" + this->_side, 1, 0, 0);
        emit sendCan(message);

    } catch (JsonException& e) {
        qDebug() << e.toString();
    }
}

void IndividualCCU::on_turnOffFlowSensorButton_clicked()
{
    try {
        CANmessage message = this->_fcp->encodeSendCommand("interface", "ccu_" + this->_side, "set_flow_" + this->_side, 0, 0, 0);
        emit sendCan(message);

    } catch (JsonException& e) {
        qDebug() << e.toString();
    }
}

void IndividualCCU::on_TurnOnFans_clicked()
{
    try {
        CANmessage message = this->_fcp->encodeSendCommand("interface", "ccu_" + this->_side, "set_fans_" + this->_side, 1, 0, 0);
        emit sendCan(message);

    } catch (JsonException& e) {
        qDebug() << e.toString();
    }
}

void IndividualCCU::on_turnOffFans_clicked()
{
    try {
        CANmessage message = this->_fcp->encodeSendCommand("interface", "ccu_" + this->_side, "set_fans_" + this->_side, 0, 0, 0);
        emit sendCan(message);

    } catch (JsonException& e) {
        qDebug() << e.toString();
    }
}

void IndividualCCU::on_ntcSpinBox_valueChanged(int arg1)
{
    updateTemperatureScroll(arg1);
    this->_ntcNumber = arg1;
}

void IndividualCCU::updateTemperatureScroll(int newNtcNumber)
{

    if (newNtcNumber == _ntcNumber) {
        return;
    } else if (newNtcNumber > _ntcNumber) {
        addNtcTemperature(new ledAndTemperatureWidget(this, "Temperature " + QString::number(newNtcNumber)));
    } else {
        removeNtcTemperature(newNtcNumber);
    }
    try {
        CANmessage message = this->_fcp->encodeSendCommand("interface", "ccu_" + this->_side, "set_ntcs_" + this->_side, this->_ntcNumber, 0, 0);
        emit sendCan(message);

    } catch (JsonException& e) {
        qDebug() << e.toString();
    }
}

void IndividualCCU::removeNtcTemperature(int index)
{

    ledAndTemperatureWidget* toRemove = this->_temperatures.at(index);

    delete toRemove;

    this->_temperatures.removeAt(index);

    return;
}

/*Method to block scroll event on ntc spin box*/
bool IndividualCCU::eventFilter(QObject* obj, QEvent* event)
{
    if (event->type() == QEvent::Wheel && obj == this->ui->ntcSpinBox) {
        return true;
    }
    return false;
}

void IndividualCCU::setFcp(FCPCom* fcp)
{
    this->_fcp = fcp;
}

void IndividualCCU::setSide(QString side)
{
    this->_side = side;
    return;
}

QString IndividualCCU::getSide()
{
    return this->_side;
}

void IndividualCCU::updateFansAndPumpsStatus(int status)
{
    if (status) {
        this->ui->fansLED->turnGreen();
    } else {
        this->ui->fansLED->turnRed();
    }
}

void IndividualCCU::updateSensorFlowStatus(int status)
{
    if (status) {
        this->ui->flowSensorLED->turnGreen();
    } else {
        this->ui->flowSensorLED->turnRed();
    }
}
