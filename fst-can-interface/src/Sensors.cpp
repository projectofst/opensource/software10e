#include "Sensors.hpp"
#include "ui_Sensors.h"

#include <QTimer>

#include <cmath>

// static SUPOT_MSG_SIG supots;
// static WT_Data water_temp;

Sensors::Sensors(QWidget* parent, FCPCom* fcp)
    : UiWindow(parent, fcp)
    , ui(new Ui::Sensors)
{
    ui->setupUi(this);

    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Sensors::updateUI);

    timer->start(100);
}

Sensors::~Sensors()
{
    delete ui;
}

void Sensors::updateUI(void)
{

    // updateSupotsUI(supots, water_temp);
}

void Sensors::process_CAN_message(CANmessage)
{
    /*
    CANdata message = msg.candata;
    switch (message.dev_id){

    case DEVICE_ID_SUPOT_FRONT:
        parse_can_supot_sig(message, &supots);
        break;
    case DEVICE_ID_SUPOT_REAR:
        parse_can_supot_sig(message, &supots);
        break;
    case DEVICE_ID_WATER_TEMP:
        parse_can_message_wt(message, &water_temp);
        break;

    default:
        break;
    }
*/
    return;
}

double beta_function(double voltage)
{
    double resistance = voltage * 10e3 / (1024 - voltage);
    return 1 / (log(resistance / 10e3) / 3374 + 1 / 297.15) - 273.15;
}

void Sensors::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}

/*
void Sensors::updateSupotsUI(SUPOT_MSG_SIG supots_values, WT_Data water_temp){

    uint16_t front_r = supots_values.front_right * 75/4095;
    uint16_t front_l = supots_values.front_left * 75/4095;
    uint16_t rear_r  = supots_values.rear_right * 75/4095;
    uint16_t rear_l  = supots_values.rear_left  * 75/4095;

    ui->supot_front_right->setValue(front_r);
    ui->supot_front_left->setValue(front_l);
    ui->supot_rear_right->setValue(rear_r);
    ui->supot_rear_left->setValue(rear_l);

    ui->temp1->setText(QString::number(beta_function(water_temp.temperature1.temp1)));
    ui->temp2->setText(QString::number(beta_function(water_temp.temperature1.temp2)));
    ui->temp3->setText(QString::number(beta_function(water_temp.temperature1.temp3)));
    ui->temp4->setText(QString::number(beta_function(water_temp.temperature2.temp1)));
    ui->temp5->setText(QString::number(beta_function(water_temp.temperature2.temp2)));
    ui->temp6->setText(QString::number(beta_function(water_temp.temperature2.temp3)));

    return;
}
*/
