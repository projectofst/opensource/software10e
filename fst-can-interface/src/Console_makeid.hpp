#ifndef MAKEID_H
#define MAKEID_H

#include <QWidget>

namespace Ui {
class MakeID;
}

class MakeID : public QWidget {
    Q_OBJECT

public:
    explicit MakeID(QWidget* parent = 0);
    ~MakeID();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::MakeID* ui;

signals:
    void id_value(int id);
};

#endif // MAKEID_H
