#pragma once
#ifndef LAP_TIME_HPP
#define LAP_TIME_HPP

#include "Custom_Lap.hpp"
#include "Exceptions/helper.hpp"
#include "UiWindow.hpp"
#include "qt-can-com/UDPSocket.hpp"
#include "run_laps.hpp"
#include "single_lap.hpp"
#include "ui_single_lap.h"
#include <QDebug>
#include <QElapsedTimer>
#include <QFileDialog>
#include <QKeyEvent>
#include <QKeySequence>
#include <QMap>
#include <QScrollArea>
#include <QTimer>
#include <QWidget>

#define MAX_RUNS 4
class Single_lap;
class run_laps;
namespace Ui {
class Lap_time;
}

class Lap_time : public UiWindow {
    Q_OBJECT

public:
    explicit Lap_time(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    QString getTitle() override { return "Lap Timer"; };
    void readjust();
    QString transform_time(qint64 time);
    void process_CAN_message(CANmessage msg) override;
    void updateFCP(FCPCom* fcp) override;
    ~Lap_time();

private slots:
    void on_lap_button_clicked();
    void update_current_lap_time();
    QList<qint64> format_time(qint64 time);

    void on_cone_button_clicked();

    void on_DNF_Button_clicked();

    void check_laps();

    void keyPressEvent(QKeyEvent* event) override;

    void on_stop_button_toggled(bool checked);

    int check_all_data();

    void on_export_button_clicked();

    void on_DC_button_clicked();

    void on_reset_all_button_clicked();

    void on_new_run_clicked();

    void delete_last();
    void on_runs_currentChanged(int index);

    void on_update_button_clicked();

    void on_del_run_clicked();

    void on_custom_lap_clicked();

    void on_site_input_textEdited(const QString& arg1);

    void on_track_type_dropdown_currentIndexChanged(const QString& arg1);

    void on_track_length_input_textEdited(const QString& arg1);

    void on_driver_input_textEdited(const QString& arg1);

    void on_date_input_textEdited(const QString& arg1);

    void on_time_input_textEdited(const QString& arg1);

    void on_responsible_input_textEdited(const QString& arg1);

    void custom_lap_added(qint64 time, int cones, int, int);

    void add_lap(Single_lap* new_lap);

    void recieveNewLapSignal(QByteArray&);
    void on_connectButton_toggled(bool checked);

    void on_sectorCombobox_currentIndexChanged(int index);

private:
    Ui::Lap_time* ui;
    Custom_Lap* custom_lap;
    QElapsedTimer* lap_timer;
    int counting_lap, cone_counter, first_time, number_of_laps, cones, number_runs, driver_change_in_course;
    QTimer* update_timer;
    QMap<QString, double> cones_map, OC;
    QVector<run_laps*> runs;
    QList<UDPSocket*> _lapSignalSocket;
    FCPCom* fcp;
signals:

    void newLap();
};

#endif // LAP_TIME_HPP
