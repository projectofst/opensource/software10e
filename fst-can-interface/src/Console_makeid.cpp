#include "Console_makeid.hpp"
#include "ui_Console_makeid.h"

MakeID::MakeID(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::MakeID)
{
    ui->setupUi(this);

    this->setWindowTitle("Make ID");
    this->setFixedHeight(150);
    this->setFixedWidth(200);
}

MakeID::~MakeID()
{
    delete ui;
}

void MakeID::on_pushButton_clicked()
{
    int dev_id = ui->dev_id->value();
    int msg_id = ui->msg_id->value();

    int id = dev_id + (msg_id << 5);

    emit id_value(id);

    this->close();
}

void MakeID::on_pushButton_2_clicked()
{
    this->close();
}
