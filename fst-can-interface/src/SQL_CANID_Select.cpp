#include "SQL_CANID_Select.hpp"
#include "ui_SQL_CANID_Select.h"

SQL_CANID_Select::SQL_CANID_Select(QWidget* parent, SQL_CANID* parentsql)
    : UiWindow(parent)
    , ui(new Ui::SQL_CANID_Select)
{
    ui->setupUi(this);

    DataBase = parentsql;
    updateViewList();
    updateDbPath();
}

SQL_CANID_Select::~SQL_CANID_Select()
{
    delete ui;
}

void SQL_CANID_Select::updateViewList()
{
    ui->ViewsCombo->clear();
    ui->ViewsCombo->addItems(DataBase->giveViews());
}

void SQL_CANID_Select::updateDbPath()
{
    ui->CurrentPath->clear();
    ui->CurrentPath->setText(DataBase->giveDbName());
}

void SQL_CANID_Select::updateTable(QString View)
{
    DataBase->giveView(View);
}
