#include "TestBenchMotor.hpp"
#include "ui_TestBenchMotor.h"

TestBenchMotor::TestBenchMotor(QWidget* parent, QString motorName, int motorNumber, FCPCom* fcp)
    : QWidget(parent)
    , ui(new Ui::TestBenchMotor)
{
    ui->setupUi(this);
    this->ui->MotorName->setText(motorName);
    this->_motorNumber = motorNumber;
    this->_fcp = fcp;
    this->_alarmManager = std::unique_ptr<DataAlarmManager>(new DataAlarmManager(this));
    connect(this->_alarmManager.get(), &DataAlarmManager::newData, this, &TestBenchMotor::newDataReady);
    connect(this->_alarmManager.get(), &DataAlarmManager::durationTimeout, this, &TestBenchMotor::durationTimeout);

    this->ui->stopwatch->setTime(0);
}

TestBenchMotor::~TestBenchMotor()
{
    delete ui;
}

void TestBenchMotor::start()
{
    durationTimeout();
}

void TestBenchMotor::stop()
{
    this->_dataQueue.clear();
    this->ui->stopwatch->stop();
    this->ui->stopwatch->setTime(0);
    this->ui->stopwatch->reset();
    this->_alarmManager->stopAll();
    this->_alarmManager->clear();
    CANmessage msg = this->_fcp->encodeSendCommand("interface", "iib", "ext_input" + QString::number(_motorNumber),
        0,
        0,
        0);
    this->clearUi();
    emit newDataReady(COM::encodeToByteArray(msg));
    ;
}

void TestBenchMotor::addNewMessage(QString messageName, int positiveTorque, int negativeTorque, int rpm, int timeMs)
{

    MotorData data = { .messageName = messageName, .positiveTorque = positiveTorque, .negativeTorque = negativeTorque, .rpm = rpm, .duration = timeMs };

    this->_dataQueue.enqueue(data);

    updateCurrentUi(this->_dataQueue.first());
    if (this->_dataQueue.size() > 1)
        updateNextUi(this->_dataQueue.at(1));
}

MotorData TestBenchMotor::newMotorMessage()
{
    MotorData currData = this->_dataQueue.dequeue();
    CANmessage msg = this->_fcp->encodeSendCommand("interface", "iib", currData.messageName,
        currData.positiveTorque,
        currData.negativeTorque,
        currData.rpm);

    QByteArray byteArrayMessage = *(COM::encodeToByteArray(msg).get());
    this->_alarmManager->addNewTimedAlarm(byteArrayMessage, 50, currData.duration);
    this->ui->stopwatch->setTime(currData.duration);
    this->ui->stopwatch->start();
    return currData;
}

void TestBenchMotor::updateCurrentUi(MotorData currentData)
{
    this->ui->currentRPM->setText(QString::number(currentData.rpm));
    this->ui->currentTorquePlus->setText(QString::number(currentData.positiveTorque));
    this->ui->currentTorqueNeg->setText(QString::number(currentData.negativeTorque));
}

void TestBenchMotor::updateNextUi(MotorData nextData)
{
    this->ui->nextRPM->setText(QString::number(nextData.rpm));
    this->ui->nextTorquePlus->setText(QString::number(nextData.positiveTorque));
    this->ui->nextTorqueNeg->setText(QString::number(nextData.negativeTorque));
}

void TestBenchMotor::clearNextUi()
{
    this->ui->nextRPM->setText(" ");
    this->ui->nextTorquePlus->setText(" ");
    this->ui->nextTorqueNeg->setText(" ");
}

void TestBenchMotor::clearCurrentUi()
{
    this->ui->currentRPM->setText(" ");
    this->ui->currentTorquePlus->setText(" ");
    this->ui->currentTorqueNeg->setText(" ");
}

void TestBenchMotor::clearUi()
{
    this->clearNextUi();
    this->clearCurrentUi();
}

void TestBenchMotor::durationTimeout()
{
    MotorData currentData, nextData;
    if (!this->_dataQueue.isEmpty()) {
        currentData = newMotorMessage();
        updateCurrentUi(currentData);

        if (!this->_dataQueue.isEmpty()) {
            nextData = this->_dataQueue.first();
            updateNextUi(nextData);
        } else {
            this->clearNextUi();
        }
    } else {
        this->clearUi();
    }
}

void TestBenchMotor::on_StartButton_clicked()
{
    this->start();
}

void TestBenchMotor::on_StopButton_clicked()
{
    this->stop();
}
