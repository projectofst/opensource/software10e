#ifndef TESTBENCH_H
#define TESTBENCH_H

#include "COM.hpp"
#include "MotorBench/TestBenchMotor.hpp"
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"
#include "ui_MotorBench.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QPalette>
#include <QString>
#include <QTextEdit>
#include <QTextStream>
#include <QWidget>
#include <regex>

namespace Ui {
class TestBench;
}

class TestBench : public UiWindow {
    Q_OBJECT

public:
    explicit TestBench(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    void updateFCP(FCPCom* fcp) override;
    QString getTitle() override { return "Test Bench"; };
    ~TestBench() override;

private slots:
    void process_CAN_message(CANmessage) override;

    void on_startButton_clicked();

    void on_refreshButton_clicked();

    void sendData(std::shared_ptr<QByteArray> data);

    bool isStringInt(QString string);

    bool isCSVOk();

    int getMotorFromString(QString motor);

    void on_stopButton_clicked();

    void on_addCustomButton_clicked();

    bool isSingleMotor(QString motor);

    void on_Load_clicked();

    bool verifyInputLine(QString string);

    bool isLineSkippable(QString line);

    void on_sendEIMLED_clicked();

    void updateUI(CANmessage message);

private:
    Ui::TestBench* ui;
    QList<std::shared_ptr<TestBenchMotor>> _motors;
};

#endif // TESTBENCH_H
