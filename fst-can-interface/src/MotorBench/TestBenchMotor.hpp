#ifndef TESTBENCHMOTOR_HPP
#define TESTBENCHMOTOR_HPP

#include "CAN_IDs.h"
#include "COM.hpp"
#include "DataAlarmManager.hpp"
#include "fcpcom.hpp"
#include <QQueue>
#include <QWidget>
#include <memory>
typedef struct {
    QString messageName;
    int positiveTorque;
    int negativeTorque;
    int rpm;

    int duration;
} MotorData;

namespace Ui {
class TestBenchMotor;
}

class TestBenchMotor : public QWidget {
    Q_OBJECT

public:
    explicit TestBenchMotor(QWidget* parent = nullptr, QString motorName = "", int motorNumber = -1, FCPCom* fcp = nullptr);
    ~TestBenchMotor();
    void addNewMessage(QString messageName, int positiveTorque, int negativeTorque, int rpm, int timeMs);

    void start();
    void stop();
private slots:
    MotorData newMotorMessage();
    void updateCurrentUi(MotorData currentData);
    void updateNextUi(MotorData nextData);
    void durationTimeout();
    void clearUi();
    void clearNextUi();
    void clearCurrentUi();
    void on_StartButton_clicked();

    void on_StopButton_clicked();

private:
    Ui::TestBenchMotor* ui;
    QQueue<MotorData> _dataQueue;
    FCPCom* _fcp;
    std::unique_ptr<DataAlarmManager> _alarmManager;
    int _motorNumber;

signals:
    void newDataReady(std::shared_ptr<QByteArray> data);
};

#endif // TESTBENCHMOTOR_HPP
