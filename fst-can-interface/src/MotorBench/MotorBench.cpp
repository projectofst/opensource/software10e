#include "MotorBench.hpp"

#define MAX_SPEED_RPM 21000 // rpm
#define MAX_TORQUE 21 // N/m
#define FrontLeft 0
#define FrontRight 1
#define RearLeft 2
#define RearRight 3

TestBench::TestBench(QWidget* parent, FCPCom* fcp)
    : UiWindow(parent, fcp)
    , ui(new Ui::TestBench)
{
    ui->setupUi(this);
    std::shared_ptr<TestBenchMotor> FLmotor = std::make_shared<TestBenchMotor>(this, "FL", FrontLeft, this->_fcp);
    this->_motors.append(FLmotor);
    std::shared_ptr<TestBenchMotor> FRmotor = std::make_shared<TestBenchMotor>(this, "FR", FrontRight, this->_fcp);
    this->_motors.append(FRmotor);
    std::shared_ptr<TestBenchMotor> RLmotor = std::make_shared<TestBenchMotor>(this, "RL", RearLeft, this->_fcp);
    this->_motors.append(RLmotor);
    std::shared_ptr<TestBenchMotor> RRmotor = std::make_shared<TestBenchMotor>(this, "RR", RearRight, this->_fcp);
    this->_motors.append(RRmotor);

    this->ui->FrontMotorLayout->addWidget(FLmotor.get());
    this->ui->FrontMotorLayout->addWidget(FRmotor.get());
    this->ui->RearMotorLayout->addWidget(RLmotor.get());
    this->ui->RearMotorLayout->addWidget(RRmotor.get());

    for (auto motor : this->_motors) {
        connect(motor.get(), &TestBenchMotor::newDataReady, this, &TestBench::sendData);
    }
}

TestBench::~TestBench()
{
    delete ui;
}

void TestBench::updateUI(CANmessage message)
{
    try {
        if (this->_fcp->isCorrectReturnCommand(message, "iib", "input_mode")) {
            QPair<QString, QMap<QString, double>> pair = this->_fcp->decodeMessage(message);
            QMap<QString, double> data = pair.second;
            if (data.value("ret1") == 2) {
                this->ui->EIMLED->turnGreen();
            } else {
                this->ui->EIMLED->turnRed();
            }
        }
    } catch (JsonException& e) {
        qDebug() << e.toString();
    }
}

void TestBench::process_CAN_message(CANmessage message)
{
    if (this->_fcp->getDevice(message.candata.dev_id)->getName() == "iib") {
        updateUI(message);
    }
}

void TestBench::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}

void TestBench::sendData(std::shared_ptr<QByteArray> data)
{
    this->send_to_CAN(*data.get());
}

void TestBench::on_startButton_clicked()
{
    for (auto motor : this->_motors) {
        motor->start();
    }
}

bool TestBench::isStringInt(QString string)
{
    bool ok;
    string.toInt(&ok, 10);
    return ok;
}

bool TestBench::isCSVOk()
{
    QString text = this->ui->CSVtext->toPlainText();
    bool CSVOk = true;
    this->ui->CSVtext->clear();
    QColor black(0, 0, 0);
    QStringList lines = text.split("\n");

    for (QString line : lines) {
        line = line.trimmed();
        QColor red(255, 0, 0);
        this->ui->CSVtext->setTextColor(black);
        if (this->isLineSkippable(line))
            continue;
        if (!this->verifyInputLine(line)) {
            this->ui->CSVtext->setTextColor(red);
            CSVOk = false;
        }
        this->ui->CSVtext->insertPlainText(line + "\n");
    }
    this->ui->CSVtext->setTextColor(black);
    return CSVOk;
}

int TestBench::getMotorFromString(QString motor)
{
    bool ok;
    int num = motor.toInt(&ok, 10);
    if (motor == "FL") {
        return FrontLeft;
    } else if (motor == "FR") {
        return FrontRight;
    } else if (motor == "RR") {
        return RearRight;
    } else if (motor == "RL") {
        return RearLeft;
    } else if (ok && num >= 0 && num < 5) { /*is number*/
        return num;
    } else {
        return -1;
    }
}
void TestBench::on_refreshButton_clicked()
{
    for (auto motor : this->_motors) {
        motor->stop();
    }
    if (this->isCSVOk()) {
        QString text = this->ui->CSVtext->toPlainText();
        text = text.trimmed();
        this->ui->CSVtext->setText(text);
        QStringList lines = text.split("\n");
        for (QString line : lines) {
            QString motor, torquePos, torqueNeg, rpm, time;
            QStringList parameters = line.split(" ");
            if (verifyInputLine(line)) {
                motor = parameters[0];
                torquePos = parameters[1];
                torqueNeg = parameters[2];
                rpm = parameters[3];
                time = parameters[4];
                int motorNum = getMotorFromString(motor);
                this->_motors.at(motorNum)->addNewMessage("ext_input" + QString::number(motorNum), torquePos.toInt(), torqueNeg.toInt(), rpm.toInt(), time.toInt());
            }
        }
    }
}

void TestBench::on_stopButton_clicked()
{
    for (auto motor : this->_motors) {
        motor->stop();
    }
}

void TestBench::on_addCustomButton_clicked()
{
    QPalette palette;
    palette.setColor(ui->motorLabel->foregroundRole(), Qt::black);
    ui->motorLabel->setPalette(palette);

    QString motor = this->ui->customMotorBox->currentText();
    QString torquePlus = this->ui->torquePosInput->text();
    QString torqueNeg = this->ui->torqueNegInput->text();
    QString RPM = this->ui->rpmInput->text();
    QString time = this->ui->timeInput->text();
    bool overallOk = true;
    if (motor == "") {
        palette.setColor(ui->motorLabel->foregroundRole(), Qt::red);
        ui->motorLabel->setPalette(palette);
        overallOk = false;
    }

    if (overallOk) {
        if (isSingleMotor(motor))
            this->ui->CSVtext->insertPlainText(motor + " " + torquePlus + " " + torqueNeg + " " + RPM + " " + time + "\n");
        else if (motor == "Rear") {
            this->ui->CSVtext->insertPlainText("RR " + torquePlus + " " + torqueNeg + " " + RPM + " " + time + "\n");
            this->ui->CSVtext->insertPlainText("RL " + torquePlus + " " + torqueNeg + " " + RPM + " " + time + "\n");
        } else if (motor == "Front") {
            this->ui->CSVtext->insertPlainText("FR " + torquePlus + " " + torqueNeg + " " + RPM + " " + time + "\n");
            this->ui->CSVtext->insertPlainText("FL " + torquePlus + " " + torqueNeg + " " + RPM + " " + time + "\n");
        } else if (motor == "All") {
            this->ui->CSVtext->insertPlainText("FR " + torquePlus + " " + torqueNeg + " " + RPM + " " + time + "\n");
            this->ui->CSVtext->insertPlainText("FL " + torquePlus + " " + torqueNeg + " " + RPM + " " + time + "\n");
            this->ui->CSVtext->insertPlainText("RR " + torquePlus + " " + torqueNeg + " " + RPM + " " + time + "\n");
            this->ui->CSVtext->insertPlainText("RL " + torquePlus + " " + torqueNeg + " " + RPM + " " + time + "\n");
        }
        on_refreshButton_clicked();
    }
}

bool TestBench::isSingleMotor(QString motor)
{
    return (motor == "RL" || motor == "FL" || motor == "RR" || motor == "FR");
}

void TestBench::on_Load_clicked()
{

    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open LogFile"), QDir::currentPath(), tr("Files (*.log *.txt)"));

    QFile inputFile(fileName);

    if (inputFile.open(QIODevice::ReadOnly)) {
        QTextStream in(&inputFile);
        this->ui->CSVtext->clear();
        while (!in.atEnd()) {
            QString line = in.readLine();
            this->ui->CSVtext->insertPlainText(line + "\n");
        }
        inputFile.close();
    }
}

bool TestBench::isLineSkippable(QString line)
{
    return (line == "" || (line.size() > 0 && line.at(0) == "#") || line == " " || line == "\n");
}

bool TestBench::verifyInputLine(QString string)
{
    /* https://regex101.com
       Regex tool for php/JS/Python/Go, you just need to find the equivalent symbols for cpp, helped me a lot
       to understand regular expressions.
    */
    std::regex regExpression("(^[FR][RL])\\s(\\d{1,5})\\s(\\d{1,5})\\s(\\d{1,5})\\s(\\d{1,})$");
    std::string standardString = string.toStdString();
    std::smatch matches;

    std::regex_search(standardString, matches, regExpression);
    return matches.size() > 0;
}

void TestBench::on_sendEIMLED_clicked()
{
    /*2 is the External input mode. Json Limitations dont allow us to have a description of constants
        Change when Json is updated to support this.
    */
    CANmessage msg = this->_fcp->encodeSendCommand("interface", "iib", "input_mode", 2, 0, 0);

    this->send_to_CAN(*(COM::encodeToByteArray(msg)));
}
