#include "testwidget.hpp"
#include "COM.hpp"
#include "ui_testwidget.h"

TestWidget::TestWidget(QWidget* parent, FCPCom* fcp)
    : UiWindow(parent, fcp)
    , ui(new Ui::TestWidget)
{
    ui->setupUi(this);
    this->sig = new CanSignalGenerator(this, "sin(x)", 1, 10000, 10.0, 10, this->_fcp, "ccu_right", "tach_right", "frequency_right");
    connect(this->sig, &CanSignalGenerator::newSample, this, &TestWidget::test);
}

TestWidget::~TestWidget()
{
    delete ui;
}

void TestWidget::on_Button1_clicked()
{
    this->sig->reset();
    this->sig->start();
}

void TestWidget::process_CAN_message(CANmessage)
{
}
void TestWidget::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}

void TestWidget::on_Button2_clicked()
{
}
void TestWidget::test(CANmessage msg)
{
    this->send_to_CAN(*COM::encodeToByteArray<CANmessage>(msg));
}
