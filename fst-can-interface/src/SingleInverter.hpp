#ifndef SingleInverter_HPP
#define SingleInverter_HPP

#include "can-ids/CAN_IDs.h"
#include <QWidget>

namespace Ui {
class SingleInverter;
}

class SingleInverter : public QWidget {
    Q_OBJECT

public:
    explicit SingleInverter(QWidget* parent = nullptr, QString MotorName = "MOTOR_NAME");
    ~SingleInverter();

    void updateMotorInformation(QMap<QString, double> info);
    void updateInverterInformation(QMap<QString, double> info);
    void updateInverterLimits(QMap<QString, double> info);
    void clear();
    void setName(QString name);

private:
    Ui::SingleInverter* ui;
    QString _motorName;
};

#endif // SingleInverter_HPP
