#include "versions.hpp"
#include "pcb_version.hpp"
#include "table.h"
#include "ui_versions.h"
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QTimer>
#include <qpalette.h>

versions::versions(QWidget* parent, FCPCom* fcp, Helper*, ConfigManager*)
    : UiWindow(parent, fcp)
    , ui(new Ui::versions)
{
    ui->setupUi(this);
    this->fcp = fcp;

    init_devices(); /* Get all the  possible devices */

    timer1 = new QTimer(this);
    connect(timer1, SIGNAL(timeout()), this, SLOT(request_version()));
    timer_status = 0;
    on_refresh_button_clicked();
}

versions::~versions()
{
    delete ui;
}

void versions::process_CAN_message(CANmessage message)
{
    QPair<QString, QMap<QString, double>> decoded;
    try {
        decoded = fcp->decodeMessage(message);
        if (decoded.first == "ans_get" && decoded.second["id"] == 0) {
            qDebug() << "data: " << decoded.second["data"];
            QString device_name = fcp->getDevice(message.candata.dev_id)->getName();
            unsigned version = decoded.second["data"];
            process_new_version(device_name, QString::number(version, 16));
        }
        // "Sometimes shit happen and you can't do anything about it" - (Mahatma Gandhi 2021)
    } catch (...) {
        return;
    }
}

void versions::request_version()
{
    CANmessage get_msg;
    foreach (auto device, devices) {
        try {
            get_msg = fcp->encodeGetConfig("interface", device->getName(), "version");
            emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(get_msg).get()));
        } catch (...) {
            continue;
        }
    }
    // Block re-trigger
    on_refresh_button_clicked();
    return;
}

void versions::process_new_version(QString pcb_name, QString version)
{
    qDebug() << "version" << version;
    foreach (auto pcb, all_pcbs) {
        if (pcb->get_name() == pcb_name) {
            pcb->set_current_version(version);
        }
    }
    return;
}

void versions::on_refresh_button_clicked()
{
    if (timer_status == 0) {
        timer1->start(50);
        timer_status = 1;
    } else {
        timer1->stop();
        timer_status = 0;
    }
}

void versions::init_devices()
{
    devices = fcp->getDevices();
    foreach (auto device, devices) {
        pcb_version* new_pcb = new pcb_version;
        new_pcb->set_name(device->getName());
        all_pcbs.append(new_pcb);
        ui->list->addWidget(new_pcb);
    }
}

void versions::updateFCP(FCPCom* fcp)
{
    setFCP(fcp);
}
