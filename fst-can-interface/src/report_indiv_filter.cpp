#include "report_indiv_filter.hpp"
#include "ui_report_indiv_filter.h"

Report_indiv_filter::Report_indiv_filter(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::Report_indiv_filter)
{
    ui->setupUi(this);
    ui->advanced_options->hide();
}
int Report_indiv_filter::get_set_status()
{
    return ui->set_check_box->isChecked();
}

int Report_indiv_filter::get_get_status()
{
    return ui->get_check_box->isChecked();
}

int Report_indiv_filter::get_error_status()
{
    return ui->error_check_box->isChecked();
}

int Report_indiv_filter::get_reset_status()
{
    return ui->reset_check_box->isChecked();
}

int Report_indiv_filter::get_master_check_box_status()
{
    return ui->master_check_box->isChecked();
}

void Report_indiv_filter::set_device_name(QString name)
{
    ui->device_name->setText(name);
    return;
}

QString Report_indiv_filter::get_device_name()
{
    return ui->device_name->text();
}

Report_indiv_filter::~Report_indiv_filter()
{
    delete ui;
}

void Report_indiv_filter::on_advanced_button_clicked()
{
    ui->advanced_options->isHidden() ? ui->advanced_options->show() : ui->advanced_options->hide();
}

void Report_indiv_filter::on_master_check_box_stateChanged(int arg1)
{
    if (arg1) {
        ui->get_check_box->setCheckState(Qt::CheckState(2));
        ui->set_check_box->setCheckState(Qt::CheckState(2));
        ui->error_check_box->setCheckState(Qt::CheckState(2));
        ui->reset_check_box->setCheckState(Qt::CheckState(2));
    } else {
        ui->get_check_box->setCheckState(Qt::CheckState(0));
        ui->set_check_box->setCheckState(Qt::CheckState(0));
        ui->error_check_box->setCheckState(Qt::CheckState(0));
        ui->reset_check_box->setCheckState(Qt::CheckState(0));
    }
}

void Report_indiv_filter::on_del_but_clicked()
{
    return;
}
