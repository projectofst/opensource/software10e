#ifndef POTPROGRESS_HPP
#define POTPROGRESS_HPP

#include <QWidget>
#include <stdint.h>

#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/TE_CAN.h"

namespace Ui {
class PotProgress;
}

class PotProgress : public QWidget {
    Q_OBJECT

public:
    explicit PotProgress(QWidget* parent = nullptr, QString title = "");
    ~PotProgress();
    void update_limits(TE_THRESHOLD_LIMIT limit, int);
    void update_value(int);

private:
    Ui::PotProgress* ui;
    int limits[6];
    int reading;
    QString check_te_limit(int);
};

#endif // POTPROGRESS_HPP
