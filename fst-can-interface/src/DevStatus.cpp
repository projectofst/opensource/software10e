#include "DevStatus.hpp"
#include "ui_DevStatus.h"

#include <QPushButton>
#include <QShortcut>

DevStatus::DevStatus(QWidget* parent, /*QDockWidget *dockwidget,*/ FCPCom* fcp, Helper*, ConfigManager*)
    : UiWindow(parent, fcp)
    , ui(new Ui::DevStatus)
{

    ui->setupUi(this);
    this->fcp = fcp;

    // Create Gradient Colors
    red = QLinearGradient(0, 0, 0, 100);
    red.setColorAt(0, QColor(0, 0x99, 0xFE));
    red.setColorAt(1, QColor(0, 0x99, 0xFE));
    green = QLinearGradient(100, 0, 0, 100);
    green.setColorAt(0, QColor(0, 0xAA, 0));
    green.setColorAt(1, QColor(0, 0xAA, 0));

    yellow = QLinearGradient(100, 0, 0, 100);
    yellow.setColorAt(0, QColor(190, 190, 0));
    yellow.setColorAt(1, QColor(225, 225, 0));

    lastK = -1;
    currentK = 1;
    createDevs();

    resize(this->width());
    QTimer* verifyStatus = new QTimer;
    verifyStatus->setInterval(2000);
    verifyStatus->start();
    connect(verifyStatus, SIGNAL(timeout()), this, SLOT(silentWarning()));

    new QShortcut(QKeySequence(Qt::ALT + Qt::Key_Q), this, SLOT(closeWindow()));
}

DevStatus::~DevStatus(void)
{
    delete ui;
}

void DevStatus::closeWindow(void)
{
    this->close();
}

void DevStatus::process_CAN_message(CANmessage Msg)
{
    QString device_name;
    try {
        device_name = fcp->getDevice(Msg.candata.dev_id)->getName();
    } catch (...) {
        return;
    }

    if (DevButtons.keys().contains(device_name)) {
        // Green = Receiving messages
        QPalette temppal = DevButtons[device_name]->palette();
        temppal.setBrush(QPalette::Button, green);
        DevButtons[device_name]->setPalette(temppal);
        DevTimers[device_name]->start(3000);
        DevStates[device_name] = 1;
        try {
            auto decoded = fcp->decodeMessage(Msg);
            if (decoded.first == "ans_get") {
                DevButtons[device_name]->setDescription("#" + QString::number((unsigned)decoded.second["data"], 16));
            }
        } catch (noSuchMessageException const&) {
            return;
        }
    }
}

void DevStatus::resize(int Width)
{
    layoutDevs(((Width - 80) / 150));
}

void DevStatus::silentWarning()
{
    foreach (QTimer* timer, DevTimers) {
        if (DevStates[DevTimers.key(timer)] == 1) {
            if (!timer->isActive()) {
                // Yellow = We received messages but we are not receiving them anymore
                QPalette temppal = DevButtons[DevTimers.key(timer)]->palette();
                temppal.setBrush(QPalette::Button, yellow);
                DevButtons[DevTimers.key(timer)]->setPalette(temppal);
                DevButtons[DevTimers.key(timer)]->setDescription("(Timed out)");
            } else {
                CANmessage get_msg;
                // It's alive, send version request
                try {
                    get_msg = fcp->encodeGetConfig("interface", DevTimers.key(timer), "version");
                    emit send_to_CAN(*(COM::encodeToByteArray<CANmessage>(get_msg).get()));
                } catch (...) {
                    continue;
                }
            }
        }
    }
}

void DevStatus::createDevs()
{
    int i = 0;
    foreach (auto& dev, this->fcp->getDevices()) {
        QString dev_name = dev->getName();
        int dev_id = dev->getId();
        auto dev_button = new QCommandLinkButton(QString::number(dev_id) + QString(". ") + QString(dev_name), QString("NONE"));
        DevButtons[dev_name] = dev_button;

        QPalette temppal = DevButtons[dev_name]->palette();
        temppal.setBrush(QPalette::Button, red);
        DevButtons[dev_name]->setAutoFillBackground(true);
        DevButtons[dev_name]->setPalette(temppal);
        DevButtons[dev_name]->setMinimumSize(QSize(150, 50));
        DevButtons[dev_name]->setMaximumSize(QSize(150, 50));

        DevTimers[dev_name] = new QTimer;
        DevTimers[dev_name]->setSingleShot(true);

        DevStates[dev_name] = 0;
        connect(dev_button, &QPushButton::clicked, this, [this, dev_name](bool) { emit openWidget(dev_name, true); });

        i++;
    }
}

void DevStatus::layoutDevs(int k)
{
    k = k - 1;
    if (lastK != k) {
        QGridLayout* DevicesGrid = new QGridLayout;
        int c = 0;
        int l = 0;
        foreach (QWidget* Button, DevButtons) {
            DevicesGrid->addWidget(Button, l, c);
            c++;
            if (c > k) {
                c = 0;
                l++;
            }
        }
        foreach (QObject* Button, ui->Devs->layout()->children()) {
            ui->Devs->layout()->removeWidget(static_cast<QWidget*>(Button));
        }
        delete ui->Devs->layout();
        ui->Devs->setLayout(DevicesGrid);
        lastK = k;
    }
}

void DevStatus::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}

void DevStatus::resizeEvent(QResizeEvent* event)
{
    QWidget::resizeEvent(event);
    resize(this->width());
}
