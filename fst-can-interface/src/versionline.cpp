#include "versionline.hpp"
#include "ui_versionline.h"

#include "UiWindow.hpp"

VersionLine::VersionLine(QWidget* parent)
    : UiWindow(parent)
    , ui(new Ui::VersionLine)
{
    ui->setupUi(this);
}

VersionLine::~VersionLine()
{
    delete ui;
}
