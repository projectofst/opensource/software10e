#ifndef UIWINDOW_HPP
#define UIWINDOW_HPP

#include "ConfigManager/configmanager.hpp"
#include "can-ids/CAN_IDs.h"
#include "fcpcom.hpp"
#include <QCloseEvent>
#include <QWidget>

namespace Ui {
class UiWidget;
}

class UiWindow : public QWidget {
    Q_OBJECT

public:
    explicit UiWindow(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    virtual QString getTitle() { return "Empty"; };
    virtual void export_state() { qDebug() << "EXPORT " + this->objectName() + ": Not implemented"; };
    virtual void load_state(QMap<QString, QVariant>) { qDebug() << "LOAD " + this->objectName() + ": Not implemented"; };
    virtual void setPrivateValue(QVariant) {};
    ~UiWindow(void);

signals:
    void send_to_CAN(QByteArray&);
    void newLogMessage(CANmessage);
    void removeFromObservers(UiWindow* win);
    void removeFromActiveWidgets(UiWindow* win);
    void changeLogTime();
    void openWidget(QString, bool);

public slots:
    virtual void process_CAN_message(CANmessage) = 0;
    virtual void updateFCP(FCPCom*);
    virtual void handleTimeChange();
    FCPCom* getFCP();
    virtual void setFCP(FCPCom* fcp);

protected:
    FCPCom* _fcp;
    virtual void closeEvent(QCloseEvent*);

private:
};

#endif // UIWINDOW_HPP
