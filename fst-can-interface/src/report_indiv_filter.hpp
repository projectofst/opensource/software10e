#ifndef REPORT_INDIV_FILTER_HPP
#define REPORT_INDIV_FILTER_HPP

#include <QWidget>

namespace Ui {
class Report_indiv_filter;
}

class Report_indiv_filter : public QWidget {
    Q_OBJECT

public:
    explicit Report_indiv_filter(QWidget* parent = nullptr);
    ~Report_indiv_filter();
    int get_set_status();
    int get_get_status();
    QString get_device_name();
    void set_device_name(QString name);
    int get_error_status();
    int get_reset_status();
    int get_master_check_box_status();
    Ui::Report_indiv_filter* ui;

private slots:

    void on_advanced_button_clicked();

    void on_master_check_box_stateChanged(int arg1);

    void on_del_but_clicked();

private:
};

#endif // REPORT_INDIV_FILTER_HPP
