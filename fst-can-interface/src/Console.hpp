/**********************************************************************
 *	 FST CAN tools --- interface
 *
 *	 Console widget header
 *	 _______________________________________________________________
 *
 *	 Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *	 This program is free software; you can redistribute it and/or
 *	 modify it under the terms of the GNU General Public License
 *	 as published by the Free Software Foundation; version 2 of the
 *	 License only.
 *
 *	 This program is distributed in the hope that it will be useful,
 *	 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	 GNU General Public License for more details.
 *
 *	 You should have received a copy of the GNU General Public License
 *	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

#ifndef CONSOLE_HPP
#define CONSOLE_HPP

#include <QDateTime>
#include <QFile>
#include <QMutex>
#include <QPair>
#include <QTextStream>
#include <QTimer>
#include <QVector>
#include <QWidget>
#include <queue>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "COM_SerialPort.hpp"
#include "Console_makeid.hpp"
#include "can-ids/CAN_IDs.h"
#include "filterwidget.hpp"

#include "CanSignalGenerator.hpp"
#include "ConfigManager/configmanagerdialog.hpp"
#include "Console/FcpDisplay.hpp"
#include "Exceptions/helper.hpp"
#include "LogReplay/LogKeeper.hpp"
#include "Resources/FCPSignalDisplayValue.hpp"
#include "Resources/nameValueInputTemplate.hpp"
#include "UiWindow.hpp"
#include "fcpcom.hpp"
#include "state_manager.hpp"
#include <QComboBox>

#define MAX_LOG_LINES_CONSOLE 100 // Number of messages to hold in window
#define LOG_FLUSH_timestamp 5000 // Time between flushes
#define TELEMETRY_TIMOUT 5500

namespace Ui {
class Console;
}

class Console : public UiWindow {
    Q_OBJECT

public:
    explicit Console(QWidget* parent = nullptr, FCPCom* fcp = nullptr, Helper* log = nullptr, ConfigManager* config_manager = nullptr);
    ~Console(void) override;
    bool _linked;
    void updateFCP(FCPCom* fcp) override;
    void load_state(QMap<QString, QVariant> config) override;
    QString getTitle() override { return "Console"; };
    void export_state() override;

private slots:
    void addFilter(FilterWidget* filter);
    void addDevices();
    void process_CAN_message(CANmessage msg) override;
    void on_consoleclear_clicked(void);
    void on_sendbutton_clicked(void);
    void on_pushButton_5_clicked();
    void on_show_ids_clicked();
    void send_start_log();
    void set_id_value(int id);
    void on_AddMessageFilterButton_clicked();
    void on_make_id_clicked();
    void on_var_type_currentIndexChanged(const QString& arg1);
    void update();
    void update_LED();
    void updateTelemetryLed();
    void parseTelemetry(CANmessage message);
    void on_DeviceBox_currentTextChanged(const QString& arg1);
    void on_addFcpButton_clicked();
    void removeFilter(FilterWidget* filter);
    void updateMessageBox(QString currentText, QComboBox* box, QString type);
    void on_DeviceChoice_currentTextChanged(const QString& arg1);
    void updateFCPInfo(QString messageType, QString componentName, QString type);
    void on_JsonComponentNameBox_currentTextChanged(const QString& arg1);
    void on_MessageTypeBox_currentTextChanged();
    void clearFcpInfo();
    QString clearComboBoxInput(QString toClean);
    uint64_t findFCPInfo(QString info);
    void on_sendFCPMessage_clicked();
    void on_adcToggleButton_toggled(bool checked);
    void processLogMessage(float, CANmessage msg);
    void redirectCanMessage(CANmessage msg);
    void on_sigGeneratorStartInput_editingFinished();
    void on_signalGeneratorEndInput_editingFinished();
    void on_signalGeneratorStepInput_editingFinished();
    void on_signalGeneratorDelayInput_editingFinished();
    void on_SigGeneratorDeviceBox_currentIndexChanged(const QString& arg1);
    void on_SigGeneratorMessageBox_currentIndexChanged(const QString& arg1);
    void on_startSigGen_clicked();
    void on_stopSigGen_clicked();
    void on_resetSigGen_clicked();
    void handleNewCanSigGeneratorSample(CANmessage message);
    void on_signalExpressionInput_editingFinished();

    void on_log_button_clicked(bool checked);

    void on_loadState_clicked();

    void on_TabWidget_currentChanged(int index);

private:
    Ui::Console* ui;
    FcpDisplay* fcpdisplay_widget;
    bool filter_message(CANmessage msg);
    void write_to_console(CANmessage msg);
    QVector<FilterWidget*> filter_vector;
    QMap<QString, FCPSignalDisplayValue*> _fcpDisplayMap;
    bool no_output;
    MakeID make_id;
    QDateTime time;
    QTimer* telemetry_timer;
    QTextStream* in;
    QString var_type;
    QString messages;
    QTimer* LED_timer;
    int _activeTelemetry = 0;
    int _telemetryError = 0;
    QList<nameValueInputTemplate*>* _fcpInputList;
    QMap<QString, Device*> _deviceMap;
    CanSignalGenerator* _canSigGen;
    QFile* log_file;
    QTextStream* log_out;
    bool logging = false;
    void log(CANmessage);
    QDateTime start;
    unsigned log_count = 0;
    ConfigManager* config_manager;
    QList<nameValueInputTemplate*> fcp_input_boxes;
    ConfigManagerDialog* config_dialog;
    int selected_tab;
};

#endif
