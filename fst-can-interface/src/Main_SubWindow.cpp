#include "Main_SubWindow.hpp"
#include "ui_Main_SubWindow.h"

SubWindow::SubWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::SubWindow)
{
    ui->setupUi(this);
}

SubWindow::~SubWindow()
{
    delete ui;
}
