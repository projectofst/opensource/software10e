#include "comselectline.hpp"
#include "COM_Manager.hpp"
#include "ui_comselectline.h"

#include <QComboBox>
#include <QDebug>
#include <QMessageBox>

COMSelectLine::COMSelectLine(QWidget* parent, Comsguy* comsguy)
    : QWidget(parent)
    , ui(new Ui::COMSelectLine)

{
    this->comsguy = comsguy;

    ui->setupUi(this);
    QList<QString> args = this->comsguy->Templates.keys();
    args.detach();
    ui->COM1->addItems(args);
    ui->COM2->addItems(args);

    int default_index;
    if ((default_index = ui->COM1->findText("Serial")) == -1) {
        default_index = 0;
    }

    ui->COM1->setCurrentIndex(default_index);

    if ((default_index = ui->COM2->findText("Interface@0")) == -1) {
        default_index = 0;
    }

    ui->COM2->setCurrentIndex(default_index);

    auto icon = QIcon(":/CustomIcons/CustomIcons/link.png");
    ui->connectButton->setIcon(icon);
    streaming_movie = new QMovie(":/animated_radio.gif");
    streaming_movie->setScaledSize(ui->streamingLabel->size());

    ui->COM2->setCurrentIndex(default_index);

    this->conn_state = true;
    this->down();
    ui->streamingLabel->setMovie(streaming_movie);
}

COM* COMSelectLine::get_com1()
{
    return this->targetcom1;
}

void COMSelectLine::delete_com()
{
    on_close_clicked();
}

COMSelectLine::~COMSelectLine()
{
    delete ui;
}

void COMSelectLine::on_close_clicked()
{
    emit remove_com(this->com_id);
    delete this;
}

void COMSelectLine::on_COM1_currentTextChanged(const QString& arg1)
{
    targetcom1 = NULL;
    if (comsguy->Templates.contains(arg1)) {
        targetcom1 = comsguy->Templates.value(arg1);

    } else {
        return;
    }
    ui->address1->clear();
    ui->address1->addItems(targetcom1->getOptions());
    return;
}

void COMSelectLine::on_address1_currentTextChanged(const QString& arg1)
{
    targetcom1->setId(arg1);
    return;
}

void COMSelectLine::on_COM2_currentTextChanged(const QString& arg1)
{
    targetcom2 = NULL;
    if (comsguy->Templates.contains(arg1)) {
        targetcom2 = comsguy->Templates.value(arg1);
    } else {
        qDebug() << "Failed to find the requested COM";
        return;
    }

    ui->address2->clear();
    ui->address2->addItems(targetcom2->getOptions());
    return;
}

void COMSelectLine::on_address2_currentTextChanged(const QString& arg1)
{
    if (targetcom2->protocol != "Interface") {
        targetcom2->setId(arg1);
    }
    return;
}

void COMSelectLine::connect_api(QString COM1, QString COM2, QString address1, QString address2, bool btn_is_checked)
{
    /* when we set the button state to false this slot runs again, so we need to
     * check who triggered the function: the user or the code
     */

    static bool visited = false;

    COM* targetcom1 = comsguy->Templates.value(COM1);
    COM* targetcom2 = comsguy->Templates.value(COM2);

    if (targetcom2->protocol == "Interface") {
        address2 = targetcom2->getId().replace("Interface@", "");
    }

    if (COM1 == "" || COM2 == "" || (COM1 == COM2)) {
        if (!visited) {
            visited = true;
            QMessageBox::information(this, tr("Interface"), tr("Failed to connect\nCheck your connection settings"));
            qDebug() << "Failed to connect";
            ui->connectButton->setChecked(false);
            return;
        }
        visited = false;
        return;
    }

    if (btn_is_checked) {
        comsguy->connectCOM(COM1, address1, COM2, address2);

        // Replace Template targets with the actual created COM targets
        targetcom1 = comsguy->getCOM(targetcom1->getId())[0];
        targetcom2 = comsguy->getCOM(targetcom2->getId())[0];

        connect(targetcom1, &COM::upstream_up, this, &COMSelectLine::up);
        connect(targetcom1, &COM::upstream_down, this, &COMSelectLine::down);

        if (targetcom1->status() == 0) {
            ui->connectButton->setChecked(false);
            return;
        }
        if (targetcom2->status() == 0) {
            ui->connectButton->setChecked(false);
            return;
        }
    } else {
        comsguy->disconnectCOM(COM1, address1, COM2, address2);
    }
}

void COMSelectLine::on_connectButton_toggled(bool checked)
{
    /* when we set the button state to false this slot runs again, so we need to
     * check who triggered the function: the user or the code
     */

    QString COM1 = ui->COM1->currentText();
    QString COM2 = ui->COM2->currentText();
    QString address1 = ui->address1->currentText();
    QString address2 = ui->address2->currentText();

    connect_api(COM1, COM2, address1, address2, checked);
}

void COMSelectLine::up()
{
    if (this->conn_state == false) {
        this->streaming_movie->jumpToFrame(0);
        this->streaming_movie->start();
        this->conn_state = true;
    }
}

void COMSelectLine::down()
{
    if (this->conn_state == true) {
        this->streaming_movie->stop();
        this->streaming_movie->jumpToFrame(0);
        this->conn_state = false;
    }
}

QMap<QString, QVariant> COMSelectLine::export_state()
{
    QMap<QString, QVariant> current_state;
    current_state["COM1"] = ui->COM1->currentIndex();
    current_state["COM2"] = ui->COM2->currentIndex();
    current_state["address1"] = ui->address1->currentText();
    current_state["address2"] = ui->address2->currentText();
    return current_state;
}

void COMSelectLine::auto_set(int COM1, int COM2, QString address1, QString address2, bool)
{
    ui->COM1->setCurrentIndex(COM1);
    ui->COM2->setCurrentIndex(COM2);
    ui->address1->setCurrentText(address1);
    ui->address2->setCurrentText(address2);
}

void COMSelectLine::setComId(int id)
{
    this->com_id = id;
}

int COMSelectLine::getComId()
{
    return this->com_id;
}
