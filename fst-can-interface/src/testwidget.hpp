#ifndef TESTWIDGET_HPP
#define TESTWIDGET_HPP

#include "CanSignalGenerator.hpp"
#include "UiWindow.hpp"
#include <QMap>
#include <QWidget>
#include <memory>

namespace Ui {
class TestWidget;
}

class TestWidget : public UiWindow {
    Q_OBJECT

public:
    explicit TestWidget(QWidget* parent = nullptr, FCPCom* fcp = nullptr);
    ~TestWidget();

    virtual void process_CAN_message(CANmessage);
    virtual void updateFCP(FCPCom*);

    CanSignalGenerator* sig;

private slots:
    void on_Button1_clicked();
    void on_Button2_clicked();
    void test(CANmessage msg);

private:
    Ui::TestWidget* ui;
};

#endif // TESTWIDGET_HPP
