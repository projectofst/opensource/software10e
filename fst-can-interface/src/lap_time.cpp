﻿#include "lap_time.hpp"
#include "ui_lap_time.h"

Lap_time::Lap_time(QWidget* parent, FCPCom* fcp, Helper*, ConfigManager*)
    : UiWindow(parent, fcp)
    , ui(new Ui::Lap_time)
{
    ui->setupUi(this);
    this->fcp = fcp;
    custom_lap = new Custom_Lap();
    connect(custom_lap, &Custom_Lap::lap_finished, this, &Lap_time::custom_lap_added);
    QPalette palette;
    first_time = 1;
    counting_lap = 0;
    number_of_laps = 1;
    cones = 0;
    driver_change_in_course = 0;
    number_runs = 0;

    lap_timer = new QElapsedTimer; /*init lap timer*/

    update_timer = new QTimer;
    connect(update_timer, &QTimer::timeout, this, &Lap_time::update_current_lap_time);
    update_timer->start(10);

    cones_map["Endurance"] = 2.0 * 1000;
    cones_map["Skidpad"] = 0.2 * 1000;
    cones_map["Autocross"] = 2.0 * 1000;

    OC["Endurance"] = 10.0 * 1000;
    OC["Autocross"] = 10.0 * 1000;

    palette.setColor(ui->testing_site_text->foregroundRole(), Qt::red);
    ui->testing_site_text->setPalette(palette);

    palette.setColor(ui->driver_text->foregroundRole(), Qt::red);
    ui->driver_text->setPalette(palette);

    palette.setColor(ui->date_text->foregroundRole(), Qt::red);
    ui->date_text->setPalette(palette);

    palette.setColor(ui->rec_resp_text->foregroundRole(), Qt::red);
    ui->rec_resp_text->setPalette(palette);

    palette.setColor(ui->track_type_text->foregroundRole(), Qt::red);
    ui->track_type_text->setPalette(palette);

    palette.setColor(ui->length_text->foregroundRole(), Qt::red);
    ui->length_text->setPalette(palette);

    palette.setColor(ui->time_text->foregroundRole(), Qt::red);
    ui->time_text->setPalette(palette);

    CANdata msg;

    ui->sectorCombobox->addItem("Sector:");
    for (int i = 0; i < 3; i++) {
        this->_lapSignalSocket.append(new UDPSocket(*(COM::encodeToByteArray<CANdata>(msg))));
        connect(this->_lapSignalSocket[i], &UDPSocket::new_messages, this, &Lap_time::recieveNewLapSignal);
        ui->sectorCombobox->addItem(QString::number(i));
    }
    connect(this, &Lap_time::newLap, this, &Lap_time::on_lap_button_clicked);
}

Lap_time::~Lap_time()
{
    foreach (auto socket, this->_lapSignalSocket) {
        delete socket;
    }
    delete this->lap_timer;
    delete ui;
}

void Lap_time::on_lap_button_clicked()
{
    if ((runs.size()) && (!runs[ui->all_runs->currentIndex()]->get_dnf_flag())) {
        qint64 time;
        Single_lap* new_lap;
        run_laps* current_run = runs[ui->all_runs->currentIndex()];

        if (driver_change_in_course) {
            time = lap_timer->elapsed();
            ui->output->append("Driver change complete: " + transform_time(time) + ".");
            lap_timer->restart();
            driver_change_in_course = 0;
            ui->current_lap->setStyleSheet("QLabel {color : black;}");
            return;
        }

        if (counting_lap == 0) {
            counting_lap = 1;
            lap_timer->start();
            if (ui->stop_button->isChecked())
                ui->stop_button->setChecked(0);
            return;
        } else {
            QString last_lap, lap_id;
            time = lap_timer->elapsed();
            last_lap = transform_time(time); /*Get the string corresponding to the time*/
            int lap = current_run->get_number_laps();
            lap++;

            lap_id = "Lap #" + QString::number(lap);
            new_lap = new Single_lap(nullptr, lap, lap_id, time, last_lap, cones);
            add_lap(new_lap);
        }
    } else {
        ui->output->append("Cant Add lap to DNF run.");
    }
}

void Lap_time::update_current_lap_time()
{
    if (counting_lap != 0) {
        qint64 time;
        QList<qint64> times;
        QString pretty_time;

        time = lap_timer->elapsed();
        times = format_time(time);

        pretty_time = transform_time(time);
        ui->current_lap->setText(pretty_time);
        if (driver_change_in_course) {
            if (time > 180000)
                ui->current_lap->setStyleSheet("QLabel {color : red; }");
            else {
                ui->current_lap->setStyleSheet("QLabel {color : green; }");
            }
        }
    }
    return;
}

QList<qint64> Lap_time::format_time(qint64 time)
{

    QList<qint64> times;
    qint64 miliseconds, seconds, minutes;

    miliseconds = time % 1000;
    time = qint64(time / 1000);
    seconds = time % 60;
    time = qint64(time / 60);
    minutes = time;

    times.append(minutes);
    times.append(seconds);
    times.append(miliseconds);

    return times;
}

QString Lap_time::transform_time(qint64 time)
{
    char pretty_time[32];
    QList<qint64> times;

    times = format_time(time);
    sprintf(pretty_time, "%02lld:%02lld.%03lld", times[0], times[1], times[2]);
    QString x = pretty_time; /*macacada*/
    return x;
}

void Lap_time::on_cone_button_clicked()
{
    if (!runs.empty()) {
        if (!runs[ui->all_runs->currentIndex()]->get_dnf_flag()) {
            cones++;
            ui->output->setTextColor(Qt::red);
            QString output = "One cone added to the lap, making it a total of " + QString::number(cones) + " cones.";
            ui->output->append(output);
            ui->output->setTextColor(Qt::black);
        }
    } else {
        ui->output->append("There aren't any runs to add Cones");
    }
}

void Lap_time::readjust()
{
    number_of_laps--;
    Single_lap* current_lap;

    int number_of_laps = runs[ui->all_runs->currentIndex()]->get_number_laps();
    run_laps* current_run = runs[ui->all_runs->currentIndex()];

    for (int i = 0; i < number_of_laps; i++) { /*Get to the lap that is out of order*/
        current_lap = current_run->lap_number(i);
        if (current_lap->get_lap_number() != i + 1) {
            for (; i < number_of_laps; i++) { /*Decrement lap after the onewe find*/
                current_lap = current_run->lap_number(i);
                current_lap->set_new_lap_number(i + 1);
            }
            check_laps();
            return;
        }
    }
    check_laps();
    return;
}

void Lap_time::check_laps()
{
    run_laps* current_run = runs[ui->all_runs->currentIndex()];
    Single_lap *current_lap, *best_lap;
    if (current_run->get_number_laps()) {
        best_lap = current_run->lap_number(0);
        ui->best_lap_insert->setText(best_lap->get_time_string());
        for (int i = 1; i < current_run->get_number_laps(); i++) {
            current_lap = runs[ui->all_runs->currentIndex()]->lap_number(i);
            if (current_lap->get_time() < best_lap->get_time()) {
                current_run->set_new_fastest_lap(current_lap);
                ui->best_lap_insert->setText(best_lap->get_time_string());
            }
        }
    } else {
        current_run->set_new_fastest_lap(nullptr);
        ui->best_lap_insert->setText("--:--.---");
        ui->diff_insert->setText("--:--.---");
        first_time = 1;
    }
    return;
}

void Lap_time::on_DNF_Button_clicked()
{
    if (!runs.empty()) {
        int lap = runs[ui->all_runs->currentIndex()]->get_number_laps();
        Single_lap* new_lap = new Single_lap(nullptr, lap);
        new_lap->set_DNF_lap();

        connect(new_lap->ui->delete_button, &QPushButton::clicked, this, [this, new_lap]() {
            new_lap->get_DNF_flag() ? this->runs[ui->all_runs->currentIndex()]->set_DNF_flag(0) : this->runs[ui->all_runs->currentIndex()]->set_DNF_flag(0);
            this->runs[ui->all_runs->currentIndex()]->remove_lap(new_lap->get_lap_number());
            delete new_lap;
            this->readjust();
        });
        runs[ui->all_runs->currentIndex()]->add_lap(new_lap);
    } else {
        ui->output->append("There aren't any runs to add DNF");
    }
}

void Lap_time::keyPressEvent(QKeyEvent* event)
{
    /*Hot Keys*/
    switch (event->key()) {
    case Qt::Key_L: {
        on_lap_button_clicked();
        break;
    }
    case Qt::Key_C: {
        on_cone_button_clicked();
        break;
    }
    case Qt::Key_S: {
        ui->stop_button->setChecked(!ui->stop_button->isChecked());
        on_stop_button_toggled(ui->stop_button->isChecked());
        break;
    }
    case Qt::Key_1: {
        ui->all_runs->setCurrentIndex(0);
        break;
    }
    case Qt::Key_2: {
        ui->all_runs->setCurrentIndex(1);
        break;
    }
    case Qt::Key_3: {
        ui->all_runs->setCurrentIndex(2);
        break;
    }
    case Qt::Key_4: {
        ui->all_runs->setCurrentIndex(3);
        break;
    }
    case Qt::Key_5: {
        ui->all_runs->setCurrentIndex(4);
        break;
    }
    case Qt::Key_6: {
        ui->all_runs->setCurrentIndex(5);
        break;
    }
    case Qt::Key_7: {
        ui->all_runs->setCurrentIndex(6);
        break;
    }
    case Qt::Key_8: {
        ui->all_runs->setCurrentIndex(7);
        break;
    }
    case Qt::Key_9: {
        ui->all_runs->setCurrentIndex(8);
        break;
    }
    case Qt::Key_Space: {
        on_DC_button_clicked();
        break;
    }
    case Qt::Key_N: {
        on_new_run_clicked();
        break;
    }
    case Qt::Key_D: {
        delete_last();
        break;
    }
    case Qt::Key_U: {
        on_update_button_clicked();
        break;
    }
    }
    return;
}

void Lap_time::on_stop_button_toggled(bool checked)
{
    if (checked)
        counting_lap = !checked;
    else {
        lap_timer->restart();
        counting_lap = 1;
    }
    cones = 0;
    return;
}

int Lap_time::check_all_data()
{
    return ui->track_type_dropdown->currentIndex() != 0 && (ui->track_length_input->text() != "") && (ui->driver_input->text() != "") && (ui->responsible_input->text() != "");
}

void Lap_time::on_DC_button_clicked()
{
    on_lap_button_clicked();
    driver_change_in_course ^= 1;
    if (!driver_change_in_course)
        ui->current_lap->setStyleSheet("QLabel {color : black; }");
    return;
}

void Lap_time::on_export_button_clicked()
{

    if (check_all_data()) {
        QString filePath = QFileDialog::getSaveFileName(this,
            tr("Save"), QDir::currentPath(), tr("CSV files (*.csv)"));
        QString actual_file_path;

        QFile file(filePath);
        if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream stream(&file);
            stream << "#Track Type, Track Length, Driver(s), Date, Recording Responsible, \n";
            stream << ui->track_type_dropdown->currentText() << ","
                   << ui->track_length_input->text() << ","
                   << ui->driver_input->text() << ","
                   << ui->date_input->text() << ","
                   << ui->site_input->text() << ","
                   << ui->time_input->text() << ","
                   << ui->responsible_input->text() << "\n";
            stream << "#Lap Number, Time, cones, time with penalties\n";
            for (int k = 0; k < runs.size(); k++) {
                stream << "#Run " + QString::number(k) + "\n";
                for (int i = 0; i < runs[k]->get_number_laps(); ++i) {
                    QString pen_time_string;
                    if (runs[k]->get_dnf_flag()) {
                        stream << "DNF";
                    } else {
                        qint64 pen_time = runs[k]->lap_number(i)->get_time();
                        pen_time += runs[k]->lap_number(i)->get_cones() * cones_map[ui->track_type_dropdown->currentText()];
                        pen_time_string = transform_time(pen_time);
                        stream << runs[k]->lap_number(i)->get_lap_number() << "," << runs[k]->lap_number(i)->get_time_string() << "," << runs[k]->lap_number(i)->get_cones() << "," << pen_time_string << ","
                                                                                                                                                                                                          "\n";
                    }
                }
                stream.flush();
            }
            file.close();
        } else {
            ui->output->append("Could not open file");
        }
    } else {
        ui->output->append("Not all parameters are set");
    }
}

void Lap_time::on_reset_all_button_clicked()
{
    ui->output->clear();
}

void Lap_time::on_new_run_clicked()
{
    number_runs++;
    run_laps* new_run = new run_laps(this, ui->track_length_input->text().toInt(), ui->track_type_dropdown->currentText());
    ui->all_runs->addTab(new_run, "Run " + QString::number(number_runs));
    runs.append(new_run);
    return;
}

void Lap_time::delete_last()
{
    run_laps* current_run = runs[ui->all_runs->currentIndex()];
    if (current_run->get_number_laps())
        current_run->lap_number(current_run->get_number_laps() - 1)->press_del(); /* remove last lap of current run*/
    return;
}

void Lap_time::on_runs_currentChanged(int index)
{
    Single_lap* best_lap = runs[index]->get_fastest_lap();
    if (best_lap != nullptr)
        ui->best_lap_insert->setText(transform_time(best_lap->get_time()));
    else {
        ui->best_lap_insert->setText("--:--.---");
        ui->diff_insert->setText("--:--.---");
    }
}

void Lap_time::on_update_button_clicked()
{
    if (runs.size()) {
        run_laps* current_run = runs[ui->all_runs->currentIndex()];
        current_run->set_track(ui->track_type_dropdown->currentText());
        current_run->set_length(ui->track_length_input->text().toInt());
    }
}

void Lap_time::on_del_run_clicked()
{
    number_runs--;
    int index = ui->all_runs->currentIndex();
    runs[ui->all_runs->currentIndex()]->on_clear_clicked();
    delete runs[ui->all_runs->currentIndex()];
    runs.remove(index);
    for (int i = index; i < number_runs; i++) {
        ui->all_runs->setTabText(i, "Run " + QString::number(i + 1));
    }
    return;
}

void Lap_time::on_custom_lap_clicked()
{
    custom_lap->show();
}

void Lap_time::on_site_input_textEdited(const QString& arg1)
{
    QPalette palette;
    if (arg1 == "") {
        palette.setColor(ui->testing_site_text->foregroundRole(), Qt::red);
        ui->testing_site_text->setPalette(palette);
    } else {
        palette.setColor(ui->testing_site_text->foregroundRole(), Qt::green);
        ui->testing_site_text->setPalette(palette);
    }
}

void Lap_time::on_track_type_dropdown_currentIndexChanged(const QString& arg1)
{
    QPalette palette;
    if (arg1 == "") {
        palette.setColor(ui->track_type_text->foregroundRole(), Qt::red);
        ui->track_type_text->setPalette(palette);
    } else {
        palette.setColor(ui->track_type_text->foregroundRole(), Qt::green);
        ui->track_type_text->setPalette(palette);
    }
}

void Lap_time::on_track_length_input_textEdited(const QString& arg1)
{
    QPalette palette;
    if (arg1 == "") {
        palette.setColor(ui->length_text->foregroundRole(), Qt::red);
        ui->length_text->setPalette(palette);
    } else {
        palette.setColor(ui->length_text->foregroundRole(), Qt::green);
        ui->length_text->setPalette(palette);
    }
}

void Lap_time::on_driver_input_textEdited(const QString& arg1)
{
    QPalette palette;
    if (arg1 == "") {
        palette.setColor(ui->driver_text->foregroundRole(), Qt::red);
        ui->driver_text->setPalette(palette);
    } else {
        palette.setColor(ui->driver_text->foregroundRole(), Qt::green);
        ui->driver_text->setPalette(palette);
    }
}

void Lap_time::on_date_input_textEdited(const QString& arg1)
{
    QPalette palette;
    if (arg1 == "") {
        palette.setColor(ui->date_text->foregroundRole(), Qt::red);
        ui->date_text->setPalette(palette);
    } else {
        palette.setColor(ui->date_text->foregroundRole(), Qt::green);
        ui->date_text->setPalette(palette);
    }
}

void Lap_time::on_time_input_textEdited(const QString& arg1)
{
    QPalette palette;
    if (arg1 == "") {
        palette.setColor(ui->time_text->foregroundRole(), Qt::red);
        ui->time_text->setPalette(palette);
    } else {
        palette.setColor(ui->time_text->foregroundRole(), Qt::green);
        ui->time_text->setPalette(palette);
    }
}

void Lap_time::on_responsible_input_textEdited(const QString& arg1)
{
    QPalette palette;
    if (arg1 == "") {
        palette.setColor(ui->rec_resp_text->foregroundRole(), Qt::red);
        ui->rec_resp_text->setPalette(palette);
    } else {
        palette.setColor(ui->rec_resp_text->foregroundRole(), Qt::green);
        ui->rec_resp_text->setPalette(palette);
    }
}

void Lap_time::custom_lap_added(qint64 time, int cones, int, int)
{
    run_laps* current_run = runs[ui->all_runs->currentIndex()];
    int laps = current_run->get_number_laps();
    laps++;
    Single_lap* custom_lap = new Single_lap(nullptr, laps, "Lap #" + QString::number(laps), time, transform_time(time), cones);
    add_lap(custom_lap);
}

void Lap_time::add_lap(Single_lap* new_lap)
{
    QString diff;

    run_laps* current_run = runs[ui->all_runs->currentIndex()];
    qint64 time = lap_timer->elapsed();

    QString last_lap = transform_time(time); /*Get the string corresponding to the time*/

    connect(new_lap->ui->delete_button, &QPushButton::clicked, this, [this, new_lap]() {
        new_lap->get_DNF_flag() ? this->runs[ui->all_runs->currentIndex()]->set_DNF_flag(0) : this->runs[ui->all_runs->currentIndex()]->set_DNF_flag(0);
        this->runs[ui->all_runs->currentIndex()]->remove_lap(new_lap->get_lap_number() - 1);
        delete new_lap;this->readjust(); });

    if (current_run->get_fastest_lap() != nullptr) { /*If a best lap exists*/
        if (time < current_run->get_fastest_lap()->get_time()) { /*check for best lap*/
            if (!first_time) {
                diff = transform_time(-(time - current_run->get_fastest_lap()->get_time()));
                ui->diff_insert->setText("- " + diff);
            }

            current_run->get_fastest_lap()->set_new_lap_time(time);
            ui->best_lap_insert->setText(last_lap);

            QString best_lap;
            best_lap = "New fastest lap posted with the time of " + last_lap + "s.";
            ui->output->setTextColor(Qt::darkGreen);
            ui->output->append(best_lap);
            ui->output->setTextColor(Qt::black);

        } else {
            diff = transform_time(time - current_run->get_fastest_lap()->get_time());
            ui->diff_insert->setText("+ " + diff);
        }
    } else {
        current_run->set_new_fastest_lap(new_lap);
    }
    ui->last_lap_insert->setText(last_lap);
    runs[ui->all_runs->currentIndex()]->add_lap(new_lap);
    QString output = "Lap number " + QString::number(number_of_laps) + " set with the time " + last_lap + "s.";

    number_of_laps++;
    cones = 0;
    first_time = 0;
    ui->output->append(output);
    lap_timer->restart();
}

void Lap_time::recieveNewLapSignal(QByteArray&)
{
    emit newLap();
}

void Lap_time::process_CAN_message(CANmessage)
{
}
void Lap_time::updateFCP(FCPCom* fcp)
{
    this->setFCP(fcp);
}

void Lap_time::on_connectButton_toggled(bool checked)
{
    if (checked) {
        QString info = this->ui->connectInput->text();
        this->_lapSignalSocket[ui->sectorCombobox->currentText().toInt()]->open(info);
    } else {
        this->_lapSignalSocket[ui->sectorCombobox->currentText().toInt()]->close();
    }
}

void Lap_time::on_sectorCombobox_currentIndexChanged(int index)
{
    if (index != 0) {
        this->ui->connectInput->setText("192.168.200.101:");
    } else {
        this->ui->connectInput->setText("");
    }
}
