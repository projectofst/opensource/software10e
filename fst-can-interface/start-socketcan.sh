# run this as root to start the socket can driver and bring the socket can interface up

#!/bin/bash

if [[ "$OSTYPE" == "linux-gnu" ]]; then
	modprobe can
	ip link set can0 type can bitrate 1000000
	ip link set up can0
	ip link set can1 type can bitrate 1000000
	ip link set up can1
else
	echo "not running of linux"
fi
