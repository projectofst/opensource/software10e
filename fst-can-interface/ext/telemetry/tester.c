#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <ctype.h>
#include <stdint.h>


#include "../can-ids/CAN_IDs.h"
#include "../can-ids/Devices/DCU_CAN.h"
#include "../can-ids/Devices/TE_CAN.h"



unsigned char data[13];

unsigned char CRC_calculate(unsigned char *data, unsigned int n)
{
	unsigned int i, j;
	unsigned char CRC = 0b01000001;
	unsigned char DIN, IN0, IN1, IN2;
	unsigned char aux;

	for(j=0;j<n;j++){
		aux = 0b10000000;
		for(i=0;i<8;i++){
			/* shift the whole byte */
			DIN = (data[j] & aux) >> (7-i);	/* isolates the ith MSB from byte on LSB position */
			IN0 = DIN ^ ((CRC & 0b10000000)>>7);	/* DIN XOR CRC[7] */
			IN1 = (CRC & 0b00000001) ^ IN0;			/* CRC[0] XOR IN0 */
			IN2 = ((CRC & 0b00000010)>>1) ^ IN0;	/* CRC[1] XOR IN0 */
			CRC = ((CRC << 1) & 0b11111000) | IN2<<2 | IN1<<1 | IN0;	/* shifts CRC 1 bit left and assigns 3 LSBs to IN2,IN1,IN0 */
			aux = aux >> 1;		/* shifts one bit from byte */
		}
	}
	return CRC;
}

/**********************************************************************
 * Name:	digestCAN
 * Args:	CANdata CANmessage
 * Return:	length of digesteed string
 * Desc:	Transforms CAN message into binnary string to be sent
 *			by UART
 **********************************************************************/
int digestCAN(CANdata CANmessage)
{
	data[0] = CANmessage.sid >> 8;
	data[1] = CANmessage.sid;
	data[2] = CANmessage.dlc;
	data[3] = 0;
	data[4] = 0;
	data[5] = 0;
	data[6] = 0;
	data[7] = 0;
	data[8] = 0;
	data[9] = 0;
	data[10] = 0;
	/* do NOT break!
	 * set only the relevant values */
	switch(CANmessage.dlc){
		case 8:
			data[10] = CANmessage.data[3];
		case 7:
			data[9] = CANmessage.data[3] >> 8;
		case 6:
			data[8] = CANmessage.data[2];
		case 5:
			data[7] = CANmessage.data[2] >> 8;
		case 4:
			data[6] = CANmessage.data[1];
		case 3:
			data[5] = CANmessage.data[1] >> 8;
		case 2:
			data[4] = CANmessage.data[0];
		case 1:
			data[3] = CANmessage.data[0] >> 8;
			/*case 0:
			  nothing to do*/
		default:
			break;
	}
	data[11] = CRC_calculate(data, 11);
	data[12] = data[11] ^ 255;
	return 13;
}

// https://stackoverflow.com/a/6947758
int set_interface_attribs(int fd, int speed, int parity)
{
	struct termios tty;
	memset(&tty, 0, sizeof tty);
	if (tcgetattr(fd, &tty) != 0) {
		perror("error from tcgetattr");
		return -1;
	}

	cfsetospeed(&tty, speed);
	cfsetispeed(&tty, speed);

	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8; // 8-bit chars
	// disable IGNBRK for mismatched speed tests; otherwise receive break
	// as \000 chars
	tty.c_iflag &= ~IGNBRK; // disable break processing
	tty.c_lflag = 0; // no signaling chars, no echo,
	// no canonical processing
	tty.c_oflag = 0; // no remapping, no delays
	tty.c_cc[VMIN]	= 0; // read doesn't block
	tty.c_cc[VTIME] = 5; // 0.5 seconds read timeout

	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

	tty.c_cflag |= (CLOCAL | CREAD); // ignore modem controls,
	// enable reading
	tty.c_cflag &= ~(PARENB | PARODD); // shut off parity
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;

	if (tcsetattr(fd, TCSANOW, &tty) != 0) {
		perror("error from tcsetattr");
		return -1;
	}
	return 0;
}

void set_blocking(int fd, int should_block)
{
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (fd, &tty) != 0)
	{
		perror("error from tggetattr");
		return;
	}

	tty.c_cc[VMIN]	= should_block ? 1 : 0;
	tty.c_cc[VTIME] = 5; // 0.5 seconds read timeout

	if (tcsetattr (fd, TCSANOW, &tty) != 0)
		perror("error setting term attributes");
}

void help(void)
{
	printf("Usage tester -p [port] -f [file]\n");
}

void set_message(CANdata *msg, unsigned dev_id,  unsigned msg_id, unsigned dlc, 
		unsigned data0, unsigned data1, unsigned data2, unsigned data3) 
{
	msg->sid = 0;
	msg->dev_id = dev_id;
	msg->msg_id = msg_id;
	msg->dlc = dlc;
	msg->data[0] = data0;
	msg->data[1] = data1;
	msg->data[2] = data2;
	msg->data[3] = data3;
}

void send_message(CANdata *msg, int fd) {
	puts("[+] Sending...");
	printf("sid: %#0x, msg_id: %#0x, dev_id: %#0x\n", msg->sid, msg->msg_id, msg->dev_id);
	printf("dlc: %d\n", msg->dlc);
	printf("data[0]: %#0x\n", msg->data[0]);
	printf("data[1]: %#0x\n", msg->data[1]);
	printf("data[2]: %#0x\n", msg->data[2]);
	printf("data[3]: %#0x\n", msg->data[3]);
	digestCAN(*msg);

	write(fd, data, 13);
	usleep(100000);
}

void example_message(int fd) 
{
	CANdata msg;
	set_message(&msg, DEVICE_ID_DCU, MSG_ID_DCU_STATUS, 8, 0xFEFE, 0xABBA, 0xDEAD, 0xBEEF);
	send_message(&msg, fd);
}

void send_file(FILE *fp, int fd)
{
	char buffer[256];
	unsigned dev_id, msg_id, dlc, data0, data1, data2, data3;
	CANdata msg;
	while (fgets(buffer, 256, fp)) {
		printf("%s", buffer);
		char *aux = buffer;
		while (*aux == ' ' || *aux == '\t') aux++;
		if (*aux == '#') {
			continue;
		}
		sscanf(buffer, "%d,%d,%d,%d,%d,%d,%d", &dev_id, &msg_id, &dlc,
				&data0, &data1, &data2, &data3);
		set_message(&msg, dev_id, msg_id, dlc, data0, data1, data2, data3);
		send_message(&msg, fd);
	}
}


int main(int argc, char *argv[])
{
	int c;
	char *portname = NULL;
	char *filename = NULL;
	CANdata message;
	
	if (argc < 2) {
		help();
		return 0;
	}

	while ((c = getopt (argc, argv, "p:f:h")) != -1) {
		printf("option: %c, %s\n",c,optarg);
		switch (c) {
			case 'p':
				portname = optarg;
				break;
			case 'f':
				filename = optarg;
				break;
			default:
				help();
				return 0;
		}
	}

	printf("Trying to open port %s\n", portname);
	int fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0) {
		perror("Error opening port");
		exit(errno);
	}

	set_interface_attribs(fd, B9600, 0); // set baud rate to 1M, 8n1 (no parity)
	set_blocking(fd, 0); // set no blocking

	if (filename == NULL) {
		example_message(fd);
		return 0;
	}

	FILE *fp = fopen(filename, "r");
	if (fp == NULL) {
		printf("Error opening file %s", filename);
		return 1;
	}
	send_file(fp, fd);
}
