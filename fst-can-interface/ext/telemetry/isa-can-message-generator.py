import datetime
from random import *


time = datetime.datetime.now()


file = open("/home/dferrao/Desktop/isa.log", "w")
timestamp = 1
for k in range(20000):
    if k % 10 != 0:
        message_Canid = str(hex(randint(1313, 1320)))
        message_dlc = "0x006"
        message_data0 = "0x000"
        message_data1 = str(hex(randint(0, 65535)))
        message_data2 = str(hex(randint(0, 65535)))
        message_data3 = str(hex(randint(0, 65535)))
        timestamp += 1
        line = (
            message_Canid
            + ","
            + message_dlc
            + ","
            + message_data0
            + ","
            + message_data1
            + ","
            + message_data2
            + ","
            + message_data3
            + ","
            + str(hex(timestamp))
            + "\n"
        )
        file.write(line)
    else:
        message_Canid = "0x511"
        message_dlc = "0x000"
        message_data0 = str(hex((randint(128, 130) << 8) + randint(33, 42)))
        message_data1 = str(hex(randint(0, 65535)))
        message_data2 = str(hex(randint(0, 65535)))
        message_data3 = str(hex(randint(0, 65535)))
        timestamp += 1
        line = (
            message_Canid
            + ","
            + message_dlc
            + ","
            + message_data0
            + ","
            + message_data1
            + ","
            + message_data2
            + ","
            + message_data3
            + ","
            + str(hex(timestamp))
            + "\n"
        )
        file.write(line)
