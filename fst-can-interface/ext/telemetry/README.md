# Readme

This program provides a loopback tester for the fst-can-interface.
It should be able to read CAN messages from a file and reproduced them in a serial port.

So far it only sends a sample message to port.

Using `socat -d -d pty,raw,echo=0 pty,raw,echo=0` will allow the creation of two ports on /dev/pts/
The `interface` must bind to one and the `tester` must bind to the other.


