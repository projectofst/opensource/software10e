import serial, sys
from time import sleep
import re
import socket

if len(sys.argv) != 2:
    print("Usage: ./tester.py port")
    exit()

UDP_PORT1 = 60000
UDP_PORT2 = 60001
port = sys.argv[1]


ser = serial.Serial(port)


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sock.bind(("", UDP_PORT1))
sock.setblocking(0)

send_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
send_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
send_sock.settimeout(0.2)
send_sock.bind(("", UDP_PORT2))
send_sock.setblocking(0)

while True:
    try:
        data, addr = sock.recvfrom(1024)
        print(data)
        ser.write(data)
    except Exception as e:
        pass

    if ser.inWaiting() > 1:
        msg = ser.readline()
        send_sock.sendto(msg, ("<broadcast>", UDP_PORT2))
