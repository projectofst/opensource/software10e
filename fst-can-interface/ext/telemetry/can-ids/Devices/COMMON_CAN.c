#include <stdbool.h>
#include <stdint.h>
#include "can-ids/CAN_IDs.h"
#include "COMMON_CAN.h"

void parse_can_common_RTD(CANdata message, COMMON_MSG_RTD_ON *RTD){
    RTD->step = (RTD_ON) ((message.data[0]) & 0b1111);

    return;
}

void parse_can_common_LOG(CANdata message, COMMON_LOG_MSG *LOG){
    LOG->type = (LOG_TYPE) message.data[0];

    return;
}

void parse_can_common_TS(CANdata message, COMMON_MSG_TS *TS){
    TS->step = (TS_STEP) message.data[0];

    return;
}

void parse_can_common_RTD_OFF(CANdata message, COMMON_MSG_RTD_OFF *rtd_off_msg){
    rtd_off_msg->step = (RTD_OFF) message.data[0];

    return;
}
