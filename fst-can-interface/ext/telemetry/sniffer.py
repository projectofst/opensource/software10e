import serial, sys
from time import sleep
import re

port = sys.argv[1]
file = sys.argv[2]

if len(sys.argv) != 3:
    print("Usage: ./tester.py port file")
    exit()

ser = serial.Serial(port)

data = [0 for i in range(4)]

with open(file) as f:
    while True:
        print("wainting for serial")
        if ser.in_waiting:
            print("There were things")
            read_line = ser.readline()
            print("Read things")
            print(read_line)

        print("There aren't things")
        line = f.readline()
        if "#" in line:
            continue

        if line == "":
            break

        (sid, dlc, *data) = (int(s) for s in line.split(",") if s.isdigit())

        s = str.encode(f"{sid},{dlc},{data[0]},{data[1]},{data[2]},{data[3]}\n")
        print(s)
        print("")
        ser.write(s)
        sleep(0.01)
