import datetime
import os
from random import *


time = datetime.datetime.now()


file = open(str(os.getcwd()) + "/isa.log", "w")
timestamp = 1
for k in range(20000):

    if k % 10 != 0:
        message_dev_id = "31"
        message_msg_id = "10"
        message_dlc = "6"
        message_data0 = "0"
        message_data1 = str((randint(0, 65535)))
        message_data2 = str((randint(0, 65535)))
        message_data3 = str((randint(0, 65535)))
        timestamp += 500
    else:
        message_dev_id = "9"
        message_msg_id = "10"
        message_dlc = "8"
        message_data0 = str(((randint(128, 130) << 8) + randint(33, 42)))
        message_data1 = str((randint(0, 65535)))
        message_data2 = str((randint(0, 65535)))
        message_data3 = str((randint(0, 65535)))
        timestamp += 500
    line = (
        message_dev_id
        + ","
        + message_msg_id
        + ","
        + message_dlc
        + ","
        + message_data0
        + ","
        + message_data1
        + ","
        + message_data2
        + ","
        + message_data3
        + ","
        + str((timestamp))
        + "\n"
    )
    file.write(line)
