/**********************************************************************
 *	 CAN IDs for FST 08e
 *
 *	 log:
 *	 ______________________________________________________________
 *	 2017 - Projecto FST Lisboa
 **********************************************************************/

#ifndef __CAN_IDS_H__
#define __CAN_IDS_H__

#include <stdint.h>

#ifndef _CANDATA
#define _CANDATA
typedef struct {
	union {
        struct {
			uint16_t dev_id:5; // Least significant
			// First bit of msg_id determines if msg is reserved or not.
			// 0 == reserved (higher priority) (0-31 decimal)
			uint16_t msg_id:6; // Most significant
		};
		uint16_t sid;
	};
	uint16_t dlc:4;
	uint16_t data[4];
} CANdata;
#endif

typedef struct {
	CANdata candata;
	uint32_t timestamp;
} CANmessage;

#define CAN_GET_MSG_ID(sid) ((sid & 0b0000011111100000) >> 5)
#define CAN_GET_DEV_ID(sid) (sid & 0b0000000000011111)

/* DEVICE IDs (0-31) */
#define DEVICE_ID_DCU				8
#define DEVICE_ID_TE				9
#define DEVICE_ID_DASH				10
#define DEVICE_ID_LOGGER			11
#define DEVICE_ID_STEERING_WHEEL	12
#define DEVICE_ID_ARM				13
#define DEVICE_ID_BMS_MASTER		14
#define DEVICE_ID_BMS_VERBOSE		15
#define DEVICE_ID_IIB				16
#define DEVICE_ID_TELEMETRY			17
#define DEVICE_ID_SUPOT_FRONT		18
#define DEVICE_ID_SUPOT_REAR		19
#define DEVICE_ID_AHRS				20
#define DEVICE_ID_IMU				21
#define DEVICE_ID_GPS				22
#define DEVICE_ID_WATER_TEMP		23
#define DEVICE_ID_TIRETEMP          24
#define DEVICE_ID_STEER				25
#define DEVICE_ID_INTERFACE			31


/* RESERVED MESSAGE IDS (0-31)
 * Place your unique high priority message IDs here */
// Digital Control Unit
// Torque Encoder
#define MSG_ID_TE_MAIN				10
#define MSG_ID_IIB_SPEED 			11

#define MSG_ID_MASTER_TS_STATE		12
#define MSG_ID_MASTER_IMD_ERROR		13
#define MSG_ID_MASTER_AMS_ERROR		14
#define MSG_ID_MASTER_STATUS		15


/* Common message ids */

//RTD ACTIVATION SEQUENCE
//pls fix nomenclatura 
#define CMD_ID_COMMON_RTD_ON		16
#define CMD_ID_COMMON_RTD_OFF       17  
#define CMD_ID_COMMON_TS 			18

#define MSG_ID_COMMON_RESET         29
#define MSG_ID_COMMON_TRAP          30
#define MSG_ID_COMMON_PARSE_ERROR   31


// LOG (TRAP/ERROR) (common to all devices)


#endif
