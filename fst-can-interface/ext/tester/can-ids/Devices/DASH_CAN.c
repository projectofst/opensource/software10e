#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "can-ids/CAN_IDs.h"
#include "DASH_CAN.h"


void parse_can_message_status(uint16_t data[4], DASH_MSG_status *status) {
	status->board_temp = data[0];
    status->SC         = data[1] & 1;
	status->LEDs       = data[1] >> 1;
}

void parse_can_dash(CANdata message, DASH_CAN_Data *data) {
	if (message.dev_id != DEVICE_ID_DASH) {
		/*FIXME: send info for error logging*/
        return;
	}

	switch (message.msg_id){
		case MSG_ID_DASH_STATUS:
			parse_can_message_status(message.data, &(data->status));
			break;
	}
}
