#include "SE_CAN.h"

void parse_can_SE(CANdata message, STEER_MSG_SIG *steer){
	if (message.dev_id != DEVICE_ID_STEER) {
		/*FIXME: send info for error logging*/
        return;
	}
	else {
		steer->time_stamp = message.data[0];
		steer->steer_encoder_sig = message.data[1];
	}
}
