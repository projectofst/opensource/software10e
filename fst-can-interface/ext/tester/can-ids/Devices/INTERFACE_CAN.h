#ifndef __INTERFACE_CAN_H__
#define __INTERFACE_CAN_H__

#include "can-ids/CAN_IDs.h"

#define CMD_ID_INTERFACE_DCU_TOGGLE            32
#define CMD_ID_INTERFACE_DEBUG_TOGGLE          33
#define CMD_ID_INTERFACE_SET_PEDAL_THRESHOLD   34


/*enums*/
typedef enum{
    FANS      = 0,
    PUMPS1    = 1,
    PUMPS2    = 2,
    CUA       = 3,
    CUB       = 4,
    SC        = 5,
    BLUE_TSAL = 6,
    DCDC_DRS  = 7,
}DCU_TOGGLE;

typedef enum{
    DEBUG_TOGGLE_TORQUE_ENCODER  = 0,
    DEBUG_TOGGLE_DCU             = 1,
    DEBUG_TOGGLE_DASH            = 2,
}DEBUG_TOGGLE;

typedef enum{
    BRAKE = 0,
    THROTTLE = 1,
}THRESHOLD_PEDAL;

typedef enum{
    LOWER = 0,
    UPPER = 1,
}THRESHOLD_BOUND;

/*message structs*/
typedef struct{
    THRESHOLD_PEDAL pedal;
    THRESHOLD_BOUND bound;
} INTERFACE_MSG_PEDAL_THRESHOLD;

typedef struct{
    DCU_TOGGLE code;

} INTERFACE_MSG_DCU_TOGGLE;

typedef struct{
    DEBUG_TOGGLE device;
} INTERFACE_MSG_DEBUG_TOGGLE;

// MAIN STRUCT
typedef struct {
    INTERFACE_MSG_DCU_TOGGLE dcu_toggle;
    INTERFACE_MSG_DEBUG_TOGGLE debug_toggle;
    INTERFACE_MSG_PEDAL_THRESHOLD pedal_threshold;

} INTERFACE_CAN_Data;

void parse_can_interface(CANdata message, INTERFACE_CAN_Data *data);
void parse_can_message_debug_toggle(uint16_t data0, INTERFACE_MSG_DEBUG_TOGGLE *debug_toggle);
void parse_can_message_dcu_toggle(uint16_t data[4], INTERFACE_MSG_DCU_TOGGLE *dcu_toggle);
void parse_can_message_pedal_threshold(uint16_t data0, INTERFACE_MSG_PEDAL_THRESHOLD *pedal_threshold);


#endif
