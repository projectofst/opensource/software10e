#include <stdbool.h>
#include <stdint.h>
#include "can-ids/CAN_IDs.h"
#include "INTERFACE_CAN.h"

#include <stdio.h>


void parse_can_message_dcu_toggle(uint16_t data[4], INTERFACE_MSG_DCU_TOGGLE *dcu_toggle)
{
    dcu_toggle->code = (DCU_TOGGLE) (data[0] & 0b1111);
}

void parse_can_message_debug_toggle(uint16_t data0, INTERFACE_MSG_DEBUG_TOGGLE *debug_toggle)
{
    debug_toggle->device = (DEBUG_TOGGLE) (data0 & 0b1111);
}


void parse_can_message_pedal_threshold(uint16_t data0, INTERFACE_MSG_PEDAL_THRESHOLD *pedal_threshold){
    if(data0 & 0x1)
        pedal_threshold->bound=UPPER;
    else
        pedal_threshold->bound=LOWER;

    if((data0 >> 1)& 0x1)
        pedal_threshold->pedal=THROTTLE;

    else pedal_threshold->pedal=BRAKE;

    return;
}

void parse_can_interface(CANdata message, INTERFACE_CAN_Data *data){
	if (message.dev_id != DEVICE_ID_INTERFACE) {
		/*FIXME: send info for error logging*/
        return;
	}

	switch (message.msg_id) {
        case CMD_ID_INTERFACE_DCU_TOGGLE:
			parse_can_message_dcu_toggle(message.data, &(data->dcu_toggle));
			break;
        case CMD_ID_INTERFACE_DEBUG_TOGGLE:
            parse_can_message_debug_toggle(message.data[0], &(data->debug_toggle));
            break;
        case CMD_ID_INTERFACE_SET_PEDAL_THRESHOLD:
            parse_can_message_pedal_threshold(message.data[0], &(data->pedal_threshold));
            break;
	}
}
