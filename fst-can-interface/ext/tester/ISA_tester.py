import serial, sys
from time import sleep
import re


if len(sys.argv) != 3:
    print("Usage: ./tester.py port file")
    exit()

port = sys.argv[1]
file = sys.argv[2]


ser = serial.Serial(port)

data = [0 for i in range(5)]

with open(file) as f:
    while True:
        line = f.readline()
        print(line)

        if "#" in line:
            continue

        if line == "":
            break

        print(list(int(s, 16) for s in line.split(",")))
        (sid, dlc, *data) = (int(s, 16) for s in line.split(","))
        s = str.encode(
            f"{sid},{dlc},{data[0]},{data[1]},{data[2]},{data[3]},{data[4]}\n"
        )
        print(s)
        print("")
        ser.write(s)
        sleep(0.1)
