import serial, sys
from time import sleep
import re


if len(sys.argv) != 2:
    print("Usage: ./reader.py port")
    exit()

port = sys.argv[1]


ser = serial.Serial(port)

data = [0 for i in range(5)]

while True:
    line = ser.readline()
    print(line)
