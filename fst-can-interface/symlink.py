import os
import os.path
import platform
from os import path


def createSymlinks():
    string = platform.system()

    if string == "Linux":
        if path.exists("../fst-can-interface/can-ids"):
            # This removes a symbolic link on python in tmp directory
            os.remove("../fst-can-interface/can-ids")
        src = "../lib/can-ids"
        dst = "can-ids"
        # This creates a symbolic link on python in tmp directory
        os.symlink(src, dst)

        if path.exists("../fst-can-interface/can-ids-spec"):
            # This removes a symbolic link on python in tmp directory
            os.remove("../fst-can-interface/can-ids-spec")
        src = "../lib/can-ids-spec"
        dst = "can-ids-spec"
        # This creates a symbolic link on python in tmp directory
        os.symlink(src, dst)

    if string == "Windows":
        if path.exists("../fst-can-interface/can-ids"):
            # This removes symbolic link
            os.system("rd can-ids ..\lib\can-ids")  # if not rd then try 'del' command
        # This creates a symbolic link on python in tmp directory
        os.system("mklink /D can-ids ..\lib\can-ids")
        if path.exists("../fst-can-interface/can-ids-spec"):
            # This removes symbolic link
            os.system(
                "rd can-ids ..\lib\can-ids-spec"
            )  # if not rd then try 'del' command
        # This creates a symbolic link on python in tmp directory
        os.system("mklink /D can-ids-spec ..\lib\can-ids-spec")


createSymlinks()
