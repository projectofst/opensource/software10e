/**
 ** This file is part of the CANinterface project.
 ** Copyright 2021 João Freitas joaj.freitas@gmail.com.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "COM_Loopback.h"

COM_Loopback::COM_Loopback()
{
    protocol = "Loopback";
    Status = 0;
}

int COM_Loopback::status(void)
{
    return Status;
}

bool COM_Loopback::open(const QString)
{
    Status = 1;
    return true;
}

void COM_Loopback::read(void)
{
    return;
}

QStringList COM_Loopback::getOptions(void)
{
    return {};
}

int COM_Loopback::send(QByteArray& array)
{
    CANmessage msg = COM::decodeFromByteArray<CANmessage>(array);

    emit new_messages(*(COM::encodeToByteArray<CANmessage>(msg)));
    return 0;
}

int COM_Loopback::close(void)
{
    Status = -1;
    return 0;
}
