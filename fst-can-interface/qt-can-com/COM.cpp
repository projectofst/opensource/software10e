#include "COM.hpp"

COM::COM()
{
    Status = 0;
}

int COM::status(void)
{
    return Status;
}

bool COM::open(const QString)
{
    Status = 1;
    return true;
}

int COM::close(void)
{
    Status = -1;
    delete this;
    return 0;
}

int COM::send(QByteArray&)
{
    return 0;
}

CANmessage COM::pop(void)
{
    CANmessage package;
    package.candata.dev_id = 0;
    package.candata.msg_id = 0;
    return package;
}

void COM::read(void)
{
    return;

}

QStringList COM::getOptions(void)
{
    return this->options;
}
void COM::addOption(QString option){
    options.append(option);
    return;
}

void COM::clearOptions(void){
    options.clear();
    return;
}

void COM::setId(QString address) {
    if (!addresses.empty()) {
        this->COMId = this->protocol + "-" + this->addresses.value(address);
    } else {
        this->COMId = this->protocol + "-ONLY";
    }
}

QString COM::getId() {
    return this->COMId;
}

void COM::createInstance() {
    this->connections_count++;
}

void COM::deleteInstance() {
    this->connections_count--;
}

int COM::getInstances() {
    return this->connections_count;
}


//Streaming controls

void COM::setStreamingSpeed(double) {
    return;
}

double COM::getStreamingSpeed(void) {
    return 1.0;
}

void COM::fast_forward(double) {
    return;
}

void COM::fast_backwards(double) {
    return;
}

void COM::play_pause() {
    return;
}
