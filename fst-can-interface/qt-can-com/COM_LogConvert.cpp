#include "COM_LogConvert.hpp"
#include "ui_COM_LogConvert.h"

#include <QFileDialog>
#include <QString>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "can-ids/CAN_IDs.h"

LogConverter::LogConverter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogConverter)
{
    ui->setupUi(this);

    this->setWindowTitle("Log Converter\n");
    this->setFixedHeight(300);
    this->setFixedWidth(700);
}

LogConverter::~LogConverter()
{
    delete ui;
}

void LogConverter::on_Inputfile_clicked()
{
   return;
}

void LogConverter::on_input_file_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
           tr("Open Log"), tr("Log (*.log)"));

    ui->input_text->setText(filename);
    this->input_file = filename;
}

void LogConverter::on_output_file_clicked()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save Log"),
                                tr(""), tr("Log (*.log)"));
    ui->output_text->setText(filename);
    this->output_file = filename;
}

void binary_to_can(QString input, QString output) {

    uint32_t year, month, day, hour, minute, second;

    unsigned int byte_count=0;

    /*CAN message structure*/
    CANmessage msg;
    msg.candata.sid = 0;
    msg.candata.dlc = 0;
    msg.candata.data[0]=0;
    msg.candata.data[1]=0;
    msg.candata.data[2]=0;
    msg.candata.data[3]=0;
    msg.timestamp = 0;

    FILE* file_in=fopen(input.toLatin1().data(), "r");

    unsigned int data;
    unsigned int reads=0;
    char string;

    char buffer[256];

    fgets(buffer, 255, (FILE*) file_in);
    sscanf(buffer, "*** NEW LOG. %u/%u/%u : %u:%u:%u ***", &day, &month, &year, &hour, &minute, &second);

    fgets(buffer, 10, (FILE*) file_in);

    FILE *file_out = fopen(output.toLatin1().data(), "a");

    reads=fread(&string, sizeof(char), 1, file_in);
    if (strncmp(&string, "T", 1)==0){
        fprintf(file_out, "RESTART - NEW RUN\n");
        reads=0;
    }
    else {
        data=(unsigned int)string&0xFF;
        reads=fread(&string, sizeof(char), 1, file_in);
        data=(data<<8)|(string&0xFF);
    }

    while(reads!= 0){
        switch(byte_count){
            case 0:
                msg.candata.sid = data&0xFFFF;
                break;

            case 1:
                msg.candata.dlc = (data>>8)&0xFFFF;
                msg.candata.data[0] = (data<<8)&0xFFFF;
                break;

            case 2:
                msg.candata.data[0] |= (data>>8);
                msg.candata.data[1] = (data<<8)&0xFFFF;
                break;

            case 3:
                msg.candata.data[1] |= (data>>8);
                msg.candata.data[2] = (data<<8)&0xFFFF;
            break;

            case 4:
                msg.candata.data[2] |= (data>>8);
                msg.candata.data[3] = (data<<8)&0xFFFF;
            break;

            case 5:
                msg.candata.data[3] |= (data>>8);
                msg.timestamp = (data)&0xFF;
            break;

        case 6:
                msg.timestamp |= data & 0xFF00;
                msg.timestamp |= (data << 16) & 0xFF0000;
            break;
    }

        if (byte_count == 6){
            byte_count=0;
            fprintf(file_out, "%u,%u,%u,%u,%u,%u,%lu\n",msg.candata.sid, msg.candata.dlc, msg.candata.data[0], msg.candata.data[1],
                    msg.candata.data[2], msg.candata.data[3], msg.timestamp);

        }
        else {
            byte_count++;
        }

        reads=fread(&string, sizeof(char), 1, file_in);
        data=(unsigned int)string&0xFF;
        reads=fread(&string, sizeof(char), 1, file_in);
        data=(data<<8)|(string&0xFF);
    }
    fclose(file_in);
    fclose(file_out);
}

void LogConverter::on_convert_clicked()
{
    binary_to_can(this->input_file, this->output_file);
}


