#include "COM_UdpSendSock.hpp"

UdpSendSock::UdpSendSock()
{
    datagram = new char[256];
    count = 0;
    return;
}

bool UdpSendSock::open(QString Add)
{
    udpsocket = new QUdpSocket;
    IP4Address = Add.section(":",0,0);
    Port = Add.section(":",1,1).toUShort();
    return true;
}

int UdpSendSock::send(QByteArray& message)
{
    CANmessage msg = COM::decodeFromByteArray<CANmessage>(message);
    char canid[16];
    sprintf(canid,"%d",msg.candata.sid);
    char payload[16];
    sprintf(payload,"%d",msg.candata.sid);
    datagram = QByteArray(payload);
    datagram.prepend(canid,2);
    udpsocket->writeDatagram(datagram, QHostAddress(IP4Address), Port);
    datagram.clear();
    return 1;
}

int UdpSendSock::close()
{
    udpsocket->~QUdpSocket();
    delete this;
    emit closing();
    return 1;
}
