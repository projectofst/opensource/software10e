#ifndef UDPSocket_HPP
#define UDPSocket_HPP

#include "COM.hpp"
#include <sys/time.h>
#include <QTimer>

#include <QHostAddress>

class UDPSocket : public COM
{
    Q_OBJECT

public:

    /**
     * @brief UDPSocket - Constructor. Constructs a UDP Socket.
     * @param presenceData - QByteArray containing the message you want to send periodically.
     * @param freuquency - Frequency at which the presenceData will be sent. Default = 500ms.
     */
    UDPSocket(QByteArray presenceData, uint freuquency);

    /**
     * @brief UDPSocket - Constructor. This is an overloaded contstructor
     * @param presenceData - QByteArray containing the message you want to send periodically
     */
    UDPSocket(QByteArray presenceData);

    /**
     * @brief UDPSocket - Constructor
     */
    UDPSocket();
    int StatusInt;

    void changeOffsetBoolStatus();

    template<typename T>  void setPresenceMessage(T data){
        this->_presenceData = *(COM::encodeToByteArray<T>(data));
    }

public slots:

    /**
     * @brief open - Opens a connection with the details given in
     * @param details - Connetion details.
     * @return a boolean that represents whether the opening of the connection was successful.
     */
    virtual bool open(QString details) override;

    /**
     * @brief close - Closes the connection.
     * @return
     */
    virtual int close(void) override;

    /**
     * @brief send - Sends a package in the form of a ByteArray.
     * @return
     */
    virtual int send(QByteArray& ) override;

    /**
     * @brief read - Reads a all pending packages.
     */
    virtual void read(void) override;



private:
    uint _frequency = 500;
    QMap<QString, QString> addresses;
    QString _ip, _port;
    QUdpSocket * _udpSocket;
    QByteArray _packageData;
    QTimer* _presenceTimer;
    QByteArray _presenceData;
    QTimer *_watchdog_timer;
    QTimer *updateMessageTime_Timer;
    unsigned long int offsetMessageTime;
    bool offsetMessageTime_update = false;

private slots:
    bool parseArgs(QString args);
    void sendPresence();
    void init();
    void watchdog();

signals:
};

#endif // UDPSocket_HPP

