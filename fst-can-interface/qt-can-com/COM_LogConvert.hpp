#ifndef LOGCONVERTER_H
#define LOGCONVERTER_H

#include <QWidget>
#include <QString>

namespace Ui {
class LogConverter;
}

class LogConverter : public QWidget
{
    Q_OBJECT

public:
    explicit LogConverter(QWidget *parent = 0);
    ~LogConverter();

private slots:
    void on_Inputfile_clicked();
    void on_input_file_clicked();
    void on_output_file_clicked();

    void on_convert_clicked();

private:
    Ui::LogConverter *ui;
    QString input_file;
    QString output_file;
};

#endif // LOGCONVERTER_H
