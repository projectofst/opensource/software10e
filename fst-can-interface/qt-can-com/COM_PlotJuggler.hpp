#ifndef PLOTJUGGLER_HPP
#define PLOTJUGGLER_HPP

#include "COM.hpp"
#include "fcpcom.hpp"
#include <QHostAddress>

class PlotJuggler : public COM
{
    Q_OBJECT

public:
    PlotJuggler(int sizeOfRead, QByteArray presenceData, FCPCom *fcp);
    PlotJuggler(int sizeOfRead);
    template<typename T>  void setPresenceMessage(T data){
        this->_presenceData = *(COM::encodeToByteArray<T>(data));
    }

public slots:

    virtual bool open(QString) override;
    virtual int close(void) override;
    virtual int send(QByteArray& ) override;
    virtual void read(void) override {};



private:
    QString _ip, _port;
    QUdpSocket * _udpSocket;
    QByteArray _packageData;
    QTimer* _presenceTimer;
    int _sizeOfRead;
    QByteArray _presenceData;
    FCPCom *fcp;
private slots:
    void sendPresence();
    void init();


signals:
};

#endif // UDPSocket_HPP

