/**
 ** This file is part of the CANinterface project.
 ** Copyright 2021 João Freitas joaj.freitas@gmail.com.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/



#include "com_socketcan.h"

#include <QString>
#include <QtSerialBus>


COM_SocketCAN::COM_SocketCAN()
{
    Status = 0;
    watchdog_timer = new QTimer();
    this->updateTime_timer = new QTimer();
    watchdog_timer->setSingleShot(true);
    connect(watchdog_timer, &QTimer::timeout, this, &COM_SocketCAN::watchdog);
    connect(this->updateTime_timer, &QTimer::timeout, this, &COM_SocketCAN::updateTimeBool);
}

void COM_SocketCAN::updateTimeBool()
{
    this->updateTime = !this->updateTime;
}

int COM_SocketCAN::status(void)
{
    return Status;
}

bool COM_SocketCAN::open(const QString arg)
{
    if (!QCanBus::instance()->plugins().contains(QStringLiteral("socketcan"))) {
        printf("Failed to find socket can\n");
        return false;
    }

    QString errorString;
    device = QCanBus::instance()->createDevice(
            QStringLiteral("socketcan"), arg, &errorString);

    if (!device) {
        QMessageBox msgBox;
        msgBox.setText(errorString);
        msgBox.exec();
    } else {
        device->connectDevice();
    }

    connect(device, &QCanBusDevice::framesReceived, this, &COM_SocketCAN::read);

    emit upstream_up();
    watchdog_timer->start(1000);
    Status = 1;
    return true;
}

void COM_SocketCAN::read(void)
{
    QCanBusFrame frame;
    struct timeval time1;

    while (device->framesAvailable()) {
        frame = device->readFrame();

        CANmessage msg;
        msg.candata.dlc = 8;
        msg.timestamp = frame.timeStamp().microSeconds()/100;
        if (!this->updateTime)
        {
            this->updateTime = true;
            this->ofsetTime = frame.timeStamp().microSeconds()/100;
            if (!this->updateTime_timer->isActive())
                this->updateTime_timer->start(100);
        }

        gettimeofday(&time1, NULL);
        msg.timestamp = frame.timeStamp().microSeconds()/100 - this->ofsetTime +((unsigned long long)time1.tv_sec * 1000 + (unsigned long long)time1.tv_usec / 1000);

        msg.candata.sid = frame.frameId();
        QByteArray payload = frame.payload();

        auto dlc = frame.payload().length();
        char *datas =  payload.data();


        for (int i=0; i<dlc/2; i++) {
            msg.candata.data[i] = (datas[2*i+1] << 8) + datas[2*i];
        }

        watchdog_timer->start(1000);
        emit upstream_up();
        emit new_messages(*(COM::encodeToByteArray<CANmessage>(msg)));
    }
    return;
}

QStringList COM_SocketCAN::getOptions(void)
{
    return {"vcan0", "vcan1", "vcan2", "vcan3", "can0", "can1", "can2", "can3"};
}

int COM_SocketCAN::send(QByteArray& array)
{
    CANmessage msg = COM::decodeFromByteArray<CANmessage>(array);


    emit new_messages(*(COM::encodeToByteArray<CANmessage>(msg)));
    QCanBusFrame frame;

    frame.setFrameId(msg.candata.dev_id + 32 * msg.candata.msg_id);
    QByteArray payload;

    for(int i=0; i < msg.candata.dlc/2; i++)
    {
        payload.append((const char*) (msg.candata.data + i), sizeof(uint16_t));
    }

    frame.setPayload(payload);

    device->writeFrame(frame);
    return 0;
}

int COM_SocketCAN::close(void)
{
    Status = -1;
    emit upstream_down();
    device->disconnectDevice();
    delete device;
    return 0;
}

void COM_SocketCAN::watchdog() {
    emit upstream_down();
    watchdog_timer->start(1000);
}

