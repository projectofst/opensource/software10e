/**
 ** This file is part of the CANinterface project.
 ** Copyright 2021 João Freitas joaj.freitas@gmail.com.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/


#ifndef COM_SOCKETCAN_H
#define COM_SOCKETCAN_H

#include <QObject>
#include <QCanBus>
#include <QTimer>
#include <sys/time.h>
#include "COM.hpp"

class COM_SocketCAN : public COM
{
    Q_OBJECT
public:
    COM_SocketCAN();
    int status(void);
    bool open(const QString);
    int close(void);
    void updateTimeBool();

    QStringList getOptions(void);
private:
    QCanBusDevice *device;
    QTimer *watchdog_timer;
    QTimer *updateTime_timer;
    bool updateTime = false;
    unsigned int ofsetTime = 0;
    uint64_t timeOffSet = 0;

private slots:
    void watchdog(void);

public slots:
    void read(void);
    int send(QByteArray& array);

};

#endif // COM_SOCKETCAN_H
