#include "UDPSocket.hpp"


UDPSocket::UDPSocket(QByteArray presenceData, uint frequency){
    protocol = "socketcan";
    this->_presenceData = presenceData;
    this->_frequency = frequency;
    this->init();
}

UDPSocket::UDPSocket(QByteArray presenceData){
    this->_presenceData = presenceData;
    this->init();
}

UDPSocket::UDPSocket()
{
    this->_presenceData = QByteArray();
    this->init();
}

void UDPSocket::init(){
    _watchdog_timer = new QTimer(this);
    _watchdog_timer->setSingleShot(true);
    connect(_watchdog_timer, &QTimer::timeout, this, &UDPSocket::watchdog);
    /*Adds the option to appear on dropdown*/
    addOption("FST10e");
    addOption("FST09");
    addOption("LocalHost");
    addOption("Remote Dash");

    protocol = "Network";

    /*If you find a better way of doing this please change*/
    addresses.insert("FST10e", "192.168.200.101:5001");
    addresses.insert("FST09", " 192.168.200.102:5001");
    addresses.insert("LocalHost", "127.0.0.1:5001");
    addresses.insert("Remote Dash", "192.168.2.180:5002");

    _udpSocket = new QUdpSocket(this);
    this->_presenceTimer = new QTimer();
    this->_presenceTimer->setInterval(this->_frequency);
    this->updateMessageTime_Timer = new QTimer();
    connect(this->updateMessageTime_Timer, &QTimer::timeout, this, &UDPSocket::changeOffsetBoolStatus);
    connect(this->_presenceTimer, &QTimer::timeout, this, &UDPSocket::sendPresence);
}

bool UDPSocket::open(QString details)
{
    if(!parseArgs(details)){
        return false;
    }


    /*Call the read method when there are messages ready to be read*/
    connect(_udpSocket,&QUdpSocket::readyRead, this,&UDPSocket::read);

    /* Basic internet socket things... */
    _udpSocket->connectToHost(_ip, _port.toUShort());


    /* FIXME:
     * I do not understand how these errors operate, so i dont check them
     * if you do understand pls fix this. At this state, a connection error
     * is NOT checked.
     */

    /*
    if(_tcpSocket->error() != 0){
        qDebug() << _tcpSocket->errorString();
        return false;
    }
    */

    /* These values were brainlessly added to the code because of what had been
     * written before. I do not know if they are even needed, probably are.
     */


    StatusInt = 1;
    this->Status = 1;

    this->_presenceTimer->start();

    emit upstream_up();
    _watchdog_timer->start(1000);
    return true;
}

void UDPSocket::read(void)
{
    _packageData.clear(); /*Just for good measure*/

    _packageData = _udpSocket->readAll(); /*Get the available info into a byte array*/
    struct timeval current_time2;

    char charData[sizeof(CANmessage)];
    QDataStream in(&_packageData, QIODevice::ReadOnly); /*Put it into a DataStream for sequential read*/
    CANmessage message;
    while(!in.atEnd()){
        _watchdog_timer->start(1000);
        emit upstream_up();
        /*Read with the size of a CANmessage (16) */
        in.readRawData(charData, sizeof(CANmessage));
        QByteArray array(QByteArray::fromRawData(charData, sizeof(CANmessage)));
        message = COM::decodeFromByteArray<CANmessage>(array);
        if (!this->offsetMessageTime_update)
        {
            this->offsetMessageTime_update = true;
            this->offsetMessageTime = message.timestamp;
            if (!this->updateMessageTime_Timer->isActive())
                this->updateMessageTime_Timer->start(10000);
        }
        gettimeofday(&current_time2, NULL);
        message.timestamp += ((unsigned long long)current_time2.tv_sec * 1000 + (unsigned long long)current_time2.tv_usec / 1000) - this->offsetMessageTime;
        emit new_messages(*(COM::encodeToByteArray<CANmessage>(message).get())); /*Tell the world a new message has arrived*/
    }
}

int UDPSocket::send(QByteArray &msg)
{
    _udpSocket->write(msg);
    return 1;
}

void UDPSocket::sendPresence(){
    this->send(this->_presenceData);
    return;
}
int UDPSocket::close()
{
    Status = -1;
     _udpSocket->disconnectFromHost();
     this->_presenceTimer->stop();
     emit upstream_down();
     return 0;
}

bool UDPSocket::parseArgs(QString args){

    /*Check if it is a known address*/
    if(addresses.contains(args)){
        args = addresses.value(args);
    }

    /*The user input should come in the form of <ip>:<port>*/
    QList<QString> argList = args.split(":");
    if(argList.size() != 2) return false;

    _ip = argList[0];
    _port = argList[1];

    return true;
}

void UDPSocket::watchdog() {
    _watchdog_timer->start(1000);
    emit upstream_down();
}

void UDPSocket::changeOffsetBoolStatus()
{
    this->offsetMessageTime_update = !this->offsetMessageTime_update;
}
