#ifndef LOGPLAY_H
#define LOGPLAY_H

#include "COM.hpp"

#include <QObject>
#include <QFile>
#include <QQueue>
#include <QFileDialog>

class LogPlay : public COM
{
        Q_OBJECT

public:
    LogPlay();
    virtual bool open(QString) override;
    virtual int close(void) override;
    virtual CANmessage pop(void) override;
    virtual int status() override {return _Status;}

private:
    int _Status;
    QFile *file;
    QTextStream *filestream;
    QTimer *replaytimer;
    QString templine;
    QQueue<CANmessage> pendmessages;
    uint64_t AbsTime;
    uint64_t OldTime;
    QHash<uint64_t,CANmessage> Messages;
    void next_message(void);

public slots:
    virtual void read(void) override;
    virtual int send(QByteArray& ) override;

signals:
};

#endif // LOGPLAY_H
