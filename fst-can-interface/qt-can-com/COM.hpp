#ifndef COM_H
#define COM_H

#include <QObject>
#include <QSerialPort>
#include <QTcpSocket>
#include <QTcpServer>
#include <QtNetwork>
#include <QNetworkSession>
#include <QTimer>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <queue>
#include <thread>
#include <mutex>
#include <QMutex>
#include <QUdpSocket>

#include "can-ids/CAN_IDs.h"

namespace Ui {
class COM;
}


class COM : public QObject
{
    Q_OBJECT

    /* some coms seem to do the work of a device (like the telemetry COM, UdpSocket)
     *
     * A better way to achieve this would be to define some kind of comunicator responsible
     * for such things as knowing the address of the interface.
     *
     * It could also be used to simplify FCP get/sets and cmds.
     *
     * See ftest and fcp python implementations for an example of this.
     */
public:
    COM();
    int Status; //COM status should use a QEnum instead of an int
    virtual bool open(QString);
    virtual int close(void);
    virtual int send(QByteArray&);
    virtual CANmessage pop(void); //needed ?
    virtual int status(void); // see above regarding int Status
    virtual QStringList getOptions(void);
    virtual void setId(QString address); // what is this?
    virtual QString getId(); // what is this?
    void createInstance();
    void deleteInstance();
    int getInstances();

    /* Stream controls */
    void setStreamingSpeed(double speed);
    double getStreamingSpeed(void);
    void fast_forward(double);
    void fast_backwards(double);
    void play_pause(void);

    template<typename T> static T decodeFromByteArray(QByteArray array){
        QDataStream in(&array, QIODevice::ReadOnly); /*Put it into a DataStream for sequential read*/

        char charData[sizeof(T)];
        T* message;
        in.readRawData(charData, sizeof(T));
        message = (T*) charData;
        /*Dont forget to free the string you allocced*/
        return *message;
    }

    template<typename T> static QList<T> decodeFromByteArrayToList(QByteArray& array){
        QDataStream in(&array, QIODevice::ReadOnly); /*Put it into a DataStream for sequential read*/
        QList<T> list;
        char* charData = (char*) malloc(sizeof(char) * sizeof (T));
        while(!in.atEnd()){
            T* message;
            in.readRawData(charData, sizeof(T));
            message = (T*) charData;
            /*Dont forget to free the string you allocced*/
            list.append(message);
        }
        free(charData);
        return list;

    }
    template<typename T> static std::shared_ptr<QByteArray> encodeToByteArray(T data){
        char * buffer = (char* )&data;
        /*get the message into an array of bytes*/
        return std::make_shared<QByteArray>(buffer, sizeof (T));
    }

    QMap<QString, QString> addresses;
    QString protocol = "unset";
    QString COMId = "unset";
    int connections_count = 0;

public slots:
    virtual void read(void);

private:
    QMap<QString,COM*> Interfaces;
    QMap<QString,COM*> Loggers;
    QStringList options;
protected:
    void addOption(QString option);
    void clearOptions(void);
signals:
    void new_messages(QByteArray&);
    void broadcast_in_message(CANmessage);
    void downstream_up(); //interface is up
    void downstream_down(); //insterface is down
    void upstream_up(); //interface is up
    void upstream_down(); //insterface is down
};

#endif // COM_H
