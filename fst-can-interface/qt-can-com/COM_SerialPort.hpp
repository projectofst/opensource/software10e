/**********************************************************************
 *   FST CAN tools --- interface
 *
 *   SerialPort class header
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/
#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QQueue>
#include <QTimer>
#include <QDateTime>

#include "COM.hpp"
#include "QSerialPortInfo"
#include "Exceptions/logutils.hpp"

namespace Ui {
    class SerialPort;
}

class SerialPort : public COM
{
	Q_OBJECT

private:
	int message_counter, sum;
	int _fd;
	QSerialPort serial;
	QMutex mutex;
	unsigned int _use_extID;
    QDateTime datetime;
    bool updateTime = false;
    QTimer *updateTime_timer;
    uint64_t offsetTime = 0;

	std::thread _worker;

    std::queue<CANmessage> _queue;
    QQueue<CANmessage> send_queue;
	QMutex _queueLkc;




	unsigned char _CRC_calculate(const unsigned char *data, const unsigned int index, const unsigned int buffersize, unsigned int n);
	int _CRC_verify(const unsigned char *data, const unsigned int index, const unsigned int buffersize, unsigned int n, const unsigned char CRC);

	unsigned int aux_buffer_size;
	unsigned int buffer_size;

	QByteArray data;

	unsigned char *buffer;
	char *aux_buffer;

	unsigned int W_caret;
	unsigned int R_caret;



public:
	SerialPort(void);
    virtual ~SerialPort(void) override;
    virtual int status() override {return StatusInt;}
        int StatusInt;

    int fd(void){return _fd;}

    virtual bool open(const QString portname) override;
    virtual int  close(void) override;
    virtual QStringList getOptions(void) override;
	int _writePort(const unsigned char *data, unsigned int n);

	void extended_ID_mode(void);
	void standard_ID_mode(void);

    void timeBoolChange();

    QByteArray pop_raw_data(void);

    void set_message_to_queue(CANmessage msg);
    virtual CANmessage pop(void) override;

    int debug_status;

signals:
    void send_to_log(CANmessage);
	void aborting(void);
    void new_raw(void);

public slots:
    virtual int send(QByteArray&) override;
	void handleError(QSerialPort::SerialPortError error);

    virtual void read(void) override;

private slots:
    void _send();
};


#endif
