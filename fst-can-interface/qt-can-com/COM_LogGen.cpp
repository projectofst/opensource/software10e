#include "COM_LogGen.hpp"
#include <math.h>

LogGenerator::LogGenerator()
{
    custDialog();
    addCustMsg(nullptr);

    MessageMS = new QTimer;

    StatusInt = 0;

    addOption("Random");
    addOption("Custom");
    //addOption("Last");
    timestamp = 0;
    contador = 0;

    protocol = "Generator";
}

bool LogGenerator::open(QString Mode)
{
    qDebug()<<Mode;
    if (Mode == "Random") {
        StatusInt = 1;
        clearOptions();
        addOption("Random");
        MessageMS->setInterval(1000/50);
        connect(MessageMS,SIGNAL(timeout()),this,SLOT(read()));
        MessageMS->start();
    } else if (Mode == "Custom") {
        Custom->exec();
        StatusInt = 2;
        clearOptions();
        addOption("Custom");
        MessageMS->setInterval(1000/50);
        connect(MessageMS,SIGNAL(timeout()),this,SLOT(read()));
        MessageMS->start();
    }
    return true;
}

int LogGenerator::close(void)
{
    MessageMS->stop();
    disconnect(MessageMS,SIGNAL(timeout()),
               this,SLOT(read()));
    StatusInt = 0;
    clearOptions();
    addOption("Random");
    addOption("Custom");
    return(0);
}

int LogGenerator::send(QByteArray& array)
{
    CANdataa *newmsg = new CANdataa;
    CANmessage Msg = COM::decodeFromByteArray<CANmessage>(array);
    newmsg->dev_id = Msg.candata.dev_id;
    newmsg->msg_id = Msg.candata.msg_id;
    newmsg->dlc = Msg.candata.dlc;
    newmsg->data[0] = Msg.candata.data[0];
    newmsg->data[1] = Msg.candata.data[1];
    newmsg->data[2] = Msg.candata.data[2];
    newmsg->data[3] = Msg.candata.data[3];
    addCustMsg(newmsg);
    return 0;
}

void LogGenerator::read(void)
{
    int TempDevID;
    int TempDlcID;
    int TempMsgID;
    int TempData0;
    int TempData1;
    int TempData2;
    int TempData3;

    foreach (CANdataa CustMsg, CustMsgList) {
        if ((StatusInt == 1) || ((StatusInt == 2) && (CustMsg.dev_id < 0))) {
            TempDevID = DataGen.bounded(0,99);
        } else {
            TempDevID = CustMsg.dev_id;
        }
        if ((StatusInt == 1) || ((StatusInt == 2) && (CustMsg.dev_id < 0))) {
            TempMsgID = DataGen.bounded(0,99);
        } else {
            TempMsgID = CustMsg.msg_id;
        }
        if ((StatusInt == 1) || ((StatusInt == 2) && (CustMsg.dlc < 0))) {
            TempDlcID = DataGen.bounded(0,99);
        } else {
            TempDlcID = CustMsg.dlc;
        }
        if ((StatusInt == 1) || ((StatusInt == 2) && (CustMsg.data[0] < 0))) {
            TempData0 = DataGen.bounded(0,99);
        } else {
            TempData0 = CustMsg.data[0];
        }
        if ((StatusInt == 1) || ((StatusInt == 2) && (CustMsg.data[1] < 0))) {
            TempData1 = DataGen.bounded(0,99);
        } else {
            TempData1 = CustMsg.data[1];
        }
        if ((StatusInt == 1) || ((StatusInt == 2) && (CustMsg.data[2] < 0))) {
            TempData2 = DataGen.bounded(0,99);
        } else {
            TempData2 = CustMsg.data[2];
        }
        if ((StatusInt == 1) || ((StatusInt == 2) && (CustMsg.data[3] < 0))) {
            TempData3 = DataGen.bounded(0,99);
        } else {
            TempData3 = CustMsg.data[3];
        }

        CANmessage Msg;
        Msg.candata.dev_id = static_cast<uint16_t>(TempDevID);
        Msg.candata.msg_id = static_cast<uint16_t>(TempMsgID);
        Msg.candata.dlc = static_cast<uint16_t>(TempDlcID);
        //Msg.candata.data[0] = static_cast<uint16_t>(TempData0);

        Msg.candata.data[0] = static_cast<uint16_t>(TempData0);
        Msg.candata.data[1] = static_cast<uint16_t>(TempData1);
        Msg.candata.data[2] = static_cast<uint16_t>(TempData2);
        Msg.candata.data[3] = static_cast<uint16_t>(TempData3);
        Msg.timestamp = timestamp;
        contador ++;
        if (contador == 100){
            contador = 12;
        }
        timestamp++;
        emit new_messages(*(COM::encodeToByteArray<CANmessage>(Msg).get()));
    }
}

void LogGenerator::custDialog(void)
{
    Custom = new QDialog(nullptr);
    Custom->setWindowTitle("Custom Message List");

    Custom->setModal(true);

    QVBoxLayout *Lay = new QVBoxLayout(Custom);
    QTableWidget *Table = new QTableWidget(Custom);
    Table->setObjectName("Table");
    Lay->addWidget(Table);

    Table->setColumnCount(7);

    QStringList Headers;
    Headers.append("DEVID");
    Headers.append("MSGID");
    Headers.append("DLC");
    Headers.append("DATA0");
    Headers.append("DATA1");
    Headers.append("DATA2");
    Headers.append("DATA3");

    Table->setHorizontalHeaderLabels(Headers);
    Table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    QHBoxLayout *SubLay = new QHBoxLayout;
    Lay->addLayout(SubLay);
    QPushButton *Generate = new QPushButton;
    Generate->setText("Generate");
    SubLay->addWidget(Generate);
    connect(Generate, &QPushButton::clicked,
            Custom, &QDialog::close);
    QPushButton *AddLine = new QPushButton;
    AddLine->setText("New");
    SubLay->addWidget(AddLine);
    connect(AddLine, SIGNAL(clicked()),
            this, SLOT(addCustMsg()));
    QPushButton *Delete = new QPushButton;
    Delete->setText("Delete All");
    SubLay->addWidget(Delete);
    connect(Delete, &QPushButton::clicked,
            this, &LogGenerator::clearCustMsgs);

    Custom->setLayout(Lay);

    connect(Table,SIGNAL(cellChanged(int,int)),
            this,SLOT(updateData(int,int)));
}

void LogGenerator::addCustMsg(CANdataa *msg)
{
    QTableWidget *Table = Custom->findChild<QTableWidget *>("Table");
    disconnect(Table,SIGNAL(cellChanged(int,int)),
               this,SLOT(updateData(int,int)));

    QTableWidgetItem *TempItem = new QTableWidgetItem;
    Table->clear();
    if (msg == nullptr)
        CustMsgList.append(*new CANdataa);
    else {
        CustMsgList.append(*msg);
        qDebug("looool");
    }
    Table->setRowCount(CustMsgList.count());

    for (int i = 0;i < CustMsgList.count();i++) {
        TempItem->setData(0,QVariant(CustMsgList.at(i).dev_id));
        Table->setItem(i,0,TempItem);
        TempItem = new QTableWidgetItem;
        TempItem->setData(0,QVariant(CustMsgList.at(i).msg_id));
        Table->setItem(i,1,TempItem);
        TempItem = new QTableWidgetItem;
        TempItem->setData(0,QVariant(CustMsgList.at(i).dlc));
        Table->setItem(i,2,TempItem);
        TempItem = new QTableWidgetItem;
        TempItem->setData(0,QVariant(CustMsgList.at(i).data[0]));
        Table->setItem(i,3,TempItem);
        TempItem = new QTableWidgetItem;
        TempItem->setData(0,QVariant(CustMsgList.at(i).data[1]));
        Table->setItem(i,4,TempItem);
        TempItem = new QTableWidgetItem;
        TempItem->setData(0,QVariant(CustMsgList.at(i).data[2]));
        Table->setItem(i,5,TempItem);
        TempItem = new QTableWidgetItem;
        TempItem->setData(0,QVariant(CustMsgList.at(i).data[3]));
        Table->setItem(i,6,TempItem);
        TempItem = new QTableWidgetItem;
    }

    QStringList Headers;
    Headers.append("DEVID");
    Headers.append("MSGID");
    Headers.append("DLC");
    Headers.append("DATA0");
    Headers.append("DATA1");
    Headers.append("DATA2");
    Headers.append("DATA3");

    Table->setHorizontalHeaderLabels(Headers);

    connect(Table,SIGNAL(cellChanged(int,int)),
            this,SLOT(updateData(int,int)));
}

void LogGenerator::clearCustMsgs(void)
{
    CustMsgList.clear();
    addCustMsg();
}

void LogGenerator::updateData(int row,int col)
{
    QTableWidget *Table = static_cast<QTableWidget*>(QObject::sender());
    CANdataa Msg;

    switch (col) {
    case 0:
        Msg = CustMsgList.takeAt(row);
        Msg.dev_id = Table->item(row,col)->data(0).toInt();
        CustMsgList.insert(row,Msg);
        break;
    case 1:
        Msg = CustMsgList.takeAt(row);
        Msg.msg_id = Table->item(row,col)->data(0).toInt();
        CustMsgList.insert(row,Msg);
        break;
    case 2:
        Msg = CustMsgList.takeAt(row);
        Msg.dlc = Table->item(row,col)->data(0).toInt();
        CustMsgList.insert(row,Msg);
        break;
    case 3:
        Msg = CustMsgList.takeAt(row);
        Msg.data[0] = Table->item(row,col)->data(0).toInt();
        CustMsgList.insert(row,Msg);
        break;
    case 4:
        Msg = CustMsgList.takeAt(row);
        Msg.data[1] = Table->item(row,col)->data(0).toInt();
        CustMsgList.insert(row,Msg);
        break;
    case 5:
        Msg = CustMsgList.takeAt(row);
        Msg.data[2] = Table->item(row,col)->data(0).toInt();
        CustMsgList.insert(row,Msg);
        break;
    case 6:
        Msg = CustMsgList.takeAt(row);
        Msg.data[3] = Table->item(row,col)->data(0).toInt();
        CustMsgList.insert(row,Msg);
        break;
    }
}
