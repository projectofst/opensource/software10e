#include "COM_PlotJuggler.hpp"
#include "json.hpp"

#include <iostream>
using json = nlohmann::json;

PlotJuggler::PlotJuggler(int sizeOfRead, QByteArray presenceData, FCPCom *fcp){
    this->_sizeOfRead = sizeOfRead;
    this->_presenceData = presenceData;
    this->fcp = fcp;
    this->init();
}

PlotJuggler::PlotJuggler(int sizeOfRead)
{
    this->_sizeOfRead = sizeOfRead;
    this->_presenceData = QByteArray();
    this->init();
}

void PlotJuggler::init() {
    _udpSocket = new QUdpSocket(this);
    this->_presenceTimer = new QTimer();
    this->_presenceTimer->setInterval(500);
    connect(this->_presenceTimer, &QTimer::timeout, this, &PlotJuggler::sendPresence);
    protocol = "PlotJuggler";
}

bool PlotJuggler::open(QString)
{

    _ip = "127.0.0.1";
    _port = "9870";

    /* Basic internet socket things... */

    _udpSocket->connectToHost(_ip, _port.toUShort());

    Status = 1;

    this->_presenceTimer->start();


    return true;
}

int PlotJuggler::send(QByteArray &msg)
{
    CANmessage temp;
    QMap<QString, double>::iterator i;
    json j;

    QPair<QString, QMap<QString, double>> decoded_signals;
    temp = COM::decodeFromByteArray<CANmessage>(msg);
    try {
        temp = COM::decodeFromByteArray<CANmessage>(msg);
        auto decoded_signals = fcp->decodeMessageMuxed(temp);
        for(i = decoded_signals.second.begin(); i != decoded_signals.second.end(); ++i) {
            j[i.key().toUtf8().constData()] = i.value();
        }
        std::string send_string = j.dump();
        const char *buffer = send_string.c_str();
        _udpSocket->write((const char *) buffer, strlen((const char *) buffer));
    } catch (...) {
        return 1;
    }
    /*
    std::string send_string = j.dump();
    const char *buffer = send_string.c_str();

    _udpSocket->write((const char *) buffer, strlen((const char *) buffer));

    */
    return 1;
}

void PlotJuggler::sendPresence(){
    this->send(this->_presenceData);
    return;
}
int PlotJuggler::close()
{
    _udpSocket->disconnectFromHost();
    this->_presenceTimer->stop();
    return 0;
}


