#ifndef UDPSENDSOCK_H
#define UDPSENDSOCK_H

#include "COM.hpp"

namespace Ui {
    class UdpSendSock;
}

class UdpSendSock : public COM
{
    Q_OBJECT

public:
    UdpSendSock();
    virtual bool open(QString) override;
    virtual int close(void) override;

public slots:
    virtual int send(QByteArray&) override;

private:
    QByteArray datagram;
    QUdpSocket *udpsocket;
    QString IP4Address;
    quint16 Port;
    int count;

signals:
    void closing(void);
};

#endif // UDPSENDSOCK_H
