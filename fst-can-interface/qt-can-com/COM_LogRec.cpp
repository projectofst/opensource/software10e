#include "COM_LogRec.hpp"

LogRec::LogRec()
{
    connect(&_logtimer, SIGNAL(timeout()), this, SLOT(flush()));
    StatusInt = 0;
    protocol = "LogRec";
}

bool LogRec::open(const QString)
{
    /*time = time.currentDateTime();
    if (filename.isEmpty()) {
        _logfile.setFileName(QDateTime::currentDateTime().toString());
    } else {
        _logfile.setFileName(filename);
    }

    if(_logfile.open(QIODevice::Append | QIODevice::Text)){

        StatusInt = 1;

        _logstream.setDevice(&_logfile);

        // set session delimiter (if file exists, it gets appended)
        _logstream << time.toString(QString("#########################################\n"));
        _logstream << time.toString(QString("# dddd, dd MMMM yyyy\n"));
        _logstream << time.toString(QString("# hh:mm:ss\n"));
        _logstream << time.toString(QString("#########################################\n"));

        _logtimer.start(LOG_FLUSH_timestamp);	// restart timer

        return true;
    } else {
        return false;
    }*/
    return 0;

}

int LogRec::close(void)
{
    _logtimer.stop();
    _logstream.flush();
    _logfile.close();

    StatusInt = 0;
    return 0;
}

int LogRec::send(QByteArray& data)
{
    char log_string[512];
    CANmessage msg = COM::decodeFromByteArray<CANmessage>(data);
    sprintf(log_string, "%u,%u,%u,%u,%u,%u", msg.candata.sid, msg.candata.dlc, msg.candata.data[0],
            msg.candata.data[1], msg.candata.data[2], msg.candata.data[3]); // msg.timestamp
    _logstream << log_string << "\n"; // Do not flush here!!! Computer would be bogged down flushing every message

    return 0;
}

void LogRec::read(void){}

void LogRec::flush(void)
{
    _logstream.flush();
    _logtimer.start( LOG_FLUSH_timestamp );	// restart timer
}
