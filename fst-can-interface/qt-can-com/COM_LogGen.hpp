#ifndef LOGGENERATOR_H
#define LOGGENERATOR_H

#include <QObject>
#include <QWidget>
#include <QMetaEnum>
#include <QTableWidgetItem>
#include <QRandomGenerator>
#include <QTimer>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QPushButton>

#include "can-ids/CAN_IDs.h"
#include "COM.hpp"

class LogGenerator : public COM
{
    Q_OBJECT

typedef struct {
    union {
        struct {
            int dev_id; // Least significant
            // First bit of msg_id determines if msg is reserved or not.
            // 0 == reserved (higher priority) (0-31 decimal)
            int msg_id; // Most significant
        };
        int sid;
    };
    int dlc;
    int data[4];
} CANdataa;

public:
    LogGenerator();
    QDialog *Custom;
    virtual bool open(QString) override;
    virtual int close(void) override;
    virtual int status(void) override {return StatusInt;}
        int StatusInt;
    QList<CANdataa> CustMsgList;

public slots:
    virtual int send(QByteArray&) override;
    virtual void read(void) override;
    void addCustMsg(CANdataa *msg = nullptr);
    void clearCustMsgs(void);
    void updateData(int,int);

private:
    QQueue<CANmessage> MsgQueue;
    QTimer *MessageMS;
    QRandomGenerator DataGen;
    void custDialog(void);
    int timestamp;
    double contador;
};

#endif // LOGGENERATOR_H
