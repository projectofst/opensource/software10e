#include "COM_LogPlay.hpp"

LogPlay::LogPlay()
{
    replaytimer = new QTimer;
    replaytimer->setTimerType(Qt::PreciseTimer);
    connect(replaytimer, &QTimer::timeout,
            this, &LogPlay::next_message);
    addOption("No Options");
    _Status = 0;
    protocol = "Player";
}

int LogPlay::send(QByteArray&) {
    return 0;
}

void LogPlay::next_message() {
    if (filestream->atEnd()) {
        replaytimer->stop();
        file->close();
        return;
    }
    QString FileData = filestream->readLine();
    qDebug() << FileData;

    CANmessage message;
    auto fields = FileData.split(",");
    message.candata.sid = fields.at(0).toInt();
    message.candata.dlc = fields.at(1).toInt();
    for (int i=0; i<4; i++) {
        message.candata.data[i] = fields.at(2+i).toInt();

    }
    message.timestamp = fields.at(6).toInt();

    qDebug() << "sending";
    emit new_messages(*(COM::encodeToByteArray<CANmessage>(message).get()));
}

bool LogPlay::open(QString filename)
{
    if (filename.isEmpty() || filename == "")
        filename = QFileDialog::getOpenFileName(nullptr, tr("Open File"), ".", tr("Text files (*.txt)"));


    file = new QFile(filename);
    if(!file->open(QIODevice::ReadOnly)) {
        QMessageBox::information(nullptr, "error", file->errorString());
        _Status = 0;
        return false;
    }
    filestream = new QTextStream(file);

    next_message();
    replaytimer->start(10);

    AbsTime = 0;
    OldTime = -1;
    /*while(!filestream.atEnd()) {
        printf("new line\n");
        templine = filestream.readLine();
        CANmessage msg;
        msg.candata.sid = templine.section(",",0,0).toUShort();
        msg.candata.dlc = templine.section(",",1,1).toUShort();
        msg.candata.data[0] = templine.section(",",2,2).toUShort();
        msg.candata.data[1] = templine.section(",",3,3).toUShort();
        msg.candata.data[2] = templine.section(",",4,4).toUShort();
        msg.candata.data[3] = templine.section(",",5,5).toUShort();
        msg.timestamp = templine.section(",",6,6).toUShort();
        if (AbsTime<OldTime)
            AbsTime = OldTime + msg.timestamp;
        else
            AbsTime = msg.timestamp;
        OldTime = AbsTime;
        Messages.insertMulti(AbsTime,msg);
    }*/
    //file.close();
    AbsTime = 0;
    OldTime = 0;
    _Status = 1;
    return true;
}

void LogPlay::read(void)
{
    /*AbsTime = OldTime + 1000;
    while (OldTime != AbsTime) {
        if (Messages.contains(OldTime)){
            foreach(CANmessage msg,Messages.values(OldTime))
                pendmessages.enqueue(msg);
                emit new_messages(1);
        }
    }*/
}

CANmessage LogPlay::pop(void){

    /*CANmessage package;

    if(!pendmessages.empty()){
        package = pendmessages.dequeue();
    }
    return package;*/

    CANmessage message;
    message.candata.dev_id = 0;
    message.candata.dlc = 0;
    return message;
}

int LogPlay::close(void)
{

    delete this;
    return 1;
}
