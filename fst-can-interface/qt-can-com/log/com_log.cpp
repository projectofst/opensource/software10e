#include "com_log.h"

#include <QString>
#include <QDebug>
#include <QFileDialog>

COM_Log::COM_Log()
{    Status = 0;
    qDebug() << "Start Log";
    this->timer = new QTimer();
    watchdog_timer = new QTimer();
    watchdog_timer->setSingleShot(true);
    connect(watchdog_timer, &QTimer::timeout, this, &COM_Log::watchdog);
}

int COM_Log::status(void)
{
    return Status;
}

bool COM_Log::open(const QString arg)
{
    QString fileName;
    qDebug() << "arg:" << arg;
    if (arg == "Search") {
        fileName = QFileDialog::getOpenFileName(nullptr,
            tr("Open Log"), "", tr("Log Files (*.csv)"));
    }
    else {
        fileName = arg;
    }

    file = new QFile(fileName);
    try {
        file->open(QIODevice::ReadOnly);
    }
    catch (...) {
        QMessageBox msgBox;
        msgBox.setText(QString("Failed to open %s").arg(fileName));
        msgBox.exec();
        return false;
    }

    in = new QTextStream(file);
    connect(this->timer, &QTimer::timeout, this, &COM_Log::read);
    timer->start(10);
    Status = 1;
    time = 0;

    emit upstream_up();
    watchdog_timer->start(1000);
    return true;
}

void COM_Log::read(void) {
    while (true) {
        if (in->atEnd()) {
           timer->stop();
           file->close();
           return;
        }
        QString line = in->readLine();
        auto split = line.split(",");
        if (split.length() != 14) {
            continue;
        }

        CANmessage message;
        message.timestamp = split[0].toFloat();
        message.candata.sid = split[2].toUInt();
        message.candata.dlc = split[4].toUShort();
        for (int i=0; i<4; i++) {
            message.candata.data[i] = (split[6 + 2*i].toUShort() << 8) + split[5 + 2*i].toUShort();
        }
        watchdog_timer->start(1000);
        emit upstream_up();
        emit new_messages(*(COM::encodeToByteArray<CANmessage>(message)));
        if (message.timestamp*100 >= (time + 1)) {
            time += 1;
            break;
        }
    }

    return;
}

QStringList COM_Log::getOptions(void) {
    return {"Search"};

}

int COM_Log::send(QByteArray&) {
    return 0;
}

int COM_Log::close(void) {
    timer->stop();
    file->close();
    Status = -1;
    emit upstream_down();
    return 0;
}

void COM_Log::watchdog() {
    emit upstream_down();
    this->watchdog_timer->start(1000);
}

