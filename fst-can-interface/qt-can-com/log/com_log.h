#ifndef COM_LOG_H
#define COM_LOG_H

#include <QObject>
#include "COM.hpp"

class COM_Log : public COM
{
    Q_OBJECT
public:
    explicit COM_Log();
    int status(void);
    bool open(const QString);
    int close(void);

    QStringList getOptions(void);

public slots:
    void read(void);
    int send(QByteArray& array);

private:
    QFile *file = nullptr;
    QTextStream *in = nullptr;
    QTimer *timer;
    unsigned long time;
    QTimer *watchdog_timer;

private slots:
    void watchdog(void);


signals:

};

#endif // COM_LOG_H
