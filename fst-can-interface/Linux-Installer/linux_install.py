from alive_progress import alive_bar
import click
import distro
import time
import sys
import os

sys.stdout.reconfigure(encoding="utf-8")

setup_scripts = {}

reference_path = os.getcwd().replace("/Linux-Installer", "")
current_distro = distro.linux_distribution(full_distribution_name=False)


def home_dir():
    return "/".join(reference_path.split("/")[0:3])


setup_scripts["manjaro"] = [
    {
        "stage_desc": "Installing Essentials",
        "stage_commands": [
            "pacman -S base base-devel python-pip mesa clang git wget protobuf yaml-cpp ccache --noconfirm"
        ],
    },
    {
        "stage_desc": "Installing QT Components",
        "stage_commands": [
            "pacman -S qt5-base qt5-charts qt5-serialport qt5-serialbus qt5-translations qt5-quickcontrols2 qt5-x11extras --noconfirm"
        ],
    },
    {
        "stage_desc": "Downloading Qt-Advanced-Docking-System",
        "stage_commands": [
            "cd " + reference_path + " && git submodule init && git submodule update",
        ],
    },
    {
        "stage_desc": "Building Qt-Advanced-Docking-System",
        "stage_commands": [
            "cd "
            + reference_path
            + "/Qt-Advanced-Docking-System && qmake && make install",
        ],
    },
    {
        "stage_desc": "Copying libs",
        "stage_commands": [
            "cd "
            + reference_path
            + "/Qt-Advanced-Docking-System && cp installed/lib/* /usr/lib",
        ],
    },
    {
        "stage_desc": "Building CAN Interface",
        "stage_commands": [
            "cd " + reference_path + " && make",
        ],
    },
]

setup_scripts["ubuntu"] = [
    {
        "stage_desc": "Installing Essentials",
        "stage_commands": [
            "apt install -y git cmake wget software-properties-common build-essential libssl-dev libxcb1-dev libx11-dev libgl1-mesa-dev libudev-dev libyaml-cpp-dev libfuse2 ccache"
        ],
    },
    {
        "stage_desc": "Registering APT rep",
        "stage_commands": [
            "add-apt-repository -y ppa:beineri/opt-qt-5.15.2-" + current_distro[2],
            "apt-get -y update",
        ],
    },
    {
        "stage_desc": "Performing ENV Config",
        "stage_commands": [
            "echo 'source /opt/qt515/bin/qt515-env.sh' >> " + home_dir() + "/.bashrc"
        ],
    },
    {
        "stage_desc": "Installing QT Components",
        "stage_commands": [
            "apt-get -y install qt515base qt515charts-no-lgpl qt515declarative qt515quickcontrols2 qt515serialport qt515serialbus qt515x11extras"
        ],
    },
    {
        "stage_desc": "Downloading Qt-Advanced-Docking-System",
        "stage_commands": [
            "cd " + reference_path + " && git submodule init && git submodule update",
        ],
    },
    {
        "stage_desc": "Building Qt-Advanced-Docking-System",
        "stage_commands": [
            "cd "
            + reference_path
            + "/Qt-Advanced-Docking-System && /opt/qt515/bin/qmake && make install",
        ],
    },
    {
        "stage_desc": "Copying libs",
        "stage_commands": [
            "cd "
            + reference_path
            + "/Qt-Advanced-Docking-System && cp installed/lib/* /usr/lib",
        ],
    },
    {
        "stage_desc": "WARNING",
        "stage_commands": [
            "echo WARNING: PLEASE RESTART YOUR TERMINAL IN ORDER TO REFRESH ENV VARIABLES",
            "echo THEN YOU JUST NEED TO RUN make inside fst-can-interface folder",
        ],
    },
]


def title():
    print(" ______ _____ _______ ")
    print("|  ____/ ____|__   __|")
    print("| |__ | (___    | |   ")
    print("|  __| \___ \   | |   ")
    print("| |    ____) |  | |   ")
    print("|_|   |_____/   |_|   ")
    print("================================================")
    print("FST CAN Interface Development Environment Setup")
    print("Developed by Gonçalo Abreu Corrêa Brito da Silva")
    print("================================================")
    print("This software licence can be found at:")
    print(
        "https://gitlab.com/projectofst/software10e/-/raw/master/fst-can-interface/Windows-Installer/packages/CANinterface/meta/license.txt"
    )
    print("\n")


def expected(setup_script):
    total = 0
    for stage in setup_script:
        total += len(stage["stage_commands"])
    return total


if __name__ == "__main__":

    if os.geteuid() != 0:
        print("Please run as root")
        sys.exit(0)

    title()

    if not click.confirm("Do you agree with the license terms?"):
        sys.exit(0)

    print("Installing FST CAN Interface at " + "-".join(current_distro))

    if current_distro[0] not in setup_scripts:
        print("[ERROR]: This installer doesn't support your distro yet")
        sys.exit(0)

    if os.getcwd().split("/")[-1] != "Linux-Installer":
        print(
            "[ERROR]: Unknown location. Please make sure to run this script at: fst-can-interface/Linux-Installer"
        )
        sys.exit(0)

    script = setup_scripts[current_distro[0]]

    with alive_bar(expected(script)) as bar:
        for stage in script:
            bar.text(stage["stage_desc"])
            for command in stage["stage_commands"]:
                os.system(command)
                bar()
